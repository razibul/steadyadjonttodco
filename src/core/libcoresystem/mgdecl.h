#ifndef __MGDECL_H__
#define __MGDECL_H__


#ifdef WIN32
	#pragma warning(disable: 4114)
    #pragma warning(disable: 4786)
    #pragma warning(disable: 4788)
    #pragma warning(disable: 4267)

	#pragma warning(disable : 4996)
#endif


//////////////////////////////////////////////////////////////////////
// C includes
//////////////////////////////////////////////////////////////////////
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <time.h>

//////////////////////////////////////////////////////////////////////
// C++ includes
//////////////////////////////////////////////////////////////////////
// none


#include "stldecl.h"	// includes files STL


//////////////////////////////////////////////////////////////////////
// definitions of basic types
//////////////////////////////////////////////////////////////////////

// definintion of MGNEW which does not throw exception
//#define MGNEW new			// old standard
//#define MGNEW new(nothrow)	// recent standard


// definition of basic types and their sizes
typedef char				MGChar;
typedef int					MGInt;
typedef unsigned int		MGSize;
typedef long long			MGLInt;
typedef unsigned long long	MGLSize;
typedef double 				MGFloat;
typedef string				MGString;

#define TYPESIZE_MGFLOAT	8
#define TYPESIZE_MGCHAR		1
#define TYPESIZE_MGINT		4
#define TYPESIZE_MGSIZE		4
#define TYPESIZE_MGLINT		8
#define TYPESIZE_MGLSIZE	8 



typedef vector<MGFloat>	MGFloatArr;
typedef vector<MGInt>	MGIntArr;

// function for checking size of standard types
void CheckTypeSizes();


//////////////////////////////////////////////////////////////////////
// auxiliary objects 
//////////////////////////////////////////////////////////////////////
#include "mgexcept.h"	// file with objects and macros used for exception handling
#include "mgconst.h"	// file with definitions of constant values
#include "mgversion.h"	// file with objects and macros used for version tracking


#ifndef MAX
#define MAX(a,b)    (((a) > (b)) ? (a) : (b))
#endif

#ifndef MIN
#define MIN(a,b)    (((a) < (b)) ? (a) : (b))
#endif

#define SIGN(f)	    ((f) < 0 ? -1 : 1)


extern const char STR_TRUE[];
extern const char STR_FALSE[];

bool StrToBool( const MGString& str);
MGString BoolToStr( const bool& b);


#endif	// __MGDECL_H__
