#include "libcoresystem/mgdecl.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceExceptionHandling {

// constants used for exception handling
const char EX_FILE_STR[]		= "FILE";
const char EX_MATH_STR[]		= "MATH";
const char EX_MEMORY_STR[]		= "MEM";
const char EX_INTERNAL_STR[]	= "INTERNAL";
const char EX_ASSERT_STR[]		= "ASSERT";
const char EX_REXP_STR[]		= "REXP";
const char EX_SMATRIX_STR[]		= "SMATRIX";



} // end of namespace ExceptionHandlingTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
