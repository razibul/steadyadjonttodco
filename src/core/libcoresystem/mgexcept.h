#ifndef __MGEXCEPT_H__
#define __MGEXCEPT_H__


#define THIS_FILE __FILE__	// defines name of header or implementation file


#ifdef WIN32
	#define ISNAN(f)  _isnan(f)
	#define ISINF(f)  ! _finite(f)
#else
	#define ISNAN(f)  isnan(f)
	#define ISINF(f)  ! finite(f)
#endif



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceExceptionHandling {

// constants used for exception handling
extern const char EX_FILE_STR[];
extern const char EX_INTERNAL_STR[];
extern const char EX_ASSERT_STR[];
extern const char EX_MATH_STR[];


// name of file used for tracing
#define TRACE_FILE_NAME "trace.txt"



//////////////////////////////////////////////////////////////////////
// class Trace
//////////////////////////////////////////////////////////////////////
class Trace
{
public:
	Trace( const MGString& name) : mName( name ) { mFile.open( mName.c_str()); mFile.close(); }

	ostream&	rFile()		{ return mFile;}

	void		Open()		{ mFile.open( mName.c_str(), ios_base::out | ios_base::app ); Verify(); }
	void		Close()		{ mFile.close();}

	void		Verify();
	void		Out( const char *sfile, const MGInt& nline);

private:
	MGString	mName;
	ofstream	mFile;
};

inline void Trace::Verify()
{
	if ( mFile.bad() )
		cout << "the gtrace file '" << mName << "' is bad" << endl;
}

inline void Trace::Out( const char *sfile, const MGInt& nline)
{	
	rFile() << "FILE:" << sfile << " - LINE:" << nline << ";  ";
}

//////////////////////////////////////////////////////////////////////
//	class TraceSingleton
//////////////////////////////////////////////////////////////////////
class TraceSingleton
{
public:
	
	static Trace*	GetInstance()
	{
		static auto_ptr<Trace>	ptr;
		
		if ( ptr.get() == 0)
			ptr.reset( new Trace( TRACE_FILE_NAME) );

		return ptr.get();
	}

protected:
	TraceSingleton()		{}

private:
};
//////////////////////////////////////////////////////////////////////




//////////////////////////////////////////////////////////////////////
// class Except - base, abstract class for all exceptions
//////////////////////////////////////////////////////////////////////
class Except
{
public:
	Except()				{ mComment = mFileName = ""; mLineNo = 0;}

	Except( const Except& ex) : mFileName(ex.mFileName), mLineNo(ex.mLineNo), mComment(ex.mComment)  {};
	Except( MGString com, MGString fname, MGInt line) : mFileName(fname), mLineNo(line), mComment(com) {};
		
	virtual ~Except()	{};

    Except& operator = (const Except& ex);
	
	virtual MGString	GetExPrefix() 	 const 	= 0;
	
	virtual void		WriteInfo( ostream &f);

protected:
	MGString	mFileName;
	MGInt		mLineNo;
	MGString	mComment;	
};


inline Except& Except::operator = (const Except& ex)
{ 
	mFileName = ex.mFileName;
	mLineNo   = ex.mLineNo;
	mComment  = ex.mComment;
	return *this; 
}


inline void Except::WriteInfo( ostream &f)
{
	f << "FILE:" << mFileName << " - LINE:" << mLineNo << ";" << endl;
	f << "    " << GetExPrefix() << ": " << mComment << endl;
}



//////////////////////////////////////////////////////////////////////
// class ExceptFile
//  used when some problems occur with opening, reading and parsing
//  information from files
//  passes to handler comment + add. info. (e.g. file name)
//////////////////////////////////////////////////////////////////////
class ExceptFile : public Except
{	
protected:
	MGString	mFileInfo;
	
public:
	ExceptFile()						{ mFileInfo = "";}
	ExceptFile( MGString com, MGString info, MGString fname, MGInt line)
		: Except( com, fname, line)		{mFileInfo = info;};
	virtual ~ExceptFile()	{};
	
	virtual MGString	GetExPrefix()	const 	{ return EX_FILE_STR;}

	virtual void		WriteInfo( ostream &f);
};

inline void ExceptFile::WriteInfo( ostream &f)
{
	f << "FILE:" << mFileName << " - LINE:" << mLineNo << ";" << endl;
	f << "    " << GetExPrefix() << ": " << mComment << " '" << mFileInfo << "'" << endl;
}



//////////////////////////////////////////////////////////////////////
// class ExceptAssert
//	used in ASSERT macro
//////////////////////////////////////////////////////////////////////
class ExceptAssert : public Except
{	
public:
	ExceptAssert()			{};
	ExceptAssert( MGString com, MGString fname, MGInt line) 
		: Except( com, fname, line) {};
		
	virtual ~ExceptAssert()	{};
	
	virtual MGString	GetExPrefix()	const { return EX_ASSERT_STR;}
};

//////////////////////////////////////////////////////////////////////
// class InternalException
//  for handling general internal problems
//////////////////////////////////////////////////////////////////////
class ExceptInternal : public Except
{
public:
	ExceptInternal()			{};
	ExceptInternal( MGString com, MGString fname, MGInt line) 
		: Except( com, fname, line) {};
		
	virtual ~ExceptInternal()	{};
	
	virtual MGString	GetExPrefix()	const { return EX_INTERNAL_STR;}
};

//////////////////////////////////////////////////////////////////////
// class ExceptMath
//  for math. calc. exceptions
//////////////////////////////////////////////////////////////////////
class ExceptMath : public Except
{	
public:
	ExceptMath()				{};
	ExceptMath( MGString com, MGString fname, MGInt line) 
		: Except( com, fname, line) {};
		
	virtual ~ExceptMath()	{};
	
	virtual MGString	GetExPrefix()	const { return EX_MATH_STR;}
};

} // end of namespace SpaceExceptionHandling
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace EHandler = SpaceExceptionHandling;







//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//// macros for tracing
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

#define INFO(sz) \
	{ \
		EHandler::TraceSingleton::GetInstance()->Open(); \
		EHandler::TraceSingleton::GetInstance()->Out( THIS_FILE, __LINE__); \
		EHandler::TraceSingleton::GetInstance()->rFile() << sz << endl; \
		EHandler::TraceSingleton::GetInstance()->Close(); \
	}

//-----------------------------------------
#ifdef _DEBUG
//-----------------------------------------

#define TRACE(sz) \
	{ \
		EHandler::TraceSingleton::GetInstance()->Open(); \
		EHandler::TraceSingleton::GetInstance()->Out( THIS_FILE, __LINE__); \
		EHandler::TraceSingleton::GetInstance()->rFile() << sz << endl; \
		EHandler::TraceSingleton::GetInstance()->Close(); \
	}


#define TRACE_EXCEPTION(e) \
	{ \
		EHandler::TraceSingleton::GetInstance()->Open(); \
		(e).WriteInfo( EHandler::TraceSingleton::GetInstance()->rFile() ); \
		EHandler::TraceSingleton::GetInstance()->Close(); \
	}


//extern EHandler::Trace gtrace;
//
//
//#define INIT_TRACE	EHandler::Trace gtrace( TRACE_FILE_NAME );
//
//#define TRACE(sz) \
//	{ \
//		gtrace.Open(); \
//		gtrace.Out( THIS_FILE, __LINE__); \
//		gtrace.rFile() << sz << endl; \
//		gtrace.Close(); \
//	}
//
//
//#define TRACE_EXCEPTION(e) \
//	{ \
//		gtrace.Open(); \
//		(e).WriteInfo( gtrace.rFile() ); \
//		gtrace.Close(); \
//	}



//-----------------------------------------
#else // _DEBUG
//-----------------------------------------

#define INIT_TRACE

#define	TRACE(sz) \
	{ \
	}

#define	TRACE_EXCEPTION(e) \
	{ \
	}

//-----------------------------------------
#endif // _DEBUG
//-----------------------------------------





#define TRACE_TO_STDERR(e) \
	{ \
		(e).WriteInfo( cerr); \
	}

#define TRACE_TO_CERR TRACE_TO_STDERR


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//// macros for throwing Exceptions 
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
	#ifdef assert
		// if compiler provides a assert macro in "assert.h"
		#define ASSERT assert
	#else
		// assert macro which does not depend on compiler; uses exception ExceptAssert
		#define ASSERT(f) \
		{ \
		if (!(f)) \
			throw EHandler::ExceptAssert( "assertion failed: '" #f "'", THIS_FILE, __LINE__ ); \
		}
	#endif // assert
#else
	#define ASSERT(f) \
	{ \
	}
#endif // _DEBUG





#define THROW_FILE(f1, f2) \
	{ \
		throw EHandler::ExceptFile( f1, f2, THIS_FILE, __LINE__ ); \
	}

#define THROW_INTERNAL(f) \
	{ \
		ostringstream _os_; \
		_os_ << f; \
		throw EHandler::ExceptInternal( _os_.str(), THIS_FILE, __LINE__ ); \
	}


#define THROW_INTERNAL_OBSOLETE(f) \
	{ \
		throw EHandler::ExceptInternal( f, THIS_FILE, __LINE__ ); \
	}





// TODO:: fix for proper resolving of s in IS_INFNAN

#define IS_INFNAN_THROW(f,s) \
{ \
	if ( ISNAN(f)) \
		throw EHandler::ExceptMath( "not a number: '" #f "' - " #s " ", THIS_FILE, __LINE__ ); \
	if ( ISINF(f)) \
		throw EHandler::ExceptMath( "INF number: '" #f "' - " #s " ", THIS_FILE, __LINE__ ); \
}


#define ISNAN_THROW(f,s) \
{ \
	if ( ISNAN(f)) \
		throw EHandler::ExceptMath( "not a number: '" #f "' - " #s " ", THIS_FILE, __LINE__ ); \
}

#define ISINF_THROW(f,s) \
{ \
	if ( ISINF(f)) \
		throw EHandler::ExceptMath( "INF number: '" #f "' - " #s " ", THIS_FILE, __LINE__ ); \
}






#endif	// __MGEXCEPT_H__
