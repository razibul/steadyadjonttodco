#include "libcoresystem/mgdecl.h"

const char STR_TRUE[]	= "TRUE";
const char STR_FALSE[]	= "FALSE";

bool StrToBool( const MGString& str)
{
	if ( str == MGString( STR_TRUE) )
		return true;
	else if ( str == MGString( STR_FALSE) )
		return false;
	else
	{
		ostringstream os;
		os << "unrecognized string used for bool representation: " << str;
		THROW_INTERNAL( os.str().c_str() );
	}
}

MGString BoolToStr( const bool& b)
{
	if ( b)
		return MGString( STR_TRUE);
	else
		return MGString( STR_FALSE);
}


void CheckTypeSizes()
{
	bool bOk = true;
	//cout << "MGFloat\t" << sizeof(MGFloat) << "\t" << TYPESIZE_MGFLOAT << endl;
	//cout << "MGChar\t"  << sizeof(MGChar)  << "\t" << TYPESIZE_MGCHAR << endl;
	//cout << "MGInt\t"   << sizeof(MGInt)   << "\t" << TYPESIZE_MGINT << endl;
	//cout << "MGSize\t"  << sizeof(MGSize)  << "\t" << TYPESIZE_MGSIZE << endl;
	//cout << "MGLInt\t"  << sizeof(MGLInt)  << "\t" << TYPESIZE_MGLINT << endl;
	//cout << "MGLSize\t" << sizeof(MGLSize) << "\t" << TYPESIZE_MGLSIZE << endl;

	if ( sizeof(MGFloat) != TYPESIZE_MGFLOAT )
	{
		bOk = false;
		cout	<< "size of MGFloat is not compatible (is " 
				<< sizeof(MGFloat) <<"; should be " << TYPESIZE_MGFLOAT << ")" << endl;
	}

	if ( sizeof(MGChar)  != TYPESIZE_MGCHAR )
	{
		bOk = false;
		cout	<< "size of MGChar is not compatible (is " 
				<< sizeof(MGChar) <<"; should be " << TYPESIZE_MGCHAR << ")" << endl;
	}

	if ( sizeof(MGInt)   != TYPESIZE_MGINT )
	{
		bOk = false;
		cout	<< "size of MGInt is not compatible (is " 
				<< sizeof(MGInt) <<"; should be " << TYPESIZE_MGINT << ")" << endl;
	}

	if ( sizeof(MGSize)  != TYPESIZE_MGSIZE )
	{
		bOk = false;
		cout	<< "size of MGSize is not compatible (is " 
				<< sizeof(MGSize) <<"; should be " << TYPESIZE_MGSIZE << ")" << endl;
	}

	if ( sizeof(MGLInt)  != TYPESIZE_MGLINT )
	{
		bOk = false;
		cout	<< "size of MGLInt is not compatible (is " 
				<< sizeof(MGLInt) <<"; should be " << TYPESIZE_MGLINT << ")" << endl;
	}

	if ( sizeof(MGLSize) != TYPESIZE_MGLSIZE )
	{
		bOk = false;
		cout	<< "size of MGLSize is not compatible (is " 
				<< sizeof(MGLSize) <<"; should be " << TYPESIZE_MGLSIZE << ")" << endl;
	}

	if ( ! bOk)
		THROW_INTERNAL( "from CheckTypeSizes()");
}



