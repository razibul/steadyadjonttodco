#ifndef __CONVGRID_H__
#define __CONVGRID_H__


#include "libcoreio/gridfacade.h"
#include "libcoreio/intffacade.h"
#include "libcoregeom/vect.h"

using namespace Geom;

const MGSize CELLSIZE_EDGE	= 2;

const MGSize CELLSIZE_TRI	= 3;
const MGSize CELLSIZE_QUAD	= 4;

const MGSize CELLSIZE_TET	= 4;
const MGSize CELLSIZE_HEX	= 8;
const MGSize CELLSIZE_PRISM	= 6;
const MGSize CELLSIZE_PIRAM	= 5;

const MGSize CELLNFACE_TET		= 4;
const MGSize CELLNFACE_HEX		= 6;
const MGSize CELLNFACE_PRISM	= 5;
const MGSize CELLNFACE_PIRAM	= 5;




//////////////////////////////////////////////////////////////////////
// class GridSizes
//////////////////////////////////////////////////////////////////////
template <MGSize DIM>
class GridSizes
{
};

template <>
class GridSizes<DIM_1D>
{
public:
	enum { CELL_MAXSIZE		= CELLSIZE_EDGE };
};

template <>
class GridSizes<DIM_2D>
{
public:
	enum { CELL_MAXSIZE		= CELLSIZE_QUAD };
};

template <>
class GridSizes<DIM_3D>
{
public:
	enum { CELL_MAXSIZE		= CELLSIZE_HEX };
};




//////////////////////////////////////////////////////////////////////
// class ConvNode
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class ConvNode : public Vect<DIM>
{
};

//////////////////////////////////////////////////////////////////////
// class ConvNode
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class ConvCell
{
public:
	ConvCell( const MGSize& type=0) : mType(type)	{}

	MGSize			Size() const				{ return mType;}
	const MGSize&	cId( const MGSize& i) const	{ return mtabId[i];}
	MGSize&			rId( const MGSize& i)		{ return mtabId[i];}

private:
	MGSize	mType;
	MGSize	mtabId[ GridSizes<DIM>::CELL_MAXSIZE ];
};

//////////////////////////////////////////////////////////////////////
// class ConvNode
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class ConvBFace : public ConvCell< static_cast<Dimension>( DIM-1)>
{
public:
	const MGSize&	cSurfId() const	{ return mSurfId;}
	MGSize&			rSurfId()		{ return mSurfId;}

private:
	MGSize	mSurfId;
};



//////////////////////////////////////////////////////////////////////
// class ConvGrid
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class ConvGrid : public IO::GridBndFacade, public IO::GridReadFacade
{
public:
	ConvGrid( const MGString& name="-") : mGrdName( name)	{}
	virtual	~ConvGrid()		{}

	const MGString&	Name() const	{ return mGrdName;}

	MGSize	SizeNodeTab() const		{ return mtabNode.size(); }
	MGSize	SizeCellTab() const		{ return mtabCell.size(); }
	//MGSize	SizeInCellTab() const	{ return mtabInCell.size(); }
	//MGSize	SizeExCellTab() const	{ return mtabExCell.size(); }
	MGSize	SizeBFaceTab() const	{ return mtabBFace.size(); }

	const ConvNode<DIM>&		cNode( const MGSize& i) const	{ return mtabNode[i];}
	const ConvCell<DIM>&		cCell( const MGSize& i) const	{ return mtabCell[i];}
	const ConvBFace<DIM>&		cBFace( const MGSize& i) const	{ return mtabBFace[i];}


	void	Transform();

protected:

	ConvCell<DIM>&		rCell( const MGSize& i)		{ return mtabCell[i];}
	ConvBFace<DIM>&		rBFace( const MGSize& i) 	{ return mtabBFace[i];}

protected:

	// grid facade
	virtual Dimension	Dim() const											{ return DIM;}

	virtual MGSize	IOSizeNodeTab() const									{ return mtabNode.size(); }
	virtual MGSize	IOSizeCellTab( const MGSize& type) const;
	virtual MGSize	IOSizeBFaceTab( const MGSize& type) const;

	virtual void	IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	virtual void	IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	virtual void	IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id) const;

	virtual MGSize	IONumCellTypes() const									{ return 1;}
	virtual MGSize	IONumBFaceTypes() const									{ return 1;}
	virtual MGSize	IOCellType( const MGSize& i) const;
	virtual MGSize	IOBFaceType( const MGSize& i) const;

	virtual void	IOTabNodeResize( const MGSize& n)						{ mtabNode.resize( n);}
	virtual void	IOTabCellResize( const MGSize& type, const MGSize& n);
	virtual void	IOTabBFaceResize( const MGSize& type, const MGSize& n);

	virtual void	IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id);
	virtual void	IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id );
	virtual void	IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id );
	virtual void	IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id );

	virtual void	IOGetName( MGString& name) const						{ name = mGrdName;}
	virtual void	IOSetName( const MGString& name)						{ mGrdName = name;}

	// grid bnd facade
	virtual MGSize	IOSizeBNodeTab() const	{ return 0;}

	virtual void	IOGetBNodePos( vector<MGFloat>& tabx, const MGSize& id) const	{}
	virtual void	IOGetBNodeVn( vector<MGFloat>& tabx, const MGSize& id) const	{}
	virtual void	IOGetBNodePId( MGSize& idparent, const MGSize& id) const		{}
	virtual void	IOGetBFaceBIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const {}

	
private:
	MGString		mGrdName;

	vector< ConvNode<DIM> >		mtabNode;
	vector< ConvCell<DIM> >		mtabCell;
	vector< ConvBFace<DIM> >	mtabBFace;
};
//////////////////////////////////////////////////////////////////////



template <Dimension DIM>
MGSize ConvGrid<DIM>::IOSizeCellTab( const MGSize& type) const				
{ 
	if ( type == (DIM+1) )
		return mtabCell.size();

	return 0;
}


template <Dimension DIM>
MGSize ConvGrid<DIM>::IOSizeBFaceTab( const MGSize& type) const				
{ 
	if ( type == DIM )
		return mtabBFace.size();

	return 0;
}

template <Dimension DIM>
MGSize ConvGrid<DIM>::IOCellType( const MGSize& i) const						
{ 
	if ( i == 0 )
		return DIM+1;

	THROW_INTERNAL( "ConvGrid<DIM>::IOCellType - too many types");

	return 0;
}


template <Dimension DIM>
MGSize ConvGrid<DIM>::IOBFaceType( const MGSize& i) const						
{ 
	if ( i == 0 )
		return DIM;

	THROW_INTERNAL( "ConvGrid<DIM>::IOBFaceType - too many types");

	return 0;
}

template <Dimension DIM>
void ConvGrid<DIM>::IOTabCellResize( const MGSize& type, const MGSize& n)	
{ 
	if ( type == (DIM+1) )
		mtabCell.resize( n);
}


template <Dimension DIM>
void ConvGrid<DIM>::IOTabBFaceResize( const MGSize& type, const MGSize& n)	
{ 
	if ( type == DIM )
		mtabBFace.resize( n);
}



template <Dimension DIM>
void ConvGrid<DIM>::IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mtabNode[id].cX(i);
}


template <Dimension DIM>
void ConvGrid<DIM>::IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	if ( type != (DIM+1) )
		THROW_INTERNAL("only simplex type cells are implemented !");
	
	ASSERT( tabid.size() == (DIM+1) );

	for ( MGSize i=0; i<=DIM; ++i)
		tabid[i] = mtabCell[id].cId(i);
}


template <Dimension DIM>
void ConvGrid<DIM>::IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	if ( type != (DIM) )
		THROW_INTERNAL("only simplex type faces are implemented !");
	
	ASSERT( tabid.size() == (DIM) );

	for ( MGSize i=0; i<DIM; ++i)
		tabid[i] = mtabBFace[id].cId(i);
}


template <Dimension DIM>
void ConvGrid<DIM>::IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id) const
{
	if ( type != (DIM) )
		THROW_INTERNAL("only simplex type faces are implemented !");

	idsurf = mtabBFace[id].cSurfId();
}


template <Dimension DIM>
void ConvGrid<DIM>::IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id)
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		mtabNode[id].rX(i) = tabx[i];
}


template <Dimension DIM>
void ConvGrid<DIM>::IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
{
	if ( type != (DIM+1) )
		THROW_INTERNAL("only simplex type cells are implemented !");
	
	ASSERT( tabid.size() == (DIM+1) );

	for ( MGSize i=0; i<=DIM; ++i)
		mtabCell[id].rId(i) = tabid[i];
}


template <Dimension DIM>
void ConvGrid<DIM>::IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
{
	if ( type != (DIM) )
		THROW_INTERNAL("only simplex type faces are implemented !");
	
	ASSERT( tabid.size() == (DIM) );

	for ( MGSize i=0; i<DIM; ++i)
		mtabBFace[id].rId(i) = tabid[i];
}


template <Dimension DIM>
void ConvGrid<DIM>::IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id )
{
	if ( type != (DIM) )
		THROW_INTERNAL("only simplex type faces are implemented !");

	mtabBFace[id].rSurfId() = idsurf;
}



template <>
inline void ConvGrid<DIM_2D>::Transform()
{
	const MGFloat x0 = -0.5;
	const MGFloat dx =  2.0;
	const MGFloat r0 =  1.0;

	for ( MGSize i=0; i<mtabNode.size(); ++i)
	{
		Vect2D pos = 	mtabNode[i];

		MGFloat x = ( pos.cX() - x0 ) / dx;
		MGFloat fi = 0.5 * (1. - x) * M_PI;
		MGFloat r = pos.cY() + r0;

		mtabNode[i].rX() = r * cos( fi);
		mtabNode[i].rY() = r * sin( fi);
	}
}


template <>
inline void ConvGrid<DIM_3D>::Transform()
{
	const MGFloat x0 = -0.5;
	const MGFloat dx =  2.0;
	const MGFloat r0 =  1.0;

	for ( MGSize i=0; i<mtabNode.size(); ++i)
	{
		Vect3D pos = 	mtabNode[i];

		MGFloat x = ( pos.cX() - x0 ) / dx;
		MGFloat fi = 0.5 * (1. - x) * M_PI;
		MGFloat r = pos.cY() + r0;

		mtabNode[i].rX() = r * cos( fi);
		mtabNode[i].rY() = r * sin( fi);
		mtabNode[i].rZ() = pos.cZ();
	}
}


#endif // __CONVGRID_H__
