#include "libcoresystem/mgdecl.h"

#include "libcoreio/readmsh2.h"
#include "libcoreio/writemsh2.h"
#include "libcoreio/writetec.h" 
#include "libcoreio/writethor.h" 
#include "libcoreio/writevtk.h" 

#include "libcoreio/readsol.h" 
#include "libcoreio/writesol.h" 

#include "libcoreio/readplot3d.h" 

#include "convgrid.h"
#include "convsolution.h"

//////////////////////////////////////////////////////////////////////
int main( int argc, char* argv[])
{
	try
	{

		{
			ConvGrid<DIM_3D> grid3d;

			IO::ReadMSH2	readMSH2( grid3d);
			readMSH2.DoRead( argv[1], false);

			grid3d.Transform();

			IO::WriteMSH2	writeMSH2( grid3d);
			writeMSH2.DoWrite( argv[2], false);

			IO::WriteTEC	writeTEC( grid3d);
			writeTEC.DoWrite( "out.dat");

			return 0;
		}
		{
			ConvGrid<DIM_2D> grid2d;

			IO::ReadMSH2	readMSH2( grid2d);
			readMSH2.DoRead( argv[1], false);

			grid2d.Transform();

			IO::WriteMSH2	writeMSH2( grid2d);
			writeMSH2.DoWrite( argv[2], false);

			IO::WriteTEC	writeTEC( grid2d);
			writeTEC.DoWrite( "out.dat");

			return 0;
		}

		{
			//if ( argc != 5 && argc != 6 )
			//{
			//	printf( "call:\txconvert  [in sol name] bin/ascii [out sol name] bin/ascii  [[title]]\n");
			//	cout << argc << endl;
			//	return 1;
			//}

			//MGString title = "";
			//if ( argc == 6 )
			//	title = MGString( argv[5] );


			//ConvSolution<DIM_3D,6> sol3d;

			//bool bin = ( MGString(argv[2]) == MGString("bin") );
			//bool bout = ( MGString(argv[4]) == MGString("bin") );

			//IO::ReadSOL		readSOL( sol3d);
			//readSOL.DoRead( argv[1], bin);

			//IO::WriteSOL	writeSOL( sol3d, title);
			//writeSOL.DoWrite( argv[3], bout);


			ConvGrid<DIM_2D> grid2d;

			IO::ReadPlot3D readPlot3D( grid2d);
			readPlot3D.DoRead( argv[1]);

			IO::WriteTEC	writeTEC( grid2d);
			writeTEC.DoWrite( "out.dat");

			IO::WriteMSH2	writeMSH2( grid2d);
			writeMSH2.DoWrite( argv[2], false);

			return 0;


			//IO::ReadMSH2	readMSH2( grid2d);
			//readMSH2.DoRead( argv[1], false);

			//IO::WriteVTK	writeVTK( grid2d);
			//writeVTK.DoWrite( argv[2]);

			//return 0;
		}


		{
			// solution BIN to ASCII or ASCII to BIN
			if ( argc != 5 && argc != 6 )
			{
				printf( "call:\txconvert  [in sol name] bin/ascii [out sol name] bin/ascii  [[title]]\n");
				cout << argc << endl;
				return 1;
			}

			MGString title = "";
			if ( argc == 6 )
				title = MGString( argv[5] );


			ConvSolution<DIM_3D,6> sol3d;

			bool bin = ( MGString(argv[2]) == MGString("bin") );
			bool bout = ( MGString(argv[4]) == MGString("bin") );

			IO::ReadSOL		readSOL( sol3d);
			readSOL.DoRead( argv[1], bin);

			IO::WriteSOL	writeSOL( sol3d, title);
			writeSOL.DoWrite( argv[3], bout);

			return 0;
		}



		ConvGrid<DIM_2D> grid2d;

		IO::ReadMSH2	readMSH2( grid2d);
		readMSH2.DoRead( argv[1], false);

		IO::WriteMSH2	writeMSH2( grid2d);
		writeMSH2.DoWrite( argv[2], false);

		//IO::WriteTEC	writeTEC( grid2d);
		//writeTEC.DoWrite( argv[2]);

		return 0;

		//---------------------------------------------------------------------
	
		//ConvGrid<DIM_3D> grid;

		//IO::ReadMSH2	readMSH2( grid);
		//readMSH2.DoRead( "grid.msh2", false);

		//IO::WriteTEC	writeTEC( grid);
		//writeTEC.DoWrite( "out.dat");

		//IO::WriteTHOR	writeTHOR( grid);
		//writeTHOR.DoWrite( "out.thor0");

		//return 0;

		//if ( argc != 4)
		//{
		//	printf( "call:\txconvert [msh2 name]\n");
		//	return 1;
		//} 
		
		//---------------------------------------------------------------------

		return 0;

	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		cout << endl << "System exception: " << e.what() << endl;
	}
	catch( ...) 
	{
		TRACE( "Unknown exception thrown" );
		cout << endl << "Unknown exception thrown" << endl;
	}

	return 0;
} 
 