#ifndef __FGARRVECTOR_H__
#define __FGARRVECTOR_H__

#include "libcoresystem/mgdecl.h"
#include "arrvector.h"
#include "arrbitset.h"


//////////////////////////////////////////////////////////////////////
//	class fgarrvector - dynamic array with gaps; based on arrvector container
//	indexing is like for fortran arrays - 1...N
//////////////////////////////////////////////////////////////////////
template <class T,size_t SHIFT=16>
class fgarrvector
{
	typedef arrvector<T,SHIFT>	vector_type;
	typedef arrbitset<SHIFT>	bitset_type;

public:
	typedef size_t				size_type;
	typedef T					value_type;
	typedef value_type			*pointer;
	typedef value_type&			reference;
	typedef const value_type	*const_pointer;
	typedef const value_type&	const_reference;


	class const_iterator;


	////////////////////////////
	// CLASS iterator
	////////////////////////////
	class iterator
	{
		friend class fgarrvector<T,SHIFT>;
		friend class const_iterator;

	protected:		
		iterator( const size_type& ipos, fgarrvector<T,SHIFT>* ptab) : miPos(ipos), mpTab(ptab)	{}

	public:
		typedef forward_iterator_tag			iterator_category;
		typedef class fgarrvector<T,SHIFT>		parent;

		typedef typename parent::pointer		pointer;
		typedef typename parent::reference		reference;
		typedef typename parent::value_type		value_type;

		
		iterator()	{}

		iterator( const iterator& itr)	{ miPos=itr.miPos; mpTab=itr.mpTab;}

		const size_type&	index() const		{ return miPos;}

		bool				is_valid() const	{ return mpTab->is_valid(miPos);}

		
		reference operator*() const
		{
			return (*mpTab)[miPos];
		}

		pointer operator->() const
		{
			return (&( (*mpTab)[miPos] ));
		}

		iterator& operator ++()	
		{
			do	{
				++miPos;
				if ( miPos >= mpTab->size() )
					break;
			} while ( ! mpTab->is_valid(miPos) );
			return (*this);
		}

		iterator operator ++(int)	
		{
			iterator itmp(*this);
			do	{
				++miPos;
				if ( miPos >= mpTab->size() )
					break;
			} while ( ! mpTab->is_valid(miPos) );
			return itmp;
		}

		iterator& operator --()	
		{
			do	{
				--miPos;
				if ( miPos <= 0 )
					break;
			} while ( ! mpTab->is_valid(miPos) );
			return (*this);
		}

		iterator operator --(int)	
		{
			iterator itmp(*this);
			do	{
				--miPos;
				if ( miPos <= 0 )
					break;
			} while ( ! mpTab->is_valid(miPos) );
			return itmp;
		}

		bool operator == (const iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos == i2.miPos);
		}

		bool operator != (const iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos != i2.miPos);
		}

		bool operator < (const iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos < i2.miPos);
		}

		bool operator > (const iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos > i2.miPos);
		}

		bool operator <= (const iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos <= i2.miPos);
		}

		bool operator >= (const iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos >= i2.miPos);
		}

	protected:
		size_type	miPos;
		parent*		mpTab;
	};


	////////////////////////////
	// CLASS const_iterator
	////////////////////////////
	class const_iterator
	{
		friend class fgarrvector<T,SHIFT>;

	protected:		
		const_iterator( const size_type& ipos, const fgarrvector<T,SHIFT>* ptab) : miPos(ipos), mpTab(ptab)	{}

	public:
		typedef forward_iterator_tag				iterator_category;
		typedef class fgarrvector<T,SHIFT>			parent;

		typedef typename parent::const_pointer		const_pointer;
		typedef typename parent::const_reference	const_reference;
		typedef typename parent::value_type			value_type;



		const_iterator() : miPos(0), mpTab(NULL)	{}

		const_iterator( const const_iterator& itr) : miPos(itr.miPos), mpTab(itr.mpTab)	{}
		const_iterator( const iterator& itr) : miPos(itr.miPos), mpTab(itr.mpTab)	{}

		
		
		const size_type&	index() const		{ return miPos;}

		bool				is_valid() const	{ return mpTab->is_valid(miPos);}


		const_reference operator*() const
		{
			return (*mpTab)[miPos];
		}

		const_pointer operator->() const
		{
			return (&( (*mpTab)[miPos] ));
		}

		const_iterator& operator ++()	
		{
			do	{
				++miPos;
				if ( miPos >= mpTab->size() )
					break;
			} while ( ! mpTab->is_valid(miPos) );
			return (*this);
		}

		const_iterator operator ++(int)	
		{
			const_iterator itmp(*this);
			do	{
				++miPos;
				if ( miPos >= mpTab->size() )
					break;
			} while ( ! mpTab->is_valid(miPos) );
			return itmp;
		}

		const_iterator& operator --()	
		{
			do	{
				--miPos;
				if ( miPos <= 0 )
					break;
			} while ( ! mpTab->is_valid(miPos) );
			return (*this);
		}

		const_iterator operator --(int)	
		{
			const_iterator itmp(*this);
			do	{
				--miPos;
				if ( miPos <= 0 )
					break;
			} while ( ! mpTab->is_valid(miPos) );
			return itmp;
		}

		bool operator == (const const_iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos == i2.miPos);
		}

		bool operator != (const const_iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos != i2.miPos);
		}

		bool operator < (const const_iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos < i2.miPos);
		}

		bool operator > (const const_iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos > i2.miPos);
		}

		bool operator <= (const const_iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos <= i2.miPos);
		}

		bool operator >= (const const_iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos >= i2.miPos);
		}

	protected:
		size_type		miPos;
		const parent	*mpTab;
	};
	////////////////////////////

public:
	fgarrvector()
	{	
		// inserting 0-element
		mtab.push_back( value_type() );
		mtabit.push_back( false ); 
	}	

	fgarrvector( const fgarrvector& t) : mtab(t.mtab), mtabit(t.mtabit), mtaber(t.mtaber)	{}



	const T&	operator[]( const size_type& i) const	{ return mtab[i];}
	T&			operator[]( const size_type& i)			{ return mtab[i];}

	bool		is_valid( const size_type& i) const		{ return mtabit[i];}

	size_type		size() const						{ return mtab.size(); }
	size_type		size_valid() const					{ return mtab.size() - mtaber.size() - 1; }

	iterator	begin()									{ iterator itr( 0, this); return ++itr; }
	iterator	end()									{ return iterator( mtab.size(), this); }

	const_iterator	begin() const						{ const_iterator itr( 0, this); return ++itr; }
	const_iterator	end() const							{ return const_iterator( mtab.size(), this); }


	size_type	insert( const value_type& t)
	{
		if ( mtaber.size() > 0)
		{
			const size_type& ind = mtaber.back();

			mtab[ind] = t;
			mtabit.set( ind, true);
			mtaber.pop_back();
			return ind;
		}
		else
		{
			//printf( " darray = %d / %d\n", mtab.size(), mtab.capacity() );
			mtab.push_back( t );
			mtabit.push_back( true );
			return mtab.size() - 1;
		}
	}

	void	clear()
	{
		mtab.clear();
		mtabit.clear();
		mtaber.clear();

		mtab.push_back( value_type() );
		mtabit.push_back( false ); 
	}

	void	erase( const size_type& ind)
	{
		if ( mtabit[ ind ] )
		{
			mtabit.set( ind, false );
			mtaber.push_back( ind);
		}
	}

	void	erase( const iterator& itr)
	{
		if ( mtabit[ itr.index() ] )
		{
			mtabit.set( itr.index(), false );
			mtaber.push_back( itr.index() );
		}
	}

	template <class TSIZE>
	void	erase( const vector<TSIZE>& tab)
	{
		mtaber.reserve( mtaber.size() + tab.size() );
		for ( size_type i=0; i<tab.size(); ++i)
			if ( mtabit[ tab[i] ] )
			{
				mtabit.set( static_cast<size_t>( tab[i] ), false );
				mtaber.push_back( static_cast<size_t>( tab[i] ) );
			}
	}


	void resize( const size_type& newsize)
	{
		//mtab.resize( newsize+1 );
		//mtabit.resize( newsize+1 );
		//mtab[0] = value_type();
		//mtabit.set( 0, false);
		//mtaber.clear();

		resize( newsize, value_type() );
	}

	void resize( const size_type& newsize, const value_type& t)
	{
		mtab.resize( newsize+1, t );
		mtabit.resize( newsize+1, true);
		mtab[0] = value_type();
		mtabit.set( 0, false);
		mtaber.clear();
	}

	void	consolidate();

	void	dump( ostream& f=cout) const;

private:
	vector_type			mtab;
	bitset_type			mtabit;
	vector<size_type>	mtaber;
};
//////////////////////////////////////////////////////////////////////



template <class T,size_t SHIFT>
inline void fgarrvector<T,SHIFT>::consolidate()
{
	sort( mtaber.begin(), mtaber.end() );
	mtaber.erase( unique( mtaber.begin(), mtaber.end() ), mtaber.end() );
}


template <class T,size_t SHIFT>
inline void fgarrvector<T,SHIFT>::dump( ostream& f) const
{
	f << "number of holes = " << mtaber.size() << endl;
	
	f << "MTAB:" << endl;
	for ( size_type i=0; i<mtab.size(); ++i)
		f << " " << setw(4) << i << " = " << (int)mtab[i] << " | " << (int)mtabit[i] << endl;

	f << "MSET:" << endl;
	for ( vector<size_type>::const_iterator sitr=mtaber.begin(); sitr!=mtaber.end(); ++sitr)
		f << " " << setw(4) << *sitr << endl;
	f << "END:" << endl;

	f << endl;
	mtab.dump( f);
}

#endif // __FGARRVECTOR_H__

