#ifndef __ARRAYTMPL_H__
#define __ARRAYTMPL_H__



//////////////////////////////////////////////////////////////////////
// class ArrayTmpl
//////////////////////////////////////////////////////////////////////
template <class T, size_t N>
class ArrayTmpl : public ArrayTmpl<T,N-1>
{
public:
	ArrayTmpl( const ArrayTmpl<T,N-1>& base, const T& t) : ArrayTmpl<T,N-1>( base), mVar(t)	{}

	template <size_t ID>
	const T&	cElem() const { return ArrayTmpl<T,ID>::mVar; } 

protected:
	T mVar;
};

template <class T>
class ArrayTmpl<T,1>
{
public:
	ArrayTmpl( const T& t) : mVar(t)	{}

	template <size_t ID>
	const T&	cElem() const { return ArrayTmpl<T,ID>::mVar; } 

protected:
	T mVar;
};



/*
//////////////////////////////////////////////////////////////////////
// EXAMPLE

	int k = 88;
	ArrayTmpl<int&,1>	carr( k);

	cout << carr.cElem<1>() << endl;
	k = 77;
	cout << carr.cElem<1>() << endl;

	carr.cElem<1>() = 3;
	cout << k << endl;

	ArrayTmpl<int,4>	arr( ArrayTmpl<int,3>( ArrayTmpl<int,2>( ArrayTmpl<int,1>( 5), 4 ), 3 ), 2);

	cout << "id = 1 -- " << arr.cElem<1>() << endl;
	cout << "id = 2 -- " << arr.cElem<2>() << endl;
	cout << "id = 3 -- " << arr.cElem<3>() << endl;
	cout << "id = 4 -- " << arr.cElem<4>() << endl;

// MATRIX
	typedef ArrayTmpl<int,3> TRow;

	ArrayTmpl< TRow, 1>	row( TRow( ArrayTmpl<int,2>( ArrayTmpl<int,1>(  5),  4 ),  3 ) );

	ArrayTmpl< TRow, 3> mtx( ArrayTmpl< TRow, 2>( ArrayTmpl< TRow, 1>(
						TRow( ArrayTmpl<int,2>( ArrayTmpl<int,1>(  5),  4 ),  3 )), 
						TRow( ArrayTmpl<int,2>( ArrayTmpl<int,1>( 15), 14 ), 13 )), 
						TRow( ArrayTmpl<int,2>( ArrayTmpl<int,1>( 25), 24 ), 23 ));

	cout << setw(3) << mtx.cElem<1>().cElem<1>() << " " << setw(3) << mtx.cElem<1>().cElem<2>() << " " << setw(3) << mtx.cElem<1>().cElem<3>() << endl;
	cout << setw(3) << mtx.cElem<2>().cElem<1>() << " " << setw(3) << mtx.cElem<2>().cElem<2>() << " " << setw(3) << mtx.cElem<2>().cElem<3>() << endl;
	cout << setw(3) << mtx.cElem<3>().cElem<1>() << " " << setw(3) << mtx.cElem<3>().cElem<2>() << " " << setw(3) << mtx.cElem<3>().cElem<3>() << endl;

	cout << sizeof( ArrayTmpl< TRow, 3> ) << " " << 3*3*sizeof(int) << endl;
*/


#endif // __ARRAYTMPL_H__
