#ifndef __TRIPLE_H__
#define __TRIPLE_H__


template <class T1, class T2, class T3>
struct triple
{
	triple( const T1& t1, const T2& t2, const T3& t3 ) 
			: first(t1), second(t2), third(t3)	{}
	
	T1	first;
	T2	second;
	T3	third;
};


template <class T1, class T2, class T3>
inline bool operator == ( const triple<T1,T2,T3>& c1, const triple<T1,T2,T3>& c2 )
{
	return ( c1.first == c2.first && c1.second == c2.second && c1.third == c2.third );
}


template <class T1, class T2, class T3>
inline bool operator < ( const triple<T1,T2,T3>& c1, const triple<T1,T2,T3>& c2 )
{
	if ( c1.first == c2.first )
	{
		if ( c1.second == c2.second )
		{
			return ( c1.third < c2.third );
		}

		return ( c1.second < c2.second );
	}

	return ( c1.first < c2.first );
}


#endif // __TRIPLE_H__
