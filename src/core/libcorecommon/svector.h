#ifndef __SVECTOR_H__
#define __SVECTOR_H__




#ifdef _MATRIX_SIZE_CHECKING

	#define CHECK(f) \
	{ \
		if (! (f) ) \
			THROW_INTERNAL( "SMatrix check error: '" #f "'"); \
	}

#else // _MATRIX_SIZE_CHECKING

	#define CHECK(f) \
	{ \
	}

#endif // _MATRIX_SIZE_CHECKING


#define SMTX_TINY 1.0e-20 

#define SMTX_MAX_ROTATIONS 30



//////////////////////////////////////////////////////////////////////
// class SVector
//////////////////////////////////////////////////////////////////////
template< MGSize MAX_SIZE, class ELEM_TYPE=MGFloat>
class SVector
{
public:
	enum { SIZE = MAX_SIZE};

	SVector();
	explicit SVector( const MGSize& nrows);

	SVector( const MGSize& nrows, const ELEM_TYPE& d);
	SVector( const SVector< MAX_SIZE, ELEM_TYPE>& v);

	template<class VECTOR> explicit SVector( const VECTOR& v);


    ELEM_TYPE&			operator()(const MGSize& i)			{ return *(mtab+i); }
    const ELEM_TYPE&	operator()(const MGSize& i) const	{ return *(mtab+i); }

    ELEM_TYPE&			operator[](const MGSize& i)			{ return *(mtab+i); }
    const ELEM_TYPE&	operator[](const MGSize& i) const	{ return *(mtab+i); }

    void	Init( const ELEM_TYPE& x);
    void	Resize( const MGSize& nrows);

	SVector< MAX_SIZE, ELEM_TYPE>&	operator = ( const SVector< MAX_SIZE, ELEM_TYPE>& a);
	SVector< MAX_SIZE, ELEM_TYPE>&	operator = ( const ELEM_TYPE& a);

	static MGSize	MaxSize()		{ return MAX_SIZE;}
    const MGSize&	Size() const	{ return mNRows;}

	const MGSize&	NRows() const	{ return mNRows;}

	MGSize&			rNRows() 		{ return mNRows;}



	SVector< MAX_SIZE, ELEM_TYPE>&	operator*=( const SVector< MAX_SIZE, ELEM_TYPE>& v);
	SVector< MAX_SIZE, ELEM_TYPE>&	operator-=( const SVector< MAX_SIZE, ELEM_TYPE>& v);
	SVector< MAX_SIZE, ELEM_TYPE>&	operator+=( const SVector< MAX_SIZE, ELEM_TYPE>& v);
	SVector< MAX_SIZE, ELEM_TYPE>&	operator/=( const SVector< MAX_SIZE, ELEM_TYPE>& v);
	
	SVector< MAX_SIZE, ELEM_TYPE>&	operator*=( const MGFloat& d);
	SVector< MAX_SIZE, ELEM_TYPE>&	operator-=( const MGFloat& d);
	SVector< MAX_SIZE, ELEM_TYPE>&	operator+=( const MGFloat& d);
	SVector< MAX_SIZE, ELEM_TYPE>&	operator/=( const MGFloat& d);


	void	Write( ostream& f=cout) const;

        ELEM_TYPE* GetCoreVec() { return mtab; };

protected:
	MGSize	mNRows;
    ELEM_TYPE mtab[MAX_SIZE>0?MAX_SIZE:1];
};
//////////////////////////////////////////////////////////////////////

template< MGSize MAX_SIZE, class ELEM_TYPE>
SVector< MAX_SIZE, ELEM_TYPE>::SVector() : mNRows(MAX_SIZE)
{
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
SVector< MAX_SIZE, ELEM_TYPE>::SVector( const MGSize& nrows)
{
	CHECK( nrows <= MAX_SIZE);
	mNRows = nrows;

	memset( mtab, 0, MAX_SIZE*sizeof(ELEM_TYPE) );
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
SVector< MAX_SIZE, ELEM_TYPE>::SVector( const MGSize& nrows, const ELEM_TYPE& d)
{
	CHECK( nrows <= MAX_SIZE);
	mNRows = nrows;
	for ( MGSize i=0; i<MAX_SIZE; i++)
		mtab[i] = d;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
SVector< MAX_SIZE, ELEM_TYPE>::SVector( const SVector< MAX_SIZE, ELEM_TYPE>& v)
{
	CHECK( v.NRows() <= MAX_SIZE);
	mNRows = v.NRows();
	for ( MGSize i=0; i<mNRows; i++)
		(*this)(i) = v(i);
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
template< class VECTOR>
SVector< MAX_SIZE, ELEM_TYPE>::SVector( const VECTOR& v)
{
	CHECK( v.Size() <= MAX_SIZE);
	mNRows = v.Size();
	for ( MGSize i=0; i<mNRows; i++)
		(*this)(i) = v(i);
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
void SVector< MAX_SIZE, ELEM_TYPE>::Init( const ELEM_TYPE& x)
{ 
    for ( MGSize i=0; i < mNRows; i++)
        (*this)(i) = x;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
void SVector< MAX_SIZE, ELEM_TYPE>::Resize( const MGSize& nrows)
{ 
	CHECK( nrows <= MAX_SIZE);
	mNRows = nrows;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE>& SVector< MAX_SIZE, ELEM_TYPE>::operator = ( const SVector< MAX_SIZE, ELEM_TYPE>& a)
{
	mNRows = a.mNRows;

    for ( MGSize i=0; i < mNRows; ++i)
        (*this)(i) = a(i);

	return *this;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE>& SVector< MAX_SIZE, ELEM_TYPE>::operator = ( const ELEM_TYPE& a)
{
	mNRows = MAX_SIZE;

    for ( MGSize i=0; i < mNRows; ++i)
        (*this)(i) = a;

	return *this;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SVector< MAX_SIZE, ELEM_TYPE>::Write( ostream& f) const
{
	f <<"+---------------+\n";
	for( MGSize i=0; i<mNRows; i++)
	{
		f << "| ";
		f << setw(12) << setprecision(6) <<(*this)(i) << " ";
		f << " |\n";
	}
	f << "+---------------+\n";
}






template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE>& SVector< MAX_SIZE, ELEM_TYPE>::operator*=( const SVector< MAX_SIZE, ELEM_TYPE>& v)
{
	CHECK( NRows() == v.NRows() );
	for( MGSize i=0; i<mNRows; i++)
		(*this)(i) *= v(i);
	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE>& SVector< MAX_SIZE, ELEM_TYPE>::operator-=( const SVector< MAX_SIZE, ELEM_TYPE>& v)
{
	CHECK( NRows() == v.NRows() );
	for( MGSize i=0; i<mNRows; i++)
		(*this)(i) -= v(i);
	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE>& SVector< MAX_SIZE, ELEM_TYPE>::operator+=( const SVector< MAX_SIZE, ELEM_TYPE>& v)
{
	CHECK( NRows() == v.NRows() );
	for( MGSize i=0; i<mNRows; i++)
		(*this)(i) += v(i);
	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE>& SVector< MAX_SIZE, ELEM_TYPE>::operator/=( const SVector< MAX_SIZE, ELEM_TYPE>& v)
{
	CHECK( NRows() == v.NRows() );
	for( MGSize i=0; i<mNRows; i++)
		(*this)(i) /= v(i);
	return *this;
}

	
template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE>& SVector< MAX_SIZE, ELEM_TYPE>::operator*=( const MGFloat& d)
{
	for( MGSize i=0; i<mNRows; i++)
		(*this)(i) *= d;
	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE>& SVector< MAX_SIZE, ELEM_TYPE>::operator-=( const MGFloat& d)
{
	for( MGSize i=0; i<mNRows; i++)
		(*this)(i) -= d;
	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE>& SVector< MAX_SIZE, ELEM_TYPE>::operator+=( const MGFloat& d)
{
	for( MGSize i=0; i<mNRows; i++)
		(*this)(i) += d;
	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE>& SVector< MAX_SIZE, ELEM_TYPE>::operator/=( const MGFloat& d)
{
	for( MGSize i=0; i<mNRows; i++)
		(*this)(i) /= d;
	return *this;
}




template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE> operator +( const SVector< MAX_SIZE, ELEM_TYPE>& v1, const SVector< MAX_SIZE, ELEM_TYPE>& v2)
{
	CHECK( v1.NRows() == v2.NRows() );
	SVector< MAX_SIZE, ELEM_TYPE> vtemp(v1.NRows());
	for( MGSize i=0; i<v1.NRows(); i++)
		vtemp(i) = v1(i) + v2(i);
	return vtemp;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE> operator -( const SVector< MAX_SIZE, ELEM_TYPE>& v1, const SVector< MAX_SIZE, ELEM_TYPE>& v2)
{
	CHECK( v1.NRows() == v2.NRows() );
	SVector< MAX_SIZE, ELEM_TYPE> vtemp(v1.NRows());
	for( MGSize i=0; i<v1.NRows(); i++)
		vtemp(i) = v1(i) - v2(i);
	return vtemp;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE> operator *( const SVector< MAX_SIZE, ELEM_TYPE>& v1, const SVector< MAX_SIZE, ELEM_TYPE>& v2)
{
	CHECK( v1.NRows() == v2.NRows() );
	SVector< MAX_SIZE, ELEM_TYPE> vtemp(v1.NRows());
	for( MGSize i=0; i<v1.NRows(); i++)
		vtemp(i) = v1(i) * v2(i);
	return vtemp;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE> operator /( const SVector< MAX_SIZE, ELEM_TYPE>& v1, const SVector< MAX_SIZE, ELEM_TYPE>& v2)
{
	CHECK( v1.NRows() == v2.NRows() );
	SVector< MAX_SIZE, ELEM_TYPE> vtemp(v1.NRows());
	for( MGSize i=0; i<v1.NRows(); i++)
		vtemp(i) = v1(i) / v2(i);
	return vtemp;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE> operator *( const SVector< MAX_SIZE, ELEM_TYPE>& v, const MGFloat& d)
{
	SVector< MAX_SIZE, ELEM_TYPE> vtemp(v.NRows());
	for( MGSize i=0; i<v.NRows(); i++)
		vtemp(i) = v(i) * d;
	return vtemp;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE> operator *( const MGFloat& d, const SVector< MAX_SIZE, ELEM_TYPE>& v)
{
	SVector< MAX_SIZE, ELEM_TYPE> vtemp(v.NRows());
	for( MGSize i=0; i<v.NRows(); i++)
		vtemp(i) = v(i) * d;
	return vtemp;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE> operator /( const SVector< MAX_SIZE, ELEM_TYPE>& v, const MGFloat& d)
{
	SVector< MAX_SIZE, ELEM_TYPE> vtemp(v.NRows());
	for( MGSize i=0; i<v.NRows(); i++)
		vtemp(i) = v(i) / d;
	return vtemp;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE> operator /( const MGFloat& d, const SVector< MAX_SIZE, ELEM_TYPE>& v )
{
	SVector< MAX_SIZE, ELEM_TYPE> vtemp(v.NRows());
	for( MGSize i=0; i<v.NRows(); i++)
		vtemp(i) = d / v(i);
	return vtemp;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline ELEM_TYPE Dot( const SVector< MAX_SIZE, ELEM_TYPE>& v1, const SVector< MAX_SIZE, ELEM_TYPE>& v2)
{
	CHECK( v1.NRows() == v2.NRows() );
	ELEM_TYPE	d=0.0;
	for ( MGSize i=0; i<v1.NRows(); ++i )
		d += v1(i)*v2(i);

	return d;
}


#endif // __SVECTOR_H__
