#include "interpol.h"


//////////////////////////////////////////////////////////////////////
// class InterpBase
//////////////////////////////////////////////////////////////////////
InterpBase::InterpBase()
{
	mbDir = true;
	mbDestr = true;
	mNop = 0;
	mXl = mXr = 0;
	mptabX = mptabY = NULL;
}


void InterpBase::Destroy()
{
	if ( mbDestr)
	{
		if ( mptabX ) delete mptabX;
		if ( mptabY ) delete mptabY;
	}

	mptabX = mptabY = NULL;
	mXl = mXr = 0;
	mNop = 0;
}


void InterpBase::Init( const MGInt& n)
{
	Destroy();
	
	mNop = n;
}


void InterpBase::CopyPoints( const MGInt& nn, MGFloatArr* ptabx, MGFloatArr* ptaby)
{
	MGInt i;

	ASSERT( ptabx && ptaby && nn>1);
	Init( nn);

	mptabX  = new MGFloatArr( mNop);
	mptabY  = new MGFloatArr( mNop);

	for ( i=0; i<mNop; i++)
	{
		(*mptabX)[i] = (*ptabx)[i];
		(*mptabY)[i] = (*ptaby)[i];
	}

	mXl = (*mptabX)[0];
	mXr = (*mptabX)[mNop-1];
	mbDestr = true;
	if ( mXr > mXl)
		mbDir = true;
	else
		mbDir = false;
}


void InterpBase::UsePoints( const MGInt& nn, MGFloatArr* ptabx, MGFloatArr* ptaby)
{
	ASSERT( ptabx && ptaby && nn>1);
	Init( nn);

	mptabX  = ptabx;
	mptabY  = ptaby;

	mXl = (*mptabX)[0];
	mXr = (*mptabX)[mNop-1];
	mbDestr = false;
	if ( mXr > mXl)
		mbDir = true;
	else
		mbDir = false;
}



void InterpBase::ExportTEC( char *fname)
{
	MGInt	ind;
	FILE	*f = fopen( fname, "wt");

	TRACE( "InterpBase::ExportTEC");
	fprintf( f, "VARIABLES = \"X\", \"Y\"" );
	fprintf( f, "ZONE I=%d, F=POINT\n", mNop );
	
	for ( ind = 0; ind < mNop; ind++)
	{
		fprintf( f, "%.12lf %.12lf \n", (*mptabX)[ind], (*mptabY)[ind] );
	}

	fprintf( f, "\n");

	fclose( f);
}




//////////////////////////////////////////////////////////////////////
// class Spline
//////////////////////////////////////////////////////////////////////
void Spline::Destroy()
{
	mtabA1.erase( mtabA1.begin(), mtabA1.end() );
	mtabA2.erase( mtabA2.begin(), mtabA2.end() );
	mtabA3.erase( mtabA3.begin(), mtabA3.end() );
	mtabA4.erase( mtabA4.begin(), mtabA4.end() );

	InterpBase::Destroy();
}


void Spline::Init( const MGInt& n)
{
	InterpBase::Init( n);

	mtabA1.reserve( n); mtabA1.insert( mtabA1.end(), n, 0);
	mtabA2.reserve( n); mtabA2.insert( mtabA2.end(), n, 0);
	mtabA3.reserve( n); mtabA3.insert( mtabA3.end(), n, 0);
	mtabA4.reserve( n); mtabA4.insert( mtabA4.end(), n, 0);
}




/*
TFunction* Spline::CreateCopy()
{
	MGInt i;
	Spline *vfun = new Spline;

	vfun->CopyPoints( nop, x, y);
	for ( i=0; i<nop; i++)
	{
		vfun->a1[i] = a1[i];
		vfun->a2[i] = a2[i];
		vfun->a3[i] = a3[i];
		vfun->a4[i] = a4[i];
	}
	return vfun;
}

*/





void Spline::CalcCoeff()
{
	MGInt		i;
	MGFloat		hj, hjm;
	MGFloat		xtmp, h12, hn12;
	MGFloat		cj, cjp;
	MGFloatArr	tab;


	ASSERT( mNop > 1);
	
	if ( mNop == 2)
	{
		for ( i=0; i<mNop; i++)
		{
//			char sbuf[1024];
//			sprintf( sbuf, "x: %lg %lg", (*mptabX)[0], (*mptabX)[1] );
//			printf( "%s\n", sbuf);
//			sprintf( sbuf, "y: %lg %lg", (*mptabY)[0], (*mptabY)[1] );
//			printf( "%s\n", sbuf);

			mtabA3[i] = mtabA4[i] = 0;
			mtabA1[i] = ((*mptabY)[1]*(*mptabX)[0] - (*mptabY)[0]*(*mptabX)[1]) / ((*mptabX)[0] - (*mptabX)[1]);
			mtabA2[i] = ((*mptabY)[1] - (*mptabY)[0]) / ((*mptabX)[1] - (*mptabX)[0]);
		}
	
	}
	else
	{

		tab.reserve( mNop);
		tab.insert( tab.end(), mNop, 0);

		for ( i=0; i<mNop; i++)
			mtabA1[i] = mtabA2[i] = mtabA3[i] = mtabA4[i] = 0;

		for ( i=0; i<mNop-1; i++)
			tab[i] = (*mptabX)[i+1] - (*mptabX)[i];

		for ( i=2; i<mNop-2; i++)
		{
			hj  = tab[i];
			hjm = tab[i-1];
			mtabA1[i] = 2.0*( hj + hjm);
			mtabA2[i] = hj;
			mtabA3[i] = hjm;
			mtabA4[i] = 3.0*(( (*mptabY)[i+1] - (*mptabY)[i])/hj - ( (*mptabY)[i] - (*mptabY)[i-1])/hjm );
		}

		i = 1;
		hj  = tab[i];
		hjm = tab[i-1];
		mtabA1[i] = 3.0*hjm + 2.0*hj + hjm*hjm/hj;
		mtabA2[i] = hj - hjm*hjm/hj;
		mtabA4[i] = 3.0*(( (*mptabY)[i+1] - (*mptabY)[i])/hj - ( (*mptabY)[i] - (*mptabY)[i-1])/hjm );

		i = mNop - 2;
		hj  = tab[i];
		hjm = tab[i-1];
		mtabA1[i] = 3.0*hj + 2.0*hjm + hj*hj/hjm;
		mtabA3[i] = hjm - hj*hj/hjm;
		mtabA4[i] = 3.0*(( (*mptabY)[i+1] - (*mptabY)[i])/hj - ( (*mptabY)[i] - (*mptabY)[i-1])/hjm );

		for ( i=2; i<mNop-1; i++)
		{
			xtmp = mtabA3[i]/mtabA1[i-1];
			mtabA1[i] = mtabA1[i] - mtabA2[i-1]*xtmp;
			mtabA4[i] = mtabA4[i] - mtabA4[i-1]*xtmp;
		}

		mtabA3[mNop-2] = mtabA4[mNop-2]/mtabA1[mNop-2];
		for ( i=mNop-3; i>=1; i--)
		{
			mtabA3[i] = ( mtabA4[i] - mtabA2[i]*mtabA3[i+1])/mtabA1[i];
		}

		h12 = tab[0]/tab[1];
		mtabA3[0] = mtabA3[1]*( 1.0 + h12) - mtabA3[2]*h12;
		hn12 = tab[mNop-2]/tab[mNop-3];
		mtabA3[mNop-1] = mtabA3[mNop-2]*( 1.0 + hn12) - mtabA3[mNop-3]*hn12;

		for ( i=0; i<mNop-1; i++)
		{
			mtabA1[i] = (*mptabY)[i];
			cj  = mtabA3[i];
			cjp = mtabA3[i+1];
			hj = tab[i];
			mtabA4[i] = ( cjp - cj)/hj/3.0;
			mtabA2[i] = ( (*mptabY)[i+1] - (*mptabY)[i])/hj - hj*( cj*2.0 + cjp)/3.0;
		}

	}
}





void Spline::Diff( const MGFloat& xx, MGFloat& yy, MGFloat& d1y, MGFloat& d2y) const
{
	MGInt i = 0;
	MGFloat t;

	do
	{
		i++;
		if ( i >= mNop )
			break;
	}
	while ( (( (*mptabX)[i] < xx && mbDir) || ( (*mptabX)[i] > xx && !mbDir)) );

	if ( i >= mNop)
		i -= 2;
	else
		i--;

	t = xx - (*mptabX)[i];

	yy  = ((mtabA4[i]*t + mtabA3[i])*t + mtabA2[i])*t + mtabA1[i];
	d1y = (3.0*mtabA4[i]*t + 2.0*mtabA3[i])*t + mtabA2[i];
	d2y = 6.0*mtabA4[i]*t + 2.0*mtabA3[i];
}




//////////////////////////////////////////////////////////////////////
// class PolyLine
//////////////////////////////////////////////////////////////////////
void PolyLine::Diff( const MGFloat& xx, MGFloat& yy, MGFloat& d1y, MGFloat& d2y) const
{
	MGInt i = 0;

	do
	{
		i++;
		if ( i >= mNop )
			break;
	}
	while ( (( (*mptabX)[i] < xx && mbDir) || ( (*mptabX)[i] > xx && !mbDir)) );

	if ( i >= mNop)
		i -= 2;
	else
		i--;

	d2y = 0.0;
	d1y = ( (*mptabY)[i+1] - (*mptabY)[i] )/( (*mptabX)[i+1] - (*mptabX)[i] );
	yy = d1y*(xx - (*mptabX)[i]) + (*mptabY)[i];
}
