#ifndef __FACTORY_H__
#define __FACTORY_H__


#include "libcoresystem/mgdecl.h"
#include "singleton.h"


template <class PARAM>
class FactoryBase
{
public:
	virtual MGString	Name() const	= 0;
	virtual void		CreatorsList( vector<PARAM>& tabparam) const	= 0;
};

template <class PARAM>
class FactoryList
{
public:
	void	RegisterFactory( const FactoryBase<PARAM>* pfactory)
	{
		mtabFactory.push_back( pfactory);
	}

	void	PrintInfo( ostream& os = cout) const
	{
		for ( MGSize i=0; i<mtabFactory.size(); ++i)
		{
			vector<PARAM> tabcreator;

			mtabFactory[i]->CreatorsList( tabcreator );

			os << "BASE CLASE : " << mtabFactory[i]->Name();
			os << endl;

			for ( MGSize j=0; j< tabcreator.size(); ++j)
				os << "  " << j << " -- \"" << tabcreator[j] << "\"" << endl;
			os << endl;
		}

		os << endl;
	}

private:
	vector<const FactoryBase<PARAM>*>	mtabFactory;
};



//////////////////////////////////////////////////////////////////////
//	class Factory
//////////////////////////////////////////////////////////////////////
template <class PARAM, class CREATOR>
class Factory : FactoryBase<PARAM>
{
	friend class Singleton< Factory<PARAM,CREATOR> >;

public:
	void		Register( const PARAM& id, CREATOR *ptr);
	void		UnRegister( const PARAM& id, CREATOR *ptr);

	virtual MGString	Name() const;
	virtual void		CreatorsList( vector<PARAM>& tabparam) const;


	bool		Exists( const PARAM& id ) const;
	CREATOR*	GetCreator( const PARAM& id ) const;

protected:
	Factory()	
	{
		Singleton< FactoryList<PARAM> >::GetInstance()->RegisterFactory( this );
	}

private:
	map<PARAM, CREATOR* >	mmapCreators;
};



template <class PARAM, class CREATOR>
void Factory<PARAM,CREATOR>::Register( const PARAM& id, CREATOR *ptr)
{
	if ( mmapCreators.find(id) != mmapCreators.end() )
	{
		ostringstream os;
		os << "registration failed - creator with id = '" << id << "' already exists" <<  endl;
		TRACE( os.str() );
		THROW_INTERNAL( os.str() );
	}

	mmapCreators.insert( typename map<PARAM, CREATOR* >::value_type( id, ptr ) );
}


template <class PARAM, class CREATOR>
void Factory<PARAM,CREATOR>::UnRegister( const PARAM& id, CREATOR *ptr)
{
	typename map<PARAM, CREATOR* >::iterator itr;

	if ( (itr = mmapCreators.find(id) ) == mmapCreators.end() )
	{
		ostringstream os;
		os << "unregistration failed - creator with id = '" << id << "' does not exist" <<  endl;
		TRACE( os.str() );
		THROW_INTERNAL( os.str() );
	}

	if ( itr->second != ptr)
	{
		ostringstream os;
		os << "unregistration failed - creator with id = '" << id << "' points to different address" <<  endl;
		TRACE( os.str() );
		THROW_INTERNAL( os.str() );
	}

	mmapCreators.erase( itr);
}


template <class PARAM, class CREATOR>
MGString Factory<PARAM,CREATOR>::Name() const
{
	return CREATOR::BaseClassName();
}


template <class PARAM, class CREATOR>
void Factory<PARAM,CREATOR>::CreatorsList( vector<PARAM>& tabparam) const
{
	tabparam.clear();

	for ( typename map<PARAM, CREATOR* >::const_iterator citr=mmapCreators.begin(); citr!=mmapCreators.end(); ++citr)
	{
		tabparam.push_back( citr->first);
	}
}



template <class PARAM, class CREATOR>
bool Factory<PARAM,CREATOR>::Exists( const PARAM& id ) const
{
	return ( mmapCreators.find(id) != mmapCreators.end() );
}


template <class PARAM, class CREATOR>
CREATOR* Factory<PARAM,CREATOR>::GetCreator( const PARAM& id ) const
{
	typename map<PARAM, CREATOR* >::const_iterator citr = mmapCreators.find(id);

	if ( citr == mmapCreators.end() )
	{
		ostringstream os;
		os << "creator with id = '" << id << "' not found";
		TRACE( os.str() );
		THROW_INTERNAL( os.str() );

		return 0;
	}

	return citr->second;
}




//////////////////////////////////////////////////////////////////////
//	class Creator
//////////////////////////////////////////////////////////////////////
template <class T>
class Creator
{
public:
	static MGString BaseClassName()	{ return T::Info(); }

	virtual ~Creator()	{}

	virtual T*		Create() const			= 0;
	virtual T*		Create( T& base) const	= 0;
};



//////////////////////////////////////////////////////////////////////
//	class ConcCreator
//////////////////////////////////////////////////////////////////////
template <class PARAM, class T, class TBASE>
class ConcCreator : public Creator<TBASE>
{
public:
	explicit ConcCreator( const PARAM& id)
	{
		Singleton< Factory< PARAM,Creator<TBASE> > >::GetInstance()->Register( mId = id, this);
	}

	virtual ~ConcCreator()
	{
		Singleton< Factory< PARAM,Creator<TBASE> > >::GetInstance()->UnRegister( mId, this);
	}

	TBASE*		Create() const				{ return new T; }
	TBASE*		Create( TBASE& base) const	{ return NULL;}

private:
	PARAM	mId;
};


//////////////////////////////////////////////////////////////////////
//	class ConcCopyCreator
//////////////////////////////////////////////////////////////////////
template <class PARAM, class T, class TBASE>
class ConcCopyCreator : public Creator<TBASE>
{
public:
	explicit ConcCopyCreator( const PARAM& id)
	{
		Singleton< Factory< PARAM,Creator<TBASE> > >::GetInstance()->Register( mId = id, this);
	}

	virtual ~ConcCopyCreator()
	{
		Singleton< Factory< PARAM,Creator<TBASE> > >::GetInstance()->UnRegister( mId, this);
	}

	TBASE*		Create() const				{ return NULL;}
	TBASE*		Create( TBASE& base) const	{ return new T( base); }

private:
	PARAM	mId;
};


#endif // __FACTORY_H__
