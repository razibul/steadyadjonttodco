#ifndef __SMATRIX_H__
#define __SMATRIX_H__

#include "svector.h"


typedef size_t	TSize;

//////////////////////////////////////////////////////////////////////
// class QRDecomp
//////////////////////////////////////////////////////////////////////
template< class T, class MATRIX>
class QRDecomp
{
public:
	typedef TSize	size_type;

	QRDecomp( const MATRIX& mtx);

	void	Execute();
	void	AssembleQ( MATRIX& mtxQ);
	void	AssembleR( MATRIX& mtxR);
	T		GetDeterminant();
	void	Solve( MATRIX& mtxB);

	T		Error();

private:
	MATRIX		mA;
	vector<T>	mtabD;	
	size_type	mNrow;
	size_type	mNcol;
	T			mSign;
};
//////////////////////////////////////////////////////////////////////


template< class T, class MATRIX>
QRDecomp<T,MATRIX>::QRDecomp( const MATRIX& mtx) : mA(mtx), mtabD(mtx.NCols()), mNrow(mtx.NRows()), mNcol(mtx.NCols()), mSign(1)	
{
	if ( mtx.NRows() < mtx.NCols() )
		THROW_INTERNAL( "QRDecomp : QRDecomp : n_rows must be greater or equal then n_cols");
}


template< class T, class MATRIX>
void QRDecomp<T,MATRIX>::Execute()
{
	for ( size_type k=0; k<mNcol; ++k) 
	{
		// Compute 2-norm of k-th column without under/overflow.
		T nrm = 0;
		for ( size_type i=k; i<mNrow; ++i) 
			nrm += mA(i,k)*mA(i,k);
		nrm = sqrt( nrm);

		if ( nrm != T(0) ) 
		{
			// Form k-th Householder vector.
			if ( mA(k,k) < T(0) ) 
				nrm = -nrm;

			for ( size_type i=k; i<mNrow; ++i) 
				mA(i,k) /= nrm;

			mA(k,k) += T(1);

			// Apply transformation to remaining columns.
			for ( size_type j=k+1; j<mNcol; ++j) 
			{
				T s = 0.0; 
				for ( size_type i=k; i<mNrow; ++i) 
					s += mA(i,k)*mA(i,j);

				s = -s/mA(k,k);
				for ( size_type i=k; i<mNrow; ++i) 
					mA(i,j) += s*mA(i,k);

			}
		}

		mtabD[k] = -nrm;
	}

	if ( mtabD.size() % 2 != 0 )
		mSign = -1;
	else
		mSign = 1;
}


template< class T, class MATRIX>
void QRDecomp<T,MATRIX>::AssembleQ( MATRIX& mtxQ)
{
	mtxQ.Init( 0);
	mtxQ.Resize( mNrow, mNcol);

	for ( size_type k=0; k<mNcol; ++k) 
		mtxQ(k,k) = T(1);

	for ( size_type l=mNcol; l>0; --l) 
	{
		size_type k = l-1;

		for ( size_type j=k; j<mNcol; ++j) 
		{
			if ( mA(k,k) != 0) 
			{
				T s = T(0);
				for ( size_type i=k; i<mNrow; ++i) 
					s += mA(i,k) * mtxQ(i,j);

				s = s / mA(k,k); // should be commented for 2nd version
				for ( size_type i=k; i<mNrow; ++i) 
					mtxQ(i,j) -= s * mA(i,k);
			}
		}
	}

}

template< class T, class MATRIX>
void QRDecomp<T,MATRIX>::AssembleR( MATRIX& mtxR)
{
	mtxR.Init( 0);
	mtxR.Resize( mNcol, mNcol);

	for ( size_type j=0; j<mNcol; ++j)
	{
		mtxR(j,j) = mtabD[j];
		for ( size_type k=j+1; k<mNcol; ++k)
			mtxR(j,k) = mA(j,k);
	}
}



template< class T, class MATRIX>
void QRDecomp<T,MATRIX>::Solve( MATRIX& mtxB)
{
	size_type nbcol = mtxB.NCols(); 

	// Compute Y = transpose(Q)*B
	for ( size_type k=0; k<mNcol; ++k) 
	{
		for ( size_type j=0; j<nbcol; ++j) 
		{
			T s = T(0); 
			for ( size_type i=k; i<mNrow; ++i) 
				s += mA(i,k) * mtxB(i,j);

			s = s / mA(k,k);
			for ( size_type i=k; i<mNrow; ++i) 
				mtxB(i,j) -= s * mA(i,k);
		}
	}

	// Solve R*X = Y;
	for ( size_type l=mNcol; l>0; --l) 
	{
		size_type k = l-1;

		for ( size_type j=0; j<nbcol; ++j) 
			mtxB(k,j) /= mtabD[k];

		for ( size_type i=0; i<k; ++i) 
			for (size_type j=0; j<nbcol; ++j) 
				mtxB(i,j) -= mtxB(k,j) * mA(i,k);
	}

}


template< class T, class MATRIX>
T QRDecomp<T,MATRIX>::GetDeterminant()
{
	T d = mtabD[0] * mSign;

	for ( size_type j=1; j<mtabD.size(); ++j)
		d *= mtabD[j];

	return d;
}


template< class T, class MATRIX>
T QRDecomp<T,MATRIX>::Error()
{
	T tmax = abs( mtabD[0] );
	T tmin = abs( mtabD[0] );

	for ( size_type j=1; j<mtabD.size(); ++j)
	{
		if ( abs(mtabD[j]) > tmax) tmax = abs(mtabD[j]);
		if ( abs(mtabD[j]) < tmin) tmin = abs(mtabD[j]);
	}

	return tmax / tmin;


	//T ctmp, c;

	//ctmp = c = T(0);//T(1) / mtabD[0];

	//for ( size_type j=1; j<mNcol; ++j)
	//{
	//	T sum = T(0);
	//	for ( size_type k=0; k<j-1; ++k)
	//		sum += mA(k,j)*mA(k,j);

	//	ctmp = sqrt(sum) / mtabD[j];

	//	if ( ctmp > c)
	//		c = ctmp;
	//}

	//for ( size_type j=0; j<mtabD.size(); ++j)
	//	cout << " ------ " << mtabD[j] << endl;

	//return c;
}






//////////////////////////////////////////////////////////////////////
// class SMatrix
//////////////////////////////////////////////////////////////////////
template< MGSize MAX_SIZE, class ELEM_TYPE=MGFloat>
class SMatrix
{
public:
	SMatrix();
	SMatrix( const MGSize& nrows, const MGSize& ncols);
	SMatrix( const MGSize& nrows, const MGSize& ncols, const ELEM_TYPE& d);
	SMatrix( const SMatrix< MAX_SIZE, ELEM_TYPE>& a);
	SMatrix( const SVector< MAX_SIZE, ELEM_TYPE>& v);

    ELEM_TYPE&			operator()(const MGSize& i, const MGSize& j)		{ return mtab[i*MAX_SIZE + j]; }
    const ELEM_TYPE&	operator()(const MGSize& i, const MGSize& j) const	{ return mtab[i*MAX_SIZE + j]; }

    ELEM_TYPE&			operator[](const MGSize& i)							{ return mtab[i]; }
    const ELEM_TYPE&	operator[](const MGSize& i) const					{ return mtab[i]; }

    void	Init( const ELEM_TYPE& d);
	void	InitDelta( const MGSize& n, const ELEM_TYPE& d=1.0);
	void	InitDelta( const SVector< MAX_SIZE, ELEM_TYPE>& v);
    void	Resize( const MGSize& nrows, const MGSize& ncols);
	void	Resize( const MGSize& nrows, const MGSize& ncols, const ELEM_TYPE& d)	{ Resize(nrows, ncols); Init(d);}

	SMatrix< MAX_SIZE, ELEM_TYPE>&	operator = ( const SMatrix< MAX_SIZE, ELEM_TYPE>& a);
	SMatrix< MAX_SIZE, ELEM_TYPE>&	operator = ( const SVector< MAX_SIZE, ELEM_TYPE>& v);

	SMatrix< MAX_SIZE, ELEM_TYPE>&	operator+=( const SMatrix< MAX_SIZE, ELEM_TYPE>& a);
	SMatrix< MAX_SIZE, ELEM_TYPE>&	operator-=( const SMatrix< MAX_SIZE, ELEM_TYPE>& a);

	SMatrix< MAX_SIZE, ELEM_TYPE>&	operator+=( const ELEM_TYPE& d);
	SMatrix< MAX_SIZE, ELEM_TYPE>&	operator-=( const ELEM_TYPE& d);
	SMatrix< MAX_SIZE, ELEM_TYPE>&	operator*=( const ELEM_TYPE& d);
	SMatrix< MAX_SIZE, ELEM_TYPE>&	operator/=( const ELEM_TYPE& d);

	static MGSize	MaxSize()		{ return MAX_SIZE;}

	const MGSize&	NRows() const	{ return mNRows;}
	const MGSize&	NCols() const	{ return mNCols;}

	MGSize&			rNRows() 	{ return mNRows;}
	MGSize&			rNCols() 	{ return mNCols;}


	template < MGSize N, class T>
	void	InsertSubMtx( const MGSize& irow, const MGSize& icol, const SMatrix<N,T>& mtx);

	void	Transp();
	void	Invert();
	ELEM_TYPE	Determinant() const;

	void	Gauss( SMatrix< MAX_SIZE, ELEM_TYPE>& b);
	void	LUSolve( SMatrix< MAX_SIZE, ELEM_TYPE>& b);

	void	LUDecomp();


	// eigenvalue decomposition based on Jacobi method

	void	Decompose( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxD, SMatrix< MAX_SIZE, ELEM_TYPE>& mtxL, const bool& bsort=false);
	void	Decompose( SVector< MAX_SIZE, ELEM_TYPE>& vecD, SMatrix< MAX_SIZE, ELEM_TYPE>& mtxL, const bool& bsort=false);


	void	DecomposeOld( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxD, SMatrix< MAX_SIZE, ELEM_TYPE>& mtxL, const bool& bsort=false);
	void	DecomposeOld( SVector< MAX_SIZE, ELEM_TYPE>& vecD, SMatrix< MAX_SIZE, ELEM_TYPE>& mtxL, const bool& bsort=false);

	void	DecomposeNew( SVector< MAX_SIZE, ELEM_TYPE>& vecD, SMatrix< MAX_SIZE, ELEM_TYPE>& mtxL, const bool& bsort=false);


	// decompose symetric matrix to N : mtx = N^T * N
	void	NDecompose( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxN);

	// mtxD must be a diagonal matrix
	void	Assemble( const SMatrix< MAX_SIZE, ELEM_TYPE>& mtxL, 
					  const SMatrix< MAX_SIZE, ELEM_TYPE>& mtxD, 
					  const SMatrix< MAX_SIZE, ELEM_TYPE>& mtxLI );

	void	Assemble( const SMatrix< MAX_SIZE, ELEM_TYPE>& mtxL, 
					  const SVector< MAX_SIZE, ELEM_TYPE>& vecD, 
					  const SMatrix< MAX_SIZE, ELEM_TYPE>& mtxLI);


	void	Write( ostream& f=cout) const;

//protected:
	ELEM_TYPE	Determinant2x2() const;
	ELEM_TYPE	Determinant3x3() const;
	ELEM_TYPE	Determinant4x4() const;

	void	Invert2x2();
	void	Invert3x3();
	void	Invert4x4();
	void	Invert5x5();
	void	Invert6x6();
	void	Invert7x7();

	void	LUDecomp( vector<MGSize>& indx);
	void	BSubSolve( vector<MGSize>& indx, SMatrix< MAX_SIZE, ELEM_TYPE>& b);

	void	RotateMatrix( SMatrix< MAX_SIZE, ELEM_TYPE>& a, 
						  ELEM_TYPE& g, ELEM_TYPE& h, ELEM_TYPE& s, ELEM_TYPE& c, ELEM_TYPE& tau, 
						  const MGInt& i, const MGInt& j, const MGInt& k, const MGInt& l);

	void	RotateMatrixNew( SMatrix< MAX_SIZE, ELEM_TYPE>& a, 
						  ELEM_TYPE& g, ELEM_TYPE& h, ELEM_TYPE& s, ELEM_TYPE& c, ELEM_TYPE& tau, 
						  const MGSize& i, const MGSize& j, const MGSize& k, const MGSize& l);

        ELEM_TYPE* GetCoreVec() { return mtab; };
private:
	MGSize		mNRows;
	MGSize		mNCols;
    ELEM_TYPE	mtab[MAX_SIZE * MAX_SIZE];
};
//////////////////////////////////////////////////////////////////////



template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix< MAX_SIZE, ELEM_TYPE>::SMatrix() : mNRows(MAX_SIZE), mNCols(MAX_SIZE)
{
//	MGSize n = MAX_SIZE*MAX_SIZE;
//	for ( MGSize i=0; i<n; i++)
//		mtab[i] = 0.0;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix< MAX_SIZE, ELEM_TYPE>::SMatrix( const MGSize& nrows, const MGSize& ncols) : mNRows(nrows), mNCols(ncols)
{
	CHECK( nrows <= MAX_SIZE);
	CHECK( ncols <= MAX_SIZE);

	::memset( mtab, 0, MAX_SIZE*MAX_SIZE*sizeof(ELEM_TYPE) );
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix< MAX_SIZE, ELEM_TYPE>::SMatrix( const MGSize& nrows, const MGSize& ncols, const ELEM_TYPE& d) : mNRows(nrows), mNCols(ncols)
{
	CHECK( nrows <= MAX_SIZE);
	CHECK( ncols <= MAX_SIZE);
	//mNRows = nrows;
	//mNCols = ncols;
	MGSize n = MAX_SIZE*MAX_SIZE;
	for ( MGSize i=0; i<n; i++)
		mtab[i] = d;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix< MAX_SIZE, ELEM_TYPE>::SMatrix( const SMatrix< MAX_SIZE, ELEM_TYPE>& a) : mNRows(a.NRows()), mNCols(a.NCols())
{
	CHECK( a.NRows() <= MAX_SIZE);
	CHECK( a.NCols() <= MAX_SIZE);
	//mNRows = a.NRows();
	//mNCols = a.NCols();

    for (MGSize i=0; i < mNRows; ++i)
      for (MGSize j=0; j < mNCols; ++j)
        (*this)(i,j) = a(i,j);
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix< MAX_SIZE, ELEM_TYPE>::SMatrix( const SVector< MAX_SIZE, ELEM_TYPE>& v) : mNRows(v.NRows()), mNCols(1)
{
	CHECK( v.NRows() <= MAX_SIZE);
	//mNRows = v.NRows();
	//mNCols = 1;
	for ( MGSize i=0; i<mNRows; ++i)
		(*this)(i,0) = v(i);
}




template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::Init( const ELEM_TYPE& d)
{
	//::memset( mtab, 0, MAX_SIZE*MAX_SIZE*sizeof(ELEM_TYPE) );

	MGSize n = MAX_SIZE*MAX_SIZE;
	for ( MGSize i=0; i<n; ++i)
		mtab[i] = d;

//	for (MGSize i=0; i < mNRows; i++)
//      for (MGSize j=0; j < mNCols; j++)
//        (*this)(i,j) = d;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::InitDelta( const MGSize& n, const ELEM_TYPE& d)
{
	CHECK( n <= MAX_SIZE);
	mNRows = mNCols = n;


    for (MGSize i=0; i < mNRows; ++i)
	{
		for (MGSize j=0; j < mNCols; ++j)
			(*this)(i,j) = 0.0;
		(*this)(i,i) = d;
	}
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::InitDelta( const SVector< MAX_SIZE, ELEM_TYPE>& v)
{
	CHECK( v.Size() <= MAX_SIZE);
	mNRows = mNCols = v.Size();


    for (MGSize i=0; i < mNRows; ++i)
	{
		for (MGSize j=0; j < mNCols; ++j)
			(*this)(i,j) = 0.0;
		(*this)(i,i) = v(i);
	}
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::Resize( const MGSize& nrows, const MGSize& ncols)
{
	CHECK( nrows <= MAX_SIZE);
	CHECK( ncols <= MAX_SIZE);
	mNRows = nrows;
	mNCols = ncols;

	//memset( mtab, 0, MAX_SIZE*MAX_SIZE*sizeof(ELEM_TYPE) );
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix< MAX_SIZE, ELEM_TYPE>& SMatrix< MAX_SIZE, ELEM_TYPE>::operator = ( const SMatrix< MAX_SIZE, ELEM_TYPE>& a)
{
	CHECK( a.NRows() <= MAX_SIZE);
	CHECK( a.NCols() <= MAX_SIZE);
	mNRows = a.mNRows;
	mNCols = a.mNCols;

    for (MGSize i=0; i < mNRows; ++i)
      for (MGSize j=0; j < mNCols; ++j)
        (*this)(i,j) = a(i,j);

	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix< MAX_SIZE, ELEM_TYPE>& SMatrix< MAX_SIZE, ELEM_TYPE>::operator = ( const SVector< MAX_SIZE, ELEM_TYPE>& v)
{
	CHECK( v.NRows() <= MAX_SIZE);
	mNRows = v.NRows();
	mNCols = 1;

    for (MGSize i=0; i < mNRows; ++i)
        (*this)(i,0) = v(i);

	return *this;
}



template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::Transp()
{
	SMatrix< MAX_SIZE, ELEM_TYPE>	atmp(mNCols, mNRows);
	MGSize i,j;

	for( i=0; i<mNRows; ++i)
		for( j=0; j<mNCols; ++j)
			atmp(j,i) = (*this)(i,j);

	(*this) = atmp;
}



template< MGSize MAX_SIZE, class ELEM_TYPE>
template < MGSize N, class T >
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::InsertSubMtx( const MGSize& irow, const MGSize& icol, const SMatrix<N,T>& mtx)
{
	CHECK( mtx.NRows()+irow <= mNRows && mtx.NCols()+icol <= mNCols );
	if ( !( mtx.NRows()+irow <= mNRows && mtx.NCols()+icol <= mNCols ) )
		THROW_INTERNAL("CRASH !");

	for ( MGSize i=0; i<mtx.NRows(); ++i)
		for ( MGSize j=0; j<mtx.NCols(); ++j)
			(*this)(i+irow,j+icol) = mtx(i,j);
}




/*
C-----------------------------------------------------------------
C    ROZWIAZANIE UKLADU ROWNAN LINIOWYCH
C---------------------------------------------
       
       SUBROUTINE GAUSS2(A,N,M)
         DIMENSION A(N,M)
         DO 2 I=1,N
           C=.0
           DO 1 J=I,N
             D=A(J,I)
             IF(ABS(C).GE.ABS(D))GO TO 1
               C=D
               L=J
   1       CONTINUE
           A(L,I)=A(I,I)
           A(I,I)=C-1.
           I1=I+1
           DO 2 K=I1,M
             D=A(L,K)
             A(L,K)=A(I,K)
             A(I,K)=D
             D=D/C
             DO 2 J=1,N
   2           A(J,K)=A(J,K)-D*A(J,I)
         RETURN
       END
*/


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::Gauss( SMatrix< MAX_SIZE, ELEM_TYPE>& b)
{
//	TRACE2("mnrows = %d mncols = %d", mNRows, mNCols );
//	TRACE2("b.mnrows = %d b.mncols = %d", b.mNRows, b.mNCols );
	CHECK( mNRows == mNCols);
	CHECK( mNRows == b.mNRows);

	ELEM_TYPE	d, c;
	MGSize		i, k, j, i1, l=0;
	for ( i=0; i<mNRows; ++i)
	{
		c = 0.0;
		for ( j=i; j<mNRows; ++j)
		{
			d = (*this)(j,i);
			if ( fabs(c) < fabs(d))
			{
				c = d;
				l = j;
			}
		}
		(*this)(l,i) = (*this)(i,i);
		(*this)(i,i) = c - 1.0;
		i1 = i + 1;

		for ( k=i1; k<mNRows; ++k)
		{
			d = (*this)(l,k);
			(*this)(l,k) = (*this)(i,k);
			(*this)(i,k) = d;
			d /= c;
			for ( j =0; j<mNRows; ++j)
				(*this)(j,k) = (*this)(j,k) - d*(*this)(j,i);
		}

		for ( k=0; k<b.mNCols; ++k)
		{
			d = b(l,k);
			b(l,k) = b(i,k);
			b(i,k) = d;
			d /= c;
			for ( j =0; j<mNRows; ++j)
				b(j,k) = b(j,k) - d*(*this)(j,i);
		}
	}
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::LUDecomp()
{
	CHECK( mNRows == mNCols);


	for ( MGSize j=0; j<mNCols; ++j)
	{
		for ( MGSize ib=1; ib<=j; ++ib)
		{
			ELEM_TYPE sum = (*this)(ib,j);

			for ( MGSize k=0; k<ib; ++k)
				sum -= (*this)(ib,k) * (*this)(k,j);

			(*this)(ib,j) = sum;
		}

		for ( MGSize ia=j+1; ia<mNRows; ++ia)
		{
			ELEM_TYPE sum = (*this)(ia,j);

			for ( MGSize k=0; k<j; ++k)
				sum -= (*this)(ia,k) * (*this)(k,j);

			(*this)(ia,j) = sum / (*this)(j,j);
		}
	}

}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::LUSolve( SMatrix< MAX_SIZE, ELEM_TYPE>& b)
{
	vector<MGSize> indx;

	LUDecomp( indx);
	BSubSolve( indx, b);
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::LUDecomp( vector<MGSize>& indx)
{
	CHECK( mNRows == mNCols);

	ELEM_TYPE	d;

	vector<ELEM_TYPE>	tabvv( mNRows);

	MGSize	i, imax, j, k; 
	ELEM_TYPE big, dum, sum, temp; 

	indx.resize( mNRows);

	d = 1.0;
	for ( i=0; i<mNRows; ++i) 
	{
		big = 0.0; 
		for ( j=0; j<mNRows; ++j) 
			if ( ( temp = fabs( (*this)(i,j)) ) > big) 
				big = temp; 

		if ( fabs(big) < SMTX_TINY)
			THROW_INTERNAL( "SMatrix LUD error: Singular matrix - " << big);

		tabvv[i] = 1.0 / big;
	} 

	for ( j=0; j<mNRows; ++j) 
	{
		for ( i=0; i<j; ++i) 
		{
			sum = (*this)(i,j); 
			for ( k=0; k<i; ++k) 
				sum -= (*this)(i,k) * (*this)(k,j); 
			(*this)(i,j) = sum; 
		} 

		big = 0.0;
		for ( i=j; i<mNRows; ++i) 
		{
			sum = (*this)(i,j); 
			for ( k=0; k<j; ++k)
				sum -= (*this)(i,k) * (*this)(k,j); 
			(*this)(i,j) = sum; 
			if ( ( dum = tabvv[i]*fabs(sum) ) >= big) 
			{
				big = dum; 
				imax = i; 
			} 
		} 
		if (j != imax) 
		{
			for ( k=0; k<mNRows; ++k) 
			{
				dum = (*this)(imax,k); 
				(*this)(imax,k) = (*this)(j,k); 
				(*this)(j,k) = dum; 
			} 
			d = -d;
			tabvv[imax] = tabvv[j];
		} 
		indx[j]=imax; 
		if ( ::fabs( (*this)(j,j) ) < ZERO) 
			(*this)(j,j) = SMTX_TINY;
		if (j != mNRows) 
		{
			dum = 1.0/((*this)(j,j)); 
			for ( i=j+1; i<mNRows; ++i) 
				(*this)(i,j) *= dum; 
		} 
	}

}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::BSubSolve( vector<MGSize>& indx, SMatrix< MAX_SIZE, ELEM_TYPE>& b)
{
	CHECK( mNRows == mNCols);
	CHECK( mNRows == b.mNRows);

	//MGSize	ip, i, j, k; 
	//MGInt	ii;
	//ELEM_TYPE sum; 

	//this->Write();
	//b.Write();

	//for ( k=0; k<b.mNCols; ++k)
	//{
	//	ii = -1;
	//	for ( i=0; i<mNRows; ++i) 
	//	{ 
	//		ip = indx[i]; 
	//		sum = b(ip,k); 
	//		b(ip,k) = b(i,k); 
	//		if ( ii+1) 
	//			for ( j=ii; j<=i-1; ++j) 
	//				sum -= (*this)(i,j)*b(j,k); 
	//		else if ( ::fabs( sum) < ZERO) 
	//			ii = (MGInt)i;
	//		b(i,k) = sum; 
	//	} 
	//
	//	for ( i=mNRows; i>0; --i) 
	//	{
	//		sum = b(i-1,k); 
	//		for ( j=i; j<mNRows; ++j) 
	//			sum -= (*this)(i-1,j)*b(j,k); 
	//		b(i-1,k) = sum/(*this)(i-1,i-1);
	//	}
	//}



	for ( MGSize k=0; k<b.mNCols; ++k)
	{
		for ( MGSize i=0; i<mNRows; ++i) 
		{ 
			MGSize ip = indx[i]; 
			ELEM_TYPE sum = b(ip,k); 
			b(ip,k) = b(i,k); 
			for ( MGSize j=0; j<i; ++j) 
				sum -= (*this)(i,j)*b(j,k); 
			b(i,k) = sum; 
		} 

		for ( MGInt i=mNRows-1; i>=0; --i) 
		{
			ELEM_TYPE sum = b(i,k); 
			for ( MGSize j=i+1; j<mNRows; ++j) 
				sum -= (*this)(i,j)*b(j,k); 
			b(i,k) = sum/(*this)(i,i);
		}	

	}

}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::Invert2x2()
{
	CHECK( mNRows == mNCols == 2);

	SMatrix< MAX_SIZE, ELEM_TYPE> mtemp = (*this);

	ELEM_TYPE	det = Determinant2x2();

	//ASSERT( ::fabs( det) > ZERO*ZERO);
	det = ELEM_TYPE( 1.0/det );

	(*this)(0,0) =   mtemp(1,1) * det;
	(*this)(0,1) = - mtemp(0,1) * det;
	(*this)(1,0) = - mtemp(1,0) * det;
	(*this)(1,1) =   mtemp(0,0) * det;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::Invert3x3()
{
	CHECK( mNRows == mNCols == 3);

	SMatrix< MAX_SIZE, ELEM_TYPE> mtemp = (*this);

	ELEM_TYPE	det = Determinant3x3();

	ASSERT( ::fabs( det) > ZERO*ZERO);
	det = ELEM_TYPE( 1.0/det);

	(*this)(0,0) =   ( mtemp(1,1)*mtemp(2,2) - mtemp(1,2)*mtemp(2,1) ) * det;
	(*this)(0,1) = - ( mtemp(0,1)*mtemp(2,2) - mtemp(0,2)*mtemp(2,1) ) * det;
	(*this)(0,2) =   ( mtemp(0,1)*mtemp(1,2) - mtemp(1,1)*mtemp(0,2) ) * det;
	(*this)(1,0) = - ( mtemp(1,0)*mtemp(2,2) - mtemp(1,2)*mtemp(2,0) ) * det;
	(*this)(1,1) =   ( mtemp(0,0)*mtemp(2,2) - mtemp(0,2)*mtemp(2,0) ) * det;
	(*this)(1,2) = - ( mtemp(0,0)*mtemp(1,2) - mtemp(0,2)*mtemp(1,0) ) * det;
	(*this)(2,0) =   ( mtemp(1,0)*mtemp(2,1) - mtemp(1,1)*mtemp(2,0) ) * det;
	(*this)(2,1) = - ( mtemp(0,0)*mtemp(2,1) - mtemp(0,1)*mtemp(2,0) ) * det;
	(*this)(2,2) =   ( mtemp(0,0)*mtemp(1,1) - mtemp(0,1)*mtemp(1,0) ) * det;
}



template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::Invert4x4()
{
	CHECK( mNRows == mNCols == 4);

	SMatrix< MAX_SIZE, ELEM_TYPE> mtemp = (*this);


	ELEM_TYPE det2_12_01 = mtemp(1,0)*mtemp(2,1) - mtemp(1,1)*mtemp(2,0);
	ELEM_TYPE det2_12_02 = mtemp(1,0)*mtemp(2,2) - mtemp(1,2)*mtemp(2,0);
	ELEM_TYPE det2_12_03 = mtemp(1,0)*mtemp(2,3) - mtemp(1,3)*mtemp(2,0);
	ELEM_TYPE det2_12_12 = mtemp(1,1)*mtemp(2,2) - mtemp(1,2)*mtemp(2,1);
	ELEM_TYPE det2_12_13 = mtemp(1,1)*mtemp(2,3) - mtemp(1,3)*mtemp(2,1);
	ELEM_TYPE det2_12_23 = mtemp(1,2)*mtemp(2,3) - mtemp(1,3)*mtemp(2,2);
	ELEM_TYPE det2_13_01 = mtemp(1,0)*mtemp(3,1) - mtemp(1,1)*mtemp(3,0);
	ELEM_TYPE det2_13_02 = mtemp(1,0)*mtemp(3,2) - mtemp(1,2)*mtemp(3,0);
	ELEM_TYPE det2_13_03 = mtemp(1,0)*mtemp(3,3) - mtemp(1,3)*mtemp(3,0);
	ELEM_TYPE det2_13_12 = mtemp(1,1)*mtemp(3,2) - mtemp(1,2)*mtemp(3,1);  
	ELEM_TYPE det2_13_13 = mtemp(1,1)*mtemp(3,3) - mtemp(1,3)*mtemp(3,1);
	ELEM_TYPE det2_13_23 = mtemp(1,2)*mtemp(3,3) - mtemp(1,3)*mtemp(3,2);  
	ELEM_TYPE det2_23_01 = mtemp(2,0)*mtemp(3,1) - mtemp(2,1)*mtemp(3,0);
	ELEM_TYPE det2_23_02 = mtemp(2,0)*mtemp(3,2) - mtemp(2,2)*mtemp(3,0);
	ELEM_TYPE det2_23_03 = mtemp(2,0)*mtemp(3,3) - mtemp(2,3)*mtemp(3,0);
	ELEM_TYPE det2_23_12 = mtemp(2,1)*mtemp(3,2) - mtemp(2,2)*mtemp(3,1);
	ELEM_TYPE det2_23_13 = mtemp(2,1)*mtemp(3,3) - mtemp(2,3)*mtemp(3,1);
	ELEM_TYPE det2_23_23 = mtemp(2,2)*mtemp(3,3) - mtemp(2,3)*mtemp(3,2);

	ELEM_TYPE det3_012_012 = mtemp(0,0)*det2_12_12 - mtemp(0,1)*det2_12_02 + mtemp(0,2)*det2_12_01;
	ELEM_TYPE det3_012_013 = mtemp(0,0)*det2_12_13 - mtemp(0,1)*det2_12_03 + mtemp(0,3)*det2_12_01;			
	ELEM_TYPE det3_012_023 = mtemp(0,0)*det2_12_23 - mtemp(0,2)*det2_12_03 + mtemp(0,3)*det2_12_02;			
	ELEM_TYPE det3_012_123 = mtemp(0,1)*det2_12_23 - mtemp(0,2)*det2_12_13 + mtemp(0,3)*det2_12_12;			
	ELEM_TYPE det3_013_012 = mtemp(0,0)*det2_13_12 - mtemp(0,1)*det2_13_02 + mtemp(0,2)*det2_13_01;
	ELEM_TYPE det3_013_013 = mtemp(0,0)*det2_13_13 - mtemp(0,1)*det2_13_03 + mtemp(0,3)*det2_13_01;
	ELEM_TYPE det3_013_023 = mtemp(0,0)*det2_13_23 - mtemp(0,2)*det2_13_03 + mtemp(0,3)*det2_13_02;			
	ELEM_TYPE det3_013_123 = mtemp(0,1)*det2_13_23 - mtemp(0,2)*det2_13_13 + mtemp(0,3)*det2_13_12;			
	ELEM_TYPE det3_023_012 = mtemp(0,0)*det2_23_12 - mtemp(0,1)*det2_23_02 + mtemp(0,2)*det2_23_01;
	ELEM_TYPE det3_023_013 = mtemp(0,0)*det2_23_13 - mtemp(0,1)*det2_23_03 + mtemp(0,3)*det2_23_01;
	ELEM_TYPE det3_023_023 = mtemp(0,0)*det2_23_23 - mtemp(0,2)*det2_23_03 + mtemp(0,3)*det2_23_02;
	ELEM_TYPE det3_023_123 = mtemp(0,1)*det2_23_23 - mtemp(0,2)*det2_23_13 + mtemp(0,3)*det2_23_12;					
	ELEM_TYPE det3_123_012 = mtemp(1,0)*det2_23_12 - mtemp(1,1)*det2_23_02 + mtemp(1,2)*det2_23_01;
	ELEM_TYPE det3_123_013 = mtemp(1,0)*det2_23_13 - mtemp(1,1)*det2_23_03 + mtemp(1,3)*det2_23_01;
	ELEM_TYPE det3_123_023 = mtemp(1,0)*det2_23_23 - mtemp(1,2)*det2_23_03 + mtemp(1,3)*det2_23_02;
	ELEM_TYPE det3_123_123 = mtemp(1,1)*det2_23_23 - mtemp(1,2)*det2_23_13 + mtemp(1,3)*det2_23_12;

	ELEM_TYPE det = mtemp(0,0)*det3_123_123 - mtemp(0,1)*det3_123_023 
					+ mtemp(0,2)*det3_123_013 - mtemp(0,3)*det3_123_012;

	//ASSERT( ::fabs( det) > ZERO*ZERO*ZERO);

	det = ELEM_TYPE( 1.0/det);

	(*this)(0,0) =  det3_123_123 * det;
	(*this)(0,1) = -det3_023_123 * det;
	(*this)(0,2) =  det3_013_123 * det;
	(*this)(0,3) = -det3_012_123 * det;

	(*this)(1,0) = -det3_123_023 * det;
	(*this)(1,1) =  det3_023_023 * det;
	(*this)(1,2) = -det3_013_023 * det;
	(*this)(1,3) =  det3_012_023 * det;

	(*this)(2,0) =  det3_123_013 * det;
	(*this)(2,1) = -det3_023_013 * det;
	(*this)(2,2) =  det3_013_013 * det;
	(*this)(2,3) = -det3_012_013 * det;

	(*this)(3,0) = -det3_123_012 * det;
	(*this)(3,1) =  det3_023_012 * det;
	(*this)(3,2) = -det3_013_012 * det;
	(*this)(3,3) =  det3_012_012 * det;
 
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::Invert5x5()
{
	CHECK( mNRows == mNCols == 5);

	const SMatrix< MAX_SIZE, ELEM_TYPE>& mtemp = (*this);


	ELEM_TYPE det2_23_01 = mtemp(2,0)*mtemp(3,1) - mtemp(2,1)*mtemp(3,0);
	ELEM_TYPE det2_23_02 = mtemp(2,0)*mtemp(3,2) - mtemp(2,2)*mtemp(3,0);
	ELEM_TYPE det2_23_03 = mtemp(2,0)*mtemp(3,3) - mtemp(2,3)*mtemp(3,0);
	ELEM_TYPE det2_23_04 = mtemp(2,0)*mtemp(3,4) - mtemp(2,4)*mtemp(3,0);
	ELEM_TYPE det2_23_12 = mtemp(2,1)*mtemp(3,2) - mtemp(2,2)*mtemp(3,1);
	ELEM_TYPE det2_23_13 = mtemp(2,1)*mtemp(3,3) - mtemp(2,3)*mtemp(3,1);
	ELEM_TYPE det2_23_14 = mtemp(2,1)*mtemp(3,4) - mtemp(2,4)*mtemp(3,1);
	ELEM_TYPE det2_23_23 = mtemp(2,2)*mtemp(3,3) - mtemp(2,3)*mtemp(3,2);
	ELEM_TYPE det2_23_24 = mtemp(2,2)*mtemp(3,4) - mtemp(2,4)*mtemp(3,2);
	ELEM_TYPE det2_23_34 = mtemp(2,3)*mtemp(3,4) - mtemp(2,4)*mtemp(3,3);
	ELEM_TYPE det2_24_01 = mtemp(2,0)*mtemp(4,1) - mtemp(2,1)*mtemp(4,0);
	ELEM_TYPE det2_24_02 = mtemp(2,0)*mtemp(4,2) - mtemp(2,2)*mtemp(4,0);
	ELEM_TYPE det2_24_03 = mtemp(2,0)*mtemp(4,3) - mtemp(2,3)*mtemp(4,0);
	ELEM_TYPE det2_24_04 = mtemp(2,0)*mtemp(4,4) - mtemp(2,4)*mtemp(4,0);
	ELEM_TYPE det2_24_12 = mtemp(2,1)*mtemp(4,2) - mtemp(2,2)*mtemp(4,1);
	ELEM_TYPE det2_24_13 = mtemp(2,1)*mtemp(4,3) - mtemp(2,3)*mtemp(4,1);
	ELEM_TYPE det2_24_14 = mtemp(2,1)*mtemp(4,4) - mtemp(2,4)*mtemp(4,1);
	ELEM_TYPE det2_24_23 = mtemp(2,2)*mtemp(4,3) - mtemp(2,3)*mtemp(4,2);
	ELEM_TYPE det2_24_24 = mtemp(2,2)*mtemp(4,4) - mtemp(2,4)*mtemp(4,2);
	ELEM_TYPE det2_24_34 = mtemp(2,3)*mtemp(4,4) - mtemp(2,4)*mtemp(4,3);
	ELEM_TYPE det2_34_01 = mtemp(3,0)*mtemp(4,1) - mtemp(3,1)*mtemp(4,0);
	ELEM_TYPE det2_34_02 = mtemp(3,0)*mtemp(4,2) - mtemp(3,2)*mtemp(4,0);
	ELEM_TYPE det2_34_03 = mtemp(3,0)*mtemp(4,3) - mtemp(3,3)*mtemp(4,0);
	ELEM_TYPE det2_34_04 = mtemp(3,0)*mtemp(4,4) - mtemp(3,4)*mtemp(4,0);
	ELEM_TYPE det2_34_12 = mtemp(3,1)*mtemp(4,2) - mtemp(3,2)*mtemp(4,1);
	ELEM_TYPE det2_34_13 = mtemp(3,1)*mtemp(4,3) - mtemp(3,3)*mtemp(4,1);
	ELEM_TYPE det2_34_14 = mtemp(3,1)*mtemp(4,4) - mtemp(3,4)*mtemp(4,1);
	ELEM_TYPE det2_34_23 = mtemp(3,2)*mtemp(4,3) - mtemp(3,3)*mtemp(4,2);
	ELEM_TYPE det2_34_24 = mtemp(3,2)*mtemp(4,4) - mtemp(3,4)*mtemp(4,2);
	ELEM_TYPE det2_34_34 = mtemp(3,3)*mtemp(4,4) - mtemp(3,4)*mtemp(4,3);


	ELEM_TYPE det3_123_012 = mtemp(1,0)*det2_23_12 - mtemp(1,1)*det2_23_02 + mtemp(1,2)*det2_23_01;
	ELEM_TYPE det3_123_013 = mtemp(1,0)*det2_23_13 - mtemp(1,1)*det2_23_03 + mtemp(1,3)*det2_23_01;
	ELEM_TYPE det3_123_014 = mtemp(1,0)*det2_23_14 - mtemp(1,1)*det2_23_04 + mtemp(1,4)*det2_23_01;
	ELEM_TYPE det3_123_023 = mtemp(1,0)*det2_23_23 - mtemp(1,2)*det2_23_03  + mtemp(1,3)*det2_23_02;
	ELEM_TYPE det3_123_024 = mtemp(1,0)*det2_23_24 - mtemp(1,2)*det2_23_04  + mtemp(1,4)*det2_23_02;
	ELEM_TYPE det3_123_034 = mtemp(1,0)*det2_23_34 - mtemp(1,3)*det2_23_04  + mtemp(1,4)*det2_23_03;
	ELEM_TYPE det3_123_123 = mtemp(1,1)*det2_23_23 - mtemp(1,2)*det2_23_13  + mtemp(1,3)*det2_23_12;
	ELEM_TYPE det3_123_124 = mtemp(1,1)*det2_23_24 - mtemp(1,2)*det2_23_14  + mtemp(1,4)*det2_23_12;
	ELEM_TYPE det3_123_134 = mtemp(1,1)*det2_23_34 - mtemp(1,3)*det2_23_14  + mtemp(1,4)*det2_23_13;
	ELEM_TYPE det3_123_234 = mtemp(1,2)*det2_23_34 - mtemp(1,3)*det2_23_24  + mtemp(1,4)*det2_23_23;
	ELEM_TYPE det3_124_012 = mtemp(1,0)*det2_24_12 - mtemp(1,1)*det2_24_02  + mtemp(1,2)*det2_24_01;
	ELEM_TYPE det3_124_013 = mtemp(1,0)*det2_24_13 - mtemp(1,1)*det2_24_03  + mtemp(1,3)*det2_24_01;
	ELEM_TYPE det3_124_014 = mtemp(1,0)*det2_24_14 - mtemp(1,1)*det2_24_04  + mtemp(1,4)*det2_24_01;
	ELEM_TYPE det3_124_023 = mtemp(1,0)*det2_24_23 - mtemp(1,2)*det2_24_03  + mtemp(1,3)*det2_24_02;
	ELEM_TYPE det3_124_024 = mtemp(1,0)*det2_24_24 - mtemp(1,2)*det2_24_04  + mtemp(1,4)*det2_24_02;
	ELEM_TYPE det3_124_034 = mtemp(1,0)*det2_24_34 - mtemp(1,3)*det2_24_04  + mtemp(1,4)*det2_24_03;
	ELEM_TYPE det3_124_123 = mtemp(1,1)*det2_24_23 - mtemp(1,2)*det2_24_13  + mtemp(1,3)*det2_24_12;
	ELEM_TYPE det3_124_124 = mtemp(1,1)*det2_24_24 - mtemp(1,2)*det2_24_14  + mtemp(1,4)*det2_24_12;
	ELEM_TYPE det3_124_134 = mtemp(1,1)*det2_24_34 - mtemp(1,3)*det2_24_14  + mtemp(1,4)*det2_24_13;
	ELEM_TYPE det3_124_234 = mtemp(1,2)*det2_24_34 - mtemp(1,3)*det2_24_24  + mtemp(1,4)*det2_24_23;
	ELEM_TYPE det3_134_012 = mtemp(1,0)*det2_34_12 - mtemp(1,1)*det2_34_02  + mtemp(1,2)*det2_34_01;
	ELEM_TYPE det3_134_013 = mtemp(1,0)*det2_34_13 - mtemp(1,1)*det2_34_03  + mtemp(1,3)*det2_34_01;
	ELEM_TYPE det3_134_014 = mtemp(1,0)*det2_34_14 - mtemp(1,1)*det2_34_04  + mtemp(1,4)*det2_34_01;
	ELEM_TYPE det3_134_023 = mtemp(1,0)*det2_34_23 - mtemp(1,2)*det2_34_03  + mtemp(1,3)*det2_34_02;
	ELEM_TYPE det3_134_024 = mtemp(1,0)*det2_34_24 - mtemp(1,2)*det2_34_04  + mtemp(1,4)*det2_34_02;
	ELEM_TYPE det3_134_034 = mtemp(1,0)*det2_34_34 - mtemp(1,3)*det2_34_04  + mtemp(1,4)*det2_34_03;
	ELEM_TYPE det3_134_123 = mtemp(1,1)*det2_34_23 - mtemp(1,2)*det2_34_13  + mtemp(1,3)*det2_34_12;
	ELEM_TYPE det3_134_124 = mtemp(1,1)*det2_34_24 - mtemp(1,2)*det2_34_14  + mtemp(1,4)*det2_34_12;
	ELEM_TYPE det3_134_134 = mtemp(1,1)*det2_34_34 - mtemp(1,3)*det2_34_14  + mtemp(1,4)*det2_34_13;
	ELEM_TYPE det3_134_234 = mtemp(1,2)*det2_34_34 - mtemp(1,3)*det2_34_24  + mtemp(1,4)*det2_34_23;
	ELEM_TYPE det3_234_012 = mtemp(2,0)*det2_34_12 - mtemp(2,1)*det2_34_02  + mtemp(2,2)*det2_34_01;
	ELEM_TYPE det3_234_013 = mtemp(2,0)*det2_34_13 - mtemp(2,1)*det2_34_03  + mtemp(2,3)*det2_34_01;
	ELEM_TYPE det3_234_014 = mtemp(2,0)*det2_34_14 - mtemp(2,1)*det2_34_04  + mtemp(2,4)*det2_34_01;
	ELEM_TYPE det3_234_023 = mtemp(2,0)*det2_34_23 - mtemp(2,2)*det2_34_03  + mtemp(2,3)*det2_34_02;
	ELEM_TYPE det3_234_024 = mtemp(2,0)*det2_34_24 - mtemp(2,2)*det2_34_04  + mtemp(2,4)*det2_34_02;
	ELEM_TYPE det3_234_034 = mtemp(2,0)*det2_34_34 - mtemp(2,3)*det2_34_04  + mtemp(2,4)*det2_34_03;
	ELEM_TYPE det3_234_123 = mtemp(2,1)*det2_34_23 - mtemp(2,2)*det2_34_13  + mtemp(2,3)*det2_34_12;
	ELEM_TYPE det3_234_124 = mtemp(2,1)*det2_34_24 - mtemp(2,2)*det2_34_14  + mtemp(2,4)*det2_34_12;
	ELEM_TYPE det3_234_134 = mtemp(2,1)*det2_34_34 - mtemp(2,3)*det2_34_14  + mtemp(2,4)*det2_34_13;
	ELEM_TYPE det3_234_234 = mtemp(2,2)*det2_34_34 - mtemp(2,3)*det2_34_24  + mtemp(2,4)*det2_34_23;


	ELEM_TYPE det4_0123_0123 = mtemp(0,0)*det3_123_123 - mtemp(0,1)*det3_123_023  + mtemp(0,2)*det3_123_013 - mtemp(0,3)*det3_123_012;
	ELEM_TYPE det4_0123_0124 = mtemp(0,0)*det3_123_124 - mtemp(0,1)*det3_123_024  + mtemp(0,2)*det3_123_014 - mtemp(0,4)*det3_123_012;
	ELEM_TYPE det4_0123_0134 = mtemp(0,0)*det3_123_134 - mtemp(0,1)*det3_123_034  + mtemp(0,3)*det3_123_014 - mtemp(0,4)*det3_123_013;
	ELEM_TYPE det4_0123_0234 = mtemp(0,0)*det3_123_234 - mtemp(0,2)*det3_123_034  + mtemp(0,3)*det3_123_024 - mtemp(0,4)*det3_123_023;
	ELEM_TYPE det4_0123_1234 = mtemp(0,1)*det3_123_234 - mtemp(0,2)*det3_123_134  + mtemp(0,3)*det3_123_124 - mtemp(0,4)*det3_123_123;
	ELEM_TYPE det4_0124_0123 = mtemp(0,0)*det3_124_123 - mtemp(0,1)*det3_124_023  + mtemp(0,2)*det3_124_013 - mtemp(0,3)*det3_124_012;
	ELEM_TYPE det4_0124_0124 = mtemp(0,0)*det3_124_124 - mtemp(0,1)*det3_124_024  + mtemp(0,2)*det3_124_014 - mtemp(0,4)*det3_124_012;
	ELEM_TYPE det4_0124_0134 = mtemp(0,0)*det3_124_134 - mtemp(0,1)*det3_124_034  + mtemp(0,3)*det3_124_014 - mtemp(0,4)*det3_124_013;
	ELEM_TYPE det4_0124_0234 = mtemp(0,0)*det3_124_234 - mtemp(0,2)*det3_124_034  + mtemp(0,3)*det3_124_024 - mtemp(0,4)*det3_124_023;
	ELEM_TYPE det4_0124_1234 = mtemp(0,1)*det3_124_234 - mtemp(0,2)*det3_124_134  + mtemp(0,3)*det3_124_124 - mtemp(0,4)*det3_124_123;
	ELEM_TYPE det4_0134_0123 = mtemp(0,0)*det3_134_123 - mtemp(0,1)*det3_134_023  + mtemp(0,2)*det3_134_013 - mtemp(0,3)*det3_134_012;
	ELEM_TYPE det4_0134_0124 = mtemp(0,0)*det3_134_124 - mtemp(0,1)*det3_134_024  + mtemp(0,2)*det3_134_014 - mtemp(0,4)*det3_134_012;
	ELEM_TYPE det4_0134_0134 = mtemp(0,0)*det3_134_134 - mtemp(0,1)*det3_134_034  + mtemp(0,3)*det3_134_014 - mtemp(0,4)*det3_134_013;
	ELEM_TYPE det4_0134_0234 = mtemp(0,0)*det3_134_234 - mtemp(0,2)*det3_134_034  + mtemp(0,3)*det3_134_024 - mtemp(0,4)*det3_134_023;
	ELEM_TYPE det4_0134_1234 = mtemp(0,1)*det3_134_234 - mtemp(0,2)*det3_134_134  + mtemp(0,3)*det3_134_124 - mtemp(0,4)*det3_134_123;
	ELEM_TYPE det4_0234_0123 = mtemp(0,0)*det3_234_123 - mtemp(0,1)*det3_234_023  + mtemp(0,2)*det3_234_013 - mtemp(0,3)*det3_234_012;
	ELEM_TYPE det4_0234_0124 = mtemp(0,0)*det3_234_124 - mtemp(0,1)*det3_234_024  + mtemp(0,2)*det3_234_014 - mtemp(0,4)*det3_234_012;
	ELEM_TYPE det4_0234_0134 = mtemp(0,0)*det3_234_134 - mtemp(0,1)*det3_234_034  + mtemp(0,3)*det3_234_014 - mtemp(0,4)*det3_234_013;
	ELEM_TYPE det4_0234_0234 = mtemp(0,0)*det3_234_234 - mtemp(0,2)*det3_234_034  + mtemp(0,3)*det3_234_024 - mtemp(0,4)*det3_234_023;
	ELEM_TYPE det4_0234_1234 = mtemp(0,1)*det3_234_234 - mtemp(0,2)*det3_234_134  + mtemp(0,3)*det3_234_124 - mtemp(0,4)*det3_234_123;
	ELEM_TYPE det4_1234_0123 = mtemp(1,0)*det3_234_123 - mtemp(1,1)*det3_234_023  + mtemp(1,2)*det3_234_013 - mtemp(1,3)*det3_234_012;
	ELEM_TYPE det4_1234_0124 = mtemp(1,0)*det3_234_124 - mtemp(1,1)*det3_234_024  + mtemp(1,2)*det3_234_014 - mtemp(1,4)*det3_234_012;
	ELEM_TYPE det4_1234_0134 = mtemp(1,0)*det3_234_134 - mtemp(1,1)*det3_234_034  + mtemp(1,3)*det3_234_014 - mtemp(1,4)*det3_234_013;
	ELEM_TYPE det4_1234_0234 = mtemp(1,0)*det3_234_234 - mtemp(1,2)*det3_234_034  + mtemp(1,3)*det3_234_024 - mtemp(1,4)*det3_234_023;
	ELEM_TYPE det4_1234_1234 = mtemp(1,1)*det3_234_234 - mtemp(1,2)*det3_234_134  + mtemp(1,3)*det3_234_124 - mtemp(1,4)*det3_234_123;
				

	ELEM_TYPE det = mtemp(0,0)*det4_1234_1234 - mtemp(0,1)*det4_1234_0234 
					+ mtemp(0,2)*det4_1234_0134 - mtemp(0,3)*det4_1234_0124 + mtemp(0,4)*det4_1234_0123;


	ASSERT( ::fabs( det) > ZERO*ZERO*ZERO*ZERO);

	det = ELEM_TYPE( 1.0/det);

	(*this)(0,0) =  det4_1234_1234 * det;
	(*this)(0,1) = -det4_0234_1234 * det;
	(*this)(0,2) =  det4_0134_1234 * det;
	(*this)(0,3) = -det4_0124_1234 * det;
	(*this)(0,4) =  det4_0123_1234 * det;


	(*this)(1,0) = -det4_1234_0234 * det;
	(*this)(1,1) =  det4_0234_0234 * det;
	(*this)(1,2) = -det4_0134_0234 * det;
	(*this)(1,3) =  det4_0124_0234 * det;
	(*this)(1,4) = -det4_0123_0234 * det;

	(*this)(2,0) =  det4_1234_0134 * det;
	(*this)(2,1) = -det4_0234_0134 * det;
	(*this)(2,2) =  det4_0134_0134 * det;
	(*this)(2,3) = -det4_0124_0134 * det;
	(*this)(2,4) =  det4_0123_0134 * det;

	(*this)(3,0) = -det4_1234_0124 * det;
	(*this)(3,1) =  det4_0234_0124 * det;
	(*this)(3,2) = -det4_0134_0124 * det;
	(*this)(3,3) =  det4_0124_0124 * det;
	(*this)(3,4) = -det4_0123_0124 * det;

	(*this)(4,0) =  det4_1234_0123 * det;
	(*this)(4,1) = -det4_0234_0123 * det;
	(*this)(4,2) =  det4_0134_0123 * det;
	(*this)(4,3) = -det4_0124_0123 * det;
	(*this)(4,4) =  det4_0123_0123 * det;  
}



template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::Invert6x6()
{
	CHECK( mNRows == mNCols == 6);

	const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx = (*this);

	ELEM_TYPE det2_34_01 = mtx(3,0) * mtx(4,1) - mtx(3,1) * mtx(4,0);
	ELEM_TYPE det2_34_02 = mtx(3,0) * mtx(4,2) - mtx(3,2) * mtx(4,0);
	ELEM_TYPE det2_34_03 = mtx(3,0) * mtx(4,3) - mtx(3,3) * mtx(4,0);
	ELEM_TYPE det2_34_04 = mtx(3,0) * mtx(4,4) - mtx(3,4) * mtx(4,0);
	ELEM_TYPE det2_34_05 = mtx(3,0) * mtx(4,5) - mtx(3,5) * mtx(4,0);
	ELEM_TYPE det2_34_12 = mtx(3,1) * mtx(4,2) - mtx(3,2) * mtx(4,1);
	ELEM_TYPE det2_34_13 = mtx(3,1) * mtx(4,3) - mtx(3,3) * mtx(4,1);
	ELEM_TYPE det2_34_14 = mtx(3,1) * mtx(4,4) - mtx(3,4) * mtx(4,1);
	ELEM_TYPE det2_34_15 = mtx(3,1) * mtx(4,5) - mtx(3,5) * mtx(4,1);
	ELEM_TYPE det2_34_23 = mtx(3,2) * mtx(4,3) - mtx(3,3) * mtx(4,2);
	ELEM_TYPE det2_34_24 = mtx(3,2) * mtx(4,4) - mtx(3,4) * mtx(4,2);
	ELEM_TYPE det2_34_25 = mtx(3,2) * mtx(4,5) - mtx(3,5) * mtx(4,2);
	ELEM_TYPE det2_34_34 = mtx(3,3) * mtx(4,4) - mtx(3,4) * mtx(4,3);
	ELEM_TYPE det2_34_35 = mtx(3,3) * mtx(4,5) - mtx(3,5) * mtx(4,3);
	ELEM_TYPE det2_34_45 = mtx(3,4) * mtx(4,5) - mtx(3,5) * mtx(4,4);
	ELEM_TYPE det2_35_01 = mtx(3,0) * mtx(5,1) - mtx(3,1) * mtx(5,0);
	ELEM_TYPE det2_35_02 = mtx(3,0) * mtx(5,2) - mtx(3,2) * mtx(5,0);
	ELEM_TYPE det2_35_03 = mtx(3,0) * mtx(5,3) - mtx(3,3) * mtx(5,0);
	ELEM_TYPE det2_35_04 = mtx(3,0) * mtx(5,4) - mtx(3,4) * mtx(5,0);
	ELEM_TYPE det2_35_05 = mtx(3,0) * mtx(5,5) - mtx(3,5) * mtx(5,0);
	ELEM_TYPE det2_35_12 = mtx(3,1) * mtx(5,2) - mtx(3,2) * mtx(5,1);
	ELEM_TYPE det2_35_13 = mtx(3,1) * mtx(5,3) - mtx(3,3) * mtx(5,1);
	ELEM_TYPE det2_35_14 = mtx(3,1) * mtx(5,4) - mtx(3,4) * mtx(5,1);
	ELEM_TYPE det2_35_15 = mtx(3,1) * mtx(5,5) - mtx(3,5) * mtx(5,1);
	ELEM_TYPE det2_35_23 = mtx(3,2) * mtx(5,3) - mtx(3,3) * mtx(5,2);
	ELEM_TYPE det2_35_24 = mtx(3,2) * mtx(5,4) - mtx(3,4) * mtx(5,2);
	ELEM_TYPE det2_35_25 = mtx(3,2) * mtx(5,5) - mtx(3,5) * mtx(5,2);
	ELEM_TYPE det2_35_34 = mtx(3,3) * mtx(5,4) - mtx(3,4) * mtx(5,3);
	ELEM_TYPE det2_35_35 = mtx(3,3) * mtx(5,5) - mtx(3,5) * mtx(5,3);
	ELEM_TYPE det2_35_45 = mtx(3,4) * mtx(5,5) - mtx(3,5) * mtx(5,4);
	ELEM_TYPE det2_45_01 = mtx(4,0) * mtx(5,1) - mtx(4,1) * mtx(5,0);
	ELEM_TYPE det2_45_02 = mtx(4,0) * mtx(5,2) - mtx(4,2) * mtx(5,0);
	ELEM_TYPE det2_45_03 = mtx(4,0) * mtx(5,3) - mtx(4,3) * mtx(5,0);
	ELEM_TYPE det2_45_04 = mtx(4,0) * mtx(5,4) - mtx(4,4) * mtx(5,0);
	ELEM_TYPE det2_45_05 = mtx(4,0) * mtx(5,5) - mtx(4,5) * mtx(5,0);
	ELEM_TYPE det2_45_12 = mtx(4,1) * mtx(5,2) - mtx(4,2) * mtx(5,1);
	ELEM_TYPE det2_45_13 = mtx(4,1) * mtx(5,3) - mtx(4,3) * mtx(5,1);
	ELEM_TYPE det2_45_14 = mtx(4,1) * mtx(5,4) - mtx(4,4) * mtx(5,1);
	ELEM_TYPE det2_45_15 = mtx(4,1) * mtx(5,5) - mtx(4,5) * mtx(5,1);
	ELEM_TYPE det2_45_23 = mtx(4,2) * mtx(5,3) - mtx(4,3) * mtx(5,2);
	ELEM_TYPE det2_45_24 = mtx(4,2) * mtx(5,4) - mtx(4,4) * mtx(5,2);
	ELEM_TYPE det2_45_25 = mtx(4,2) * mtx(5,5) - mtx(4,5) * mtx(5,2);
	ELEM_TYPE det2_45_34 = mtx(4,3) * mtx(5,4) - mtx(4,4) * mtx(5,3);
	ELEM_TYPE det2_45_35 = mtx(4,3) * mtx(5,5) - mtx(4,5) * mtx(5,3);
	ELEM_TYPE det2_45_45 = mtx(4,4) * mtx(5,5) - mtx(4,5) * mtx(5,4);

	ELEM_TYPE det3_234_012 =  mtx(2,0) * det2_34_12 -  mtx(2,1) * det2_34_02 +  mtx(2,2) * det2_34_01;
	ELEM_TYPE det3_234_013 =  mtx(2,0) * det2_34_13 -  mtx(2,1) * det2_34_03 +  mtx(2,3) * det2_34_01;
	ELEM_TYPE det3_234_014 =  mtx(2,0) * det2_34_14 -  mtx(2,1) * det2_34_04 +  mtx(2,4) * det2_34_01;
	ELEM_TYPE det3_234_015 =  mtx(2,0) * det2_34_15 -  mtx(2,1) * det2_34_05 +  mtx(2,5) * det2_34_01;
	ELEM_TYPE det3_234_023 =  mtx(2,0) * det2_34_23 -  mtx(2,2) * det2_34_03 +  mtx(2,3) * det2_34_02;
	ELEM_TYPE det3_234_024 =  mtx(2,0) * det2_34_24 -  mtx(2,2) * det2_34_04 +  mtx(2,4) * det2_34_02;
	ELEM_TYPE det3_234_025 =  mtx(2,0) * det2_34_25 -  mtx(2,2) * det2_34_05 +  mtx(2,5) * det2_34_02;
	ELEM_TYPE det3_234_034 =  mtx(2,0) * det2_34_34 -  mtx(2,3) * det2_34_04 +  mtx(2,4) * det2_34_03;
	ELEM_TYPE det3_234_035 =  mtx(2,0) * det2_34_35 -  mtx(2,3) * det2_34_05 +  mtx(2,5) * det2_34_03;
	ELEM_TYPE det3_234_045 =  mtx(2,0) * det2_34_45 -  mtx(2,4) * det2_34_05 +  mtx(2,5) * det2_34_04;
	ELEM_TYPE det3_234_123 =  mtx(2,1) * det2_34_23 -  mtx(2,2) * det2_34_13 +  mtx(2,3) * det2_34_12;
	ELEM_TYPE det3_234_124 =  mtx(2,1) * det2_34_24 -  mtx(2,2) * det2_34_14 +  mtx(2,4) * det2_34_12;
	ELEM_TYPE det3_234_125 =  mtx(2,1) * det2_34_25 -  mtx(2,2) * det2_34_15 +  mtx(2,5) * det2_34_12;
	ELEM_TYPE det3_234_134 =  mtx(2,1) * det2_34_34 -  mtx(2,3) * det2_34_14 +  mtx(2,4) * det2_34_13;
	ELEM_TYPE det3_234_135 =  mtx(2,1) * det2_34_35 -  mtx(2,3) * det2_34_15 +  mtx(2,5) * det2_34_13;
	ELEM_TYPE det3_234_145 =  mtx(2,1) * det2_34_45 -  mtx(2,4) * det2_34_15 +  mtx(2,5) * det2_34_14;
	ELEM_TYPE det3_234_234 =  mtx(2,2) * det2_34_34 -  mtx(2,3) * det2_34_24 +  mtx(2,4) * det2_34_23;
	ELEM_TYPE det3_234_235 =  mtx(2,2) * det2_34_35 -  mtx(2,3) * det2_34_25 +  mtx(2,5) * det2_34_23;
	ELEM_TYPE det3_234_245 =  mtx(2,2) * det2_34_45 -  mtx(2,4) * det2_34_25 +  mtx(2,5) * det2_34_24;
	ELEM_TYPE det3_234_345 =  mtx(2,3) * det2_34_45 -  mtx(2,4) * det2_34_35 +  mtx(2,5) * det2_34_34;
	ELEM_TYPE det3_235_012 =  mtx(2,0) * det2_35_12 -  mtx(2,1) * det2_35_02 +  mtx(2,2) * det2_35_01;
	ELEM_TYPE det3_235_013 =  mtx(2,0) * det2_35_13 -  mtx(2,1) * det2_35_03 +  mtx(2,3) * det2_35_01;
	ELEM_TYPE det3_235_014 =  mtx(2,0) * det2_35_14 -  mtx(2,1) * det2_35_04 +  mtx(2,4) * det2_35_01;
	ELEM_TYPE det3_235_015 =  mtx(2,0) * det2_35_15 -  mtx(2,1) * det2_35_05 +  mtx(2,5) * det2_35_01;
	ELEM_TYPE det3_235_023 =  mtx(2,0) * det2_35_23 -  mtx(2,2) * det2_35_03 +  mtx(2,3) * det2_35_02;
	ELEM_TYPE det3_235_024 =  mtx(2,0) * det2_35_24 -  mtx(2,2) * det2_35_04 +  mtx(2,4) * det2_35_02;
	ELEM_TYPE det3_235_025 =  mtx(2,0) * det2_35_25 -  mtx(2,2) * det2_35_05 +  mtx(2,5) * det2_35_02;
	ELEM_TYPE det3_235_034 =  mtx(2,0) * det2_35_34 -  mtx(2,3) * det2_35_04 +  mtx(2,4) * det2_35_03;
	ELEM_TYPE det3_235_035 =  mtx(2,0) * det2_35_35 -  mtx(2,3) * det2_35_05 +  mtx(2,5) * det2_35_03;
	ELEM_TYPE det3_235_045 =  mtx(2,0) * det2_35_45 -  mtx(2,4) * det2_35_05 +  mtx(2,5) * det2_35_04;
	ELEM_TYPE det3_235_123 =  mtx(2,1) * det2_35_23 -  mtx(2,2) * det2_35_13 +  mtx(2,3) * det2_35_12;
	ELEM_TYPE det3_235_124 =  mtx(2,1) * det2_35_24 -  mtx(2,2) * det2_35_14 +  mtx(2,4) * det2_35_12;
	ELEM_TYPE det3_235_125 =  mtx(2,1) * det2_35_25 -  mtx(2,2) * det2_35_15 +  mtx(2,5) * det2_35_12;
	ELEM_TYPE det3_235_134 =  mtx(2,1) * det2_35_34 -  mtx(2,3) * det2_35_14 +  mtx(2,4) * det2_35_13;
	ELEM_TYPE det3_235_135 =  mtx(2,1) * det2_35_35 -  mtx(2,3) * det2_35_15 +  mtx(2,5) * det2_35_13;
	ELEM_TYPE det3_235_145 =  mtx(2,1) * det2_35_45 -  mtx(2,4) * det2_35_15 +  mtx(2,5) * det2_35_14;
	ELEM_TYPE det3_235_234 =  mtx(2,2) * det2_35_34 -  mtx(2,3) * det2_35_24 +  mtx(2,4) * det2_35_23;
	ELEM_TYPE det3_235_235 =  mtx(2,2) * det2_35_35 -  mtx(2,3) * det2_35_25 +  mtx(2,5) * det2_35_23;
	ELEM_TYPE det3_235_245 =  mtx(2,2) * det2_35_45 -  mtx(2,4) * det2_35_25 +  mtx(2,5) * det2_35_24;
	ELEM_TYPE det3_235_345 =  mtx(2,3) * det2_35_45 -  mtx(2,4) * det2_35_35 +  mtx(2,5) * det2_35_34;
	ELEM_TYPE det3_245_012 =  mtx(2,0) * det2_45_12 -  mtx(2,1) * det2_45_02 +  mtx(2,2) * det2_45_01;
	ELEM_TYPE det3_245_013 =  mtx(2,0) * det2_45_13 -  mtx(2,1) * det2_45_03 +  mtx(2,3) * det2_45_01;
	ELEM_TYPE det3_245_014 =  mtx(2,0) * det2_45_14 -  mtx(2,1) * det2_45_04 +  mtx(2,4) * det2_45_01;
	ELEM_TYPE det3_245_015 =  mtx(2,0) * det2_45_15 -  mtx(2,1) * det2_45_05 +  mtx(2,5) * det2_45_01;
	ELEM_TYPE det3_245_023 =  mtx(2,0) * det2_45_23 -  mtx(2,2) * det2_45_03 +  mtx(2,3) * det2_45_02;
	ELEM_TYPE det3_245_024 =  mtx(2,0) * det2_45_24 -  mtx(2,2) * det2_45_04 +  mtx(2,4) * det2_45_02;
	ELEM_TYPE det3_245_025 =  mtx(2,0) * det2_45_25 -  mtx(2,2) * det2_45_05 +  mtx(2,5) * det2_45_02;
	ELEM_TYPE det3_245_034 =  mtx(2,0) * det2_45_34 -  mtx(2,3) * det2_45_04 +  mtx(2,4) * det2_45_03;
	ELEM_TYPE det3_245_035 =  mtx(2,0) * det2_45_35 -  mtx(2,3) * det2_45_05 +  mtx(2,5) * det2_45_03;
	ELEM_TYPE det3_245_045 =  mtx(2,0) * det2_45_45 -  mtx(2,4) * det2_45_05 +  mtx(2,5) * det2_45_04;
	ELEM_TYPE det3_245_123 =  mtx(2,1) * det2_45_23 -  mtx(2,2) * det2_45_13 +  mtx(2,3) * det2_45_12;
	ELEM_TYPE det3_245_124 =  mtx(2,1) * det2_45_24 -  mtx(2,2) * det2_45_14 +  mtx(2,4) * det2_45_12;
	ELEM_TYPE det3_245_125 =  mtx(2,1) * det2_45_25 -  mtx(2,2) * det2_45_15 +  mtx(2,5) * det2_45_12;
	ELEM_TYPE det3_245_134 =  mtx(2,1) * det2_45_34 -  mtx(2,3) * det2_45_14 +  mtx(2,4) * det2_45_13;
	ELEM_TYPE det3_245_135 =  mtx(2,1) * det2_45_35 -  mtx(2,3) * det2_45_15 +  mtx(2,5) * det2_45_13;
	ELEM_TYPE det3_245_145 =  mtx(2,1) * det2_45_45 -  mtx(2,4) * det2_45_15 +  mtx(2,5) * det2_45_14;
	ELEM_TYPE det3_245_234 =  mtx(2,2) * det2_45_34 -  mtx(2,3) * det2_45_24 +  mtx(2,4) * det2_45_23;
	ELEM_TYPE det3_245_235 =  mtx(2,2) * det2_45_35 -  mtx(2,3) * det2_45_25 +  mtx(2,5) * det2_45_23;
	ELEM_TYPE det3_245_245 =  mtx(2,2) * det2_45_45 -  mtx(2,4) * det2_45_25 +  mtx(2,5) * det2_45_24;
	ELEM_TYPE det3_245_345 =  mtx(2,3) * det2_45_45 -  mtx(2,4) * det2_45_35 +  mtx(2,5) * det2_45_34;
	ELEM_TYPE det3_345_012 =  mtx(3,0) * det2_45_12 -  mtx(3,1) * det2_45_02 +  mtx(3,2) * det2_45_01;
	ELEM_TYPE det3_345_013 =  mtx(3,0) * det2_45_13 -  mtx(3,1) * det2_45_03 +  mtx(3,3) * det2_45_01;
	ELEM_TYPE det3_345_014 =  mtx(3,0) * det2_45_14 -  mtx(3,1) * det2_45_04 +  mtx(3,4) * det2_45_01;
	ELEM_TYPE det3_345_015 =  mtx(3,0) * det2_45_15 -  mtx(3,1) * det2_45_05 +  mtx(3,5) * det2_45_01;
	ELEM_TYPE det3_345_023 =  mtx(3,0) * det2_45_23 -  mtx(3,2) * det2_45_03 +  mtx(3,3) * det2_45_02;
	ELEM_TYPE det3_345_024 =  mtx(3,0) * det2_45_24 -  mtx(3,2) * det2_45_04 +  mtx(3,4) * det2_45_02;
	ELEM_TYPE det3_345_025 =  mtx(3,0) * det2_45_25 -  mtx(3,2) * det2_45_05 +  mtx(3,5) * det2_45_02;
	ELEM_TYPE det3_345_034 =  mtx(3,0) * det2_45_34 -  mtx(3,3) * det2_45_04 +  mtx(3,4) * det2_45_03;
	ELEM_TYPE det3_345_035 =  mtx(3,0) * det2_45_35 -  mtx(3,3) * det2_45_05 +  mtx(3,5) * det2_45_03;
	ELEM_TYPE det3_345_045 =  mtx(3,0) * det2_45_45 -  mtx(3,4) * det2_45_05 +  mtx(3,5) * det2_45_04;
	ELEM_TYPE det3_345_123 =  mtx(3,1) * det2_45_23 -  mtx(3,2) * det2_45_13 +  mtx(3,3) * det2_45_12;
	ELEM_TYPE det3_345_124 =  mtx(3,1) * det2_45_24 -  mtx(3,2) * det2_45_14 +  mtx(3,4) * det2_45_12;
	ELEM_TYPE det3_345_125 =  mtx(3,1) * det2_45_25 -  mtx(3,2) * det2_45_15 +  mtx(3,5) * det2_45_12;
	ELEM_TYPE det3_345_134 =  mtx(3,1) * det2_45_34 -  mtx(3,3) * det2_45_14 +  mtx(3,4) * det2_45_13;
	ELEM_TYPE det3_345_135 =  mtx(3,1) * det2_45_35 -  mtx(3,3) * det2_45_15 +  mtx(3,5) * det2_45_13;
	ELEM_TYPE det3_345_145 =  mtx(3,1) * det2_45_45 -  mtx(3,4) * det2_45_15 +  mtx(3,5) * det2_45_14;
	ELEM_TYPE det3_345_234 =  mtx(3,2) * det2_45_34 -  mtx(3,3) * det2_45_24 +  mtx(3,4) * det2_45_23;
	ELEM_TYPE det3_345_235 =  mtx(3,2) * det2_45_35 -  mtx(3,3) * det2_45_25 +  mtx(3,5) * det2_45_23;
	ELEM_TYPE det3_345_245 =  mtx(3,2) * det2_45_45 -  mtx(3,4) * det2_45_25 +  mtx(3,5) * det2_45_24;
	ELEM_TYPE det3_345_345 =  mtx(3,3) * det2_45_45 -  mtx(3,4) * det2_45_35 +  mtx(3,5) * det2_45_34;

	ELEM_TYPE det4_1234_0123 =  mtx(1,0) * det3_234_123 -  mtx(1,1) * det3_234_023 +  mtx(1,2) * det3_234_013 -  mtx(1,3) * det3_234_012;
	ELEM_TYPE det4_1234_0124 =  mtx(1,0) * det3_234_124 -  mtx(1,1) * det3_234_024 +  mtx(1,2) * det3_234_014 -  mtx(1,4) * det3_234_012;
	ELEM_TYPE det4_1234_0125 =  mtx(1,0) * det3_234_125 -  mtx(1,1) * det3_234_025 +  mtx(1,2) * det3_234_015 -  mtx(1,5) * det3_234_012;
	ELEM_TYPE det4_1234_0134 =  mtx(1,0) * det3_234_134 -  mtx(1,1) * det3_234_034 +  mtx(1,3) * det3_234_014 -  mtx(1,4) * det3_234_013;
	ELEM_TYPE det4_1234_0135 =  mtx(1,0) * det3_234_135 -  mtx(1,1) * det3_234_035 +  mtx(1,3) * det3_234_015 -  mtx(1,5) * det3_234_013;
	ELEM_TYPE det4_1234_0145 =  mtx(1,0) * det3_234_145 -  mtx(1,1) * det3_234_045 +  mtx(1,4) * det3_234_015 -  mtx(1,5) * det3_234_014;
	ELEM_TYPE det4_1234_0234 =  mtx(1,0) * det3_234_234 -  mtx(1,2) * det3_234_034 +  mtx(1,3) * det3_234_024 -  mtx(1,4) * det3_234_023;
	ELEM_TYPE det4_1234_0235 =  mtx(1,0) * det3_234_235 -  mtx(1,2) * det3_234_035 +  mtx(1,3) * det3_234_025 -  mtx(1,5) * det3_234_023;
	ELEM_TYPE det4_1234_0245 =  mtx(1,0) * det3_234_245 -  mtx(1,2) * det3_234_045 +  mtx(1,4) * det3_234_025 -  mtx(1,5) * det3_234_024;
	ELEM_TYPE det4_1234_0345 =  mtx(1,0) * det3_234_345 -  mtx(1,3) * det3_234_045 +  mtx(1,4) * det3_234_035 -  mtx(1,5) * det3_234_034;
	ELEM_TYPE det4_1234_1234 =  mtx(1,1) * det3_234_234 -  mtx(1,2) * det3_234_134 +  mtx(1,3) * det3_234_124 -  mtx(1,4) * det3_234_123;
	ELEM_TYPE det4_1234_1235 =  mtx(1,1) * det3_234_235 -  mtx(1,2) * det3_234_135 +  mtx(1,3) * det3_234_125 -  mtx(1,5) * det3_234_123;
	ELEM_TYPE det4_1234_1245 =  mtx(1,1) * det3_234_245 -  mtx(1,2) * det3_234_145 +  mtx(1,4) * det3_234_125 -  mtx(1,5) * det3_234_124;
	ELEM_TYPE det4_1234_1345 =  mtx(1,1) * det3_234_345 -  mtx(1,3) * det3_234_145 +  mtx(1,4) * det3_234_135 -  mtx(1,5) * det3_234_134;
	ELEM_TYPE det4_1234_2345 =  mtx(1,2) * det3_234_345 -  mtx(1,3) * det3_234_245 +  mtx(1,4) * det3_234_235 -  mtx(1,5) * det3_234_234;
	ELEM_TYPE det4_1235_0123 =  mtx(1,0) * det3_235_123 -  mtx(1,1) * det3_235_023 +  mtx(1,2) * det3_235_013 -  mtx(1,3) * det3_235_012;
	ELEM_TYPE det4_1235_0124 =  mtx(1,0) * det3_235_124 -  mtx(1,1) * det3_235_024 +  mtx(1,2) * det3_235_014 -  mtx(1,4) * det3_235_012;
	ELEM_TYPE det4_1235_0125 =  mtx(1,0) * det3_235_125 -  mtx(1,1) * det3_235_025 +  mtx(1,2) * det3_235_015 -  mtx(1,5) * det3_235_012;
	ELEM_TYPE det4_1235_0134 =  mtx(1,0) * det3_235_134 -  mtx(1,1) * det3_235_034 +  mtx(1,3) * det3_235_014 -  mtx(1,4) * det3_235_013;
	ELEM_TYPE det4_1235_0135 =  mtx(1,0) * det3_235_135 -  mtx(1,1) * det3_235_035 +  mtx(1,3) * det3_235_015 -  mtx(1,5) * det3_235_013;
	ELEM_TYPE det4_1235_0145 =  mtx(1,0) * det3_235_145 -  mtx(1,1) * det3_235_045 +  mtx(1,4) * det3_235_015 -  mtx(1,5) * det3_235_014;
	ELEM_TYPE det4_1235_0234 =  mtx(1,0) * det3_235_234 -  mtx(1,2) * det3_235_034 +  mtx(1,3) * det3_235_024 -  mtx(1,4) * det3_235_023;
	ELEM_TYPE det4_1235_0235 =  mtx(1,0) * det3_235_235 -  mtx(1,2) * det3_235_035 +  mtx(1,3) * det3_235_025 -  mtx(1,5) * det3_235_023;
	ELEM_TYPE det4_1235_0245 =  mtx(1,0) * det3_235_245 -  mtx(1,2) * det3_235_045 +  mtx(1,4) * det3_235_025 -  mtx(1,5) * det3_235_024;
	ELEM_TYPE det4_1235_0345 =  mtx(1,0) * det3_235_345 -  mtx(1,3) * det3_235_045 +  mtx(1,4) * det3_235_035 -  mtx(1,5) * det3_235_034;
	ELEM_TYPE det4_1235_1234 =  mtx(1,1) * det3_235_234 -  mtx(1,2) * det3_235_134 +  mtx(1,3) * det3_235_124 -  mtx(1,4) * det3_235_123;
	ELEM_TYPE det4_1235_1235 =  mtx(1,1) * det3_235_235 -  mtx(1,2) * det3_235_135 +  mtx(1,3) * det3_235_125 -  mtx(1,5) * det3_235_123;
	ELEM_TYPE det4_1235_1245 =  mtx(1,1) * det3_235_245 -  mtx(1,2) * det3_235_145 +  mtx(1,4) * det3_235_125 -  mtx(1,5) * det3_235_124;
	ELEM_TYPE det4_1235_1345 =  mtx(1,1) * det3_235_345 -  mtx(1,3) * det3_235_145 +  mtx(1,4) * det3_235_135 -  mtx(1,5) * det3_235_134;
	ELEM_TYPE det4_1235_2345 =  mtx(1,2) * det3_235_345 -  mtx(1,3) * det3_235_245 +  mtx(1,4) * det3_235_235 -  mtx(1,5) * det3_235_234;
	ELEM_TYPE det4_1245_0123 =  mtx(1,0) * det3_245_123 -  mtx(1,1) * det3_245_023 +  mtx(1,2) * det3_245_013 -  mtx(1,3) * det3_245_012;
	ELEM_TYPE det4_1245_0124 =  mtx(1,0) * det3_245_124 -  mtx(1,1) * det3_245_024 +  mtx(1,2) * det3_245_014 -  mtx(1,4) * det3_245_012;
	ELEM_TYPE det4_1245_0125 =  mtx(1,0) * det3_245_125 -  mtx(1,1) * det3_245_025 +  mtx(1,2) * det3_245_015 -  mtx(1,5) * det3_245_012;
	ELEM_TYPE det4_1245_0134 =  mtx(1,0) * det3_245_134 -  mtx(1,1) * det3_245_034 +  mtx(1,3) * det3_245_014 -  mtx(1,4) * det3_245_013;
	ELEM_TYPE det4_1245_0135 =  mtx(1,0) * det3_245_135 -  mtx(1,1) * det3_245_035 +  mtx(1,3) * det3_245_015 -  mtx(1,5) * det3_245_013;
	ELEM_TYPE det4_1245_0145 =  mtx(1,0) * det3_245_145 -  mtx(1,1) * det3_245_045 +  mtx(1,4) * det3_245_015 -  mtx(1,5) * det3_245_014;
	ELEM_TYPE det4_1245_0234 =  mtx(1,0) * det3_245_234 -  mtx(1,2) * det3_245_034 +  mtx(1,3) * det3_245_024 -  mtx(1,4) * det3_245_023;
	ELEM_TYPE det4_1245_0235 =  mtx(1,0) * det3_245_235 -  mtx(1,2) * det3_245_035 +  mtx(1,3) * det3_245_025 -  mtx(1,5) * det3_245_023;
	ELEM_TYPE det4_1245_0245 =  mtx(1,0) * det3_245_245 -  mtx(1,2) * det3_245_045 +  mtx(1,4) * det3_245_025 -  mtx(1,5) * det3_245_024;
	ELEM_TYPE det4_1245_0345 =  mtx(1,0) * det3_245_345 -  mtx(1,3) * det3_245_045 +  mtx(1,4) * det3_245_035 -  mtx(1,5) * det3_245_034;
	ELEM_TYPE det4_1245_1234 =  mtx(1,1) * det3_245_234 -  mtx(1,2) * det3_245_134 +  mtx(1,3) * det3_245_124 -  mtx(1,4) * det3_245_123;
	ELEM_TYPE det4_1245_1235 =  mtx(1,1) * det3_245_235 -  mtx(1,2) * det3_245_135 +  mtx(1,3) * det3_245_125 -  mtx(1,5) * det3_245_123;
	ELEM_TYPE det4_1245_1245 =  mtx(1,1) * det3_245_245 -  mtx(1,2) * det3_245_145 +  mtx(1,4) * det3_245_125 -  mtx(1,5) * det3_245_124;
	ELEM_TYPE det4_1245_1345 =  mtx(1,1) * det3_245_345 -  mtx(1,3) * det3_245_145 +  mtx(1,4) * det3_245_135 -  mtx(1,5) * det3_245_134;
	ELEM_TYPE det4_1245_2345 =  mtx(1,2) * det3_245_345 -  mtx(1,3) * det3_245_245 +  mtx(1,4) * det3_245_235 -  mtx(1,5) * det3_245_234;
	ELEM_TYPE det4_1345_0123 =  mtx(1,0) * det3_345_123 -  mtx(1,1) * det3_345_023 +  mtx(1,2) * det3_345_013 -  mtx(1,3) * det3_345_012;
	ELEM_TYPE det4_1345_0124 =  mtx(1,0) * det3_345_124 -  mtx(1,1) * det3_345_024 +  mtx(1,2) * det3_345_014 -  mtx(1,4) * det3_345_012;
	ELEM_TYPE det4_1345_0125 =  mtx(1,0) * det3_345_125 -  mtx(1,1) * det3_345_025 +  mtx(1,2) * det3_345_015 -  mtx(1,5) * det3_345_012;
	ELEM_TYPE det4_1345_0134 =  mtx(1,0) * det3_345_134 -  mtx(1,1) * det3_345_034 +  mtx(1,3) * det3_345_014 -  mtx(1,4) * det3_345_013;
	ELEM_TYPE det4_1345_0135 =  mtx(1,0) * det3_345_135 -  mtx(1,1) * det3_345_035 +  mtx(1,3) * det3_345_015 -  mtx(1,5) * det3_345_013;
	ELEM_TYPE det4_1345_0145 =  mtx(1,0) * det3_345_145 -  mtx(1,1) * det3_345_045 +  mtx(1,4) * det3_345_015 -  mtx(1,5) * det3_345_014;
	ELEM_TYPE det4_1345_0234 =  mtx(1,0) * det3_345_234 -  mtx(1,2) * det3_345_034 +  mtx(1,3) * det3_345_024 -  mtx(1,4) * det3_345_023;
	ELEM_TYPE det4_1345_0235 =  mtx(1,0) * det3_345_235 -  mtx(1,2) * det3_345_035 +  mtx(1,3) * det3_345_025 -  mtx(1,5) * det3_345_023;
	ELEM_TYPE det4_1345_0245 =  mtx(1,0) * det3_345_245 -  mtx(1,2) * det3_345_045 +  mtx(1,4) * det3_345_025 -  mtx(1,5) * det3_345_024;
	ELEM_TYPE det4_1345_0345 =  mtx(1,0) * det3_345_345 -  mtx(1,3) * det3_345_045 +  mtx(1,4) * det3_345_035 -  mtx(1,5) * det3_345_034;
	ELEM_TYPE det4_1345_1234 =  mtx(1,1) * det3_345_234 -  mtx(1,2) * det3_345_134 +  mtx(1,3) * det3_345_124 -  mtx(1,4) * det3_345_123;
	ELEM_TYPE det4_1345_1235 =  mtx(1,1) * det3_345_235 -  mtx(1,2) * det3_345_135 +  mtx(1,3) * det3_345_125 -  mtx(1,5) * det3_345_123;
	ELEM_TYPE det4_1345_1245 =  mtx(1,1) * det3_345_245 -  mtx(1,2) * det3_345_145 +  mtx(1,4) * det3_345_125 -  mtx(1,5) * det3_345_124;
	ELEM_TYPE det4_1345_1345 =  mtx(1,1) * det3_345_345 -  mtx(1,3) * det3_345_145 +  mtx(1,4) * det3_345_135 -  mtx(1,5) * det3_345_134;
	ELEM_TYPE det4_1345_2345 =  mtx(1,2) * det3_345_345 -  mtx(1,3) * det3_345_245 +  mtx(1,4) * det3_345_235 -  mtx(1,5) * det3_345_234;
	ELEM_TYPE det4_2345_0123 =  mtx(2,0) * det3_345_123 -  mtx(2,1) * det3_345_023 +  mtx(2,2) * det3_345_013 -  mtx(2,3) * det3_345_012;
	ELEM_TYPE det4_2345_0124 =  mtx(2,0) * det3_345_124 -  mtx(2,1) * det3_345_024 +  mtx(2,2) * det3_345_014 -  mtx(2,4) * det3_345_012;
	ELEM_TYPE det4_2345_0125 =  mtx(2,0) * det3_345_125 -  mtx(2,1) * det3_345_025 +  mtx(2,2) * det3_345_015 -  mtx(2,5) * det3_345_012;
	ELEM_TYPE det4_2345_0134 =  mtx(2,0) * det3_345_134 -  mtx(2,1) * det3_345_034 +  mtx(2,3) * det3_345_014 -  mtx(2,4) * det3_345_013;
	ELEM_TYPE det4_2345_0135 =  mtx(2,0) * det3_345_135 -  mtx(2,1) * det3_345_035 +  mtx(2,3) * det3_345_015 -  mtx(2,5) * det3_345_013;
	ELEM_TYPE det4_2345_0145 =  mtx(2,0) * det3_345_145 -  mtx(2,1) * det3_345_045 +  mtx(2,4) * det3_345_015 -  mtx(2,5) * det3_345_014;
	ELEM_TYPE det4_2345_0234 =  mtx(2,0) * det3_345_234 -  mtx(2,2) * det3_345_034 +  mtx(2,3) * det3_345_024 -  mtx(2,4) * det3_345_023;
	ELEM_TYPE det4_2345_0235 =  mtx(2,0) * det3_345_235 -  mtx(2,2) * det3_345_035 +  mtx(2,3) * det3_345_025 -  mtx(2,5) * det3_345_023;
	ELEM_TYPE det4_2345_0245 =  mtx(2,0) * det3_345_245 -  mtx(2,2) * det3_345_045 +  mtx(2,4) * det3_345_025 -  mtx(2,5) * det3_345_024;
	ELEM_TYPE det4_2345_0345 =  mtx(2,0) * det3_345_345 -  mtx(2,3) * det3_345_045 +  mtx(2,4) * det3_345_035 -  mtx(2,5) * det3_345_034;
	ELEM_TYPE det4_2345_1234 =  mtx(2,1) * det3_345_234 -  mtx(2,2) * det3_345_134 +  mtx(2,3) * det3_345_124 -  mtx(2,4) * det3_345_123;
	ELEM_TYPE det4_2345_1235 =  mtx(2,1) * det3_345_235 -  mtx(2,2) * det3_345_135 +  mtx(2,3) * det3_345_125 -  mtx(2,5) * det3_345_123;
	ELEM_TYPE det4_2345_1245 =  mtx(2,1) * det3_345_245 -  mtx(2,2) * det3_345_145 +  mtx(2,4) * det3_345_125 -  mtx(2,5) * det3_345_124;
	ELEM_TYPE det4_2345_1345 =  mtx(2,1) * det3_345_345 -  mtx(2,3) * det3_345_145 +  mtx(2,4) * det3_345_135 -  mtx(2,5) * det3_345_134;
	ELEM_TYPE det4_2345_2345 =  mtx(2,2) * det3_345_345 -  mtx(2,3) * det3_345_245 +  mtx(2,4) * det3_345_235 -  mtx(2,5) * det3_345_234;

	ELEM_TYPE det5_01234_01234 =  mtx(0,0) * det4_1234_1234 -  mtx(0,1) * det4_1234_0234 +  mtx(0,2) * det4_1234_0134 -  mtx(0,3) * det4_1234_0124 +  mtx(0,4) * det4_1234_0123;
	ELEM_TYPE det5_01234_01235 =  mtx(0,0) * det4_1234_1235 -  mtx(0,1) * det4_1234_0235 +  mtx(0,2) * det4_1234_0135 -  mtx(0,3) * det4_1234_0125 +  mtx(0,5) * det4_1234_0123;
	ELEM_TYPE det5_01234_01245 =  mtx(0,0) * det4_1234_1245 -  mtx(0,1) * det4_1234_0245 +  mtx(0,2) * det4_1234_0145 -  mtx(0,4) * det4_1234_0125 +  mtx(0,5) * det4_1234_0124;
	ELEM_TYPE det5_01234_01345 =  mtx(0,0) * det4_1234_1345 -  mtx(0,1) * det4_1234_0345 +  mtx(0,3) * det4_1234_0145 -  mtx(0,4) * det4_1234_0135 +  mtx(0,5) * det4_1234_0134;
	ELEM_TYPE det5_01234_02345 =  mtx(0,0) * det4_1234_2345 -  mtx(0,2) * det4_1234_0345 +  mtx(0,3) * det4_1234_0245 -  mtx(0,4) * det4_1234_0235 +  mtx(0,5) * det4_1234_0234;
	ELEM_TYPE det5_01234_12345 =  mtx(0,1) * det4_1234_2345 -  mtx(0,2) * det4_1234_1345 +  mtx(0,3) * det4_1234_1245 -  mtx(0,4) * det4_1234_1235 +  mtx(0,5) * det4_1234_1234;
	ELEM_TYPE det5_01235_01234 =  mtx(0,0) * det4_1235_1234 -  mtx(0,1) * det4_1235_0234 +  mtx(0,2) * det4_1235_0134 -  mtx(0,3) * det4_1235_0124 +  mtx(0,4) * det4_1235_0123;
	ELEM_TYPE det5_01235_01235 =  mtx(0,0) * det4_1235_1235 -  mtx(0,1) * det4_1235_0235 +  mtx(0,2) * det4_1235_0135 -  mtx(0,3) * det4_1235_0125 +  mtx(0,5) * det4_1235_0123;
	ELEM_TYPE det5_01235_01245 =  mtx(0,0) * det4_1235_1245 -  mtx(0,1) * det4_1235_0245 +  mtx(0,2) * det4_1235_0145 -  mtx(0,4) * det4_1235_0125 +  mtx(0,5) * det4_1235_0124;
	ELEM_TYPE det5_01235_01345 =  mtx(0,0) * det4_1235_1345 -  mtx(0,1) * det4_1235_0345 +  mtx(0,3) * det4_1235_0145 -  mtx(0,4) * det4_1235_0135 +  mtx(0,5) * det4_1235_0134;
	ELEM_TYPE det5_01235_02345 =  mtx(0,0) * det4_1235_2345 -  mtx(0,2) * det4_1235_0345 +  mtx(0,3) * det4_1235_0245 -  mtx(0,4) * det4_1235_0235 +  mtx(0,5) * det4_1235_0234;
	ELEM_TYPE det5_01235_12345 =  mtx(0,1) * det4_1235_2345 -  mtx(0,2) * det4_1235_1345 +  mtx(0,3) * det4_1235_1245 -  mtx(0,4) * det4_1235_1235 +  mtx(0,5) * det4_1235_1234;
	ELEM_TYPE det5_01245_01234 =  mtx(0,0) * det4_1245_1234 -  mtx(0,1) * det4_1245_0234 +  mtx(0,2) * det4_1245_0134 -  mtx(0,3) * det4_1245_0124 +  mtx(0,4) * det4_1245_0123;
	ELEM_TYPE det5_01245_01235 =  mtx(0,0) * det4_1245_1235 -  mtx(0,1) * det4_1245_0235 +  mtx(0,2) * det4_1245_0135 -  mtx(0,3) * det4_1245_0125 +  mtx(0,5) * det4_1245_0123;
	ELEM_TYPE det5_01245_01245 =  mtx(0,0) * det4_1245_1245 -  mtx(0,1) * det4_1245_0245 +  mtx(0,2) * det4_1245_0145 -  mtx(0,4) * det4_1245_0125 +  mtx(0,5) * det4_1245_0124;
	ELEM_TYPE det5_01245_01345 =  mtx(0,0) * det4_1245_1345 -  mtx(0,1) * det4_1245_0345 +  mtx(0,3) * det4_1245_0145 -  mtx(0,4) * det4_1245_0135 +  mtx(0,5) * det4_1245_0134;
	ELEM_TYPE det5_01245_02345 =  mtx(0,0) * det4_1245_2345 -  mtx(0,2) * det4_1245_0345 +  mtx(0,3) * det4_1245_0245 -  mtx(0,4) * det4_1245_0235 +  mtx(0,5) * det4_1245_0234;
	ELEM_TYPE det5_01245_12345 =  mtx(0,1) * det4_1245_2345 -  mtx(0,2) * det4_1245_1345 +  mtx(0,3) * det4_1245_1245 -  mtx(0,4) * det4_1245_1235 +  mtx(0,5) * det4_1245_1234;
	ELEM_TYPE det5_01345_01234 =  mtx(0,0) * det4_1345_1234 -  mtx(0,1) * det4_1345_0234 +  mtx(0,2) * det4_1345_0134 -  mtx(0,3) * det4_1345_0124 +  mtx(0,4) * det4_1345_0123;
	ELEM_TYPE det5_01345_01235 =  mtx(0,0) * det4_1345_1235 -  mtx(0,1) * det4_1345_0235 +  mtx(0,2) * det4_1345_0135 -  mtx(0,3) * det4_1345_0125 +  mtx(0,5) * det4_1345_0123;
	ELEM_TYPE det5_01345_01245 =  mtx(0,0) * det4_1345_1245 -  mtx(0,1) * det4_1345_0245 +  mtx(0,2) * det4_1345_0145 -  mtx(0,4) * det4_1345_0125 +  mtx(0,5) * det4_1345_0124;
	ELEM_TYPE det5_01345_01345 =  mtx(0,0) * det4_1345_1345 -  mtx(0,1) * det4_1345_0345 +  mtx(0,3) * det4_1345_0145 -  mtx(0,4) * det4_1345_0135 +  mtx(0,5) * det4_1345_0134;
	ELEM_TYPE det5_01345_02345 =  mtx(0,0) * det4_1345_2345 -  mtx(0,2) * det4_1345_0345 +  mtx(0,3) * det4_1345_0245 -  mtx(0,4) * det4_1345_0235 +  mtx(0,5) * det4_1345_0234;
	ELEM_TYPE det5_01345_12345 =  mtx(0,1) * det4_1345_2345 -  mtx(0,2) * det4_1345_1345 +  mtx(0,3) * det4_1345_1245 -  mtx(0,4) * det4_1345_1235 +  mtx(0,5) * det4_1345_1234;
	ELEM_TYPE det5_02345_01234 =  mtx(0,0) * det4_2345_1234 -  mtx(0,1) * det4_2345_0234 +  mtx(0,2) * det4_2345_0134 -  mtx(0,3) * det4_2345_0124 +  mtx(0,4) * det4_2345_0123;
	ELEM_TYPE det5_02345_01235 =  mtx(0,0) * det4_2345_1235 -  mtx(0,1) * det4_2345_0235 +  mtx(0,2) * det4_2345_0135 -  mtx(0,3) * det4_2345_0125 +  mtx(0,5) * det4_2345_0123;
	ELEM_TYPE det5_02345_01245 =  mtx(0,0) * det4_2345_1245 -  mtx(0,1) * det4_2345_0245 +  mtx(0,2) * det4_2345_0145 -  mtx(0,4) * det4_2345_0125 +  mtx(0,5) * det4_2345_0124;
	ELEM_TYPE det5_02345_01345 =  mtx(0,0) * det4_2345_1345 -  mtx(0,1) * det4_2345_0345 +  mtx(0,3) * det4_2345_0145 -  mtx(0,4) * det4_2345_0135 +  mtx(0,5) * det4_2345_0134;
	ELEM_TYPE det5_02345_02345 =  mtx(0,0) * det4_2345_2345 -  mtx(0,2) * det4_2345_0345 +  mtx(0,3) * det4_2345_0245 -  mtx(0,4) * det4_2345_0235 +  mtx(0,5) * det4_2345_0234;
	ELEM_TYPE det5_02345_12345 =  mtx(0,1) * det4_2345_2345 -  mtx(0,2) * det4_2345_1345 +  mtx(0,3) * det4_2345_1245 -  mtx(0,4) * det4_2345_1235 +  mtx(0,5) * det4_2345_1234;
	ELEM_TYPE det5_12345_01234 =  mtx(1,0) * det4_2345_1234 -  mtx(1,1) * det4_2345_0234 +  mtx(1,2) * det4_2345_0134 -  mtx(1,3) * det4_2345_0124 +  mtx(1,4) * det4_2345_0123;
	ELEM_TYPE det5_12345_01235 =  mtx(1,0) * det4_2345_1235 -  mtx(1,1) * det4_2345_0235 +  mtx(1,2) * det4_2345_0135 -  mtx(1,3) * det4_2345_0125 +  mtx(1,5) * det4_2345_0123;
	ELEM_TYPE det5_12345_01245 =  mtx(1,0) * det4_2345_1245 -  mtx(1,1) * det4_2345_0245 +  mtx(1,2) * det4_2345_0145 -  mtx(1,4) * det4_2345_0125 +  mtx(1,5) * det4_2345_0124;
	ELEM_TYPE det5_12345_01345 =  mtx(1,0) * det4_2345_1345 -  mtx(1,1) * det4_2345_0345 +  mtx(1,3) * det4_2345_0145 -  mtx(1,4) * det4_2345_0135 +  mtx(1,5) * det4_2345_0134;
	ELEM_TYPE det5_12345_02345 =  mtx(1,0) * det4_2345_2345 -  mtx(1,2) * det4_2345_0345 +  mtx(1,3) * det4_2345_0245 -  mtx(1,4) * det4_2345_0235 +  mtx(1,5) * det4_2345_0234;
	ELEM_TYPE det5_12345_12345 =  mtx(1,1) * det4_2345_2345 -  mtx(1,2) * det4_2345_1345 +  mtx(1,3) * det4_2345_1245 -  mtx(1,4) * det4_2345_1235 +  mtx(1,5) * det4_2345_1234;

	ELEM_TYPE det6_012345_012345 =  mtx(0,0) * det5_12345_12345 -  mtx(0,1) * det5_12345_02345 +  mtx(0,2) * det5_12345_01345 -  mtx(0,3) * det5_12345_01245 +  mtx(0,4) * det5_12345_01235 -  mtx(0,5) * det5_12345_01234;

	ELEM_TYPE det = ELEM_TYPE( 1.0/det6_012345_012345);

	(*this)(0,0) =   det5_12345_12345 * det;
	(*this)(0,1) = - det5_02345_12345 * det;
	(*this)(0,2) =   det5_01345_12345 * det;
	(*this)(0,3) = - det5_01245_12345 * det;
	(*this)(0,4) =   det5_01235_12345 * det;
	(*this)(0,5) = - det5_01234_12345 * det;

	(*this)(1,0) = - det5_12345_02345 * det;
	(*this)(1,1) =   det5_02345_02345 * det;
	(*this)(1,2) = - det5_01345_02345 * det;
	(*this)(1,3) =   det5_01245_02345 * det;
	(*this)(1,4) = - det5_01235_02345 * det;
	(*this)(1,5) =   det5_01234_02345 * det;

	(*this)(2,0) =   det5_12345_01345 * det;
	(*this)(2,1) = - det5_02345_01345 * det;
	(*this)(2,2) =   det5_01345_01345 * det;
	(*this)(2,3) = - det5_01245_01345 * det;
	(*this)(2,4) =   det5_01235_01345 * det;
	(*this)(2,5) = - det5_01234_01345 * det;

	(*this)(3,0) = - det5_12345_01245 * det;
	(*this)(3,1) =   det5_02345_01245 * det;
	(*this)(3,2) = - det5_01345_01245 * det;
	(*this)(3,3) =   det5_01245_01245 * det;
	(*this)(3,4) = - det5_01235_01245 * det;
	(*this)(3,5) =   det5_01234_01245 * det;

	(*this)(4,0) =   det5_12345_01235 * det;
	(*this)(4,1) = - det5_02345_01235 * det;
	(*this)(4,2) =   det5_01345_01235 * det;
	(*this)(4,3) = - det5_01245_01235 * det;
	(*this)(4,4) =   det5_01235_01235 * det;
	(*this)(4,5) = - det5_01234_01235 * det;

	(*this)(5,0) = - det5_12345_01234 * det;
	(*this)(5,1) =   det5_02345_01234 * det;
	(*this)(5,2) = - det5_01345_01234 * det;
	(*this)(5,3) =   det5_01245_01234 * det;
	(*this)(5,4) = - det5_01235_01234 * det;
	(*this)(5,5) =   det5_01234_01234 * det;

}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::Invert7x7()
{
	CHECK( mNRows == mNCols == 7);

	const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx = (*this);

	ELEM_TYPE det2_45_01 = mtx(4,0) * mtx(5,1) - mtx(4,1) * mtx(5,0);
	ELEM_TYPE det2_45_02 = mtx(4,0) * mtx(5,2) - mtx(4,2) * mtx(5,0);
	ELEM_TYPE det2_45_03 = mtx(4,0) * mtx(5,3) - mtx(4,3) * mtx(5,0);
	ELEM_TYPE det2_45_04 = mtx(4,0) * mtx(5,4) - mtx(4,4) * mtx(5,0);
	ELEM_TYPE det2_45_05 = mtx(4,0) * mtx(5,5) - mtx(4,5) * mtx(5,0);
	ELEM_TYPE det2_45_06 = mtx(4,0) * mtx(5,6) - mtx(4,6) * mtx(5,0);
	ELEM_TYPE det2_45_12 = mtx(4,1) * mtx(5,2) - mtx(4,2) * mtx(5,1);
	ELEM_TYPE det2_45_13 = mtx(4,1) * mtx(5,3) - mtx(4,3) * mtx(5,1);
	ELEM_TYPE det2_45_14 = mtx(4,1) * mtx(5,4) - mtx(4,4) * mtx(5,1);
	ELEM_TYPE det2_45_15 = mtx(4,1) * mtx(5,5) - mtx(4,5) * mtx(5,1);
	ELEM_TYPE det2_45_16 = mtx(4,1) * mtx(5,6) - mtx(4,6) * mtx(5,1);
	ELEM_TYPE det2_45_23 = mtx(4,2) * mtx(5,3) - mtx(4,3) * mtx(5,2);
	ELEM_TYPE det2_45_24 = mtx(4,2) * mtx(5,4) - mtx(4,4) * mtx(5,2);
	ELEM_TYPE det2_45_25 = mtx(4,2) * mtx(5,5) - mtx(4,5) * mtx(5,2);
	ELEM_TYPE det2_45_26 = mtx(4,2) * mtx(5,6) - mtx(4,6) * mtx(5,2);
	ELEM_TYPE det2_45_34 = mtx(4,3) * mtx(5,4) - mtx(4,4) * mtx(5,3);
	ELEM_TYPE det2_45_35 = mtx(4,3) * mtx(5,5) - mtx(4,5) * mtx(5,3);
	ELEM_TYPE det2_45_36 = mtx(4,3) * mtx(5,6) - mtx(4,6) * mtx(5,3);
	ELEM_TYPE det2_45_45 = mtx(4,4) * mtx(5,5) - mtx(4,5) * mtx(5,4);
	ELEM_TYPE det2_45_46 = mtx(4,4) * mtx(5,6) - mtx(4,6) * mtx(5,4);
	ELEM_TYPE det2_45_56 = mtx(4,5) * mtx(5,6) - mtx(4,6) * mtx(5,5);
	ELEM_TYPE det2_46_01 = mtx(4,0) * mtx(6,1) - mtx(4,1) * mtx(6,0);
	ELEM_TYPE det2_46_02 = mtx(4,0) * mtx(6,2) - mtx(4,2) * mtx(6,0);
	ELEM_TYPE det2_46_03 = mtx(4,0) * mtx(6,3) - mtx(4,3) * mtx(6,0);
	ELEM_TYPE det2_46_04 = mtx(4,0) * mtx(6,4) - mtx(4,4) * mtx(6,0);
	ELEM_TYPE det2_46_05 = mtx(4,0) * mtx(6,5) - mtx(4,5) * mtx(6,0);
	ELEM_TYPE det2_46_06 = mtx(4,0) * mtx(6,6) - mtx(4,6) * mtx(6,0);
	ELEM_TYPE det2_46_12 = mtx(4,1) * mtx(6,2) - mtx(4,2) * mtx(6,1);
	ELEM_TYPE det2_46_13 = mtx(4,1) * mtx(6,3) - mtx(4,3) * mtx(6,1);
	ELEM_TYPE det2_46_14 = mtx(4,1) * mtx(6,4) - mtx(4,4) * mtx(6,1);
	ELEM_TYPE det2_46_15 = mtx(4,1) * mtx(6,5) - mtx(4,5) * mtx(6,1);
	ELEM_TYPE det2_46_16 = mtx(4,1) * mtx(6,6) - mtx(4,6) * mtx(6,1);
	ELEM_TYPE det2_46_23 = mtx(4,2) * mtx(6,3) - mtx(4,3) * mtx(6,2);
	ELEM_TYPE det2_46_24 = mtx(4,2) * mtx(6,4) - mtx(4,4) * mtx(6,2);
	ELEM_TYPE det2_46_25 = mtx(4,2) * mtx(6,5) - mtx(4,5) * mtx(6,2);
	ELEM_TYPE det2_46_26 = mtx(4,2) * mtx(6,6) - mtx(4,6) * mtx(6,2);
	ELEM_TYPE det2_46_34 = mtx(4,3) * mtx(6,4) - mtx(4,4) * mtx(6,3);
	ELEM_TYPE det2_46_35 = mtx(4,3) * mtx(6,5) - mtx(4,5) * mtx(6,3);
	ELEM_TYPE det2_46_36 = mtx(4,3) * mtx(6,6) - mtx(4,6) * mtx(6,3);
	ELEM_TYPE det2_46_45 = mtx(4,4) * mtx(6,5) - mtx(4,5) * mtx(6,4);
	ELEM_TYPE det2_46_46 = mtx(4,4) * mtx(6,6) - mtx(4,6) * mtx(6,4);
	ELEM_TYPE det2_46_56 = mtx(4,5) * mtx(6,6) - mtx(4,6) * mtx(6,5);
	ELEM_TYPE det2_56_01 = mtx(5,0) * mtx(6,1) - mtx(5,1) * mtx(6,0);
	ELEM_TYPE det2_56_02 = mtx(5,0) * mtx(6,2) - mtx(5,2) * mtx(6,0);
	ELEM_TYPE det2_56_03 = mtx(5,0) * mtx(6,3) - mtx(5,3) * mtx(6,0);
	ELEM_TYPE det2_56_04 = mtx(5,0) * mtx(6,4) - mtx(5,4) * mtx(6,0);
	ELEM_TYPE det2_56_05 = mtx(5,0) * mtx(6,5) - mtx(5,5) * mtx(6,0);
	ELEM_TYPE det2_56_06 = mtx(5,0) * mtx(6,6) - mtx(5,6) * mtx(6,0);
	ELEM_TYPE det2_56_12 = mtx(5,1) * mtx(6,2) - mtx(5,2) * mtx(6,1);
	ELEM_TYPE det2_56_13 = mtx(5,1) * mtx(6,3) - mtx(5,3) * mtx(6,1);
	ELEM_TYPE det2_56_14 = mtx(5,1) * mtx(6,4) - mtx(5,4) * mtx(6,1);
	ELEM_TYPE det2_56_15 = mtx(5,1) * mtx(6,5) - mtx(5,5) * mtx(6,1);
	ELEM_TYPE det2_56_16 = mtx(5,1) * mtx(6,6) - mtx(5,6) * mtx(6,1);
	ELEM_TYPE det2_56_23 = mtx(5,2) * mtx(6,3) - mtx(5,3) * mtx(6,2);
	ELEM_TYPE det2_56_24 = mtx(5,2) * mtx(6,4) - mtx(5,4) * mtx(6,2);
	ELEM_TYPE det2_56_25 = mtx(5,2) * mtx(6,5) - mtx(5,5) * mtx(6,2);
	ELEM_TYPE det2_56_26 = mtx(5,2) * mtx(6,6) - mtx(5,6) * mtx(6,2);
	ELEM_TYPE det2_56_34 = mtx(5,3) * mtx(6,4) - mtx(5,4) * mtx(6,3);
	ELEM_TYPE det2_56_35 = mtx(5,3) * mtx(6,5) - mtx(5,5) * mtx(6,3);
	ELEM_TYPE det2_56_36 = mtx(5,3) * mtx(6,6) - mtx(5,6) * mtx(6,3);
	ELEM_TYPE det2_56_45 = mtx(5,4) * mtx(6,5) - mtx(5,5) * mtx(6,4);
	ELEM_TYPE det2_56_46 = mtx(5,4) * mtx(6,6) - mtx(5,6) * mtx(6,4);
	ELEM_TYPE det2_56_56 = mtx(5,5) * mtx(6,6) - mtx(5,6) * mtx(6,5);

	ELEM_TYPE det3_345_012 =  mtx(3,0) * det2_45_12 -  mtx(3,1) * det2_45_02 +  mtx(3,2) * det2_45_01;
	ELEM_TYPE det3_345_013 =  mtx(3,0) * det2_45_13 -  mtx(3,1) * det2_45_03 +  mtx(3,3) * det2_45_01;
	ELEM_TYPE det3_345_014 =  mtx(3,0) * det2_45_14 -  mtx(3,1) * det2_45_04 +  mtx(3,4) * det2_45_01;
	ELEM_TYPE det3_345_015 =  mtx(3,0) * det2_45_15 -  mtx(3,1) * det2_45_05 +  mtx(3,5) * det2_45_01;
	ELEM_TYPE det3_345_016 =  mtx(3,0) * det2_45_16 -  mtx(3,1) * det2_45_06 +  mtx(3,6) * det2_45_01;
	ELEM_TYPE det3_345_023 =  mtx(3,0) * det2_45_23 -  mtx(3,2) * det2_45_03 +  mtx(3,3) * det2_45_02;
	ELEM_TYPE det3_345_024 =  mtx(3,0) * det2_45_24 -  mtx(3,2) * det2_45_04 +  mtx(3,4) * det2_45_02;
	ELEM_TYPE det3_345_025 =  mtx(3,0) * det2_45_25 -  mtx(3,2) * det2_45_05 +  mtx(3,5) * det2_45_02;
	ELEM_TYPE det3_345_026 =  mtx(3,0) * det2_45_26 -  mtx(3,2) * det2_45_06 +  mtx(3,6) * det2_45_02;
	ELEM_TYPE det3_345_034 =  mtx(3,0) * det2_45_34 -  mtx(3,3) * det2_45_04 +  mtx(3,4) * det2_45_03;
	ELEM_TYPE det3_345_035 =  mtx(3,0) * det2_45_35 -  mtx(3,3) * det2_45_05 +  mtx(3,5) * det2_45_03;
	ELEM_TYPE det3_345_036 =  mtx(3,0) * det2_45_36 -  mtx(3,3) * det2_45_06 +  mtx(3,6) * det2_45_03;
	ELEM_TYPE det3_345_045 =  mtx(3,0) * det2_45_45 -  mtx(3,4) * det2_45_05 +  mtx(3,5) * det2_45_04;
	ELEM_TYPE det3_345_046 =  mtx(3,0) * det2_45_46 -  mtx(3,4) * det2_45_06 +  mtx(3,6) * det2_45_04;
	ELEM_TYPE det3_345_056 =  mtx(3,0) * det2_45_56 -  mtx(3,5) * det2_45_06 +  mtx(3,6) * det2_45_05;
	ELEM_TYPE det3_345_123 =  mtx(3,1) * det2_45_23 -  mtx(3,2) * det2_45_13 +  mtx(3,3) * det2_45_12;
	ELEM_TYPE det3_345_124 =  mtx(3,1) * det2_45_24 -  mtx(3,2) * det2_45_14 +  mtx(3,4) * det2_45_12;
	ELEM_TYPE det3_345_125 =  mtx(3,1) * det2_45_25 -  mtx(3,2) * det2_45_15 +  mtx(3,5) * det2_45_12;
	ELEM_TYPE det3_345_126 =  mtx(3,1) * det2_45_26 -  mtx(3,2) * det2_45_16 +  mtx(3,6) * det2_45_12;
	ELEM_TYPE det3_345_134 =  mtx(3,1) * det2_45_34 -  mtx(3,3) * det2_45_14 +  mtx(3,4) * det2_45_13;
	ELEM_TYPE det3_345_135 =  mtx(3,1) * det2_45_35 -  mtx(3,3) * det2_45_15 +  mtx(3,5) * det2_45_13;
	ELEM_TYPE det3_345_136 =  mtx(3,1) * det2_45_36 -  mtx(3,3) * det2_45_16 +  mtx(3,6) * det2_45_13;
	ELEM_TYPE det3_345_145 =  mtx(3,1) * det2_45_45 -  mtx(3,4) * det2_45_15 +  mtx(3,5) * det2_45_14;
	ELEM_TYPE det3_345_146 =  mtx(3,1) * det2_45_46 -  mtx(3,4) * det2_45_16 +  mtx(3,6) * det2_45_14;
	ELEM_TYPE det3_345_156 =  mtx(3,1) * det2_45_56 -  mtx(3,5) * det2_45_16 +  mtx(3,6) * det2_45_15;
	ELEM_TYPE det3_345_234 =  mtx(3,2) * det2_45_34 -  mtx(3,3) * det2_45_24 +  mtx(3,4) * det2_45_23;
	ELEM_TYPE det3_345_235 =  mtx(3,2) * det2_45_35 -  mtx(3,3) * det2_45_25 +  mtx(3,5) * det2_45_23;
	ELEM_TYPE det3_345_236 =  mtx(3,2) * det2_45_36 -  mtx(3,3) * det2_45_26 +  mtx(3,6) * det2_45_23;
	ELEM_TYPE det3_345_245 =  mtx(3,2) * det2_45_45 -  mtx(3,4) * det2_45_25 +  mtx(3,5) * det2_45_24;
	ELEM_TYPE det3_345_246 =  mtx(3,2) * det2_45_46 -  mtx(3,4) * det2_45_26 +  mtx(3,6) * det2_45_24;
	ELEM_TYPE det3_345_256 =  mtx(3,2) * det2_45_56 -  mtx(3,5) * det2_45_26 +  mtx(3,6) * det2_45_25;
	ELEM_TYPE det3_345_345 =  mtx(3,3) * det2_45_45 -  mtx(3,4) * det2_45_35 +  mtx(3,5) * det2_45_34;
	ELEM_TYPE det3_345_346 =  mtx(3,3) * det2_45_46 -  mtx(3,4) * det2_45_36 +  mtx(3,6) * det2_45_34;
	ELEM_TYPE det3_345_356 =  mtx(3,3) * det2_45_56 -  mtx(3,5) * det2_45_36 +  mtx(3,6) * det2_45_35;
	ELEM_TYPE det3_345_456 =  mtx(3,4) * det2_45_56 -  mtx(3,5) * det2_45_46 +  mtx(3,6) * det2_45_45;
	ELEM_TYPE det3_346_012 =  mtx(3,0) * det2_46_12 -  mtx(3,1) * det2_46_02 +  mtx(3,2) * det2_46_01;
	ELEM_TYPE det3_346_013 =  mtx(3,0) * det2_46_13 -  mtx(3,1) * det2_46_03 +  mtx(3,3) * det2_46_01;
	ELEM_TYPE det3_346_014 =  mtx(3,0) * det2_46_14 -  mtx(3,1) * det2_46_04 +  mtx(3,4) * det2_46_01;
	ELEM_TYPE det3_346_015 =  mtx(3,0) * det2_46_15 -  mtx(3,1) * det2_46_05 +  mtx(3,5) * det2_46_01;
	ELEM_TYPE det3_346_016 =  mtx(3,0) * det2_46_16 -  mtx(3,1) * det2_46_06 +  mtx(3,6) * det2_46_01;
	ELEM_TYPE det3_346_023 =  mtx(3,0) * det2_46_23 -  mtx(3,2) * det2_46_03 +  mtx(3,3) * det2_46_02;
	ELEM_TYPE det3_346_024 =  mtx(3,0) * det2_46_24 -  mtx(3,2) * det2_46_04 +  mtx(3,4) * det2_46_02;
	ELEM_TYPE det3_346_025 =  mtx(3,0) * det2_46_25 -  mtx(3,2) * det2_46_05 +  mtx(3,5) * det2_46_02;
	ELEM_TYPE det3_346_026 =  mtx(3,0) * det2_46_26 -  mtx(3,2) * det2_46_06 +  mtx(3,6) * det2_46_02;
	ELEM_TYPE det3_346_034 =  mtx(3,0) * det2_46_34 -  mtx(3,3) * det2_46_04 +  mtx(3,4) * det2_46_03;
	ELEM_TYPE det3_346_035 =  mtx(3,0) * det2_46_35 -  mtx(3,3) * det2_46_05 +  mtx(3,5) * det2_46_03;
	ELEM_TYPE det3_346_036 =  mtx(3,0) * det2_46_36 -  mtx(3,3) * det2_46_06 +  mtx(3,6) * det2_46_03;
	ELEM_TYPE det3_346_045 =  mtx(3,0) * det2_46_45 -  mtx(3,4) * det2_46_05 +  mtx(3,5) * det2_46_04;
	ELEM_TYPE det3_346_046 =  mtx(3,0) * det2_46_46 -  mtx(3,4) * det2_46_06 +  mtx(3,6) * det2_46_04;
	ELEM_TYPE det3_346_056 =  mtx(3,0) * det2_46_56 -  mtx(3,5) * det2_46_06 +  mtx(3,6) * det2_46_05;
	ELEM_TYPE det3_346_123 =  mtx(3,1) * det2_46_23 -  mtx(3,2) * det2_46_13 +  mtx(3,3) * det2_46_12;
	ELEM_TYPE det3_346_124 =  mtx(3,1) * det2_46_24 -  mtx(3,2) * det2_46_14 +  mtx(3,4) * det2_46_12;
	ELEM_TYPE det3_346_125 =  mtx(3,1) * det2_46_25 -  mtx(3,2) * det2_46_15 +  mtx(3,5) * det2_46_12;
	ELEM_TYPE det3_346_126 =  mtx(3,1) * det2_46_26 -  mtx(3,2) * det2_46_16 +  mtx(3,6) * det2_46_12;
	ELEM_TYPE det3_346_134 =  mtx(3,1) * det2_46_34 -  mtx(3,3) * det2_46_14 +  mtx(3,4) * det2_46_13;
	ELEM_TYPE det3_346_135 =  mtx(3,1) * det2_46_35 -  mtx(3,3) * det2_46_15 +  mtx(3,5) * det2_46_13;
	ELEM_TYPE det3_346_136 =  mtx(3,1) * det2_46_36 -  mtx(3,3) * det2_46_16 +  mtx(3,6) * det2_46_13;
	ELEM_TYPE det3_346_145 =  mtx(3,1) * det2_46_45 -  mtx(3,4) * det2_46_15 +  mtx(3,5) * det2_46_14;
	ELEM_TYPE det3_346_146 =  mtx(3,1) * det2_46_46 -  mtx(3,4) * det2_46_16 +  mtx(3,6) * det2_46_14;
	ELEM_TYPE det3_346_156 =  mtx(3,1) * det2_46_56 -  mtx(3,5) * det2_46_16 +  mtx(3,6) * det2_46_15;
	ELEM_TYPE det3_346_234 =  mtx(3,2) * det2_46_34 -  mtx(3,3) * det2_46_24 +  mtx(3,4) * det2_46_23;
	ELEM_TYPE det3_346_235 =  mtx(3,2) * det2_46_35 -  mtx(3,3) * det2_46_25 +  mtx(3,5) * det2_46_23;
	ELEM_TYPE det3_346_236 =  mtx(3,2) * det2_46_36 -  mtx(3,3) * det2_46_26 +  mtx(3,6) * det2_46_23;
	ELEM_TYPE det3_346_245 =  mtx(3,2) * det2_46_45 -  mtx(3,4) * det2_46_25 +  mtx(3,5) * det2_46_24;
	ELEM_TYPE det3_346_246 =  mtx(3,2) * det2_46_46 -  mtx(3,4) * det2_46_26 +  mtx(3,6) * det2_46_24;
	ELEM_TYPE det3_346_256 =  mtx(3,2) * det2_46_56 -  mtx(3,5) * det2_46_26 +  mtx(3,6) * det2_46_25;
	ELEM_TYPE det3_346_345 =  mtx(3,3) * det2_46_45 -  mtx(3,4) * det2_46_35 +  mtx(3,5) * det2_46_34;
	ELEM_TYPE det3_346_346 =  mtx(3,3) * det2_46_46 -  mtx(3,4) * det2_46_36 +  mtx(3,6) * det2_46_34;
	ELEM_TYPE det3_346_356 =  mtx(3,3) * det2_46_56 -  mtx(3,5) * det2_46_36 +  mtx(3,6) * det2_46_35;
	ELEM_TYPE det3_346_456 =  mtx(3,4) * det2_46_56 -  mtx(3,5) * det2_46_46 +  mtx(3,6) * det2_46_45;
	ELEM_TYPE det3_356_012 =  mtx(3,0) * det2_56_12 -  mtx(3,1) * det2_56_02 +  mtx(3,2) * det2_56_01;
	ELEM_TYPE det3_356_013 =  mtx(3,0) * det2_56_13 -  mtx(3,1) * det2_56_03 +  mtx(3,3) * det2_56_01;
	ELEM_TYPE det3_356_014 =  mtx(3,0) * det2_56_14 -  mtx(3,1) * det2_56_04 +  mtx(3,4) * det2_56_01;
	ELEM_TYPE det3_356_015 =  mtx(3,0) * det2_56_15 -  mtx(3,1) * det2_56_05 +  mtx(3,5) * det2_56_01;
	ELEM_TYPE det3_356_016 =  mtx(3,0) * det2_56_16 -  mtx(3,1) * det2_56_06 +  mtx(3,6) * det2_56_01;
	ELEM_TYPE det3_356_023 =  mtx(3,0) * det2_56_23 -  mtx(3,2) * det2_56_03 +  mtx(3,3) * det2_56_02;
	ELEM_TYPE det3_356_024 =  mtx(3,0) * det2_56_24 -  mtx(3,2) * det2_56_04 +  mtx(3,4) * det2_56_02;
	ELEM_TYPE det3_356_025 =  mtx(3,0) * det2_56_25 -  mtx(3,2) * det2_56_05 +  mtx(3,5) * det2_56_02;
	ELEM_TYPE det3_356_026 =  mtx(3,0) * det2_56_26 -  mtx(3,2) * det2_56_06 +  mtx(3,6) * det2_56_02;
	ELEM_TYPE det3_356_034 =  mtx(3,0) * det2_56_34 -  mtx(3,3) * det2_56_04 +  mtx(3,4) * det2_56_03;
	ELEM_TYPE det3_356_035 =  mtx(3,0) * det2_56_35 -  mtx(3,3) * det2_56_05 +  mtx(3,5) * det2_56_03;
	ELEM_TYPE det3_356_036 =  mtx(3,0) * det2_56_36 -  mtx(3,3) * det2_56_06 +  mtx(3,6) * det2_56_03;
	ELEM_TYPE det3_356_045 =  mtx(3,0) * det2_56_45 -  mtx(3,4) * det2_56_05 +  mtx(3,5) * det2_56_04;
	ELEM_TYPE det3_356_046 =  mtx(3,0) * det2_56_46 -  mtx(3,4) * det2_56_06 +  mtx(3,6) * det2_56_04;
	ELEM_TYPE det3_356_056 =  mtx(3,0) * det2_56_56 -  mtx(3,5) * det2_56_06 +  mtx(3,6) * det2_56_05;
	ELEM_TYPE det3_356_123 =  mtx(3,1) * det2_56_23 -  mtx(3,2) * det2_56_13 +  mtx(3,3) * det2_56_12;
	ELEM_TYPE det3_356_124 =  mtx(3,1) * det2_56_24 -  mtx(3,2) * det2_56_14 +  mtx(3,4) * det2_56_12;
	ELEM_TYPE det3_356_125 =  mtx(3,1) * det2_56_25 -  mtx(3,2) * det2_56_15 +  mtx(3,5) * det2_56_12;
	ELEM_TYPE det3_356_126 =  mtx(3,1) * det2_56_26 -  mtx(3,2) * det2_56_16 +  mtx(3,6) * det2_56_12;
	ELEM_TYPE det3_356_134 =  mtx(3,1) * det2_56_34 -  mtx(3,3) * det2_56_14 +  mtx(3,4) * det2_56_13;
	ELEM_TYPE det3_356_135 =  mtx(3,1) * det2_56_35 -  mtx(3,3) * det2_56_15 +  mtx(3,5) * det2_56_13;
	ELEM_TYPE det3_356_136 =  mtx(3,1) * det2_56_36 -  mtx(3,3) * det2_56_16 +  mtx(3,6) * det2_56_13;
	ELEM_TYPE det3_356_145 =  mtx(3,1) * det2_56_45 -  mtx(3,4) * det2_56_15 +  mtx(3,5) * det2_56_14;
	ELEM_TYPE det3_356_146 =  mtx(3,1) * det2_56_46 -  mtx(3,4) * det2_56_16 +  mtx(3,6) * det2_56_14;
	ELEM_TYPE det3_356_156 =  mtx(3,1) * det2_56_56 -  mtx(3,5) * det2_56_16 +  mtx(3,6) * det2_56_15;
	ELEM_TYPE det3_356_234 =  mtx(3,2) * det2_56_34 -  mtx(3,3) * det2_56_24 +  mtx(3,4) * det2_56_23;
	ELEM_TYPE det3_356_235 =  mtx(3,2) * det2_56_35 -  mtx(3,3) * det2_56_25 +  mtx(3,5) * det2_56_23;
	ELEM_TYPE det3_356_236 =  mtx(3,2) * det2_56_36 -  mtx(3,3) * det2_56_26 +  mtx(3,6) * det2_56_23;
	ELEM_TYPE det3_356_245 =  mtx(3,2) * det2_56_45 -  mtx(3,4) * det2_56_25 +  mtx(3,5) * det2_56_24;
	ELEM_TYPE det3_356_246 =  mtx(3,2) * det2_56_46 -  mtx(3,4) * det2_56_26 +  mtx(3,6) * det2_56_24;
	ELEM_TYPE det3_356_256 =  mtx(3,2) * det2_56_56 -  mtx(3,5) * det2_56_26 +  mtx(3,6) * det2_56_25;
	ELEM_TYPE det3_356_345 =  mtx(3,3) * det2_56_45 -  mtx(3,4) * det2_56_35 +  mtx(3,5) * det2_56_34;
	ELEM_TYPE det3_356_346 =  mtx(3,3) * det2_56_46 -  mtx(3,4) * det2_56_36 +  mtx(3,6) * det2_56_34;
	ELEM_TYPE det3_356_356 =  mtx(3,3) * det2_56_56 -  mtx(3,5) * det2_56_36 +  mtx(3,6) * det2_56_35;
	ELEM_TYPE det3_356_456 =  mtx(3,4) * det2_56_56 -  mtx(3,5) * det2_56_46 +  mtx(3,6) * det2_56_45;
	ELEM_TYPE det3_456_012 =  mtx(4,0) * det2_56_12 -  mtx(4,1) * det2_56_02 +  mtx(4,2) * det2_56_01;
	ELEM_TYPE det3_456_013 =  mtx(4,0) * det2_56_13 -  mtx(4,1) * det2_56_03 +  mtx(4,3) * det2_56_01;
	ELEM_TYPE det3_456_014 =  mtx(4,0) * det2_56_14 -  mtx(4,1) * det2_56_04 +  mtx(4,4) * det2_56_01;
	ELEM_TYPE det3_456_015 =  mtx(4,0) * det2_56_15 -  mtx(4,1) * det2_56_05 +  mtx(4,5) * det2_56_01;
	ELEM_TYPE det3_456_016 =  mtx(4,0) * det2_56_16 -  mtx(4,1) * det2_56_06 +  mtx(4,6) * det2_56_01;
	ELEM_TYPE det3_456_023 =  mtx(4,0) * det2_56_23 -  mtx(4,2) * det2_56_03 +  mtx(4,3) * det2_56_02;
	ELEM_TYPE det3_456_024 =  mtx(4,0) * det2_56_24 -  mtx(4,2) * det2_56_04 +  mtx(4,4) * det2_56_02;
	ELEM_TYPE det3_456_025 =  mtx(4,0) * det2_56_25 -  mtx(4,2) * det2_56_05 +  mtx(4,5) * det2_56_02;
	ELEM_TYPE det3_456_026 =  mtx(4,0) * det2_56_26 -  mtx(4,2) * det2_56_06 +  mtx(4,6) * det2_56_02;
	ELEM_TYPE det3_456_034 =  mtx(4,0) * det2_56_34 -  mtx(4,3) * det2_56_04 +  mtx(4,4) * det2_56_03;
	ELEM_TYPE det3_456_035 =  mtx(4,0) * det2_56_35 -  mtx(4,3) * det2_56_05 +  mtx(4,5) * det2_56_03;
	ELEM_TYPE det3_456_036 =  mtx(4,0) * det2_56_36 -  mtx(4,3) * det2_56_06 +  mtx(4,6) * det2_56_03;
	ELEM_TYPE det3_456_045 =  mtx(4,0) * det2_56_45 -  mtx(4,4) * det2_56_05 +  mtx(4,5) * det2_56_04;
	ELEM_TYPE det3_456_046 =  mtx(4,0) * det2_56_46 -  mtx(4,4) * det2_56_06 +  mtx(4,6) * det2_56_04;
	ELEM_TYPE det3_456_056 =  mtx(4,0) * det2_56_56 -  mtx(4,5) * det2_56_06 +  mtx(4,6) * det2_56_05;
	ELEM_TYPE det3_456_123 =  mtx(4,1) * det2_56_23 -  mtx(4,2) * det2_56_13 +  mtx(4,3) * det2_56_12;
	ELEM_TYPE det3_456_124 =  mtx(4,1) * det2_56_24 -  mtx(4,2) * det2_56_14 +  mtx(4,4) * det2_56_12;
	ELEM_TYPE det3_456_125 =  mtx(4,1) * det2_56_25 -  mtx(4,2) * det2_56_15 +  mtx(4,5) * det2_56_12;
	ELEM_TYPE det3_456_126 =  mtx(4,1) * det2_56_26 -  mtx(4,2) * det2_56_16 +  mtx(4,6) * det2_56_12;
	ELEM_TYPE det3_456_134 =  mtx(4,1) * det2_56_34 -  mtx(4,3) * det2_56_14 +  mtx(4,4) * det2_56_13;
	ELEM_TYPE det3_456_135 =  mtx(4,1) * det2_56_35 -  mtx(4,3) * det2_56_15 +  mtx(4,5) * det2_56_13;
	ELEM_TYPE det3_456_136 =  mtx(4,1) * det2_56_36 -  mtx(4,3) * det2_56_16 +  mtx(4,6) * det2_56_13;
	ELEM_TYPE det3_456_145 =  mtx(4,1) * det2_56_45 -  mtx(4,4) * det2_56_15 +  mtx(4,5) * det2_56_14;
	ELEM_TYPE det3_456_146 =  mtx(4,1) * det2_56_46 -  mtx(4,4) * det2_56_16 +  mtx(4,6) * det2_56_14;
	ELEM_TYPE det3_456_156 =  mtx(4,1) * det2_56_56 -  mtx(4,5) * det2_56_16 +  mtx(4,6) * det2_56_15;
	ELEM_TYPE det3_456_234 =  mtx(4,2) * det2_56_34 -  mtx(4,3) * det2_56_24 +  mtx(4,4) * det2_56_23;
	ELEM_TYPE det3_456_235 =  mtx(4,2) * det2_56_35 -  mtx(4,3) * det2_56_25 +  mtx(4,5) * det2_56_23;
	ELEM_TYPE det3_456_236 =  mtx(4,2) * det2_56_36 -  mtx(4,3) * det2_56_26 +  mtx(4,6) * det2_56_23;
	ELEM_TYPE det3_456_245 =  mtx(4,2) * det2_56_45 -  mtx(4,4) * det2_56_25 +  mtx(4,5) * det2_56_24;
	ELEM_TYPE det3_456_246 =  mtx(4,2) * det2_56_46 -  mtx(4,4) * det2_56_26 +  mtx(4,6) * det2_56_24;
	ELEM_TYPE det3_456_256 =  mtx(4,2) * det2_56_56 -  mtx(4,5) * det2_56_26 +  mtx(4,6) * det2_56_25;
	ELEM_TYPE det3_456_345 =  mtx(4,3) * det2_56_45 -  mtx(4,4) * det2_56_35 +  mtx(4,5) * det2_56_34;
	ELEM_TYPE det3_456_346 =  mtx(4,3) * det2_56_46 -  mtx(4,4) * det2_56_36 +  mtx(4,6) * det2_56_34;
	ELEM_TYPE det3_456_356 =  mtx(4,3) * det2_56_56 -  mtx(4,5) * det2_56_36 +  mtx(4,6) * det2_56_35;
	ELEM_TYPE det3_456_456 =  mtx(4,4) * det2_56_56 -  mtx(4,5) * det2_56_46 +  mtx(4,6) * det2_56_45;

	ELEM_TYPE det4_2345_0123 =  mtx(2,0) * det3_345_123 -  mtx(2,1) * det3_345_023 +  mtx(2,2) * det3_345_013 -  mtx(2,3) * det3_345_012;
	ELEM_TYPE det4_2345_0124 =  mtx(2,0) * det3_345_124 -  mtx(2,1) * det3_345_024 +  mtx(2,2) * det3_345_014 -  mtx(2,4) * det3_345_012;
	ELEM_TYPE det4_2345_0125 =  mtx(2,0) * det3_345_125 -  mtx(2,1) * det3_345_025 +  mtx(2,2) * det3_345_015 -  mtx(2,5) * det3_345_012;
	ELEM_TYPE det4_2345_0126 =  mtx(2,0) * det3_345_126 -  mtx(2,1) * det3_345_026 +  mtx(2,2) * det3_345_016 -  mtx(2,6) * det3_345_012;
	ELEM_TYPE det4_2345_0134 =  mtx(2,0) * det3_345_134 -  mtx(2,1) * det3_345_034 +  mtx(2,3) * det3_345_014 -  mtx(2,4) * det3_345_013;
	ELEM_TYPE det4_2345_0135 =  mtx(2,0) * det3_345_135 -  mtx(2,1) * det3_345_035 +  mtx(2,3) * det3_345_015 -  mtx(2,5) * det3_345_013;
	ELEM_TYPE det4_2345_0136 =  mtx(2,0) * det3_345_136 -  mtx(2,1) * det3_345_036 +  mtx(2,3) * det3_345_016 -  mtx(2,6) * det3_345_013;
	ELEM_TYPE det4_2345_0145 =  mtx(2,0) * det3_345_145 -  mtx(2,1) * det3_345_045 +  mtx(2,4) * det3_345_015 -  mtx(2,5) * det3_345_014;
	ELEM_TYPE det4_2345_0146 =  mtx(2,0) * det3_345_146 -  mtx(2,1) * det3_345_046 +  mtx(2,4) * det3_345_016 -  mtx(2,6) * det3_345_014;
	ELEM_TYPE det4_2345_0156 =  mtx(2,0) * det3_345_156 -  mtx(2,1) * det3_345_056 +  mtx(2,5) * det3_345_016 -  mtx(2,6) * det3_345_015;
	ELEM_TYPE det4_2345_0234 =  mtx(2,0) * det3_345_234 -  mtx(2,2) * det3_345_034 +  mtx(2,3) * det3_345_024 -  mtx(2,4) * det3_345_023;
	ELEM_TYPE det4_2345_0235 =  mtx(2,0) * det3_345_235 -  mtx(2,2) * det3_345_035 +  mtx(2,3) * det3_345_025 -  mtx(2,5) * det3_345_023;
	ELEM_TYPE det4_2345_0236 =  mtx(2,0) * det3_345_236 -  mtx(2,2) * det3_345_036 +  mtx(2,3) * det3_345_026 -  mtx(2,6) * det3_345_023;
	ELEM_TYPE det4_2345_0245 =  mtx(2,0) * det3_345_245 -  mtx(2,2) * det3_345_045 +  mtx(2,4) * det3_345_025 -  mtx(2,5) * det3_345_024;
	ELEM_TYPE det4_2345_0246 =  mtx(2,0) * det3_345_246 -  mtx(2,2) * det3_345_046 +  mtx(2,4) * det3_345_026 -  mtx(2,6) * det3_345_024;
	ELEM_TYPE det4_2345_0256 =  mtx(2,0) * det3_345_256 -  mtx(2,2) * det3_345_056 +  mtx(2,5) * det3_345_026 -  mtx(2,6) * det3_345_025;
	ELEM_TYPE det4_2345_0345 =  mtx(2,0) * det3_345_345 -  mtx(2,3) * det3_345_045 +  mtx(2,4) * det3_345_035 -  mtx(2,5) * det3_345_034;
	ELEM_TYPE det4_2345_0346 =  mtx(2,0) * det3_345_346 -  mtx(2,3) * det3_345_046 +  mtx(2,4) * det3_345_036 -  mtx(2,6) * det3_345_034;
	ELEM_TYPE det4_2345_0356 =  mtx(2,0) * det3_345_356 -  mtx(2,3) * det3_345_056 +  mtx(2,5) * det3_345_036 -  mtx(2,6) * det3_345_035;
	ELEM_TYPE det4_2345_0456 =  mtx(2,0) * det3_345_456 -  mtx(2,4) * det3_345_056 +  mtx(2,5) * det3_345_046 -  mtx(2,6) * det3_345_045;
	ELEM_TYPE det4_2345_1234 =  mtx(2,1) * det3_345_234 -  mtx(2,2) * det3_345_134 +  mtx(2,3) * det3_345_124 -  mtx(2,4) * det3_345_123;
	ELEM_TYPE det4_2345_1235 =  mtx(2,1) * det3_345_235 -  mtx(2,2) * det3_345_135 +  mtx(2,3) * det3_345_125 -  mtx(2,5) * det3_345_123;
	ELEM_TYPE det4_2345_1236 =  mtx(2,1) * det3_345_236 -  mtx(2,2) * det3_345_136 +  mtx(2,3) * det3_345_126 -  mtx(2,6) * det3_345_123;
	ELEM_TYPE det4_2345_1245 =  mtx(2,1) * det3_345_245 -  mtx(2,2) * det3_345_145 +  mtx(2,4) * det3_345_125 -  mtx(2,5) * det3_345_124;
	ELEM_TYPE det4_2345_1246 =  mtx(2,1) * det3_345_246 -  mtx(2,2) * det3_345_146 +  mtx(2,4) * det3_345_126 -  mtx(2,6) * det3_345_124;
	ELEM_TYPE det4_2345_1256 =  mtx(2,1) * det3_345_256 -  mtx(2,2) * det3_345_156 +  mtx(2,5) * det3_345_126 -  mtx(2,6) * det3_345_125;
	ELEM_TYPE det4_2345_1345 =  mtx(2,1) * det3_345_345 -  mtx(2,3) * det3_345_145 +  mtx(2,4) * det3_345_135 -  mtx(2,5) * det3_345_134;
	ELEM_TYPE det4_2345_1346 =  mtx(2,1) * det3_345_346 -  mtx(2,3) * det3_345_146 +  mtx(2,4) * det3_345_136 -  mtx(2,6) * det3_345_134;
	ELEM_TYPE det4_2345_1356 =  mtx(2,1) * det3_345_356 -  mtx(2,3) * det3_345_156 +  mtx(2,5) * det3_345_136 -  mtx(2,6) * det3_345_135;
	ELEM_TYPE det4_2345_1456 =  mtx(2,1) * det3_345_456 -  mtx(2,4) * det3_345_156 +  mtx(2,5) * det3_345_146 -  mtx(2,6) * det3_345_145;
	ELEM_TYPE det4_2345_2345 =  mtx(2,2) * det3_345_345 -  mtx(2,3) * det3_345_245 +  mtx(2,4) * det3_345_235 -  mtx(2,5) * det3_345_234;
	ELEM_TYPE det4_2345_2346 =  mtx(2,2) * det3_345_346 -  mtx(2,3) * det3_345_246 +  mtx(2,4) * det3_345_236 -  mtx(2,6) * det3_345_234;
	ELEM_TYPE det4_2345_2356 =  mtx(2,2) * det3_345_356 -  mtx(2,3) * det3_345_256 +  mtx(2,5) * det3_345_236 -  mtx(2,6) * det3_345_235;
	ELEM_TYPE det4_2345_2456 =  mtx(2,2) * det3_345_456 -  mtx(2,4) * det3_345_256 +  mtx(2,5) * det3_345_246 -  mtx(2,6) * det3_345_245;
	ELEM_TYPE det4_2345_3456 =  mtx(2,3) * det3_345_456 -  mtx(2,4) * det3_345_356 +  mtx(2,5) * det3_345_346 -  mtx(2,6) * det3_345_345;
	ELEM_TYPE det4_2346_0123 =  mtx(2,0) * det3_346_123 -  mtx(2,1) * det3_346_023 +  mtx(2,2) * det3_346_013 -  mtx(2,3) * det3_346_012;
	ELEM_TYPE det4_2346_0124 =  mtx(2,0) * det3_346_124 -  mtx(2,1) * det3_346_024 +  mtx(2,2) * det3_346_014 -  mtx(2,4) * det3_346_012;
	ELEM_TYPE det4_2346_0125 =  mtx(2,0) * det3_346_125 -  mtx(2,1) * det3_346_025 +  mtx(2,2) * det3_346_015 -  mtx(2,5) * det3_346_012;
	ELEM_TYPE det4_2346_0126 =  mtx(2,0) * det3_346_126 -  mtx(2,1) * det3_346_026 +  mtx(2,2) * det3_346_016 -  mtx(2,6) * det3_346_012;
	ELEM_TYPE det4_2346_0134 =  mtx(2,0) * det3_346_134 -  mtx(2,1) * det3_346_034 +  mtx(2,3) * det3_346_014 -  mtx(2,4) * det3_346_013;
	ELEM_TYPE det4_2346_0135 =  mtx(2,0) * det3_346_135 -  mtx(2,1) * det3_346_035 +  mtx(2,3) * det3_346_015 -  mtx(2,5) * det3_346_013;
	ELEM_TYPE det4_2346_0136 =  mtx(2,0) * det3_346_136 -  mtx(2,1) * det3_346_036 +  mtx(2,3) * det3_346_016 -  mtx(2,6) * det3_346_013;
	ELEM_TYPE det4_2346_0145 =  mtx(2,0) * det3_346_145 -  mtx(2,1) * det3_346_045 +  mtx(2,4) * det3_346_015 -  mtx(2,5) * det3_346_014;
	ELEM_TYPE det4_2346_0146 =  mtx(2,0) * det3_346_146 -  mtx(2,1) * det3_346_046 +  mtx(2,4) * det3_346_016 -  mtx(2,6) * det3_346_014;
	ELEM_TYPE det4_2346_0156 =  mtx(2,0) * det3_346_156 -  mtx(2,1) * det3_346_056 +  mtx(2,5) * det3_346_016 -  mtx(2,6) * det3_346_015;
	ELEM_TYPE det4_2346_0234 =  mtx(2,0) * det3_346_234 -  mtx(2,2) * det3_346_034 +  mtx(2,3) * det3_346_024 -  mtx(2,4) * det3_346_023;
	ELEM_TYPE det4_2346_0235 =  mtx(2,0) * det3_346_235 -  mtx(2,2) * det3_346_035 +  mtx(2,3) * det3_346_025 -  mtx(2,5) * det3_346_023;
	ELEM_TYPE det4_2346_0236 =  mtx(2,0) * det3_346_236 -  mtx(2,2) * det3_346_036 +  mtx(2,3) * det3_346_026 -  mtx(2,6) * det3_346_023;
	ELEM_TYPE det4_2346_0245 =  mtx(2,0) * det3_346_245 -  mtx(2,2) * det3_346_045 +  mtx(2,4) * det3_346_025 -  mtx(2,5) * det3_346_024;
	ELEM_TYPE det4_2346_0246 =  mtx(2,0) * det3_346_246 -  mtx(2,2) * det3_346_046 +  mtx(2,4) * det3_346_026 -  mtx(2,6) * det3_346_024;
	ELEM_TYPE det4_2346_0256 =  mtx(2,0) * det3_346_256 -  mtx(2,2) * det3_346_056 +  mtx(2,5) * det3_346_026 -  mtx(2,6) * det3_346_025;
	ELEM_TYPE det4_2346_0345 =  mtx(2,0) * det3_346_345 -  mtx(2,3) * det3_346_045 +  mtx(2,4) * det3_346_035 -  mtx(2,5) * det3_346_034;
	ELEM_TYPE det4_2346_0346 =  mtx(2,0) * det3_346_346 -  mtx(2,3) * det3_346_046 +  mtx(2,4) * det3_346_036 -  mtx(2,6) * det3_346_034;
	ELEM_TYPE det4_2346_0356 =  mtx(2,0) * det3_346_356 -  mtx(2,3) * det3_346_056 +  mtx(2,5) * det3_346_036 -  mtx(2,6) * det3_346_035;
	ELEM_TYPE det4_2346_0456 =  mtx(2,0) * det3_346_456 -  mtx(2,4) * det3_346_056 +  mtx(2,5) * det3_346_046 -  mtx(2,6) * det3_346_045;
	ELEM_TYPE det4_2346_1234 =  mtx(2,1) * det3_346_234 -  mtx(2,2) * det3_346_134 +  mtx(2,3) * det3_346_124 -  mtx(2,4) * det3_346_123;
	ELEM_TYPE det4_2346_1235 =  mtx(2,1) * det3_346_235 -  mtx(2,2) * det3_346_135 +  mtx(2,3) * det3_346_125 -  mtx(2,5) * det3_346_123;
	ELEM_TYPE det4_2346_1236 =  mtx(2,1) * det3_346_236 -  mtx(2,2) * det3_346_136 +  mtx(2,3) * det3_346_126 -  mtx(2,6) * det3_346_123;
	ELEM_TYPE det4_2346_1245 =  mtx(2,1) * det3_346_245 -  mtx(2,2) * det3_346_145 +  mtx(2,4) * det3_346_125 -  mtx(2,5) * det3_346_124;
	ELEM_TYPE det4_2346_1246 =  mtx(2,1) * det3_346_246 -  mtx(2,2) * det3_346_146 +  mtx(2,4) * det3_346_126 -  mtx(2,6) * det3_346_124;
	ELEM_TYPE det4_2346_1256 =  mtx(2,1) * det3_346_256 -  mtx(2,2) * det3_346_156 +  mtx(2,5) * det3_346_126 -  mtx(2,6) * det3_346_125;
	ELEM_TYPE det4_2346_1345 =  mtx(2,1) * det3_346_345 -  mtx(2,3) * det3_346_145 +  mtx(2,4) * det3_346_135 -  mtx(2,5) * det3_346_134;
	ELEM_TYPE det4_2346_1346 =  mtx(2,1) * det3_346_346 -  mtx(2,3) * det3_346_146 +  mtx(2,4) * det3_346_136 -  mtx(2,6) * det3_346_134;
	ELEM_TYPE det4_2346_1356 =  mtx(2,1) * det3_346_356 -  mtx(2,3) * det3_346_156 +  mtx(2,5) * det3_346_136 -  mtx(2,6) * det3_346_135;
	ELEM_TYPE det4_2346_1456 =  mtx(2,1) * det3_346_456 -  mtx(2,4) * det3_346_156 +  mtx(2,5) * det3_346_146 -  mtx(2,6) * det3_346_145;
	ELEM_TYPE det4_2346_2345 =  mtx(2,2) * det3_346_345 -  mtx(2,3) * det3_346_245 +  mtx(2,4) * det3_346_235 -  mtx(2,5) * det3_346_234;
	ELEM_TYPE det4_2346_2346 =  mtx(2,2) * det3_346_346 -  mtx(2,3) * det3_346_246 +  mtx(2,4) * det3_346_236 -  mtx(2,6) * det3_346_234;
	ELEM_TYPE det4_2346_2356 =  mtx(2,2) * det3_346_356 -  mtx(2,3) * det3_346_256 +  mtx(2,5) * det3_346_236 -  mtx(2,6) * det3_346_235;
	ELEM_TYPE det4_2346_2456 =  mtx(2,2) * det3_346_456 -  mtx(2,4) * det3_346_256 +  mtx(2,5) * det3_346_246 -  mtx(2,6) * det3_346_245;
	ELEM_TYPE det4_2346_3456 =  mtx(2,3) * det3_346_456 -  mtx(2,4) * det3_346_356 +  mtx(2,5) * det3_346_346 -  mtx(2,6) * det3_346_345;
	ELEM_TYPE det4_2356_0123 =  mtx(2,0) * det3_356_123 -  mtx(2,1) * det3_356_023 +  mtx(2,2) * det3_356_013 -  mtx(2,3) * det3_356_012;
	ELEM_TYPE det4_2356_0124 =  mtx(2,0) * det3_356_124 -  mtx(2,1) * det3_356_024 +  mtx(2,2) * det3_356_014 -  mtx(2,4) * det3_356_012;
	ELEM_TYPE det4_2356_0125 =  mtx(2,0) * det3_356_125 -  mtx(2,1) * det3_356_025 +  mtx(2,2) * det3_356_015 -  mtx(2,5) * det3_356_012;
	ELEM_TYPE det4_2356_0126 =  mtx(2,0) * det3_356_126 -  mtx(2,1) * det3_356_026 +  mtx(2,2) * det3_356_016 -  mtx(2,6) * det3_356_012;
	ELEM_TYPE det4_2356_0134 =  mtx(2,0) * det3_356_134 -  mtx(2,1) * det3_356_034 +  mtx(2,3) * det3_356_014 -  mtx(2,4) * det3_356_013;
	ELEM_TYPE det4_2356_0135 =  mtx(2,0) * det3_356_135 -  mtx(2,1) * det3_356_035 +  mtx(2,3) * det3_356_015 -  mtx(2,5) * det3_356_013;
	ELEM_TYPE det4_2356_0136 =  mtx(2,0) * det3_356_136 -  mtx(2,1) * det3_356_036 +  mtx(2,3) * det3_356_016 -  mtx(2,6) * det3_356_013;
	ELEM_TYPE det4_2356_0145 =  mtx(2,0) * det3_356_145 -  mtx(2,1) * det3_356_045 +  mtx(2,4) * det3_356_015 -  mtx(2,5) * det3_356_014;
	ELEM_TYPE det4_2356_0146 =  mtx(2,0) * det3_356_146 -  mtx(2,1) * det3_356_046 +  mtx(2,4) * det3_356_016 -  mtx(2,6) * det3_356_014;
	ELEM_TYPE det4_2356_0156 =  mtx(2,0) * det3_356_156 -  mtx(2,1) * det3_356_056 +  mtx(2,5) * det3_356_016 -  mtx(2,6) * det3_356_015;
	ELEM_TYPE det4_2356_0234 =  mtx(2,0) * det3_356_234 -  mtx(2,2) * det3_356_034 +  mtx(2,3) * det3_356_024 -  mtx(2,4) * det3_356_023;
	ELEM_TYPE det4_2356_0235 =  mtx(2,0) * det3_356_235 -  mtx(2,2) * det3_356_035 +  mtx(2,3) * det3_356_025 -  mtx(2,5) * det3_356_023;
	ELEM_TYPE det4_2356_0236 =  mtx(2,0) * det3_356_236 -  mtx(2,2) * det3_356_036 +  mtx(2,3) * det3_356_026 -  mtx(2,6) * det3_356_023;
	ELEM_TYPE det4_2356_0245 =  mtx(2,0) * det3_356_245 -  mtx(2,2) * det3_356_045 +  mtx(2,4) * det3_356_025 -  mtx(2,5) * det3_356_024;
	ELEM_TYPE det4_2356_0246 =  mtx(2,0) * det3_356_246 -  mtx(2,2) * det3_356_046 +  mtx(2,4) * det3_356_026 -  mtx(2,6) * det3_356_024;
	ELEM_TYPE det4_2356_0256 =  mtx(2,0) * det3_356_256 -  mtx(2,2) * det3_356_056 +  mtx(2,5) * det3_356_026 -  mtx(2,6) * det3_356_025;
	ELEM_TYPE det4_2356_0345 =  mtx(2,0) * det3_356_345 -  mtx(2,3) * det3_356_045 +  mtx(2,4) * det3_356_035 -  mtx(2,5) * det3_356_034;
	ELEM_TYPE det4_2356_0346 =  mtx(2,0) * det3_356_346 -  mtx(2,3) * det3_356_046 +  mtx(2,4) * det3_356_036 -  mtx(2,6) * det3_356_034;
	ELEM_TYPE det4_2356_0356 =  mtx(2,0) * det3_356_356 -  mtx(2,3) * det3_356_056 +  mtx(2,5) * det3_356_036 -  mtx(2,6) * det3_356_035;
	ELEM_TYPE det4_2356_0456 =  mtx(2,0) * det3_356_456 -  mtx(2,4) * det3_356_056 +  mtx(2,5) * det3_356_046 -  mtx(2,6) * det3_356_045;
	ELEM_TYPE det4_2356_1234 =  mtx(2,1) * det3_356_234 -  mtx(2,2) * det3_356_134 +  mtx(2,3) * det3_356_124 -  mtx(2,4) * det3_356_123;
	ELEM_TYPE det4_2356_1235 =  mtx(2,1) * det3_356_235 -  mtx(2,2) * det3_356_135 +  mtx(2,3) * det3_356_125 -  mtx(2,5) * det3_356_123;
	ELEM_TYPE det4_2356_1236 =  mtx(2,1) * det3_356_236 -  mtx(2,2) * det3_356_136 +  mtx(2,3) * det3_356_126 -  mtx(2,6) * det3_356_123;
	ELEM_TYPE det4_2356_1245 =  mtx(2,1) * det3_356_245 -  mtx(2,2) * det3_356_145 +  mtx(2,4) * det3_356_125 -  mtx(2,5) * det3_356_124;
	ELEM_TYPE det4_2356_1246 =  mtx(2,1) * det3_356_246 -  mtx(2,2) * det3_356_146 +  mtx(2,4) * det3_356_126 -  mtx(2,6) * det3_356_124;
	ELEM_TYPE det4_2356_1256 =  mtx(2,1) * det3_356_256 -  mtx(2,2) * det3_356_156 +  mtx(2,5) * det3_356_126 -  mtx(2,6) * det3_356_125;
	ELEM_TYPE det4_2356_1345 =  mtx(2,1) * det3_356_345 -  mtx(2,3) * det3_356_145 +  mtx(2,4) * det3_356_135 -  mtx(2,5) * det3_356_134;
	ELEM_TYPE det4_2356_1346 =  mtx(2,1) * det3_356_346 -  mtx(2,3) * det3_356_146 +  mtx(2,4) * det3_356_136 -  mtx(2,6) * det3_356_134;
	ELEM_TYPE det4_2356_1356 =  mtx(2,1) * det3_356_356 -  mtx(2,3) * det3_356_156 +  mtx(2,5) * det3_356_136 -  mtx(2,6) * det3_356_135;
	ELEM_TYPE det4_2356_1456 =  mtx(2,1) * det3_356_456 -  mtx(2,4) * det3_356_156 +  mtx(2,5) * det3_356_146 -  mtx(2,6) * det3_356_145;
	ELEM_TYPE det4_2356_2345 =  mtx(2,2) * det3_356_345 -  mtx(2,3) * det3_356_245 +  mtx(2,4) * det3_356_235 -  mtx(2,5) * det3_356_234;
	ELEM_TYPE det4_2356_2346 =  mtx(2,2) * det3_356_346 -  mtx(2,3) * det3_356_246 +  mtx(2,4) * det3_356_236 -  mtx(2,6) * det3_356_234;
	ELEM_TYPE det4_2356_2356 =  mtx(2,2) * det3_356_356 -  mtx(2,3) * det3_356_256 +  mtx(2,5) * det3_356_236 -  mtx(2,6) * det3_356_235;
	ELEM_TYPE det4_2356_2456 =  mtx(2,2) * det3_356_456 -  mtx(2,4) * det3_356_256 +  mtx(2,5) * det3_356_246 -  mtx(2,6) * det3_356_245;
	ELEM_TYPE det4_2356_3456 =  mtx(2,3) * det3_356_456 -  mtx(2,4) * det3_356_356 +  mtx(2,5) * det3_356_346 -  mtx(2,6) * det3_356_345;
	ELEM_TYPE det4_2456_0123 =  mtx(2,0) * det3_456_123 -  mtx(2,1) * det3_456_023 +  mtx(2,2) * det3_456_013 -  mtx(2,3) * det3_456_012;
	ELEM_TYPE det4_2456_0124 =  mtx(2,0) * det3_456_124 -  mtx(2,1) * det3_456_024 +  mtx(2,2) * det3_456_014 -  mtx(2,4) * det3_456_012;
	ELEM_TYPE det4_2456_0125 =  mtx(2,0) * det3_456_125 -  mtx(2,1) * det3_456_025 +  mtx(2,2) * det3_456_015 -  mtx(2,5) * det3_456_012;
	ELEM_TYPE det4_2456_0126 =  mtx(2,0) * det3_456_126 -  mtx(2,1) * det3_456_026 +  mtx(2,2) * det3_456_016 -  mtx(2,6) * det3_456_012;
	ELEM_TYPE det4_2456_0134 =  mtx(2,0) * det3_456_134 -  mtx(2,1) * det3_456_034 +  mtx(2,3) * det3_456_014 -  mtx(2,4) * det3_456_013;
	ELEM_TYPE det4_2456_0135 =  mtx(2,0) * det3_456_135 -  mtx(2,1) * det3_456_035 +  mtx(2,3) * det3_456_015 -  mtx(2,5) * det3_456_013;
	ELEM_TYPE det4_2456_0136 =  mtx(2,0) * det3_456_136 -  mtx(2,1) * det3_456_036 +  mtx(2,3) * det3_456_016 -  mtx(2,6) * det3_456_013;
	ELEM_TYPE det4_2456_0145 =  mtx(2,0) * det3_456_145 -  mtx(2,1) * det3_456_045 +  mtx(2,4) * det3_456_015 -  mtx(2,5) * det3_456_014;
	ELEM_TYPE det4_2456_0146 =  mtx(2,0) * det3_456_146 -  mtx(2,1) * det3_456_046 +  mtx(2,4) * det3_456_016 -  mtx(2,6) * det3_456_014;
	ELEM_TYPE det4_2456_0156 =  mtx(2,0) * det3_456_156 -  mtx(2,1) * det3_456_056 +  mtx(2,5) * det3_456_016 -  mtx(2,6) * det3_456_015;
	ELEM_TYPE det4_2456_0234 =  mtx(2,0) * det3_456_234 -  mtx(2,2) * det3_456_034 +  mtx(2,3) * det3_456_024 -  mtx(2,4) * det3_456_023;
	ELEM_TYPE det4_2456_0235 =  mtx(2,0) * det3_456_235 -  mtx(2,2) * det3_456_035 +  mtx(2,3) * det3_456_025 -  mtx(2,5) * det3_456_023;
	ELEM_TYPE det4_2456_0236 =  mtx(2,0) * det3_456_236 -  mtx(2,2) * det3_456_036 +  mtx(2,3) * det3_456_026 -  mtx(2,6) * det3_456_023;
	ELEM_TYPE det4_2456_0245 =  mtx(2,0) * det3_456_245 -  mtx(2,2) * det3_456_045 +  mtx(2,4) * det3_456_025 -  mtx(2,5) * det3_456_024;
	ELEM_TYPE det4_2456_0246 =  mtx(2,0) * det3_456_246 -  mtx(2,2) * det3_456_046 +  mtx(2,4) * det3_456_026 -  mtx(2,6) * det3_456_024;
	ELEM_TYPE det4_2456_0256 =  mtx(2,0) * det3_456_256 -  mtx(2,2) * det3_456_056 +  mtx(2,5) * det3_456_026 -  mtx(2,6) * det3_456_025;
	ELEM_TYPE det4_2456_0345 =  mtx(2,0) * det3_456_345 -  mtx(2,3) * det3_456_045 +  mtx(2,4) * det3_456_035 -  mtx(2,5) * det3_456_034;
	ELEM_TYPE det4_2456_0346 =  mtx(2,0) * det3_456_346 -  mtx(2,3) * det3_456_046 +  mtx(2,4) * det3_456_036 -  mtx(2,6) * det3_456_034;
	ELEM_TYPE det4_2456_0356 =  mtx(2,0) * det3_456_356 -  mtx(2,3) * det3_456_056 +  mtx(2,5) * det3_456_036 -  mtx(2,6) * det3_456_035;
	ELEM_TYPE det4_2456_0456 =  mtx(2,0) * det3_456_456 -  mtx(2,4) * det3_456_056 +  mtx(2,5) * det3_456_046 -  mtx(2,6) * det3_456_045;
	ELEM_TYPE det4_2456_1234 =  mtx(2,1) * det3_456_234 -  mtx(2,2) * det3_456_134 +  mtx(2,3) * det3_456_124 -  mtx(2,4) * det3_456_123;
	ELEM_TYPE det4_2456_1235 =  mtx(2,1) * det3_456_235 -  mtx(2,2) * det3_456_135 +  mtx(2,3) * det3_456_125 -  mtx(2,5) * det3_456_123;
	ELEM_TYPE det4_2456_1236 =  mtx(2,1) * det3_456_236 -  mtx(2,2) * det3_456_136 +  mtx(2,3) * det3_456_126 -  mtx(2,6) * det3_456_123;
	ELEM_TYPE det4_2456_1245 =  mtx(2,1) * det3_456_245 -  mtx(2,2) * det3_456_145 +  mtx(2,4) * det3_456_125 -  mtx(2,5) * det3_456_124;
	ELEM_TYPE det4_2456_1246 =  mtx(2,1) * det3_456_246 -  mtx(2,2) * det3_456_146 +  mtx(2,4) * det3_456_126 -  mtx(2,6) * det3_456_124;
	ELEM_TYPE det4_2456_1256 =  mtx(2,1) * det3_456_256 -  mtx(2,2) * det3_456_156 +  mtx(2,5) * det3_456_126 -  mtx(2,6) * det3_456_125;
	ELEM_TYPE det4_2456_1345 =  mtx(2,1) * det3_456_345 -  mtx(2,3) * det3_456_145 +  mtx(2,4) * det3_456_135 -  mtx(2,5) * det3_456_134;
	ELEM_TYPE det4_2456_1346 =  mtx(2,1) * det3_456_346 -  mtx(2,3) * det3_456_146 +  mtx(2,4) * det3_456_136 -  mtx(2,6) * det3_456_134;
	ELEM_TYPE det4_2456_1356 =  mtx(2,1) * det3_456_356 -  mtx(2,3) * det3_456_156 +  mtx(2,5) * det3_456_136 -  mtx(2,6) * det3_456_135;
	ELEM_TYPE det4_2456_1456 =  mtx(2,1) * det3_456_456 -  mtx(2,4) * det3_456_156 +  mtx(2,5) * det3_456_146 -  mtx(2,6) * det3_456_145;
	ELEM_TYPE det4_2456_2345 =  mtx(2,2) * det3_456_345 -  mtx(2,3) * det3_456_245 +  mtx(2,4) * det3_456_235 -  mtx(2,5) * det3_456_234;
	ELEM_TYPE det4_2456_2346 =  mtx(2,2) * det3_456_346 -  mtx(2,3) * det3_456_246 +  mtx(2,4) * det3_456_236 -  mtx(2,6) * det3_456_234;
	ELEM_TYPE det4_2456_2356 =  mtx(2,2) * det3_456_356 -  mtx(2,3) * det3_456_256 +  mtx(2,5) * det3_456_236 -  mtx(2,6) * det3_456_235;
	ELEM_TYPE det4_2456_2456 =  mtx(2,2) * det3_456_456 -  mtx(2,4) * det3_456_256 +  mtx(2,5) * det3_456_246 -  mtx(2,6) * det3_456_245;
	ELEM_TYPE det4_2456_3456 =  mtx(2,3) * det3_456_456 -  mtx(2,4) * det3_456_356 +  mtx(2,5) * det3_456_346 -  mtx(2,6) * det3_456_345;
	ELEM_TYPE det4_3456_0123 =  mtx(3,0) * det3_456_123 -  mtx(3,1) * det3_456_023 +  mtx(3,2) * det3_456_013 -  mtx(3,3) * det3_456_012;
	ELEM_TYPE det4_3456_0124 =  mtx(3,0) * det3_456_124 -  mtx(3,1) * det3_456_024 +  mtx(3,2) * det3_456_014 -  mtx(3,4) * det3_456_012;
	ELEM_TYPE det4_3456_0125 =  mtx(3,0) * det3_456_125 -  mtx(3,1) * det3_456_025 +  mtx(3,2) * det3_456_015 -  mtx(3,5) * det3_456_012;
	ELEM_TYPE det4_3456_0126 =  mtx(3,0) * det3_456_126 -  mtx(3,1) * det3_456_026 +  mtx(3,2) * det3_456_016 -  mtx(3,6) * det3_456_012;
	ELEM_TYPE det4_3456_0134 =  mtx(3,0) * det3_456_134 -  mtx(3,1) * det3_456_034 +  mtx(3,3) * det3_456_014 -  mtx(3,4) * det3_456_013;
	ELEM_TYPE det4_3456_0135 =  mtx(3,0) * det3_456_135 -  mtx(3,1) * det3_456_035 +  mtx(3,3) * det3_456_015 -  mtx(3,5) * det3_456_013;
	ELEM_TYPE det4_3456_0136 =  mtx(3,0) * det3_456_136 -  mtx(3,1) * det3_456_036 +  mtx(3,3) * det3_456_016 -  mtx(3,6) * det3_456_013;
	ELEM_TYPE det4_3456_0145 =  mtx(3,0) * det3_456_145 -  mtx(3,1) * det3_456_045 +  mtx(3,4) * det3_456_015 -  mtx(3,5) * det3_456_014;
	ELEM_TYPE det4_3456_0146 =  mtx(3,0) * det3_456_146 -  mtx(3,1) * det3_456_046 +  mtx(3,4) * det3_456_016 -  mtx(3,6) * det3_456_014;
	ELEM_TYPE det4_3456_0156 =  mtx(3,0) * det3_456_156 -  mtx(3,1) * det3_456_056 +  mtx(3,5) * det3_456_016 -  mtx(3,6) * det3_456_015;
	ELEM_TYPE det4_3456_0234 =  mtx(3,0) * det3_456_234 -  mtx(3,2) * det3_456_034 +  mtx(3,3) * det3_456_024 -  mtx(3,4) * det3_456_023;
	ELEM_TYPE det4_3456_0235 =  mtx(3,0) * det3_456_235 -  mtx(3,2) * det3_456_035 +  mtx(3,3) * det3_456_025 -  mtx(3,5) * det3_456_023;
	ELEM_TYPE det4_3456_0236 =  mtx(3,0) * det3_456_236 -  mtx(3,2) * det3_456_036 +  mtx(3,3) * det3_456_026 -  mtx(3,6) * det3_456_023;
	ELEM_TYPE det4_3456_0245 =  mtx(3,0) * det3_456_245 -  mtx(3,2) * det3_456_045 +  mtx(3,4) * det3_456_025 -  mtx(3,5) * det3_456_024;
	ELEM_TYPE det4_3456_0246 =  mtx(3,0) * det3_456_246 -  mtx(3,2) * det3_456_046 +  mtx(3,4) * det3_456_026 -  mtx(3,6) * det3_456_024;
	ELEM_TYPE det4_3456_0256 =  mtx(3,0) * det3_456_256 -  mtx(3,2) * det3_456_056 +  mtx(3,5) * det3_456_026 -  mtx(3,6) * det3_456_025;
	ELEM_TYPE det4_3456_0345 =  mtx(3,0) * det3_456_345 -  mtx(3,3) * det3_456_045 +  mtx(3,4) * det3_456_035 -  mtx(3,5) * det3_456_034;
	ELEM_TYPE det4_3456_0346 =  mtx(3,0) * det3_456_346 -  mtx(3,3) * det3_456_046 +  mtx(3,4) * det3_456_036 -  mtx(3,6) * det3_456_034;
	ELEM_TYPE det4_3456_0356 =  mtx(3,0) * det3_456_356 -  mtx(3,3) * det3_456_056 +  mtx(3,5) * det3_456_036 -  mtx(3,6) * det3_456_035;
	ELEM_TYPE det4_3456_0456 =  mtx(3,0) * det3_456_456 -  mtx(3,4) * det3_456_056 +  mtx(3,5) * det3_456_046 -  mtx(3,6) * det3_456_045;
	ELEM_TYPE det4_3456_1234 =  mtx(3,1) * det3_456_234 -  mtx(3,2) * det3_456_134 +  mtx(3,3) * det3_456_124 -  mtx(3,4) * det3_456_123;
	ELEM_TYPE det4_3456_1235 =  mtx(3,1) * det3_456_235 -  mtx(3,2) * det3_456_135 +  mtx(3,3) * det3_456_125 -  mtx(3,5) * det3_456_123;
	ELEM_TYPE det4_3456_1236 =  mtx(3,1) * det3_456_236 -  mtx(3,2) * det3_456_136 +  mtx(3,3) * det3_456_126 -  mtx(3,6) * det3_456_123;
	ELEM_TYPE det4_3456_1245 =  mtx(3,1) * det3_456_245 -  mtx(3,2) * det3_456_145 +  mtx(3,4) * det3_456_125 -  mtx(3,5) * det3_456_124;
	ELEM_TYPE det4_3456_1246 =  mtx(3,1) * det3_456_246 -  mtx(3,2) * det3_456_146 +  mtx(3,4) * det3_456_126 -  mtx(3,6) * det3_456_124;
	ELEM_TYPE det4_3456_1256 =  mtx(3,1) * det3_456_256 -  mtx(3,2) * det3_456_156 +  mtx(3,5) * det3_456_126 -  mtx(3,6) * det3_456_125;
	ELEM_TYPE det4_3456_1345 =  mtx(3,1) * det3_456_345 -  mtx(3,3) * det3_456_145 +  mtx(3,4) * det3_456_135 -  mtx(3,5) * det3_456_134;
	ELEM_TYPE det4_3456_1346 =  mtx(3,1) * det3_456_346 -  mtx(3,3) * det3_456_146 +  mtx(3,4) * det3_456_136 -  mtx(3,6) * det3_456_134;
	ELEM_TYPE det4_3456_1356 =  mtx(3,1) * det3_456_356 -  mtx(3,3) * det3_456_156 +  mtx(3,5) * det3_456_136 -  mtx(3,6) * det3_456_135;
	ELEM_TYPE det4_3456_1456 =  mtx(3,1) * det3_456_456 -  mtx(3,4) * det3_456_156 +  mtx(3,5) * det3_456_146 -  mtx(3,6) * det3_456_145;
	ELEM_TYPE det4_3456_2345 =  mtx(3,2) * det3_456_345 -  mtx(3,3) * det3_456_245 +  mtx(3,4) * det3_456_235 -  mtx(3,5) * det3_456_234;
	ELEM_TYPE det4_3456_2346 =  mtx(3,2) * det3_456_346 -  mtx(3,3) * det3_456_246 +  mtx(3,4) * det3_456_236 -  mtx(3,6) * det3_456_234;
	ELEM_TYPE det4_3456_2356 =  mtx(3,2) * det3_456_356 -  mtx(3,3) * det3_456_256 +  mtx(3,5) * det3_456_236 -  mtx(3,6) * det3_456_235;
	ELEM_TYPE det4_3456_2456 =  mtx(3,2) * det3_456_456 -  mtx(3,4) * det3_456_256 +  mtx(3,5) * det3_456_246 -  mtx(3,6) * det3_456_245;
	ELEM_TYPE det4_3456_3456 =  mtx(3,3) * det3_456_456 -  mtx(3,4) * det3_456_356 +  mtx(3,5) * det3_456_346 -  mtx(3,6) * det3_456_345;

	ELEM_TYPE det5_12345_01234 =  mtx(1,0) * det4_2345_1234 -  mtx(1,1) * det4_2345_0234 +  mtx(1,2) * det4_2345_0134 -  mtx(1,3) * det4_2345_0124 +  mtx(1,4) * det4_2345_0123;
	ELEM_TYPE det5_12345_01235 =  mtx(1,0) * det4_2345_1235 -  mtx(1,1) * det4_2345_0235 +  mtx(1,2) * det4_2345_0135 -  mtx(1,3) * det4_2345_0125 +  mtx(1,5) * det4_2345_0123;
	ELEM_TYPE det5_12345_01236 =  mtx(1,0) * det4_2345_1236 -  mtx(1,1) * det4_2345_0236 +  mtx(1,2) * det4_2345_0136 -  mtx(1,3) * det4_2345_0126 +  mtx(1,6) * det4_2345_0123;
	ELEM_TYPE det5_12345_01245 =  mtx(1,0) * det4_2345_1245 -  mtx(1,1) * det4_2345_0245 +  mtx(1,2) * det4_2345_0145 -  mtx(1,4) * det4_2345_0125 +  mtx(1,5) * det4_2345_0124;
	ELEM_TYPE det5_12345_01246 =  mtx(1,0) * det4_2345_1246 -  mtx(1,1) * det4_2345_0246 +  mtx(1,2) * det4_2345_0146 -  mtx(1,4) * det4_2345_0126 +  mtx(1,6) * det4_2345_0124;
	ELEM_TYPE det5_12345_01256 =  mtx(1,0) * det4_2345_1256 -  mtx(1,1) * det4_2345_0256 +  mtx(1,2) * det4_2345_0156 -  mtx(1,5) * det4_2345_0126 +  mtx(1,6) * det4_2345_0125;
	ELEM_TYPE det5_12345_01345 =  mtx(1,0) * det4_2345_1345 -  mtx(1,1) * det4_2345_0345 +  mtx(1,3) * det4_2345_0145 -  mtx(1,4) * det4_2345_0135 +  mtx(1,5) * det4_2345_0134;
	ELEM_TYPE det5_12345_01346 =  mtx(1,0) * det4_2345_1346 -  mtx(1,1) * det4_2345_0346 +  mtx(1,3) * det4_2345_0146 -  mtx(1,4) * det4_2345_0136 +  mtx(1,6) * det4_2345_0134;
	ELEM_TYPE det5_12345_01356 =  mtx(1,0) * det4_2345_1356 -  mtx(1,1) * det4_2345_0356 +  mtx(1,3) * det4_2345_0156 -  mtx(1,5) * det4_2345_0136 +  mtx(1,6) * det4_2345_0135;
	ELEM_TYPE det5_12345_01456 =  mtx(1,0) * det4_2345_1456 -  mtx(1,1) * det4_2345_0456 +  mtx(1,4) * det4_2345_0156 -  mtx(1,5) * det4_2345_0146 +  mtx(1,6) * det4_2345_0145;
	ELEM_TYPE det5_12345_02345 =  mtx(1,0) * det4_2345_2345 -  mtx(1,2) * det4_2345_0345 +  mtx(1,3) * det4_2345_0245 -  mtx(1,4) * det4_2345_0235 +  mtx(1,5) * det4_2345_0234;
	ELEM_TYPE det5_12345_02346 =  mtx(1,0) * det4_2345_2346 -  mtx(1,2) * det4_2345_0346 +  mtx(1,3) * det4_2345_0246 -  mtx(1,4) * det4_2345_0236 +  mtx(1,6) * det4_2345_0234;
	ELEM_TYPE det5_12345_02356 =  mtx(1,0) * det4_2345_2356 -  mtx(1,2) * det4_2345_0356 +  mtx(1,3) * det4_2345_0256 -  mtx(1,5) * det4_2345_0236 +  mtx(1,6) * det4_2345_0235;
	ELEM_TYPE det5_12345_02456 =  mtx(1,0) * det4_2345_2456 -  mtx(1,2) * det4_2345_0456 +  mtx(1,4) * det4_2345_0256 -  mtx(1,5) * det4_2345_0246 +  mtx(1,6) * det4_2345_0245;
	ELEM_TYPE det5_12345_03456 =  mtx(1,0) * det4_2345_3456 -  mtx(1,3) * det4_2345_0456 +  mtx(1,4) * det4_2345_0356 -  mtx(1,5) * det4_2345_0346 +  mtx(1,6) * det4_2345_0345;
	ELEM_TYPE det5_12345_12345 =  mtx(1,1) * det4_2345_2345 -  mtx(1,2) * det4_2345_1345 +  mtx(1,3) * det4_2345_1245 -  mtx(1,4) * det4_2345_1235 +  mtx(1,5) * det4_2345_1234;
	ELEM_TYPE det5_12345_12346 =  mtx(1,1) * det4_2345_2346 -  mtx(1,2) * det4_2345_1346 +  mtx(1,3) * det4_2345_1246 -  mtx(1,4) * det4_2345_1236 +  mtx(1,6) * det4_2345_1234;
	ELEM_TYPE det5_12345_12356 =  mtx(1,1) * det4_2345_2356 -  mtx(1,2) * det4_2345_1356 +  mtx(1,3) * det4_2345_1256 -  mtx(1,5) * det4_2345_1236 +  mtx(1,6) * det4_2345_1235;
	ELEM_TYPE det5_12345_12456 =  mtx(1,1) * det4_2345_2456 -  mtx(1,2) * det4_2345_1456 +  mtx(1,4) * det4_2345_1256 -  mtx(1,5) * det4_2345_1246 +  mtx(1,6) * det4_2345_1245;
	ELEM_TYPE det5_12345_13456 =  mtx(1,1) * det4_2345_3456 -  mtx(1,3) * det4_2345_1456 +  mtx(1,4) * det4_2345_1356 -  mtx(1,5) * det4_2345_1346 +  mtx(1,6) * det4_2345_1345;
	ELEM_TYPE det5_12345_23456 =  mtx(1,2) * det4_2345_3456 -  mtx(1,3) * det4_2345_2456 +  mtx(1,4) * det4_2345_2356 -  mtx(1,5) * det4_2345_2346 +  mtx(1,6) * det4_2345_2345;
	ELEM_TYPE det5_12346_01234 =  mtx(1,0) * det4_2346_1234 -  mtx(1,1) * det4_2346_0234 +  mtx(1,2) * det4_2346_0134 -  mtx(1,3) * det4_2346_0124 +  mtx(1,4) * det4_2346_0123;
	ELEM_TYPE det5_12346_01235 =  mtx(1,0) * det4_2346_1235 -  mtx(1,1) * det4_2346_0235 +  mtx(1,2) * det4_2346_0135 -  mtx(1,3) * det4_2346_0125 +  mtx(1,5) * det4_2346_0123;
	ELEM_TYPE det5_12346_01236 =  mtx(1,0) * det4_2346_1236 -  mtx(1,1) * det4_2346_0236 +  mtx(1,2) * det4_2346_0136 -  mtx(1,3) * det4_2346_0126 +  mtx(1,6) * det4_2346_0123;
	ELEM_TYPE det5_12346_01245 =  mtx(1,0) * det4_2346_1245 -  mtx(1,1) * det4_2346_0245 +  mtx(1,2) * det4_2346_0145 -  mtx(1,4) * det4_2346_0125 +  mtx(1,5) * det4_2346_0124;
	ELEM_TYPE det5_12346_01246 =  mtx(1,0) * det4_2346_1246 -  mtx(1,1) * det4_2346_0246 +  mtx(1,2) * det4_2346_0146 -  mtx(1,4) * det4_2346_0126 +  mtx(1,6) * det4_2346_0124;
	ELEM_TYPE det5_12346_01256 =  mtx(1,0) * det4_2346_1256 -  mtx(1,1) * det4_2346_0256 +  mtx(1,2) * det4_2346_0156 -  mtx(1,5) * det4_2346_0126 +  mtx(1,6) * det4_2346_0125;
	ELEM_TYPE det5_12346_01345 =  mtx(1,0) * det4_2346_1345 -  mtx(1,1) * det4_2346_0345 +  mtx(1,3) * det4_2346_0145 -  mtx(1,4) * det4_2346_0135 +  mtx(1,5) * det4_2346_0134;
	ELEM_TYPE det5_12346_01346 =  mtx(1,0) * det4_2346_1346 -  mtx(1,1) * det4_2346_0346 +  mtx(1,3) * det4_2346_0146 -  mtx(1,4) * det4_2346_0136 +  mtx(1,6) * det4_2346_0134;
	ELEM_TYPE det5_12346_01356 =  mtx(1,0) * det4_2346_1356 -  mtx(1,1) * det4_2346_0356 +  mtx(1,3) * det4_2346_0156 -  mtx(1,5) * det4_2346_0136 +  mtx(1,6) * det4_2346_0135;
	ELEM_TYPE det5_12346_01456 =  mtx(1,0) * det4_2346_1456 -  mtx(1,1) * det4_2346_0456 +  mtx(1,4) * det4_2346_0156 -  mtx(1,5) * det4_2346_0146 +  mtx(1,6) * det4_2346_0145;
	ELEM_TYPE det5_12346_02345 =  mtx(1,0) * det4_2346_2345 -  mtx(1,2) * det4_2346_0345 +  mtx(1,3) * det4_2346_0245 -  mtx(1,4) * det4_2346_0235 +  mtx(1,5) * det4_2346_0234;
	ELEM_TYPE det5_12346_02346 =  mtx(1,0) * det4_2346_2346 -  mtx(1,2) * det4_2346_0346 +  mtx(1,3) * det4_2346_0246 -  mtx(1,4) * det4_2346_0236 +  mtx(1,6) * det4_2346_0234;
	ELEM_TYPE det5_12346_02356 =  mtx(1,0) * det4_2346_2356 -  mtx(1,2) * det4_2346_0356 +  mtx(1,3) * det4_2346_0256 -  mtx(1,5) * det4_2346_0236 +  mtx(1,6) * det4_2346_0235;
	ELEM_TYPE det5_12346_02456 =  mtx(1,0) * det4_2346_2456 -  mtx(1,2) * det4_2346_0456 +  mtx(1,4) * det4_2346_0256 -  mtx(1,5) * det4_2346_0246 +  mtx(1,6) * det4_2346_0245;
	ELEM_TYPE det5_12346_03456 =  mtx(1,0) * det4_2346_3456 -  mtx(1,3) * det4_2346_0456 +  mtx(1,4) * det4_2346_0356 -  mtx(1,5) * det4_2346_0346 +  mtx(1,6) * det4_2346_0345;
	ELEM_TYPE det5_12346_12345 =  mtx(1,1) * det4_2346_2345 -  mtx(1,2) * det4_2346_1345 +  mtx(1,3) * det4_2346_1245 -  mtx(1,4) * det4_2346_1235 +  mtx(1,5) * det4_2346_1234;
	ELEM_TYPE det5_12346_12346 =  mtx(1,1) * det4_2346_2346 -  mtx(1,2) * det4_2346_1346 +  mtx(1,3) * det4_2346_1246 -  mtx(1,4) * det4_2346_1236 +  mtx(1,6) * det4_2346_1234;
	ELEM_TYPE det5_12346_12356 =  mtx(1,1) * det4_2346_2356 -  mtx(1,2) * det4_2346_1356 +  mtx(1,3) * det4_2346_1256 -  mtx(1,5) * det4_2346_1236 +  mtx(1,6) * det4_2346_1235;
	ELEM_TYPE det5_12346_12456 =  mtx(1,1) * det4_2346_2456 -  mtx(1,2) * det4_2346_1456 +  mtx(1,4) * det4_2346_1256 -  mtx(1,5) * det4_2346_1246 +  mtx(1,6) * det4_2346_1245;
	ELEM_TYPE det5_12346_13456 =  mtx(1,1) * det4_2346_3456 -  mtx(1,3) * det4_2346_1456 +  mtx(1,4) * det4_2346_1356 -  mtx(1,5) * det4_2346_1346 +  mtx(1,6) * det4_2346_1345;
	ELEM_TYPE det5_12346_23456 =  mtx(1,2) * det4_2346_3456 -  mtx(1,3) * det4_2346_2456 +  mtx(1,4) * det4_2346_2356 -  mtx(1,5) * det4_2346_2346 +  mtx(1,6) * det4_2346_2345;
	ELEM_TYPE det5_12356_01234 =  mtx(1,0) * det4_2356_1234 -  mtx(1,1) * det4_2356_0234 +  mtx(1,2) * det4_2356_0134 -  mtx(1,3) * det4_2356_0124 +  mtx(1,4) * det4_2356_0123;
	ELEM_TYPE det5_12356_01235 =  mtx(1,0) * det4_2356_1235 -  mtx(1,1) * det4_2356_0235 +  mtx(1,2) * det4_2356_0135 -  mtx(1,3) * det4_2356_0125 +  mtx(1,5) * det4_2356_0123;
	ELEM_TYPE det5_12356_01236 =  mtx(1,0) * det4_2356_1236 -  mtx(1,1) * det4_2356_0236 +  mtx(1,2) * det4_2356_0136 -  mtx(1,3) * det4_2356_0126 +  mtx(1,6) * det4_2356_0123;
	ELEM_TYPE det5_12356_01245 =  mtx(1,0) * det4_2356_1245 -  mtx(1,1) * det4_2356_0245 +  mtx(1,2) * det4_2356_0145 -  mtx(1,4) * det4_2356_0125 +  mtx(1,5) * det4_2356_0124;
	ELEM_TYPE det5_12356_01246 =  mtx(1,0) * det4_2356_1246 -  mtx(1,1) * det4_2356_0246 +  mtx(1,2) * det4_2356_0146 -  mtx(1,4) * det4_2356_0126 +  mtx(1,6) * det4_2356_0124;
	ELEM_TYPE det5_12356_01256 =  mtx(1,0) * det4_2356_1256 -  mtx(1,1) * det4_2356_0256 +  mtx(1,2) * det4_2356_0156 -  mtx(1,5) * det4_2356_0126 +  mtx(1,6) * det4_2356_0125;
	ELEM_TYPE det5_12356_01345 =  mtx(1,0) * det4_2356_1345 -  mtx(1,1) * det4_2356_0345 +  mtx(1,3) * det4_2356_0145 -  mtx(1,4) * det4_2356_0135 +  mtx(1,5) * det4_2356_0134;
	ELEM_TYPE det5_12356_01346 =  mtx(1,0) * det4_2356_1346 -  mtx(1,1) * det4_2356_0346 +  mtx(1,3) * det4_2356_0146 -  mtx(1,4) * det4_2356_0136 +  mtx(1,6) * det4_2356_0134;
	ELEM_TYPE det5_12356_01356 =  mtx(1,0) * det4_2356_1356 -  mtx(1,1) * det4_2356_0356 +  mtx(1,3) * det4_2356_0156 -  mtx(1,5) * det4_2356_0136 +  mtx(1,6) * det4_2356_0135;
	ELEM_TYPE det5_12356_01456 =  mtx(1,0) * det4_2356_1456 -  mtx(1,1) * det4_2356_0456 +  mtx(1,4) * det4_2356_0156 -  mtx(1,5) * det4_2356_0146 +  mtx(1,6) * det4_2356_0145;
	ELEM_TYPE det5_12356_02345 =  mtx(1,0) * det4_2356_2345 -  mtx(1,2) * det4_2356_0345 +  mtx(1,3) * det4_2356_0245 -  mtx(1,4) * det4_2356_0235 +  mtx(1,5) * det4_2356_0234;
	ELEM_TYPE det5_12356_02346 =  mtx(1,0) * det4_2356_2346 -  mtx(1,2) * det4_2356_0346 +  mtx(1,3) * det4_2356_0246 -  mtx(1,4) * det4_2356_0236 +  mtx(1,6) * det4_2356_0234;
	ELEM_TYPE det5_12356_02356 =  mtx(1,0) * det4_2356_2356 -  mtx(1,2) * det4_2356_0356 +  mtx(1,3) * det4_2356_0256 -  mtx(1,5) * det4_2356_0236 +  mtx(1,6) * det4_2356_0235;
	ELEM_TYPE det5_12356_02456 =  mtx(1,0) * det4_2356_2456 -  mtx(1,2) * det4_2356_0456 +  mtx(1,4) * det4_2356_0256 -  mtx(1,5) * det4_2356_0246 +  mtx(1,6) * det4_2356_0245;
	ELEM_TYPE det5_12356_03456 =  mtx(1,0) * det4_2356_3456 -  mtx(1,3) * det4_2356_0456 +  mtx(1,4) * det4_2356_0356 -  mtx(1,5) * det4_2356_0346 +  mtx(1,6) * det4_2356_0345;
	ELEM_TYPE det5_12356_12345 =  mtx(1,1) * det4_2356_2345 -  mtx(1,2) * det4_2356_1345 +  mtx(1,3) * det4_2356_1245 -  mtx(1,4) * det4_2356_1235 +  mtx(1,5) * det4_2356_1234;
	ELEM_TYPE det5_12356_12346 =  mtx(1,1) * det4_2356_2346 -  mtx(1,2) * det4_2356_1346 +  mtx(1,3) * det4_2356_1246 -  mtx(1,4) * det4_2356_1236 +  mtx(1,6) * det4_2356_1234;
	ELEM_TYPE det5_12356_12356 =  mtx(1,1) * det4_2356_2356 -  mtx(1,2) * det4_2356_1356 +  mtx(1,3) * det4_2356_1256 -  mtx(1,5) * det4_2356_1236 +  mtx(1,6) * det4_2356_1235;
	ELEM_TYPE det5_12356_12456 =  mtx(1,1) * det4_2356_2456 -  mtx(1,2) * det4_2356_1456 +  mtx(1,4) * det4_2356_1256 -  mtx(1,5) * det4_2356_1246 +  mtx(1,6) * det4_2356_1245;
	ELEM_TYPE det5_12356_13456 =  mtx(1,1) * det4_2356_3456 -  mtx(1,3) * det4_2356_1456 +  mtx(1,4) * det4_2356_1356 -  mtx(1,5) * det4_2356_1346 +  mtx(1,6) * det4_2356_1345;
	ELEM_TYPE det5_12356_23456 =  mtx(1,2) * det4_2356_3456 -  mtx(1,3) * det4_2356_2456 +  mtx(1,4) * det4_2356_2356 -  mtx(1,5) * det4_2356_2346 +  mtx(1,6) * det4_2356_2345;
	ELEM_TYPE det5_12456_01234 =  mtx(1,0) * det4_2456_1234 -  mtx(1,1) * det4_2456_0234 +  mtx(1,2) * det4_2456_0134 -  mtx(1,3) * det4_2456_0124 +  mtx(1,4) * det4_2456_0123;
	ELEM_TYPE det5_12456_01235 =  mtx(1,0) * det4_2456_1235 -  mtx(1,1) * det4_2456_0235 +  mtx(1,2) * det4_2456_0135 -  mtx(1,3) * det4_2456_0125 +  mtx(1,5) * det4_2456_0123;
	ELEM_TYPE det5_12456_01236 =  mtx(1,0) * det4_2456_1236 -  mtx(1,1) * det4_2456_0236 +  mtx(1,2) * det4_2456_0136 -  mtx(1,3) * det4_2456_0126 +  mtx(1,6) * det4_2456_0123;
	ELEM_TYPE det5_12456_01245 =  mtx(1,0) * det4_2456_1245 -  mtx(1,1) * det4_2456_0245 +  mtx(1,2) * det4_2456_0145 -  mtx(1,4) * det4_2456_0125 +  mtx(1,5) * det4_2456_0124;
	ELEM_TYPE det5_12456_01246 =  mtx(1,0) * det4_2456_1246 -  mtx(1,1) * det4_2456_0246 +  mtx(1,2) * det4_2456_0146 -  mtx(1,4) * det4_2456_0126 +  mtx(1,6) * det4_2456_0124;
	ELEM_TYPE det5_12456_01256 =  mtx(1,0) * det4_2456_1256 -  mtx(1,1) * det4_2456_0256 +  mtx(1,2) * det4_2456_0156 -  mtx(1,5) * det4_2456_0126 +  mtx(1,6) * det4_2456_0125;
	ELEM_TYPE det5_12456_01345 =  mtx(1,0) * det4_2456_1345 -  mtx(1,1) * det4_2456_0345 +  mtx(1,3) * det4_2456_0145 -  mtx(1,4) * det4_2456_0135 +  mtx(1,5) * det4_2456_0134;
	ELEM_TYPE det5_12456_01346 =  mtx(1,0) * det4_2456_1346 -  mtx(1,1) * det4_2456_0346 +  mtx(1,3) * det4_2456_0146 -  mtx(1,4) * det4_2456_0136 +  mtx(1,6) * det4_2456_0134;
	ELEM_TYPE det5_12456_01356 =  mtx(1,0) * det4_2456_1356 -  mtx(1,1) * det4_2456_0356 +  mtx(1,3) * det4_2456_0156 -  mtx(1,5) * det4_2456_0136 +  mtx(1,6) * det4_2456_0135;
	ELEM_TYPE det5_12456_01456 =  mtx(1,0) * det4_2456_1456 -  mtx(1,1) * det4_2456_0456 +  mtx(1,4) * det4_2456_0156 -  mtx(1,5) * det4_2456_0146 +  mtx(1,6) * det4_2456_0145;
	ELEM_TYPE det5_12456_02345 =  mtx(1,0) * det4_2456_2345 -  mtx(1,2) * det4_2456_0345 +  mtx(1,3) * det4_2456_0245 -  mtx(1,4) * det4_2456_0235 +  mtx(1,5) * det4_2456_0234;
	ELEM_TYPE det5_12456_02346 =  mtx(1,0) * det4_2456_2346 -  mtx(1,2) * det4_2456_0346 +  mtx(1,3) * det4_2456_0246 -  mtx(1,4) * det4_2456_0236 +  mtx(1,6) * det4_2456_0234;
	ELEM_TYPE det5_12456_02356 =  mtx(1,0) * det4_2456_2356 -  mtx(1,2) * det4_2456_0356 +  mtx(1,3) * det4_2456_0256 -  mtx(1,5) * det4_2456_0236 +  mtx(1,6) * det4_2456_0235;
	ELEM_TYPE det5_12456_02456 =  mtx(1,0) * det4_2456_2456 -  mtx(1,2) * det4_2456_0456 +  mtx(1,4) * det4_2456_0256 -  mtx(1,5) * det4_2456_0246 +  mtx(1,6) * det4_2456_0245;
	ELEM_TYPE det5_12456_03456 =  mtx(1,0) * det4_2456_3456 -  mtx(1,3) * det4_2456_0456 +  mtx(1,4) * det4_2456_0356 -  mtx(1,5) * det4_2456_0346 +  mtx(1,6) * det4_2456_0345;
	ELEM_TYPE det5_12456_12345 =  mtx(1,1) * det4_2456_2345 -  mtx(1,2) * det4_2456_1345 +  mtx(1,3) * det4_2456_1245 -  mtx(1,4) * det4_2456_1235 +  mtx(1,5) * det4_2456_1234;
	ELEM_TYPE det5_12456_12346 =  mtx(1,1) * det4_2456_2346 -  mtx(1,2) * det4_2456_1346 +  mtx(1,3) * det4_2456_1246 -  mtx(1,4) * det4_2456_1236 +  mtx(1,6) * det4_2456_1234;
	ELEM_TYPE det5_12456_12356 =  mtx(1,1) * det4_2456_2356 -  mtx(1,2) * det4_2456_1356 +  mtx(1,3) * det4_2456_1256 -  mtx(1,5) * det4_2456_1236 +  mtx(1,6) * det4_2456_1235;
	ELEM_TYPE det5_12456_12456 =  mtx(1,1) * det4_2456_2456 -  mtx(1,2) * det4_2456_1456 +  mtx(1,4) * det4_2456_1256 -  mtx(1,5) * det4_2456_1246 +  mtx(1,6) * det4_2456_1245;
	ELEM_TYPE det5_12456_13456 =  mtx(1,1) * det4_2456_3456 -  mtx(1,3) * det4_2456_1456 +  mtx(1,4) * det4_2456_1356 -  mtx(1,5) * det4_2456_1346 +  mtx(1,6) * det4_2456_1345;
	ELEM_TYPE det5_12456_23456 =  mtx(1,2) * det4_2456_3456 -  mtx(1,3) * det4_2456_2456 +  mtx(1,4) * det4_2456_2356 -  mtx(1,5) * det4_2456_2346 +  mtx(1,6) * det4_2456_2345;
	ELEM_TYPE det5_13456_01234 =  mtx(1,0) * det4_3456_1234 -  mtx(1,1) * det4_3456_0234 +  mtx(1,2) * det4_3456_0134 -  mtx(1,3) * det4_3456_0124 +  mtx(1,4) * det4_3456_0123;
	ELEM_TYPE det5_13456_01235 =  mtx(1,0) * det4_3456_1235 -  mtx(1,1) * det4_3456_0235 +  mtx(1,2) * det4_3456_0135 -  mtx(1,3) * det4_3456_0125 +  mtx(1,5) * det4_3456_0123;
	ELEM_TYPE det5_13456_01236 =  mtx(1,0) * det4_3456_1236 -  mtx(1,1) * det4_3456_0236 +  mtx(1,2) * det4_3456_0136 -  mtx(1,3) * det4_3456_0126 +  mtx(1,6) * det4_3456_0123;
	ELEM_TYPE det5_13456_01245 =  mtx(1,0) * det4_3456_1245 -  mtx(1,1) * det4_3456_0245 +  mtx(1,2) * det4_3456_0145 -  mtx(1,4) * det4_3456_0125 +  mtx(1,5) * det4_3456_0124;
	ELEM_TYPE det5_13456_01246 =  mtx(1,0) * det4_3456_1246 -  mtx(1,1) * det4_3456_0246 +  mtx(1,2) * det4_3456_0146 -  mtx(1,4) * det4_3456_0126 +  mtx(1,6) * det4_3456_0124;
	ELEM_TYPE det5_13456_01256 =  mtx(1,0) * det4_3456_1256 -  mtx(1,1) * det4_3456_0256 +  mtx(1,2) * det4_3456_0156 -  mtx(1,5) * det4_3456_0126 +  mtx(1,6) * det4_3456_0125;
	ELEM_TYPE det5_13456_01345 =  mtx(1,0) * det4_3456_1345 -  mtx(1,1) * det4_3456_0345 +  mtx(1,3) * det4_3456_0145 -  mtx(1,4) * det4_3456_0135 +  mtx(1,5) * det4_3456_0134;
	ELEM_TYPE det5_13456_01346 =  mtx(1,0) * det4_3456_1346 -  mtx(1,1) * det4_3456_0346 +  mtx(1,3) * det4_3456_0146 -  mtx(1,4) * det4_3456_0136 +  mtx(1,6) * det4_3456_0134;
	ELEM_TYPE det5_13456_01356 =  mtx(1,0) * det4_3456_1356 -  mtx(1,1) * det4_3456_0356 +  mtx(1,3) * det4_3456_0156 -  mtx(1,5) * det4_3456_0136 +  mtx(1,6) * det4_3456_0135;
	ELEM_TYPE det5_13456_01456 =  mtx(1,0) * det4_3456_1456 -  mtx(1,1) * det4_3456_0456 +  mtx(1,4) * det4_3456_0156 -  mtx(1,5) * det4_3456_0146 +  mtx(1,6) * det4_3456_0145;
	ELEM_TYPE det5_13456_02345 =  mtx(1,0) * det4_3456_2345 -  mtx(1,2) * det4_3456_0345 +  mtx(1,3) * det4_3456_0245 -  mtx(1,4) * det4_3456_0235 +  mtx(1,5) * det4_3456_0234;
	ELEM_TYPE det5_13456_02346 =  mtx(1,0) * det4_3456_2346 -  mtx(1,2) * det4_3456_0346 +  mtx(1,3) * det4_3456_0246 -  mtx(1,4) * det4_3456_0236 +  mtx(1,6) * det4_3456_0234;
	ELEM_TYPE det5_13456_02356 =  mtx(1,0) * det4_3456_2356 -  mtx(1,2) * det4_3456_0356 +  mtx(1,3) * det4_3456_0256 -  mtx(1,5) * det4_3456_0236 +  mtx(1,6) * det4_3456_0235;
	ELEM_TYPE det5_13456_02456 =  mtx(1,0) * det4_3456_2456 -  mtx(1,2) * det4_3456_0456 +  mtx(1,4) * det4_3456_0256 -  mtx(1,5) * det4_3456_0246 +  mtx(1,6) * det4_3456_0245;
	ELEM_TYPE det5_13456_03456 =  mtx(1,0) * det4_3456_3456 -  mtx(1,3) * det4_3456_0456 +  mtx(1,4) * det4_3456_0356 -  mtx(1,5) * det4_3456_0346 +  mtx(1,6) * det4_3456_0345;
	ELEM_TYPE det5_13456_12345 =  mtx(1,1) * det4_3456_2345 -  mtx(1,2) * det4_3456_1345 +  mtx(1,3) * det4_3456_1245 -  mtx(1,4) * det4_3456_1235 +  mtx(1,5) * det4_3456_1234;
	ELEM_TYPE det5_13456_12346 =  mtx(1,1) * det4_3456_2346 -  mtx(1,2) * det4_3456_1346 +  mtx(1,3) * det4_3456_1246 -  mtx(1,4) * det4_3456_1236 +  mtx(1,6) * det4_3456_1234;
	ELEM_TYPE det5_13456_12356 =  mtx(1,1) * det4_3456_2356 -  mtx(1,2) * det4_3456_1356 +  mtx(1,3) * det4_3456_1256 -  mtx(1,5) * det4_3456_1236 +  mtx(1,6) * det4_3456_1235;
	ELEM_TYPE det5_13456_12456 =  mtx(1,1) * det4_3456_2456 -  mtx(1,2) * det4_3456_1456 +  mtx(1,4) * det4_3456_1256 -  mtx(1,5) * det4_3456_1246 +  mtx(1,6) * det4_3456_1245;
	ELEM_TYPE det5_13456_13456 =  mtx(1,1) * det4_3456_3456 -  mtx(1,3) * det4_3456_1456 +  mtx(1,4) * det4_3456_1356 -  mtx(1,5) * det4_3456_1346 +  mtx(1,6) * det4_3456_1345;
	ELEM_TYPE det5_13456_23456 =  mtx(1,2) * det4_3456_3456 -  mtx(1,3) * det4_3456_2456 +  mtx(1,4) * det4_3456_2356 -  mtx(1,5) * det4_3456_2346 +  mtx(1,6) * det4_3456_2345;
	ELEM_TYPE det5_23456_01234 =  mtx(2,0) * det4_3456_1234 -  mtx(2,1) * det4_3456_0234 +  mtx(2,2) * det4_3456_0134 -  mtx(2,3) * det4_3456_0124 +  mtx(2,4) * det4_3456_0123;
	ELEM_TYPE det5_23456_01235 =  mtx(2,0) * det4_3456_1235 -  mtx(2,1) * det4_3456_0235 +  mtx(2,2) * det4_3456_0135 -  mtx(2,3) * det4_3456_0125 +  mtx(2,5) * det4_3456_0123;
	ELEM_TYPE det5_23456_01236 =  mtx(2,0) * det4_3456_1236 -  mtx(2,1) * det4_3456_0236 +  mtx(2,2) * det4_3456_0136 -  mtx(2,3) * det4_3456_0126 +  mtx(2,6) * det4_3456_0123;
	ELEM_TYPE det5_23456_01245 =  mtx(2,0) * det4_3456_1245 -  mtx(2,1) * det4_3456_0245 +  mtx(2,2) * det4_3456_0145 -  mtx(2,4) * det4_3456_0125 +  mtx(2,5) * det4_3456_0124;
	ELEM_TYPE det5_23456_01246 =  mtx(2,0) * det4_3456_1246 -  mtx(2,1) * det4_3456_0246 +  mtx(2,2) * det4_3456_0146 -  mtx(2,4) * det4_3456_0126 +  mtx(2,6) * det4_3456_0124;
	ELEM_TYPE det5_23456_01256 =  mtx(2,0) * det4_3456_1256 -  mtx(2,1) * det4_3456_0256 +  mtx(2,2) * det4_3456_0156 -  mtx(2,5) * det4_3456_0126 +  mtx(2,6) * det4_3456_0125;
	ELEM_TYPE det5_23456_01345 =  mtx(2,0) * det4_3456_1345 -  mtx(2,1) * det4_3456_0345 +  mtx(2,3) * det4_3456_0145 -  mtx(2,4) * det4_3456_0135 +  mtx(2,5) * det4_3456_0134;
	ELEM_TYPE det5_23456_01346 =  mtx(2,0) * det4_3456_1346 -  mtx(2,1) * det4_3456_0346 +  mtx(2,3) * det4_3456_0146 -  mtx(2,4) * det4_3456_0136 +  mtx(2,6) * det4_3456_0134;
	ELEM_TYPE det5_23456_01356 =  mtx(2,0) * det4_3456_1356 -  mtx(2,1) * det4_3456_0356 +  mtx(2,3) * det4_3456_0156 -  mtx(2,5) * det4_3456_0136 +  mtx(2,6) * det4_3456_0135;
	ELEM_TYPE det5_23456_01456 =  mtx(2,0) * det4_3456_1456 -  mtx(2,1) * det4_3456_0456 +  mtx(2,4) * det4_3456_0156 -  mtx(2,5) * det4_3456_0146 +  mtx(2,6) * det4_3456_0145;
	ELEM_TYPE det5_23456_02345 =  mtx(2,0) * det4_3456_2345 -  mtx(2,2) * det4_3456_0345 +  mtx(2,3) * det4_3456_0245 -  mtx(2,4) * det4_3456_0235 +  mtx(2,5) * det4_3456_0234;
	ELEM_TYPE det5_23456_02346 =  mtx(2,0) * det4_3456_2346 -  mtx(2,2) * det4_3456_0346 +  mtx(2,3) * det4_3456_0246 -  mtx(2,4) * det4_3456_0236 +  mtx(2,6) * det4_3456_0234;
	ELEM_TYPE det5_23456_02356 =  mtx(2,0) * det4_3456_2356 -  mtx(2,2) * det4_3456_0356 +  mtx(2,3) * det4_3456_0256 -  mtx(2,5) * det4_3456_0236 +  mtx(2,6) * det4_3456_0235;
	ELEM_TYPE det5_23456_02456 =  mtx(2,0) * det4_3456_2456 -  mtx(2,2) * det4_3456_0456 +  mtx(2,4) * det4_3456_0256 -  mtx(2,5) * det4_3456_0246 +  mtx(2,6) * det4_3456_0245;
	ELEM_TYPE det5_23456_03456 =  mtx(2,0) * det4_3456_3456 -  mtx(2,3) * det4_3456_0456 +  mtx(2,4) * det4_3456_0356 -  mtx(2,5) * det4_3456_0346 +  mtx(2,6) * det4_3456_0345;
	ELEM_TYPE det5_23456_12345 =  mtx(2,1) * det4_3456_2345 -  mtx(2,2) * det4_3456_1345 +  mtx(2,3) * det4_3456_1245 -  mtx(2,4) * det4_3456_1235 +  mtx(2,5) * det4_3456_1234;
	ELEM_TYPE det5_23456_12346 =  mtx(2,1) * det4_3456_2346 -  mtx(2,2) * det4_3456_1346 +  mtx(2,3) * det4_3456_1246 -  mtx(2,4) * det4_3456_1236 +  mtx(2,6) * det4_3456_1234;
	ELEM_TYPE det5_23456_12356 =  mtx(2,1) * det4_3456_2356 -  mtx(2,2) * det4_3456_1356 +  mtx(2,3) * det4_3456_1256 -  mtx(2,5) * det4_3456_1236 +  mtx(2,6) * det4_3456_1235;
	ELEM_TYPE det5_23456_12456 =  mtx(2,1) * det4_3456_2456 -  mtx(2,2) * det4_3456_1456 +  mtx(2,4) * det4_3456_1256 -  mtx(2,5) * det4_3456_1246 +  mtx(2,6) * det4_3456_1245;
	ELEM_TYPE det5_23456_13456 =  mtx(2,1) * det4_3456_3456 -  mtx(2,3) * det4_3456_1456 +  mtx(2,4) * det4_3456_1356 -  mtx(2,5) * det4_3456_1346 +  mtx(2,6) * det4_3456_1345;
	ELEM_TYPE det5_23456_23456 =  mtx(2,2) * det4_3456_3456 -  mtx(2,3) * det4_3456_2456 +  mtx(2,4) * det4_3456_2356 -  mtx(2,5) * det4_3456_2346 +  mtx(2,6) * det4_3456_2345;

	ELEM_TYPE det6_012345_012345 =  mtx(0,0) * det5_12345_12345 -  mtx(0,1) * det5_12345_02345 +  mtx(0,2) * det5_12345_01345 -  mtx(0,3) * det5_12345_01245 +  mtx(0,4) * det5_12345_01235 -  mtx(0,5) * det5_12345_01234;
	ELEM_TYPE det6_012345_012346 =  mtx(0,0) * det5_12345_12346 -  mtx(0,1) * det5_12345_02346 +  mtx(0,2) * det5_12345_01346 -  mtx(0,3) * det5_12345_01246 +  mtx(0,4) * det5_12345_01236 -  mtx(0,6) * det5_12345_01234;
	ELEM_TYPE det6_012345_012356 =  mtx(0,0) * det5_12345_12356 -  mtx(0,1) * det5_12345_02356 +  mtx(0,2) * det5_12345_01356 -  mtx(0,3) * det5_12345_01256 +  mtx(0,5) * det5_12345_01236 -  mtx(0,6) * det5_12345_01235;
	ELEM_TYPE det6_012345_012456 =  mtx(0,0) * det5_12345_12456 -  mtx(0,1) * det5_12345_02456 +  mtx(0,2) * det5_12345_01456 -  mtx(0,4) * det5_12345_01256 +  mtx(0,5) * det5_12345_01246 -  mtx(0,6) * det5_12345_01245;
	ELEM_TYPE det6_012345_013456 =  mtx(0,0) * det5_12345_13456 -  mtx(0,1) * det5_12345_03456 +  mtx(0,3) * det5_12345_01456 -  mtx(0,4) * det5_12345_01356 +  mtx(0,5) * det5_12345_01346 -  mtx(0,6) * det5_12345_01345;
	ELEM_TYPE det6_012345_023456 =  mtx(0,0) * det5_12345_23456 -  mtx(0,2) * det5_12345_03456 +  mtx(0,3) * det5_12345_02456 -  mtx(0,4) * det5_12345_02356 +  mtx(0,5) * det5_12345_02346 -  mtx(0,6) * det5_12345_02345;
	ELEM_TYPE det6_012345_123456 =  mtx(0,1) * det5_12345_23456 -  mtx(0,2) * det5_12345_13456 +  mtx(0,3) * det5_12345_12456 -  mtx(0,4) * det5_12345_12356 +  mtx(0,5) * det5_12345_12346 -  mtx(0,6) * det5_12345_12345;
	ELEM_TYPE det6_012346_012345 =  mtx(0,0) * det5_12346_12345 -  mtx(0,1) * det5_12346_02345 +  mtx(0,2) * det5_12346_01345 -  mtx(0,3) * det5_12346_01245 +  mtx(0,4) * det5_12346_01235 -  mtx(0,5) * det5_12346_01234;
	ELEM_TYPE det6_012346_012346 =  mtx(0,0) * det5_12346_12346 -  mtx(0,1) * det5_12346_02346 +  mtx(0,2) * det5_12346_01346 -  mtx(0,3) * det5_12346_01246 +  mtx(0,4) * det5_12346_01236 -  mtx(0,6) * det5_12346_01234;
	ELEM_TYPE det6_012346_012356 =  mtx(0,0) * det5_12346_12356 -  mtx(0,1) * det5_12346_02356 +  mtx(0,2) * det5_12346_01356 -  mtx(0,3) * det5_12346_01256 +  mtx(0,5) * det5_12346_01236 -  mtx(0,6) * det5_12346_01235;
	ELEM_TYPE det6_012346_012456 =  mtx(0,0) * det5_12346_12456 -  mtx(0,1) * det5_12346_02456 +  mtx(0,2) * det5_12346_01456 -  mtx(0,4) * det5_12346_01256 +  mtx(0,5) * det5_12346_01246 -  mtx(0,6) * det5_12346_01245;
	ELEM_TYPE det6_012346_013456 =  mtx(0,0) * det5_12346_13456 -  mtx(0,1) * det5_12346_03456 +  mtx(0,3) * det5_12346_01456 -  mtx(0,4) * det5_12346_01356 +  mtx(0,5) * det5_12346_01346 -  mtx(0,6) * det5_12346_01345;
	ELEM_TYPE det6_012346_023456 =  mtx(0,0) * det5_12346_23456 -  mtx(0,2) * det5_12346_03456 +  mtx(0,3) * det5_12346_02456 -  mtx(0,4) * det5_12346_02356 +  mtx(0,5) * det5_12346_02346 -  mtx(0,6) * det5_12346_02345;
	ELEM_TYPE det6_012346_123456 =  mtx(0,1) * det5_12346_23456 -  mtx(0,2) * det5_12346_13456 +  mtx(0,3) * det5_12346_12456 -  mtx(0,4) * det5_12346_12356 +  mtx(0,5) * det5_12346_12346 -  mtx(0,6) * det5_12346_12345;
	ELEM_TYPE det6_012356_012345 =  mtx(0,0) * det5_12356_12345 -  mtx(0,1) * det5_12356_02345 +  mtx(0,2) * det5_12356_01345 -  mtx(0,3) * det5_12356_01245 +  mtx(0,4) * det5_12356_01235 -  mtx(0,5) * det5_12356_01234;
	ELEM_TYPE det6_012356_012346 =  mtx(0,0) * det5_12356_12346 -  mtx(0,1) * det5_12356_02346 +  mtx(0,2) * det5_12356_01346 -  mtx(0,3) * det5_12356_01246 +  mtx(0,4) * det5_12356_01236 -  mtx(0,6) * det5_12356_01234;
	ELEM_TYPE det6_012356_012356 =  mtx(0,0) * det5_12356_12356 -  mtx(0,1) * det5_12356_02356 +  mtx(0,2) * det5_12356_01356 -  mtx(0,3) * det5_12356_01256 +  mtx(0,5) * det5_12356_01236 -  mtx(0,6) * det5_12356_01235;
	ELEM_TYPE det6_012356_012456 =  mtx(0,0) * det5_12356_12456 -  mtx(0,1) * det5_12356_02456 +  mtx(0,2) * det5_12356_01456 -  mtx(0,4) * det5_12356_01256 +  mtx(0,5) * det5_12356_01246 -  mtx(0,6) * det5_12356_01245;
	ELEM_TYPE det6_012356_013456 =  mtx(0,0) * det5_12356_13456 -  mtx(0,1) * det5_12356_03456 +  mtx(0,3) * det5_12356_01456 -  mtx(0,4) * det5_12356_01356 +  mtx(0,5) * det5_12356_01346 -  mtx(0,6) * det5_12356_01345;
	ELEM_TYPE det6_012356_023456 =  mtx(0,0) * det5_12356_23456 -  mtx(0,2) * det5_12356_03456 +  mtx(0,3) * det5_12356_02456 -  mtx(0,4) * det5_12356_02356 +  mtx(0,5) * det5_12356_02346 -  mtx(0,6) * det5_12356_02345;
	ELEM_TYPE det6_012356_123456 =  mtx(0,1) * det5_12356_23456 -  mtx(0,2) * det5_12356_13456 +  mtx(0,3) * det5_12356_12456 -  mtx(0,4) * det5_12356_12356 +  mtx(0,5) * det5_12356_12346 -  mtx(0,6) * det5_12356_12345;
	ELEM_TYPE det6_012456_012345 =  mtx(0,0) * det5_12456_12345 -  mtx(0,1) * det5_12456_02345 +  mtx(0,2) * det5_12456_01345 -  mtx(0,3) * det5_12456_01245 +  mtx(0,4) * det5_12456_01235 -  mtx(0,5) * det5_12456_01234;
	ELEM_TYPE det6_012456_012346 =  mtx(0,0) * det5_12456_12346 -  mtx(0,1) * det5_12456_02346 +  mtx(0,2) * det5_12456_01346 -  mtx(0,3) * det5_12456_01246 +  mtx(0,4) * det5_12456_01236 -  mtx(0,6) * det5_12456_01234;
	ELEM_TYPE det6_012456_012356 =  mtx(0,0) * det5_12456_12356 -  mtx(0,1) * det5_12456_02356 +  mtx(0,2) * det5_12456_01356 -  mtx(0,3) * det5_12456_01256 +  mtx(0,5) * det5_12456_01236 -  mtx(0,6) * det5_12456_01235;
	ELEM_TYPE det6_012456_012456 =  mtx(0,0) * det5_12456_12456 -  mtx(0,1) * det5_12456_02456 +  mtx(0,2) * det5_12456_01456 -  mtx(0,4) * det5_12456_01256 +  mtx(0,5) * det5_12456_01246 -  mtx(0,6) * det5_12456_01245;
	ELEM_TYPE det6_012456_013456 =  mtx(0,0) * det5_12456_13456 -  mtx(0,1) * det5_12456_03456 +  mtx(0,3) * det5_12456_01456 -  mtx(0,4) * det5_12456_01356 +  mtx(0,5) * det5_12456_01346 -  mtx(0,6) * det5_12456_01345;
	ELEM_TYPE det6_012456_023456 =  mtx(0,0) * det5_12456_23456 -  mtx(0,2) * det5_12456_03456 +  mtx(0,3) * det5_12456_02456 -  mtx(0,4) * det5_12456_02356 +  mtx(0,5) * det5_12456_02346 -  mtx(0,6) * det5_12456_02345;
	ELEM_TYPE det6_012456_123456 =  mtx(0,1) * det5_12456_23456 -  mtx(0,2) * det5_12456_13456 +  mtx(0,3) * det5_12456_12456 -  mtx(0,4) * det5_12456_12356 +  mtx(0,5) * det5_12456_12346 -  mtx(0,6) * det5_12456_12345;
	ELEM_TYPE det6_013456_012345 =  mtx(0,0) * det5_13456_12345 -  mtx(0,1) * det5_13456_02345 +  mtx(0,2) * det5_13456_01345 -  mtx(0,3) * det5_13456_01245 +  mtx(0,4) * det5_13456_01235 -  mtx(0,5) * det5_13456_01234;
	ELEM_TYPE det6_013456_012346 =  mtx(0,0) * det5_13456_12346 -  mtx(0,1) * det5_13456_02346 +  mtx(0,2) * det5_13456_01346 -  mtx(0,3) * det5_13456_01246 +  mtx(0,4) * det5_13456_01236 -  mtx(0,6) * det5_13456_01234;
	ELEM_TYPE det6_013456_012356 =  mtx(0,0) * det5_13456_12356 -  mtx(0,1) * det5_13456_02356 +  mtx(0,2) * det5_13456_01356 -  mtx(0,3) * det5_13456_01256 +  mtx(0,5) * det5_13456_01236 -  mtx(0,6) * det5_13456_01235;
	ELEM_TYPE det6_013456_012456 =  mtx(0,0) * det5_13456_12456 -  mtx(0,1) * det5_13456_02456 +  mtx(0,2) * det5_13456_01456 -  mtx(0,4) * det5_13456_01256 +  mtx(0,5) * det5_13456_01246 -  mtx(0,6) * det5_13456_01245;
	ELEM_TYPE det6_013456_013456 =  mtx(0,0) * det5_13456_13456 -  mtx(0,1) * det5_13456_03456 +  mtx(0,3) * det5_13456_01456 -  mtx(0,4) * det5_13456_01356 +  mtx(0,5) * det5_13456_01346 -  mtx(0,6) * det5_13456_01345;
	ELEM_TYPE det6_013456_023456 =  mtx(0,0) * det5_13456_23456 -  mtx(0,2) * det5_13456_03456 +  mtx(0,3) * det5_13456_02456 -  mtx(0,4) * det5_13456_02356 +  mtx(0,5) * det5_13456_02346 -  mtx(0,6) * det5_13456_02345;
	ELEM_TYPE det6_013456_123456 =  mtx(0,1) * det5_13456_23456 -  mtx(0,2) * det5_13456_13456 +  mtx(0,3) * det5_13456_12456 -  mtx(0,4) * det5_13456_12356 +  mtx(0,5) * det5_13456_12346 -  mtx(0,6) * det5_13456_12345;
	ELEM_TYPE det6_023456_012345 =  mtx(0,0) * det5_23456_12345 -  mtx(0,1) * det5_23456_02345 +  mtx(0,2) * det5_23456_01345 -  mtx(0,3) * det5_23456_01245 +  mtx(0,4) * det5_23456_01235 -  mtx(0,5) * det5_23456_01234;
	ELEM_TYPE det6_023456_012346 =  mtx(0,0) * det5_23456_12346 -  mtx(0,1) * det5_23456_02346 +  mtx(0,2) * det5_23456_01346 -  mtx(0,3) * det5_23456_01246 +  mtx(0,4) * det5_23456_01236 -  mtx(0,6) * det5_23456_01234;
	ELEM_TYPE det6_023456_012356 =  mtx(0,0) * det5_23456_12356 -  mtx(0,1) * det5_23456_02356 +  mtx(0,2) * det5_23456_01356 -  mtx(0,3) * det5_23456_01256 +  mtx(0,5) * det5_23456_01236 -  mtx(0,6) * det5_23456_01235;
	ELEM_TYPE det6_023456_012456 =  mtx(0,0) * det5_23456_12456 -  mtx(0,1) * det5_23456_02456 +  mtx(0,2) * det5_23456_01456 -  mtx(0,4) * det5_23456_01256 +  mtx(0,5) * det5_23456_01246 -  mtx(0,6) * det5_23456_01245;
	ELEM_TYPE det6_023456_013456 =  mtx(0,0) * det5_23456_13456 -  mtx(0,1) * det5_23456_03456 +  mtx(0,3) * det5_23456_01456 -  mtx(0,4) * det5_23456_01356 +  mtx(0,5) * det5_23456_01346 -  mtx(0,6) * det5_23456_01345;
	ELEM_TYPE det6_023456_023456 =  mtx(0,0) * det5_23456_23456 -  mtx(0,2) * det5_23456_03456 +  mtx(0,3) * det5_23456_02456 -  mtx(0,4) * det5_23456_02356 +  mtx(0,5) * det5_23456_02346 -  mtx(0,6) * det5_23456_02345;
	ELEM_TYPE det6_023456_123456 =  mtx(0,1) * det5_23456_23456 -  mtx(0,2) * det5_23456_13456 +  mtx(0,3) * det5_23456_12456 -  mtx(0,4) * det5_23456_12356 +  mtx(0,5) * det5_23456_12346 -  mtx(0,6) * det5_23456_12345;
	ELEM_TYPE det6_123456_012345 =  mtx(1,0) * det5_23456_12345 -  mtx(1,1) * det5_23456_02345 +  mtx(1,2) * det5_23456_01345 -  mtx(1,3) * det5_23456_01245 +  mtx(1,4) * det5_23456_01235 -  mtx(1,5) * det5_23456_01234;
	ELEM_TYPE det6_123456_012346 =  mtx(1,0) * det5_23456_12346 -  mtx(1,1) * det5_23456_02346 +  mtx(1,2) * det5_23456_01346 -  mtx(1,3) * det5_23456_01246 +  mtx(1,4) * det5_23456_01236 -  mtx(1,6) * det5_23456_01234;
	ELEM_TYPE det6_123456_012356 =  mtx(1,0) * det5_23456_12356 -  mtx(1,1) * det5_23456_02356 +  mtx(1,2) * det5_23456_01356 -  mtx(1,3) * det5_23456_01256 +  mtx(1,5) * det5_23456_01236 -  mtx(1,6) * det5_23456_01235;
	ELEM_TYPE det6_123456_012456 =  mtx(1,0) * det5_23456_12456 -  mtx(1,1) * det5_23456_02456 +  mtx(1,2) * det5_23456_01456 -  mtx(1,4) * det5_23456_01256 +  mtx(1,5) * det5_23456_01246 -  mtx(1,6) * det5_23456_01245;
	ELEM_TYPE det6_123456_013456 =  mtx(1,0) * det5_23456_13456 -  mtx(1,1) * det5_23456_03456 +  mtx(1,3) * det5_23456_01456 -  mtx(1,4) * det5_23456_01356 +  mtx(1,5) * det5_23456_01346 -  mtx(1,6) * det5_23456_01345;
	ELEM_TYPE det6_123456_023456 =  mtx(1,0) * det5_23456_23456 -  mtx(1,2) * det5_23456_03456 +  mtx(1,3) * det5_23456_02456 -  mtx(1,4) * det5_23456_02356 +  mtx(1,5) * det5_23456_02346 -  mtx(1,6) * det5_23456_02345;
	ELEM_TYPE det6_123456_123456 =  mtx(1,1) * det5_23456_23456 -  mtx(1,2) * det5_23456_13456 +  mtx(1,3) * det5_23456_12456 -  mtx(1,4) * det5_23456_12356 +  mtx(1,5) * det5_23456_12346 -  mtx(1,6) * det5_23456_12345;

	ELEM_TYPE det7_0123456_0123456 =  mtx(0,0) * det6_123456_123456 -  mtx(0,1) * det6_123456_023456 +  mtx(0,2) * det6_123456_013456 -  mtx(0,3) * det6_123456_012456 +  mtx(0,4) * det6_123456_012356 -  mtx(0,5) * det6_123456_012346 +  mtx(0,6) * det6_123456_012345;

	ELEM_TYPE det = ELEM_TYPE( 1.0/det7_0123456_0123456);

	(*this)(0,0) =   det6_123456_123456 * det;
	(*this)(0,1) = - det6_023456_123456 * det;
	(*this)(0,2) =   det6_013456_123456 * det;
	(*this)(0,3) = - det6_012456_123456 * det;
	(*this)(0,4) =   det6_012356_123456 * det;
	(*this)(0,5) = - det6_012346_123456 * det;
	(*this)(0,6) =   det6_012345_123456 * det;

	(*this)(1,0) = - det6_123456_023456 * det;
	(*this)(1,1) =   det6_023456_023456 * det;
	(*this)(1,2) = - det6_013456_023456 * det;
	(*this)(1,3) =   det6_012456_023456 * det;
	(*this)(1,4) = - det6_012356_023456 * det;
	(*this)(1,5) =   det6_012346_023456 * det;
	(*this)(1,6) = - det6_012345_023456 * det;

	(*this)(2,0) =   det6_123456_013456 * det;
	(*this)(2,1) = - det6_023456_013456 * det;
	(*this)(2,2) =   det6_013456_013456 * det;
	(*this)(2,3) = - det6_012456_013456 * det;
	(*this)(2,4) =   det6_012356_013456 * det;
	(*this)(2,5) = - det6_012346_013456 * det;
	(*this)(2,6) =   det6_012345_013456 * det;

	(*this)(3,0) = - det6_123456_012456 * det;
	(*this)(3,1) =   det6_023456_012456 * det;
	(*this)(3,2) = - det6_013456_012456 * det;
	(*this)(3,3) =   det6_012456_012456 * det;
	(*this)(3,4) = - det6_012356_012456 * det;
	(*this)(3,5) =   det6_012346_012456 * det;
	(*this)(3,6) = - det6_012345_012456 * det;

	(*this)(4,0) =   det6_123456_012356 * det;
	(*this)(4,1) = - det6_023456_012356 * det;
	(*this)(4,2) =   det6_013456_012356 * det;
	(*this)(4,3) = - det6_012456_012356 * det;
	(*this)(4,4) =   det6_012356_012356 * det;
	(*this)(4,5) = - det6_012346_012356 * det;
	(*this)(4,6) =   det6_012345_012356 * det;

	(*this)(5,0) = - det6_123456_012346 * det;
	(*this)(5,1) =   det6_023456_012346 * det;
	(*this)(5,2) = - det6_013456_012346 * det;
	(*this)(5,3) =   det6_012456_012346 * det;
	(*this)(5,4) = - det6_012356_012346 * det;
	(*this)(5,5) =   det6_012346_012346 * det;
	(*this)(5,6) = - det6_012345_012346 * det;

	(*this)(6,0) =   det6_123456_012345 * det;
	(*this)(6,1) = - det6_023456_012345 * det;
	(*this)(6,2) =   det6_013456_012345 * det;
	(*this)(6,3) = - det6_012456_012345 * det;
	(*this)(6,4) =   det6_012356_012345 * det;
	(*this)(6,5) = - det6_012346_012345 * det;
	(*this)(6,6) =   det6_012345_012345 * det;

}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::Invert()
{
	CHECK( mNRows == mNCols);

	switch ( mNRows)
	{
	case 2:
		Invert2x2();
		return;

	case 3:
		Invert3x3();
		return;

	case 4:
		Invert4x4();
		return;

	case 5:
		Invert5x5();
		return;

	case 6:
		Invert6x6();
		return;
	}


	SMatrix< MAX_SIZE, ELEM_TYPE> mtemp = (*this);
	InitDelta( mNRows);

	QRDecomp< ELEM_TYPE, SMatrix< MAX_SIZE, ELEM_TYPE> >	qrd( mtemp );

	qrd.Execute();
	qrd.Solve( *this);

	//mtemp.LUSolve( *this);
}



template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::Write( ostream& f) const
{
	/*f << "+-";
	for( MGSize j=0; j<mNCols; ++j)
		f << "-------------";
	f << "-+" << endl;
*/
	for( MGSize i=0; i<mNRows; ++i)
	{
		//f << "| ";
		for( MGSize j=0; j<mNCols; ++j)
			f << setprecision(5) << setw(12) << (*this)(i,j) << " ";
		//f << " |" << endl;
	}

	/*f << "+-";
	for( MGSize j=0; j<mNCols; ++j)
		f << "-------------";
	f << "-+" << endl;*/
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline ELEM_TYPE SMatrix< MAX_SIZE, ELEM_TYPE>::Determinant2x2() const
{
	CHECK( mNRows == mNCols == 2);

	return	(*this)(0,0)*(*this)(1,1) - (*this)(0,1)*(*this)(1,0) ;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline ELEM_TYPE SMatrix< MAX_SIZE, ELEM_TYPE>::Determinant3x3() const
{
	CHECK( mNRows == mNCols == 3);

	return	(*this)(0,0)*( (*this)(1,1)*(*this)(2,2) - (*this)(1,2)*(*this)(2,1) ) -
			(*this)(0,1)*( (*this)(1,0)*(*this)(2,2) - (*this)(1,2)*(*this)(2,0) ) +
			(*this)(0,2)*( (*this)(1,0)*(*this)(2,1) - (*this)(1,1)*(*this)(2,0) );

}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline ELEM_TYPE SMatrix< MAX_SIZE, ELEM_TYPE>::Determinant4x4() const
{
	CHECK( mNRows == mNCols == 4);
	THROW_INTERNAL( "Not implemented!");
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline ELEM_TYPE SMatrix< MAX_SIZE, ELEM_TYPE>::Determinant() const
{
	CHECK( mNRows == mNCols);

	switch ( mNRows )
	{
	case 1:
		return (*this)(0,0);

	case 2:
		return Determinant2x2();

	case 3:
		return Determinant3x3();
	}

	THROW_INTERNAL( "Not implemented! mNRows = " << mNRows);
	return 0.0;

	//SMatrix< MAX_SIZE, ELEM_TYPE> mtx = *this;
	//vector<MGSize> indx;

	//mtx.LUDecomp( indx);

	//ELEM_TYPE	dtemp = 1.0;
	//for( MGSize i=0; i<mNRows; ++i)
	//{
	//	dtemp *= mtx(i,i);
	//}
	//dtemp *= (mNRows%2?1:-1);
	//return dtemp;
}

//////////////////////////////////////////////////////////////////////

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::Decompose( SVector< MAX_SIZE, ELEM_TYPE>& vecD, SMatrix< MAX_SIZE, ELEM_TYPE>& mtxL, const bool& bsort)
{
	DecomposeNew( vecD, mtxL, bsort);
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::Decompose( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxD, SMatrix< MAX_SIZE, ELEM_TYPE>& mtxL, const bool& bsort)
{
	SVector< MAX_SIZE, ELEM_TYPE> vecD;

	mtxD.InitDelta( mNRows);

	DecomposeNew( vecD, mtxL, bsort);

	for ( MGSize ip=0; ip<mNRows; ++ip) 
		mtxD(ip,ip) = vecD(ip);

}

//////////////////////////////////////////////////////////////////////

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::RotateMatrix( 
		SMatrix< MAX_SIZE, ELEM_TYPE>& a, 
		ELEM_TYPE& g, ELEM_TYPE& h, ELEM_TYPE& s, ELEM_TYPE& c, ELEM_TYPE& tau, 
		const MGInt& i, const MGInt& j, const MGInt& k, const MGInt& l
		)
{
	g = a(i,j);
	h = a(k,l);
	a(i,j) = g - s*( h+g*tau );
	a(k,l) = h + s*( g-h*tau );
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::DecomposeOld( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxD, SMatrix< MAX_SIZE, ELEM_TYPE>& mtxL, const bool& bsort)
{
	CHECK( mNRows == mNCols);
	

	MGSize i, k;
	ELEM_TYPE tresh, theta, tau, t, sm, s, h, g, c, tmp;

	SVector< MAX_SIZE, ELEM_TYPE>	b(mNRows);
	SVector< MAX_SIZE, ELEM_TYPE>	z(mNRows);
	SVector< MAX_SIZE, ELEM_TYPE>	w(mNRows);


	// initialize
	mtxL.InitDelta( mNRows);
	mtxD.InitDelta( mNRows);

	for ( MGSize ip=0; ip<mNRows; ++ip) 
	{
		b(ip) = w(ip) = (*this)(ip,ip);
		z(ip) = 0.0;
	}

	// begin rotation sequence
	for ( i=0; i<SMTX_MAX_ROTATIONS; ++i) 
	{
		sm = 0.0;
		for ( MGSize ip=0; ip<mNRows-1; ++ip) 
		{
			for ( MGSize iq=ip+1; iq<mNRows; ++iq)
				sm += ::fabs( (*this)(ip,iq) );
		}

		if ( ::fabs( sm) < ZERO )
			break;

		if (i < 3)                                // first 3 sweeps
			tresh = 0.2*sm/(mNRows*mNRows);
		else
			tresh = 0.0;


		for ( MGInt ip=0; ip<(MGInt)mNRows-1; ++ip) 
		{
			for ( MGInt iq=ip+1; iq<(MGInt)mNRows; ++iq) 
			{
				g = 100.0 * ::fabs( (*this)(ip,iq) );

				// after 4 sweeps
				// if ( i > 3 && ( ::fabs(w(ip))+g) == ::fabs(w(ip)) && (::fabs(w(iq))+g) == ::fabs(w(iq)))
				if ( i > 4 &&  ::fabs( ::fabs(w(ip))+g - ::fabs(w(ip)) ) < ZERO  && ::fabs( ::fabs(w(iq))+g - ::fabs(w(iq))) < ZERO )
				{
					(*this)(ip,iq) = 0.0;
				}
				else if ( ::fabs( (*this)(ip,iq) ) > tresh) 
				{
					h = w(iq) - w(ip);
					if ( ::fabs( ::fabs(h)+g - ::fabs(h)) < ZERO )
					{
						t = ( (*this)(ip,iq) ) / h;
					} 
					else 
					{
						theta = 0.5*h / ( (*this)(ip,iq) );
						t = 1.0 / ( ::fabs(theta) + ::sqrt( 1.0+theta*theta) );
						if ( theta < 0.0)
						{
							t = -t;
						}
					}
					c = 1.0 / ::sqrt(1+t*t);
					s = t*c;
					tau = s/(1.0+c);
					h = t*(*this)(ip,iq);
					z(ip) -= h;
					z(iq) += h;
					w(ip) -= h;
					w(iq) += h;
					(*this)(ip,iq) = 0.0;

					// ip already shifted left by 1 unit
					for ( MGInt j = 0; j <= (MGInt)ip-1; ++j) 
						RotateMatrix( *this, g, h, s, c, tau, j, ip, j, iq );

					// ip and iq already shifted left by 1 unit
					for ( MGInt j = ip+1; j <= (MGInt)iq-1; ++j) 
						RotateMatrix( *this, g, h, s, c, tau, ip, j, j, iq );

					// iq already shifted left by 1 unit
					for ( MGInt j=iq+1; j<(MGInt)mNRows; ++j) 
						RotateMatrix( *this, g, h, s, c, tau, ip, j, iq, j );

					for ( MGInt j=0; j<(MGInt)mNRows; ++j) 
						RotateMatrix( mtxL, g, h, s, c, tau, j, ip, j, iq );

				}
			}

		}

		for ( MGSize ip=0; ip<mNRows; ++ip) 
		{
			b(ip) += z(ip);
			w(ip) = b(ip);
			z(ip) = 0.0;
		}
	}


	// this is NEVER called
	if ( i >= SMTX_MAX_ROTATIONS )
	{
		Write();
		THROW_INTERNAL( "SMatrix Decompose error: Error extracting eigenfunctions");
	}

	if ( bsort)
	{
		// sort eigenfunctions                 these changes do not affect accuracy 
		for ( MGSize j=0; j<mNRows-1; ++j)                  // boundary incorrect
		{
			k = j;
			tmp = w(k);
			for ( MGSize i=j+1; i<mNRows; ++i)                // boundary incorrect, shifted already
			{
				if (w(i) >= tmp)                   // why exchage if same?
				{
					k = i;
					tmp = w(k);
				}
			}
			if ( k != j) 
			{
				w(k) = w(j);
				w(j) = tmp;
				for ( MGSize i=0; i<mNRows; ++i) 
				{
					tmp = mtxL(i,j);
					mtxL(i,j) = mtxL(i,k);
					mtxL(i,k) = tmp;
				}
			}
		}
	}


	for ( MGSize ip=0; ip<mNRows; ++ip) 
		mtxD(ip,ip) = w(ip);
	return;


	// insure eigenvector consistency (i.e., Jacobi can compute vectors that
	// are negative of one another (.707,.707,0) and (-.707,-.707,0). This can
	// reek havoc in hyperstreamline/other stuff. We will select the most
	// positive eigenvector.
	MGInt  ceil_half_n = ((MGInt)mNRows >> 1) + ((MGInt)mNRows & 1);
	MGInt numPos;

	for ( MGInt j=0; j<(MGInt)mNRows; j++)
	{
		for ( numPos=0, i=0; i<mNRows; ++i)
		{
			if ( mtxL(i,j) >= 0.0 )
			{
				++numPos;
			}
		}
	//    if ( numPos < ceil(double(n)/double(2.0)) )
		if ( numPos < ceil_half_n)
		{
			for( MGInt i=0; i<(MGInt)mNRows; ++i)
			{
				mtxL(i,j) *= -1.0;
			}
		}
	}

	for ( MGSize ip=0; ip<mNRows; ++ip) 
		mtxD(ip,ip) = w(ip);

}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::DecomposeOld( SVector< MAX_SIZE, ELEM_TYPE>& mtxD, SMatrix< MAX_SIZE, ELEM_TYPE>& mtxL, const bool& bsort)
{
	CHECK( mNRows == mNCols);
	

	MGSize i, k;
	ELEM_TYPE tresh, theta, tau, t, sm, s, h, g, c, tmp;

	SVector< MAX_SIZE, ELEM_TYPE>	b(mNRows);
	SVector< MAX_SIZE, ELEM_TYPE>	z(mNRows);
	SVector< MAX_SIZE, ELEM_TYPE>	w(mNRows);


	// initialize
	mtxL.InitDelta( mNRows);
	//mtxD.InitDelta( mNRows);
	mtxD.Resize( mNRows);

	for ( MGSize ip=0; ip<mNRows; ++ip) 
	{
		b(ip) = w(ip) = (*this)(ip,ip);
		z(ip) = 0.0;
	}

	// begin rotation sequence
	for ( i=0; i<SMTX_MAX_ROTATIONS; ++i) 
	{
		sm = 0.0;
		for ( MGSize ip=0; ip<mNRows-1; ++ip) 
		{
			for ( MGSize iq=ip+1; iq<mNRows; ++iq)
			{
				sm += ::fabs( (*this)(ip,iq) );
			}
		}

		if ( ::fabs( sm) < ZERO )
			break;

		if (i < 3)                                // first 3 sweeps
			tresh = 0.2*sm/(mNRows*mNRows);
		else
			tresh = 0.0;


		for ( MGInt ip=0; ip<(MGInt)mNRows-1; ++ip) 
		{
			for ( MGInt iq=ip+1; iq<(MGInt)mNRows; ++iq) 
			{
				g = 100.0 * ::fabs( (*this)(ip,iq) );

				// after 4 sweeps
				// if ( i > 3 && ( ::fabs(w(ip))+g) == ::fabs(w(ip)) && (::fabs(w(iq))+g) == ::fabs(w(iq)))
				if ( i > 4 &&  ::fabs( ::fabs(w(ip))+g - ::fabs(w(ip)) ) < ZERO  && ::fabs( ::fabs(w(iq))+g - ::fabs(w(iq))) < ZERO )
				{
					(*this)(ip,iq) = 0.0;
				}
				else if ( ::fabs( (*this)(ip,iq) ) > tresh) 
				{
					h = w(iq) - w(ip);
					if ( ::fabs( ::fabs(h)+g - ::fabs(h)) < ZERO )
					{
						t = ( (*this)(ip,iq) ) / h;
					} 
					else 
					{
						theta = 0.5*h / ( (*this)(ip,iq) );
						t = 1.0 / ( ::fabs(theta) + ::sqrt( 1.0+theta*theta) );
						if ( theta < 0.0)
						{
							t = -t;
						}
					}
					c = 1.0 / ::sqrt(1+t*t);
					s = t*c;
					tau = s/(1.0+c);
					h = t*(*this)(ip,iq);
					z(ip) -= h;
					z(iq) += h;
					w(ip) -= h;
					w(iq) += h;
					(*this)(ip,iq) = 0.0;

					// ip already shifted left by 1 unit
					for ( MGInt j = 0; j <= (MGInt)ip-1; ++j) 
					{
						RotateMatrix( *this, g, h, s, c, tau, j, ip, j, iq );
					}

					// ip and iq already shifted left by 1 unit
					for ( MGInt j = ip+1; j <= (MGInt)iq-1; ++j) 
					{
						RotateMatrix( *this, g, h, s, c, tau, ip, j, j, iq );
					}

					// iq already shifted left by 1 unit
					for ( MGInt j=iq+1; j<(MGInt)mNRows; ++j) 
					{
						RotateMatrix( *this, g, h, s, c, tau, ip, j, iq, j );
					}

					for ( MGInt j=0; j<(MGInt)mNRows; ++j) 
					{
						RotateMatrix( mtxL, g, h, s, c, tau, j, ip, j, iq );
					}

				}
			}

		}

		for ( MGSize ip=0; ip<mNRows; ++ip) 
		{
			b(ip) += z(ip);
			w(ip) = b(ip);
			z(ip) = 0.0;
		}
	}


	// this is NEVER called
	if ( i >= SMTX_MAX_ROTATIONS )
	{
		Write();
		THROW_INTERNAL( "SMatrix Decompose error: Error extracting eigenfunctions");
	}


	if ( bsort)
	{
		// sort eigenfunctions                 these changes do not affect accuracy 
		for ( MGSize j=0; j<mNRows-1; ++j)                  // boundary incorrect
		{
			k = j;
			tmp = w(k);
			for ( MGSize i=j+1; i<mNRows; ++i)                // boundary incorrect, shifted already
			{
				if (w(i) >= tmp)                   // why exchage if same?
				{
					k = i;
					tmp = w(k);
				}
			}
			if ( k != j) 
			{
				w(k) = w(j);
				w(j) = tmp;
				for ( MGSize i=0; i<mNRows; ++i) 
				{
					tmp = mtxL(i,j);
					mtxL(i,j) = mtxL(i,k);
					mtxL(i,k) = tmp;
				}
			}
		}
	}


	for ( MGSize ip=0; ip<mNRows; ++ip) 
		mtxD(ip) = w(ip);
	return;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::NDecompose( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxN)
{
	// TODO :: check if matrix is symetric
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxD;	// TODO :: should be SVector
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxL;

	mtxN = *this;
	mtxN.Decompose( mtxD, mtxL);
	mtxL.Invert();		// TODO :: transposition should be OK

	for ( MGSize k=0; k<MAX_SIZE; ++k)	// TODO :: check if it should not be Size() ???
		mtxD(k,k) = ::sqrt( mtxD(k,k));

	mtxN = mtxD * mtxL;
}


//////////////////////////////////////////////////////////////////////

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::RotateMatrixNew( 
		SMatrix< MAX_SIZE, ELEM_TYPE>& a, 
		ELEM_TYPE& g, ELEM_TYPE& h, ELEM_TYPE& s, ELEM_TYPE& c, ELEM_TYPE& tau, 
		const MGSize& i, const MGSize& j, const MGSize& k, const MGSize& l
		)
{
	g = a(i,j);
	h = a(k,l);
	a(i,j) = g - s*( h+g*tau );
	a(k,l) = h + s*( g-h*tau );
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix< MAX_SIZE, ELEM_TYPE>::DecomposeNew( SVector< MAX_SIZE, ELEM_TYPE>& vecD, SMatrix< MAX_SIZE, ELEM_TYPE>& mtxL, const bool& bsort)
{
	CHECK( mNRows == mNCols);

	ELEM_TYPE eps = numeric_limits<ELEM_TYPE>::epsilon();
	

	MGSize i, k;
	ELEM_TYPE tresh, theta, tau, t, sm, s, h, g, c, tmp;

	SVector< MAX_SIZE, ELEM_TYPE>	b(mNRows);
	SVector< MAX_SIZE, ELEM_TYPE>	z(mNRows);
	SVector< MAX_SIZE, ELEM_TYPE>	w(mNRows);


	// initialize
	mtxL.InitDelta( mNRows);
	vecD.Resize( mNRows);

	for ( MGSize ip=0; ip<mNRows; ++ip) 
	{
		b(ip) = w(ip) = (*this)(ip,ip);
		z(ip) = 0.0;
	}

	// begin rotation sequence
	for ( i=0; i<SMTX_MAX_ROTATIONS; ++i) 
	{
		sm = 0.0;
		for ( MGSize ip=0; ip<mNRows-1; ++ip) 
		{
			for ( MGSize iq=ip+1; iq<mNRows; ++iq)
			{
				sm += ::fabs( (*this)(ip,iq) );
			}
		}

		if ( sm == 0.0 )
			break;

		if (i < 3)                                // first 3 sweeps
			tresh = 0.2*sm/(mNRows*mNRows);
		else
			tresh = 0.0;


		for ( MGSize ip=0; ip<mNRows-1; ++ip) 
		{
			for ( MGSize iq=ip+1; iq<mNRows; ++iq) 
			{
				g = 100.0 * ::fabs( (*this)(ip,iq) );

				// after 4 sweeps
				if ( i > 3 && g <= eps*::fabs( w(ip)) && g <= eps*::fabs( w(iq)) )
				{
					(*this)(ip,iq) = 0.0;
				}
				else if ( ::fabs( (*this)(ip,iq) ) > tresh) 
				{
					h = w(iq) - w(ip);

					if ( g <= eps * ::fabs( h) )
					{
						t = ( (*this)(ip,iq) ) / h;
					} 
					else 
					{
						theta = 0.5*h / ( (*this)(ip,iq) );
						t = 1.0 / ( ::fabs(theta) + ::sqrt( 1.0+theta*theta) );

						if ( theta < 0.0)
							t = -t;
					}

					c = 1.0 / ::sqrt(1+t*t);
					s = t*c;
					tau = s/(1.0+c);
					h = t*(*this)(ip,iq);
					z(ip) -= h;
					z(iq) += h;
					w(ip) -= h;
					w(iq) += h;
					(*this)(ip,iq) = 0.0;

					for ( MGSize j = 0; j < ip; ++j) 
						RotateMatrixNew( *this, g, h, s, c, tau, j, ip, j, iq );

					for ( MGSize j = ip+1; j < iq; ++j) 
						RotateMatrixNew( *this, g, h, s, c, tau, ip, j, j, iq );

					for ( MGSize j=iq+1; j<mNRows; ++j) 
						RotateMatrixNew( *this, g, h, s, c, tau, ip, j, iq, j );

					for ( MGSize j=0; j<mNRows; ++j) 
						RotateMatrixNew( mtxL, g, h, s, c, tau, j, ip, j, iq );

				}
			}

		}

		for ( MGSize ip=0; ip<mNRows; ++ip) 
		{
			b(ip) += z(ip);
			w(ip) = b(ip);
			z(ip) = 0.0;
		}
	}


	// this should NEVER be called
	if ( i >= SMTX_MAX_ROTATIONS )
	{
		Write();
		THROW_INTERNAL( "SMatrix Decompose error: Error extracting eigenfunctions");
	}


	if ( bsort)
	{
		// sort eigenfunctions                 these changes do not affect accuracy 
		for ( MGSize j=0; j<mNRows-1; ++j)                  // boundary incorrect
		{
			k = j;
			tmp = w(k);
			for ( MGSize i=j+1; i<mNRows; ++i)                // boundary incorrect, shifted already
			{
				if (w(i) >= tmp)                   // why exchage if same?
				{
					k = i;
					tmp = w(k);
				}
			}
			if ( k != j) 
			{
				w(k) = w(j);
				w(j) = tmp;
				for ( MGSize i=0; i<mNRows; ++i) 
				{
					tmp = mtxL(i,j);
					mtxL(i,j) = mtxL(i,k);
					mtxL(i,k) = tmp;
				}
			}
		}
	}


	for ( MGSize ip=0; ip<mNRows; ++ip) 
		vecD(ip) = w(ip);
	return;
}
//////////////////////////////////////////////////////////////////////

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix<MAX_SIZE, ELEM_TYPE>::Assemble(const SMatrix< MAX_SIZE, ELEM_TYPE>& mtxL, 
												   const SMatrix< MAX_SIZE, ELEM_TYPE>& mtxD, 
												   const SMatrix< MAX_SIZE, ELEM_TYPE>& mtxLI )
{
	CHECK( (mtxL.NRows() == mtxLI.NCols()) && (mtxL.NCols() == mtxLI.NRows()) && (vecD.NRows() == mtxLI.NRows()));

	Resize( mtxL.NRows(), mtxLI.NCols() );

	for( MGSize i=0; i<mtxL.NRows(); ++i)
		for( MGSize j=0; j<mtxLI.NCols(); ++j)
		{
			(*this)(i,j) = 0.0;
			for( MGSize k=0; k<mtxL.NCols(); ++k)
				(*this)(i,j) += mtxL(i,k) * mtxD(k,k) * mtxLI(k,j);
		}
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void SMatrix<MAX_SIZE, ELEM_TYPE>::Assemble(const SMatrix< MAX_SIZE, ELEM_TYPE>& mtxL, 
												   const SVector< MAX_SIZE, ELEM_TYPE>& vecD, 
												   const SMatrix< MAX_SIZE, ELEM_TYPE>& mtxLI )
{
	CHECK( (mtxL.NRows() == mtxLI.NCols()) && (mtxL.NCols() == mtxLI.NRows()) && (vecD.NRows() == mtxLI.NRows()));

	Resize( mtxL.NRows(), mtxLI.NCols() );

	for( MGSize i=0; i<mtxL.NRows(); ++i)
		for( MGSize j=0; j<mtxLI.NCols(); ++j)
		{
			(*this)(i,j) = 0.0;
			for( MGSize k=0; k<mtxL.NCols(); ++k)
				(*this)(i,j) += mtxL(i,k) * vecD(k) * mtxLI(k,j);
		}
}

//////////////////////////////////////////////////////////////////////


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix<MAX_SIZE, ELEM_TYPE>& SMatrix<MAX_SIZE, ELEM_TYPE>::operator += ( const SMatrix<MAX_SIZE, ELEM_TYPE>& a)
{
	CHECK( (NRows() == a.NRows()) && (NCols() == a.NCols()) );

	for( MGSize i=0; i<a.NRows(); ++i)
		for( MGSize j=0; j<a.NCols(); ++j)
			(*this)(i,j) += a(i,j);

	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix<MAX_SIZE, ELEM_TYPE>& SMatrix<MAX_SIZE, ELEM_TYPE>::operator -= ( const SMatrix<MAX_SIZE, ELEM_TYPE>& a)
{
	CHECK( (NRows() == a.NRows()) && (NCols() == a.NCols()) );

	for( MGSize i=0; i<a.NRows(); ++i)
		for( MGSize j=0; j<a.NCols(); ++j)
			(*this)(i,j) -= a(i,j);

	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix<MAX_SIZE, ELEM_TYPE>& SMatrix<MAX_SIZE, ELEM_TYPE>::operator += ( const ELEM_TYPE& d)
{
	for( MGSize i=0; i<NRows(); ++i)
		for( MGSize j=0; j<NCols(); ++j)
			(*this)(i,j) += d;

	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix<MAX_SIZE, ELEM_TYPE>& SMatrix<MAX_SIZE, ELEM_TYPE>::operator -= ( const ELEM_TYPE& d)
{
	for( MGSize i=0; i<NRows(); ++i)
		for( MGSize j=0; j<NCols(); ++j)
			(*this)(i,j) -= d;

	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix<MAX_SIZE, ELEM_TYPE>& SMatrix<MAX_SIZE, ELEM_TYPE>::operator *= ( const ELEM_TYPE& d)
{
	for( MGSize i=0; i<NRows(); ++i)
		for( MGSize j=0; j<NCols(); ++j)
			(*this)(i,j) *= d;

	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix<MAX_SIZE, ELEM_TYPE>& SMatrix<MAX_SIZE, ELEM_TYPE>::operator /= ( const ELEM_TYPE& d)
{
	for( MGSize i=0; i<NRows(); ++i)
		for( MGSize j=0; j<NCols(); ++j)
			(*this)(i,j) /= d;

	return *this;
}


//////////////////////////////////////////////////////////////////////

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix<MAX_SIZE, ELEM_TYPE> operator * ( const SMatrix<MAX_SIZE, ELEM_TYPE>& a1, const SMatrix<MAX_SIZE, ELEM_TYPE>& a2)
{
	CHECK( a1.NCols() == a2.NRows() );

	SMatrix<MAX_SIZE, ELEM_TYPE>	atmp(a1.NRows(), a2.NCols());
	for( MGSize i=0; i<a1.NRows(); ++i)
		for( MGSize j=0; j<a2.NCols(); ++j)
		{
			atmp(i,j) = 0.0;
			for( MGSize k=0; k<a1.NCols(); ++k)
				atmp(i,j) += a1(i,k) * a2(k,j);
		}

	return atmp;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix<MAX_SIZE, ELEM_TYPE> operator * ( const ELEM_TYPE& d, const SMatrix<MAX_SIZE, ELEM_TYPE>& a)
{
	SMatrix<MAX_SIZE, ELEM_TYPE>	atmp(a.NRows(), a.NCols());
	for( MGSize i=0; i<a.NRows(); ++i)
		for( MGSize j=0; j<a.NCols(); ++j)
			atmp(i,j) = d * a(i,j);

	return atmp;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix<MAX_SIZE, ELEM_TYPE> operator * ( const SMatrix<MAX_SIZE, ELEM_TYPE>& a, const ELEM_TYPE& d)
{
	SMatrix<MAX_SIZE, ELEM_TYPE>	atmp(a.NRows(), a.NCols());
	for( MGSize i=0; i<a.NRows(); ++i)
		for( MGSize j=0; j<a.NCols(); ++j)
			atmp(i,j) = d * a(i,j);

	return atmp;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix<MAX_SIZE, ELEM_TYPE> operator / ( const SMatrix<MAX_SIZE, ELEM_TYPE>& a, const ELEM_TYPE& d)
{
	SMatrix<MAX_SIZE, ELEM_TYPE>	atmp(a.NRows(), a.NCols());
	for( MGSize i=0; i<a.NRows(); ++i)
		for( MGSize j=0; j<a.NCols(); ++j)
			atmp(i,j) = a(i,j) / d;

	return atmp;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix<MAX_SIZE, ELEM_TYPE> operator + ( const SMatrix<MAX_SIZE, ELEM_TYPE>& a1, const SMatrix<MAX_SIZE, ELEM_TYPE>& a2)
{
	CHECK( (a1.NRows() == a2.NRows()) && (a1.NCols() == a2.NCols()) );

	SMatrix<MAX_SIZE, ELEM_TYPE>	atmp(a1.NRows(), a1.NCols());
	for( MGSize i=0; i<a1.NRows(); ++i)
		for( MGSize j=0; j<a1.NCols(); ++j)
			atmp(i,j) = a1(i,j) + a2(i,j);

	return atmp;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SMatrix<MAX_SIZE, ELEM_TYPE> operator - ( const SMatrix<MAX_SIZE, ELEM_TYPE>& a1, const SMatrix<MAX_SIZE, ELEM_TYPE>& a2)
{
	CHECK( (a1.NRows() == a2.NRows()) && (a1.NCols() == a2.NCols()) );

	SMatrix<MAX_SIZE, ELEM_TYPE>	atmp(a1.NRows(), a1.NCols());
	for( MGSize i=0; i<a1.NRows(); ++i)
		for( MGSize j=0; j<a1.NCols(); ++j)
			atmp(i,j) = a1(i,j) - a2(i,j);

	return atmp;
}


//////////////////////////////////////////////////////////////////////

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline SVector< MAX_SIZE, ELEM_TYPE> operator * ( const SMatrix< MAX_SIZE, ELEM_TYPE>& a, const SVector< MAX_SIZE, ELEM_TYPE>& v)
{
	CHECK( a.NCols() == v.NRows() );

	SVector<MAX_SIZE,ELEM_TYPE>	atmp(a.NRows());

	for( MGSize i=0; i<a.NRows(); ++i)
	{
		atmp(i) = 0.0;
		for( MGSize k=0; k<a.NCols(); ++k)
			atmp(i) += a(i,k) * v(k);
	}

	return atmp;
}

//////////////////////////////////////////////////////////////////////

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void MtxMultMtxD( SMatrix<MAX_SIZE, ELEM_TYPE>& r, const SMatrix<MAX_SIZE, ELEM_TYPE>& a, const ELEM_TYPE& d)
{
	r.Resize( a.NRows(), a.NCols());
	for( MGSize i=0; i<a.NRows(); ++i)
		for( MGSize j=0; j<a.NCols(); ++j)
		{
			MGSize id = i*MAX_SIZE + j;
			r[id] = d * a[id];
			//r(i,j) = d * a(i,j);
		}
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void MtxMultMtxMtx( SMatrix< MAX_SIZE, ELEM_TYPE>& r, const SMatrix< MAX_SIZE, ELEM_TYPE>& a1, const SMatrix< MAX_SIZE, ELEM_TYPE>& a2)
{
	CHECK( a1.NCols() == a2.NRows() );

	r.Resize( a1.NRows(), a2.NCols() );

	for( MGSize i=0; i<a1.NRows(); ++i)
		for( MGSize j=0; j<a2.NCols(); ++j)
		{
			r(i,j) = a1(i,0) * a2(0,j);
			for( MGSize k=1; k<a1.NCols(); ++k)
				r(i,j) += a1(i,k) * a2(k,j);
		}
}


//  :: if matrix is NOT resized to MAX_SIZE then specialized function will not work !!!!
template< class ELEM_TYPE>
inline void MtxMultMtxMtx( SMatrix< 4, ELEM_TYPE>& r, const SMatrix< 4, ELEM_TYPE>& a1, const SMatrix< 4, ELEM_TYPE>& a2)
{
	CHECK( a1.NCols() == a2.NRows() );

	r.Resize( a1.NRows(), a2.NCols() );

	for( MGSize i=0; i<a1.NRows(); ++i)
		for( MGSize j=0; j<a2.NCols(); ++j)
			r(i,j) = a1(i,0) * a2(0,j) + a1(i,1) * a2(1,j) + a1(i,2) * a2(2,j) + a1(i,3) * a2(3,j);
}

template< class ELEM_TYPE>
inline void MtxMultMtxMtx( SMatrix< 5, ELEM_TYPE>& r, const SMatrix< 5, ELEM_TYPE>& a1, const SMatrix< 5, ELEM_TYPE>& a2)
{
	CHECK( a1.NCols() == a2.NRows() );

	r.Resize( a1.NRows(), a2.NCols() );

	for( MGSize i=0; i<a1.NRows(); ++i)
		for( MGSize j=0; j<a2.NCols(); ++j)
			r(i,j) = a1(i,0) * a2(0,j) + a1(i,1) * a2(1,j) + a1(i,2) * a2(2,j) + a1(i,3) * a2(3,j) + a1(i,4) * a2(4,j);
}






template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void VecMultMtxVec( SVector< MAX_SIZE, ELEM_TYPE>& r, const SMatrix< MAX_SIZE, ELEM_TYPE>& a, const SVector< MAX_SIZE, ELEM_TYPE>& v)
{
	CHECK( a.NCols() == v.NRows() );

	r.Resize( a.NRows() );

	for( MGSize i=0; i<a.NRows(); ++i)
	{
		r(i) = a(i,0) * v(0);
		for( MGSize k=1; k<a.NCols(); ++k)
			r(i) += a(i,k) * v(k);
	}
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void VecAddMultMtxVec( SVector< MAX_SIZE, ELEM_TYPE>& r, const SMatrix< MAX_SIZE, ELEM_TYPE>& a, const SVector< MAX_SIZE, ELEM_TYPE>& v)
{
	CHECK( a.NCols() == v.NRows() );
	CHECK( a.NRows() == r.NRows() );

	for( MGSize i=0; i<a.NRows(); ++i)
	{
		for( MGSize k=0; k<a.NCols(); ++k)
			r(i) += a(i,k) * v(k);
	}
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void MtxSumMtxMtx( SMatrix< MAX_SIZE, ELEM_TYPE>& r, const SMatrix< MAX_SIZE, ELEM_TYPE>& a1, const SMatrix< MAX_SIZE, ELEM_TYPE>& a2)
{
	CHECK( (a1.NRows() == a2.NRows()) && (a1.NCols() == a2.NCols()) );

	r.Resize(a1.NRows(), a1.NCols());
	for( MGSize i=0; i<a1.NRows(); ++i)
		for( MGSize j=0; j<a1.NCols(); ++j)
		{
			MGSize id = i*MAX_SIZE + j;
			r[id] = a1[id] + a2[id];
		}
}





#endif // __SMATRIX_H__
