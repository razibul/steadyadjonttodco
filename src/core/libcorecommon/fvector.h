#ifndef __FVECTOR_H__
#define __FVECTOR_H__


#include "libcoresystem/mgdecl.h"


//////////////////////////////////////////////////////////////////////
//	class fvector
//	indexing is like for fortran arrays - 1...N
//////////////////////////////////////////////////////////////////////
template <class T>
class fvector : public vector<T>
{
	typedef typename vector<T>::size_type	size_type;

public:
	typedef vector<T>								vector_base;
	typedef typename vector_base::iterator			iterator;
	typedef typename vector_base::const_iterator	const_iterator;


	fvector() : vector_base()	{ vector_base::push_back( T() );}

	iterator		begin()								{ return ++( vector_base::begin() ); }
	const_iterator	begin() const						{ return ++( vector_base::begin() ); }

	void			resize( size_type n)				{ vector_base::resize( n+1, T()); }
	void			resize( size_type n, const T& val)	{ vector_base::resize( n+1, val ); }

	void			reserve( size_type n)				{ vector_base::reserve( n+1); }

	size_type		size_valid() const					{ return ( vector_base::size() - 1 );}

	bool			empty() const						{ return ( vector_base::size() <= 1 ); }
	
	void			clear()								{ vector_base::erase( begin(), vector_base::end() ); }

	void			erase( const size_t& ind)			{ /*vector_base::erase( iterator( &(*this)[ind] ) );*/ }

	size_t			insert( const T& t)					{ push_back( t); return vector_base::size() - 1; }

};


#endif // __FVECTOR_H__
