#ifndef __KEY_H__
#define __KEY_H__

#include "libcoresystem/mgdecl.h"

//////////////////////////////////////////////////////////////////////
//	class Key
//////////////////////////////////////////////////////////////////////
template <size_t SIZE, class T=MGSize> 
class Key
{
public:
	typedef size_t			size_type;

	Key();
	Key( const Key<SIZE,T>& key);
	Key( const T& i1, const T& i2);
	Key( const T& i1, const T& i2, const T& i3);
	Key( const T& i1, const T& i2, const T& i3, const T& i4);

	explicit Key( const T tab[]);


	static	size_type	Size()		 				{ return SIZE;}

	const T&	cFirst() const						{ return *mtab;}
	const T&	cSecond() const						{ return *(mtab+1);}
	const T&	cThird() const						{ return *(mtab+2);}
	const T&	cFourth() const						{ return *(mtab+3);}
	const T&	cElem( const size_type& i) const	{ return *(mtab+i);}

	T&			rFirst()							{ return *mtab;}
	T&			rSecond()							{ return *(mtab+1);}
	T&			rThird()							{ return *(mtab+2);}
	T&			rFourth()							{ return *(mtab+3);}
	T&			rElem( const size_type& i)			{ return *(mtab+i);}

	void	Sort();
	void	Rotate( const size_type& i)				{ rotate( mtab, mtab+i, mtab+SIZE);}
	void	Reverse()								{ reverse( mtab, mtab+SIZE);}

	void	Dump(  ostream& o = cout) const;

private:
	T	mtab[SIZE];
};
//////////////////////////////////////////////////////////////////////


template <size_t SIZE, class T> 
inline Key<SIZE,T>::Key()
{
	::memset( mtab, 0, SIZE*sizeof(T) );
}

template <size_t SIZE, class T> 
inline Key<SIZE,T>::Key( const Key<SIZE,T>& key)
{
	for ( size_type i=0; i<SIZE; ++i)
		mtab[i] = key.mtab[i];
}

template <size_t SIZE, class T> 
inline Key<SIZE,T>::Key( const T tab[])
{
	for ( size_type i=0; i<SIZE; ++i)
		mtab[i] = tab[i];
}


template <size_t SIZE, class T> 
inline Key<SIZE,T>::Key( const T& i1, const T& i2)
{
	mtab[0] = i1; 
	mtab[1] = i2;
	//Sort();
}

template <size_t SIZE, class T> 
inline Key<SIZE,T>::Key( const T& i1, const T& i2, const T& i3)
{
	mtab[0] = i1; 
	mtab[1] = i2;
	mtab[2] = i3;
	//Sort();
}

template <size_t SIZE, class T> 
inline Key<SIZE,T>::Key( const T& i1, const T& i2, const T& i3, const T& i4)
{
	mtab[0] = i1; 
	mtab[1] = i2;
	mtab[2] = i3;
	mtab[3] = i4;
	//Sort();
}

template <size_t SIZE, class T> 
inline void Key<SIZE,T>::Dump( ostream& o) const
{
	o << "Key = [ " << mtab[0];
	for ( size_type i=1; i<SIZE; ++i)
		o << ", " << mtab[i];
	o << " ]" << endl;
}


template <size_t SIZE, class T> 
inline void Key<SIZE,T>::Sort()
{
	sort( mtab, mtab+SIZE);
}



//////////////////////////////////////////////////////////////////////
// comparison operators

template <size_t SIZE, class T> 
inline bool operator == ( const Key<SIZE,T>& c1, const Key<SIZE,T>& c2 )
{
	for ( typename Key<SIZE,T>::size_type i=0; i<SIZE; ++i)
		if ( c1.cElem(i) != c2.cElem(i) )
			return false;

	return true;
}

template <size_t SIZE, class T> 
inline bool operator != ( const Key<SIZE,T>& c1, const Key<SIZE,T>& c2 )
{
	if ( c1 == c2 )
		return false;

	return true;
}


template <size_t SIZE, class T> 
inline bool operator < ( const Key<SIZE,T>& c1, const Key<SIZE,T>& c2 )
{

	if ( c1.cFirst() < c2.cFirst() )
		return true;

	if ( c1.cFirst() > c2.cFirst() )
		return false;

	Key<SIZE-1,T> w1, w2;

	for ( typename Key<SIZE,T>::size_type i=1; i<SIZE; ++i)
	{
		w1.rElem( i-1 ) = c1.cElem(i);
		w2.rElem( i-1 ) = c2.cElem(i);
	}

	return w1 < w2;
}

template <class T>
inline bool operator < ( const Key<1,T>& c1, const Key<1,T>& c2 )
{
	return c1.cFirst() < c2.cFirst();
}




template <size_t SIZE, class T> 
inline bool operator > ( const Key<SIZE,T>& c1, const Key<SIZE,T>& c2 )
{

	if ( c1.cFirst() > c2.cFirst() )
		return true;

	if ( c1.cFirst() < c2.cFirst() )
		return false;

	Key<SIZE-1,T> w1, w2;

	for ( typename Key<SIZE,T>::size_type i=1; i<SIZE; ++i)
	{
		w1.rElem( i-1 ) = c1.cElem(i);
		w2.rElem( i-1 ) = c2.cElem(i);
	}

	return w1 > w2;
}

template <class T>
inline bool operator > ( const Key<1,T>& c1, const Key<1,T>& c2 )
{
	return c1.cFirst() > c2.cFirst();
} 



//////////////////////////////////////////////////////////////////////
// class KeyData - only first element of the pair is used for sorting
//////////////////////////////////////////////////////////////////////
template<class TKEY, class TDATA>
class KeyData : public pair<TKEY,TDATA>
{
public:
	KeyData()	{}
	KeyData( const TKEY& k) : pair<TKEY,TDATA>( k, TDATA() )	{}
	KeyData( const TKEY& k, const TDATA& d) : pair<TKEY,TDATA>(k,d)	{}
};

template<class TKEY, class TDATA>
inline bool operator == ( const KeyData<TKEY, TDATA>& c1, const KeyData<TKEY, TDATA>& c2 )
{
	return c1.first == c2.first;
}

template<class TKEY, class TDATA>
inline bool operator != ( const KeyData<TKEY, TDATA>& c1, const KeyData<TKEY, TDATA>& c2 )
{
	return c1.first != c2.first;
}


template<class TKEY, class TDATA>
inline bool operator < ( const KeyData<TKEY, TDATA>& c1, const KeyData<TKEY, TDATA>& c2 )
{
	return c1.first < c2.first;
}



#endif // __KEY_H__
