#ifndef __PROGRESSBAR_H__
#define __PROGRESSBAR_H__


#include "libcoresystem/mgdecl.h"


//////////////////////////////////////////////////////////////////////
//  class ProgressBar
//////////////////////////////////////////////////////////////////////
class ProgressBar
{
public:
	ProgressBar( const MGSize& len) : mLength(len)	{}

	void	Init( const MGSize& limit, const bool& bFinishType=true);
	void	PrintReferenceBar();
	void	Start();
	void	DoProgress( const MGSize& ipos);
	void	Finish();
	
	ProgressBar	operator ++();

private:
	bool	mbFinishType;
	MGSize	mLength;
	MGSize	mLimit;
	MGSize	mPos;
	MGSize	mCurPos;
	MGFloat	mDelta;
	

	clock_t	mStart;
	clock_t	mFinish;
	MGFloat	mDuration;
};
//////////////////////////////////////////////////////////////////////


inline void ProgressBar::Init( const MGSize& limit, const bool& bFinishType)
{
	mbFinishType = bFinishType;
	mLimit = limit;
	mDelta = ((MGFloat)mLength -0.00000001) / (MGFloat)mLimit;
}


inline void ProgressBar::PrintReferenceBar()
{
	cout << "\n|";
	for ( MGSize i=1; i<mLength-1; ++i)
		cout << "=";
	cout << "|\n";
}


inline void ProgressBar::Start()
{
	mPos = mCurPos = 0;
	mStart = clock();
}


inline void ProgressBar::DoProgress( const MGSize& ipos)
{
	MGSize n = (MGSize)( ::ceil( (ipos+1) * mDelta ) );
	n -= mCurPos;

	if ( n)
	{
		for ( MGSize i=0; i<n; ++i)
			cout << ">" << std::flush;
	}

	mCurPos += n;
}

inline void ProgressBar::Finish()
{
	mFinish = clock();
	mDuration = (double)(mFinish - mStart) / CLOCKS_PER_SEC;

	if ( mbFinishType )
	{
		cout << " " << setw(7) << mLimit;
		cout << " | " << setw(10) << setprecision(3) << mDuration << " sec" << endl;
	}
	else
	{
		cout << " TIME = " << setw(10) << setprecision(3) << mDuration << " sec" << endl;
	}
}



inline ProgressBar ProgressBar::operator ++()
{
	MGSize n = (MGSize)( ::ceil( (mPos+1) * mDelta ) );
	n -= mCurPos;

	if ( n)
	{
		for ( MGSize i=0; i<n; ++i)
			cout << ">" << std::flush;
	}

	mCurPos += n;
	++mPos;
	
	return (*this);
}




#endif // __PROGRESSBAR_H__
