#ifndef __ARRAY_H__
#define __ARRAY_H__


template <class T, size_t N>
class ArrayData;


//////////////////////////////////////////////////////////////////////
// class ArrayDataCommon
//////////////////////////////////////////////////////////////////////
template <class T, size_t N>
class ArrayDataCommon
{
	friend class ArrayData<T,N>;

public:
	enum { SIZE = N };

	ArrayDataCommon()	{}
	ArrayDataCommon( const ArrayDataCommon<T,N>& arr)	{ for ( size_t i=0; i<SIZE; ++i) mtab[i] = arr.mtab[i];	}

	T&			operator [] ( const size_t& i)			{ return mtab[i]; }
	const T&	operator [] ( const size_t& i) const 	{ return mtab[i]; }

private:
	T	mtab[SIZE];
};



//////////////////////////////////////////////////////////////////////
// class ArrayData
//////////////////////////////////////////////////////////////////////
template <class T, size_t N>
class ArrayData : public ArrayDataCommon<T,N>
{
public:
	ArrayData( T tab[])									{ for (size_t i=0; i<N; ++i) this->mtab[i] = tab[i]; }
	ArrayData( const T& t, const ArrayData<T,N-1>& tab)	{ this->mtab[0]=t; for (size_t i=1; i<N; ++i) this->mtab[i] = tab[i-1]; }
};


//////////////////////////////////////////////////////////////////////
// class ArrayData - with specialization

template <class T>
class ArrayData<T,1> : public ArrayDataCommon<T,1>
{
public:
	explicit ArrayData( T tab[])									{ this->mtab[0] = tab[0]; }
	explicit ArrayData( const T& t1)								{ this->mtab[0] = t1; }
};

template <class T>
class ArrayData<T,2> : public ArrayDataCommon<T,2>
{
public:
	explicit ArrayData( T tab[])									{ this->mtab[0] = tab[0]; this->mtab[1] = tab[1]; }
	explicit ArrayData( const T& t, const ArrayData<T,1>& tab)	{ this->mtab[0]=t; this->mtab[1] = tab[0]; }
	explicit ArrayData( const T& t1, const T& t2)				{ this->mtab[0] = t1; this->mtab[1] = t2; }
};

template <class T>
class ArrayData<T,3> : public ArrayDataCommon<T,3>
{
public:
	explicit ArrayData( T tab[])									{ this->mtab[0] = tab[0]; this->mtab[1] = tab[1]; this->mtab[2] = tab[2]; }
	explicit ArrayData( const T& t, const ArrayData<T,2>& tab)	{ this->mtab[0]=t; this->mtab[1] = tab[0]; this->mtab[2] = tab[1]; }
	explicit ArrayData( const T& t1, const T& t2, const T& t3)	{ this->mtab[0] = t1; this->mtab[1] = t2; this->mtab[2] = t3; }
};

template <class T>
class ArrayData<T,4> : public ArrayDataCommon<T,4>
{
public:
	explicit ArrayData( T tab[])									{ this->mtab[0] = tab[0]; this->mtab[1] = tab[1]; this->mtab[2] = tab[2]; this->mtab[3] = tab[3]; }
	explicit ArrayData( const T& t, const ArrayData<T,3>& tab)	{ this->mtab[0]=t; this->mtab[1] = tab[0]; this->mtab[2] = tab[1]; this->mtab[3] = tab[2]; }
	explicit ArrayData( const T& t1, const T& t2, const T& t3, const T& t4)	
														{ this->mtab[0] = t1; this->mtab[1] = t2; this->mtab[2] = t3; this->mtab[3] = t4; }
};

template <class T>
class ArrayData<T,5> : public ArrayDataCommon<T,5>
{
public:
	explicit ArrayData( T tab[])									{ this->mtab[0] = tab[0]; this->mtab[1] = tab[1]; this->mtab[2] = tab[2]; this->mtab[3] = tab[3]; this->mtab[4] = tab[4]; }
	explicit ArrayData( const T& t, const ArrayData<T,4>& tab)	{ this->mtab[0]=t; this->mtab[1] = tab[0]; this->mtab[2] = tab[1]; this->mtab[3] = tab[2]; this->mtab[4] = tab[3]; }
	explicit ArrayData( const T& t1, const T& t2, const T& t3, const T& t4, const T& t5)	
														{ this->mtab[0] = t1; this->mtab[1] = t2; this->mtab[2] = t3; this->mtab[3] = t4; this->mtab[4] = t5; }
};






//////////////////////////////////////////////////////////////////////
// class ArrayTabProxy
//////////////////////////////////////////////////////////////////////
template <class T, size_t N>
class ArrayTabProxy
{
public:
	enum { SIZE = N };

	ArrayTabProxy( T* ptr) : mptab( ptr)	{}

	T&			operator [] ( const size_t& i)			{ return mptab[i]; }
	const T&	operator [] ( const size_t& i) const 	{ return mptab[i]; }

private:
	T*	mptab;
};


//////////////////////////////////////////////////////////////////////
// class ArrayProxy
//////////////////////////////////////////////////////////////////////
template <class T, size_t N>
class ArrayProxy : public ArrayData<T*,N>
{
public:
	enum { SIZE = N };

	ArrayProxy( T& t1) : ArrayData<T*,SIZE>( &t1)	{}
	ArrayProxy( T& t1, T& t2) : ArrayData<T*,SIZE>( &t1, &t2)	{}
	ArrayProxy( T& t1, T& t2, T& t3) : ArrayData<T*,SIZE>( &t1, &t2, &t3)	{}
	ArrayProxy( T& t1, T& t2, T& t3, T& t4) : ArrayData<T*,SIZE>( &t1, &t2, &t3, &t4)	{}

	T&			operator [] ( const size_t& i)			{ return * ArrayData<T*,N>::operator[](i); }
	const T&	operator [] ( const size_t& i) const 	{ return * ArrayData<T*,N>::operator[](i); }

	void	SetElem( const size_t& i, T* ptr)			{ ArrayData<T*,N>::operator[](i) = ptr; }
};



//////////////////////////////////////////////////////////////////////
// class ArrayConstProxy
//////////////////////////////////////////////////////////////////////
template <class T, size_t N>
class ArrayConstProxy : public ArrayData<const T*,N>
{
public:
	enum { SIZE = N };

	//ArrayConstProxy()	{};
	ArrayConstProxy( const T& t1) : ArrayData<const T*,SIZE>( &t1)	{}
	ArrayConstProxy( const T& t1, const T& t2) : ArrayData<const T*,SIZE>( &t1, &t2)	{}
	ArrayConstProxy( const T& t1, const T& t2, const T& t3) : ArrayData<const T*,SIZE>( &t1, &t2, &t3)	{}
	ArrayConstProxy( const T& t1, const T& t2, const T& t3, const T& t4) : ArrayData<const T*,SIZE>( &t1, &t2, &t3, &t4)	{}
	ArrayConstProxy( const T& t1, const T& t2, const T& t3, const T& t4, const T& t5) : ArrayData<const T*,SIZE>( &t1, &t2, &t3, &t4, &t5)	{}

	const T&	operator [] ( const size_t& i) const 	{ return * ArrayData<const T*,N>::operator[](i); }

	void	SetElem( const size_t& i, const T* ptr)		{ ArrayData<const T*,N>::operator[](i) = ptr; }
};



/*
EXAMPLE :

template < class T, size_t N, template< class T, size_t N > class DATA = ArrayData >
class Vector
{
public:
	typedef DATA<T,N>	ARRAY;

	Vector( const DATA<T,N>& data) : mData( data)	{}

	ARRAY	mData;
};



int main()
{
	int a = 6;
	int b = 3;
	int c = 4;

	ArrayData<int,3>	adata( a, b, c);
	ArrayProxy<int,3>	aproxyf( a, b, c);

	cout << adata[2] << endl;
	cout << aproxyf[2] << endl;

	c = 7;
	cout << aproxyf[2] << endl;

	Vector<int,3> vect( Vector<int,3>::ARRAY( a, b, c) );


	cout << "HELLO" << endl << endl;

	return 0;
}
*/

#endif // __ARRAY_H__
