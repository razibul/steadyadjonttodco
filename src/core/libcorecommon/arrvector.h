#ifndef __ARRVECTOR_H__
#define __ARRVECTOR_H__

#include "libcoresystem/mgdecl.h"



//////////////////////////////////////////////////////////////////////
//  class arrvector
//
//  SHIFT defines the size of the chunk vectors ( 2^SHIFT ). 
//  By defalt the chunk size is 1024
//////////////////////////////////////////////////////////////////////
template <class T, size_t SHIFT=10, class ALLOC = allocator<T> >
class arrvector
{

public:
	typedef typename ALLOC::size_type			size_type;
	typedef typename ALLOC::difference_type		difference_type;
	typedef typename ALLOC::pointer				pointer;
	typedef typename ALLOC::const_pointer		const_pointer;
	typedef typename ALLOC::reference			reference;
	typedef typename ALLOC::const_reference		const_reference;
	typedef typename ALLOC::value_type			value_type;

	typedef ALLOC												allocator_type;
	typedef	vector< value_type, allocator_type >				chunk_type;
	typedef typename ALLOC::template rebind<chunk_type>::other	chunkallocator_type;
	typedef vector< chunk_type, chunkallocator_type >			vector_type;



	class const_iterator;
	class iterator;


	////////////////////////////
	// CLASS iterator
	////////////////////////////
	class iterator
	{
		friend class arrvector<T,SHIFT,ALLOC>;
		friend class const_iterator;

	protected:		
		iterator( const size_type& ipos, arrvector<T,SHIFT>* ptab) : mid(ipos), mptab(ptab)	{}

	public:
		typedef random_access_iterator_tag			iterator_category;
		typedef class arrvector<T,SHIFT,ALLOC>		parent;

		typedef typename parent::difference_type	difference_type;
		typedef typename parent::pointer			pointer;
		typedef typename parent::reference			reference;
		typedef typename parent::value_type			value_type;

		
		iterator() : mid(0), mptab(NULL)	{}

		iterator( const iterator& itr) : mid(itr.mid), mptab(itr.mptab)	{}

		const size_type&	index() const		{ return mid;}

		
		reference operator*()
		{
			return (*mptab)[mid];
		}

		pointer operator->()
		{
			return (&( (*mptab)[mid] ));
		}

		iterator& operator ++()	
		{
			++mid;
			return (*this);
		}

		iterator operator ++(int)	
		{
			iterator itmp(*this);
			++mid;
			return itmp;
		}

		iterator& operator --()	
		{
			--mid;
			return (*this);
		}

		iterator operator --(int)	
		{
			iterator itmp(*this);
			--mid;
			return itmp;
		}

		difference_type operator - ( const iterator& itr) const
		{
			ASSERT( mptab == itr.mptab );
			return (difference_type) mid - itr.mid;
		}

		iterator operator + ( const difference_type& diff) const
		{
			return iterator( mid + diff, mptab);
		}

		iterator operator - ( const difference_type& diff) const
		{
			return iterator( mid - diff, mptab);
		}


		bool operator == (const iterator& i2) const
		{
			ASSERT( mptab == i2.mptab );
			return ( mid == i2.mid);
		}

		bool operator != (const iterator& i2) const
		{
			ASSERT( mptab == i2.mptab );
			return ( mid != i2.mid);
		}

		bool operator < (const iterator& i2) const
		{
			ASSERT( mptab == i2.mptab );
			return ( mid < i2.mid);
		}

		bool operator > (const iterator& i2) const
		{
			ASSERT( mptab == i2.mptab );
			return ( mid > i2.mid);
		}

		bool operator <= (const iterator& i2) const
		{
			ASSERT( mptab == i2.mptab );
			return ( mid <= i2.mid);
		}

		bool operator >= (const iterator& i2) const
		{
			ASSERT( mptab == i2.mptab );
			return ( mid >= i2.mid);
		}

	protected:
		size_type	mid;
		arrvector<T,SHIFT>	*mptab;
	};


	////////////////////////////
	// CLASS const_iterator
	////////////////////////////
	class const_iterator
	{
		friend class arrvector<T,SHIFT>;

	protected:		
		const_iterator( const size_type& ipos, arrvector<T,SHIFT>* ptab) : mid(ipos), mptab(ptab)	{}

	public:
		typedef random_access_iterator_tag			iterator_category;
		typedef class arrvector<T,SHIFT,ALLOC>		parent;

		typedef typename parent::difference_type	difference_type;
		typedef typename parent::const_pointer		const_pointer;
		typedef typename parent::const_reference	const_reference;
		typedef typename parent::value_type			value_type;


		const_iterator() : mid(0), mptab(NULL)	{}

		const_iterator( const const_iterator& itr) : mid(itr.mid), mptab(itr.mptab)	{}
		const_iterator( const iterator& itr) : mid(itr.mid), mptab(itr.mptab)	{}

		const size_type&	index() const		{ return mid;}

		
		const_reference operator*() const
		{
			return (*mptab)[mid];
		}

		const_pointer operator->() const
		{
			return (&( (*mptab)[mid] ));
		}

		const_iterator& operator ++()	
		{
			++mid;
			return (*this);
		}

		const_iterator operator ++(int)	
		{
			iterator itmp(*this);
			++mid;
			return itmp;
		}

		const_iterator& operator --()	
		{
			--mid;
			return (*this);
		}

		const_iterator operator --(int)	
		{
			iterator itmp(*this);
			--mid;
			return itmp;
		}

		difference_type operator - ( const const_iterator& itr) const
		{
			ASSERT( mptab == itr.mptab );
			return (difference_type) mid - itr.mid;
		}

		const_iterator operator + ( const difference_type& diff) const
		{
			return const_iterator( mid + diff, mptab);
		}

		const_iterator operator - ( const difference_type& diff) const
		{
			return const_iterator( mid - diff, mptab);
		}

		bool operator == (const const_iterator& i2) const
		{
			ASSERT( mptab == i2.mptab );
			return ( mid == i2.mid);
		}

		bool operator != (const const_iterator& i2) const
		{
			ASSERT( mptab == i2.mptab );
			return ( mid != i2.mid);
		}

		bool operator < (const const_iterator& i2) const
		{
			ASSERT( mptab == i2.mptab );
			return ( mid < i2.mid);
		}

		bool operator > (const const_iterator& i2) const
		{
			ASSERT( mptab == i2.mptab );
			return ( mid > i2.mid);
		}

		bool operator <= (const const_iterator& i2) const
		{
			ASSERT( mptab == i2.mptab );
			return ( mid <= i2.mid);
		}

		bool operator >= (const const_iterator& i2) const
		{
			ASSERT( mptab == i2.mptab );
			return ( mid >= i2.mid);
		}

	protected:
		size_type	mid;
		arrvector<T,SHIFT>	*mptab;
	};
	////////////////////////////

private:
	enum { MASK = ~((~static_cast<size_type>(0)) << SHIFT ) };
	enum { CHUNKSIZE = static_cast<size_type>(1) << SHIFT }; 

public:
	arrvector()	: msize(0)	{}


	size_type			size() const		{ return msize;}
	size_type			chunk_size() const	{ return CHUNKSIZE;}

	void				resize( const size_type& size)
						{
							reserve( size);
							msize = size;
						}
						
	void 				resize( const size_type& newsize, const value_type& t)
						{
							msize = newsize;
							const size_type n = master_index(msize) + 1;
							mtab.resize(n);
							for ( size_type i=0; i<n; ++i)
								mtab[i].resize( CHUNKSIZE, t);

							//reserve( newsize);
							//msize = newsize;
							//for ( size_type i=0; i<msize; ++i)
							//	(*this)[i] = t;
						}
						

	void				reserve( const size_type& size)
						{
							const size_type n = master_index(size) + 1;
							if ( mtab.size() == n)
								return;

							if ( mtab.size() < n )
							{
								const size_type cursize = mtab.size();
								mtab.resize( n);
								for ( size_type i=cursize; i<n; ++i)
									mtab[i].resize( CHUNKSIZE);
							}
							else
							//if ( n < mtab.size() )
							{
								const size_type cursize = mtab.size();
								for ( size_type i=n; i<cursize; ++i)
									mtab[i].clear();
								mtab.resize(n);
							}
						}

	void				clear()
						{
							mtab.clear();
							msize = 0;
						}

	void				erase( iterator);
	iterator			insert( const_iterator& itr, const value_type& val);

	const_reference		operator[]( const size_type& i) const	{ return mtab[master_index(i)][inner_index(i)];}
	reference			operator[]( const size_type& i)			{ return mtab[master_index(i)][inner_index(i)];}


	void				push_back( const T& val)
						{
							const size_type id = msize;
							resize( msize + 1);
							(*this)[id] = val;
						}

	void				pop_back()
						{
							resize( msize - 1);
						}

	const_reference		back() const		{ return (*this)[msize-1]; }
	reference			back()				{ return (*this)[msize-1]; }

	const_reference		front() const		{ return mtab[0][0]; }
	reference			front()				{ return mtab[0][0]; }

	const_iterator		begin() const		{ return const_iterator(0,this); }
	const_iterator		end() const			{ return const_iterator(this->size(),this); }

	iterator			begin()				{ return iterator(0,this); }
	iterator			end()				{ return iterator(this->size(),this); }


	size_type			master_index( const size_type& i) const	{ return i >> SHIFT; }
	size_type			inner_index( const size_type& i) const	{ return i & MASK; }

	void				dump( ostream& f=cout) const
						{
							f << "msize = " << msize << endl;
							f << "mtab.size() = " << mtab.size() << endl;
							for ( size_type i=0; i<mtab.size(); ++i)
							{
								f << "   mtab[" << i << "].size() = " << mtab[i].size() << endl;
								for ( size_type k=0; k<mtab[i].size(); ++k)
									f << mtab[i][k] << " ";
									//f << "(" << mtab[i][k].first << " " << mtab[i][k].second << ") ";
								f << endl;
							}
						}

private:
	size_type	msize;	// number of all valid elements
	vector_type	mtab;
};



#endif // __ARRVECTOR_H__
