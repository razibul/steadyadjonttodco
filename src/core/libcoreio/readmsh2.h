#ifndef __IO_READMSH2_H__
#define __IO_READMSH2_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreio/gridfacade.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class ReadMSH2
//////////////////////////////////////////////////////////////////////
class ReadMSH2
{
public:
	ReadMSH2( GridReadFacade& grd, MGString title = "") : mGrd(grd), mTitle(title)	{}


	void	DoRead( const MGString& fname);
	void	DoRead( const MGString& fname, const bool& bin);
	void	DoRead( istream& file, const bool& bin);

	static bool			IsBin( const MGString& fname);
	static Dimension	ReadDim( const MGString& fname, const bool& bin);

protected:

	template <Dimension DIM> void	ReadMSHascii( istream& file);
	template <Dimension DIM> void	ReadMSHbin( istream& file);

	static bool	CheckVersion_ascii( istream& file);
	static bool	CheckVersion_bin( istream& file);

	static const MGString& VerNum()	{ return mstVersionNumber;}
	static const MGString& VerStr()	{ return mstVersionString;}

private:
	static MGString	mstVersionString;
	static MGString	mstVersionNumber;

	GridReadFacade&		mGrd;

	MGString		mTitle;
};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;



#endif // __IO_READMSH2_H__

