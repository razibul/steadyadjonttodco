#ifndef __IO_WRITETEC_H__
#define __IO_WRITETEC_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreio/gridfacade.h"
#include "libcoreio/solfacade.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class WriteTEC
//////////////////////////////////////////////////////////////////////
class WriteTEC
{
public:
	//WriteTEC( const GridWriteFacade& grd, const SolFacade* psol=NULL, MGString title = "") : mGrd(grd), mpSol(psol), mTitle(title)	{}
	WriteTEC( const GridWriteFacade& grd, const SolFacade* psol=NULL, MGString title = "") 
		: mGrd(grd), mTitle(title)	
	{ 
		if ( psol) 
			mtabpSol.push_back( psol); 
	}

	WriteTEC( const GridWriteFacade& grd, const vector<const SolFacade*>& tabpsol, MGString title = "") 
		: mGrd(grd), mTitle(title)	
	{ 
		mtabpSol = tabpsol;
	}

	void	DoWrite( const MGString& fname);
	void	DoWrite( ostream& file);


protected:
	template <Dimension DIM> void	WriteHeader( ostringstream& obuf, const MGSize& neq, const MGSize& ncell);
	template <Dimension DIM> void	WriteTECascii( ostream& file);
	template <Dimension DIM> void	ConvertCellIds( MGString& sbuf, const vector<MGSize>& tabid, const MGSize& n) const;
	template <Dimension DIM> void	ConvertBFaceIds( MGString& sbuf, const vector<MGSize>& tabid, const MGSize& n) const;

private:
	const GridWriteFacade&	mGrd;
	//const SolFacade			*mpSol;

	vector<const SolFacade*> mtabpSol;


	MGString			mFName;

	MGString			mTitle;
	vector<MGString>	mtabVars;

	MGString			mZoneT;
	MGString			mElemT;

	MGSize				mnNode;
	MGSize				mnElem;

};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __IO_WRITETEC_H__

