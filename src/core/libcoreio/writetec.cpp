#include "writetec.h"
#include "libcoreio/store.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {



	
template <>
void WriteTEC::ConvertCellIds<DIM_2D>( MGString& sbuf, const vector<MGSize>& tabid, const MGSize& n) const
{
	ostringstream os;

	switch ( n)
	{
	case 3:	// triangle
		os << tabid[0]+1 << " " << tabid[1]+1 << " " << tabid[2]+1 << " " << tabid[2]+1;
		break;

	case 4:	// quadrilateral
		os << tabid[0]+1 << " " << tabid[1]+1 << " " << tabid[2]+1 << " " << tabid[3]+1;
		break;

	default:
		THROW_INTERNAL( "WriteTEC<DIM_2D>::ConvertCellIds - unrecognized cell type");
	}

	sbuf = os.str();
}


template <>
void WriteTEC::ConvertCellIds<DIM_3D>( MGString& sbuf, const vector<MGSize>& tabid, const MGSize& n) const
{
	ostringstream os;

	switch ( n)
	{
	case 4:	// tetrahedron
		os << tabid[0]+1 << " " << tabid[1]+1 << " " << tabid[2]+1 << " " << tabid[2]+1 << " " << tabid[3]+1 << " " << tabid[3]+1 << " " << tabid[3]+1 << " " << tabid[3]+1;
		break;

	case 5:	// pyramid
		os << tabid[0]+1 << " " << tabid[1]+1 <<  " " <<tabid[2]+1 << " " << tabid[3]+1 << " " << tabid[4]+1 << " " << tabid[4]+1 << " " << tabid[4]+1 << " " << tabid[4]+1;
		break;

	case 6:	// prism
		os << tabid[0]+1 << " " << tabid[1]+1 << " " << tabid[2]+1 << " " << tabid[2]+1 << " " << tabid[3]+1 << " " << tabid[4]+1 << " " << tabid[5]+1 << " " << tabid[5]+1;
		break;

	case 8:	// hexahedron
		os << tabid[0]+1 << " " << tabid[1]+1 << " " << tabid[2]+1 << " " << tabid[3]+1 << " " << tabid[4]+1 << " " << tabid[5]+1 << " " << tabid[6]+1 << " " << tabid[7]+1;
		break;

	default:
		THROW_INTERNAL( "WriteTEC<DIM_3D>::ConvertCellIds - unrecognized cell type");
	}

	sbuf = os.str();
}

template <Dimension DIM>
void WriteTEC::ConvertBFaceIds( MGString& sbuf, const vector<MGSize>& tabid, const MGSize& n) const
{
}



template <>
void WriteTEC::WriteHeader<DIM_2D>( ostringstream& obuf, const MGSize& neq, const MGSize& ncell)
{
	obuf << "TITLE = \"" << mTitle << "\"" << endl;
	obuf << "VARIABLES = ";
	obuf << "\"X\", \"Y\"";

	for ( MGSize i=0; i<neq; ++i)
		obuf << ", \"V" << setw(2) << setfill('0') << i+1 << "\"";
	obuf << endl;

	obuf << "ZONE T=\"" << mTitle << "\", N=" << mGrd.IOSizeNodeTab() << ", E=" << ncell 
		 << ", F=FEPOINT, ET=QUADRILATERAL\n";

	//file << "TITLE = \"" << mTitle << "\"" << endl;
	//file << "VARIABLES = ";
	//file << "\"X\", \"Y\"";

	//for ( MGSize i=0; i<neq; ++i)
	//	file << ", \"V" << setw(2) << setfill('0') << i+1 << "\"";
	//file << endl;

	//file << "ZONE T=\"" << mTitle << "\", N=" << mGrd.IOSizeNodeTab() << ", E=" << ncell 
	//	 << ", F=FEPOINT, ET=QUADRILATERAL\n";
}


template <>
void WriteTEC::WriteHeader<DIM_3D>( ostringstream& obuf, const MGSize& neq, const MGSize& ncell)
{
	obuf << "TITLE = \"" << mTitle << "\"" << endl;
	obuf << "VARIABLES = ";
	obuf << "\"X\", \"Y\", \"Z\"";

	for ( MGSize i=0; i<neq; ++i)
		obuf << ", \"V" << setw(2) << setfill('0') << i+1 << "\"";
	obuf << endl;

	obuf << "ZONE T=\"" << mTitle << "\", N=" << mGrd.IOSizeNodeTab() << ", E=" << ncell 
		 << ", F=FEPOINT, ET=BRICK\n";


	//file << "TITLE = \"" << mTitle << "\"" << endl;
	//file << "VARIABLES = ";
	//file << "\"X\", \"Y\", \"Z\"";

	//for ( MGSize i=0; i<neq; ++i)
	//	file << ", \"V" << setw(2) << setfill('0') << i+1 << "\"";
	//file << endl;

	//file << "ZONE T=\"" << mTitle << "\", N=" << mGrd.IOSizeNodeTab() << ", E=" << ncell 
	//	 << ", F=FEPOINT, ET=BRICK\n";
}


template <Dimension DIM>
void WriteTEC::WriteTECascii( ostream& file)
{
	ostringstream obuf;

	MGString	sbuf;

	MGSize neq = 0;
	MGSize ncell = 0;

	for ( MGSize i=0; i<mGrd.IONumCellTypes(); ++i)
		ncell += mGrd.IOSizeCellTab( mGrd.IOCellType( i) );
	
	//if ( mpSol)
	//{
	//	if ( mpSol->IOSize() != mGrd.IOSizeNodeTab() )
	//		THROW_INTERNAL( "Incompatible vector sizes in WriteSolTEC()" );

	//	neq = mpSol->IOBlockSize();
	//}

	for ( MGSize isol=0; isol<mtabpSol.size(); ++isol)
	{
		if ( mtabpSol[isol]->IOSize() != mGrd.IOSizeNodeTab() )
			THROW_INTERNAL( "Incompatible vector sizes in WriteSolTEC() isol = "<<isol );

		neq += mtabpSol[isol]->IOBlockSize();
	}

	cout << "neq = " << neq << endl;
	WriteHeader<DIM>( obuf, neq, ncell);

	obuf << setfill(' ');

	// nodes nad states at nodes

	vector<MGFloat> tabState(neq);
	vector<MGFloat>	tabx(DIM);

	for ( MGSize i=0; i<mGrd.IOSizeNodeTab(); ++i)
	{
		mGrd.IOGetNodePos( tabx, i);

		for ( MGSize k=0; k<DIM; ++k)
			obuf << setprecision(16) << setw(23) << tabx[k] << " ";

		for ( MGSize isol=0; isol<mtabpSol.size(); ++isol)
		{
			mtabpSol[isol]->IOGetBlockVector( tabState, i);
			mtabpSol[isol]->UndimToDim( tabState );

			for ( MGSize k=0; k<mtabpSol[isol]->IOBlockSize(); ++k)
				obuf << setprecision(16) << setw(23) << tabState[k] << " ";
		}

		//if ( mpSol)
		//{
		//	mpSol->IOGetBlockVector( tabState, i);
		//	mpSol->UndimToDim( tabState );

		//	for ( MGSize k=0; k<neq; ++k)
		//		obuf << setprecision(16) << setw(23) << tabState[k] << " ";
		//}

		obuf << endl;
	}

	//file.write( obuf.str().c_str() , obuf.str().size()*sizeof(char) );
	//obuf.str().clear();

	// cells

	vector<MGSize> tabid;

	MGSize nb = mGrd.IONumCellTypes();

	for ( MGSize k=0; k<nb; ++k)
	{
		MGSize ntyp = mGrd.IOCellType( k);
		MGSize n = mGrd.IOSizeCellTab( ntyp);
		tabid.resize(ntyp);

		for ( MGSize i=0; i<n; ++i)
		{
			mGrd.IOGetCellIds( tabid, ntyp, i);

			ConvertCellIds<DIM>( sbuf, tabid, ntyp);

			obuf << sbuf << endl;
		}
	}

	file.write( obuf.str().c_str() , obuf.str().size()*sizeof(char) );
}


//template <Dimension DIM>
//void WriteTEC::WriteTECascii( ostream& file)
//{
//	MGString	sbuf;
//
//	MGSize neq = 0;
//	MGSize ncell = 0;
//
//	for ( MGSize i=0; i<mGrd.IONumCellTypes(); ++i)
//		ncell += mGrd.IOSizeCellTab( mGrd.IOCellType( i) );
//	
//	if ( mpSol)
//	{
//		if ( mpSol->IOSize() != mGrd.IOSizeNodeTab() )
//			THROW_INTERNAL( "Incompatible vector sizes in WriteSolTEC()" );
//
//		neq = mpSol->IOBlockSize();
//	}
//
//	WriteHeader<DIM>( file, neq, ncell);
//
//	file << setfill(' ');
//
//	// nodes nad states at nodes
//
//	vector<MGFloat> tabState(neq);
//	vector<MGFloat>	tabx(DIM);
//
//	for ( MGSize i=0; i<mGrd.IOSizeNodeTab(); ++i)
//	{
//		mGrd.IOGetNodePos( tabx, i);
//
//		for ( MGSize k=0; k<DIM; ++k)
//			file << setprecision(16) << setw(23) << tabx[k] << " ";
//
//		if ( mpSol)
//		{
//			mpSol->IOGetBlockVector( tabState, i);
//			mpSol->UndimToDim( tabState );
//
//			for ( MGSize k=0; k<neq; ++k)
//				file << setprecision(16) << setw(23) << tabState[k] << " ";
//		}
//
//		file << endl;
//	}
//
//
//	// cells
//
//	vector<MGSize> tabid;
//
//	MGSize nb = mGrd.IONumCellTypes();
//
//	for ( MGSize k=0; k<nb; ++k)
//	{
//		MGSize ntyp = mGrd.IOCellType( k);
//		MGSize n = mGrd.IOSizeCellTab( ntyp);
//		tabid.resize(ntyp);
//
//		for ( MGSize i=0; i<n; ++i)
//		{
//			mGrd.IOGetCellIds( tabid, ntyp, i);
//
//			ConvertCellIds<DIM>( sbuf, tabid, ntyp);
//
//			file << sbuf << endl;
//		}
//	}
//
//}



inline void WriteTEC::DoWrite( ostream& file )
{
	for ( MGSize isol=0; isol<mtabpSol.size(); ++isol)
		if ( mtabpSol[isol]->Dim() != mGrd.Dim() )
			THROW_INTERNAL( "WriteTEC::DoWrite() - Incompatible dimension of grid and solution; isol = "<<isol);

	//if ( mpSol)
	//	if ( mpSol->Dim() != mGrd.Dim() )
	//		THROW_INTERNAL( "WriteTEC::DoWrite() - Incompatible dimension of grid and solution");

	if ( mGrd.Dim() == DIM_2D )
	{
		WriteTECascii<DIM_2D>( file);
	}
	else if ( mGrd.Dim() == DIM_3D )
	{
		WriteTECascii<DIM_3D>( file);
	}
	else
		THROW_INTERNAL( "WriteTEC::DoWrite - bad dim");

}


void WriteTEC::DoWrite( const MGString& fname)
{
	ofstream file( fname.c_str(), ios::out);

	try
	{
		if ( ! file)
			THROW_FILE( "can not open the file", fname);

		cout << "Writing TEC " << fname;
		cout.flush();

		DoWrite( file);
		cout <<  " - FINISHED" << endl;
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: WriteTEC::DoWrite - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}





} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

