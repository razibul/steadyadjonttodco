#ifndef __WRITEVTK_H__
#define __WRITEVTK_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreio/gridfacade.h"
#include "libcoreio/solfacade.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
class AddInfo
{
public:
    AddInfo(MGString nname, MGSize i) : name(nname), valueTab(vector<MGFloat>(i, 0.0)) {}
   
    const MGString& cName() const {return this->name;}
    const vector<MGFloat>& cValueTab() const {return this->valueTab;}
   
    vector<MGFloat>& rValueTab() {return this->valueTab;}
private:
    MGString name;
    vector<MGFloat> valueTab;
};

//////////////////////////////////////////////////////////////////////
// class WriteVTK
//////////////////////////////////////////////////////////////////////
class WriteVTK
{
public:
    WriteVTK( const GridWriteFacade& grd, SolFacade* psol=NULL, MGString title = "") : mGrd(grd), mpSol(psol), mTitle(title) {}

    void    DoWrite( const MGString& fname, const vector<AddInfo >* addInfo = static_cast<vector<AddInfo >* >(NULL) );
    void    DoWrite( ostream& file, const vector<AddInfo >* addInfo = static_cast<vector<AddInfo >* >(NULL) );
    template <Dimension DIM>
    void    DoWriteCellCentered( const MGString& fname, const vector<AddInfo >* addInfo = static_cast<vector<AddInfo >* >(NULL) );

protected:
    template <Dimension DIM> void   WriteVTKascii( ostream& file, const vector<AddInfo >* addInfo = static_cast<vector<AddInfo >* >(NULL) );

private:
    const GridWriteFacade&	mGrd;
    const SolFacade			*mpSol;
   
    MGString            mFName;

    MGString            mTitle;
    vector<MGString>    mtabVars;

    MGString            mZoneT;
    MGString            mElemT;

    MGSize              mnNode;
    MGSize              mnElem;

};

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __WRITEVTK_H__
  