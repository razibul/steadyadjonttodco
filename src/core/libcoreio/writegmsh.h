#ifndef __IO_WRITEGMSH_H__
#define __IO_WRITEGMSH_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreio/gridfacade.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class WriteGMSH
//////////////////////////////////////////////////////////////////////
class WriteGMSH
{
public:
	WriteGMSH( GridWriteFacade& grd, MGString title = "") : mGrd(grd), mTitle(title)	{}


	void	DoWrite( const MGString& fname, const bool& bin);
	void	DoWrite( ostream& file, const bool& bin);

protected:
	template <Dimension DIM> void	WriteMSHascii( ostream& file);
	template <Dimension DIM> void	WriteMSHbin( ostream& file);

	template <Dimension DIM> MGSize	GetGMSHtype( const MGSize& type);

private:
	GridWriteFacade&		mGrd;

	MGString		mTitle;
};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;



#endif // __IO_WRITEGMSH_H__
 