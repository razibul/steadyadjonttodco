#include "libcoreio/readplot3d.h"
#include "libcoreio/store.h"
#include "libcorecommon/regexp.h"
#include "libcorecommon/progressbar.h"
#include "libcoregeom/tree.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

void ReadPlot3D::ExtractBoundary()
{
	cout << endl;
	cout << "ExtractBoundary" << endl;


	vector< Key<2> > tabface;
	vector< Key<2> >::iterator itr, itre;

	tabface.reserve( 2 * mtabCell.size() );


	for ( int i=0; i<mtabCell.size(); ++i)
	{
		Key<3>& cell = mtabCell[i];
		Key<2> key;

		key = Key<2>( cell.cElem(0), cell.cElem(1) );	
		key.Sort();
		tabface.push_back( key );

		key = Key<2>( cell.cElem(1), cell.cElem(2) );	
		key.Sort();
		tabface.push_back( key );

		key = Key<2>( cell.cElem(2), cell.cElem(0) );	
		key.Sort();
		tabface.push_back( key );

	}

	//cout << " before = " << tabedge.size() << endl;
	sort( tabface.begin(), tabface.end() );

	vector< Key<2> > tabbnd;
	for ( itr=tabface.begin(); itr!=tabface.end(); ++itr)
	{
		vector< Key<2> >::iterator itrnext = itr+1;

		if ( itrnext!=tabface.end() )
		{
			if ( *itr != *itrnext)
				tabbnd.push_back( *itr);
			else
				itr = itrnext;
		}
		else
			tabbnd.push_back( *itr);
	}

	cout << " tabbnd = " << tabbnd.size() << endl;

	// orient
	sort( tabbnd.begin(), tabbnd.end() );

	vector<bool> taborient( tabbnd.size(), true);
	for ( int i=0; i<mtabCell.size(); ++i)
	{
		Key<3>& cell = mtabCell[i];
		Key<2> key, keysort;
		pair< vector< Key<2> >::iterator, vector< Key<2> >::iterator > result;

		keysort = key = Key<2>( cell.cElem(0), cell.cElem(1) );
		keysort.Sort();
		if ( key != keysort )
		{
			result = equal_range( tabbnd.begin(), tabbnd.end(), keysort );
			if ( *result.first == keysort)
				taborient[ result.first - tabbnd.begin() ] = false;
		}


		keysort = key = Key<2>( cell.cElem(1), cell.cElem(2) );	
		keysort.Sort();
		if ( key != keysort )
		{
			result = equal_range( tabbnd.begin(), tabbnd.end(), keysort );
			if ( *result.first == keysort)
				taborient[ result.first - tabbnd.begin() ] = false;
		}


		keysort = key = Key<2>( cell.cElem(2), cell.cElem(0) );	
		keysort.Sort();
		if ( key != keysort )
		{
			result = equal_range( tabbnd.begin(), tabbnd.end(), keysort );
			if ( *result.first == keysort)
				taborient[ result.first - tabbnd.begin() ] = false;
		}

	}

	//itre = unique( tabbnd.begin(), tabbnd.end() );
	//cout << " tabbnd unique = " << itre - tabbnd.begin() << endl;


	// NACA 0012
	mtabBnd.resize( 2 );		// 0 - viscous; 1 - farfield

	Vect<DIM_2D> vcglob( 0.5, 0.);

	for ( int i=0; i<tabbnd.size(); ++i)
	{
		Key<2>& cell = tabbnd[i];

		if ( taborient[i] )
			cell.Reverse();

		Vect<DIM_2D> vc = 0.5 * ( mtabNode[cell.cFirst()] + mtabNode[cell.cSecond()]  );

		if ( (vc - vcglob).module() < 1.0)
			mtabBnd[0].push_back( cell);
		else
			mtabBnd[1].push_back( cell);
	}


	//// flatplate
	//mtabBnd.resize( 5 );		// 0 - viscous; 1 - farfield

	//MGFloat x0 = 1.0e-6;
	//MGFloat xLeft = -0.332;
	//MGFloat xRight = 1.995;
	//MGFloat yTop = 0.99;
	//MGFloat yBottom = 1.0e-7;

	//Vect<DIM_2D> vcglob( 0.5, 0.);

	//for ( int i=0; i<tabbnd.size(); ++i)
	//{
	//	Key<2>& cell = tabbnd[i];

	//	if ( taborient[i] )
	//		cell.Reverse();

	//	Vect<DIM_2D> v1 = mtabNode[cell.cFirst()];
	//	Vect<DIM_2D> v2 = mtabNode[cell.cSecond()];

	//	if ( v1.cX() < xLeft && v2.cX() < xLeft )
	//		mtabBnd[0].push_back( cell);
	//	else
	//	if ( v1.cY() > yTop && v2.cY() > yTop )
	//		mtabBnd[1].push_back( cell);
	//	else
	//	if ( v1.cX() > xRight && v2.cX() > xRight )
	//		mtabBnd[2].push_back( cell);
	//	else
	//	if ( v1.cY() < yBottom && v2.cY() < yBottom && v1.cX() > -x0 && v2.cX() > -x0 )
	//		mtabBnd[3].push_back( cell);
	//	else
	//	if ( v1.cY() < yBottom && v2.cY() < yBottom && v1.cX() < x0 && v2.cX() < x0 )
	//		mtabBnd[4].push_back( cell);
	//	else
	//		THROW_INTERNAL( "BC CRASH");

	//}




	//////////////////////
	string name = "out_surf.dat";
	cout << "Write surf TEC - " << name << endl;

	ofstream of( name.c_str());

	for ( int u=0; u<mtabBnd.size(); ++u)
	{
		of << "VARIABLES = \"X\", \"Y\"" << endl;
		of << "ZONE T=\"grid\", N=" << mtabNode.size() << ", E=" << mtabBnd[u].size() << ", F=FEPOINT, ET=LINESEG" << endl;

		for ( int i=0; i<mtabNode.size(); ++i)
		{
			Vect2D& v = mtabNode[i];
			of << setprecision( 16) << v.cX() << " " << v.cY() << endl;
		}

		for ( int i=0; i<mtabBnd[u].size(); ++i)
		{
			of << mtabBnd[u][i].cFirst()+1 << " " << mtabBnd[u][i].cSecond()+1  << endl;
		}
	}

}



void ReadPlot3D::Stitch()
{
	// find TE
	MGSize ite;
	for ( MGSize i=0; i<mtabBlockN[0].cX(); ++i)
	{
		Vect<DIM_2D> vt = mtabNode[i];
		Vect<DIM_2D> vb = mtabNode[ mtabBlockN[0].cX()-1 - i];

		if ( (vb - vt).module() > 1.0e-14 )
		{
			ite = i;
			break;
		}
	}

	cout << "ite = " << ite << endl;

	mtabNode.erase( mtabNode.begin(), mtabNode.begin() + ite );

	for ( MGSize i=0; i<mtabCell.size(); ++i)
		for ( MGSize in=0; in<3; ++in)
			if ( mtabCell[i].cElem(in) >= ite )
				mtabCell[i].rElem(in) -= ite;
			else
				mtabCell[i].rElem(in) = mtabBlockN[0].cX() - 1 - mtabCell[i].cElem(in) - ite;

	// boundary

}


void ReadPlot3D::RemoveDuplicatePoints( const MGFloat& zero)
{
	cout << endl;
	cout << "RemoveDuplicateNodes" << endl;

	multimap< Vect2D, int>	nodmap;

	for ( int i=0; i<mtabNode.size(); ++i)
		nodmap.insert( multimap< Vect2D, int>::value_type( mtabNode[i], i) );

	cout << "mtabNode.size() = " << mtabNode.size() << endl;
	cout << "nodmap.size() = " << nodmap.size() << endl;

	multimap< Vect2D, int>::iterator	itr, itrold;

	map<int,int>	idmap;
	vector<Vect2D>	tabnn;

	int id = 0;
	itrold = itr = nodmap.begin();

	tabnn.push_back( itr->first);
	idmap.insert( map<int,int>::value_type( itr->second, id ) );
		
	itr->second = id;

	for ( ++itr; itr!=nodmap.end(); ++itr, ++itrold)
	{
		if ( ! ( itrold->first == itr->first ) )
		{
			tabnn.push_back( itr->first);
			++id;
		}

		idmap.insert( map<int,int>::value_type( itr->second, id ) );
		itr->second = id;
	}

	cout << id << " " << tabnn.size() << endl;

	for ( int i=0; i<mtabCell.size(); ++i)
	{
		Key<3>& cell = mtabCell[i];

		cell.rFirst()  = idmap[cell.cFirst()];
		cell.rSecond() = idmap[cell.cSecond()];
		cell.rThird()  = idmap[cell.cThird()];
	}

	mtabNode.swap( tabnn);
	cout << mtabNode.size() << endl;
}


void ReadPlot3D::CopyToGrid()
{
	mGrd.IOTabNodeResize( mtabNode.size() );

	vector<MGFloat>	tabx(DIM_2D);

	for ( MGSize i=0; i<mtabNode.size(); ++i)
	{
		for ( MGInt j=0; j<DIM_2D; ++j)
			tabx[j] = mtabNode[i].cX(j);

		mGrd.IOSetNodePos( tabx, i );

	}

	MGSize	ntyp = DIM_2D+1;	// only simplex grid is supported
	vector<MGSize> tabid;

	tabid.resize( ntyp);
	mGrd.IOTabCellResize( ntyp, mtabCell.size() );

	for ( MGSize i=0; i<mtabCell.size(); ++i)
	{
		tabid[0] = mtabCell[i].cFirst();;
		tabid[1] = mtabCell[i].cThird();;
		tabid[2] = mtabCell[i].cSecond();;
		mGrd.IOSetCellIds( tabid, ntyp, i);
	}

	MGSize nbface = 0;
	for ( MGSize ibc=0; ibc<mtabBnd.size(); ++ibc)
		nbface += mtabBnd[ibc].size();


	mGrd.IOTabBFaceResize( 2, nbface );

	tabid.resize( 2 );
	MGSize id = 0;
	for ( MGSize ibc=0; ibc<mtabBnd.size(); ++ibc)
	{
		for ( MGSize i=0; i<mtabBnd[ibc].size(); ++i)
		{
			tabid[0] = mtabBnd[ibc][i].cFirst();
			tabid[1] = mtabBnd[ibc][i].cSecond();
			mGrd.IOSetBFaceSurf( ibc+1, 2, id );
			mGrd.IOSetBFaceIds( tabid, 2, id );
			++id;
		}
	}

}


void ReadPlot3D::Read( istream& file)
{
	ProgressBar	bar(40);

	file >> mBlockN;
	
	mtabBlockN.resize( mBlockN );
	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
		file >> mtabBlockN[iblock].rX() >> mtabBlockN[iblock].rY();

	MGSize n=0;
	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
		n += mtabBlockN[iblock].cX() * mtabBlockN[iblock].cY();

	mtabNode.resize( n );

	MGSize offset = 0;
	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
	{
		MGSize nnode = mtabBlockN[iblock].cX() * mtabBlockN[iblock].cY();
		vector< Vect<DIM_2D> > tabnode;
		tabnode.resize( nnode );

		for ( MGSize i=0; i<mtabBlockN[iblock].cY(); ++i)
			for ( MGSize j=0; j<mtabBlockN[iblock].cX(); ++j)
				file >> mtabNode[ i*mtabBlockN[iblock].cX() + j + offset].rX();

		for ( MGSize i=0; i<mtabBlockN[iblock].cY(); ++i)
			for ( MGSize j=0; j<mtabBlockN[iblock].cX(); ++j)
				file >> mtabNode[ i*mtabBlockN[iblock].cX() + j + offset].rY();

		offset = nnode;
	}

	MGSize	ntyp = DIM_2D+1;

	MGSize nc=0;
	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
		nc += ( mtabBlockN[iblock].cX()-1) * (mtabBlockN[iblock].cY()-1);

	mtabCell.resize( 2*nc);

	MGSize id = 0;
	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
	{
		// cells

		MGSize nxcell = mtabBlockN[iblock].cX() - 1; 
		MGSize nycell = mtabBlockN[iblock].cY() - 1; 
		MGSize ncell =  nxcell * nycell;

		vector<MGSize> tabid;
		tabid.resize( ntyp);

		for ( MGSize i=0; i<nycell; ++i)
			for ( MGSize j=0; j<nxcell; ++j)
				//if ( j < nxcell/2)
				if ( j >= nxcell/2)
				{
					mtabCell[id].rElem(0) = i    *mtabBlockN[iblock].cX() + j;
					mtabCell[id].rElem(1) = (i+1)*mtabBlockN[iblock].cX() + j;
					mtabCell[id].rElem(2) = (i+1)*mtabBlockN[iblock].cX() + j+1;
					++id;

					mtabCell[id].rElem(0) = i    *mtabBlockN[iblock].cX() + j;
					mtabCell[id].rElem(1) = (i+1)*mtabBlockN[iblock].cX() + j+1;
					mtabCell[id].rElem(2) = i    *mtabBlockN[iblock].cX() + j+1;
					++id;
				}
				else
				{
					mtabCell[id].rElem(0) = i    *mtabBlockN[iblock].cX() + j;
					mtabCell[id].rElem(1) = (i+1)*mtabBlockN[iblock].cX() + j;
					mtabCell[id].rElem(2) = i    *mtabBlockN[iblock].cX() + j+1;
					++id;

					mtabCell[id].rElem(0) = (i+1)*mtabBlockN[iblock].cX() + j;
					mtabCell[id].rElem(1) = (i+1)*mtabBlockN[iblock].cX() + j+1;
					mtabCell[id].rElem(2) = i    *mtabBlockN[iblock].cX() + j+1;
					++id;
				}
	}

	//Stitch();
	RemoveDuplicatePoints();
	ExtractBoundary();

	CopyToGrid();
}


//template <Dimension DIM> 
//void ReadPlot3D::Read( istream& file)
//{
//	ProgressBar	bar(40);
//
//	file >> mBlockN;
//	
//	mtabBlockN.resize( mBlockN );
//	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
//		file >> mtabBlockN[iblock].rX() >> mtabBlockN[iblock].rY();
//
//	MGSize n=0;
//	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
//		n += mtabBlockN[iblock].cX() * mtabBlockN[iblock].cY();
//
//	mGrd.IOTabNodeResize( n );
//
//	MGSize offset = 0;
//	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
//	{
//		MGSize nnode = mtabBlockN[iblock].cX() * mtabBlockN[iblock].cY();
//		vector< Vect<DIM_2D> > tabnode;
//		tabnode.resize( nnode );
//
//		for ( MGSize i=0; i<mtabBlockN[iblock].cY(); ++i)
//			for ( MGSize j=0; j<mtabBlockN[iblock].cX(); ++j)
//				file >> tabnode[ i*mtabBlockN[iblock].cX() + j].rX();
//
//		for ( MGSize i=0; i<mtabBlockN[iblock].cY(); ++i)
//			for ( MGSize j=0; j<mtabBlockN[iblock].cX(); ++j)
//				file >> tabnode[ i*mtabBlockN[iblock].cX() + j].rY();
//
//		vector<MGFloat>	tabx(DIM_2D);
//		for ( MGSize i=0; i<nnode; ++i)
//		{
//			tabx[0] = tabnode[i].cX();
//			tabx[1] = tabnode[i].cY();
//			mGrd.IOSetNodePos( tabx, i + offset );
//		}
//
//		offset = nnode;
//	}
//
//	MGSize	ntyp = DIM_2D+1;
//
//	MGSize nc=0;
//	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
//		nc += ( mtabBlockN[iblock].cX()-1) * (mtabBlockN[iblock].cY()-1);
//
//	mGrd.IOTabCellResize( ntyp, 2*nc);
//
//	MGSize id = 0;
//	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
//	{
//		// cells
//
//		MGSize nxcell = mtabBlockN[iblock].cX() - 1; 
//		MGSize nycell = mtabBlockN[iblock].cY() - 1; 
//		MGSize ncell =  nxcell * nycell;
//
//		vector<MGSize> tabid;
//		tabid.resize( ntyp);
//
//		for ( MGSize i=0; i<nycell; ++i)
//			for ( MGSize j=0; j<nxcell; ++j)
//				if ( j < nxcell/2)
//				{
//					tabid[0] = i    *mtabBlockN[iblock].cX() + j;
//					tabid[1] = (i+1)*mtabBlockN[iblock].cX() + j;
//					tabid[2] = (i+1)*mtabBlockN[iblock].cX() + j+1;
//					mGrd.IOSetCellIds( tabid, ntyp, id++);
//
//					tabid[0] = i    *mtabBlockN[iblock].cX() + j;
//					tabid[1] = (i+1)*mtabBlockN[iblock].cX() + j+1;
//					tabid[2] = i    *mtabBlockN[iblock].cX() + j+1;
//					mGrd.IOSetCellIds( tabid, ntyp, id++);
//				}
//				else
//				{
//					tabid[0] = i    *mtabBlockN[iblock].cX() + j;
//					tabid[1] = (i+1)*mtabBlockN[iblock].cX() + j;
//					tabid[2] = i    *mtabBlockN[iblock].cX() + j+1;
//					mGrd.IOSetCellIds( tabid, ntyp, id++);
//
//					tabid[0] = (i+1)*mtabBlockN[iblock].cX() + j;
//					tabid[1] = (i+1)*mtabBlockN[iblock].cX() + j+1;
//					tabid[2] = i    *mtabBlockN[iblock].cX() + j+1;
//					mGrd.IOSetCellIds( tabid, ntyp, id++);
//				}
//
//	}
//
//
////	read(2,*) nbl
////     read(2,*) (idim(n),jdim(n),kdim(n),n=1,nbl)
////      do n=1,nbl
////        read(2,*) (((x(i,j,k,n),i=1,idim(n)),j=1,jdim(n)),k=1,kdim(n)),
////     +            (((y(i,j,k,n),i=1,idim(n)),j=1,jdim(n)),k=1,kdim(n)),
////     +            (((z(i,j,k,n),i=1,idim(n)),j=1,jdim(n)),k=1,kdim(n))
////      enddo
//
//
//	//MGSize nvar = mtabVar.size() - DIM;
//	//if ( mtabVar.size() < DIM)
//	//	THROW_INTERNAL("ReadTEC::ReadTECascii :: negative nvar = " << nvar );
////
////
////
////	mGrd.IOTabNodeResize( mNNode);
////	if ( mpSol) mpSol->IOResize( mNNode);
////
////	vector<MGFloat>	tabx(DIM);
////	vector<MGFloat>	tabs(nvar);
////
////	cout << endl;
////	bar.Init( mNNode );
////	bar.Start();
////	for ( MGSize i=0; i<mNNode; ++i, ++bar)
////	{
////		for ( MGInt j=0; j<DIM; ++j)
////			file >> tabx[j];
////
////		mGrd.IOSetNodePos( tabx, i );
////
////		for ( MGSize j=0; j<nvar; ++j)
////			file >> tabs[j];
////
////		if ( mpSol)
////			mpSol->IOSetBlockVector( tabs, i);
////
////	}
////	bar.Finish();
////
////	MGSize	ntyp = DIM+1;	// only simplex grid is supported
////	vector<MGSize> tabid;
////
////	tabid.resize( ntyp);
////	mGrd.IOTabCellResize( ntyp, mNCell);
////
////	bar.Init( mNCell );
////	bar.Start();
////	for ( MGSize i=0; i<mNCell; ++i, ++bar)
////	{
////		ReadCell<DIM>( file, tabid, i);
////		mGrd.IOSetCellIds( tabid, ntyp, i);
////	}
////	bar.Finish();
////
////	//cout << endl;
//}


void ReadPlot3D::DoRead( istream& file )
{
	if ( mGrd.Dim() == DIM_2D )
	{
		Read( file);
	}
	else
		THROW_INTERNAL( "ReadPlot3D::DoRead() - bad dim");
}


void ReadPlot3D::DoRead( const MGString& fname)
{
	ifstream file( fname.c_str(), ios::in);

	try
	{
		if ( ! file)
			THROW_FILE( "can not open the file", fname);

		cout << "Reading Plot3D '" << fname << "' :";
		cout.flush();

		DoRead( file);

		cout <<  "Reading Plot3D - FINISHED" << endl;
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: ReadPlot3D::DoRead() - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

