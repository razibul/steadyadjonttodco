#ifndef __DUMMYSOL_H__
#define __DUMMYSOL_H__

#include "libcoreio/solfacade.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

//////////////////////////////////////////////////////////////////////
// class DummySol
//////////////////////////////////////////////////////////////////////
class DummySol : public SolFacade
{
public:
	DummySol( const MGSize& n, const MGSize& bn, const Dimension& dim, const MGFloat& d=0.0)
		: mDim(dim), mSize(n), mBlockSize(bn), mVal(d)	{}

	virtual Dimension	Dim() const		{ return mDim;}

	virtual void	IOGetName( MGString& name) const	{ name = "dummy";}
	virtual void	IOSetName( const MGString& name)	{}

	virtual MGSize	IOSize()					{ return mSize;}
	virtual MGSize	IOBlockSize() const			{ return mBlockSize;}

	virtual void	IOResize( const MGSize& n)	{ mSize = n;}

	virtual void	IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const	
		{ for (MGSize i=0; i<mBlockSize; ++i) tabx[i] = mVal; }

	virtual void	IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id )	{}

	virtual void	DimToUndim( vector<MGFloat>& tab ) const	{}
	virtual void	UndimToDim( vector<MGFloat>& tab ) const	{}

private:
	Dimension	mDim;
	MGSize		mBlockSize;
	MGSize		mSize;
	MGFloat		mVal;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __DUMMYSOL_H__
