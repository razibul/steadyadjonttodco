#ifndef __IO_READPLOT3D_H__
#define __IO_READPLOT3D_H__

#include "libcoresystem/mgdecl.h"
#include "libcorecommon/key.h"
#include "libcoregeom/dimension.h"
#include "libcoregeom/vect.h"

#include "libcoreio/gridfacade.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class ReadTEC
//////////////////////////////////////////////////////////////////////
class ReadPlot3D
{
public:
	ReadPlot3D( GridReadFacade& grd) : mGrd(grd)	{}

	void	DoRead( const MGString& fname);
	void	DoRead( istream& file);

protected:
	void	Read( istream& file);
	void	Stitch();
	void	CopyToGrid();
	void	RemoveDuplicatePoints( const MGFloat& zero = 1.0e-12);
	void	ExtractBoundary();

private:
	GridReadFacade&		mGrd;

	MGString			mFileTitle;

	MGSize	mBlockN;
	vector< Geom::Vect<DIM_2D,MGSize> > mtabBlockN;

	vector< Vect<DIM_2D> >		mtabNode;
	vector< Key<3> >			mtabCell;
	vector< vector<Key<2> > >	mtabBnd;
};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __IO_READPLOT3D_H__
