#include "writevtk.h"
#include "store.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


template <Dimension DIM>
/**
 * @brief
 * @param file
 * @param addInfo Node oriented additional data
 */
void WriteVTK::WriteVTKascii( ostream& file, const vector<AddInfo >* addInfo )
{
    file<<"# vtk DataFile Version 2.0"<<endl;
    file<<this->mTitle<<endl;
    file<<"ASCII"<<endl<<endl;

    file<<"DATASET UNSTRUCTURED_GRID"<<endl;
    file<<"POINTS "<<this->mGrd.IOSizeNodeTab()<<" float"<<endl;
    for ( MGSize i=0; i<mGrd.IOSizeNodeTab(); ++i)
    {
        vector<MGFloat> tabx(DIM);
        mGrd.IOGetNodePos( tabx, i);

        for ( MGSize k=0; k<DIM; ++k)
        {
            file << setprecision(16) << tabx[k] << " ";
        }
        if ( DIM == DIM_2D )  file << 5.0;
        file << endl;
    }

    file<<endl;
    MGSize ncell = this->mGrd.IOSizeCellTab(DIM+1);
    MGSize nnum = ncell * (DIM+1 +1);
    file<<"CELLS "<<ncell<<" "<<nnum<<endl;
    for ( MGSize i=0; i<ncell; ++i)
    {
        vector<MGSize> tabid(DIM+1);
        mGrd.IOGetCellIds( tabid, DIM+1, i);
        file<<DIM+1<<" ";
        for(int j=0; j<DIM+1; ++j)
            file<<tabid[j]<<" ";
        file<<endl;
    }

    file<<endl;
    file<<"CELL_TYPES "<<ncell<<endl;
    for ( MGSize i=0; i<ncell; ++i)
    {
        switch (DIM)
        {
            case DIM_2D:
                file<<5<<endl;
                break;
            case DIM_3D:
                file<<10<<" ";
                break;
        }
    }
    file<<endl;

	// boundary nodes flag
	{
		MGSize nb = mGrd.IONumBFaceTypes();
		if ( nb != 1)
			THROW_INTERNAL( "only one type of bfaes is supported !!!");

		MGSize ntyp = mGrd.IOBFaceType( 0);
		MGSize n = mGrd.IOSizeBFaceTab( ntyp);

		vector<MGSize> tabbn;
		tabbn.reserve( DIM * n);

		vector<MGSize>	tabid(ntyp);

		for ( MGSize i=0; i<n; ++i)
		{
			mGrd.IOGetBFaceIds( tabid, ntyp, i);
			for ( MGSize k=0; k<ntyp; ++k)
				tabbn.push_back( tabid[k] );
		}

		sort( tabbn.begin(), tabbn.end() );
		tabbn.erase( unique( tabbn.begin(), tabbn.end() ), tabbn.end() );

        file<<endl;
        file<<"POINT_DATA "<<this->mGrd.IOSizeNodeTab()<<endl;
		file << "SCALARS fixed int" << endl;
		file << "LOOKUP_TABLE default" << endl;

		for ( MGSize i=0; i<mGrd.IOSizeNodeTab(); ++i)
			if ( binary_search( tabbn.begin(), tabbn.end(), i) )
				file << " 1" << endl;
			else
				file << " 0" << endl;

	}


    if ( mpSol || addInfo != NULL)//Write Point Data from solution
    {
        file<<endl;
        file<<"POINT_DATA "<<this->mGrd.IOSizeNodeTab()<<endl;

        if(mpSol)
        {
            MGSize neq  = mpSol->IOBlockSize();
            vector<MGFloat> tabState(neq);
            for(MGSize eq = 0; eq < neq; ++eq)
            {
                file<<"SCALARS V0"<<eq+1<<" float"<<endl;
                file<<"LOOKUP_TABLE V0"<<eq+1<<endl;
                for ( MGSize i=0; i<mGrd.IOSizeNodeTab(); ++i)
                {
                    mpSol->IOGetBlockVector( tabState, i);
                    mpSol->UndimToDim( tabState );
                    file << setprecision(8) << tabState[eq] << endl;
                }
                file<<endl;
            }
        }
        if ( addInfo != NULL)//Additional data
        {
            MGSize neq  = (*addInfo).size();
            for(MGSize eq = 0; eq < neq; ++eq)
            {
                file<<"SCALARS "<<(*addInfo)[eq].cName()<<" float"<<endl;
                file<<"LOOKUP_TABLE V0"<<eq+1<<endl;
                for ( MGSize i=0; i<mGrd.IOSizeNodeTab(); ++i)
                {
                    file << setprecision(8) << (*addInfo)[eq].cValueTab()[i] << endl;
                }
                file<<endl;
            }
        }
    }
}



inline void WriteVTK::DoWrite( ostream& file, const vector<AddInfo >* addInfo )
{
    if ( mpSol)
        if ( mpSol->Dim() != mGrd.Dim() )
            THROW_INTERNAL( "WriteVTK::DoWrite() - Incompatible dimension of grid and solution");

    if ( mGrd.Dim() == DIM_2D )
    {
        WriteVTKascii<DIM_2D>( file, addInfo);
    }
    else if ( mGrd.Dim() == DIM_3D )
    {
        WriteVTKascii<DIM_3D>( file, addInfo);
    }
    else
        THROW_INTERNAL( "WriteVTK::DoWrite - bad dim");

}

void WriteVTK::DoWrite( const MGString& fname, const vector<AddInfo >* addInfo)
{
    ofstream file( fname.c_str(), ios::out);

    try
    {
        if ( ! file)
            THROW_FILE( "can not open the file", fname);

        cout << "Writing TEC " << fname;
        cout.flush();

        DoWrite( file, addInfo);
        cout <<  " - FINISHED" << endl;
    }
    catch ( EHandler::Except& e)
    {
        cout << "ERROR: WriteVTK::DoWrite - file name: " << fname << endl << endl;
        TRACE_EXCEPTION( e);
        TRACE_TO_STDERR( e);

        THROW_INTERNAL("END");
    }
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

  