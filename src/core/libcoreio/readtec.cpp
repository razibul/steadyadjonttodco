#include "libcoreio/readtec.h"
#include "libcoreio/store.h"
#include "libcorecommon/regexp.h"
#include "libcorecommon/progressbar.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


const char TEC_TITLE[]		= "TITLE";
const char TEC_FILETYPE[]	= "FILETYPE";
const char TEC_VARIABLES[]	= "VARIABLES";

const char TEC_ZONE[]		= "ZONE";


 

void ReadTEC::InitVariables( MGString& sparams)
{
	MGString	buf;
	RegExp		rex;

	rex.InitPattern( "^\"([^\", \t]*)\"[, \t]*(.*)$");
	rex.SetString( sparams);
	while ( rex.IsOk() )
	{
		buf = rex.GetSubString(2);

		mtabVar.push_back( rex.GetSubString(1));

		rex.InitPattern( "^\"([^\", \t]*)\"[, \t]*(.*)$");
		rex.SetString( buf);
	}

	//for ( MGSize i=0; i<mtabVar.size(); ++i)
	//	cout << mtabVar[i] << endl;
}



void ReadTEC::InitZone( MGString& sparams)
{
	istringstream is;

	RegExp	rex;

	// number of nodes
	rex.InitPattern( ",[ \t]*(Nodes|N)[ \t]*=[ \t]*([0-9]*)" );
	rex.SetString( sparams);
	if ( rex.IsOk() )
	{
		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mNNode;

		//cout << mNNode << endl;
	}
	else
		THROW_INTERNAL( "ReadTEC::ReadHeader :: ZONE failed to find the NODES - " << is.str() );


	// number of cells
	rex.InitPattern( ",[ \t]*(Elements|E)[ \t]*=[ \t]*([0-9]*)" );
	rex.SetString( sparams);
	if ( rex.IsOk() )
	{
		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mNCell;

		//cout << mNCell << endl;
	}
	else
		THROW_INTERNAL( "ReadTEC::ReadHeader :: ZONE failed to find the CELLS - " << is.str() );

	// element type
	rex.InitPattern( ",[ \t]*(ZONETYPE|ET)[ \t]*=[ \t]*([^, \t]*)" );
	rex.SetString( sparams);
	if ( rex.IsOk() )
	{
		//cout << "rex 1 = "<< rex.GetSubString( 1) << endl;
		//cout << "rex 2 = "<< rex.GetSubString( 2) << endl;

		mEType = rex.GetSubString( 2);
		StripBlanks( mEType);
		//cout << endl << mEType << endl;
	}
	else
		THROW_INTERNAL( "ReadTEC::ReadHeader :: ZONE failed to find the ET - " << is.str() );

}


void ReadTEC::ReadHeader( istream& file)
{
	istringstream is;
	MGString	sname, sver;
	do
	{
		IO::ReadLine( file, is, "#");
		is >> sname;

		if ( sname == MGString( TEC_TITLE ) )
		{
			RegExp	rex;
			rex.InitPattern( "^([A-Z]*)[ \t]*=[ \t]*\"(.*)\"(.*)$" );
			rex.SetString( is.str());

			if ( rex.IsOk() )
			{
				mFileTitle = rex.GetSubString( 2);
			}
			else
				THROW_INTERNAL( "ReadTEC::ReadHeader :: failed to recognize the TITLE - " << is.str() );

		}
		else
		if ( sname == MGString( TEC_VARIABLES ) )
		{
			RegExp		rex;
			MGString	buf;

			rex.InitPattern( "^([A-Z]*)[ \t]*=[ \t]*(.*)$" );
			rex.SetString( is.str());


			if ( rex.IsOk() )
			{
				//cout << "rex 1 = "<< rex.GetSubString( 1) << endl;
				//cout << "rex 2 = "<< rex.GetSubString( 2) << endl;
				buf = rex.GetSubString( 2);

				StripBlanks( buf);

				//cout << endl;
				//cout << "----------- buf = "<< buf << endl;

				do
				{
					IO::ReadLine( file, is, "#");
					is >> sname;

					StripBlanks( sname);
					//cout << sname << endl;
					//for ( MGSize i=0; i<sname.size(); ++i)
					//	cout << "'" << sname[i] << "'" << endl;

					if ( sname == MGString( TEC_ZONE) || sname == MGString( TEC_TITLE) || sname == MGString( TEC_FILETYPE)  )
						break;

					//buf += ", " + is.str();
					buf += ", " + sname;
					
					//cout << endl;
					//cout << "----------- buf = "<< buf << endl;
					
				}
				while ( true);

				//cout << endl;
				//cout << "----------- buf = "<< buf << endl;
				
				InitVariables( buf);
			}
			else
				THROW_INTERNAL( "ReadTEC::ReadHeader :: failed to recognize the VARIABLES - " << is.str() );

			if ( mtabVar.size() == 0 )
				THROW_INTERNAL( "ReadTEC::ReadHeader :: failed to initialize the VARIABLES - " << is.str() );

		}

		//cout << sname << endl;
		//cout << is.str() << endl;
	}
	while ( sname != MGString( "ZONE") );

	streampos mark;

	MGString zonedef = is.str();
	do
	{
		mark = file.tellg();
		IO::ReadLine( file, is, "#");
		is >> sname;

		if ( is.str().find_first_of( "=" ) == is.str().npos )
		{
			file.seekg( mark);
			break;
		}

		zonedef += ", " + is.str();
	}
	while ( true);

	//cout << zonedef << endl;
	InitZone( zonedef);

}


template <Dimension DIM> 
void ReadTEC::ReadCell( istream& file, vector<MGSize>& tabid, const MGSize& id)
{
}

template <> 
void ReadTEC::ReadCell<DIM_2D>( istream& file, vector<MGSize>& tabid, const MGSize& id)
{
	vector<MGSize> tablocid;

	if ( mEType == MGString( "FEQuadrilateral") || mEType == MGString( "QUADRILATERAL") )
	{
		tablocid.resize( 4);
		for ( MGSize j=0; j<4; ++j)
			file >> tablocid[j];

		if ( tablocid[2] != tablocid[3] )
			THROW_INTERNAL("ReadTEC::ReadCell<DIM_2D> :: it is NOT a triangle" );

		tabid[0] = tablocid[0]-1;
		tabid[1] = tablocid[1]-1;
		tabid[2] = tablocid[2]-1;
	}
	else
	if ( mEType == MGString( "TRIANGLE") )
	{
		tablocid.resize( 3);
		for ( MGSize j=0; j<3; ++j)
			file >> tablocid[j];

		tabid[0] = tablocid[0]-1;
		tabid[1] = tablocid[1]-1;
		tabid[2] = tablocid[2]-1;
	}
	else
	THROW_INTERNAL( "ReadTEC::ReadCell<DIM_2D> :: unknown cell type '" << mEType << "'" );
}


template <> 
void ReadTEC::ReadCell<DIM_3D>( istream& file, vector<MGSize>& tabid, const MGSize& id)
{
	vector<MGSize> tablocid;

	if ( mEType == MGString( "BRICK") )
	{
		tablocid.resize( 8);
		for ( MGSize j=0; j<8; ++j)
			file >> tablocid[j];

		if ( tablocid[2] != tablocid[3] || tablocid[4] != tablocid[5] || tablocid[5] != tablocid[6] || tablocid[6] != tablocid[7] )
			THROW_INTERNAL("ReadTEC::ReadCell<DIM_3D> :: it is NOT a tetrahedron" );

		tabid[0] = tablocid[0]-1;
		tabid[1] = tablocid[1]-1;
		tabid[2] = tablocid[2]-1;
		tabid[3] = tablocid[4]-1;
	}
	else
	if ( mEType == MGString( "TETRAHEDRON") )
	{
		tablocid.resize( 4);
		for ( MGSize j=0; j<4; ++j)
			file >> tablocid[j];

		tabid[0] = tablocid[0]-1;
		tabid[1] = tablocid[1]-1;
		tabid[2] = tablocid[2]-1;
		tabid[3] = tablocid[3]-1;
	}
	else
		THROW_INTERNAL( "ReadTEC::ReadCell<DIM_3D> :: unknown cell type '" << mEType << "'" );
}


template <Dimension DIM> 
void ReadTEC::ReadTECascii( istream& file)
{
	ProgressBar	bar(40);

	//cout << endl;

	//cout << " --- 1 " << endl;
	ReadHeader( file);

	//cout << " --- mtabVar.size()  = " << mtabVar.size() << endl;

	MGSize nvar = mtabVar.size() - DIM;
	if ( mtabVar.size() < DIM)
		THROW_INTERNAL("ReadTEC::ReadTECascii :: negative nvar = " << nvar );



	mGrd.IOTabNodeResize( mNNode);
	if ( mpSol) mpSol->IOResize( mNNode);

	vector<MGFloat>	tabx(DIM);
	vector<MGFloat>	tabs(nvar);

	cout << endl;
	bar.Init( mNNode );
	bar.Start();
	for ( MGSize i=0; i<mNNode; ++i, ++bar)
	{
		for ( MGInt j=0; j<DIM; ++j)
			file >> tabx[j];

		mGrd.IOSetNodePos( tabx, i );

		for ( MGSize j=0; j<nvar; ++j)
			file >> tabs[j];

		if ( mpSol)
			mpSol->IOSetBlockVector( tabs, i);

	}
	bar.Finish();

	MGSize	ntyp = DIM+1;	// only simplex grid is supported
	vector<MGSize> tabid;

	tabid.resize( ntyp);
	mGrd.IOTabCellResize( ntyp, mNCell);

	bar.Init( mNCell );
	bar.Start();
	for ( MGSize i=0; i<mNCell; ++i, ++bar)
	{
		ReadCell<DIM>( file, tabid, i);
		mGrd.IOSetCellIds( tabid, ntyp, i);
	}
	bar.Finish();

	//cout << endl;
}


void ReadTEC::DoRead( istream& file )
{
	if ( mpSol)
		if ( mpSol->Dim() != mGrd.Dim() )
			THROW_INTERNAL( "ReadTEC::DoRead() - Incompatible dimension of grid and solution");

	if ( mGrd.Dim() == DIM_2D )
	{
		ReadTECascii<DIM_2D>( file);
	}
	else if ( mGrd.Dim() == DIM_3D )
	{
		ReadTECascii<DIM_3D>( file);
	}
	else
		THROW_INTERNAL( "ReadTEC::DoRead() - bad dim");
}


void ReadTEC::DoRead( const MGString& fname)
{
	ifstream file( fname.c_str(), ios::in);

	try
	{
		if ( ! file)
			THROW_FILE( "can not open the file", fname);

		cout << "Reading TEC '" << fname << "' :";
		cout.flush();

		DoRead( file);

		cout <<  "Reading TEC - FINISHED" << endl;
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: ReadTEC::DoRead() - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

