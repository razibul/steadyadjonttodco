#ifndef __IO_WRITEINTF_H__
#define __IO_WRITEINTF_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreio/intffacade.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//





//////////////////////////////////////////////////////////////////////
// class WriteINTF
//////////////////////////////////////////////////////////////////////
class WriteINTF
{
public:
	WriteINTF( IntfFacade& intf, MGString title = "") : mIntf(intf), mTitle(title)	{}


	void	DoWrite( const MGString& fname, const bool& bin );
	void	DoWrite( ostream& file, const bool& bin );

protected:
	template <Dimension DIM> void	WriteINTFascii( ostream& file);
	template <Dimension DIM> void	WriteINTFbin( ostream& file);

	static const MGString& VerNum()	{ return mstVersionNumber;}
	static const MGString& VerStr()	{ return mstVersionString;}

private:
	static MGString	mstVersionString;
	static MGString	mstVersionNumber;

	IntfFacade&		mIntf;

	MGString		mTitle;

};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __IO_WRITEINTF_H__

