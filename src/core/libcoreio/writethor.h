#ifndef __IO_WRITETHOR_H__
#define __IO_WRITETHOR_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreio/gridfacade.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class WriteTHOR
//////////////////////////////////////////////////////////////////////
class WriteTHOR
{
public:
	WriteTHOR( GridWriteFacade& grd, MGString title = "") : mGrd(grd), mTitle(title)	{}


	void	DoWrite( const MGString& fname);
	void	DoWrite( ostream& file);

protected:
	template <Dimension DIM> void	WriteMSHascii( ostream& file);

private:
	GridWriteFacade&	mGrd;

	MGString		mTitle;
};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;



#endif // __IO_WRITETHOR_H__
