#ifndef __IO_STORE_H__
#define __IO_STORE_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/vect.h"
#include "libcorecommon/regexp.h"



using namespace Geom;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {




const char	STR_COMMENT[] = "//";


extern const char STORE_TYPE_ASCII[];
extern const char STORE_TYPE_BIN[];



inline void ReadLine( istream& f, MGString& buf, const MGString& comm = STR_COMMENT)
{
	getline( f, buf);
	buf = buf.substr(0,buf.find( comm));

	//MGSize id = buf.find( comm);
	//if ( id < buf.size() )
	//	buf.erase( id, buf.size());
}


inline void ReadLine( istream& f, istringstream& isbuf, const MGString& comm = STR_COMMENT)
{
	MGString buf;
	ReadLine( f, buf, comm);

	isbuf.clear();
	isbuf.str( buf);
}




// reads line until ; is found
// removes white-space chars and C like comments
// if eof reached returns false
bool ReadLineC( istream& file, MGString& buf);


// reads line until ; is found
// removes C like comments
// if eof reached returns false
bool ReadLineCww( istream& file, MGString& buf);


inline void Strip( MGString& str, const MGString& pat)
{
   if ( str.empty() ) 
	   return;

   MGSize startIndex = str.find_first_not_of( pat.c_str());
   MGSize endIndex = str.find_last_not_of( pat.c_str());
   MGString tempstr = str;

   str.erase();
   str = tempstr.substr( startIndex, ( endIndex - startIndex + 1) );
}


inline void StripBlanks( MGString& str)
{
	Strip( str, " \t\n\r");
}


// if BIN returns true
// if ASCII returns false
// if unknown throws exception
inline bool FileTypeToBool( const MGString& str)
{
	if ( str == MGString( STORE_TYPE_BIN ) ) 
		return true;
	else if ( str == MGString( STORE_TYPE_ASCII ) )
		return false;
	else
	{
		ostringstream os;
		os << "unknown file type: '" << str << "'";
		THROW_INTERNAL( os.str() );
	}

	return false;
}


//////////////////////////////////////////////////////////////////////
// helper functions

MGSize ReadMtxId( vector< vector<MGSize> >& mtxres, const MGString& sparams);

MGSize ReadTabUInt( vector<MGSize>& tabres, const MGString& sparams);

MGSize ReadTabId( vector<MGSize>& tabres, const MGString& sparams);

MGSize ReadTabFloat( vector<MGFloat>& tabres, const MGString& sparams);

MGSize ReadMtxFloat( vector< vector<MGFloat> >& mtxres, const MGString& sparams);


template <Dimension DIM>
MGSize ReadTabVect( vector< Vect<DIM> >& tabres, const MGString& sparams)
{
	RegExp		rex;
	MGString	buf;

	istringstream is;
	vector<MGFloat>	tab;

	tabres.clear();

	rex.InitPattern( "^\\(([^\\(\\)]*)\\)(.*)$");
	rex.SetString( sparams);
	while ( rex.IsOk() )
	{
		buf = rex.GetSubString( 2);

		tab.clear();
		ReadTabFloat( tab, rex.GetSubString( 1) );
	
		Vect<DIM> vct;
		for ( MGSize k=0; k<DIM; ++k)
			vct.rX(k) = tab[k];

		tabres.push_back( vct);


		rex.InitPattern( "^,\\(([^\\(\\)]*)\\)(.*)$");
		rex.SetString( buf);
	}

	return 0;
}

template <Dimension DIM>
MGSize ReadMtxVect( vector< vector< Vect<DIM> > >& mtxres, const MGString& sparams)
{
	RegExp		rex;
	MGString	buf;

	istringstream is;
	vector< Vect<DIM> >	tab;

	mtxres.clear();

	rex.InitPattern( "^(.*)\\((\\(.+\\))\\)$");
	rex.SetString( sparams);
	while ( rex.IsOk() )
	{
		buf = rex.GetSubString( 1);

		tab.clear();
		ReadTabVect<DIM>( tab, rex.GetSubString( 2) );
	
		mtxres.insert( mtxres.begin() ,tab );


		rex.InitPattern( "^(.*)\\((\\(.+\\))\\),$");
		rex.SetString( buf);
	}

	return 0;
}





} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __IO_STORE_H__

