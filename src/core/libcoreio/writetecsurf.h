#ifndef __IO_WRITETECSURF_H__
#define __IO_WRITETECSURF_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreio/gridfacade.h"
#include "libcoreio/solfacade.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class WriteTECSurf
//////////////////////////////////////////////////////////////////////
class WriteTECSurf
{
public:
	WriteTECSurf( const GridBndFacade& grd, const SolFacade* psol=NULL, MGString title = "") 
		: mGrd(grd), mpSol(psol), mTitle(title)	{}

	void	DoWrite( const MGString& fname);
	void	DoWrite( ostream& file);


protected:
	template <Dimension DIM> void	WriteHeader( ostringstream& obuf, const MGString& title, const MGSize& neq, const MGSize& nnode, const MGSize& ncell);
	template <Dimension DIM> void	WriteTECascii( ostream& file);
	template <Dimension DIM> void	WriteTECascii_( ostream& file);
	template <Dimension DIM> void	ConvertBFaceIds( MGString& sbuf, const vector<MGSize>& tabid, const MGSize& n) const;

private:
	const GridBndFacade&	mGrd;
	const SolFacade			*mpSol;

	MGString			mFName;

	MGString			mTitle;
	vector<MGString>	mtabVars;

	MGString			mZoneT;
	MGString			mElemT;

	MGSize				mnNode;
	MGSize				mnElem;

};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __IO_WRITETECSURF_H__

