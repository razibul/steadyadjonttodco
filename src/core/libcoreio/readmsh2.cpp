
#include "readmsh2.h"
#include "libcoreio/store.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

MGString ReadMSH2::mstVersionString = "MSH_VER";
MGString ReadMSH2::mstVersionNumber = "2.0";


bool ReadMSH2::IsBin( const MGString& fname)
{
	ifstream ifile;

	ifile.open( fname.c_str(), ios::in | ios::binary);

	if ( CheckVersion_bin( ifile) )
		return true;

	return false;
}



bool ReadMSH2::CheckVersion_ascii( istream& file)
{
	istringstream is;
	MGString	sname, sver;

	ReadLine( file, is);
	is >> sname >> sver;

	if ( sname != VerStr() != 0 || sver != VerNum() != 0 )
		return false;

	return true;
}


bool ReadMSH2::CheckVersion_bin( istream& file)
{
	MGSize		n;
	MGString	sname, sver;

	file.read( (char*)&n, sizeof(MGSize) );
	if ( n > 255)
		return false;//THROW_INTERNAL( "bad binary file" );

	sname.resize( n);
	file.read( &sname[0], n);
	
	file.read( (char*)&n, sizeof(MGSize) );
	if ( n > 255)
		return false;//THROW_INTERNAL( "bad binary file" );

	sver.resize( n);
	file.read( &sver[0], n);

	if ( sname != VerStr() != 0 || sver != VerNum() != 0)
		return false;

	return true;
}


Dimension ReadMSH2::ReadDim( const MGString& fname, const bool& bin)
{
	Dimension	dim;
	ifstream	file;
	MGString	skey;

	if ( bin)
	{
		file.open( fname.c_str(), ios::in | ios::binary);
		
		if ( ! CheckVersion_bin( file) )
		{
			ostringstream os;
			os << "incompatible grid file version (current ver: '" << VerStr() << " " << VerNum() << "')";
	
			THROW_INTERNAL( os.str() );
		}
	
		file.read( (char*)&dim, sizeof(Dimension) );
	}
	else
	{
		file.open( fname.c_str(), ios::in );
		
		if ( ! CheckVersion_ascii( file) )
		{
			ostringstream os;
			os << "incompatible grid file version (current ver: '" << VerStr() << " " << VerNum() << "')";
	
			THROW_INTERNAL( os.str() );
		}
		
		istringstream is;
		
		ReadLine( file, is);	// dimension
		is >> skey;
	
		dim = ::StrToDim( skey);
	
	}

	return dim;
}


template <Dimension DIM>
inline void ReadMSH2::ReadMSHascii( istream& file)
{
	istringstream is;

	MGString	skey, sname, sver;
	MGSize		n;
	Dimension	dim;


	if ( ! CheckVersion_ascii( file) )
	{
		ostringstream os;
		os << "incompatible grid file version (current ver: '" << VerStr() << " " << VerNum() << "')";

		THROW_INTERNAL( os.str() );
	}

	ReadLine( file, is);	// dimension
	is >> skey;

	dim = ::StrToDim( skey);
	if ( dim != DIM)
		THROW_INTERNAL( "Incompatible dimension" );


	ReadLine( file, skey);	// description
	mGrd.IOSetName( skey);


	// nodes

	ReadLine( file, is);	// number of nodes
	is >> n;

	mGrd.IOTabNodeResize( n);


	vector<MGFloat>	tabx(DIM);

	for ( MGSize i=0; i<n; ++i)
	{
		ReadLine( file, is);
		for ( MGInt j=0; j<DIM; ++j)
		{
			is >> tabx[j];
			//tabx[j] /= lref;
		}

		mGrd.IOSetNodePos( tabx, i );

	}


	// cells

	MGSize	nb, ntyp, id;
	vector<MGSize> tabid;

	ReadLine( file, is);	// number of cell blocks
	is >> nb;

	for ( MGSize k=0; k<nb; ++k)
	{
		ReadLine( file, is);	// block type and number of cells
		is >> ntyp >> n;

		tabid.resize( ntyp);
		mGrd.IOTabCellResize( ntyp, n);

		for ( MGSize i=0; i<n; ++i)
		{
			ReadLine( file, is);
			for ( MGSize j=0; j<ntyp; ++j)
			{
				is >> id;
				tabid[j] = id-1;
			}

			mGrd.IOSetCellIds( tabid, ntyp, i );
		}

	}


	// bfaces

	ReadLine( file, is);	// number of cell blocks
	is >> nb;

	for ( MGSize k=0; k<nb; ++k)
	{
		ReadLine( file, is);	// block type and number of cells
		is >> ntyp >> n;

		tabid.resize( ntyp);

		if ( n)	// <--- !!!
		{
			mGrd.IOTabBFaceResize( ntyp, n);

			for ( MGSize i=0; i<n; ++i)
			{
				ReadLine( file, is);

				is >> id;
				mGrd.IOSetBFaceSurf( id, ntyp, i );


				//if ( id == 3 || id == 4)
				//{
				//	cout << "-- 34 " ;
				//	for ( MGSize j=0; j<ntyp; ++j)
				//	{
				//		is >> id;
				//		tabid[ntyp-j-1] = id-1;
				//	}
				//}
				//else
				//{
				//	for ( MGSize j=0; j<ntyp; ++j)
				//	{
				//		is >> id;
				//		tabid[j] = id-1;
				//	}
				//}

				for ( MGSize j=0; j<ntyp; ++j)
				{
					is >> id;
					tabid[j] = id-1;
				}

				mGrd.IOSetBFaceIds( tabid, ntyp, i );
			}
		}
		else
		{
			TRACE( "ReadMSH2::ReadMSHascii :: no boundary faces in here !!!!");
			cout << "ReadMSH2::ReadMSHascii :: no boundary faces in here !!!!" << endl;
		}

	}

}


template <Dimension DIM>
inline void ReadMSH2::ReadMSHbin( istream& file)
{
	MGString sbuf, sname, sver;
	MGSize	n, id;
	Dimension	dim;


	if ( ! CheckVersion_bin( file) )
	{
		ostringstream os;
		os << "incompatible grid file version (current ver: '" << VerStr() << " " << VerNum() << "')";

		THROW_INTERNAL( os.str() );
	}

	file.read( (char*)&dim, sizeof(Dimension) );
	if ( dim != DIM)
		THROW_INTERNAL( "Incompatible grid dimension" );

	file.read( (char*)&n, sizeof(MGSize) );
	sbuf.resize(n);
	file.read( (char*)&sbuf[0], n );
	mGrd.IOSetName( sbuf);
	
	// nodes

	file.read( (char*)&n, sizeof(MGSize) );
	mGrd.IOTabNodeResize( n);

	vector<MGFloat>	tabx(DIM);

	for ( MGSize i=0; i<n; ++i)
	{
		file.read( (char*)&tabx[0], DIM*sizeof(MGFloat) );
	
		mGrd.IOSetNodePos( tabx, i);
	}


	// cells
	MGSize	nb, ntyp;
	vector<MGSize>	tabid;

	file.read( (char*)&nb, sizeof(MGSize) );
	for ( MGSize k=0; k<nb; ++k)
	{
		file.read( (char*)&ntyp, sizeof(MGSize) );
		file.read( (char*)&n, sizeof(MGSize) );

		tabid.resize( ntyp);
		mGrd.IOTabCellResize( ntyp, n);

		for ( MGSize i=0; i<n; ++i)
		{
			file.read( (char*)&tabid[0], ntyp*sizeof(MGSize) );

			mGrd.IOSetCellIds( tabid, ntyp, i);
		}
	}

	// bfaces

	file.read( (char*)&nb, sizeof(MGSize) );
	for ( MGSize k=0; k<nb; ++k)
	{
		file.read( (char*)&ntyp, sizeof(MGSize) );
		file.read( (char*)&n, sizeof(MGSize) );

		tabid.resize( ntyp);

		if ( n)
		{
			mGrd.IOTabBFaceResize( ntyp, n);

			for ( MGSize i=0; i<n; ++i)
			{
				file.read( (char*)&id, sizeof(MGSize) );
				file.read( (char*)&tabid[0], ntyp*sizeof(MGSize) );

				mGrd.IOSetBFaceSurf( id, ntyp, i );
				mGrd.IOSetBFaceIds( tabid, ntyp, i);
			}
		}
		else
		{
			TRACE( "ReadMSH2::ReadMSHascii :: no boundary faces in here !!!!");
			cout << "ReadMSH2::ReadMSHascii :: no boundary faces in here !!!!" << endl;
		}
	}

}


void ReadMSH2::DoRead( istream& file, const bool& bin)
{
	if ( mGrd.Dim() == DIM_2D )
	{
		if ( bin)
			ReadMSHbin<DIM_2D>( file);
		else
			ReadMSHascii<DIM_2D>( file);
	}
	else if ( mGrd.Dim() == DIM_3D )
	{
		if ( bin)
			ReadMSHbin<DIM_3D>( file);
		else
			ReadMSHascii<DIM_3D>( file);
	}
	else
		THROW_INTERNAL( "ReadMSH2::DoRead - bad dim");
}


void ReadMSH2::DoRead( const MGString& fname, const bool& bin)
{
	ifstream ifile;

	if ( bin)
		ifile.open( fname.c_str(), ios::in | ios::binary);
	else
		ifile.open( fname.c_str(), ios::in );

	try
	{
		if ( ! ifile)
			THROW_FILE( "can not open the file", fname);

		DoRead( ifile, bin);
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: ReadMSH2::DoRead - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}

}


void ReadMSH2::DoRead( const MGString& fname)
{
	bool bin = IsBin( fname);

	ifstream ifile;

	if ( bin)
		ifile.open( fname.c_str(), ios::in | ios::binary);
	else
		ifile.open( fname.c_str(), ios::in );

	try
	{
		if ( ! ifile)
			THROW_FILE( "can not open the file", fname);

		DoRead( ifile, bin);
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: ReadMSH2::DoRead - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}

}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

