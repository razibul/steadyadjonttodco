#ifndef __IO_READSOL_H__
#define __IO_READSOL_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreio/solfacade.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//




//////////////////////////////////////////////////////////////////////
// class ReadSOL
//////////////////////////////////////////////////////////////////////
//template <class VECT>
class ReadSOL
{
public:
	ReadSOL( SolFacade& sol, MGString title = "") : mSol(sol), mTitle(title)	{}

	void	ReadHEADER( const MGString& fname, const bool& bin );
	void	ReadHEADER( istream& file, const bool& bin );

	void	DoRead( const MGString& fname, const bool& bin );
	void	DoRead( istream& file, const bool& bin );

	const Dimension&	cDim() const			{ return mDim;}
	const MGString&		cDescription() const	{ return mDesc;}
	const MGSize&		cBlockSize() const		{ return mNEq;}
	const MGSize&		cSize() const			{ return mN;}

	static bool			IsBin( const MGString& fname);
	static Dimension	ReadDim( const MGString& fname, const bool& bin); 
	
protected:
	//template <Dimension DIM> void	ReadSOLascii( istream& file);
	//template <Dimension DIM> void	ReadSOLbin( istream& file);
	
	static bool	CheckVersion_ascii( istream& file);
	static bool	CheckVersion_bin( istream& file); 
	
	static const MGString& VerNum()	{ return mstVersionNumber;}
	static const MGString& VerStr()	{ return mstVersionString;}

	void	ReadHEADERascii( istream& file );
	void	ReadHEADERbin( istream& file );

	void	ReadSOLascii( istream& file);
	void	ReadSOLbin( istream& file);

private:
	static MGString	mstVersionString;
	static MGString	mstVersionNumber;

	SolFacade&		mSol;

	MGString		mTitle;

	MGString	mDesc;
	Dimension	mDim;
	MGSize		mNEq;
	MGSize		mN;
};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __IO_READSOL_H__

