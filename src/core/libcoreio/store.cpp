#include "libcoreio/store.h"
#include "libcorecommon/regexp.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {



const char STORE_TYPE_ASCII[]	= "ASCII";
const char STORE_TYPE_BIN[]		= "BIN";


//#define SPACES " \t\r\n"
//inline string trim_right (const string & s, const string & t = SPACES)
//{
//	string d (s);
//	string::size_type i (d.find_last_not_of (t));
//	if (i == string::npos)
//		return "";
//	else
//		return d.erase (d.find_last_not_of (t) + 1) ;
//} 
//
//inline string trim_left (const string & s, const string & t = SPACES)
//{
//	string d (s);
//	return d.erase (0, s.find_first_not_of (t)) ;
//} 
//
//inline string trim (const string & s, const string & t = SPACES)
//{
//	string d (s);
//	return trim_left (trim_right (d, t), t) ;
//}  

bool ReadLineC( istream& file, MGString& buf)
{
	char	ch, chp;
	bool	bCom = false;
	bool	bCom2 = false;
	bool	bStr = false;

	ch = chp = '\0';
	buf.clear();

	file.get( ch);
	while ( ! file.eof() )
	{
		if ( chp == '/' && ch == '*' )
		{
			bCom = true;
			buf.erase( buf.end()-1);
		}

		if ( chp == '/' && ch == '/' )
		{
			bCom2 = true;
			buf.erase( buf.end()-1);
		}


		if ( !bCom && !bCom2 )
		{
			if ( ch == '\'' && !bStr )
				bStr = true;
			else
				if ( ch == '\'' && bStr )
					bStr = false;

			if ( bStr)
			{
				//if ( ch == '\n')
				//	THROW_FILE( "ERROR: new line character in the string constant", name.c_str() );

				if ( ch != '\n' && ch != '\r')
					buf.push_back( ch);
			}
			else
			{
				if ( ch != '\n' && ch != '\r' && ch != ' ' && ch != '\t')
					buf.push_back( ch);

				if ( ch == ';')
					return (!file.eof());
			}

		}


		if ( chp == '*' && ch == '/' )
			bCom = false;

		if ( ch == '\n' || ch == '\r')
			bCom2 = false;


		chp = ch;

		file.get( ch);

	}

	return (!file.eof());
}


bool ReadLineCww( istream& file, MGString& buf)
{
	char	ch, chp;
	bool	bCom = false;
	bool	bCom2 = false;
	bool	bStr = false;

	ch = chp = '\0';
	buf.clear();

	file.get( ch);
	while ( ! file.eof() )
	{
		if ( chp == '/' && ch == '*' )
		{
			bCom = true;
			buf.erase( buf.end()-1);
		}

		if ( chp == '/' && ch == '/' )
		{
			bCom2 = true;
			buf.erase( buf.end()-1);
		}


		if ( !bCom && !bCom2 )
		{
			if ( ch == '\'' && !bStr )
				bStr = true;
			else
				if ( ch == '\'' && bStr )
					bStr = false;

			if ( bStr)
			{
				//if ( ch == '\n')
				//	THROW_FILE( "ERROR: new line character in the string constant", name.c_str() );

				if ( ch != '\n' && ch != '\r')
					buf.push_back( ch);
			}
			else
			{
				if ( ch != '\n' && ch != '\r' && ch != '\t' )
					buf.push_back( ch);
				else
					buf.push_back( ' ');

				if ( ch == ';')
					return (!file.eof());
			}

		}


		if ( chp == '*' && ch == '/' )
			bCom = false;

		if ( ch == '\n' || ch == '\r')
			bCom2 = false;


		chp = ch;

		file.get( ch);

	}

	return (!file.eof());
}


//////////////////////////////////////////////////////////////////////
// helper functions

MGSize ReadMtxId( vector< vector<MGSize> >& mtxres, const MGString& sparams)
{
	vector<MGSize>	tab;

	MGString	buf;
	RegExp		rex;


	rex.InitPattern( "^\\(([^\\(\\)]*)\\)(.*)$");
	rex.SetString( sparams);
	while ( rex.IsOk() )
	{
		buf = rex.GetSubString(2);

		tab.clear();
		ReadTabId( tab, rex.GetSubString( 1) );
		mtxres.push_back( tab);


		if ( buf.size() == 0 )
			return mtxres.size();

		rex.InitPattern( "^,\\(([^\\(\\)]*)\\)(.*)$");
		rex.SetString( buf);
	}
	
	return 0;
}


MGSize ReadMtxFloat( vector< vector<MGFloat> >& mtxres, const MGString& sparams)
{
	vector<MGFloat>	tab;

	MGString	buf;
	RegExp		rex;


	rex.InitPattern( "^\\(([^\\(\\)]*)\\)(.*)$");
	rex.SetString( sparams);
	while ( rex.IsOk() )
	{
		buf = rex.GetSubString(2);

		tab.clear();
		ReadTabFloat( tab, rex.GetSubString( 1) );
		mtxres.push_back( tab);


		if ( buf.size() == 0 )
			return mtxres.size();

		rex.InitPattern( "^,\\(([^\\(\\)]*)\\)(.*)$");
		rex.SetString( buf);
	}
	
	return 0;
}




MGSize ReadTabUInt( vector<MGSize>& tabres, const MGString& sparams)
{
	MGSize		id;
	RegExp		rex;
	MGString	buf;

	istringstream is;

	
	rex.InitPattern( "^([0-9]*)(.*)$");
	rex.SetString( sparams);
	while ( rex.IsOk() )
	{
		buf = rex.GetSubString(2);

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> id;

		tabres.push_back( id);	


		if ( buf.size() == 0 )
			return tabres.size();

		rex.InitPattern( "^,([0-9]*)(.*)$");
		rex.SetString( buf);
	}

	return 0;
}


MGSize ReadTabId( vector<MGSize>& tabres, const MGString& sparams)
{
	MGSize		id;
	RegExp		rex;
	MGString	buf;

	istringstream is;

	
	rex.InitPattern( "^#([0-9]*)(.*)$");
	rex.SetString( sparams);
	while ( rex.IsOk() )
	{
		buf = rex.GetSubString(2);

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> id;

		tabres.push_back( id);	


		if ( buf.size() == 0 )
			return tabres.size();

		rex.InitPattern( "^,#([0-9]*)(.*)$");
		rex.SetString( buf);
	}

	return 0;
}


MGSize ReadTabFloat( vector<MGFloat>& tabres, const MGString& sparams)
{
	MGFloat		x;
	RegExp		rex;
	MGString	buf;

	istringstream is;

	tabres.clear();

	rex.InitPattern( "^([-0-9\\.eE\\+]*)(.*)$");
	rex.SetString( sparams);
	while ( rex.IsOk() )
	{
		buf = rex.GetSubString(2);

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> x;

		tabres.push_back( x);	


		if ( buf.size() == 0 )
			return tabres.size();

		rex.InitPattern( "^,([-0-9\\.eE\\+]*)(.*)$");
		rex.SetString( buf);
	}

	return 0;
}





} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;

