
#include "writesol.h"
#include "libcoreio/store.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {


MGString WriteSOL::mstVersionString = "SOL_VER";
MGString WriteSOL::mstVersionNumber = "2.1";

const MGSize MAX_NEQ = 10;



template <Dimension DIM> 
inline void WriteSOL::WriteSOLascii( ostream& file )
{
	ostringstream obuf;


	MGString	st;
	MGSize	n, neq;

	obuf << VerStr() << " " << VerNum() << endl;	// file version
	st = ::DimToStr( DIM);
	obuf << st << endl;			// dimension


	mSol.IOGetName( st);
	//st="-----";
	if ( st == "" )
		st = mTitle;

	obuf << st << endl;			// description

	neq=mSol.IOBlockSize();
	obuf << neq << endl;

	n=mSol.IOSize();
	obuf << n << endl;			// size of the solution vector


	vector<MGFloat>	tabx(neq);

	for ( MGSize i=0; i<n; ++i)
	{
		mSol.IOGetBlockVector( tabx, i);

		mSol.UndimToDim( tabx );

		for ( MGSize k=0; k<neq; ++k)
			obuf << " " << setprecision(16) << tabx[k];
		obuf << endl;
	}

	file.write( obuf.str().c_str() , obuf.str().size()*sizeof(char) );

}


template <Dimension DIM> 
inline void WriteSOL::WriteSOLbin( ostream& file)
{
	MGSize	n, neq;
	Dimension	dim;
	MGString	str;

	n = VerStr().size();
	file.write( (char*)&n, sizeof(MGSize) );
	file.write( VerStr().c_str(), n );
	
	n = VerNum().size();
	file.write( (char*)&n, sizeof(MGSize) );
	file.write( VerNum().c_str(), n );
	
	dim = DIM;
	file.write( (char*)&dim, sizeof(Dimension) );

	mSol.IOGetName( str);
	if ( str == "" )
		str = mTitle;

	n = str.size();
	file.write( (char*)&n, sizeof(MGSize) );
	file.write( str.c_str(), n );


	neq=mSol.IOBlockSize();
	file.write( (char*)&neq, sizeof(MGSize) );

	n=mSol.IOSize();
	file.write( (char*)&n, sizeof(MGSize) );

	vector<MGFloat>	tabx(neq);

	for ( MGSize i=0; i<n; ++i)
	{
		mSol.IOGetBlockVector( tabx, i);

		mSol.UndimToDim( tabx );

		file.write( (char*)&tabx[0], neq*sizeof(MGFloat) );
	}
}

inline void WriteSOL::DoWrite( ostream& file, const bool& bin )
{
	if ( mSol.Dim() == DIM_2D )
	{
		if ( bin)
			WriteSOLbin<DIM_2D>( file);
		else
			WriteSOLascii<DIM_2D>( file);
	}
	else if ( mSol.Dim() == DIM_3D )
	{
		if ( bin)
			WriteSOLbin<DIM_3D>( file);
		else
			WriteSOLascii<DIM_3D>( file);
	}
	else
		THROW_INTERNAL( "WriteSOL::DoWrite - bad dim");
}


void WriteSOL::DoWrite( const MGString& fname, const bool& bin)
{
	ofstream ofile;

	if ( bin)
		ofile.open( fname.c_str(), ios::out | ios::binary);
	else
		ofile.open( fname.c_str(), ios::out );

	try
	{
		if ( ! ofile)
			THROW_FILE( "can not open the file", fname);

		if ( bin)
			cout << "Writing SOL bin " << fname;
		else
			cout << "Writing SOL ascii " << fname;

		cout.flush();

		DoWrite( ofile, bin);

		cout <<  " - FINISHED" << endl;
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: WriteSOL::DoWrite - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}




} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

