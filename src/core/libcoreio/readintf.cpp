
#include "readintf.h"
#include "libcoreio/store.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {



MGString ReadINTF::mstVersionString = "INTF_VER";
MGString ReadINTF::mstVersionNumber = "0.1";



template <Dimension DIM> 
inline void ReadINTF::ReadINTFascii( istream& file )
{
	istringstream is;
	
	MGString	sbuf, skey, sname, sver;
	MGSize	n, nproc;
	Dimension	dim;


	ReadLine( file, is);	// version
	is >> sname >> sver;

	if ( sname != VerStr() != 0 || sver != VerNum() != 0)
	{
		ostringstream os;
		os << "'" << sname << "' '" << sver << "'" << endl;
		os << "incompatible interface file version (current ver: '" << VerStr() << " " << VerNum() << "')";

		THROW_INTERNAL( os.str() );
	}


	ReadLine( file, is);	// dimension
	is >> skey;

	dim = ::StrToDim( skey);
	if ( dim != DIM)
		THROW_INTERNAL( "Incompatible dimension" );

	ReadLine( file, skey);	// description
	mIntf.IOSetName( skey);

	ReadLine( file, is);	// number of processors
	is >> nproc;
	mIntf.IOSetProcNum( nproc);

	ReadLine( file, is);	// size of the interface vector
	is >> n;
	mIntf.IOTabIntfResize( n);

	vector<MGSize>	tabid(3);

	for ( MGSize i=0; i<n; ++i)
	{
		ReadLine( file, is);	// idlocnode, idproc, idremnode;
		for ( MGSize k=0; k<3; ++k)
		{
			is >> tabid[k];
			--tabid[k];
		}

		mIntf.IOSetIds( tabid, i);
	
	}

}

template <Dimension DIM> 
inline void ReadINTF::ReadINTFbin( istream& file)
{
	THROW_INTERNAL( "ReadINTF::ReadINTFbin - not implemented");
}


void ReadINTF::DoRead( istream& file, const bool& bin )
{
	if ( mIntf.Dim() == DIM_2D )
	{
		if ( bin)
			ReadINTFbin<DIM_2D>( file);
		else
			ReadINTFascii<DIM_2D>( file);
	}
	else if ( mIntf.Dim() == DIM_3D )
	{
		if ( bin)
			ReadINTFbin<DIM_3D>( file);
		else
			ReadINTFascii<DIM_3D>( file);
	}
	else
		THROW_INTERNAL( "ReadmINTF::DoRead - bad dim");
}


void ReadINTF::DoRead( const MGString& fname, const bool& bin)
{
	ifstream ifile;

	if ( bin)
		ifile.open( fname.c_str(), ios::in | ios::binary);
	else
		ifile.open( fname.c_str(), ios::in );

	try
	{
		if ( ! ifile)
			THROW_FILE( "can not open the file", fname);

		DoRead( ifile, bin);
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: ReadINTF::DoRead - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}

}


} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

