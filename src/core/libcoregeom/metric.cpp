#include "metric.h"

#include "Eigen"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {


//template <Dimension DIM, class ELEM_TYPE>
//inline void IntersectMetric( Metric<DIM,ELEM_TYPE>& tr, const Metric<DIM,ELEM_TYPE>& t1, const Metric<DIM,ELEM_TYPE>& t2)
//{
//	typedef SMatrix<DIM,ELEM_TYPE>	SMtx;
//	typedef SVector<DIM,ELEM_TYPE>	SVect;
//
//	SMtx	tmp, mtx1(t1), mtx2(t2);
//	SMtx	mtxDp1, mtxLI1, mtxL1;
//	SMtx	mtxDp2, mtxLI2, mtxL2;
//	SVect	vecD1, vecD2;
//
//	//printf( "\nINSIDE INTERSECTION\n");
//	tmp = mtx1;
//	tmp.Decompose( vecD1, mtxL1);
//	mtxLI1 = mtxL1;
//	mtxLI1.Transp();
//
//
//	tmp = mtx2;
//	tmp.Decompose( vecD2, mtxL2);
//	mtxLI2 = mtxL2;
//	mtxLI2.Transp();
//
//	mtxDp1 = mtxLI1 * mtx2 * mtxL1;
//	mtxDp2 = mtxLI2 * mtx1 * mtxL2;
//
//	for ( MGSize i=0; i<DIM; ++i)
//	{
//		vecD1(i) = max( mtxDp1(i,i), vecD1(i) );
//		vecD2(i) = max( mtxDp2(i,i), vecD2(i) );
//	}
//
//	mtx1.Assemble( mtxL1, vecD1, mtxLI1 );
//	mtx2.Assemble( mtxL2, vecD2, mtxLI2 );
//
//	mtx1.Invert();
//	mtx2.Invert();
//	mtxDp1 = 0.5*( mtx1 + mtx2 );
//	mtxDp1.Invert();
//
//	tr = Metric<DIM,ELEM_TYPE>( mtxDp1);
//}


template <Dimension DIM, class ELEM_TYPE>
void IntersectMetric( Metric<DIM,ELEM_TYPE>& tr, const Metric<DIM,ELEM_TYPE>& t1, const Metric<DIM,ELEM_TYPE>& t2)
{
	using namespace Eigen;

	typedef Matrix<ELEM_TYPE,DIM,DIM> EigenMatrix;

	typedef SMatrix<DIM,ELEM_TYPE>	SMtx;
	typedef SVector<DIM,ELEM_TYPE>	SVect;

	SMtx	mtx1(t1), mtx2(t2);

	EigenMatrix emtxM1, emtxM2, emtxM1_inv;

	if ( mtx1.Determinant() < ZERO)
	{
		tr = t2;
		return;
	}

	for ( MGSize i=0; i<DIM; ++i)
		for ( MGSize j=0; j<DIM; ++j)
		{
			emtxM1(i,j) = mtx1(i,j);
			emtxM2(i,j) = mtx2(i,j);
			//emtxM1_inv(i,j) = tmp(i,j);
		}

	emtxM1_inv = emtxM1.inverse();

	EigenMatrix emtxA = emtxM1_inv * emtxM2;
	//EigenMatrix emtxA = emtxM1.inverse() * emtxM2;

	EigenSolver<MatrixXd> es(emtxA);

	EigenMatrix P = es.eigenvectors().real();
	EigenMatrix D = es.eigenvalues().real().asDiagonal();

	EigenMatrix D1 = P.transpose() * emtxM1 * P;
	EigenMatrix D2 = P.transpose() * emtxM2 * P;

	for ( int i=0; i<D.rows(); ++i)
		D(i,i) = max( D1(i,i), D2(i,i) ); 

	EigenMatrix Pi = P.inverse();
	emtxA = Pi.transpose() * D * Pi;	

	//for ( MGSize i=0; i<DIM; ++i)
	//	for ( MGSize j=0; j<DIM; ++j)
	//		mtxred(i,j) = emtxA(i,j);

	for ( MGSize i=0; i<DIM; ++i)
		for ( MGSize j=0; j<DIM; ++j)
			tr(i,j) = emtxA(i,j);
}


template void IntersectMetric<DIM_1D,MGFloat>( Metric<DIM_1D,MGFloat>& tr, const Metric<DIM_1D,MGFloat>& t1, const Metric<DIM_1D,MGFloat>& t2);
template void IntersectMetric<DIM_2D,MGFloat>( Metric<DIM_2D,MGFloat>& tr, const Metric<DIM_2D,MGFloat>& t1, const Metric<DIM_2D,MGFloat>& t2);
template void IntersectMetric<DIM_3D,MGFloat>( Metric<DIM_3D,MGFloat>& tr, const Metric<DIM_3D,MGFloat>& t1, const Metric<DIM_3D,MGFloat>& t2);

} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


