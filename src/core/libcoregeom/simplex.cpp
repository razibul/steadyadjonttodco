#include "simplex.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {




MGSize ConnTriangle::mtabFaceConn[3][2] = { {2, 1}, {0,2}, {1,0} };

MGSize ConnTetrahedron::mtabFaceConn[4][3] = { {1,2,3}, {0,3,2}, {0,1,3}, {0,2,1}};;
MGSize ConnTetrahedron::mtabEdgeConn[6][2] = { {0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {2,3}};


template <>
MGFloat	Simplex<DIM_2D>::mcoeffALPHA	= 1.0 / 2.0;

template <>
MGFloat	Simplex<DIM_2D>::mcoeffBETA		= ::sqrt(3.0)/6.0;

template <>
MGFloat	Simplex<DIM_3D>::mcoeffALPHA	= 1.0 / 3.0;

template <>
MGFloat	Simplex<DIM_3D>::mcoeffBETA		= ::sqrt(6.0)/12.0;


//////////////////////////////////////////////////////////////////////
// class SimplexFace
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// DIM_2D

template <>
MGFloat SimplexFace<DIM_2D>::Area() const
{
	GVect v = *mtab[1] - *mtab[0];
	return v.module();
}

template <>
MGFloat SimplexFace<DIM_2D>::CrossedVol( const GVect& v1, const GVect& v2) const
{
	Simplex<DIM_2D> tetup( *mtab[0], *mtab[1], v1);
	Simplex<DIM_2D> tetlo( *mtab[0], *mtab[1], v2);
	
	MGFloat	volup = tetup.Volume();
	MGFloat	vollo = tetlo.Volume();
	
	if ( volup * vollo > 0.0 )
	{
		// both points are on the same side of the face
		return -1000;
	}
	
	Vect2D vup, vlo;
	
	if ( volup > 0 )
	{
		vup = v1;
		vlo = v2;
	}
	else
	{
		vup = v2;
		vlo = v1;
	}
	
	MGFloat vol0 = Simplex<DIM_2D>( *mtab[0], vlo, vup).Volume();
	MGFloat vol1 = Simplex<DIM_2D>( *mtab[1], vup, vlo).Volume();
	
	MGFloat volret = MIN( vol0, vol1 );
	
	return volret;
}


template <>
bool SimplexFace<DIM_2D>::IsCrossed( const GVect& v1, const GVect& v2) const
{
	MGFloat vol = CrossedVol( v1, v2);
	
	if ( vol >= -1.0e-16 )		// ZERO :: ?????????
	//if ( vol >= 0.0 )
		return true;

	return false;
}

//////////////////////////////////////////////////////////////////////
// DIM_3D

template <>
MGFloat SimplexFace<DIM_3D>::Area() const
{
	Vect3D	varea;
	Vect2D	tabv[SIZE];

	tabv[0] = Vect2D( mtab[0]->cY(), mtab[0]->cZ() );
	tabv[1] = Vect2D( mtab[1]->cY(), mtab[1]->cZ() );
	tabv[2] = Vect2D( mtab[2]->cY(), mtab[2]->cZ() );
	varea.rX() = orient2d( tabv[0].cTab(), tabv[1].cTab(), tabv[2].cTab() );

	tabv[0] = Vect2D( mtab[0]->cZ(), mtab[0]->cX() );
	tabv[1] = Vect2D( mtab[1]->cZ(), mtab[1]->cX() );
	tabv[2] = Vect2D( mtab[2]->cZ(), mtab[2]->cX() );
	varea.rY() = orient2d( tabv[0].cTab(), tabv[1].cTab(), tabv[2].cTab() );

	tabv[0] = Vect2D( mtab[0]->cX(), mtab[0]->cY() );
	tabv[1] = Vect2D( mtab[1]->cX(), mtab[1]->cY() );
	tabv[2] = Vect2D( mtab[2]->cX(), mtab[2]->cY() );
	varea.rZ() = orient2d( tabv[0].cTab(), tabv[1].cTab(), tabv[2].cTab() );

	return 0.5 * varea.module();
}



template <>
MGFloat SimplexFace<DIM_3D>::CrossedVol( const GVect& v1, const GVect& v2) const
{
	Simplex<DIM_3D> tetup( *mtab[0], *mtab[1], *mtab[2], v1);
	Simplex<DIM_3D> tetlo( *mtab[0], *mtab[1], *mtab[2], v2);
	
	MGFloat	volup = tetup.Volume();
	MGFloat	vollo = tetlo.Volume();
	
	if ( volup == 0.0 && vollo == 0.0 )
	{
		// points and triangle are colinear - crossing is indefinite
		return -500;
	}

	if ( volup * vollo > 0.0 )
	{
		// both points are on the same side of the triangle
		return -1000;
	}
	
	Vect3D vup, vlo;
	
	if ( volup > 0 )
	{
		vup = v1;
		vlo = v2;
	}
	else
	{
		vup = v2;
		vlo = v1;
	}
	
	MGFloat vol0 = Simplex<DIM_3D>( *mtab[1], vup, *mtab[2], vlo).Volume();
	MGFloat vol1 = Simplex<DIM_3D>( *mtab[2], vup, *mtab[0], vlo).Volume();
	MGFloat vol2 = Simplex<DIM_3D>( *mtab[0], vup, *mtab[1], vlo).Volume();
	
	MGFloat volret = MIN( vol0, MIN( vol1, vol2) );

	return volret;
}


template <>
bool SimplexFace<DIM_3D>::IsCrossed( const GVect& v1, const GVect& v2) const
{
	MGFloat vol = CrossedVol( v1, v2);
	
	//if ( volret >= -1.0e-16 )
	if ( vol >= 0.0 )
		return true;

	return false;
}


//////////////////////////////////////////////////////////////////////
// class Simplex
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// DIM_1D

template <>
bool Simplex<DIM_1D>::IsInside( const GVect& vct, const MGFloat& zero) const
{
	const MGFloat vol0 = mtab[1]->cX() - vct.cX();
	const MGFloat vol1 = vct.cX() -mtab[0]->cX();
	
	if ( (vol0 >= zero && vol1 >= zero) || (vol0 <= zero && vol1 <= zero) )
		return true;
	
	return false;
}

//////////////////////////////////////////////////////////////////////
// DIM_2D

template <>
bool Simplex<DIM_2D>::IsInside( const GVect& vct, const MGFloat& zero) const
{
	const MGFloat vol0 = orient2d( mtab[1]->cTab(), mtab[2]->cTab(), vct.cTab() );
	const MGFloat vol1 = orient2d( mtab[2]->cTab(), mtab[0]->cTab(), vct.cTab() );
	const MGFloat vol2 = orient2d( mtab[0]->cTab(), mtab[1]->cTab(), vct.cTab() );
	
	if ( vol0 >= zero && vol1 >= zero && vol2 >= zero  )
		return true;
	
	return false;
}

	
template <>
void Simplex<DIM_2D>::SphereCenterMetric( GVect& vct, const GMetric& metric) const
{
	GMetric::SMtx mtx(metric);

	GMetric::SMtx mtxA;
	GMetric::SVect vecB;
	GMetric::SVect vec;

	for ( MGSize j=0; j<DIM_2D; ++j)
	{
		vecB[j] = 0.5 * ( metric.Distance( *mtab[j]) - metric.Distance( *mtab[j+1]) );


		GMetric::SVect vecG;

		for ( MGSize i=0; i<DIM_2D; ++i)
			vecG[i] = mtab[j]->cX(i) - mtab[j+1]->cX(i);

		vec = mtx * vecG;
		for ( MGSize i=0; i<DIM_2D; ++i)
			mtxA(j,i) = vec[i];
	}

	mtxA.Invert();

	GMetric::SMtx mtxX = mtxA * vecB;

	vct = Vect2D( mtxX(0,0), mtxX(1,0) );

	//const MGFloat& e = metric(0,0);
	//const MGFloat& f = metric(1,0);
	//const MGFloat& g = metric(1,1);

	//const MGFloat& x1 = mtab[0]->cX();
	//const MGFloat& y1 = mtab[0]->cY();
	//const MGFloat& x2 = mtab[1]->cX();
	//const MGFloat& y2 = mtab[1]->cY();
	//const MGFloat& x3 = mtab[2]->cX();
	//const MGFloat& y3 = mtab[2]->cY();

	//MGFloat c1 = e*(x1*x1 - x2*x2) + 2*f*(x1*y1 - x2*y2) + g*(y1*y1 - y2*y2);
	//MGFloat c2 = e*(x1*x1 - x3*x3) + 2*f*(x1*y1 - x3*y3) + g*(y1*y1 - y3*y3);
	//c1 *= 0.5;
	//c2 *= 0.5;

	//const MGFloat a1 = e*(x1 - x2) + f*(y1 - y2);
	//const MGFloat b1 = g*(y1 - y2) + f*(x1 - x2);
	//const MGFloat a2 = e*(x1 - x3) + f*(y1 - y3);
	//const MGFloat b2 = g*(y1 - y3) + f*(x1 - x3);

	//const MGFloat d = b1*a2 - b2*a1;
	//const MGFloat xr = (b1*c2 - b2*c1)/d;
	//const MGFloat yr = (a2*c1 - a1*c2)/d;
	//
	//vct = Vect2D( xr, yr);
}

template <>
MGFloat Simplex<DIM_2D>::IsInsideSphereMetric( const GVect& vct, const GMetric& metric, const GMetric tabmetric[SIZE] ) const
{
/*	GVect v0 = vct;
	GVect vv, tabv[SIZE];

	GMetric met = metric;
	GMetric mtmp;

	met.Invert();

	for ( MGSize k=0; k<SIZE; ++k)
	{
		mtmp = tabmetric[k];
		mtmp.Invert();
		met += mtmp;
		v0 += *mtab[k];
	}

	met = ( 1.0 / static_cast<MGFloat>( SIZE + 1) ) * met;
	v0 /= static_cast<MGFloat>( SIZE + 1);

	met.Invert();
*/

	GVect v0 = vct;
	GVect vv, tabv[SIZE];

	GMetric met = metric;

	for ( MGSize k=0; k<SIZE; ++k)
	{
		met += tabmetric[k];
		v0 += *mtab[k];
	}

	met = ( 1.0 / static_cast<MGFloat>( SIZE + 1) ) * met;
	v0 /= static_cast<MGFloat>( SIZE + 1);

	//met = metric;

	SMatrix<2> mtx;

	met.EigenDecompose( mtx);
	met.EigenMultDecomp( tabv[0], *mtab[0] - v0, mtx );
	met.EigenMultDecomp( tabv[1], *mtab[1] - v0, mtx );
	met.EigenMultDecomp( tabv[2], *mtab[2] - v0, mtx );
	met.EigenMultDecomp( vv, vct - v0, mtx );

	//met.Decompose();
	//met.MultDecomp( tabv[0], *mtab[0] - v0 );
	//met.MultDecomp( tabv[1], *mtab[1] - v0 );
	//met.MultDecomp( tabv[2], *mtab[2] - v0 );
	//met.MultDecomp( vv, vct - v0 );

	MGFloat aret = incircle( tabv[0].cTab(), tabv[1].cTab(), tabv[2].cTab(), vv.cTab() );


	//for ( MGSize k=0; k<SIZE; ++k)
	//{
	//	met = tabmetric[0];

	//	met.Decompose();
	//	met.MultDecomp( tabv[0], *mtab[0] - v0 );
	//	met.MultDecomp( tabv[1], *mtab[1] - v0 );
	//	met.MultDecomp( tabv[2], *mtab[2] - v0 );
	//	met.MultDecomp( vv, vct - v0 );

	//	aret += incircle( tabv[0].cTab(), tabv[1].cTab(), tabv[2].cTab(), vv.cTab() );
	//}

	//return 0.25 * aret;
	return aret;



	//static GVect v0;

	//SphereCenterMetric( v0, met);
	//
	//MGFloat	R2 = 0.0;

	//for ( MGSize k=0; k<SIZE; ++k)
	//{
	//	GVect vtmp = v0 - *(mtab[0]);
	//	R2 += met.Distance( vtmp);
	//}
	//R2 *= 1.0 / (MGFloat)( SIZE );

	//GVect vtmp = v0 - vct;
	//MGFloat	r2 = met.Distance( vtmp);
	//
	//MGFloat	al = ::sqrt( r2/R2);

	//return 1.0 - al;


	//////////////////////////////
	//// old approach

	//static GVect v0;

	//SphereCenterMetric( v0, metric);
	//
	//GVect vtmp = v0 - *(mtab[0]);
	//MGFloat	R2 = metric.Distance( vtmp);

	//vtmp = v0 - vct;
	//MGFloat	r2 = metric.Distance( vtmp);
	//
	//MGFloat	al = ::sqrt( r2/R2);
	////MGFloat	al = r2/R2;

	//MGSize n = 1;

	//for ( MGSize i=0; i<SIZE; i++)
	//{
	//	// TODO :: check if metric is OK
	//	{
	//		SphereCenterMetric( v0, tabmetric[i] );

	//		vtmp = v0 - *(mtab[i]);
	//		R2 = tabmetric[i].Distance( vtmp);

	//		vtmp = v0 - vct;
	//		r2 = tabmetric[i].Distance( vtmp);

	//		al += ::sqrt( r2/R2);
	//		//al += r2/R2;
	//		n++;
	//	}
	//}

	//return 1.0 - al/(MGFloat)n;
}


template <>
MGFloat Simplex<DIM_2D>::QMeasureShape() const
{
	MGFloat	vol = Volume();

	MGFloat arms = 0.0;
	for ( MGSize ifac=0; ifac<SIZE; ++ifac)
	{
		MGFloat a = GetFace( ifac).Area();
		arms += a*a;
	}
	arms = sqrt( arms / 3.0 );

	return vol / ( arms*arms);
}

template <>
MGFloat Simplex<DIM_2D>::QMeasureInterp() const
{
	MGFloat	vol = Volume();

	MGFloat multA = 1.0;
	for ( MGSize ifac=0; ifac<SIZE; ++ifac)
		multA *= GetFace( ifac).Area();

	return vol / pow( multA, 2./3. );
}



//////////////////////////////////////////////////////////////////////
// DIM_3D

template <>
bool Simplex<DIM_3D>::IsInside( const GVect& vct, const MGFloat& zero) const
{
	const MGFloat vol0 = orient3d( mtab[1]->cTab(), mtab[2]->cTab(), mtab[3]->cTab(), vct.cTab() );
	const MGFloat vol1 = orient3d( mtab[2]->cTab(), mtab[0]->cTab(), mtab[3]->cTab(), vct.cTab() );
	const MGFloat vol2 = orient3d( mtab[0]->cTab(), mtab[1]->cTab(), mtab[3]->cTab(), vct.cTab() );
	const MGFloat vol3 = orient3d( mtab[0]->cTab(), mtab[2]->cTab(), mtab[1]->cTab(), vct.cTab() );
	
	if ( vol0 >= zero && vol1 >= zero && vol2 >= zero && vol3 >= zero )
		return true;
	
	return false;
}

template <>
void Simplex<DIM_3D>::SphereCenterMetric( GVect& vct, const GMetric& metric) const
{
	GMetric::SMtx mtx(metric);

	GMetric::SMtx mtxA;
	GMetric::SVect vecB;
	GMetric::SVect vec;

	for ( MGSize j=0; j<DIM_3D; ++j)
	{
		vecB[j] = 0.5 * ( metric.Distance( *mtab[j]) - metric.Distance( *mtab[j+1]) );


		GMetric::SVect vecG;

		for ( MGSize i=0; i<DIM_3D; ++i)
			vecG[i] = mtab[j]->cX(i) - mtab[j+1]->cX(i);

		vec = mtx * vecG;
		for ( MGSize i=0; i<DIM_3D; ++i)
			mtxA(j,i) = vec[i];
	}

	mtxA.Invert();

	GMetric::SMtx mtxX = mtxA * vecB;

	vct = Vect3D( mtxX(0,0), mtxX(1,0), mtxX(2,0) );
}

template <>
MGFloat Simplex<DIM_3D>::IsInsideSphereMetric( const GVect& vct, const GMetric& metric, const GMetric tabmetric[SIZE] ) const
{
	//return insphere( mtab[0]->cTab(), mtab[1]->cTab(), mtab[3]->cTab(), mtab[2]->cTab(), vct.cTab() );

	GVect v0 = vct;
	GVect vv;
	static GVect tabv[SIZE];

	GMetric met = metric;

	for ( MGSize k=0; k<SIZE; ++k)
	{
		met += tabmetric[k];
		v0 += *mtab[k];
	}

	met = ( 1.0 / static_cast<MGFloat>( SIZE + 1) ) * met;
	v0 /= static_cast<MGFloat>( SIZE + 1);

	GMetric::SMtx mtx;

	met.EigenDecompose( mtx);
	met.EigenMultDecomp( tabv[0], *mtab[0] - v0, mtx );
	met.EigenMultDecomp( tabv[1], *mtab[1] - v0, mtx );
	met.EigenMultDecomp( tabv[2], *mtab[2] - v0, mtx );
	met.EigenMultDecomp( tabv[3], *mtab[3] - v0, mtx );
	met.EigenMultDecomp( vv, vct - v0, mtx );

	MGFloat aret = insphere( tabv[0].cTab(), tabv[1].cTab(), tabv[3].cTab(), tabv[2].cTab(), vv.cTab() );

	return aret;


/*
	Vect3D	tabv[SIZE+1];
	Vect3D	vc, vctm;

	vc = 0.25*(*mtab[0] + *mtab[1] + *mtab[2] + *mtab[3]); 

	SMatrix<MGFloat,3>	mtxA, mtxAT, mtxM, mtxL, mtxLI, mtxG;
	SVector<MGFloat,3>	vecD, vec;
	GMetric	 tr;

	//metric.Write();

	mtxA.Resize(3,3);
	mtxA = SMatrix<MGFloat,3>( metric);
	mtxA(0,1) = mtxA(0,2) = mtxA(1,2) = 0;
	//mtxA.Write();

	mtxAT = mtxA;
	mtxAT.Transp();

	mtxM = mtxA * mtxAT;
	//mtxM.Write();

	tr = GMetric( mtxM);
	tr.Decompose();
	//tr.Write();

	mtxM.Decompose( vecD, mtxL);

	MGFloat	dav = max( vecD(0), max( vecD(1), vecD(2) ));

	//mtxL.Write();
	//vecD.Write();

	mtxLI = mtxL;
	mtxLI.Transp();

	mtxG = mtxLI;
	mtxA.Init(0);
	mtxA(0,0) = ::sqrt( vecD(0)/dav );
	mtxA(1,1) = ::sqrt( vecD(1)/dav );
	mtxA(2,2) = ::sqrt( vecD(2)/dav );

	mtxG = mtxA * mtxG;

	mtxLI = mtxG;

	//mtxL.Write();
	//mtxLI.Write();
	//(mtxL*mtxLI).Write();

	//mtxLI.Init(0);
	//mtxLI(0,0) = cos(0.1);
	//mtxLI(0,2) = sin(0.1);
	//mtxLI(2,0) = -sin(0.1);
	//mtxLI(2,2) = cos(0.1);
	//mtxLI(1,1) = 1;

	vec.Resize(3);
	for ( MGSize i=0; i<SIZE; ++i)
	{
		vec(0) = (*mtab[i]-vc).cX();
		vec(1) = (*mtab[i]-vc).cY();
		vec(2) = (*mtab[i]-vc).cZ();
		//vec.Write();

		//printf( "dbef = %lg\n", Dot( vec, vec) );
		vec = mtxLI * vec;
		//printf( "daft = %lg\n", Dot( vec, vec) );

		tabv[i].rX() = vec(0);
		tabv[i].rY() = vec(1);
		tabv[i].rZ() = vec(2);

		//vec.Write();
	}

	vec(0) = (vct-vc).cX();
	vec(1) = (vct-vc).cY();
	vec(2) = (vct-vc).cZ();
	//vec.Write();

	vec = mtxLI * vec;

	vctm.rX() = vec(0);
	vctm.rY() = vec(1);
	vctm.rZ() = vec(2);

	//vec.Write();


	MGFloat	da, db;

	da = insphere( tabv[0].cTab(), tabv[1].cTab(), tabv[3].cTab(), tabv[2].cTab(), vctm.cTab() );
	db = insphere( mtab[0]->cTab(), mtab[1]->cTab(), mtab[3]->cTab(), mtab[2]->cTab(), vct.cTab() );

	//printf( "da = %lg\n", da);
	//printf( "db = %lg\n\n", db);

	//if ( da * db < 0)
	//	THROW_INTERNAL( "CRASH");

	//_getch();
	

	for ( MGSize i=0; i<SIZE; ++i)
		metric.MultDecomp( tabv[i], *mtab[i]-vct);

	metric.MultDecomp( vctm, vct-vct);

	return max( da, db);

	return inspherefast( tabv[0].cTab(), tabv[1].cTab(), tabv[3].cTab(), tabv[2].cTab(), vctm.cTab() );
	//return inspherefast( tabv[0].cTab(), tabv[1].cTab(), tabv[3].cTab(), tabv[2].cTab(), vctm.cTab() );

*/
}



template <>
MGFloat Simplex<DIM_3D>::QMeasureShape() const
{
	MGFloat	vol = Volume();

	MGFloat arms = 0.0;
	for ( MGSize ifac=0; ifac<SIZE; ++ifac)
	{
		MGFloat a = GetFace( ifac).Area();
		arms += a*a;
	}
	arms = sqrt( arms * 0.25 );

	return vol / sqrt( arms*arms*arms);
}

template <>
MGFloat Simplex<DIM_3D>::QMeasureInterp() const
{
	MGFloat	vol = Volume();

	MGFloat tabA[SIZE];
	MGFloat sumA = 0.0;
	for ( MGSize ifac=0; ifac<SIZE; ++ifac)
	{
		tabA[ifac] = GetFace( ifac).Area();
		sumA += tabA[ifac];
	}

	MGFloat denom =0.0;
	for ( MGSize i=0; i<SIZE; ++i)
		for ( MGSize j=i+1; j<SIZE; ++j)
		{
			MGFloat l = (*mtab[j] - *mtab[i]).module();
			denom += tabA[i] * tabA[j] * l;
		}

	return vol * pow( sumA / denom, 0.75 );
}




//////////////////////////////////////////////////////////////////////
// misc functions

bool FindEdgeEdgeCross( Vect3D& vcross, const Vect3D& ve1b, const Vect3D& ve1e, const Vect3D& ve2b, const Vect3D& ve2e)
{
	Vect3D vb21 = ve2b - ve1b;
	Vect3D vt1 = (ve1e - ve1b).versor();
	Vect3D vt2 = (ve2e - ve2b).versor();

	Vect3D vn1 = (vt1 % vb21).versor();
	Vect3D vn2 = (vt2 % vb21).versor();

	if ( (vn1 % vn2).module() > ZERO )
	{
		cout <<  "FindEdgeCross :: the edges are not planar !!!" << endl;
		return false;
	}

	Vect3D vn = 0.5 * (vn1 + vn2);

	Vect3D w1 = (vn % vt1).versor();
	MGFloat u2 = - ( vb21 * w1 ) / ( vt2 * w1 );
	Vect3D vcross2 = u2 * vt2 + ve2b;

	Vect3D w2 = (vt2 % vn).versor();
	MGFloat u1 = ( vb21 * w2 ) / ( vt1 * w2 );
	Vect3D vcross1 = u1 * vt1 + ve1b;

	MGFloat error = (vcross2 - vcross1).module();
	vcross = 0.5 * ( vcross1 + vcross2);

	MGFloat s1 = (vcross - ve1b) * (ve1e - vcross);
	MGFloat s2 = (vcross - ve2b) * (ve2e - vcross);

	if ( s1 < 0 || s2 < 0)
	{
		cout <<  "FindEdgeCross :: crossing point is outside the edges !!!" << endl;
		return false;
	}

	return true;
}


bool FindEdgeFaceCross( Vect3D& vcross, const Vect3D& ve1, const Vect3D& ve2, const Vect3D& vf1, const Vect3D& vf2, const Vect3D& vf3)
{
	SimplexFace<DIM_3D> face( vf1, vf2, vf3);

	if ( ! face.IsCrossed( ve1, ve2) )
		return false;

	Vect3D vn = face.Vn().versor();
	Vect3D vt = (ve2 - ve1).versor();
	Vect3D vb = vf1 - ve1;
	
	MGFloat u = (vb * vn) / (vt * vn);
	vcross = u*vt + ve1;

	return true;
}

MGFloat EdgeDistance( const Vect3D& ve1, const Vect3D& ve2, const Vect3D& vct)
{
	Vect3D va1 = (ve2 - ve1).versor();
	Vect3D vb1 = vct - ve1;
	MGFloat t1 = va1 * vb1;
	Vect3D vn1 = vb1 - va1 * t1;

	Vect3D va2 = (ve1 - ve2).versor();
	Vect3D vb2 = vct - ve2;
	MGFloat t2 = va2 * vb2;
	Vect3D vn2 = vb2 - va2 * t2;

	if ( t1 < 0.0 )
		return vb1.module();

	if ( t2 < 0.0 )
		return vb2.module();


	return 0.5 * ( vn1.module() + vn2.module() );
}




} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

