#ifndef __GEOM_H__
#define __GEOM_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/vect.h"

#include "simplex.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {




const MGFloat CELL_BOUNDARY	= 0.0005;


inline MGFloat VolumeTRI( const Vect2D& v0, const Vect2D& v1, const Vect2D& v2)
{
	return Simplex<DIM_2D>( v0, v1, v2).Volume();
	//Vect2D dv1 = v1-v0;
	//Vect2D dv2 = v2-v0;

	//return 0.5*( dv1.cX()*dv2.cY() - dv2.cX()*dv1.cY() );
}

inline MGFloat VolumeQUAD( const Vect2D& v0, const Vect2D& v1, const Vect2D& v2, const Vect2D& v3)
{
	return VolumeTRI( v0, v1, v2) + VolumeTRI( v2, v3, v0);
}




inline MGFloat AreaTRI( const Vect3D& v0, const Vect3D& v1, const Vect3D& v2)
{
	Vect3D	dv1, dv2;

	dv1 = v1-v0;
	dv2 = v2-v0;

	return 0.5*( dv1%dv2 ).module();
}

inline MGFloat AreaQUAD( const Vect3D& v0, const Vect3D& v1, const Vect3D& v2, const Vect3D& v3)
{
	return AreaTRI( v0, v1, v2) + AreaTRI( v2, v3, v0);
}


inline MGFloat VolumeTET( const Vect3D& v0, const Vect3D& v1, const Vect3D& v2, const Vect3D& v3)
{
	return Simplex<DIM_3D>( v0, v1, v2, v3).Volume();
	//return ((v1-v0) % (v2-v0)) * (v3-v0) / 6.0;
}

inline MGFloat VolumePIRAM( const Vect3D& v0, const Vect3D& v1, const Vect3D& v2, 
						    const Vect3D& v3, const Vect3D& v4)
{
	Vect3D	vc = 0.25*(v0 + v1 + v2 + v3);
	return VolumeTET(v0,v1,vc,v4) + VolumeTET(v1,v2,vc,v4) + VolumeTET(v2,v3,vc,v4) + VolumeTET(v3,v0,vc,v4);
}

inline MGFloat VolumePRISM( const Vect3D& v0, const Vect3D& v1, const Vect3D& v2, 
						    const Vect3D& v3, const Vect3D& v4, const Vect3D& v5)
{
	Vect3D	vc = (v0 + v1 + v2 + v3 + v4 + v5) / 6.0;
	return	VolumePIRAM( v0, v3, v4, v1, vc) +
			VolumePIRAM( v1, v4, v5, v2, vc) +
			VolumePIRAM( v2, v5, v3, v0, vc) +
			VolumeTET( v0, v1, v2, vc) +
			VolumeTET( v5, v4, v3, vc);

}

inline MGFloat VolumeHEX( const Vect3D& v0, const Vect3D& v1, const Vect3D& v2, const Vect3D& v3, 
						  const Vect3D& v4, const Vect3D& v5, const Vect3D& v6, const Vect3D& v7)
{
	Vect3D	vc = 0.125 * (v0 + v1 + v2 + v3 + v4 + v5 + v6 + v7);
	return	VolumePIRAM( v0, v4, v5, v1, vc) +
			VolumePIRAM( v1, v5, v6, v2, vc) +
			VolumePIRAM( v2, v6, v7, v3, vc) +
			VolumePIRAM( v3, v7, v4, v0, vc) +
			VolumePIRAM( v0, v1, v2, v3, vc) +
			VolumePIRAM( v7, v6, v5, v4, vc);
}


inline Vect2D VnSeg( const Vect2D& v0, const Vect2D& v1)
{
	Vect2D	vtmp = v1 - v0;
	return Vect2D( -vtmp.cY(), vtmp.cX() );
}

inline Vect3D VnFac( const Vect3D& v0, const Vect3D& v1, const Vect3D& v2)
{
	Vect3D	vt1 = v1 - v0, vt2 = v2 - v0;
	return 0.5 * vt1%vt2;
}


inline bool IsInsideTri( const Vect2D& vct, const Vect2D& v0, const Vect2D& v1, 
						const Vect2D& v2, const MGFloat& zero = CELL_BOUNDARY )
{
	Vect2D	vn, vdc;
	MGFloat	d;

	vn = VnSeg( v0, v1).versor();
	vdc = vct - 0.5*(v0+v1);
	d = vdc * vn;

	if ( d < -zero)
		return false;


	vn = VnSeg( v1, v2).versor();
	vdc = vct - 0.5*(v1+v2);
	d = vdc * vn;

	if ( d < -zero)
		return false;


	vn = VnSeg( v2, v0).versor();
	vdc = vct - 0.5*(v2+v0);
	d = vdc * vn;

	if ( d < -zero)
		return false;

	return true;
}

inline bool IsInsideTet( const Vect3D& vct, const Vect3D& v0, const Vect3D& v1, 
						const Vect3D& v2, const Vect3D& v3, const MGFloat& zero = CELL_BOUNDARY )
{
	Vect3D	vn, vdc;
	MGFloat	d;

	vn = VnFac( v0, v1, v2).versor();
	vdc = vct - (v0+v1+v2) /3.0;
	d = vdc * vn;

	if ( d < -zero)
		return false;

	vn = VnFac( v0, v3, v1).versor();
	vdc = vct - (v0+v3+v1) /3.0;
	d = vdc * vn;

	if ( d < -zero)
		return false;

	vn = VnFac( v1, v3, v2).versor();
	vdc = vct - (v1+v3+v2) /3.0;
	d = vdc * vn;

	if ( d < -zero)
		return false;

	vn = VnFac( v2, v3, v0).versor();
	vdc = vct - (v2+v3+v0) /3.0;
	d = vdc * vn;

	if ( d < -zero)
		return false;


	return true;
}





template <class MATRIX, class VECTOR>
inline void RotMtx( MATRIX& mtx, const VECTOR& v)
{
	MGFloat	q = 1.0;
	MGFloat kx = q<ZERO ? 1.0 : v(0);
	MGFloat ky = q<ZERO ? 0.0 : v(1); 
	MGFloat kz = q<ZERO ? 0.0 : v(2); 

	mtx.Resize( 3, 3);
	mtx.Init( 0);

	mtx(0,0) = kx;
	mtx(0,1) = ky;
	mtx(0,2) = kz;

	mtx(1,0) = -ky;
	mtx(1,1) = kx + (::fabs(kx+1)>ZERO ?  kz*kz/(1+kx) : 0);
	mtx(2,1) = mtx(1,2) = (::fabs(kx+1)>ZERO ? -ky*kz/(1+kx) : 0);

	mtx(2,0) = -kz;
	//mtx(2,1) = (::fabs(kx+1)>ZERO ? -ky*kz/(1+kx) : 0);
	mtx(2,2) = kx + (::fabs(kx+1)>ZERO ?  ky*ky/(1+kx) : 0);
}


inline MGFloat InRadiusTRI( const Vect2D& v0, const Vect2D& v1, const Vect2D& v2)
{
	return Simplex<DIM_2D>( v0, v1, v2).InscribedSphereRadius();
}

inline MGFloat InRadiusTET( const Vect3D& v0, const Vect3D& v1, const Vect3D& v2, const Vect3D& v3)
{
	return Simplex<DIM_3D>( v0, v1, v2, v3).InscribedSphereRadius();
}




} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Geom = SpaceGeomTools;


#endif // __GEOM_H__
