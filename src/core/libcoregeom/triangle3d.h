#ifndef __TRIANGLE3D_H__
#define __TRIANGLE3D_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/vect.h"
#include "predicates.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {




//////////////////////////////////////////////////////////////////////
//	class Triangle3D
//////////////////////////////////////////////////////////////////////
class Triangle3D
{
public:
	enum { SIZE = 3};


	// the Triangle3D class stores pointers to EXISTING points !
	// do not destroy them if you want to use the class functions
	Triangle3D( const Vect3D& v1, const Vect3D& v2, const Vect3D& v3);

	// returns volume with sign
	MGFloat		Area() const;
	
	Vect3D		Center() const;


	Vect3D		Vn() const;
	
	// returns volume of a tetrahedron built with the Triangle3D and point vct
	MGFloat		TetVolume( const Vect3D& vct, const MGSize& i) const;


	bool		IsAbove( const Vect3D& vct) const;
	
	bool		IsCrossed( const Vect3D& v1, const Vect3D& v2) const;


	static MGSize&	cFaceConn( const MGSize& ifc, const MGSize& in)	
													{ return mtabFaceConn[ifc][in];}


private:
	static MGSize	mtabFaceConn[DIM_2D+1][DIM_2D];

	const Vect3D*	mtab[SIZE];
};
//////////////////////////////////////////////////////////////////////

inline Triangle3D::Triangle3D( const Vect3D& v1, const Vect3D& v2, const Vect3D& v3)
{
	mtab[0] = &v1;
	mtab[1] = &v2;
	mtab[2] = &v3;
}

inline MGFloat Triangle3D::Area() const
{
// 	Vect3D	v20 = *mtab[2] - *mtab[0];
// 	Vect3D	v10 = *mtab[1] - *mtab[0];
// 
// 	return 0.5 * (v20 % v10).module();

	Vect3D	varea;
	Vect2D	tabv[SIZE];

	tabv[0] = Vect2D( mtab[0]->cY(), mtab[0]->cZ() );
	tabv[1] = Vect2D( mtab[1]->cY(), mtab[1]->cZ() );
	tabv[2] = Vect2D( mtab[2]->cY(), mtab[2]->cZ() );
	varea.rX() = orient2d( tabv[0].cTab(), tabv[1].cTab(), tabv[2].cTab() );

	tabv[0] = Vect2D( mtab[0]->cZ(), mtab[0]->cX() );
	tabv[1] = Vect2D( mtab[1]->cZ(), mtab[1]->cX() );
	tabv[2] = Vect2D( mtab[2]->cZ(), mtab[2]->cX() );
	varea.rY() = orient2d( tabv[0].cTab(), tabv[1].cTab(), tabv[2].cTab() );

	tabv[0] = Vect2D( mtab[0]->cX(), mtab[0]->cY() );
	tabv[1] = Vect2D( mtab[1]->cX(), mtab[1]->cY() );
	tabv[2] = Vect2D( mtab[2]->cX(), mtab[2]->cY() );
	varea.rZ() = orient2d( tabv[0].cTab(), tabv[1].cTab(), tabv[2].cTab() );

	return 0.5 * varea.module();

}

inline Vect3D Triangle3D::Center() const
{
	return ( *mtab[0] + *mtab[1] + *mtab[2] ) / 3.0;
}


inline Vect3D Triangle3D::Vn() const
{
	return ( *mtab[2] - *mtab[0]) % ( *mtab[1] - *mtab[0]);
}



inline bool Triangle3D::IsAbove( const Vect3D& vct) const
{
	if ( orient3d( mtab[0]->cTab(), mtab[1]->cTab(), mtab[2]->cTab(), vct.cTab() ) < 0 )
		return true;

	return false;
}



} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Geom = SpaceGeomTools;



#endif // __TRIANGLE3D_H__
