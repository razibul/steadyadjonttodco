#ifndef __PREDICATES_H__
#define __PREDICATES_H__

//#define CPU86



#ifdef SINGLE
  #define REAL1 float
#else
  #define REAL1 double
#endif 	// not defined SINGLE

// initialization
REAL1 exactinit();

// 2D stuff
REAL1 orient2d(const REAL1 *pa, const REAL1 *pb, const REAL1 *pc);
REAL1 orient2dfast(const REAL1 *pa, const REAL1 *pb, const REAL1 *pc);

REAL1 incircle(const REAL1 *pa, const REAL1 *pb, const REAL1 *pc, const REAL1 *pd);
REAL1 incirclefast(const REAL1 *pa, const REAL1 *pb, const REAL1 *pc, const REAL1 *pd);

// 3D stuff
REAL1 orient3d(const REAL1 *pa, const REAL1 *pb, const REAL1 *pc, const REAL1 *pd);
REAL1 orient3dfast(const REAL1 *pa, const REAL1 *pb, const REAL1 *pc, const REAL1 *pd);

REAL1 insphere(const REAL1 *pa, const REAL1 *pb, const REAL1 *pc, const REAL1 *pd, const REAL1 *pe);
REAL1 inspherefast(const REAL1 *pa, const REAL1 *pb, const REAL1 *pc, const REAL1 *pd, const REAL1 *pe);


#endif // __PREDICATES_H__
