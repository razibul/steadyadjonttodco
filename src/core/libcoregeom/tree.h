#ifndef __TREE_H__
#define __TREE_H__

#include "treeleaf.h"



//////////////////////////////////////////////////////////////////////
//  class Tree
//////////////////////////////////////////////////////////////////////
template <class DATA, Dimension DIM> 
class Tree : public Leaf<DATA,DIM>
{
public:
	typedef	Leaf<DATA,DIM>					TLeaf;
	typedef typename Leaf<DATA,DIM>::PAIR	TPair;


	// ns - max number of points stored at single leaf
	// 30 works quite ok for ~1Mpnt 3D problems
	explicit Tree( const MGSize& ns = 30);

	~Tree();
	
	const Box<DIM>&		cBox() const		{ return mBox;}
	
	
	
	void	InitBox( const Box<DIM>& box);
	void	Insert( const Vect<DIM>& pos, const DATA& dat);
	void	Erase( const Vect<DIM>& pos, const DATA& dat);

	// true -  the res is valid
	// false - search failed (should never happend if the pos is inside the box)
	bool	GetClosestItem( TPair& res, const Vect<DIM>& pos) const;

	// destroys all the children and data
	void	RemoveAll()		{ if ( mpRoot) Remove( mpRoot); }
	
	void	ExportTEC( char sname[]);


protected:

	TLeaf*	FindLeaf( const Vect<DIM>& pos);

	// needs some modification in order to properly remove data from the tree
	void	FindAndRemove( const Vect<DIM>& pos, const DATA& dat);

	void	Remove( TLeaf* plf);

	void	RangeSearch( const Box<DIM>& rec, vector<TLeaf*>& tab) const;
	bool	CheckRange( const Vect<DIM>& vct)	{ return mBox.IsInside( vct);}
	bool	ClosestItemFromList( TPair& res, const Vect<DIM>& pos, vector<TLeaf*>& tab) const;

	
private:
	TLeaf		*mpRoot;
	Box<DIM>	mBox;
	
};
//////////////////////////////////////////////////////////////////////



template <class DATA, Dimension DIM> 
inline Tree<DATA,DIM>::Tree( const MGSize& ns)
{
	TLeaf::rSize() = ns;
	mpRoot = NULL;
	mBox = Box<DIM>( Vect<DIM>(), Vect<DIM>() );
}

template <class DATA, Dimension DIM> 
inline Tree<DATA,DIM>::~Tree()
{
	if ( mpRoot)
		delete mpRoot;
}


template <class DATA, Dimension DIM> 
void Tree<DATA,DIM>::InitBox( const Box<DIM>& box)	
{
	Vect<DIM>	dv;
	MGFloat	h;
	
	mBox = box; 

	dv = mBox.cVMax() - mBox.cVMin();

	h = 0;
	for ( MGInt i=0; i<DIM; ++i)
		if ( h < fabs( dv.cX(i) ) )
			h = fabs( dv.cX(i) );
		
	for ( MGInt i=0; i<DIM; ++i)
		if ( fabs( dv.cX(i) ) < h )
		{
			mBox.rVMax().rX(i) += 0.5*(h-dv.cX(i));
			mBox.rVMin().rX(i) -= 0.5*(h-dv.cX(i));
		}

	TLeaf::rCenter() = mBox.cVMin(); // it is used for boundbox calc in mRoot leaf

}


template <class DATA, Dimension DIM> 
void Tree<DATA,DIM>::Insert( const Vect<DIM>& pos, const DATA& dat)
{
	if ( mpRoot == NULL)
		mpRoot = new TLeaf( mBox.Center(), this );
	
	mpRoot->Insert( pos, dat, mBox );
}



template <class DATA, Dimension DIM> 
void Tree<DATA,DIM>::RangeSearch( const Box<DIM>& rec, vector<TLeaf*>& tab) const
{
	if ( mpRoot)
		mpRoot->RangeSearch( rec, tab);
}


template <class DATA, Dimension DIM> 
bool Tree<DATA,DIM>::ClosestItemFromList( TPair& res, const Vect<DIM>& pos, vector<TLeaf*>& tab) const
{
	typename vector<TLeaf*>::iterator	itr;
	TPair				data;
	TLeaf*				plf = NULL;
	Vect<DIM>	vct;

	MGFloat		dst, dmin;
	MGSize		ind;
	bool		bFirst = true;
	
	for ( itr= tab.begin(); itr != tab.end(); ++itr)
	{
		if ( bFirst)
		{
			ind = (*itr)->IndClosestData( pos);
			if ( ind < 0 )
				THROW_INTERNAL( "problem inside Tree<DATA,DIM>::ClosestItemFromList(...)");

			vct = (*itr)->Pos(ind);
			dmin = (pos - vct).module();

			res.first = (*itr)->Pos(ind);
			res.second = (*itr)->Data(ind);
			plf = *itr;
			bFirst = false;
		}
		else
		{
			ind = (*itr)->IndClosestData( pos);
			if ( ind < 0 )
				THROW_INTERNAL( "problem inside Tree<DATA,DIM>::ClosestItemFromList(...)");

			dst = (pos - (*itr)->Pos(ind) ).module();
			if ( dst < dmin)
			{
				res.first = (*itr)->Pos(ind);
				res.second = (*itr)->Data(ind);
				plf = *itr;
				dmin = dst;
			}
		}
	}
	return true;
}


template <class DATA, Dimension DIM> 
bool Tree<DATA,DIM>::GetClosestItem( TPair& res, const Vect<DIM>& pos) const
{
	static vector<TLeaf*>				tab;
	typename vector<TLeaf*>::iterator	itr;
	
	bool bret;
	TPair				data;
	TLeaf*				plf;
	Box<DIM>			rec;
	Vect<DIM>	vct;
	MGFloat				dst;

	MGFloat	dmin = (mBox.cVMax() - mBox.cVMin()).module();

	tab.reserve(80);
	
	if ( mpRoot && mBox.IsInside( pos) )
	{
		plf = mpRoot->Traverse( pos);
		if ( plf->HaveData())
		{
			MGInt idat = plf->IndClosestData( pos);

			vct = pos - plf->Pos(idat);
			dst = vct.module();
		}
		else
		{
			if ( plf == mpRoot)
				rec = mBox;
			else
				rec = plf->FindBoundBox();
			
			plf->RangeSearch( rec, tab);
			bret = ClosestItemFromList( data, pos, tab);
			
			tab.clear();

			if ( ! bret )
				THROW_INTERNAL("Problem ...");


			vct = pos - data.first;
			dst = vct.module();
		}
		
		for ( MGInt i=0; i<DIM; ++i)
		{
			rec.rVMin().rX(i) = pos.cX(i) - dst - ZERO;
			rec.rVMax().rX(i) = pos.cX(i) + dst + ZERO;
		}
		
		RangeSearch( rec, tab);
		bret = ClosestItemFromList( res, pos, tab);

		//static MGFloat rs = 0;
		//static MGFloat rw = 0;
		//static MGFloat rmax = 0;
		//if ( tab.size() > rmax)
		//	rmax = tab.size();
		//rs += tab.size();
		//rw += 1;
		//printf( "avr tree tab size = %lf  %lf\n", rs/ rw, rmax);

		tab.resize(0);
		
		return true;
	}
	
	return false;
}





template <class DATA, Dimension DIM> 
Leaf<DATA,DIM>* Tree<DATA,DIM>::FindLeaf( const Vect<DIM>& pos)
{
	if ( mpRoot)
		return mpRoot->Traverse( pos);
		
	return NULL;
}




template <class DATA, Dimension DIM> 
void Tree<DATA,DIM>::Erase( const Vect<DIM>& pos, const DATA& dat)
{
	TLeaf	*plf = NULL;

	plf = FindLeaf( pos);

	plf->Erase( pos, dat);

	if ( ! plf->HaveData() )
		Remove( plf);
}


template <class DATA, Dimension DIM> 
void Tree<DATA,DIM>::FindAndRemove( const Vect<DIM>& pos, const DATA& dat)
{
	TLeaf	*plf = NULL;

	plf = FindLeaf( pos);

	if ( plf->Data() != dat)
		THROW_INTERNAL( "Tree::FindAndRemove : not consistent data");

	Remove( plf);
}


template <class DATA, Dimension DIM> 
void Tree<DATA,DIM>::Remove( TLeaf* plf)
{
	TLeaf* ppar;

	ASSERT( plf != NULL);

	if ( plf == mpRoot)
	{
		mpRoot = NULL;
	}
	else
	{
		ppar = plf->DetachFromParent();

		if ( ppar != NULL)
			if ( ppar->IsEmpty() )
				Remove( ppar);
	}

	delete plf;
}



template <class DATA, Dimension DIM> 
void Tree<DATA,DIM>::ExportTEC( char sname[])
{
	FILE *ftmp = fopen( sname, "wt");

	if ( mpRoot)
		mpRoot->ExportTEC( ftmp);
		
	fclose( ftmp);
}

#endif // __TREE_H__
