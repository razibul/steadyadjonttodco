#ifndef __METIRC_H__
#define __METIRC_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/vect.h"
#include "libcorecommon/smatrix.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {




//////////////////////////////////////////////////////////////////////
//	class Metric
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class ELEM_TYPE=MGFloat> 
class Metric
{
public:
	typedef SMatrix<DIM,ELEM_TYPE>	SMtx;
	typedef SVector<DIM,ELEM_TYPE>	SVect;

	enum { SIZE = DIM*(DIM+1)/2};


	Metric()	{}
	Metric( const SMatrix<DIM,ELEM_TYPE>& mtx);

    ELEM_TYPE&			operator[]( const MGSize& i)						{ return *(mtab+i); }
    const ELEM_TYPE&	operator[]( const MGSize& i) const					{ return *(mtab+i); }

	ELEM_TYPE&			operator()( const MGSize& i,const MGSize& j);
    const ELEM_TYPE&	operator()( const MGSize& i,const MGSize& j) const;


	Metric<DIM,ELEM_TYPE>&	operator +=( const Metric<DIM,ELEM_TYPE>&);
	Metric<DIM,ELEM_TYPE>&	operator -=( const Metric<DIM,ELEM_TYPE>&);
	Metric<DIM,ELEM_TYPE>&	operator *=( const Metric<DIM,ELEM_TYPE>&);
	Metric<DIM,ELEM_TYPE>&	operator +=( const ELEM_TYPE& d);
	Metric<DIM,ELEM_TYPE>&	operator -=( const ELEM_TYPE& d);
	Metric<DIM,ELEM_TYPE>&	operator *=( const ELEM_TYPE& d);
	Metric<DIM,ELEM_TYPE>&	operator /=( const ELEM_TYPE& d);

	void	Reset();

	void	InitIso( const MGFloat& h);
	void	LimitMetric( const ELEM_TYPE& hmin, const ELEM_TYPE& hmax, const ELEM_TYPE& armax);

	MGFloat	Distance( const Vect<DIM>& vct) const;


	// invert and store the metric
	void	Invert();

	// decompose metric into L^T * L with Cholesky and stores the L (lower triangular matrix)
	void	Decompose();
	void	MultDecomp( Vect<DIM>& vres, const Vect<DIM>& varg) const;

	// decompose by solving eigen problem 
	// vdiag - vector of eigenvalues
	// mtxeigen - matrix of eigenvectors
	void	EigenDecompose( SVect& vdiag, SMtx& mtxeigen) const;

	// decompose by solving eigen problem 
	// mtxeigen = \root( \Lambda) * L^{-1}
	// M = mtxeigen^{-1} * mtxeigen
	void	EigenDecompose( SMtx& mtxeigen) const;
	void	EigenMultDecomp( Vect<DIM>& vres, const Vect<DIM>& varg, const SMtx& mtx) const;

	void	EigenDecompose();
	void	EigenMultDecomp( Vect<DIM>& vres, const Vect<DIM>& varg) const;


	void	Write( FILE *f=stdout) const;

	operator	SMatrix<DIM,ELEM_TYPE>() const;

private:
	// elemets are stored as the row-wise lower triangular matrix; 
	// for 3d:
	//   0 1 3
	//   1 2 4
	//   3 4 5
	ELEM_TYPE	mtab[SIZE]; 
};
//////////////////////////////////////////////////////////////////////
typedef Metric<DIM_1D> Metric1D;
typedef Metric<DIM_2D> Metric2D;
typedef Metric<DIM_3D> Metric3D;



template <Dimension DIM, class ELEM_TYPE> 
Metric<DIM,ELEM_TYPE>::Metric( const SMatrix<DIM,ELEM_TYPE>& mtx)
{
	// TODO :: check if mtx is symmetric
	MGSize	offset = 0;
	for ( MGSize i=0; i<DIM; ++i)
	{
		offset += i;

		for ( MGSize j=0; j<=i; ++j)
			*(mtab+offset+j) = 0.5*( mtx(i,j) + mtx(j,i) );
	}
}


template <Dimension DIM, class ELEM_TYPE> 
Metric<DIM,ELEM_TYPE>::operator SMatrix<DIM,ELEM_TYPE>() const
{
	SMatrix<DIM,ELEM_TYPE>	mtx(DIM,DIM,0.0);
	MGSize	offset = 0;
	for ( MGSize i=0; i<DIM; ++i)
	{
		offset += i;

		for ( MGSize j=0; j<=i; ++j)
			mtx(i,j) = mtx(j,i) = *(mtab+offset+j);
	}

	return mtx;
}


template <Dimension DIM, class ELEM_TYPE> 
ELEM_TYPE& Metric<DIM,ELEM_TYPE>::operator()( const MGSize& i,const MGSize& j)
{
	MGSize in = i>j?i:j;
	MGSize jn = i>j?j:i;
	
	MGSize	ind = 0;
	for ( MGSize k=1; k<=in; ++k)
		ind += k;

	return *(mtab+ind+jn);
}


template <Dimension DIM, class ELEM_TYPE> 
inline const ELEM_TYPE& Metric<DIM,ELEM_TYPE>::operator()( const MGSize& i,const MGSize& j) const
{
	MGSize in = i>j?i:j;
	MGSize jn = i>j?j:i;
	
	MGSize	ind = 0;
	for ( MGSize k=1; k<=in; ++k)
		ind += k;

	return *(mtab+ind+jn);
}



template <Dimension DIM, class ELEM_TYPE> 
inline Metric<DIM,ELEM_TYPE>& Metric<DIM,ELEM_TYPE>::operator+=( const Metric<DIM,ELEM_TYPE>& mtx)
{
	for ( MGSize i=0; i<SIZE; ++i)
		mtab[i] += mtx.mtab[i];
	return *this;
}


template <Dimension DIM, class ELEM_TYPE> 
inline Metric<DIM,ELEM_TYPE>& Metric<DIM,ELEM_TYPE>::operator+=( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; ++i)
		mtab[i] += d;
	return *this;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Metric<DIM,ELEM_TYPE>& Metric<DIM,ELEM_TYPE>::operator-=( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; ++i)
		mtab[i] -= d;
	return *this;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Metric<DIM,ELEM_TYPE>& Metric<DIM,ELEM_TYPE>::operator*=( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; ++i)
		mtab[i] *= d;
	return *this;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Metric<DIM,ELEM_TYPE>& Metric<DIM,ELEM_TYPE>::operator/=( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; ++i)
		mtab[i] /= d;
	return *this;
}


template <Dimension DIM, class ELEM_TYPE> 
inline Metric<DIM,ELEM_TYPE> operator *( const MGFloat& d, const Metric<DIM,ELEM_TYPE>& mtx)
{
	Metric<DIM,ELEM_TYPE> mtxtemp;
	for( MGSize i=0; i<Metric<DIM,ELEM_TYPE>::SIZE; ++i)
		mtxtemp[i] = mtx[i] * d;
	return mtxtemp;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Metric<DIM,ELEM_TYPE> operator *( const Metric<DIM,ELEM_TYPE>& mtx, const MGFloat& d )
{
	Metric<DIM,ELEM_TYPE> mtxtemp;
	for( MGSize i=0; i<Metric<DIM,ELEM_TYPE>::SIZE; ++i)
		mtxtemp[i] = mtx[i] * d;
	return mtxtemp;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Metric<DIM,ELEM_TYPE> operator +( const Metric<DIM,ELEM_TYPE>& mtx1, const Metric<DIM,ELEM_TYPE>& mtx2)
{
	Metric<DIM,ELEM_TYPE> mtxtemp;
	for( MGSize i=0; i<Metric<DIM,ELEM_TYPE>::SIZE; ++i)
		mtxtemp[i] = mtx1[i] + mtx2[i];
	return mtxtemp;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Metric<DIM,ELEM_TYPE> operator -( const Metric<DIM,ELEM_TYPE>& mtx1, const Metric<DIM,ELEM_TYPE>& mtx2)
{
	Metric<DIM,ELEM_TYPE> mtxtemp;
	for( MGSize i=0; i<Metric<DIM,ELEM_TYPE>::SIZE; ++i)
		mtxtemp[i] = mtx1[i] - mtx2[i];
	return mtxtemp;
}




template <Dimension DIM, class ELEM_TYPE> 
inline void Metric<DIM,ELEM_TYPE>::Reset()
{
	::memset( mtab, 0, SIZE*sizeof(MGFloat) );
}


template <Dimension DIM, class ELEM_TYPE> 
inline void Metric<DIM,ELEM_TYPE>::InitIso( const MGFloat& h)
{
	THROW_INTERNAL( "Not implemented");
}

template <> 
inline void Metric<DIM_1D,MGFloat>::InitIso( const MGFloat& h)
{
	mtab[0] = 1/(h*h);
}

template <> 
inline void Metric<DIM_2D,MGFloat>::InitIso( const MGFloat& h)
{
	::memset( mtab, 0, SIZE*sizeof(MGFloat) );
	mtab[0] = mtab[2] = 1/(h*h);
}

template <> 
inline void Metric<DIM_3D,MGFloat>::InitIso( const MGFloat& h)
{
	::memset( mtab, 0, SIZE*sizeof(MGFloat) );
	mtab[0] = mtab[2] = mtab[5] = 1/(h*h);
}



template <Dimension DIM, class ELEM_TYPE> 
inline void Metric<DIM,ELEM_TYPE>::LimitMetric( const ELEM_TYPE& hmin, const ELEM_TYPE& hmax, const ELEM_TYPE& armax)
{
	ELEM_TYPE	hmin_inv = 1.0/(hmin*hmin);
	ELEM_TYPE	hmax_inv = 1.0/(hmax*hmax);

	ELEM_TYPE	lmax, lar;

	SMtx	mtxM, mtxD, mtxLI, mtxL;

	mtxM = *this;
	mtxM.Decompose( mtxD, mtxL);

	for ( MGSize i=0; i<DIM; ++i)
	{
		if ( mtxD(i,i) > hmin_inv)
			mtxD(i,i) = hmin_inv;

		if ( mtxD(i,i) < hmax_inv)
			mtxD(i,i) = hmax_inv;
	}

	if ( armax > 0)
	{
		lmax = mtxD(0,0);
		for ( MGSize i=1; i<DIM; ++i)
			if ( mtxD(i,i) > lmax)
				lmax = mtxD(i,i);

		lar = lmax / ( armax * armax);

		for ( MGSize i=0; i<DIM; ++i)
		{
			if ( mtxD(i,i) < lar)
				mtxD(i,i) = lar;
		}
	}


	mtxLI = mtxL;
	mtxLI.Invert();

	*this = mtxL * mtxD * mtxLI;

}




template <Dimension DIM, class ELEM_TYPE> 
inline void Metric<DIM,ELEM_TYPE>::Invert()
{
	THROW_INTERNAL( "Not implemented");
}


template <> 
inline void Metric<DIM_1D,MGFloat>::Decompose()
{
	mtab[0] = ::sqrt( mtab[0]);
}


template <> 
inline void Metric<DIM_2D,MGFloat>::Decompose()
{
	mtab[0] = ::sqrt( mtab[0]);
	mtab[1] /= mtab[0];
	mtab[2] = ::sqrt( mtab[2] - mtab[1]*mtab[1]);
}


template <> 
inline void Metric<DIM_3D,MGFloat>::Decompose()
{
	mtab[0] = ::sqrt( mtab[0]);
	mtab[1] /= mtab[0];
	mtab[2] = ::sqrt( mtab[2] - mtab[1]*mtab[1]);
	mtab[3] /= mtab[0];
	mtab[4] = (mtab[4] - mtab[1]*mtab[3] ) / mtab[2];
	mtab[5] = ::sqrt( mtab[5] - mtab[3]*mtab[3] - mtab[4]*mtab[4] );
}


template <Dimension DIM, class ELEM_TYPE> 
inline void Metric<DIM,ELEM_TYPE>::EigenDecompose( SVect& vecD, SMtx& mtxL) const
{
	SMtx mtxeigen = *this;
	mtxeigen.Decompose( vecD, mtxL);
}


template <> 
inline void Metric<DIM_1D,MGFloat>::EigenDecompose( SMtx& mtxeigen) const
{
	mtxeigen(0,0) = ::sqrt( mtab[0] );
}

template <> 
inline void Metric<DIM_1D,MGFloat>::EigenDecompose()
{
	mtab[0] = ::sqrt( mtab[0]);
}


template <> 
inline void Metric<DIM_2D,MGFloat>::EigenDecompose( SMtx& mtxeigen) const
{
	SMtx	mtxD, mtxL;

	mtxeigen(0,0) = mtab[0];
	mtxeigen(1,1) = mtab[2];
	mtxeigen(0,1) = mtxeigen(1,0) = mtab[1];

	mtxeigen.Decompose( mtxD, mtxL);

	mtxL.Invert();
	mtxD(0,0) = ::sqrt( mtxD(0,0) );
	mtxD(1,1) = ::sqrt( mtxD(1,1) );
	mtxeigen = mtxD * mtxL;
}


template <> 
inline void Metric<DIM_2D,MGFloat>::EigenDecompose()
{
	SMtx	mtxeigen;

	EigenDecompose( mtxeigen);

	mtab[0] = mtxeigen(0,0);
	mtab[1] = 0.5 * ( mtxeigen(0,1) + mtxeigen(1,0) );
	mtab[2] = mtxeigen(1,1);
}


template <> 
inline void Metric<DIM_3D,MGFloat>::EigenDecompose( SMtx& mtxeigen) const
{
	//THROW_INTERNAL( "Not implemented");
	SMtx	mtxD, mtxL;

	mtxeigen(0,0) = mtab[0];
	mtxeigen(1,1) = mtab[2];
	mtxeigen(2,2) = mtab[5];
	mtxeigen(0,1) = mtxeigen(1,0) = mtab[1];
	mtxeigen(0,2) = mtxeigen(2,0) = mtab[3];
	mtxeigen(1,2) = mtxeigen(2,1) = mtab[4];

	//cout << "--\n";
	//mtxeigen.Write();
	//cout << "--\n";

	mtxeigen.Decompose( mtxD, mtxL);

	mtxL.Invert();
	mtxD(0,0) = ::sqrt( mtxD(0,0) );
	mtxD(1,1) = ::sqrt( mtxD(1,1) );
	mtxD(2,2) = ::sqrt( mtxD(2,2) );
	mtxeigen = mtxD * mtxL;
}

template <> 
inline void Metric<DIM_3D,MGFloat>::EigenDecompose()
{
	SMtx	mtxeigen;

	EigenDecompose( mtxeigen);

	mtab[0] = mtxeigen(0,0);
	mtab[1] = 0.5 * ( mtxeigen(0,1) + mtxeigen(1,0) );
	mtab[2] = mtxeigen(1,1);
	mtab[3] = 0.5 * ( mtxeigen(0,2) + mtxeigen(2,0) );
	mtab[4] = 0.5 * ( mtxeigen(1,2) + mtxeigen(2,1) );
	mtab[5] = mtxeigen(2,2);
}



template <> 
inline void Metric<DIM_2D,MGFloat>::EigenMultDecomp( Vect<DIM_2D,MGFloat>& vres, const Vect<DIM_2D,MGFloat>& varg, const SMtx& mtx) const
{
	vres.rX() = mtx(0,0) * varg.cX() + mtx(0,1) * varg.cY();
	vres.rY() = mtx(1,0) * varg.cX() + mtx(1,1) * varg.cY();
}

template <> 
inline void Metric<DIM_2D,MGFloat>::EigenMultDecomp( Vect<DIM_2D,MGFloat>& vres, const Vect<DIM_2D,MGFloat>& varg) const
{
	vres.rX() = mtab[0] * varg.cX() + mtab[1] * varg.cY();
	vres.rY() = mtab[1] * varg.cX() + mtab[2] * varg.cY();
}


template <> 
inline void Metric<DIM_3D,MGFloat>::EigenMultDecomp( Vect<DIM_3D,MGFloat>& vres, const Vect<DIM_3D,MGFloat>& varg, const SMtx& mtx) const
{
	vres.rX() = mtx(0,0) * varg.cX() + mtx(0,1) * varg.cY() + mtx(0,2) * varg.cZ();
	vres.rY() = mtx(1,0) * varg.cX() + mtx(1,1) * varg.cY() + mtx(1,2) * varg.cZ();
	vres.rZ() = mtx(2,0) * varg.cX() + mtx(2,1) * varg.cY() + mtx(2,2) * varg.cZ();
}

template <> 
inline void Metric<DIM_3D,MGFloat>::EigenMultDecomp( Vect<DIM_3D,MGFloat>& vres, const Vect<DIM_3D,MGFloat>& varg) const
{
	vres.rX() = mtab[0] * varg.cX() + mtab[1] * varg.cY() + mtab[3] * varg.cZ();
	vres.rY() = mtab[1] * varg.cX() + mtab[2] * varg.cY() + mtab[4] * varg.cZ();
	vres.rZ() = mtab[3] * varg.cX() + mtab[4] * varg.cY() + mtab[5] * varg.cZ();
}


template <> 
inline MGFloat Metric<DIM_1D,MGFloat>::Distance( const Vect<DIM_1D,MGFloat>& vct) const
{
	return vct.cX()*vct.cX()*mtab[0];
}

template <> 
inline MGFloat Metric<DIM_2D,MGFloat>::Distance( const Vect<DIM_2D,MGFloat>& vct) const
{
	return vct.cX()*vct.cX()*mtab[0] + vct.cY()*vct.cY()*mtab[2] + 2.0*vct.cX()*vct.cY()*mtab[1];
}

template <> 
inline MGFloat Metric<DIM_3D,MGFloat>::Distance( const Vect<DIM_3D,MGFloat>& vct) const
{
	return vct.cX()*vct.cX()*mtab[0] + vct.cY()*vct.cY()*mtab[2] + vct.cZ()*vct.cZ()*mtab[5] +
		2.0*( vct.cX()*vct.cY()*mtab[1] + vct.cY()*vct.cZ()*mtab[4] + vct.cZ()*vct.cX()*mtab[3] );
}


template <> 
inline void Metric<DIM_2D,MGFloat>::MultDecomp( Vect<DIM_2D,MGFloat>& vres, const Vect<DIM_2D,MGFloat>& varg) const
{
	vres.rX() = mtab[0] * varg.cX() + mtab[1] * varg.cY();
	vres.rY() = mtab[2] * varg.cY();
}

template <> 
inline void Metric<DIM_3D,MGFloat>::MultDecomp( Vect<DIM_3D,MGFloat>& vres, const Vect<DIM_3D,MGFloat>& varg) const
{
	vres.rX() = mtab[0] * varg.cX() + mtab[1] * varg.cY() + mtab[3] * varg.cZ();
	vres.rY() = mtab[2] * varg.cY() + mtab[4] * varg.cZ();
	vres.rZ() = mtab[5] * varg.cZ();
}



template <> 
inline void Metric<DIM_2D,MGFloat>::Invert()
{
	const MGFloat	det = mtab[0]*mtab[2] - mtab[1]*mtab[1];

	const MGFloat t0 = mtab[2];
	const MGFloat t1 = - mtab[1];
	const MGFloat t2 = mtab[0];

	mtab[0] = t0 / det;
	mtab[1] = t1 / det;
	mtab[2] = t2 / det;
}


template <> 
inline void Metric<DIM_3D,MGFloat>::Invert()
{
	const MGFloat	det =	mtab[0]*mtab[2]*mtab[5] - mtab[0]*mtab[4]*mtab[4] - 
					mtab[1]*mtab[1]*mtab[5] + 2*mtab[1]*mtab[3]*mtab[4] - 
					mtab[3]*mtab[3]*mtab[2];

	const MGFloat	t0 = mtab[2]*mtab[5] - mtab[4]*mtab[4];
	const MGFloat	t1 = mtab[3]*mtab[4] - mtab[1]*mtab[5];
	const MGFloat	t2 = mtab[0]*mtab[5] - mtab[3]*mtab[3];
	const MGFloat	t3 = mtab[1]*mtab[4] - mtab[3]*mtab[2];
	const MGFloat	t4 = mtab[1]*mtab[3] - mtab[0]*mtab[4];
	const MGFloat	t5 = mtab[0]*mtab[2] - mtab[1]*mtab[1];

	mtab[0] = t0 / det;
	mtab[1] = t1 / det;
	mtab[2] = t2 / det;
	mtab[3] = t3 / det;
	mtab[4] = t4 / det;
	mtab[5] = t5 / det;
}


template <Dimension DIM, class ELEM_TYPE> 
inline void Metric<DIM,ELEM_TYPE>::Write( FILE *f) const
{
	fprintf( f, "+-");
	for( MGSize j=0; j<DIM; ++j)
		fprintf( f, "-------------");
	fprintf( f, "-+\n");

	for( MGSize i=0; i<DIM; ++i)
	{
		fprintf( f, "| ");
		for( MGSize j=0; j<DIM; ++j)
			fprintf( f, "%12.5lg ",(*this)(i,j));
		fprintf( f, " |\n");
	}

	fprintf( f, "+-");
	for( MGSize j=0; j<DIM; ++j)
		fprintf( f, "-------------");
	fprintf( f, "-+\n");
}

//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class ELEM_TYPE>
inline void CombineMetric( Metric<DIM,ELEM_TYPE>& tr, const Metric<DIM,ELEM_TYPE>& tin, const Metric<DIM,ELEM_TYPE>& tout)
{
	typedef SMatrix<DIM,ELEM_TYPE>	SMtx;
	typedef SVector<DIM,ELEM_TYPE>	SVect;

	SMtx mtxN, mtxNT, mtxM;
	tout.EigenDecompose( mtxN);
	mtxNT = mtxN;
	mtxNT.Transp();
	mtxM = tin;
	mtxM = mtxNT * mtxM * mtxN;

	tr = Metric<DIM,ELEM_TYPE>( mtxM);
}


template <Dimension DIM, class ELEM_TYPE>
void IntersectMetric( Metric<DIM,ELEM_TYPE>& tr, const Metric<DIM,ELEM_TYPE>& t1, const Metric<DIM,ELEM_TYPE>& t2);


} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Geom = SpaceGeomTools;




#endif // __METIRC_H__
