#include "tetrahedron.h"
#include "libcorecommon/smatrix.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {



// normal vector is directed outward
MGSize	Tetrahedron::mtabFaceConn[4][3] = { {1,2,3}, {0,3,2}, {0,1,3}, {0,2,1}};
MGSize	Tetrahedron::mtabEdgeConn[6][2] = { {0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {2,3}};


bool Tetrahedron::IsInside( const Vect3D& vct, const MGFloat& zero) const
{
	const MGFloat vol0 = orient3d( mtab[1]->cTab(), mtab[2]->cTab(), mtab[3]->cTab(), vct.cTab() );
	const MGFloat vol1 = orient3d( mtab[2]->cTab(), mtab[0]->cTab(), mtab[3]->cTab(), vct.cTab() );
	const MGFloat vol2 = orient3d( mtab[0]->cTab(), mtab[1]->cTab(), mtab[3]->cTab(), vct.cTab() );
	const MGFloat vol3 = orient3d( mtab[0]->cTab(), mtab[2]->cTab(), mtab[1]->cTab(), vct.cTab() );
	
	if ( vol0 >= zero && vol1 >= zero && vol2 >= zero && vol3 >= zero )
		return true;
	
	return false;
}

MGFloat Tetrahedron::IsInsideSphereMetric( const Vect3D& vct, const GMetric& metric) const
{
	return insphere( mtab[0]->cTab(), mtab[1]->cTab(), mtab[3]->cTab(), mtab[2]->cTab(), vct.cTab() );


	Vect3D	tabv[SIZE+1];
	Vect3D	vc, vctm;

	vc = 0.25*(*mtab[0] + *mtab[1] + *mtab[2] + *mtab[3]); 

	SMatrix<3>	mtxA, mtxAT, mtxM, mtxL, mtxLI, mtxG;
	SVector<3>	vecD, vec;
	GMetric	 tr;

	//metric.Write();

	mtxA.Resize(3,3);
	mtxA = SMatrix<3>( metric);
	mtxA(0,1) = mtxA(0,2) = mtxA(1,2) = 0;
	//mtxA.Write();

	mtxAT = mtxA;
	mtxAT.Transp();

	mtxM = mtxA * mtxAT;
	//mtxM.Write();

	tr = GMetric( mtxM);
	tr.Decompose();
	//tr.Write();

	mtxM.Decompose( vecD, mtxL);

	MGFloat	dav = max( vecD(0), max( vecD(1), vecD(2) ));

	//mtxL.Write();
	//vecD.Write();

	mtxLI = mtxL;
	mtxLI.Transp();

	mtxG = mtxLI;
	mtxA.Init(0);
	mtxA(0,0) = ::sqrt( vecD(0)/dav );
	mtxA(1,1) = ::sqrt( vecD(1)/dav );
	mtxA(2,2) = ::sqrt( vecD(2)/dav );

	mtxG = mtxA * mtxG;

	mtxLI = mtxG;

	//mtxL.Write();
	//mtxLI.Write();
	//(mtxL*mtxLI).Write();

	//mtxLI.Init(0);
	//mtxLI(0,0) = cos(0.1);
	//mtxLI(0,2) = sin(0.1);
	//mtxLI(2,0) = -sin(0.1);
	//mtxLI(2,2) = cos(0.1);
	//mtxLI(1,1) = 1;

	vec.Resize(3);
	for ( MGSize i=0; i<SIZE; ++i)
	{
		vec(0) = (*mtab[i]-vc).cX();
		vec(1) = (*mtab[i]-vc).cY();
		vec(2) = (*mtab[i]-vc).cZ();
		//vec.Write();

		//printf( "dbef = %lg\n", Dot( vec, vec) );
		vec = mtxLI * vec;
		//printf( "daft = %lg\n", Dot( vec, vec) );

		tabv[i].rX() = vec(0);
		tabv[i].rY() = vec(1);
		tabv[i].rZ() = vec(2);

		//vec.Write();
	}

	vec(0) = (vct-vc).cX();
	vec(1) = (vct-vc).cY();
	vec(2) = (vct-vc).cZ();
	//vec.Write();

	vec = mtxLI * vec;

	vctm.rX() = vec(0);
	vctm.rY() = vec(1);
	vctm.rZ() = vec(2);

	//vec.Write();


	MGFloat	da, db;

	da = insphere( tabv[0].cTab(), tabv[1].cTab(), tabv[3].cTab(), tabv[2].cTab(), vctm.cTab() );
	db = insphere( mtab[0]->cTab(), mtab[1]->cTab(), mtab[3]->cTab(), mtab[2]->cTab(), vct.cTab() );

	//printf( "da = %lg\n", da);
	//printf( "db = %lg\n\n", db);

	//if ( da * db < 0)
	//	THROW_INTERNAL( "CRASH");

	//_getch();
	

	for ( MGSize i=0; i<SIZE; ++i)
		metric.MultDecomp( tabv[i], *mtab[i]-vct);

	metric.MultDecomp( vctm, vct-vct);

	return max( da, db);

	return inspherefast( tabv[0].cTab(), tabv[1].cTab(), tabv[3].cTab(), tabv[2].cTab(), vctm.cTab() );
	//return inspherefast( tabv[0].cTab(), tabv[1].cTab(), tabv[3].cTab(), tabv[2].cTab(), vctm.cTab() );
}



} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

