#ifndef __BOX_H__
#define __BOX_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/vect.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {




//////////////////////////////////////////////////////////////////////
//  class Box
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class T=MGFloat>
class Box
{
public:
	Box() : mvMin( Vect<DIM,T>( static_cast<T>(0)) ), mvMax( Vect<DIM,T>( static_cast<T>(0)) )	{}
	Box( const Vect<DIM,T>& vmin, const Vect<DIM,T>& vmax) : mvMin( vmin), mvMax( vmax)	{}

	~Box()	{}

	void	ExportTEC( ostream *f)	{};
	
	void	Equalize();

	bool	IsInside( const Vect<DIM,T>& vct) const;
	bool	IsOverlapping( const Box<DIM>& box) const;
	
	Vect<DIM,T>			Center()		{ return (mvMin + mvMax)/T(2);}

	const Vect<DIM,T>&	cVMin() const	{ return mvMin;}
		  Vect<DIM,T>&	rVMin() 		{ return mvMin;}
		  
	const Vect<DIM,T>&	cVMax() const	{ return mvMax;}
		  Vect<DIM,T>&	rVMax() 		{ return mvMax;}


private:
	Vect<DIM,T>	mvMin;
	Vect<DIM,T>	mvMax;
};
//////////////////////////////////////////////////////////////////////

template <Dimension DIM, class T>
inline void Box<DIM,T>::Equalize()
{
	Vect<DIM,T>	dv;
	T	h;
	
	dv = mvMax - mvMin;

	h = 0;
	for ( MGInt i=0; i<DIM; ++i)
		if ( h < fabs( dv.cX(i) ) )
			h = fabs( dv.cX(i) );
		
	for ( MGInt i=0; i<DIM; ++i)
		if ( fabs( dv.cX(i) ) < h )
		{
			mvMax.rX(i) += 0.5*(h-dv.cX(i));
			mvMin.rX(i) -= 0.5*(h-dv.cX(i));
		}

}

template <Dimension DIM, class T>
inline bool Box<DIM,T>::IsInside( const Vect<DIM,T>& vct) const
{
	for ( MGInt i=0; i<DIM; ++i)
		if ( vct.cX(i) > mvMax.cX(i) || vct.cX(i) < mvMin.cX(i) )
			return false;

	return true;
}

//template < class T>
//inline bool Box<DIM_1D,T>::IsOverlapping( const Box<DIM_1D>& box) const
//{
//	if ( box.IsInside( VMin() ) || box.IsInside( VMax() ) ||
//		 IsInside( box.VMin() ) || IsInside( box.VMax() )  )
//	{
//		 return true;
//	}
//
//	return false;
//}


template <Dimension DIM, class T>
inline bool Box<DIM,T>::IsOverlapping( const Box<DIM>& box) const
{
	Box<DIM_1D>	box1, box2;

	for ( MGInt i=0; i<DIM; ++i)
	{
		box1.rVMin() = Vect1D( cVMin().cX(i) );
		box1.rVMax() = Vect1D( cVMax().cX(i) );

		box2.rVMin() = Vect1D( box.cVMin().cX(i) );
		box2.rVMax() = Vect1D( box.cVMax().cX(i) );

		if ( (!box1.IsInside( box2.cVMin() )) && (!box1.IsInside( box2.cVMax() )) &&
			 (!box2.IsInside( box1.cVMin() )) && (!box2.IsInside( box1.cVMax() ))  )
			 return false;
	}

	return true;
}


template <Dimension DIM, class T>
inline Box<DIM,T> Sum( const Box<DIM,T>& box1, const Box<DIM,T>& box2)
{
	Box<DIM,T> box;
	box.rVMin() = Minimum( box1.cVMin(), box2.cVMin() );
	box.rVMax() = Maximum( box1.cVMax(), box2.cVMax() );

	return box;
}


/*
template<>
inline void Box<DIM_1D>::ExportTEC( ostream *f)
{
	f << "VARIABLES = \"X\"" << endl;
	f << "ZONE I=2, F=POINT" << endl;
	f << mvMin.cX() << endl;
	f << mvMax.cX() << endl;
}


template<>
inline void Box<DIM_1D>::ExportTEC( FILE *f)
{
	fprintf( f, "VARIABLES = \"X\"\n");
	fprintf( f, "ZONE I=%d, F=POINT\n", 2);
	fprintf( f, "%lg\n", mvMin.cX() );
	fprintf( f, "%lg\n", mvMax.cX() );
}

template<>
inline void Box<DIM_2D>::ExportTEC( FILE *f)
{
	fprintf( f, "VARIABLES = \"X\",\"Y\"\n");

	fprintf( f, "ZONE  N=4, E=1, F=FEPOINT, ET=QUADRILATERAL\n");

	fprintf( f, "%lg %lg\n", mvMin.cX(), mvMin.cY() );
	fprintf( f, "%lg %lg\n", mvMax.cX(), mvMin.cY() );
	fprintf( f, "%lg %lg\n", mvMax.cX(), mvMax.cY() );
	fprintf( f, "%lg %lg\n", mvMin.cX(), mvMax.cY() );
	fprintf( f, "1 2 3 4\n" );
}

template<>
inline void Box<DIM_3D>::ExportTEC( FILE *f)
{
	fprintf( f, "VARIABLES = \"X\",\"Y\",\"Z\"\n");

	fprintf( f, "ZONE  N=8, E=1, F=FEPOINT, ET=BRICK\n");

	fprintf( f, "%lg %lg %lg\n", mvMin.cX(), mvMin.cY(), mvMin.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMax.cX(), mvMin.cY(), mvMin.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMax.cX(), mvMax.cY(), mvMin.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMin.cX(), mvMax.cY(), mvMin.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMin.cX(), mvMin.cY(), mvMax.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMax.cX(), mvMin.cY(), mvMax.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMax.cX(), mvMax.cY(), mvMax.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMin.cX(), mvMax.cY(), mvMax.cZ() );
	fprintf( f, "1 2 3 4 5 6 7 8\n" );
}
*/



} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Geom = SpaceGeomTools;



#endif // __BOX_H__
