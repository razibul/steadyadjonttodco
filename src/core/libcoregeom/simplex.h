#ifndef __SIMPLEX_H__
#define __SIMPLEX_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/vect.h"
#include "metric.h"
#include "predicates.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {



// TODO :: define threshold values here

//////////////////////////////////////////////////////////////////////
// class ConnTriangle - connectivity for triangle
//////////////////////////////////////////////////////////////////////
class ConnTriangle
{
public:
	static const MGSize&	cFaceConn( const MGSize& ifc, const MGSize& in)	{ return mtabFaceConn[ifc][in];}	
	static const MGSize&	cEdgeConn( const MGSize& iec, const MGSize& in)	{ return mtabFaceConn[iec][in];}	

private:
	static MGSize	mtabFaceConn[3][2];
};

//////////////////////////////////////////////////////////////////////
// class ConnTetrahedron - connectivity for tetrahedron
//////////////////////////////////////////////////////////////////////
class ConnTetrahedron
{
public:
	static const MGSize&	cFaceConn( const MGSize& ifc, const MGSize& in)	{ return mtabFaceConn[ifc][in];}	
	static const MGSize&	cEdgeConn( const MGSize& iec, const MGSize& in)	{ return mtabEdgeConn[iec][in];}	

private:
	static MGSize	mtabFaceConn[4][3];
	static MGSize	mtabEdgeConn[6][2];
};






//////////////////////////////////////////////////////////////////////
// class SimplexFace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class SimplexFace
{
	typedef Vect<DIM>	GVect;

public:
	enum { SIZE = DIM};
	enum { ESIZE = DIM*(DIM-1)/2};

	SimplexFace( const GVect& v1, const GVect& v2);
	SimplexFace( const GVect& v1, const GVect& v2, const GVect& v3);

	MGFloat		Area() const;
	GVect		Center() const;
	GVect		Vn() const;
	
	bool		IsAbove( const GVect& vct) const;
	bool		IsCrossed( const GVect& v1, const GVect& v2) const;

	MGFloat		CrossedVol( const GVect& v1, const GVect& v2) const;


	static MGSize		NumEdge()	{ return ESIZE;}

	static const MGSize&	cFaceConn( const MGSize& ifc, const MGSize& in);	
	static const MGSize&	cEdgeConn( const MGSize& iec, const MGSize& in);

private:
	const GVect*	mtab[SIZE];
};
//////////////////////////////////////////////////////////////////////


template <Dimension DIM>
inline SimplexFace<DIM>::SimplexFace( const GVect& v1, const GVect& v2)
{
	ASSERT( DIM == DIM_2D);
	mtab[0] = &v1;
	mtab[1] = &v2;
}

template <Dimension DIM>
inline SimplexFace<DIM>::SimplexFace( const GVect& v1, const GVect& v2, const GVect& v3)
{
	ASSERT( DIM == DIM_3D);
	mtab[0] = &v1;
	mtab[1] = &v2;
	mtab[2] = &v3;
}


//////////////////////////////////////////////////////////////////////
// connectivity


template <Dimension DIM>
inline const MGSize&	SimplexFace<DIM>::cFaceConn( const MGSize& ifc, const MGSize& in)	
{
	THROW_INTERNAL( "SimplexFace<DIM>::cFaceConn : this should never be called !");
	//return 0;
}

template <>
inline const MGSize&	SimplexFace<DIM_3D>::cFaceConn( const MGSize& ifc, const MGSize& in)	
{
	return ConnTriangle::cFaceConn(ifc,in);
}


template <Dimension DIM>
inline const MGSize& SimplexFace<DIM>::cEdgeConn( const MGSize& iec, const MGSize& in)	
{
	THROW_INTERNAL( "SimplexFace<DIM>::cEdgeConn : this should never be called !");
	//return 0;
}


template <>
inline const MGSize& SimplexFace<DIM_2D>::cEdgeConn( const MGSize& iec, const MGSize& in)	
{
	return in;
}

template <>
inline const MGSize& SimplexFace<DIM_3D>::cEdgeConn( const MGSize& iec, const MGSize& in)	
{
	return ConnTriangle::cEdgeConn(iec,in);
}





//////////////////////////////////////////////////////////////////////
// DIM_2D

template <>
inline SimplexFace<DIM_2D>::GVect SimplexFace<DIM_2D>::Center() const
{
	return ( *mtab[0] + *mtab[1] ) / 2.0;
}

template <>
inline SimplexFace<DIM_2D>::GVect SimplexFace<DIM_2D>::Vn() const
{
	GVect v = *mtab[1] - *mtab[0];

	return GVect( -v.cY(), v.cX() );
}

template <>
inline bool SimplexFace<DIM_2D>::IsAbove( const GVect& vct) const
{
	//if ( orient2d( mtab[0]->cTab(), mtab[1]->cTab(), vct.cTab() ) < 0 )
	if ( orient2d( mtab[0]->cTab(), mtab[1]->cTab(), vct.cTab() ) > 0 )
		return true;

	return false;
}


//////////////////////////////////////////////////////////////////////
// DIM_3D


template <>
inline SimplexFace<DIM_3D>::GVect SimplexFace<DIM_3D>::Center() const
{
	return ( *mtab[0] + *mtab[1] + *mtab[2] ) / 3.0;
}

template <>
inline SimplexFace<DIM_3D>::GVect SimplexFace<DIM_3D>::Vn() const
{
	return ( *mtab[2] - *mtab[0]) % ( *mtab[1] - *mtab[0]);
}

template <>
inline bool SimplexFace<DIM_3D>::IsAbove( const GVect& vct) const
{
	if ( orient3d( mtab[0]->cTab(), mtab[1]->cTab(), mtab[2]->cTab(), vct.cTab() ) < 0 )
		return true;

	return false;
}







//////////////////////////////////////////////////////////////////////
// class Simplex
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Simplex
{
	typedef Vect<DIM>	GVect;
	typedef Metric<DIM>	GMetric;


public:
	enum { SIZE = DIM+1};
	enum { ESIZE = DIM*(DIM+1)/2};

	Simplex( const GVect& v1, const GVect& v2);
	Simplex( const GVect& v1, const GVect& v2, const GVect& v3);
	Simplex( const GVect& v1, const GVect& v2, const GVect& v3, const GVect& v4);
	Simplex( const vector<GVect>& tabv);

	// returns volume with sign
	MGFloat		Volume() const;
	GVect		Center() const;
	
	// returns volume of a tetrahedron built with point vct and i-th face
	MGFloat		FaceSmpxVolume( const GVect& vct, const MGSize& i) const;

	MGFloat		InscribedSphereRadius() const;
	MGFloat		CircumSphereRadius() const;

	MGFloat		QMeasureALPHA() const;
	MGFloat		QMeasureBETAmax() const;
	MGFloat		QMeasureBETAmin() const;
	MGFloat		QMeasureGAMMA() const;

	MGFloat		QMeasureShape() const;
	MGFloat		QMeasureInterp() const;


	MGFloat		QMeasureMinAngle() const;
	MGFloat		QMeasureMaxAngle() const;

	void		FaceAngles( vector<MGFloat>& tabAngle) const;


	bool		IsInside( const GVect& vct, const MGFloat& zero=0.0) const;

	void		SphereCenterMetric( GVect& vct, const GMetric& metric) const;

	// if positive the vct is inside
	// if negative the vct is outside
	// if 0 the vct is cospherical
	// the calculation are not 100% exact - 0 may never happen
	MGFloat		IsInsideSphere( const GVect& vct) const;
	MGFloat		IsInsideSphereMetric( const GVect& vct, const GMetric& metric, const GMetric tabmetric[SIZE] ) const;
	
	SimplexFace<DIM>	GetFace( const MGSize& i) const;

	GVect		EdgeVt( const MGSize& i) const;

	static MGSize			NumEdge()		{ return ESIZE;}

	static const MGSize&	cFaceConn( const MGSize& ifc, const MGSize& in);	
	static const MGSize&	cEdgeConn( const MGSize& iec, const MGSize& in);

private:
	static MGFloat	mcoeffALPHA;
	static MGFloat	mcoeffBETA;

	const GVect*	mtab[SIZE];

};
//////////////////////////////////////////////////////////////////////





//////////////////////////////////////////////////////////////////////
// connectivity

template <Dimension DIM>
inline const MGSize& Simplex<DIM>::cFaceConn( const MGSize& ifc, const MGSize& in)	
{
	THROW_INTERNAL( "Simplex<DIM>::cFaceConn : this should never be called !");
	//return 0;
}

template <>
inline const MGSize& Simplex<DIM_2D>::cFaceConn( const MGSize& ifc, const MGSize& in)	
{
	return ConnTriangle::cFaceConn(ifc,in);
}

template <>
inline const MGSize& Simplex<DIM_3D>::cFaceConn( const MGSize& ifc, const MGSize& in)	
{
	return ConnTetrahedron::cFaceConn(ifc,in);
}



template <Dimension DIM>
inline const MGSize& Simplex<DIM>::cEdgeConn( const MGSize& iec, const MGSize& in)	
{
	THROW_INTERNAL( "Simplex<DIM>::cEdgeConn : this should never be called !");
	//return 0;
}

template <>
inline const MGSize& Simplex<DIM_1D>::cEdgeConn( const MGSize& iec, const MGSize& in)	
{
	return in;
}

template <>
inline const MGSize& Simplex<DIM_2D>::cEdgeConn( const MGSize& iec, const MGSize& in)	
{
	return ConnTriangle::cEdgeConn(iec,in);
}

template <>
inline const MGSize& Simplex<DIM_3D>::cEdgeConn( const MGSize& iec, const MGSize& in)	
{
	return ConnTetrahedron::cEdgeConn(iec,in);
}



//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
inline Simplex<DIM>::Simplex( const GVect& v1, const GVect& v2)
{
	ASSERT( DIM == DIM_1D);
	mtab[0] = &v1;
	mtab[1] = &v2;
}


template <Dimension DIM>
inline Simplex<DIM>::Simplex( const GVect& v1, const GVect& v2, const GVect& v3)
{
	ASSERT( DIM == DIM_2D);
	mtab[0] = &v1;
	mtab[1] = &v2;
	mtab[2] = &v3;
}

template <Dimension DIM>
inline Simplex<DIM>::Simplex( const GVect& v1, const GVect& v2, const GVect& v3, const GVect& v4)
{
	ASSERT( DIM == DIM_3D);
	mtab[0] = &v1;
	mtab[1] = &v2;
	mtab[2] = &v3;
	mtab[3] = &v4;
}

template <Dimension DIM>
inline Simplex<DIM>::Simplex( const vector<GVect>& tabv)
{
	ASSERT( tabv.size() == SIZE);
	for ( MGSize i=0; i< SIZE; ++i)
		mtab[i] = &tabv[i];
}


template <Dimension DIM>
inline Vect<DIM> Simplex<DIM>::EdgeVt( const MGSize& i) const
{
	return (*mtab[ cEdgeConn(i,1) ] - *mtab[ cEdgeConn(i,0) ]);
}





template <Dimension DIM>
inline MGFloat Simplex<DIM>::QMeasureALPHA() const
{
	return mcoeffALPHA * CircumSphereRadius() / InscribedSphereRadius();
}



template <Dimension DIM>
inline MGFloat Simplex<DIM>::QMeasureBETAmax() const
{
	MGFloat hmax = 0.0;

	for ( MGSize ie=0; ie<ESIZE; ++ie)
	{
		MGFloat h = ( *mtab[cEdgeConn(ie, 0)] - *mtab[cEdgeConn(ie, 1)] ).module();
		if ( h > hmax )
			hmax = h;
	}

	MGFloat inrad = InscribedSphereRadius();

	if ( inrad < hmax * 10.0e-6)
		inrad = hmax * 10.0e-6;

	return mcoeffBETA * hmax / inrad;
}

template <Dimension DIM>
inline MGFloat Simplex<DIM>::QMeasureBETAmin() const
{
	MGFloat hmin = ( *mtab[cEdgeConn(0, 0)] - *mtab[cEdgeConn(0, 1)] ).module();

	for ( MGSize ie=1; ie<ESIZE; ++ie)
	{
		MGFloat h = ( *mtab[cEdgeConn(ie, 0)] - *mtab[cEdgeConn(ie, 1)] ).module();
		if ( h < hmin )
			hmin = h;
	}

	MGFloat inrad = InscribedSphereRadius();

	if ( inrad < hmin * 10.0e-6)
		inrad = hmin * 10.0e-6;

	return mcoeffBETA * hmin / inrad;
}



template <Dimension DIM>
inline MGFloat Simplex<DIM>::QMeasureGAMMA() const
{
	vector<MGFloat> tab;
	FaceAngles( tab);
	MGFloat fi = *(max_element( tab.begin(), tab.end() ));

	return 1.0 / ::sin( fi);
}


template <Dimension DIM>
inline MGFloat Simplex<DIM>::QMeasureMinAngle() const
{
	vector<MGFloat> tab;
	FaceAngles( tab);
	return *(min_element( tab.begin(), tab.end() ));
}

template <Dimension DIM>
inline MGFloat Simplex<DIM>::QMeasureMaxAngle() const
{
	vector<MGFloat> tab;
	FaceAngles( tab);
	return *(max_element( tab.begin(), tab.end() ));
}



template <Dimension DIM>
inline void Simplex<DIM>::FaceAngles( vector<MGFloat>& tabAngle) const
{
	tabAngle.clear();

	for ( MGSize ie=0; ie<ESIZE; ++ie)
	{
		//const GVect vn0 = GetFace( cEdgeConn( ie, 0) ).Vn().versor();
		//const GVect vn1 = GetFace( cEdgeConn( ie, 1) ).Vn().versor();
		//MGFloat d = vn0 * vn1;
		//MGFloat fi = M_PI_2 + ::atan( d / ::sqrt( 1 - d*d) );

		const GVect vn0 = GetFace( cEdgeConn( ie, 0) ).Vn().versor();
		const GVect vn1 = GetFace( cEdgeConn( ie, 1) ).Vn().versor();
		MGFloat d = - (vn0 * vn1);
		MGFloat fi = ::atan2( ::sqrt( 1.0 - d*d), d );

		tabAngle.push_back( fi);
	}
}

template <Dimension DIM>
MGFloat Simplex<DIM>::InscribedSphereRadius() const
{
	MGFloat sum = GetFace(0).Area();
	for ( MGSize i=1; i<SIZE; ++i)
		sum += GetFace(i).Area();

	const MGFloat vol = Volume();

	return static_cast<MGFloat>( DIM ) * vol / sum;
}

//////////////////////////////////////////////////////////////////////
// DIM_1D

template <>
inline MGFloat Simplex<DIM_1D>::Volume() const
{
	return mtab[1]->cX() - mtab[0]->cX();		// TODO :: check sign !!!
}

template <>
inline Simplex<DIM_1D>::GVect Simplex<DIM_1D>::Center() const
{
	return ( *mtab[0] + *mtab[1] ) * 0.5;
}


template <>
inline MGFloat Simplex<DIM_1D>::FaceSmpxVolume( const Vect1D& vct, const MGSize& i) const
{
	return -1 * (i==0 ? ( mtab[1]->cX() - vct.cX() ) : ( vct.cX() - mtab[0]->cX() ) ); 
}


//template <>
//inline Vect<MGFloat,DIM_1D> Simplex<DIM_1D>::EdgeVt( const MGSize& i) const
//{
//	THROW_INTERNAL( "Not implemented");
//}


//////////////////////////////////////////////////////////////////////
// DIM_2D


template <>
inline MGFloat Simplex<DIM_2D>::Volume() const
{
	return 0.5 * orient2d( mtab[0]->cTab(), mtab[1]->cTab(), mtab[2]->cTab() );		// TODO :: check sign !!!
}

template <>
inline Simplex<DIM_2D>::GVect Simplex<DIM_2D>::Center() const
{
	return ( *mtab[0] + *mtab[1] + *mtab[2] ) / 3.0;
}



//template <>
//inline Vect<MGFloat,DIM_2D> Simplex<DIM_2D>::EdgeVt( const MGSize& i) const
//{
//	return (*mtab[ cEdgeConn(i,1) ] - *mtab[ cEdgeConn(i,0) ]);
//	//return (*mtab[ cFaceConn(i,1) ] - *mtab[ cFaceConn(i,0) ]);
//}


//template <>
//inline MGFloat Simplex<DIM_2D>::InscribedSphereRadius() const
//{
//	const MGFloat l1 = ( *mtab[1] - *mtab[0]).module();
//	const MGFloat l2 = ( *mtab[2] - *mtab[1]).module();
//	const MGFloat l3 = ( *mtab[0] - *mtab[2]).module();
//	const MGFloat vol = Volume();
//
//	return 2*vol / ( l1 + l2 + l3 );
//}


template <>
inline MGFloat Simplex<DIM_2D>::CircumSphereRadius() const
{
	//const MGFloat l1 = ( *mtab[1] - *mtab[0]).module();
	//const MGFloat l2 = ( *mtab[2] - *mtab[1]).module();
	//const MGFloat l3 = ( *mtab[0] - *mtab[2]).module();
	const MGFloat l1 = EdgeVt(0).module();
	const MGFloat l2 = EdgeVt(1).module();
	const MGFloat l3 = EdgeVt(2).module();

	const MGFloat vol = Volume();

	return 0.25 * l1 * l2 * l3 / vol;
}



template <>
inline SimplexFace<DIM_2D> Simplex<DIM_2D>::GetFace( const MGSize& i) const
{
	return SimplexFace<DIM_2D>( *mtab[ cFaceConn(i,0) ], *mtab[ cFaceConn(i,1) ] );
}

template <>
inline MGFloat Simplex<DIM_2D>::FaceSmpxVolume( const Vect2D& vct, const MGSize& i) const
{
	return 0.5 * orient2d( mtab[ cFaceConn(i,0) ]->cTab(), mtab[ cFaceConn(i,1) ]->cTab(), vct.cTab() );
}


template <>
inline MGFloat Simplex<DIM_2D>::IsInsideSphere( const GVect& vct) const
{
	return incircle( mtab[0]->cTab(), mtab[1]->cTab(), mtab[2]->cTab(), vct.cTab() );	// TODO :: check orient !!!
}


//////////////////////////////////////////////////////////////////////
// DIM_3D


template <>
inline MGFloat Simplex<DIM_3D>::Volume() const
{
	return orient3d( mtab[0]->cTab(), mtab[1]->cTab(), mtab[3]->cTab(), mtab[2]->cTab() ) / 6.0;
}

template <>
inline Simplex<DIM_3D>::GVect Simplex<DIM_3D>::Center() const
{
	return ( *mtab[0] + *mtab[1] + *mtab[2] + *mtab[3] ) * 0.25;
}

//template <>
//inline Vect<MGFloat,DIM_3D> Simplex<DIM_3D>::EdgeVt( const MGSize& i) const
//{
//	return (*mtab[ cEdgeConn(i,1) ] - *mtab[ cEdgeConn(i,0) ]);
//}


//template <>
//MGFloat Simplex<DIM_3D>::InscribedSphereRadius() const
//{
//	MGFloat sum = GetFace(0).Area();
//	for ( MGSize i=1; i<SIZE; ++i)
//		sum += GetFace(i).Area();
//
//	const MGFloat vol = Volume();
//
//	return 3*vol / sum;
//}


template <>
inline MGFloat Simplex<DIM_3D>::CircumSphereRadius() const
{
	const MGFloat a = EdgeVt(0).module() * EdgeVt(5).module();
	const MGFloat b = EdgeVt(1).module() * EdgeVt(4).module();
	const MGFloat c = EdgeVt(2).module() * EdgeVt(3).module();

	const MGFloat vol = Volume();

	return ::sqrt( (a+b+c)*(a+b-c)*(b+c-a)*(c+a-b) ) / ( 24.0 * vol);
}


//template <>
//inline MGFloat Simplex<DIM_3D>::QMeasureALPHA() const
//{
//	return InscribedSphereRadius() / CircumSphereRadius();
//}
//
//
//template <>
//inline MGFloat Simplex<DIM_3D>::QMeasureBETA() const
//{
//	const MGFloat coeff = ::sqrt(6.0) / 12.0;
//	MGFloat hmax = 0.0;
//
//	for ( MGSize ie=0; ie<ESIZE; ++ie)
//	{
//		MGFloat h = ( *mtab[cEdgeConn(ie, 0)] - *mtab[cEdgeConn(ie, 1)] ).module();
//		if ( h > hmax )
//			hmax = h;
//	}
//
//	return coeff * hmax / InscribedSphereRadius();
//}

template <>
inline SimplexFace<DIM_3D> Simplex<DIM_3D>::GetFace( const MGSize& i) const
{
	return SimplexFace<DIM_3D>( *mtab[ cFaceConn(i,0) ], *mtab[ cFaceConn(i,1) ], *mtab[ cFaceConn(i,2) ] );
}

template <>
inline MGFloat Simplex<DIM_3D>::FaceSmpxVolume( const GVect& vct, const MGSize& i) const
{
	return orient3d( mtab[ cFaceConn(i,0) ]->cTab(), mtab[ cFaceConn(i,2) ]->cTab(), 
					 mtab[ cFaceConn(i,1) ]->cTab(), vct.cTab() ) / 6.0;
}

template <>
inline MGFloat Simplex<DIM_3D>::IsInsideSphere( const GVect& vct) const
{
	return insphere( mtab[0]->cTab(), mtab[1]->cTab(), mtab[3]->cTab(), mtab[2]->cTab(), vct.cTab() );
}



//////////////////////////////////////////////////////////////////////
// misc functions

bool FindEdgeEdgeCross( Vect3D& vcross, const Vect3D& ve1b, const Vect3D& ve1e, const Vect3D& ve2b, const Vect3D& ve2e);

bool FindEdgeFaceCross( Vect3D& vcross, const Vect3D& ve1, const Vect3D& ve2, const Vect3D& vf1, const Vect3D& vf2, const Vect3D& vf3);

MGFloat EdgeDistance( const Vect3D& ve1, const Vect3D& ve2, const Vect3D& vct);



} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Geom = SpaceGeomTools;


#endif // __SIMPLEX_H__

