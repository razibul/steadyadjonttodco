#ifndef __GAUSSWIEGHTS_H__
#define __GAUSSWIEGHTS_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {

extern MGSize  tabN_simpl1D[];
extern MGSize  tabSumN_simpl1D[];
extern MGFloat tabGW_simpl1D[];

extern MGSize  tabN_simpl2D[];
extern MGSize  tabSumN_simpl2D[];
extern MGFloat tabGW_simpl2D[];

extern MGSize  tabN_simpl3D[];
extern MGSize  tabSumN_simpl3D[];
extern MGFloat tabGW_simpl3D[];

//////////////////////////////////////////////////////////////////////
// class GaussQuadrature
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class GaussQuadrature
{
public:
	GaussQuadrature( const MGSize& order); // up to 6th order

	MGSize			Size() const	{ return mN; }
	const MGFloat*	BaryCoordinates( const MGSize& i) const	{ return mtabGW + i*(DIM+2); }
	const MGFloat&	Weight( const MGSize& i) const { return mtabGW[ i*(DIM+2) + DIM+1 ]; }

private:
	MGSize			mN;
	const MGFloat*	mtabGW;
};


template <>
inline GaussQuadrature<DIM_1D>::GaussQuadrature( const MGSize& order) 
	: 
	mN( tabN_simpl1D[order/2]), 
	mtabGW( tabGW_simpl1D + tabSumN_simpl1D[order/2] * 3 ) 
	//mN( tabN_simpl1D[order-1]), 
	//mtabGW( tabGW_simpl1D + tabSumN_simpl1D[order-1] * 3 ) 
{}

template <>
inline GaussQuadrature<DIM_2D>::GaussQuadrature( const MGSize& order) 
	: 
	mN( tabN_simpl2D[order-1]), 
	mtabGW( tabGW_simpl2D + tabSumN_simpl2D[order-1] * 4 ) 
{}

template <>
inline GaussQuadrature<DIM_3D>::GaussQuadrature( const MGSize& order) 
	: 
	mN( tabN_simpl3D[order-1]), 
	mtabGW( tabGW_simpl3D + tabSumN_simpl3D[order-1] * 5 ) 
{}

//////////////////////////////
//extern MGSize tabN_2D[];
//extern MGSize tabSumN_2D[];
//extern MGFloat tabGW_2D[];
//
//extern MGSize tabN_3D[];
//extern MGSize tabSumN_3D[];
//extern MGFloat tabGW_3D[];
//
//template <Dimension DIM>
//class GaussIntegral
//{
//public:
//	GaussIntegral( const MGSize& order);
//
//	MGSize			Size() const	{ return mN; }
//	const MGFloat*	BaryCoordinates( const MGSize& i) const	{ return mtabGW + i*(DIM+1); }
//	const MGFloat&	Weight( const MGSize& i) const { return mtabGW[ i*(DIM+1) + DIM ]; }
//
//private:
//	MGSize			mN;
//	const MGFloat*	mtabGW;
//};
//
//
//template <>
//inline GaussIntegral<DIM_2D>::GaussIntegral( const MGSize& order) : mN( tabN_2D[order-1]), mtabGW( tabGW_2D + tabSumN_2D[order-1]*(DIM_2D+1) ) 
//{}
//
//template <>
//inline GaussIntegral<DIM_3D>::GaussIntegral( const MGSize& order) : mN( tabN_3D[order-1]), mtabGW( tabGW_3D + tabSumN_3D[order-1]*(DIM_3D+1) ) 
//{
//	//THROW_INTERNAL("NOT IMPLEMENTED !!");
//}




} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Geom = SpaceGeomTools;



#endif // __GAUSSWIEGHTS_H__
