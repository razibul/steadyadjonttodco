#include "triangle2d.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {



// normal vector is directed outward
MGSize	Triangle2D::mtabFaceConn[DIM_2D+1][DIM_2D] = { {2, 1}, {0,2}, {1,0} };


bool Triangle2D::IsInside( const Vect2D& vct, const MGFloat& zero) const
{
	const MGFloat vol0 = orient2d( mtab[1]->cTab(), mtab[2]->cTab(), vct.cTab() );
	const MGFloat vol1 = orient2d( mtab[2]->cTab(), mtab[0]->cTab(), vct.cTab() );
	const MGFloat vol2 = orient2d( mtab[0]->cTab(), mtab[1]->cTab(), vct.cTab() );
	
	if ( vol0 >= zero && vol1 >= zero && vol2 >= zero  )
		return true;
	
	return false;
}


MGFloat Triangle2D::IsInsideCircle( const Vect2D& vct) const
{
	return incircle( mtab[0]->cTab(), mtab[1]->cTab(), mtab[2]->cTab(), vct.cTab() );
}


MGFloat Triangle2D::IsInsideCircleMetric( const Vect2D& vct, const GMetric& metric) const
{
	return incircle( mtab[0]->cTab(), mtab[1]->cTab(), mtab[2]->cTab(), vct.cTab() );
}


bool Triangle2D::IsCrossed( const Vect2D& v1, const Vect2D& v2) const
{
	//Tetrahedron tetup( *mtab[0], *mtab[1], *mtab[2], v1);
	//Tetrahedron tetlo( *mtab[0], *mtab[1], *mtab[2], v2);
	//
	//MGFloat	volup = tetup.Volume();
	//MGFloat	vollo = tetlo.Volume();
	//
	//if ( volup * vollo > 0.0 )
	//{
	//	// both points are on the same side of the triangle
	//	return false;
	//}
	//
	//Vect2D vup, vlo;
	//
	//if ( volup > 0 )
	//{
	//	vup = v1;
	//	vlo = v2;
	//}
	//else
	//{
	//	vup = v2;
	//	vlo = v1;
	//}
	//
	//MGFloat vol0 = Tetrahedron( *mtab[1], vup, *mtab[2], vlo).Volume();
	//MGFloat vol1 = Tetrahedron( *mtab[2], vup, *mtab[0], vlo).Volume();
	//MGFloat vol2 = Tetrahedron( *mtab[0], vup, *mtab[1], vlo).Volume();
	//
	//MGFloat volret = MIN( vol0, MIN( vol1, vol2) );
	//
	//if ( volret >= -1.0e-16 )
	//	return true;

	return false;
}



} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

