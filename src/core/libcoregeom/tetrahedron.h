#ifndef __TETRAHEDRON_H__
#define __TETRAHEDRON_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/vect.h"
#include "metric.h"
#include "predicates.h"
#include "triangle3d.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {




//////////////////////////////////////////////////////////////////////
//	class Tetrahedron
//////////////////////////////////////////////////////////////////////
class Tetrahedron
{
	typedef Metric<DIM_3D>	GMetric;

public:
	enum { SIZE = 4};

	// the Tetrahedron class stores pointers to EXISTING points !
	// do not destroy them if you want to use the class functions
	Tetrahedron( const Vect3D& v1, const Vect3D& v2, const Vect3D& v3, const Vect3D& v4);

	// returns olume with sign
	MGFloat		Volume() const;

	Vect3D		Center() const;
	
	// returns volume of a tetrahedron biult with point vct and i-th face
	MGFloat		FaceTetVolume( const Vect3D& vct, const MGSize& i) const;

	MGFloat		InscribedSphereRadius() const;

	MGFloat		CircumSphereRadius() const;


	bool		IsInside( const Vect3D& vct, const MGFloat& zero=0.0) const;

	// if positive the vct is inside
	// if negative the vct is outside
	// if 0 the vct is cospherical
	// the calculation are not 100% exact - 0 may never happen
	MGFloat		IsInsideSphere( const Vect3D& vct) const;
	MGFloat		IsInsideSphereMetric( const Vect3D& vct, const GMetric& metric) const;
	
	Triangle3D	GetFace( const MGSize& i) const;


	static MGSize&	cFaceConn( const MGSize& ifc, const MGSize& in)	
													{ return mtabFaceConn[ifc][in];}

	static MGSize&	cEdgeConn( const MGSize& iec, const MGSize& in)	
													{ return mtabEdgeConn[iec][in];}

private:
	static MGSize	mtabFaceConn[4][3];
	static MGSize	mtabEdgeConn[6][2];

	const Vect3D*	mtab[SIZE];
};
//////////////////////////////////////////////////////////////////////




inline Tetrahedron::Tetrahedron( const Vect3D& v1, const Vect3D& v2, const Vect3D& v3, const Vect3D& v4)
{
	mtab[0] = &v1;
	mtab[1] = &v2;
	mtab[2] = &v3;
	mtab[3] = &v4;
}



inline MGFloat Tetrahedron::Volume() const
{
	return orient3d( mtab[0]->cTab(), mtab[1]->cTab(), mtab[3]->cTab(), mtab[2]->cTab() ) / 6.0;
}

inline Vect3D Tetrahedron::Center() const
{
	return 0.25 * ( *mtab[0] + *mtab[1] + *mtab[2] + *mtab[3] );
}


inline Triangle3D Tetrahedron::GetFace( const MGSize& i) const
{
	return Triangle3D( *mtab[mtabFaceConn[i][0]], *mtab[mtabFaceConn[i][1]], *mtab[mtabFaceConn[i][2]] );
}


inline MGFloat Tetrahedron::FaceTetVolume( const Vect3D& vct, const MGSize& i) const
{
	return orient3d( mtab[mtabFaceConn[i][0]]->cTab(), mtab[mtabFaceConn[i][1]]->cTab(), 
					 mtab[mtabFaceConn[i][2]]->cTab(), vct.cTab() ) / 6.0;
}


inline MGFloat Tetrahedron::IsInsideSphere( const Vect3D& vct) const
{
	return insphere( mtab[0]->cTab(), mtab[1]->cTab(), mtab[3]->cTab(), mtab[2]->cTab(), vct.cTab() );
}





} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Geom = SpaceGeomTools;



#endif // __TETRAHEDRON_H__
