#ifndef __VECT_H__
#define __VECT_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {




template <Dimension DIM, class ELEM_TYPE> 
class Vect;

template <Dimension DIM, class ELEM_TYPE> 
Vect<DIM,ELEM_TYPE> operator *( const ELEM_TYPE&,  const Vect<DIM,ELEM_TYPE>&);

template <Dimension DIM, class ELEM_TYPE> 
Vect<DIM,ELEM_TYPE> operator *( const Vect<DIM,ELEM_TYPE>&, const ELEM_TYPE&);

template <Dimension DIM, class ELEM_TYPE> 
Vect<DIM,ELEM_TYPE> operator /( const Vect<DIM,ELEM_TYPE>&, const ELEM_TYPE&);

template <Dimension DIM, class ELEM_TYPE> 
ELEM_TYPE	operator *( const Vect<DIM,ELEM_TYPE>&, const Vect<DIM,ELEM_TYPE>&);   // dot product

template <Dimension DIM, class ELEM_TYPE> 
Vect<DIM,ELEM_TYPE> operator +( const Vect<DIM,ELEM_TYPE>&, const Vect<DIM,ELEM_TYPE>&);

template <Dimension DIM, class ELEM_TYPE> 
Vect<DIM,ELEM_TYPE> operator -( const Vect<DIM,ELEM_TYPE>&, const Vect<DIM,ELEM_TYPE>&);

template <Dimension DIM, class ELEM_TYPE> 
Vect<DIM,ELEM_TYPE> operator %( const Vect<DIM,ELEM_TYPE>&, const Vect<DIM,ELEM_TYPE>&);   // vector product

template <Dimension DIM, class ELEM_TYPE> 
Vect<DIM,ELEM_TYPE> Minimum(  const Vect<DIM,ELEM_TYPE>& vec1,  const Vect<DIM,ELEM_TYPE>& vec2 );

template <Dimension DIM, class ELEM_TYPE> 
Vect<DIM,ELEM_TYPE> Maximum(  const Vect<DIM,ELEM_TYPE>& vec1,  const Vect<DIM,ELEM_TYPE>& vec2 );



//////////////////////////////////////////////////////////////////////
//	class Vect
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class ELEM_TYPE=MGFloat> 
class Vect
{
public:
    //////////////////////////////////////////////////////////////////
	// constructors
	Vect( const ELEM_TYPE& x);
	Vect( const ELEM_TYPE& x, const ELEM_TYPE& y);
	Vect( const ELEM_TYPE& x, const ELEM_TYPE& y, const ELEM_TYPE& z);
	Vect( const ELEM_TYPE& x, const ELEM_TYPE& y, const ELEM_TYPE& z, const ELEM_TYPE& w);
	Vect( const Vect<DIM,ELEM_TYPE>& vec);
	Vect();

    //////////////////////////////////////////////////////////////////
	// declaration of friend two argument operators
	friend Vect			operator *<>( const ELEM_TYPE&,  const Vect&);
	friend Vect			operator *<>( const Vect&, const ELEM_TYPE&);
	friend Vect			operator /<>( const Vect&, const ELEM_TYPE&);
	friend ELEM_TYPE	operator *<>( const Vect&, const Vect&);   // dot product
	friend Vect			operator +<>( const Vect&, const Vect&);
	friend Vect			operator -<>( const Vect&, const Vect&);
	friend Vect			operator %<>( const Vect&, const Vect&);   // vector product

	friend Vect		Minimum<>( const Vect&, const Vect& );
	friend Vect		Maximum<>( const Vect&, const Vect& );


    //////////////////////////////////////////////////////////////////
	// one argument operators
	Vect<DIM,ELEM_TYPE>&	operator  =( const Vect<DIM,ELEM_TYPE> &vec);
	Vect<DIM,ELEM_TYPE>&	operator +=( const Vect<DIM,ELEM_TYPE>&);
	Vect<DIM,ELEM_TYPE>&	operator -=( const Vect<DIM,ELEM_TYPE>&);
	Vect<DIM,ELEM_TYPE>&	operator *=( const ELEM_TYPE&);
	Vect<DIM,ELEM_TYPE>&	operator /=( const ELEM_TYPE&);

    //////////////////////////////////////////////////////////////////
	// misc functions
	ELEM_TYPE			module() const;
	Vect<DIM,ELEM_TYPE>	versor() const;
	
	
    //ELEM_TYPE&			operator()(const MGSize& i)			{ return *(mtab+i); }
    //const ELEM_TYPE&	operator()(const MGSize& i) const	{ return *(mtab+i); }

	const ELEM_TYPE&	cX( const MGSize& i) const	{ return mtab[i];}
	ELEM_TYPE&			rX( const MGSize& i)		{ return mtab[i];}

	const ELEM_TYPE&	cX() const		{ return mtab[0];}
	const ELEM_TYPE&	cY() const		{ ASSERT(DIM>1); return mtab[1];}
	const ELEM_TYPE&	cZ() const		{ ASSERT(DIM>2); return mtab[2];}
	const ELEM_TYPE&	cW() const		{ ASSERT(DIM>3); return mtab[3];}

	ELEM_TYPE&			rX()			{ return mtab[0];}
	ELEM_TYPE&			rY()			{ ASSERT(DIM>1); return mtab[1];}
	ELEM_TYPE&			rZ()			{ ASSERT(DIM>2); return mtab[2];}
	ELEM_TYPE&			rW()			{ ASSERT(DIM>3); return mtab[3];}

	const ELEM_TYPE*	cTab() const	{ return mtab;}

	void	Write( ostream& f=cout) const;

protected:
	ELEM_TYPE mtab[DIM];
};


//////////////////////////////////////////////////////////////////////
typedef Vect<DIM_1D> Vect1D;
typedef Vect<DIM_2D> Vect2D;
typedef Vect<DIM_3D> Vect3D;
typedef Vect<DIM_4D> Vect4D;




//////////////////////////////////////////////////////////////////////

template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE>::Vect( const Vect<DIM,ELEM_TYPE> &vec)
{
	ASSERT( DIM>0 && DIM<5);
	for ( MGSize i=0; i<DIM; ++i)
		mtab[i] = vec.mtab[i];
}

template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE>::Vect( const ELEM_TYPE& x)
{
	ASSERT( DIM > DIM_NONE && DIM <= DIM_4D);
	for ( MGSize i=0; i<DIM; mtab[i++]=x);
		//ASSERT( DIM == DIM_1D);
		//mtab[0] = x;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE>::Vect( const ELEM_TYPE& x, const ELEM_TYPE& y)
{
		ASSERT( DIM == DIM_2D);
		mtab[0] = x;
		mtab[1] = y;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE>::Vect( const ELEM_TYPE& x, const ELEM_TYPE& y, const ELEM_TYPE& z)
{
		ASSERT( DIM == DIM_3D);
		mtab[0] = x;
		mtab[1] = y;
		mtab[2] = z;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE>::Vect( const ELEM_TYPE& x, const ELEM_TYPE& y, const ELEM_TYPE& z, const ELEM_TYPE& w)
{
		ASSERT( DIM == DIM_4D);
		mtab[0] = x;
		mtab[1] = y;
		mtab[2] = z;
		mtab[3] = w;
}


template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE>::Vect()
{
	ASSERT( DIM > DIM_NONE && DIM <= DIM_4D);
	for ( MGSize i=0; i<DIM; mtab[i++]=(ELEM_TYPE)0.0);
}


template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE>& Vect<DIM,ELEM_TYPE>::operator =( const Vect<DIM,ELEM_TYPE> &vec)
{
	for ( MGSize i=0; i<DIM; ++i)
		mtab[i] = vec.mtab[i];
	return *this;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE>& Vect<DIM,ELEM_TYPE>::operator+=( const Vect<DIM,ELEM_TYPE>& vec)
{
	for ( MGSize i=0; i<DIM; ++i)
		mtab[i] += vec.mtab[i];
	return *this;
}


template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE>& Vect<DIM,ELEM_TYPE>::operator-=( const Vect<DIM,ELEM_TYPE>& vec)
{
	for ( MGSize i=0; i<DIM; ++i)
		mtab[i] -= vec.mtab[i];
	return *this;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE>& Vect<DIM,ELEM_TYPE>::operator*=( const ELEM_TYPE& doub)
{
	for ( MGSize i=0; i<DIM; ++i)
		mtab[i] *= doub;
	return *this;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE>& Vect<DIM,ELEM_TYPE>::operator/=( const ELEM_TYPE& doub)
{
	for ( MGSize i=0; i<DIM; ++i)
		mtab[i] /= doub;
	return *this;
}

template <Dimension DIM, class ELEM_TYPE> 
inline ELEM_TYPE Vect<DIM,ELEM_TYPE>::module() const
{
	return sqrt( (*this)*(*this));
}

template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE> Vect<DIM,ELEM_TYPE>::versor() const
{
	return (*this / module() );
}

template <Dimension DIM, class ELEM_TYPE> 
inline void Vect<DIM,ELEM_TYPE>::Write( ostream& f) const
{
	f << "[ ";
	for( MGSize i=0; i<DIM; i++)
		f << setw(12) << setprecision(6) << cX(i) << " ";
	f << "]\n";
}




//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

const MGFloat VZERO = 1.0e-8;

inline bool operator == ( const Vect2D& c1, const Vect2D& c2 )
{
	if ( fabs( c1.cX() - c2.cX() ) < VZERO &&
		 fabs( c1.cY() - c2.cY() ) < VZERO )

		return true;
	else
		return false;
}


inline bool operator < ( const Vect2D& c1, const Vect2D& c2 )
{
	if ( fabs( c1.cX() - c2.cX() ) > VZERO )
	{
		if ( c1.cX() + VZERO < c2.cX() )
			return true;
		else 
			return false;
	}
	else
	{
		if ( fabs( c1.cY() - c2.cY() ) > VZERO )
		{
			if ( c1.cY() + VZERO < c2.cY() )
				return true;
			else 
				return false;
		}
		else
		{
			return false;
		}
	}
}




inline bool operator == ( const Vect3D& c1, const Vect3D& c2 )
{
	if ( fabs( c1.cX() - c2.cX() ) < VZERO &&
		 fabs( c1.cY() - c2.cY() ) < VZERO &&
		 fabs( c1.cZ() - c2.cZ() ) < VZERO )

		return true;
	else
		return false;
}


inline bool operator < ( const Vect3D& c1, const Vect3D& c2 )
{
	if ( fabs( c1.cX() - c2.cX() ) > VZERO )
	{
		if ( c1.cX() + VZERO < c2.cX() )
			return true;
		else 
			return false;
	}
	else
	{
		if ( fabs( c1.cY() - c2.cY() ) > VZERO )
		{
			if ( c1.cY() + VZERO < c2.cY() )
				return true;
			else 
				return false;
		}
		else
		{
			if ( fabs( c1.cZ() - c2.cZ() ) > VZERO )
			{
				if ( c1.cZ() + VZERO < c2.cZ() )
					return true;
				else 
					return false;
			}
			else
			{
				return false;
			}
		}
	}
}



//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
// specialization of the Vect operators for DIM=1,2,3,4


//////////////////////////////////////////////////////////////////////
// Vect1D
//////////////////////////////////////////////////////////////////////
template<>
inline Vect1D operator*( const MGFloat& comp, const Vect1D& vec)
{
	return Vect1D( vec.mtab[0] * comp );
}


template<>
inline Vect1D operator*( const Vect1D& vec, const MGFloat& comp)
{
	return Vect1D( vec.mtab[0] * comp );
}


template<>
inline Vect1D operator/( const Vect1D& vec, const MGFloat& comp)
{
	return Vect1D( vec.mtab[0] / comp );
}


template<>
inline MGFloat operator*( const Vect1D& vec1, const Vect1D& vec2)
{
	return vec1.mtab[0] * vec2.mtab[0];
}


template<>
inline Vect1D operator+( const Vect1D& vec1, const Vect1D& vec2)
{
	return Vect1D( vec1.mtab[0] + vec2.mtab[0] );
}


template<>
inline Vect1D operator-( const Vect1D& vec1, const Vect1D& vec2)
{
	return Vect1D( vec1.mtab[0] - vec2.mtab[0] );
}




//////////////////////////////////////////////////////////////////////
// Vect2D
//////////////////////////////////////////////////////////////////////
template<>
inline Vect2D operator*( const MGFloat& comp, const Vect2D& vec)
{
	return Vect2D( vec.mtab[0] * comp, vec.mtab[1] * comp );
}


template<>
inline Vect2D operator*( const Vect2D& vec, const MGFloat& comp)
{
	return Vect2D( vec.mtab[0] * comp, vec.mtab[1] * comp );
}


template<>
inline Vect2D operator/( const Vect2D& vec, const MGFloat& comp)
{
	return Vect2D( vec.mtab[0] / comp, vec.mtab[1] / comp );
}


template<>
inline MGFloat operator*( const Vect2D& vec1, const Vect2D& vec2)
{
	return vec1.mtab[0] * vec2.mtab[0] + vec1.mtab[1] * vec2.mtab[1];
}


template<>
inline Vect2D operator+( const Vect2D& vec1, const Vect2D& vec2)
{
	return Vect2D( vec1.mtab[0] + vec2.mtab[0], vec1.mtab[1] + vec2.mtab[1] );
}


template<>
inline Vect2D operator-( const Vect2D& vec1, const Vect2D& vec2)
{
	return Vect2D( vec1.mtab[0] - vec2.mtab[0], vec1.mtab[1] - vec2.mtab[1] );
}


//////////////////////////////////////////////////////////////////////
// Vect3D
//////////////////////////////////////////////////////////////////////
template<>
inline Vect3D operator*( const MGFloat& comp, const Vect3D& vec)
{
	return Vect3D( vec.mtab[0] * comp, vec.mtab[1] * comp, vec.mtab[2] * comp );
}


template<>
inline Vect3D operator*( const Vect3D& vec, const MGFloat& comp)
{
	return Vect3D( vec.mtab[0] * comp, vec.mtab[1] * comp, vec.mtab[2] * comp );
}


template<>
inline Vect3D operator/( const Vect3D& vec, const MGFloat& comp)
{
	return Vect3D( vec.mtab[0] / comp, vec.mtab[1] / comp, vec.mtab[2] / comp );
}


template<>
inline MGFloat operator*( const Vect3D& vec1, const Vect3D& vec2)
{
	return vec1.mtab[0]*vec2.mtab[0] + vec1.mtab[1]*vec2.mtab[1] + vec1.mtab[2]*vec2.mtab[2];
}


template<>
inline Vect3D operator+( const Vect3D& vec1, const Vect3D& vec2)
{
	return Vect3D( vec1.mtab[0] + vec2.mtab[0], vec1.mtab[1] + vec2.mtab[1], vec1.mtab[2] + vec2.mtab[2] );
}


template<>
inline Vect3D operator-( const Vect3D& vec1, const Vect3D& vec2)
{
	return Vect3D( vec1.mtab[0] - vec2.mtab[0], vec1.mtab[1] - vec2.mtab[1], vec1.mtab[2] - vec2.mtab[2] );
}


template<>
inline Vect3D operator%( const Vect3D& vec1, const Vect3D& vec2)
{
	MGFloat x = vec1.mtab[1]*vec2.mtab[2] - vec1.mtab[2]*vec2.mtab[1];
	MGFloat y = vec1.mtab[2]*vec2.mtab[0] - vec1.mtab[0]*vec2.mtab[2];
	MGFloat z = vec1.mtab[0]*vec2.mtab[1] - vec1.mtab[1]*vec2.mtab[0];
	return Vect3D(x,y,z);
}



//////////////////////////////////////////////////////////////////////
// Vect4D
//////////////////////////////////////////////////////////////////////
template<>
inline Vect4D operator*( const MGFloat& comp, const Vect4D& vec)
{
	return Vect4D( vec.mtab[0] * comp, vec.mtab[1] * comp, vec.mtab[2] * comp, vec.mtab[3] * comp );
}


template<>
inline Vect4D operator*( const Vect4D& vec, const MGFloat& comp)
{
	return Vect4D( vec.mtab[0] * comp, vec.mtab[1] * comp, vec.mtab[2] * comp, vec.mtab[3] * comp );
}


template<>
inline Vect4D operator/( const Vect4D& vec, const MGFloat& comp)
{
	return Vect4D( vec.mtab[0] / comp, vec.mtab[1] / comp, vec.mtab[2] / comp, vec.mtab[3] / comp );
}


template<>
inline MGFloat operator*( const Vect4D& vec1, const Vect4D& vec2)
{
	return vec1.mtab[0]*vec2.mtab[0] + vec1.mtab[1]*vec2.mtab[1] + vec1.mtab[2]*vec2.mtab[2] + vec1.mtab[3]*vec2.mtab[3];
}


template<>
inline Vect4D operator+( const Vect4D& vec1, const Vect4D& vec2)
{
	return Vect4D( vec1.mtab[0] + vec2.mtab[0], vec1.mtab[1] + vec2.mtab[1], vec1.mtab[2] + vec2.mtab[2], vec1.mtab[3] + vec2.mtab[3] );
}


template<>
inline Vect4D operator-( const Vect4D& vec1, const Vect4D& vec2)
{
	return Vect4D( vec1.mtab[0] - vec2.mtab[0], vec1.mtab[1] - vec2.mtab[1], vec1.mtab[2] - vec2.mtab[2], vec1.mtab[3] - vec2.mtab[3] );
}



//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE> operator-( const Vect<DIM,ELEM_TYPE>& vec1, const Vect<DIM,ELEM_TYPE>& vec2)
{
	Vect<DIM,ELEM_TYPE>	v;
	for ( MGSize i=0; i<DIM; ++i)
		v.mtab[i] = vec1.mtab[i] - vec2.mtab[i];
	return v;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE> operator+( const Vect<DIM,ELEM_TYPE>& vec1, const Vect<DIM,ELEM_TYPE>& vec2)
{
	Vect<DIM,ELEM_TYPE>	v;
	for ( MGSize i=0; i<DIM; ++i)
		v.mtab[i] = vec1.mtab[i] + vec2.mtab[i];
	return v;
}

template <Dimension DIM, class ELEM_TYPE> 
inline ELEM_TYPE operator*( const Vect<DIM,ELEM_TYPE>& vec1, const Vect<DIM,ELEM_TYPE>& vec2)
{
	ELEM_TYPE	e(0);
	for ( MGSize i=0; i<DIM; ++i)
		e += vec1.mtab[i] * vec2.mtab[i];
	return e;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE> operator*( const ELEM_TYPE& e, const Vect<DIM,ELEM_TYPE>& vec)
{
	Vect<DIM,ELEM_TYPE>	v;
	for ( MGSize i=0; i<DIM; ++i)
		v.mtab[i] = e * vec.mtab[i];
	return v;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE> operator*( const Vect<DIM,ELEM_TYPE>& vec, const ELEM_TYPE& e)
{
	Vect<DIM,ELEM_TYPE>	v;
	for ( MGSize i=0; i<DIM; ++i)
		v.mtab[i] = e * vec.mtab[i];
	return v;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE> operator/( const Vect<DIM,ELEM_TYPE>& vec, const ELEM_TYPE& e)
{
	Vect<DIM,ELEM_TYPE>	v;
	for ( MGSize i=0; i<DIM; ++i)
		v.mtab[i] = vec.mtab[i] / e;
	return v;
}


template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE> Minimum(  const Vect<DIM,ELEM_TYPE>& vec1,  const Vect<DIM,ELEM_TYPE>& vec2 )
{
	Vect<DIM,ELEM_TYPE>	v;
	for ( MGSize i=0; i<DIM; ++i)
		v.mtab[i] = ( vec1.mtab[i] < vec2.mtab[i] )  ?  vec1.mtab[i]  : vec2.mtab[i];
	return v;
}

template <Dimension DIM, class ELEM_TYPE> 
inline Vect<DIM,ELEM_TYPE> Maximum(  const Vect<DIM,ELEM_TYPE>& vec1,  const Vect<DIM,ELEM_TYPE>& vec2 )
{
	Vect<DIM,ELEM_TYPE>	v;
	for ( MGSize i=0; i<DIM; ++i)
		v.mtab[i] = ( vec1.mtab[i] > vec2.mtab[i] )  ?  vec1.mtab[i]  : vec2.mtab[i];
	return v;
}

template <Dimension DIM, class ELEM_TYPE> 
inline ostream& operator <<( ostream& f, const Vect<DIM,ELEM_TYPE>& v)
{
	f << "[ ";
	for( MGSize i=0; i<DIM; i++)
		f << v.cX(i) << " ";
	f << "]";
	return f;
}

//template <Dimension DIM, class ELEM_TYPE> 
 
//inline ELEM_TYPE operator%( const Vect<DIM,ELEM_TYPE>& vec1, const Vect<DIM,ELEM_TYPE>& vec2)
//{
//	ASSERT(0);
//	ELEM_TYPE	e(0);
//	return e;
//}



} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Geom = SpaceGeomTools;



#endif // __VECT_H__
