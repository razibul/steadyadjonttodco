#ifndef __TRIANGLE2D_H__
#define __TRIANGLE2D_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/vect.h"
#include "metric.h"
#include "predicates.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {



//////////////////////////////////////////////////////////////////////
//	class Triangle2D
//////////////////////////////////////////////////////////////////////
class Triangle2D
{
	typedef Metric<DIM_2D>	GMetric;

public:
	enum { SIZE = 3};


	// the Triangle2D class stores pointers to EXISTING points !
	// do not destroy them if you want to use the class functions
	Triangle2D( const Vect2D& v1, const Vect2D& v2, const Vect2D& v3);

	// returns olume with sign
	MGFloat		Area() const;
	
	Vect2D		Center() const;


	Vect2D		Vn() const;
	

	bool		IsInside( const Vect2D& vct, const MGFloat& zero=0.0) const;

	bool		IsAbove( const Vect2D& vct) const;
	
	bool		IsCrossed( const Vect2D& v1, const Vect2D& v2) const;

	// if positive the vct is inside
	// if negative the vct is outside
	// if 0 the vct is cospherical
	// the calculation are not 100% exact - 0 may never happen
	MGFloat		IsInsideCircle( const Vect2D& vct) const;
	MGFloat		IsInsideCircleMetric( const Vect2D& vct, const GMetric& metric) const;

	static MGSize&	cFaceConn( const MGSize& ifc, const MGSize& in)	
													{ return mtabFaceConn[ifc][in];}


private:
	static MGSize	mtabFaceConn[DIM_2D+1][DIM_2D];

	const Vect2D*	mtab[SIZE];
};
//////////////////////////////////////////////////////////////////////

inline Triangle2D::Triangle2D( const Vect2D& v1, const Vect2D& v2, const Vect2D& v3)
{
	mtab[0] = &v1;
	mtab[1] = &v2;
	mtab[2] = &v3;
}

inline MGFloat Triangle2D::Area() const
{
// 	Vect2D	v20 = *mtab[2] - *mtab[0];
// 	Vect2D	v10 = *mtab[1] - *mtab[0];
// 
// 	return 0.5 * (v20 % v10).module();

	Vect2D	tabv[SIZE];

	tabv[0] = Vect2D( mtab[0]->cX(), mtab[0]->cY() );
	tabv[1] = Vect2D( mtab[1]->cX(), mtab[1]->cY() );
	tabv[2] = Vect2D( mtab[2]->cX(), mtab[2]->cY() );

	return 0.5 * orient2d( tabv[0].cTab(), tabv[1].cTab(), tabv[2].cTab() );

}

inline Vect2D Triangle2D::Center() const
{
	return ( *mtab[0] + *mtab[1] + *mtab[2] ) / 3.0;
}


inline Vect2D Triangle2D::Vn() const
{
	return ( *mtab[2] - *mtab[0]) % ( *mtab[1] - *mtab[0]);
}



inline bool Triangle2D::IsAbove( const Vect2D& vct) const
{
	if ( orient3d( mtab[0]->cTab(), mtab[1]->cTab(), mtab[2]->cTab(), vct.cTab() ) < 0 )
		return true;

	return false;
}



typedef Triangle2D Triangle;




} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Geom = SpaceGeomTools;



#endif // __TRIANGLE2D_H__
