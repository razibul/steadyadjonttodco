#include "triangle3d.h"
#include "tetrahedron.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {



// normal vector is directed outward
MGSize	Triangle3D::mtabFaceConn[DIM_2D+1][DIM_2D] = { {2, 1}, {0,2}, {1,0} };


bool Triangle3D::IsCrossed( const Vect3D& v1, const Vect3D& v2) const
{
	Tetrahedron tetup( *mtab[0], *mtab[1], *mtab[2], v1);
	Tetrahedron tetlo( *mtab[0], *mtab[1], *mtab[2], v2);
	
	MGFloat	volup = tetup.Volume();
	MGFloat	vollo = tetlo.Volume();
	
	if ( volup * vollo > 0.0 )
	{
		// both points are on the same side of the triangle
		return false;
	}
	
	Vect3D vup, vlo;
	
	if ( volup > 0 )
	{
		vup = v1;
		vlo = v2;
	}
	else
	{
		vup = v2;
		vlo = v1;
	}
	
	MGFloat vol0 = Tetrahedron( *mtab[1], vup, *mtab[2], vlo).Volume();
	MGFloat vol1 = Tetrahedron( *mtab[2], vup, *mtab[0], vlo).Volume();
	MGFloat vol2 = Tetrahedron( *mtab[0], vup, *mtab[1], vlo).Volume();
	
	MGFloat volret = MIN( vol0, MIN( vol1, vol2) );
	
	if ( volret >= -1.0e-16 )
		return true;

	return false;
}



} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

