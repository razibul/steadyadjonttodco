#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;


const string  TYPE = "ELEM_TYPE";

class Index
{
public:
	Index( const int& n, const int& m)
	{
		vector<int> tabtmp;
		GenerateRec( tabtmp, 0, n, m);
	}

	void Write()
	{
		for ( int i=0; i<mTab.size(); ++i)
		{
			for ( int k=0; k<mTab[i].size(); ++k)
				cout << mTab[i][k] << " ";
			cout << endl;
		}

		cout << "no of permut = " << mTab.size() << endl;
	}


private:

	void GenerateRec( const vector<int>& tabtmp, const int& ibeg, const int& iend, const int& level)
	{
		for ( int i=ibeg; i<iend; ++i)
		{
			vector<int> mytab = tabtmp;
			mytab.push_back(i);

			if ( level == 1)
				mTab.push_back( mytab);
			else
				GenerateRec( mytab, i+1, iend, level-1);
		}
	}


public:
	vector< vector<int> > mTab;

};



class SubDeterminant
{
public:
	//SubDeterminant()	{}

	SubDeterminant( const int& n, const int& m) : mN(n), mM(m), mindRow( m+1>n?n:m+1,m), mindCol(n,m)
	{
	}

	void Generate( ostream& of)
	{
		int shift = max(0,mN-1-mM);

		for ( int irow=0; irow<mindRow.mTab.size(); ++irow)
			for ( int i=0; i<mM; ++i)
				mindRow.mTab[irow][i] += shift;

		for ( int irow=0; irow<mindRow.mTab.size(); ++irow)
			for ( int icol=0; icol<mindCol.mTab.size(); ++icol)
			{
				of << TYPE <<" det" << mM << "_";
				for ( int i=0; i<mM; ++i)
					of << mindRow.mTab[irow][i];
				of << "_";
				for ( int j=0; j<mM; ++j)
					of << mindCol.mTab[icol][j];

				if ( mM == 2)
					expresion2( of, irow, icol);
				else
					expresion( of, irow, icol);

				of << endl;
			}

		of << endl;
	}

private:

	void expresion( ostream& of, const int& irow, const int& icol)
	{
		of << " = ";
		for ( int k=0; k<mM; ++k)
		{
			of << " mtx(" << mindRow.mTab[irow][0] << "," << mindCol.mTab[icol][k] << ") * det" << mM-1 << "_";
			for ( int i=1; i<mM; ++i)
				of << mindRow.mTab[irow][i];
				of << "_";
			for ( int j=0; j<mM; ++j)
				if ( j != k)
					of << mindCol.mTab[icol][j];

			if ( k < mM-1)
				if ( k%2 )
					of << " + ";
				else
					of << " - ";
		}
		of << ";";

	}

	void expresion2( ostream& of, const int& irow, const int& icol)
	{
		of << " = mtx(" << mindRow.mTab[irow][0] << "," << mindCol.mTab[icol][0] << ") * mtx(" << mindRow.mTab[irow][1] << "," << mindCol.mTab[icol][1]
		  << ") - mtx(" << mindRow.mTab[irow][0] << "," << mindCol.mTab[icol][1] << ") * mtx(" << mindRow.mTab[irow][1] << "," << mindCol.mTab[icol][0] << ");";
	}

private:
	int		mN;
	int		mM;

	Index	mindRow;
	Index	mindCol;
};




class Inverse
{
public:
	Inverse( const int& n) : mN(n)	{}


	void Generate()
	{
		ofstream of( "matrix.cpp");

		for ( int i=2; i<=mN; ++i)
		{
			SubDeterminant sdet(mN,i);
			sdet.Generate( of);
		}


		of << TYPE << " det = " << TYPE <<"( 1.0/det" << mN << "_";
		for ( int is=0; is<mN; ++is)
			of << is;
		of << "_";
		for ( int is=0; is<mN; ++is)
			of << is;
		of << ");" << endl;
		of << endl;

		for ( int i=0; i<mN; ++i)
		{
			for ( int j=0; j<mN; ++j)
			{

				of << "(*this)(" << i << "," << j << ") = ";

				if ( (i+j)%2 )
					of << "-";
				else
					of << " ";

				of << " det" << mN-1 << "_";

				for ( int js=0; js<mN; ++js)
					if ( js != j )
						of << js;

				of << "_";

				for ( int is=0; is<mN; ++is)
					if ( is != i )
						of << is;

				of << " * det;" << endl;
			}
			of << endl;
		}

	}

private:
	int		mN;
};

int main()
{
	Inverse	inv(7);

	inv.Generate();

	return 0;
}