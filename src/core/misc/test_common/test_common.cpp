#include "mgdecl.h"


INIT_VERSION("0.1","'something'");


class Base
{
public:
};


class A
{
public:
	virtual void DoA()	= 0;

	virtual Base* GetBase() = 0;

};

class B : public Base
{
public:
	virtual void DoB()	= 0;

};


class Conc : public A, public B
{
public:
	virtual void DoA()	{ cout << "AAAAAA" << endl;}
	virtual Base* GetBase() { return this;};


	virtual void DoB()	{ cout << "BBBBbbbb" << endl;}

};



int main( int argc, char* argv[])
{
	try
	{
		cout << "ver.: " << VERSION_NUMBER << "  build: " << VERSION_DATE << " " << VERSION_TIME << endl << endl;
		///////////////////////////////////////////////////////////////////////

		CheckTypeSizes();

		A* ptrA = new Conc;

		ptrA->DoA();

		B* ptrB = (B*)( ptrA->GetBase() );

		ptrB->DoB();


		return 0;


		MGInt qqq = 3;
		TRACE( "qqqqq " << qqq << " ---");

		THROW_INTERNAL( " azda sd jdasf dasdfnn asd" << qqq << "_________-------------");
		//THROW_INTERNAL( " azda sd jdasf dasdfnn asd");


	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		cerr << e.what() << endl;
	}

	return 0;
} 