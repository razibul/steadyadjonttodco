#ifndef __CONFIGCONST_H__
#define __CONFIGCONST_H__

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


namespace ConfigStr
{

//////////////////////////////////////////////////////////////////////
// section MASTER
	namespace Master
	{
		const char NAME[]		= "MASTER";
		const char CASE[]		= "MST_NAME";
		const char DESRIPT[]	= "MST_DESCRIPT";
	}

//////////////////////////////////////////////////////////////////////
// section SOLVER
	namespace Solver
	{
		const char NAME[] = "SOLVER";

		namespace Dim
		{
			const char KEY[]		= "SOL_DIM";
			namespace Value
			{
				const char	DIM_NONE[]	= "NONE";
				const char	DIM_0D[]	= "0D";
				const char	DIM_1D[]	= "1D";
				const char	DIM_2D[]	= "2D";
				const char	DIM_3D[]	= "3D";
				const char	DIM_4D[]	= "4D";
			}
		}

		namespace EqType
		{
			const char KEY[]		= "SOL_EQNTYPE";
			namespace Value
			{
				const char	NONE[]			= "NONE";
				const char	ADVECT[]		= "ADVECT";
				const char	POISSON[]		= "POISSON";
				const char	EULER[]			= "EULER";
				const char	NS[]			= "NS";
				const char	RANS_SA[]		= "RANS_SA";
				const char	RANS_KOMEGA[]	= "RANS_KOMEGA";
				const char	RANS_2E[]		= "RANS_2E";
			}
		}

		namespace SolType
		{
			const char KEY[]	= "SOL_SOLTYPE";
			namespace Value
			{
				const char DEFAULT[]	= "DEFAULT";
				const char OPTIM[]		= "OPTIM";
				const char ADJOINT[]	= "ADJOINT";
			}
		}
		const char HESSIAN[]	= "SOL_HESSIAN";	//temporarily for hessian computation
		const char HESSIAN_FD2ND[]	= "SOL_HESSIAN_FD2ND";	//temporarily for hessian computation
		const char MAXNITER[]	= "SOL_MAXNITER";			// max. number of iter.
		const char NSTORE[]		= "SOL_NSTORE";				// number of iter. for storing	
		const char CONVEPS[]	= "SOL_CONVEPS";			// convergence criterion

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection OPTIMIZER
		namespace Optimizer
		{
			const char NAME[]		= "OPTIMIZER";
			const char OPT_DIM[]	= "OPTIM_DIM";
			const char ALGORITHM[]	= "OPTIM_ALGOR";
			const char LOBND[]		= "OPTIM_LOBND";
			const char UPBND[]		= "OPTIM_UPBND";
			const char MAXITER[]	= "OPTIM_MAXITER";
			const char SOLPREV[]	= "OPTIM_SOLPREV";
			const char TEMPH[]		= "OPTIM_TEMPH";

			namespace Inequality
			{
				const char KEY[]	= "INEQUALITY";
				const char TOL[]	= "INEQ_TOL";

				namespace Type
				{
					const char KEY[]	= "INEQ_TYPE";
					namespace Value
					{
						const char NOTINCREASE[] = "NOTINCREASE";
						const char NOTDECREASE[] = "NOTDECREASE";
					}

				}

				namespace Value
				{
					const char KEY[] = "INEQ_VALUE";
					const char VALUE[] = "INITIAL";
				}
				
			}
			namespace Type
			{
				const char KEY[] = "OPTIM_TYPE";
				namespace Value
				{
					const char	MIN[] = "MIN";
					const char	MAX[] = "MAX";
				}
			}
			
			const char TOL[] = "OPTIM_TOL";
			namespace TolFunc
			{
				const char KEY[] = "OPTIM_TOLFUNC";
				namespace Value
				{
					const char FTOL[] = "FTOL";
					const char XTOL[] = "XTOL";
				}
			}

			namespace TolType
			{
				const char KEY[] = "OPTIM_TOLTYPE";
				namespace Value
				{
					const char REL[] = "REL";
					const char ABS[] = "ABS";
				}
			}


		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection DESIGN_PARAMETER
		namespace DesignParameter
		{
			const char NAME[]			= "DESIGNPARAMETER";
			const char FNAME[]			= "DESPAR_FNAME";
			const char INITVAL[]		= "DESPAR_INITVAL";
			const char INITVALFNAME[]	= "DESPAR_INITVAL_FNAME";

			const char SURF[]	= "SURF";
		}


//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection OBJECTIVES
		namespace Objectives
		{
			const char NAME[]	= "OBJECTIVES";
			const char OUTFNAME[] = "OBJ_OUTFNAME";
			namespace Objective
			{
				const char NAME[] = "OBJECTIVE";
				namespace Type
				{
					const char KEY[] = "OBJ_TYPE";
					namespace Value
					{
						const char FORCE[] = "FORCE";
					}
				}
				const char DESCRIPT[]	= "OBJ_DESCRIPT";
				const char DIRX[]		= "OBJ_DIRX";
				const char DIRY[]		= "OBJ_DIRY";
				const char DIRZ[]		= "OBJ_DIRZ";
				const char ADJOINT[]	= "OBJ_ADJOINT";
				const char WRITE_ADJ_TEC[] 		= "OBJ_WRITE_ADJ_TEC";
				const char WRITE_ADJ_TECSURF[]	= "OBJ_WRITE_ADJ_TECSURF";
				
				namespace TEMP_TYPE
				{
					const char NAME[]	= "OBJ_TEMP_TYPE";
					namespace Value
					{
						const char ENERGY[]		= "ENERGY";
						const char PRESSURE[]	= "PRESSURE";
					}

				}

				namespace SURF
				{
					const char KEY[] = "SURF";
				}
			}

		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection FREEZEPOINT
		namespace FreezePoint
		{
			const char NAME[]	= "FREEZEPOINT";
			
		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection GRID_ASSISTANT
		namespace GridAssistant
		{
			const char NAME[]			= "GRIDASSISTANT";
			const char PERTINITVAL[]	= "PERTURB_INITVAL";
			const char PERTINIT[]		= "PERTURB_INIT";

			const char PERTDIRX[]		= "PERTURB_DIRX";
			const char PERTDIRY[]		= "PERTURB_DIRY";
			const char PERTDIRZ[]		= "PERTURB_DIRZ";
			
			const char PERTSIGMA[]		= "PERTURB_SIGMA";
			const char PERTMU[]			= "PERTURB_MU";
			const char PERTSCALE[]		= "PERTURB_SCALE";
		}
		
//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection GRID
		namespace Grid
		{
			const char NAME[]		= "GRID";
			const char FNAME[]		= "GRD_FNAME";
			namespace FType
			{
				const char KEY[]	= "GRD_FTYPE";
				namespace Value
				{
					const char ASCII[]	= "ASCII";
					const char BIN[]	= "BIN";
				}
			}
			const char MAXANGLE[]	= "GRD_MAXANGLE";
		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection AUXDATA
		namespace AuxData
		{
			const char NAME[]		= "AUXDATA";
			const char FNAME[]		= "AUX_FNAME";
			const char FTYPE[]		= "AUX_FTYPE";
		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection SOLUTION
		namespace Solution
		{
			const char NAME[]			= "SOLUTION";
			const char RESTART[]		= "TIM_RESTART";
			const char FNAME[]			= "TIM_FNAME";
			const char FTYPE[]			= "TIM_FTYPE";
			const char WRITE_TEC[]		= "SOL_WRITE_TEC";
			const char WRITE_TECSURF[]	= "SOL_WRITE_TECSURF";
		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection PHYSICS
		namespace Physics
		{
			const char NAME[]				= "PHYSICS";
			const char REF_LENGTH[]			= "VAR_REF_LENGTH";
			const char REF_AREA[]			= "VAR_REF_AREA";
			const char REF_REYNOLDS[]		= "VAR_REF_REYNOLDS";
			const char REF_PRANDTL[]		= "VAR_REF_PRANDTL";
			const char REF_PRANDTL_TURB[]	= "VAR_REF_PRANDTL_TURB";
			const char REF_MACH[]			= "VAR_REF_MACH";

			const char REF_TRANSL_VELO[]	= "REF_TRANSL_VELO";
			const char REF_ROT_OMEGA[]		= "REF_ROT_OMEGA";
			const char REF_ROT_AXIS_ORIG[]	= "REF_ROT_AXIS_ORIG";
			const char REF_ROT_AXIS_DIR[]	= "REF_ROT_AXIS_DIR";


			const char COEF_POISSON_LAPC[]		= "COEF_POISSON_LAPC";
			const char COEF_POISSON_LAPPHI[]	= "COEF_POISSON_LAPPHI";
			const char COEF_POISSON_CONV[]		= "COEF_POISSON_CONV";

			namespace RFR_Def
			{
				const char NAME[]			= "ROT_FR";

				const char RFR_TRANSL_VELO[]	= "RFR_TRANSL_VELO";
				const char RFR_ROT_OMEGA[]		= "RFR_ROT_OMEGA";
				const char RFR_ROT_AXIS_ORIG[]	= "RFR_ROT_AXIS_ORIG";
				const char RFR_ROT_AXIS_DIR[]	= "RFR_ROT_AXIS_DIR";
			}

			namespace Var
			{
				const char NAME[]			= "VAR";
				const char REFERENCE[]		= "reference";

				namespace Type
				{
					const char KEY[]		= "VAR_TYPE";
					namespace Value
					{
						const char RPUV[]		= "VAR_RPUV";
						const char RRERURV[]	= "VAR_RRERURV";
						const char MRPA[]		= "VAR_MRPA";
					}
				}

				const char RO[]			= "VAR_RO";
				const char P[]			= "VAR_P";
				const char UX[]			= "VAR_UX";
				const char UY[]			= "VAR_UY";
				const char UZ[]			= "VAR_UZ";
				const char ROE[]		= "VAR_ROE";
				const char ROUX[]		= "VAR_ROUX";
				const char ROUY[]		= "VAR_ROUY";
				const char ROUZ[]		= "VAR_ROUZ";
				const char ALPHA[]		= "VAR_ALPHA";
				const char MACH[]		= "VAR_MACH";
				const char NUT[]		= "VAR_NUT";
				const char K[]			= "VAR_K";
				const char OMEGA[]		= "VAR_OMEGA";
			}

			namespace PERIODIC
			{
				const char NAME[]		= "PERIODIC";
				const char ID[]			= "PRD_ID";

				namespace Type
				{
					const char KEY[]	= "PRD_TYPE";
					namespace Value
					{
						const char TRANS[] = "PRD_TRANS";
						const char ANGLE[] = "PRD_ANGLE";
					}
				}

				const char SRC_ID[]		= "PRD_SOURCE_ID";
				const char TRG_ID[]		= "PRD_TARGET_ID";
				const char VCT_X[]		= "PRD_VCT_X";
				const char VCT_Y[]		= "PRD_VCT_Y";
				const char VCT_Z[]		= "PRD_VCT_Z";
				const char ANGLE[]		= "PRD_ANGLE";
				const char ANGLE_AXIS[] = "PRD_ANGLE_AXIS";
			}

			namespace BC
			{
				const char KEY[]			= "BC";
				namespace Value
				{
					const char	FARFIELD[]		= "FARFIELD";
					const char	INVISCWALL[]	= "INVISCWALL";
					const char	SYMETRY[]		= "SYMETRY";
					const char	VISCWALL[]		= "VISCWALL";
					const char	EXTRAPOL[]		= "EXTRAPOL";
					const char	DEF[]			= "DEF";
					const char	PERIODIC[]		= "PERIODIC";
				}
			}
		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection EXECUTOR
		namespace Executor
		{
			const char NAME[]		= "EXECUTOR";

			namespace BlockDef
			{
				const char KEY[]		= "BLOCKDEF";
				namespace Value
				{
					const char FULL[] = "FULL";
					const char ADJOINT[]	= "ADJOINT";
				}
			}

			//-------------------------------------------------------
			// SPACESOL section

			namespace SpaceSol
			{	
				const char NAME[]	= "SPACESOL";

				namespace Type
				{
					const char KEY[]	= "SPC_TYPE";
					namespace Value
					{
						const char	NONE[]	= "NONE";
						const char	RDS[]	= "RDS";
					}
				}

				const char NUMDIFF_EPS[]	= "SPC_NUMDIFF_EPS";
				const char SMOOTH_DT[]		= "SPC_SMOOTH_DT";

				namespace FlowFunc
				{	
					const char NAME[]	= "FLOWFUNC";

					namespace FuncType
					{
						const char KEY[]	= "FNC_TYPE";
						namespace Value
						{
							const char	NONE[]			= "NONE";
							const char	MTX_LDAN[]		= "MTX_LDAN";
							const char	MTX_LDA[]		= "MTX_LDA";
							const char	MTX_N[]			= "MTX_N";
							const char	HE_LDA_PSI[]	= "HE_LDA_PSI";
							const char	HE_LDA_LDA[]	= "HE_LDA_LDA";
							const char	HE_LDA_N[]		= "HE_LDA_N";
							const char	HE_LDAN_PSI[]	= "HE_LDAN_PSI";
							const char	HE_N_PSI[]		= "HE_N_PSI";
							const char	HE_N_N[]		= "HE_N_N";
							const char	SM_LDAN_PSI[]	= "SM_LDAN_PSI";
							const char	SM_LDA_PSI[]	= "SM_LDA_PSI";
							const char	SM_LDA_LDA[]	= "SM_LDA_LDA";
						}
					}
				}
			}


			//-------------------------------------------------------
			// TIMESOL section

			namespace TimeSol
			{	
				const char NAME[]	= "TIMESOL";
				namespace Type
				{
					const char KEY[]		= "TIM_TYPE";
					namespace Value
					{
						const char	EXPLICIT[]	= "EXPLICIT";
						const char	IMPLICIT[]	= "IMPLICIT";
						const char	PETSC[]		= "PETSC";
                        const char  TRILINOS[]  = "TRILINOS";
					}
				}

				namespace JacobType
				{
					const char KEY[]		= "TIM_JACOBTYPE";
					namespace Value
					{
						const char	NONE[]			= "NONE";
						const char	NUMERICAL[]		= "NUMERICAL";
						const char	ANALYTICAL[]	= "ANALYTICAL";
						const char	PICARD[]		= "PICARD";
					}
				}

				const char LINMAXITER[]	= "TIM_LINMAXITER";
				const char LINDKSPACE[]	= "TIM_LINDKSPACE";
				const char LINMINEPS[]	= "TIM_LINMINEPS";

				const char MAXNITER[]	= "TIM_MAXNITER";
				const char NSTORE[]		= "TIM_NSTORE";
				const char CONVEPS[]	= "TIM_CONVEPS";
				const char CFL[]		= "TIM_CFL";
				const char MINCFL[]		= "TIM_MINCFL";
				const char MAXCFL[]		= "TIM_MAXCFL";
				const char NCFLGROWTH[]	= "TIM_NCFLGROWTH";
				const char NCFLSTART[]	= "TIM_NCFLSTART";
			}

			//-------------------------------------------------------
			// INTEGRATOR section

			namespace Integrator
			{	
				const char NAME[]	= "INTEGRATOR";

				namespace Type
				{
					const char KEY[]	= "INTEG_TYPE";
					namespace Value
					{
						const char	NONE[]			= "NONE";
					}
				}
			}

			namespace Assistant
			{	
				const char TYPE[]	= "AST_TYPE";
				const char FREQ[]	= "AST_FREQ";

			}

			namespace Adjoint
			{
				const char KEY[]		= "ADJOINT";
				const char OUTFNAME[]	= "OUT_FNAME";
				namespace FD_SCHEME
				{
					const char KEY[]	= "ADJ_FDSCHEME";
					namespace Value
					{
						const char CENTRAL[] = "CENTRAL";
						const char FORWARD[] = "FORWARD";
					}
				}

			}
		}
	
	}
}
 


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CONFIGCONST_H__
