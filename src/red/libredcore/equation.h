#ifndef __EQUATION_H__
#define __EQUATION_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libcoregeom/vect.h"
#include "libcorecommon/smatrix.h"
#include "libcorecommon/svector.h"
#include "libredcore/mapper.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// enum EQN_TYPE
//////////////////////////////////////////////////////////////////////
enum EQN_TYPE
{
	EQN_UNKNOWN = 0,
	EQN_ADVECT,
	EQN_POISSON,
	EQN_EULER,
	EQN_NS,
	EQN_RANS_SA,
	EQN_RANS_KOMEGA,
	EQN_RANS_2E,
	EQN_MHD,

	EQN_RFR_EULER,
	EQN_RFR_NS,
	EQN_RFR_RANS_SA
};


//////////////////////////////////////////////////////////////////////
// enum SPLIT_TYPE
//////////////////////////////////////////////////////////////////////
enum SPLIT_TYPE
{
	SPLIT_UNKNOWN = 0,
	SPLIT_FULL,
	SPLIT_EULER_TURB,
};




//////////////////////////////////////////////////////////////////////
// class Types
//////////////////////////////////////////////////////////////////////
template <MGSize DIM, MGSize N>
class EquationTypes
{
public:
	typedef Geom::Vect<static_cast<Geom::Dimension>(DIM)>	GVec;
	typedef SVector<N>										Vec;
	typedef SVector<N,GVec>									VecGrad;
	typedef SMatrix<N>										Mtx;
};



//////////////////////////////////////////////////////////////////////
// class EquationSizes
//////////////////////////////////////////////////////////////////////
template <MGSize DIM, EQN_TYPE ET>
class EquationSizes
{
public:
	static MGString	NameEqnType()	{ return "UNKNOWN"; }
};


//////////////////////////////////////////////////////////////////////
// class EquationSizes - advection
//////////////////////////////////////////////////////////////////////
template <MGSize DIM>
class EquationSizes<DIM,EQN_ADVECT>
{
public:
	enum { SIZE = 1};		// size of the state vector
	enum { AUXSIZE = 0};		// size of the auxiliary data (per node)

	static MGString	NameEqnType()	{ return "ADVECT"; }

};

//////////////////////////////////////////////////////////////////////
// class EquationSizes - poisson
//////////////////////////////////////////////////////////////////////
template <MGSize DIM>
class EquationSizes<DIM,EQN_POISSON>
{
public:
	enum { SIZE = 1};		// size of the state vector
	enum { AUXSIZE = 0};		// size of the auxiliary data (per node)

	static MGString	NameEqnType()	{ return "POISSON"; }

};



//////////////////////////////////////////////////////////////////////
// class EquationSizes - Euler
//////////////////////////////////////////////////////////////////////
template <MGSize DIM>
class EquationSizes<DIM,EQN_EULER>
{
public:
	enum { SIZE = DIM + 2};	// size of the state vector
	enum { AUXSIZE = 0};		// size of the auxiliary data (per node)

	enum { ID_RHO = 0};
	enum { ID_P = 1};

	static MGString	NameEqnType()	{ return "EULER"; }

	template <MGSize I>
	class U
	{
	public:
		enum { ID = I+2};
	};

};



//////////////////////////////////////////////////////////////////////
// class EquationSizes - Navier Stokes
//////////////////////////////////////////////////////////////////////
template <MGSize DIM>
class EquationSizes<DIM,EQN_NS> : public EquationSizes<DIM,EQN_EULER>
{
public:
	static MGString	NameEqnType()	{ return "NS"; }
};

//////////////////////////////////////////////////////////////////////
// class EquationSizes - MHD
//////////////////////////////////////////////////////////////////////
template <MGSize DIM>
class EquationSizes<DIM,EQN_MHD> : public EquationSizes<DIM,EQN_EULER>
{
public:
	enum { SIZE = 4*DIM + 6};	// size of the state vector
	enum { AUXSIZE = 0};		// size of the auxiliary data (per node)

	enum { ID_RHO = 0};
	enum { ID_P = 2*DIM+1};

	static MGString	NameEqnType()	{ return "MHD"; }
};



//////////////////////////////////////////////////////////////////////
// class EquationSizes - Reynolds Avareged Navier Stokes - Spalart-Allmaras - 1 equation
//////////////////////////////////////////////////////////////////////
template <MGSize DIM>
class EquationSizes<DIM,EQN_RANS_SA> : public EquationSizes<DIM,EQN_NS>
{
public:
	enum { SIZE = DIM + 3};	// size of the state vector
	enum { AUXSIZE = 1};		// size of the auxiliary data (per node)

	enum { ID_K = DIM + 2};

	static MGString	NameEqnType()	{ return "RANS_SA"; }
};


//////////////////////////////////////////////////////////////////////
// class EquationSizes - Reynolds Avareged Navier Stokes - k-omega - 2 equations
//////////////////////////////////////////////////////////////////////
template <MGSize DIM>
class EquationSizes<DIM,EQN_RANS_KOMEGA> : public EquationSizes<DIM,EQN_NS>
{
public:
	enum { SIZE = DIM + 4};	// size of the state vector
	enum { AUXSIZE = 1};		// size of the auxiliary data (per node)

	enum { ID_K 	= DIM + 2};
	enum { ID_OMEGA = DIM + 3};

	static MGString	NameEqnType()	{ return "RANS_KOMEGA"; }
};


//////////////////////////////////////////////////////////////////////
// class EquationSizes - Reynolds Avareged Navier Stokes - 2 equation
//////////////////////////////////////////////////////////////////////
template <MGSize DIM>
class EquationSizes<DIM,EQN_RANS_2E> : public EquationSizes<DIM,EQN_RANS_SA>
{
public:
	enum { SIZE = DIM + 4 };	// size of the state vector
	enum { AUXSIZE = 0};		// size of the auxiliary data (per node)

	enum { ID_E = DIM + 3 };

	static MGString	NameEqnType()	{ return "RANS_2E"; }


};




//////////////////////////////////////////////////////////////////////
// class EquationSizes - Euler in rotating frame of reference
//////////////////////////////////////////////////////////////////////
template <MGSize DIM>
class EquationSizes<DIM,EQN_RFR_EULER>
{
public:
	enum { SIZE = DIM + 2};	// size of the state vector
	enum { AUXSIZE = 0};		// size of the auxiliary data (per node)

	enum { ID_RHO = 0};
	enum { ID_P = 1};

	static MGString	NameEqnType()	{ return "RFR_EULER"; }

	template <MGSize I>
	class U
	{
	public:
		enum { ID = I+2};
	};

};



//////////////////////////////////////////////////////////////////////
// class EquationSizes - Navier Stokes in rotating frame of reference
//////////////////////////////////////////////////////////////////////
template <MGSize DIM>
class EquationSizes<DIM,EQN_RFR_NS> : public EquationSizes<DIM,EQN_RFR_EULER>
{
public:
	static MGString	NameEqnType()	{ return "RFR_NS"; }
};







//////////////////////////////////////////////////////////////////////
// class EquationSplitting
//////////////////////////////////////////////////////////////////////
template <MGSize DIM, EQN_TYPE ET>
class EquationSplitting
{
public:
	class SplitingFull
	{
	public:
		typedef BType< EquationSizes<DIM,ET>::SIZE > TBlock;

		enum { BLOCK_NUM = 1};

		template <MGSize ID>
		class Block : public TBlock::template Get<ID>::TMapper
		{
			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::Vec		FVec;
			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::VecGrad	FGradVec;
			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::Mtx		FMtx;
		};

	};
};


//////////////////////////////////////////////////////////////////////
// class EquationSplitting
//  - SA -- specialization for RANS - 1 equation
//////////////////////////////////////////////////////////////////////
template <MGSize DIM>
class EquationSplitting<DIM,EQN_RANS_SA>
{
public:
	typedef typename EquationTypes< DIM, EquationSizes<DIM,EQN_RANS_SA>::SIZE >::Vec		EVec;
	typedef typename EquationTypes< DIM, EquationSizes<DIM,EQN_RANS_SA>::SIZE >::VecGrad	EGradVec;
	typedef typename EquationTypes< DIM, EquationSizes<DIM,EQN_RANS_SA>::SIZE >::Mtx		EMtx;

	class SplitingFull
	{
	public:
		typedef BType< EquationSizes<DIM,EQN_RANS_SA>::SIZE > TBlock;

		enum { BLOCK_NUM = 1};

		template <MGSize ID>
		class Block : public TBlock::template Get<ID>::TMapper
		{
			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::Vec	FVec;
			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::Mtx	FMtx;
		};

	};

	class SplitingFlowTurb
	{
	public:
		typedef BType< DIM+2, BType< EquationSizes<DIM,EQN_RANS_SA>::SIZE-DIM-2 > > TBlock;

		enum { BLOCK_NUM = 2};

		template <MGSize ID>
		class Block : public TBlock::template Get<ID>::TMapper
		{
		public:
			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::Vec	FVec;
			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::Mtx	FMtx;
		};

	};
};


//////////////////////////////////////////////////////////////////////
// class EquationSplitting
//  - k-omega -- specialization for RANS - 2 equation
//////////////////////////////////////////////////////////////////////
template <MGSize DIM>
class EquationSplitting<DIM,EQN_RANS_KOMEGA>
{
public:
	typedef typename EquationTypes< DIM, EquationSizes<DIM,EQN_RANS_KOMEGA>::SIZE >::Vec		EVec;
	typedef typename EquationTypes< DIM, EquationSizes<DIM,EQN_RANS_KOMEGA>::SIZE >::VecGrad	EGradVec;
	typedef typename EquationTypes< DIM, EquationSizes<DIM,EQN_RANS_KOMEGA>::SIZE >::Mtx		EMtx;

	class SplitingFull
	{
	public:
		typedef BType< EquationSizes<DIM,EQN_RANS_KOMEGA>::SIZE > TBlock;

		enum { BLOCK_NUM = 1};

		template <MGSize ID>
		class Block : public TBlock::template Get<ID>::TMapper
		{
			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::Vec	FVec;
			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::Mtx	FMtx;
		};

	};

	class SplitingFlowTurb
	{
	public:
		typedef BType< DIM+2, BType< EquationSizes<DIM,EQN_RANS_KOMEGA>::SIZE-DIM-2 > > TBlock;

		enum { BLOCK_NUM = 2};

		template <MGSize ID>
		class Block : public TBlock::template Get<ID>::TMapper
		{
		public:
			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::Vec	FVec;
			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::Mtx	FMtx;
		};

	};
};


////////////////////////////////////////////////////////////////////////
//// class EquationSplitting
////  - specialization for Reynolds Avareged Navier Stokes - 2 equation
////////////////////////////////////////////////////////////////////////
//template <MGSize DIM>
//class EquationSplitting<DIM,EQN_RANS_KOMEGA>
//{
//	typedef typename EquationTypes< EquationSizes<DIM,EQN_RANS_KOMEGA>::SIZE >::Vec	EVec;
//	typedef typename EquationTypes< EquationSizes<DIM,EQN_RANS_KOMEGA>::SIZE >::Mtx	EMtx;
//
//public:
//	class SplitingFull
//	{
//	public:
//		typedef BType< EquationSizes<DIM,EQN_RANS_KOMEGA>::SIZE > TBlock;
//
//		enum { BLOCK_NUM = 1};
//
//		template <MGSize ID>
//		class Block : public TBlock::template Get<ID>::TMapper
//		{
//			typedef typename EquationTypes< TBlock::template Get<ID>::TMapper::VBSIZE >::Vec	FVec;
//			typedef typename EquationTypes< TBlock::template Get<ID>::TMapper::VBSIZE >::Mtx	FMtx;
//		};
//
//	};
//
//	class SplitingFlowTurb
//	{
//	public:
//		typedef BType< DIM+2, BType< EquationSizes<DIM,EQN_RANS_KOMEGA>::SIZE-DIM-2 > > TBlock;
//
//		enum { BLOCK_NUM = 2};
//
//		template <MGSize ID>
//		class Block : public TBlock::template Get<ID>::TMapper
//		{
//		public:
//			typedef typename EquationTypes< TBlock::template Get<ID>::TMapper::VBSIZE >::Vec	FVec;
//			typedef typename EquationTypes< TBlock::template Get<ID>::TMapper::VBSIZE >::Mtx	FMtx;
//		};
//
//	};
//};




////////////////////////////////////////////////////////////////////////
//// class EquationSplitting
////  - specialization for Reynolds Avareged Navier Stokes - 2 equation
////////////////////////////////////////////////////////////////////////
//template <MGSize DIM>
//class EquationSplitting<DIM,EQN_RANS_2E>
//{
//	typedef typename EquationTypes< DIM, EquationSizes<DIM,EQN_RANS_2E>::SIZE >::Vec	EVec;
//	typedef typename EquationTypes< DIM, EquationSizes<DIM,EQN_RANS_2E>::SIZE >::Mtx	EMtx;
//
//public:
//	class SplitingFull
//	{
//	public:
//		typedef BType< EquationSizes<DIM,EQN_RANS_2E>::SIZE > TBlock;
//
//		enum { BLOCK_NUM = 1};
//
//		template <MGSize ID>
//		class Block : public TBlock::template Get<ID>::TMapper
//		{
//			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::Vec	FVec;
//			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::Mtx	FMtx;
//		};
//
//	};
//
//	class SplitingFlowTurb
//	{
//	public:
//		typedef BType< DIM+2, BType< EquationSizes<DIM,EQN_RANS_2E>::SIZE-DIM-2 > > TBlock;
//
//		enum { BLOCK_NUM = 2};
//
//		template <MGSize ID>
//		class Block : public TBlock::template Get<ID>::TMapper
//		{
//		public:
//			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::Vec	FVec;
//			typedef typename EquationTypes< DIM, TBlock::template Get<ID>::TMapper::VBSIZE >::Mtx	FMtx;
//		};
//
//	};
//};





//////////////////////////////////////////////////////////////////////
// class Eqns
//////////////////////////////////////////////////////////////////////
template <MGSize DIM, EQN_TYPE ET>
class EquationDef : public EquationSizes<DIM,ET>, public EquationSplitting<DIM,ET>
{
public:
	typedef typename EquationTypes< DIM, EquationSizes<DIM,ET>::SIZE >::Vec			FVec;
	typedef typename EquationTypes< DIM, EquationSizes<DIM,ET>::SIZE >::VecGrad		FGradVec;

	typedef typename EquationTypes< DIM, EquationSizes<DIM,ET>::SIZE >::Mtx			FMtx;

	typedef typename EquationTypes< DIM, EquationSizes<DIM,ET>::AUXSIZE >::Vec		AVec;
};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __EQUATION_H__

