#ifndef __MAPPER_H__
#define __MAPPER_H__

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class CopyElem
//////////////////////////////////////////////////////////////////////
template <int BSIZE, int OFFSET, int IND>
class CopyElem
{
public:
	template <class VGLOBAL, class VLOCAL>
	static void LocalToGlobal( VGLOBAL& vg, const VLOCAL& vl)
	{
		vg[IND+OFFSET] = vl[IND];
		CopyElem<BSIZE,OFFSET,IND-1>::LocalToGlobal( vg, vl);
	}

	template <class VGLOBAL, class VLOCAL>
	static void GlobalToLocal( VLOCAL& vl, const VGLOBAL& vg)
	{
		vl[IND] = vg[IND+OFFSET];
		CopyElem<BSIZE,OFFSET,IND-1>::GlobalToLocal( vl, vg);
	}
};

template <int BSIZE, int OFFSET>
class CopyElem<BSIZE,OFFSET,0>
{
public:
	template <class VGLOBAL, class VLOCAL>
	static void LocalToGlobal( VGLOBAL& vg, const VLOCAL& vl)
	{
		vg[OFFSET] = vl[0];
	}

	template <class VGLOBAL, class VLOCAL>
	static void GlobalToLocal( VLOCAL& vl, const VGLOBAL& vg)
	{
		vl[0] = vg[OFFSET];
	}
};


//////////////////////////////////////////////////////////////////////
// class Mapper
//////////////////////////////////////////////////////////////////////
template <int BSIZE, int OFFSET>
class Mapper
{
public:
	enum { VBSIZE = BSIZE };
	enum { VOFFSET = OFFSET };

	template <class VGLOBAL, class VLOCAL>
	static void LocalToGlobal( VGLOBAL& vg, const VLOCAL& vl)
	{
		CopyElem<BSIZE,OFFSET,BSIZE-1>::LocalToGlobal( vg, vl);
	}

	template <class VGLOBAL, class VLOCAL>
	static void GlobalToLocal( VLOCAL& vl, const VGLOBAL& vg)
	{
		CopyElem<BSIZE,OFFSET,BSIZE-1>::GlobalToLocal( vl, vg);
	}
};




//////////////////////////////////////////////////////////////////////
// class Stop
//////////////////////////////////////////////////////////////////////
class Stop
{
public:
	enum { N = 0};
	enum { SIZE = 0};
	enum { TOTSIZE = 0};

	template <int RID>
	class Get
	{
	public:
		enum { RET_ID = 0 };
		enum { RET_SIZE = 0 };
		enum { RET_OFFSET = 0 };
	};
};


//////////////////////////////////////////////////////////////////////
// class BType - used for block definitions (uses Stop)
// usage : BType< 3, BType< 5, < BType< 2 > > > for definition of spliting 10 -> 3 + 5 + 2 
//////////////////////////////////////////////////////////////////////
template <int BSIZE, class RM=Stop>
class BType
{
public:
	enum { SIZE = BSIZE};
	enum { TOTSIZE = RM::SIZE + RM::TOTSIZE};
	enum { N = RM::N + 1};

	template <int RID>
	class Get
	{
		template < bool COND, class TRUE, class FALSE>
		class IF
		{
		public:
			typedef TRUE RETT;
		};

		template < class TRUE, class FALSE>
		class IF< false, TRUE, FALSE>
		{
		public:
			typedef FALSE RETT;
		};

	public:
		enum { RET_ID = (RID > N || RID < 1) ? -1 : (( (N - RID + 1) == N) ? RID : static_cast<int>( RM::template Get<RID-1>::RET_ID+1) ) };
		enum { RET_SIZE = (RID > N || RID < 1) ? -1 : (((N - RID + 1) == N) ? SIZE : static_cast<int>( RM::template Get<RID-1>::RET_SIZE) ) };
		enum { RET_OFFSET = (RID > N || RID < 1) ? -1 : (((N - RID + 1) == N) ? 0 : static_cast<int>( RM::template Get<RID-1>::RET_OFFSET + SIZE) ) };

		typedef typename IF< 
				((N - RID + 1) == N), 
				Mapper< RET_SIZE, RET_OFFSET>, 
				Mapper< RM::template Get<RID-1>::RET_SIZE, RM::template Get<RID-1>::RET_OFFSET + SIZE > 
		>::RETT TMapper;
	};

};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __MAPPER_H__
