#ifndef __INTERFACE_H__
#define __INTERFACE_H__


#include "libcoreio/intffacade.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class InterfInfo
//////////////////////////////////////////////////////////////////////
class InterfInfo
{
public:
	const MGSize&	cLoc() const		{ return mIdLocNode; }
	const MGSize&	cRemProc() const	{ return mIdRemProc; }
	const MGSize&	cRem() const		{ return mIdRemNode; }

	MGSize&			rLoc() 				{ return mIdLocNode; }
	MGSize&			rRemProc()		 	{ return mIdRemProc; }
	MGSize&			rRem() 				{ return mIdRemNode; }

	MGSize	mIdLocNode;
	MGSize	mIdRemProc;
	MGSize	mIdRemNode;
};


//////////////////////////////////////////////////////////////////////
// class Interface
//////////////////////////////////////////////////////////////////////
class Interface : public IO::IntfFacade
{
public:
	Interface() : mNProc(0)		{}

	void 	SetDim( const Dimension& dim)				{ mDim = dim;}

	MGSize				NProc() const					{ return mNProc;}

	MGSize				Size() const					{ return mtabInterf.size(); }
	const InterfInfo&	cInfo( const MGSize& i) const	{ return mtabInterf[i];}

protected:
	virtual Dimension	Dim() const						{ return mDim;}

	virtual void	IOGetName( MGString& name) const	{ name = mName;}
	virtual void	IOSetName( const MGString& name)	{ mName = name;}

	virtual MGSize	IOGetProcNum() const				{ return mNProc;}
	virtual void	IOSetProcNum( const MGSize& n)		{ mNProc = n;}

	virtual MGSize	IOTabIntfSize() const				{ return mtabInterf.size();}
	virtual void	IOTabIntfResize( const MGSize& n)	{ mtabInterf.resize( n);}

	virtual void	IOGetIds( vector<MGSize>& tabid, const MGSize& id) const;
	virtual void	IOSetIds( const vector<MGSize>& tabid, const MGSize& id );


private:

	Dimension	mDim;
	MGSize		mNProc;
	MGString	mName;
	
	vector<InterfInfo>	mtabInterf;

};
//////////////////////////////////////////////////////////////////////




inline void Interface::IOGetIds( vector<MGSize>& tabid, const MGSize& id) const
{
	tabid[0] = mtabInterf[id].mIdLocNode;
	tabid[1] = mtabInterf[id].mIdRemProc;
	tabid[2] = mtabInterf[id].mIdRemNode;
}


inline void Interface::IOSetIds( const vector<MGSize>& tabid, const MGSize& id )
{
	mtabInterf[id].mIdLocNode = tabid[0];
	mtabInterf[id].mIdRemProc = tabid[1];
	mtabInterf[id].mIdRemNode = tabid[2];
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

#endif // __INTERFACE_H__
