
LIST ( APPEND redcore_files
	configconst.h
	equation.cpp
	equation.h
	interface.h
	mapper.h
	processinfo.cpp
	processinfo.h
	tdefs.h 
) 


INCLUDE_DIRECTORIES (
  ${FLOW2_SOURCE_DIR}/${NAME_CORE}
  ${FLOW2_SOURCE_DIR}/${NAME_RED}
)

ADD_LIBRARY ( redcore ${redcore_files} )
