#ifndef __TIMESOLVEREXPLICIT_H__
#define __TIMESOLVEREXPLICIT_H__


#include "libredconvergence/timesolverbase.h"
#include "libextsparse/sparsevector.h"
#include "libredcore/configconst.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class TimeSolverExplicit
//////////////////////////////////////////////////////////////////////
template <MGSize BSIZE>
class TimeSolverExplicit : public TimeSolverBase<BSIZE>
{
public:
	TimeSolverExplicit() : TimeSolverBase<BSIZE>()		{}

	virtual void	Create( const CfgSection* pcfgsec, SpaceSolverFacade<BSIZE>* pssol);
	virtual void	PostCreateCheck() const;
	virtual void	Init();

	virtual void	DoStep( ConvergenceInfo<BSIZE>& info);

private:
	vector<MGFloat>			mtabLocDT;
	Sparse::Vector<BSIZE>	mvecDQ;

	MGFloat		mExplicitCFL;

};
//////////////////////////////////////////////////////////////////////

template <MGSize BSIZE>
void TimeSolverExplicit<BSIZE>::Create( const CfgSection* pcfgsec, SpaceSolverFacade<BSIZE>* pssol)	
{ 
	TimeSolverBase<BSIZE>::Create( pcfgsec, pssol); 
}

template <MGSize BSIZE>
void TimeSolverExplicit<BSIZE>::PostCreateCheck() const
{
	TimeSolverBase<BSIZE>::PostCreateCheck();

	// do check
}

template <MGSize BSIZE>
void TimeSolverExplicit<BSIZE>::Init()	
{
	const MGSize nsol = this->cSpaceSol().SolutionSize();

	this->mvecQ.Resize( nsol);
	mvecDQ.Resize( nsol);
	mtabLocDT.resize( nsol);
}


template <MGSize BSIZE>
void TimeSolverExplicit<BSIZE>::DoStep( ConvergenceInfo<BSIZE>& info)
{
	MGFloat cfl = this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::TimeSol::CFL );
	
	this->rSpaceSol().InitDT( mtabLocDT, 0.01);
	this->rSpaceSol().Residuum( mvecDQ);

	ofstream	of( "_flow2_dump_vectQ.txt");

	this->mvecQ.Dump( of);

	for ( MGSize i=0; i< this->mvecQ.Size(); ++i)
		this->mvecQ[i] -= mtabLocDT[i]*mvecDQ[i]; 

	of << "---" << endl;
	this->mvecQ.Dump( of);

	//THROW_INTERNAL( "STOP");

	typename Sparse::Vector<BSIZE>::BlockVec	vecRes;

	vecRes = Sparse::BlockDot( mvecDQ, mvecDQ );

	for ( MGSize i=0; i<BSIZE; ++i)
	{
		info.mErrDQL2[i] = info.mErrResL2[i] = ::sqrt( vecRes[i] );
		info.mErrDQLinf[i] = info.mErrResLinf[i] = 0.0;
	}

}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __TIMESOLVEREXPLICIT_H__
