#ifndef __CFLMANAGER_H__
#define __CFLMANAGER_H__

#include "libcoresystem/mgdecl.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class CFLManager
//////////////////////////////////////////////////////////////////////
class CFLManager
{
public:
	CFLManager()			{}

	CFLManager( const MGFloat& cfl, const MGFloat& cmax, const MGSize& ngrowth, const MGSize& nstart)
						{ Init( cfl, cmax, ngrowth, nstart);}

	void	Init( const MGFloat& cfl, const MGFloat& cmax, const MGSize& ngrowth, const MGSize& nstart);
	void	Reset();
	void	Push();

	const MGFloat&	cCFL() const	{ return mCFL; }
	const MGSize&	cCount() const	{ return mCount; }
private:
	MGFloat			mCFLstart;
	MGFloat			mCFLmax;
	MGSize			mNGrowth;
	MGSize			mNStart;

	MGSize			mCount;
	MGFloat			mdCFL;
	MGFloat			mCFL;
};


inline void CFLManager::Init(  const MGFloat& cfl, const MGFloat& cmax, const MGSize& ngrowth, const MGSize& nstart)
{
	mNStart = nstart;
	mNGrowth = ngrowth;
	mCFLmax = cmax;
	mCFLstart = cfl;

	mdCFL = ( mCFLmax - mCFLstart) / mNGrowth;
	mCount = 0;
	mCFL = 0.0;
}


inline void CFLManager::Push()			
{ 
	++mCount; 
	if ( mCount > mNStart )
	{
		mCFL+=mdCFL; 
		mCFL = MIN( mCFL, mCFLmax); 
	}
}

inline void CFLManager::Reset()
{
	mCount = 0;
	mCFL = mCFLstart;
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CFLMANAGER_H__
