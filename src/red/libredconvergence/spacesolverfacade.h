#ifndef __SPACESOLVERFACADE_H__
#define __SPACESOLVERFACADE_H__


// PETSc objects def
#ifdef WITH_PETSC
#include <petsc.h>
#include <petscksp.h>
#endif // WITH_PETSC

#ifdef WITH_TRILINOS
#include "Epetra_VbrMatrix.h"
#include "Epetra_BlockMap.h"
#endif // WITH_TRILINOS

#include "libextsparse/sparsevector.h"
#include "libextsparse/sparsematrix.h"




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class SpaceSolverFacade
//////////////////////////////////////////////////////////////////////
template <MGSize BSIZE>
class SpaceSolverFacade
{
public:
	enum { SIZE = BSIZE };

public:
	SpaceSolverFacade()		{}

	virtual MGSize	SolutionSize() const					= 0;
	virtual void	InitDT( vector<MGFloat>& tabdt, const MGFloat& cfl) const	= 0;
	virtual void	JacobianPattern() const					= 0;

	// libsparse
	virtual void	ResizeVct( Sparse::Vector<BSIZE>& vct) const	= 0;
	virtual void	ResizeMtx( Sparse::Matrix<BSIZE>& mtx) const	= 0;

	virtual void	Residuum( Sparse::Vector<BSIZE>& vec )	= 0;
	virtual void	ResJacob( Sparse::Vector<BSIZE>& vec, Sparse::Matrix<BSIZE>& vmtx, const vector<MGFloat>& tabDT) = 0;
	virtual void	ResNumJacob(Sparse::Vector<BSIZE>& vec, Sparse::Matrix<BSIZE>& vmtx, const vector<MGFloat>& tabDT, const bool bcorrdiag = true) = 0;

	virtual void	ResJacobStrongBC( Sparse::Vector<BSIZE>& vec, Sparse::Matrix<BSIZE>& vmtx ) = 0;
	virtual void	ApplyStrongBC( Sparse::Vector<BSIZE>& vec ) = 0;


	// PETSc interface
#ifdef WITH_PETSC
	virtual void	PETScInitOwnedIdRange( pair<MGSize,MGSize>& range, vector<MGSize>& tabg) const
							{ THROW_INTERNAL( "SpaceSolverFacade -- not implemented"); }

	virtual void	PETScResizeVct( Vec& vct) const
							{ THROW_INTERNAL( "SpaceSolverFacade -- not implemented"); }
	virtual void	PETScResizeMtx( Mat& mtx) const
							{ THROW_INTERNAL( "SpaceSolverFacade -- not implemented"); }

	virtual void	PETScResiduum( Vec& vec )
							{ THROW_INTERNAL( "SpaceSolverFacade -- not implemented"); }
	virtual void	PETScResNumJacob( Vec& vec, Mat& vmtx, const vector<MGFloat>& tabDT, const bool bcorrdiag = true)
							{ THROW_INTERNAL( "SpaceSolverFacade -- not implemented"); }

	virtual void	PETScResJacobStrongBC( Vec& vec, Mat& vmtx )
							{ THROW_INTERNAL( "SpaceSolverFacade -- not implemented"); }
	virtual void	PETScApplyStrongBC( Vec& vec )
							{ THROW_INTERNAL( "SpaceSolverFacade -- not implemented"); }
#endif // WITH_PETSC

#ifdef WITH_TRILINOS
    virtual void	TRILINOSInitOwnedIdRange( pair<MGSize,MGSize>& range, vector<MGSize>& tabg) const
    { THROW_INTERNAL( "SpaceSolverFacade -- not implemented"); }
    virtual void	TRILINOSResizeGraph( const Epetra_BlockMap& map, Epetra_CrsGraph*& graph) const
    { THROW_INTERNAL( "SpaceSolverFacade -- not implemented"); }
#endif //WITH_TRILINOS

};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __SPACESOLVERFACADE_H__
