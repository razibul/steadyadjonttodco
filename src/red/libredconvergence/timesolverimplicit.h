#ifndef __TIMESOLVERIMPLICIT_H__
#define __TIMESOLVERIMPLICIT_H__


#include "timesolverbase.h"
#include "libextsparse/sparsevector.h"
#include "libextsparse/sparsematrix.h"

#include "libextsparse/precond.h"
#include "libextsparse/itersolver.h"

#include "libredcore/configconst.h"
#include "libredconvergence/cflmanager.h"


//#define WITH_VIENNACL

#ifdef WITH_VIENNACL

#define VIENNACL_WITH_OPENCL

#include "viennacl/scalar.hpp"
#include "viennacl/vector.hpp"
#include "viennacl/compressed_matrix.hpp"
#include "viennacl/linalg/prod.hpp"
#include "viennacl/linalg/ilu.hpp"
#include "viennacl/linalg/jacobi_precond.hpp"
#include "viennacl/linalg/cg.hpp"
#include "viennacl/linalg/bicgstab.hpp"
#include "viennacl/linalg/gmres.hpp"


template <std::size_t BSIZE, typename SCALARTYPE>
void copy_flow2( const viennacl::vector_base<SCALARTYPE> & gpu_vec, Sparse::Vector<BSIZE,SCALARTYPE> & cpu_vector)
{
	viennacl::copy( gpu_vec.begin(), gpu_vec.end(), &(cpu_vector[0][0]) );
}

template <std::size_t BSIZE, typename SCALARTYPE>
void copy_flow2( const Sparse::Vector<BSIZE,SCALARTYPE> & cpu_vector, viennacl::vector_base<SCALARTYPE> & gpu_vec)
{
	viennacl::copy( &(cpu_vector[0][0]), (&(cpu_vector[cpu_vector.Size()-1][BSIZE-1]))+1, gpu_vec.begin() );
}


template <std::size_t BSIZE, typename SCALARTYPE, unsigned int ALIGNMENT>
void copy_flow2( const Sparse::Matrix<BSIZE,SCALARTYPE> & cpu_matrix, viennacl::compressed_matrix<SCALARTYPE, ALIGNMENT> & gpu_matrix)
{
	std::size_t nonzeros = 0;
	for ( std::size_t irow=0; irow<cpu_matrix.NRows(); ++irow)
		nonzeros += cpu_matrix.RowSize(irow);

	nonzeros *= BSIZE*BSIZE;

	viennacl::backend::typesafe_host_array<unsigned int> row_buffer(gpu_matrix.handle1(), cpu_matrix.NRows()*BSIZE + 1);
	viennacl::backend::typesafe_host_array<unsigned int> col_buffer(gpu_matrix.handle2(), nonzeros);
	std::vector<SCALARTYPE> elements(nonzeros);

	std::size_t row_index  = 0;
	std::size_t data_index = 0;

	for ( std::size_t irow=0; irow<cpu_matrix.NRows(); ++irow)
	{
		for ( std::size_t ibrow=0; ibrow<BSIZE; ++ibrow)
		{
			row_buffer.set(row_index, data_index);
			++row_index;

			for ( std::size_t k=0; k<cpu_matrix.RowSize(irow); ++k)
			{
				const Sparse::Matrix<BSIZE,SCALARTYPE>::BlockMtx& block = cpu_matrix.GetBlock( irow, k);
				for ( std::size_t ibcol=0; ibcol<BSIZE; ++ibcol)
				{
					std::size_t igcol = cpu_matrix.GetColId(irow,k)*BSIZE + ibcol;

					col_buffer.set( data_index, igcol);
					elements[data_index] = block(ibrow,ibcol);
					++data_index;
				}

			}
		
			data_index = viennacl::tools::roundUpToNextMultiple<std::size_t>(data_index, ALIGNMENT); //take care of alignment
		}
	}

	row_buffer.set(row_index, data_index);

	gpu_matrix.set(row_buffer.get(),
		col_buffer.get(),
		&elements[0], 
		cpu_matrix.NRows()*BSIZE,
		cpu_matrix.NRows()*BSIZE,
		nonzeros);
}

#endif //WITH_VIENNACL

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

	
//////////////////////////////////////////////////////////////////////
//	class TimeSolverImplicit
//////////////////////////////////////////////////////////////////////
template <MGSize BSIZE>
class TimeSolverImplicit : public TimeSolverBase<BSIZE>
{
public:
	TimeSolverImplicit() : TimeSolverBase<BSIZE>()		{}

	virtual void	Create( const CfgSection* pcfgsec, SpaceSolverFacade<BSIZE>* pssol);
	virtual void	PostCreateCheck() const;
	virtual void	Init();

	virtual void	DoStep( ConvergenceInfo<BSIZE>& info);
	
	virtual void	BuildJacobiMatrix();
	virtual void	CFLCrash();

	virtual Sparse::Matrix<BSIZE>&	rJacobiMatrix() { return mmtxJac; };
	
private:
	vector<MGFloat>			mtabLocDT;
	Sparse::Vector<BSIZE>	mvecDQ;
	Sparse::Vector<BSIZE>	mvecRes;
	Sparse::Matrix<BSIZE>	mmtxJac;

	CFLManager	mCFLManager;
};
//////////////////////////////////////////////////////////////////////

template <MGSize BSIZE>
void TimeSolverImplicit<BSIZE>::Create( const CfgSection* pcfgsec, SpaceSolverFacade<BSIZE>* pssol)	
{ 
	TimeSolverBase<BSIZE>::Create( pcfgsec, pssol); 
}

template <MGSize BSIZE>
void TimeSolverImplicit<BSIZE>::PostCreateCheck() const
{
	TimeSolverBase<BSIZE>::PostCreateCheck();

	// do check
}

template <MGSize BSIZE>
void TimeSolverImplicit<BSIZE>::Init()	
{
	const MGSize nsol = this->cSpaceSol().SolutionSize();

	this->mvecQ.Resize( nsol);
	mvecDQ.Resize( nsol);
	mvecRes.Resize( nsol);
	mtabLocDT.resize( nsol);

	this->rSpaceSol().ResizeMtx( mmtxJac);

	MGSize nstart = 0;
	if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Executor::TimeSol::NCFLSTART ) )
		nstart = this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::TimeSol::NCFLSTART );


	mCFLManager.Init( this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::TimeSol::CFL),
		this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::TimeSol::MAXCFL ),
		this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::TimeSol::NCFLGROWTH ),
		nstart);

	mCFLManager.Reset();
}

template <MGSize BSIZE>
void TimeSolverImplicit<BSIZE>::CFLCrash()
{
	MGSize nstart = 0;
	if (this->ConfigSect().KeyExists(ConfigStr::Solver::Executor::TimeSol::NCFLSTART))
		nstart = this->ConfigSect().ValueSize(ConfigStr::Solver::Executor::TimeSol::NCFLSTART);

	mCFLManager.Init(this->ConfigSect().ValueFloat(ConfigStr::Solver::Executor::TimeSol::CFL),
		mCFLManager.cCFL(),
		2 * mCFLManager.cCount(), //this->ConfigSect().ValueSize(ConfigStr::Solver::Executor::TimeSol::NCFLGROWTH),
		nstart );

	mCFLManager.Reset();

}

template <MGSize BSIZE>
void TimeSolverImplicit<BSIZE>::BuildJacobiMatrix()
{
	MGFloat cfl = mCFLManager.cCFL();
	this->rSpaceSol().InitDT(mtabLocDT, cfl);

	MGString sjacobtype = ConfigStr::Solver::Executor::TimeSol::JacobType::Value::NUMERICAL;
	if (this->ConfigSect().KeyExists(ConfigStr::Solver::Executor::TimeSol::JacobType::KEY))
		sjacobtype = this->ConfigSect().ValueString(ConfigStr::Solver::Executor::TimeSol::JacobType::KEY);

	// compute residuum & jacobian

	if (sjacobtype == ConfigStr::Solver::Executor::TimeSol::JacobType::Value::NUMERICAL)
		this->rSpaceSol().ResNumJacob(mvecRes, mmtxJac, mtabLocDT,false);
	else
	if (sjacobtype == ConfigStr::Solver::Executor::TimeSol::JacobType::Value::ANALYTICAL)
		this->rSpaceSol().ResJacob(mvecRes, mmtxJac, mtabLocDT);
	else
		THROW_INTERNAL("unknown Jacobian type - " << sjacobtype);

	//this->rSpaceSol().ResJacobStrongBC(mvecRes, mmtxJac); //what about that?

}

template <MGSize BSIZE>
void TimeSolverImplicit<BSIZE>::DoStep( ConvergenceInfo<BSIZE>& info)
{
	//CheckQ( mvecQ);	// checking for non-physical values
	
	
	MGFloat cfl = mCFLManager.cCFL();
	mCFLManager.Push();


	//MGFloat cfl 		= this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::TimeSol::CFL );
	MGSize max_iter 	= this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::TimeSol::LINMAXITER );
	MGSize m 			= this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::TimeSol::LINDKSPACE );
	MGFloat gmres_eps 	= this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::TimeSol::LINMINEPS );

	this->rSpaceSol().InitDT( mtabLocDT, cfl);
	
	MGString sjacobtype = ConfigStr::Solver::Executor::TimeSol::JacobType::Value::NUMERICAL;
	if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Executor::TimeSol::JacobType::KEY ) )
		sjacobtype = this->ConfigSect().ValueString( ConfigStr::Solver::Executor::TimeSol::JacobType::KEY );

	// compute residuum & jacobian

	if ( sjacobtype == ConfigStr::Solver::Executor::TimeSol::JacobType::Value::NUMERICAL )
		this->rSpaceSol().ResNumJacob( mvecRes, mmtxJac, mtabLocDT);
	else
	if ( sjacobtype == ConfigStr::Solver::Executor::TimeSol::JacobType::Value::ANALYTICAL )
		this->rSpaceSol().ResJacob( mvecRes, mmtxJac, mtabLocDT);
	else
		THROW_INTERNAL( "unknown Jacobian type - " << sjacobtype );


	this->rSpaceSol().ResJacobStrongBC( mvecRes, mmtxJac );

	////////////
	for ( MGSize i=0; i<mvecRes.Size(); ++i)
		for ( MGSize j=0; j<BSIZE; ++j)
		{
			if ( ISNAN( mvecRes[i](j) ) )
			{
				mvecRes[i].Write();
				THROW_INTERNAL( "NAN :: i='" << i << "'" << " j='" << j << "'")
			}
		}

	//ofstream of( "_res_dump.txt" );
	//mmtxJac(0,0).Write();
	//mmtxJac(15402,15402).Write();

	//mvecRes.Dump( of);
	//THROW_INTERNAL( "STOP");


	//odat.mErrRes = mvecRes.Norm();
	//odat.mErrResInf = mvecRes.MaxNorm();
	
	for ( MGSize i=0; i< this->mvecQ.Size(); ++i)
	{
		mvecDQ[i].Init( 0.0);
		mvecRes[i] = - mvecRes[i];
	}
	
#ifdef WITH_VIENNACL
	cout << endl << "ViennaCL" << endl;

	viennacl::ocl::set_context_device_type(0, viennacl::ocl::cpu_tag());
		
	//std::cout << viennacl::ocl::current_device().info() << std::endl;

	typedef MGFloat TYPE;
	MGSize nsize = this->mvecQ.Size();
	viennacl::compressed_matrix<TYPE> vcl_compressed_matrix(nsize*BSIZE,nsize*BSIZE);
	viennacl::vector<TYPE> vcl_rhs(nsize*BSIZE); 
	viennacl::vector<TYPE> vcl_result(nsize*BSIZE);

	copy_flow2( mmtxJac, vcl_compressed_matrix );
	copy_flow2( mvecRes, vcl_rhs);

	//viennacl::linalg::ilu0_tag ilu0_config;
	//viennacl::linalg::ilu0_precond< viennacl::compressed_matrix<TYPE> >	vcl_ilut( vcl_compressed_matrix, ilu0_config);

	viennacl::linalg::ilut_tag ilut_config;
	viennacl::linalg::ilut_precond< viennacl::compressed_matrix<TYPE> > vcl_ilut( vcl_compressed_matrix, ilut_config);

	//viennacl::linalg::jacobi_precond< viennacl::compressed_matrix<TYPE> > vcl_jacobi( vcl_compressed_matrix, viennacl::linalg::jacobi_tag());

	//viennacl::linalg::block_ilu_precond< viennacl::compressed_matrix<TYPE>, viennacl::linalg::ilu0_tag> 
	//	vcl_block_ilu0( vcl_compressed_matrix, viennacl::linalg::ilu0_tag(),20 );

	viennacl::linalg::bicgstab_tag custom_bicgstab(gmres_eps, max_iter, m);
	vcl_result = viennacl::linalg::solve(vcl_compressed_matrix, vcl_rhs, custom_bicgstab, vcl_ilut );

	max_iter = custom_bicgstab.iters();
	gmres_eps = custom_bicgstab.error();

	//viennacl::linalg::gmres_tag custom_gmres(gmres_eps, max_iter, m);
	//vcl_result = viennacl::linalg::solve(vcl_compressed_matrix, vcl_rhs, custom_gmres, vcl_ilut );

	//max_iter = custom_gmres.iters();
	//gmres_eps = custom_gmres.error();

	copy_flow2( vcl_result, mvecDQ );

#else
	Sparse::PreILU<BSIZE>	preILU;
	preILU.Init( mmtxJac);
	GMRES< Sparse::Matrix<BSIZE>, Sparse::Vector<BSIZE>,Sparse::PreILU<BSIZE>,SMatrix<201>,SVector<201>,MGFloat >
		( mmtxJac, mvecDQ, mvecRes, preILU, m, max_iter, gmres_eps);

//	Sparse::PreDiag<BSIZE>	preDiag;
//	preDiag.Init( mmtxJac);
//	GMRES< Sparse::Matrix<BSIZE>, Sparse::Vector<BSIZE>,Sparse::PreDiag<BSIZE>,SMatrix<201>,SVector<201>,MGFloat >
//		( mmtxJac, mvecDQ, mvecRes, preDiag, m, max_iter, gmres_eps);

#endif //WITH_VIENNACL

	//FILE *ff=fopen( ProcessInfo::ModifyFileName( "dump_dq.txt" ).c_str(), "wt");
	//mvecDQ.Dump( ff);
	//fclose( ff);

	//odat.mErrDQ = mvecDQ.Norm();
	//odat.mErrDQInf = mvecDQ.MaxNorm();
	//odat.mCFL = cfl;
	//odat.mGMRESeps = gmres_eps;
	//odat.mGMRESiter = max_iter;


	for ( MGSize i=0; i<mvecDQ.Size(); ++i)
		for ( MGSize j=0; j<BSIZE; ++j)
		{
			if ( ISNAN( mvecDQ[i](j) ) )
			{
				mvecDQ[i].Write();


	ofstream ff( "dump_dq.txt");
	mvecDQ.Dump( ff);

				THROW_INTERNAL( "NAN :: i='" << i << "'")
			}
		}

	cout << resetiosflags(ios_base::scientific);
	cout << "cfl = " << cfl << " max_iter = " << max_iter << " eps = " << gmres_eps << endl;

	
	for ( MGSize i=0; i< this->mvecQ.Size(); ++i)
		this->mvecQ[i] += mvecDQ[i];

	this->rSpaceSol().ApplyStrongBC( this->mvecQ );


	typename Sparse::Vector<BSIZE>::BlockVec	vecRes;

	//vecRes = Sparse::BlockDot( mvecDQ, mvecDQ );
	vecRes = Sparse::BlockDot( mvecRes, mvecRes );

	for ( MGSize i=0; i<BSIZE; ++i)
	{
		info.mErrDQL2[i] = info.mErrResL2[i] = ::sqrt( vecRes[i] );
		info.mErrDQLinf[i] = info.mErrResLinf[i] = 0.0;
	}


	for ( MGSize i=0; i<this->mvecQ.Size(); ++i)
		for ( MGSize j=0; j<BSIZE; ++j)
		{
			if ( ISNAN( this->mvecQ[i](j) ) )
			{
				this->mvecQ[i].Write();
				THROW_INTERNAL( "NAN :: i='" << i << "'")
			}
		}
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __TIMESOLVERIMPLICIT_H__
