
#include "freezingpointcollection.h"
#include "libcorecommon/factory.h"

#include "libredcore/configconst.h"
#include "libredcore/processinfo.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



/*
template class FreezingPointCollection<DIM_2D>;
template class FreezingPointCollection<DIM_3D>;*/

void init_FreezingPointCollection()
{
	//2D
	
	static ConcCreator<
		MGString,
		FreezingPointCollection< Geom::DIM_2D >,
		FreezingPointCollection< Geom::DIM_2D >
	> gCreatorFreezingPointCollection2D( "FREEZINGPOINTCOLLECTION_2D" );
	
	//3D
	
	static ConcCreator<
		MGString,
		FreezingPointCollection< Geom::DIM_3D >,
		FreezingPointCollection< Geom::DIM_3D >
	> gCreatorFreezingPointCollection3D( "FREEZINGPOINTCOLLECTION_3D" );
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 