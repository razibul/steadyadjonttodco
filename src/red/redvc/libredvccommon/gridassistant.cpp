#include "gridassistant.h"
#include "libcorecommon/factory.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <Dimension DIM>
void GridAssistant<DIM>::Create(const CfgSection* pcfgsec, TGrid* pgrd, DesignParameterCollection<DIM>* pdesparcoll, FreezingPointCollection<DIM>* pfreezeptcoll)
{
	ConfigBase::Create( pcfgsec);
	mpGrid = pgrd;
	mpDesParColl = pdesparcoll;
	mpFreezePtColl = pfreezeptcoll;

	if (this->ConfigSect().SectionExists(ConfigStr::Solver::AuxData::NAME))
	{
		const CfgSection* pauxcfg = & this->ConfigSect().GetSection(ConfigStr::Solver::AuxData::NAME);
		mAuxFreezing.Create(DIM, pauxcfg);
	}


	if (this->ConfigSect().KeyExists(ConfigStr::Solver::GridAssistant::PERTSIGMA) && this->ConfigSect().KeyExists(ConfigStr::Solver::GridAssistant::PERTMU))
	{
		msigma = this->ConfigSect().ValueFloat(ConfigStr::Solver::GridAssistant::PERTSIGMA);
		mmu = this->ConfigSect().ValueFloat(ConfigStr::Solver::GridAssistant::PERTMU);
	}
	else
		THROW_INTERNAL("GridAssistant<DIM>::Create() -- Gaussian distribution parameters not specified");

	mscale = 1.;
	if (this->ConfigSect().KeyExists(ConfigStr::Solver::GridAssistant::PERTSCALE))
		mscale = this->ConfigSect().ValueFloat(ConfigStr::Solver::GridAssistant::PERTSCALE);

	switch (DIM)
	{
	case DIM_3D:
		mdofdir.rZ() = this->ConfigSect().ValueFloat(ConfigStr::Solver::GridAssistant::PERTDIRZ);
	case DIM_2D:
		mdofdir.rY() = this->ConfigSect().ValueFloat(ConfigStr::Solver::GridAssistant::PERTDIRY);
	case DIM_1D:
		mdofdir.rX() = this->ConfigSect().ValueFloat(ConfigStr::Solver::GridAssistant::PERTDIRX);
	}
	if (mdofdir.module() != 1.0)
		mdofdir /= mdofdir.module();

};

template <Dimension DIM>
void GridAssistant<DIM>::PostCreateCheck() const
{
	ConfigBase::PostCreateCheck();

	if (mAuxFreezing.Size() != 0 )
		mAuxFreezing.PostCreateCheck();

};

template <Dimension DIM>
void GridAssistant<DIM>::Init()
{
	if (mAuxFreezing.ConfigSect().KeyExists( ConfigStr::Solver::AuxData::FNAME) )
	{
		mAuxFreezing.Resize(mpGrid->SizeNodeTab());
		mAuxFreezing.Init();
	}

	//TODO: initial mesh perturbation is using first design param as morphing point. 
	//REMEMBER: it has been implemented only for verification of FD accuracy for adjoint computation.
	if (this->ConfigSect().KeyExists(ConfigStr::Solver::GridAssistant::PERTINIT) && (this->ConfigSect().ValueBool(ConfigStr::Solver::GridAssistant::PERTINIT) == true))
	{
		vector< GVec > perturb(mpGrid->SizeNodeTab());
		if (!this->ConfigSect().KeyExists(ConfigStr::Solver::GridAssistant::PERTINITVAL))
			THROW_INTERNAL("GridAssistant<DIM>::Init() -- Initial perturbation value not specified !!!");

		MGFloat h = this->ConfigSect().ValueFloat(ConfigStr::Solver::GridAssistant::PERTINITVAL);
		GVec core = (*mpDesParColl)[0].cNode().cPos();
		ComputePerturbation(core, perturb);
		for (MGSize i = 0; i < perturb.size(); i++)
			perturb[i] *= h;

		FixNodes(perturb);
		Perturbate(perturb);
	}

	
}; 

template <Dimension DIM>
void GridAssistant<DIM>::FixNodesBySum(vector<GVec>& vec ) const
{
	//works only when freezed nodes aren't close to each other

	//vector<MGFloat>	value;	
	GVec temp_val;
	vector<GVec> value;

	for (MGSize i = 0; i < mpFreezePtColl->Size(); i++)
	{
		temp_val = vec[ (*mpFreezePtColl)[i].cOwnerIndx() ];
		
		value.push_back(temp_val);
	}
	
	for ( MGSize i=0; i < vec.size(); i++)
	{
		const GVec pos = mpGrid->cNode(i).cPos();
		for (MGSize j = 0; j < mpFreezePtColl->Size(); j++)
		{
			const MGFloat dist = ComputePerturbation( (*mpFreezePtColl)[j].cPos() , pos , false);
			const GVec tmpvec = value[j] * dist  ;

			vec[i] -= tmpvec;
		}
		
	}
	
}

template <Dimension DIM>
void GridAssistant<DIM>::FixNodes(vector<GVec>& vec) const
{
	//FixNodesBySum(vec);
	
	if ( mAuxFreezing.Size() != 0 )
		FixNodesByAux( vec);

}

template <Dimension DIM>
void GridAssistant<DIM>::FixNodesByAux(vector<GVec>& vec) const
{
	ASSERT(vec.size() == mpGrid->SizeNodeTab());
	for (MGSize i = 0; i < vec.size(); i++)
	{
		const MGFloat valaux = mAuxFreezing[i](0);
		const MGFloat scale = 1.;
		vec[i] = vec[i] * (1. - scale * exp( - valaux*valaux ) );
	}

}

template <Dimension DIM>
void GridAssistant<DIM>::RevertToInitial()
{
	ASSERT(mpGrid->mtabNodeInitial.size() == mpGrid->mtabNode.size());
	ASSERT(mpGrid->mtabBNodeInitial.size() == mpGrid->mtabBNode.size());
	
	for (MGSize i = 0; i < mpGrid->mtabNodeInitial.size(); i++)
		mpGrid->mtabNode[i] = mpGrid->mtabNodeInitial[i];
	
	for (MGSize i = 0; i < mpGrid->mtabBNodeInitial.size(); i++)
		mpGrid->mtabBNode[i] = mpGrid->mtabBNodeInitial[i];

}

template <Dimension DIM>
void GridAssistant<DIM>::ComputePerturbation(const GVec& core, vector<GVec>& perturb) const
{
	perturb.resize( mpGrid->mtabNode.size() );

	for (MGSize i = 0; i < perturb.size(); i++)
	{
		const GVec node = mpGrid->cNode(i).cPos();
		const MGFloat dist = ComputePerturbation(core, node,true);

		perturb[i] = dist*mdofdir;
	}


}


template <Dimension DIM>
MGFloat GridAssistant<DIM>::ComputePerturbation(const GVec& core, const GVec& node, bool bmodule) const
{
	MGFloat dist = (core - node).module();
	////dist = 0.1*exp(-6 * dist*dist);
	//const MGFloat sigma = 5;
	//const MGFloat coeff = 1;
	MGFloat module = 1.;
	if (bmodule)
	{
		module = mscale / (msigma * sqrt(2 * PI));
	}
	
	dist = module * exp( -(dist - mmu)*(dist - mmu) / (2 * msigma*msigma) );
	return dist;
}

template <Dimension DIM>
void GridAssistant<DIM>::Perturbate(const vector<GVec>& perturb) const
{
	for (MGSize i = 0; i < mpGrid->SizeNodeTab(); i++)
	{
		mpGrid->mtabNode[i].rPos() += perturb[i];
	}

	mpGrid->InitBNodes();
	mpGrid->InitNodeVolume();	
	mpGrid->InitBFaceCells();
	mpGrid->CheckBFaceVn();
}

template <Dimension DIM>
void GridAssistant<DIM>::Unperturbate(const vector<GVec>& perturb) const
{
	for (MGSize i = 0; i < mpGrid->SizeNodeTab(); i++)
	{
		mpGrid->mtabNode[i].rPos() -= perturb[i];
	}

	mpGrid->InitBNodes();
	mpGrid->InitNodeVolume();
	mpGrid->InitBFaceCells();
	mpGrid->CheckBFaceVn();
}

void init_GridAssistant()
{
	static ConcCreator<
		MGString,
		GridAssistant< Geom::DIM_2D >,
		GridAssistant< Geom::DIM_2D >
	> gCreatorGridAssistant2D ( "GRIDASSISTANT_2D" );
	
	static ConcCreator<
		MGString,
		GridAssistant< Geom::DIM_3D >,
		GridAssistant< Geom::DIM_3D >
	> gCreatorGridAssistant3D ( "GRIDASSISTANT_3D" );
	
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 