#ifndef __SURFANALYZERBASE_H__
#define __SURFANALYZERBASE_H__


#include "libcoresystem/mgdecl.h" 
#include "redvc/libredvccommon/assistantbase.h"
#include "libredphysics/ranssaequations.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
// class SurfAnalyzer Base
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET>
class SurfAnalyzerBase : public AssistantBase<DIM,ET>
{
	typedef EquationDef<DIM,ET> EqDef;

	enum { EQSIZE = EqDef::SIZE };
	enum { AUXSIZE = EqDef::AUXSIZE };

	typedef Vect<DIM>				GVec;
	typedef typename EqDef::FVec	FVec;
	typedef typename EqDef::FMtx	FMtx;
	typedef SVector<EQSIZE,GVec>	FGradVec;


	typedef SVector<DIM>			FSubVec;
	typedef SMatrix<DIM>			FSubMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>	CFCell;
	typedef CFlowBFace<DIM,EQSIZE,AUXSIZE>	CFBFace;

public:

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,EQN_RANS_SA>* pphys, Data<DIM,EQN_RANS_SA>* pdata);
	virtual void	PostCreateCheck() const;
	virtual void	Init();
	virtual bool	Do();

protected:

	void	ExportTEC( const MGString& fname);


private:
	FVec	mRefVar;
	GVec 	gvecGFf;
	GVec 	gvecGFp;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __SURFANALYZERBASE_H__
