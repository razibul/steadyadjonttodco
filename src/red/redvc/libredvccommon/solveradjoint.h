#ifndef __SOLVER_H__
#define __SOLVER_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreconfig/config.h"
#include "redvc/libredvccommon/solverbase.h"
#include "libredcore/equation.h"

#include "libredphysics/physics.h"
#include "redvc/libredvccommon/dataadjoint.h"
#include "redvc/libredvccommon/executorbase.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


using namespace Geom;


//////////////////////////////////////////////////////////////////////
// Solver
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, EQN_TYPE ET>
class SolverAdjoint : public SolverBase
{
public:
	SolverAdjoint()	{}
	virtual ~SolverAdjoint();

	virtual void	Create( const CfgSection* pcfgsec);
	virtual void	PostCreateCheck() const;
	virtual void	Init();

	virtual void	Solve();
	virtual void	Solve(const bool& returnitercount = false, MGInt* itercount = 0);

	DataAdjoint<DIM,ET>&			rData()			{ return mData; }
	const DataAdjoint<DIM, ET>&	cData()	const	{ return mData; }
	
	Physics<DIM,ET>&		rPhysics()		{ return mPhysics; }
	const Physics<DIM,ET>&	cPhysics() const{ return mPhysics; }
protected:
	Physics<DIM,ET>		mPhysics;
	DataAdjoint<DIM,ET>	mData;

	bool	mbWriteTec;
	bool	mbWriteTecSurf;

	vector< ExecutorBase<DIM,ET>* >	mtabExec;
};
//////////////////////////////////////////////////////////////////////


template <Dimension DIM, EQN_TYPE ET>
SolverAdjoint<DIM,ET>::~SolverAdjoint()	
{
	for ( typename vector< ExecutorBase<DIM,ET> * >::iterator itr=mtabExec.begin(); itr!=mtabExec.end(); ++itr)
		if ( *itr != 0 )
			delete *itr;
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __SOLVER_H__
