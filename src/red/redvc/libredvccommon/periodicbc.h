#ifndef __PERIODICBC_H__
#define __PERIODICBC_H__

#include "libcorecommon/key.h"

#include "periodicbcbase.h"
//#include "periodicbcmap.h"

#include "redvc/libredvcgrid/gridsimplex.h"
//#include "grid.h"

#include "libredcore/equation.h"
#include "libredphysics/bc.h"

#include "redvc/libredvccommon/solution.h"

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_intersection.h"

#include "libcorecommon/progressbar.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


	//////////////////////////////////////////////////////////////////////
	// class PeriodicBCCalc
	//////////////////////////////////////////////////////////////////////
	template<Dimension DIM, EQN_TYPE ET>
	class PeriodicBCCalc : public PeriodicBCBase<DIM, ET>
	{
	public:
		enum { ESIZE = EquationDef<DIM, ET>::SIZE };

		typedef GridSimplex<DIM>			TGrid;
		typedef EquationDef<DIM, ET>		EqDef;
		typedef typename EqDef::FVec		FVec;

		typedef KeyData<MGSize, PeriodicKeyData<DIM> >	PeriodicKey;
		typedef vector<PeriodicKey>						ColPeriodic;

		void Init(const TGrid* grid, const Solution<DIM, ET>* sol, const CfgSection* pphysect)
		{
			mpsol = sol;
			mpGrid = grid;
			this->ReadConfig(pphysect);
			InitPeriodicMap(pphysect);
		}

		const PeriodicKeyData<DIM>&		cPeriodicKey(const MGSize& id) const;


		virtual void GetPeriodicVar(const MGSize& id, FVec& v) const;
		virtual void UpdateSolution() 	{}
	private:
		void InitPeriodicMap(const CfgSection* pphysect);
		void FindMapping(const MGSize& surfid, const MGSize& targetsurfid, const Vect<DIM>&	vct, const MGFloat& angle = 0.);

		ColPeriodic							mcolperiodic;
		const TGrid*						mpGrid;
		const Solution<DIM, ET>*			mpsol;
	};


	template<Dimension DIM, EQN_TYPE ET>
	void PeriodicBCCalc<DIM, ET >::InitPeriodicMap(const CfgSection* pphysect)
	{
		for (MGSize i = 0; i < this->mparams.size(); ++i)
		{
			switch (this->mparams[i].type)
			{
			case PRD_TRANS:
			{
				FindMapping(this->mparams[i].src, this->mparams[i].trg, this->mparams[i].vct);
				Vect<DIM> v(this->mparams[i].vct);
				v *= -1.;
				FindMapping(this->mparams[i].trg, this->mparams[i].src, v);
				break;
			}
			case PRD_ANGLE:
				THROW_INTERNAL("Angle periodic bc not implemented.");
				break;
			case PRD_NONE:
				//THROW_INTERNAL("Angle periodic bc not implemented.");
				break;
			default:
				THROW_INTERNAL("Unknown periodic bc type.");
				break;
			}
		}

		//MGSize prdsize = pphysect->GetSectionCount(ConfigStr::Solver::Physics::PERIODIC::NAME);

		//for (MGSize i = 0; i < prdsize; ++i)
		//{
		//	const CfgSection& secprd = pphysect->GetSection(ConfigStr::Solver::Physics::PERIODIC::NAME, i);
		//	MGSize srcid = secprd.ValueSize(ConfigStr::Solver::Physics::PERIODIC::SRC_ID);
		//	MGSize trgid = secprd.ValueSize(ConfigStr::Solver::Physics::PERIODIC::TRG_ID);
		//	MGString type = secprd.ValueString(ConfigStr::Solver::Physics::PERIODIC::Type::KEY);

		//	PrdBCType prdtype = StrToPrd(type);
		//	switch (prdtype)
		//	{
		//	case PRD_TRANS:
		//	{
		//		MGFloat trans[] = {
		//			secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::VCT_X),
		//			secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::VCT_Y),
		//			secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::VCT_Z)
		//		};
		//		Vect<DIM> vct;
		//		for (MGSize j = 0; j < DIM; ++j)
		//		{
		//			vct.rX(j) = trans[j];
		//		}

		//		FindMapping(srcid, trgid, vct);
		//		vct *= -1.;
		//		FindMapping(trgid, srcid, vct);
		//		break;
		//	}
		//	case PRD_ANGLE:
		//		THROW_INTERNAL("Angle periodic bc not implemented.");
		//		break;
		//	case PRD_NONE:
		//		//THROW_INTERNAL("Angle periodic bc not implemented.");
		//		break;
		//	default:
		//		THROW_INTERNAL("Unknown periodic bc type.");
		//		break;
		//	}
		//}

		sort(mcolperiodic.begin(), mcolperiodic.end());
		typename ColPeriodic::iterator itr = unique(mcolperiodic.begin(), mcolperiodic.end());
		mcolperiodic.erase(itr, mcolperiodic.end());
	}

	template<Dimension DIM, EQN_TYPE ET>
	void PeriodicBCCalc<DIM, ET >::FindMapping(const MGSize& surfid, const MGSize& targetsurfid, const Vect<DIM>&	vct, const MGFloat& angle)
	{
		typedef HFGeom::GVector<MGFloat, DIM> Vector;

		ProgressBar bar(40);
		bar.Init(mpGrid->SizeBFaceTab());
		bar.Start();
		for (MGSize inx = 0; inx < mpGrid->SizeBFaceTab(); ++inx, ++bar)
		{
			//for (typename vector< BFace<DIM, DIM> >::const_iterator citr = mpGrid->c mtabBFace.begin();
			//	citr < mtabBFace.end();
			//	++citr)
			//{
			if (mpGrid->cBFace(inx).cSurfId() == surfid)
			{
				for (MGSize trginx = 0; trginx < mpGrid->SizeBFaceTab(); ++trginx)
				{
					if (mpGrid->cBFace(trginx).cSurfId() == targetsurfid)
					{
						Vector tabpos[DIM];
						for (MGSize i = 0; i < DIM; ++i)
						{
							const MGSize& trgnodeid = mpGrid->cBFace(trginx).cId(i);
							for (MGSize j = 0; j < DIM; ++j)
							{
								tabpos[i].rX(j) = mpGrid->cNode(trgnodeid).cPos().cX(j);
							}
						}

						MGFloat maxedgesize = 0.;
						for (MGSize i = 0; i < DIM-1; ++i)
						{
							MGFloat len = 0.;
							Vector tmp(tabpos[i] - tabpos[(i + 1) % DIM]);
							for (MGSize ii = 0; ii < DIM; ++ii)
							{
								len += tmp.cX(ii)*tmp.cX(ii);
							}
							len = sqrt(len);
							if (len > maxedgesize)
							{
								maxedgesize = len;
							}
						}

						HFGeom::SubSimplex<MGFloat, DIM - 1, DIM> subb((typename HFGeom::SubSimplex<MGFloat, DIM - 1, DIM>::ARRAY(tabpos)));

						for (MGSize i = 0; i < mpGrid->cBFace(inx).Size(); ++i)
						{
							Vector pos;
							const MGSize& nodeid = mpGrid->cBFace(inx).cId(i);

							for (MGSize j = 0; j < DIM; j++)
							{
								pos.rX(j) = mpGrid->cNode(nodeid).cPos().cX(j) + vct.cX(j);
							}

							HFGeom::SubSimplex<MGFloat, 0, DIM> suba((typename  HFGeom::SubSimplex<MGFloat, 0, DIM>::ARRAY(pos)));

							HFGeom::IntersectionProx<MGFloat, MGFloat, 0, DIM - 1, DIM> inter(suba, subb);
							inter.Execute();

							MGFloat	tabB[DIM];
							inter.GetBBaryCoords(tabB);

							bool bin = true;
							for (MGSize i = 0; i<DIM; ++i)
							{
								tabB[i] /= inter.cDet();
								//cout << tabB[i] << "\t";
								if (tabB[i] < -maxedgesize*1e-4)
								{
									bin = false;
								}
							}
							//cout << endl;
							if (bin)
							{
								PeriodicKey key;
								key.first = nodeid;
								//key.second.first = trginx;
								key.second.mcellid = trginx;
								for (MGSize j = 0; j < DIM; ++j)
								{
									//key.second.second.rElem(j) = tabB[j];
									key.second.mtabcoord.rElem(j) = tabB[j];
								}

								mcolperiodic.push_back(key);
							}
						}
					}
				}
			}
		}
		bar.Finish();
	}

	template<Dimension DIM, EQN_TYPE ET>
	const PeriodicKeyData<DIM>& PeriodicBCCalc<DIM, ET >::cPeriodicKey(const MGSize& id) const
	{
		typename ColPeriodic::const_iterator it = lower_bound(mcolperiodic.begin(), mcolperiodic.end(), PeriodicKey(id));
		if ((it == mcolperiodic.end()) || (it->first != id))
		{
			THROW_INTERNAL("Error in PeriodicBCCalc::cPeriodicKey, nodeid could not be found.");
		}
		return it->second;
	}

	template<Dimension DIM, EQN_TYPE ET>
	void PeriodicBCCalc<DIM, ET >::GetPeriodicVar(const MGSize& id, FVec& v) const
	{
		for (MGSize i = 0; i < ESIZE; ++i)
		{
			v[i] = 0.;
		}
		const PeriodicKeyData<DIM>& key = this->cPeriodicKey(id);

		for (MGSize i = 0; i < DIM; ++i)
		{
			MGSize nodeid = this->mpGrid->cBFace(key.mcellid).cId(i);

			for (MGSize j = 0; j < ESIZE; ++j)
			{
				v[j] += (*mpsol)[nodeid][j] * key.mtabcoord.cElem(i);
			}
		}
	}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __PERIODICBC_H__
