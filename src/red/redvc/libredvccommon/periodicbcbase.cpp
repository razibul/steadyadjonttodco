#include "periodicbcbase.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

const char	STRPRD_TRANS[] = "TRANS";
const char	STRPRD_ANGLE[] = "ANGLE";

PrdBCType	StrToPrd(const MGString& str)
{
	if (strcmp(str.c_str(), STRPRD_TRANS) == 0)
		return PRD_TRANS;
	else if (strcmp(str.c_str(), STRPRD_ANGLE) == 0)
		return PRD_ANGLE;
	else
		return PRD_NONE;
}

MGString BCToStr(const PrdBCType& prd)
{
	switch (prd)
	{
	case PRD_TRANS:
		return STRPRD_TRANS;

	case PRD_ANGLE:
		return STRPRD_ANGLE;

	default:
		THROW_INTERNAL("Unknown Periodic BC");

	}
	return "";
}
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 