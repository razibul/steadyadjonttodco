
#include "execadjoint.h"
#include "libcorecommon/factory.h"

#include "libextsparse/precond.h"
#include "libextsparse/itersolver.h"

#include "libcoreio/store.h"
#include "libcoreio/writetec.h"
#include "libcoreio/writetecsurf.h"
#include "libcoreio/writesol.h"
#include "redvc/libredvcgrid/gridsimplex.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
void ExecAdjoint<DIM,ET,MAPPER>::Create(const CfgSection* pcfgsec, Physics<DIM, ET>* pphys, Data<DIM, ET>* pdata)
{
	ExecAdjointBase<DIM, ET,MAPPER>::Create(pcfgsec, pphys, pdata);
}
// template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
// void ExecAdjoint<DIM, ET, MAPPER>::Create(const CfgSection* pcfgsec, Physics<DIM, ET>* pphys, Data<DIM, ET>* pdata, const CfgSection* ptimsol, const CfgSection* pspcsol)
// {
// 	
// 	ExecAdjointBase<DIM, ET, MAPPER>::Create(pcfgsec, pphys, pdata,ptimsol,pspcsol);
// 
// }

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void ExecAdjoint<DIM, ET, MAPPER>::PostCreateCheck() const
{
	ExecAdjointBase<DIM, ET,MAPPER>::PostCreateCheck();
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void ExecAdjoint<DIM, ET, MAPPER>::Init()
{
	
	this->mpSpaceSol->Init();
	this->mpTimeSol->Init();

	this->mpTimeSol->rLocalSolution().Resize(this->cData().cSolution().Size());

	const MGSize nsol = this->cData().cSolution().Size();
	mvecFuncGradSol.Resize( nsol);
	mvecAdj.Resize(nsol);
	mvecvecAdj.clear();
	mvecvecAdj.resize( (this->mpObjectiveColl)->Size() );
	//mvecvecAdj.resize( (*mpObjectiveColl).Size() );

	//mvecAdj.Resize(nsol);
	//mvecTangent.Resize(nsol);
	//mvecFuncGradSol.Resize(nsol);
	//mvecFuncGradDesign.Resize(nsol);
	//mvecResGradDesign.Resize(nsol);

	this->mh = 1e-6;
	//mvecObjectiveGrad.resize( this->cData().cDesignParamCollection().Size());

}
template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
bool ExecAdjoint<DIM, ET, MAPPER>::SolveHessian()
{
	// FD approach for construction of hessian matrix 
	cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << endl;
	cout << "@@ MPIPETScExecAdjoint::SolveHessian @@" << endl;
	cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << endl;

	const MGSize desparsize = this->cDataAdj().cDesignParamCollection().Size();
	vector< vector<MGFloat> > mtxHess;
	mtxHess.resize(desparsize);
	for (MGSize i = 0; i < mtxHess.size(); i++)
		mtxHess[i].resize(desparsize, 0.0);


	for (int i = 0; i < desparsize; i++)
	{
		cout << "@@ design parameter : " << i + 1 << " / " << desparsize << endl;


		
		/*
		1. initialize (build jacobi mtx and rhs for adjoint)
		2. build rhs for tangent eqn (compute ResGradDesign w.r.t. to i-th despar)
		3. solve tangent eqn.
		4. build rhs for adjoint eqn (compute residual 1st adjoint eqn)
		4a -- perturbate mesh
		4b -- perturbate solution
		4c -- build matrix
		4d -- build rhs for adjoint
		4e -- compute mtx*vecadjoint + ObjectiveGradSol;
		4f -- unperturbate solution
		4g -- unperturbate mesh

		4h -- unperturbate mesh
		4i -- unperturbate solution
		4j -- build matrix
		4k -- build rhs for adjoint
		4l -- compute mtx*vecadjoint + ObjectiveGradSol;
		4m -- perturbate mesh
		4n -- perturbate solution
		4o -- central fd: (4e - 4l)/(2*h)
		4p -- scale by -1.

		5. solve adjoint eqn
		6. for each despar
		
		compute hessian projection on \beta_p
		6a. compute ResGradDesign(
		6b. compute backward

		*/








	} // end of loop over design params (for each row in hessian mtx)

	return false;
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
bool ExecAdjoint<DIM, ET, MAPPER>::SolveFinal()
{
	//testing mesh perturbation and freezing for dlr_f6 case

	for (MGSize i = 0; i < this->cDataAdj().cDesignParamCollection().Size(); i++)
	{
		vector<GVec> perturb;
		GVec vec = this->cDataAdj().cDesignParamCollection()[i].cNode().cPos();
		
		this->rDataAdj().rGridAssistant().ComputePerturbation(vec, perturb);

		//for (MGSize z = 0; z < perturb.size(); z++)
		//{
		//	this->rDataAdj().rSolAdjoint()[z](0) = perturb[z].cZ();
		//}

		//ostringstream oss1;
		//oss1 << "_test_bef_perturb_" << i << ".dat";
		IO::WriteTECSurf writer(this->cData().cGrid(), &this->cDataAdj().cSolAdjoint());
		//writer.DoWrite(oss1.str().c_str());

		this->rDataAdj().rGridAssistant().FixNodes(perturb);

		this->rDataAdj().rGridAssistant().Perturbate(perturb);
		//for (MGSize z = 0; z < perturb.size(); z++)
		//{
		//	this->rDataAdj().rSolAdjoint()[z](0) = perturb[z].cZ();
		//}

//	writes perturbed mesh to disk 
// 		ostringstream oss2;
// 		oss2 << "_test_perturb_" << i << ".dat";
// 		writer.DoWrite( oss2.str().c_str() );
		
		this->rDataAdj().rGridAssistant().Unperturbate(perturb);

	}
	
	
// 	return false;
	//end testing mesh perturbation and freezing for dlr_f6 case

	//SolveHessian();
	//return 0;

	ResiduumGradSol(); //building jacobi matrix - same for each objective

	for (MGSize i = 0; i < (this->mpObjectiveColl)->Size(); i++)
	{
		(*this->mpObjectiveColl)[i]->ComputeGradSolution( this->cData().cSolution() );

		if ( (*this->mpObjectiveColl)[i]->cDoAdjoint() )
		{
			cout << "Adjoint for objective " << i+1 << " of " << this->mpObjectiveColl->Size() << " : " << (*this->mpObjectiveColl)[i]->cDescription() << endl;
			
			Sparse::Vector<ESIZE> vecFuncGradSolESIZE;
			vecFuncGradSolESIZE.Resize( this->cData().cSolution().Size() );
			vecFuncGradSolESIZE = (*this->mpObjectiveColl)[i]->rGradSolution();
			for (MGSize j = 0; j < mvecFuncGradSol.Size(); j++)
				MAPPER::GlobalToLocal(mvecFuncGradSol[j], vecFuncGradSolESIZE[j]);

			//for (MGSize j = 0; j < mvecFuncGradSol.Size(); j++)
			//{
			//	for (MGSize k = 0; k < ESIZE; k++)
			//	{
			//		this->rData().rSolution()[j][k] = vecFuncGradSolESIZE[j][k];
			//	}
			//	
			//}

			//IO::WriteTECSurf writefunc(this->cData().cGrid(), &this->cData().cSolution());
			//writefunc.DoWrite("_funcgradsol.dat");

			////return 0;

			SolveAdjointEqn();
			mvecvecAdj[i] =  mvecAdj ;
		}

	}



	ResiduumGradDesign();

	for (MGSize i = 0; i < (this->mpObjectiveColl)->Size(); i++)
		(*(this->mpObjectiveColl))[i]->ComputeObjective( this->cData().cSolution() );
	
	//SolveTangentEqn();

	LocalToGlobal();
	

	for (MGSize i = 0; i < mvecvecAdj.size(); i++)
	{
		const bool mbWriteAdjTec = (*(this->mpObjectiveColl))[i]->cIfWriteAdjTec();
		const bool mbWriteAdjTecSurf = (*(this->mpObjectiveColl))[i]->cIfWriteAdjTecSurf();
		
		if ( mbWriteAdjTec || mbWriteAdjTecSurf )
		{
			for (MGSize j = 0; j < mvecvecAdj[i].Size(); ++j)
				MAPPER::LocalToGlobal(this->rDataAdj().rSolAdjoint()[j], mvecvecAdj[i][j]);

			if ( mbWriteAdjTec ) 
			{
				IO::WriteTEC writeAdj(this->cData().cGrid(), &this->cDataAdj().cSolAdjoint());
				ostringstream oss;
				oss << "_adjoint_" << i << ".dat";
				writeAdj.DoWrite( oss.str().c_str() );
			}
			if ( mbWriteAdjTecSurf )
			{
				IO::WriteTECSurf writeAdjSurf(this->cData().cGrid(), &this->cDataAdj().cSolAdjoint());
				ostringstream oss_surf;
				oss_surf << "_adjoint_surf_" << i << ".dat";
				writeAdjSurf.DoWrite( oss_surf.str().c_str() );
			}
		}
	}
	
	
	for (MGSize i = 0; i < mvecvecAdj.size(); i++)
	{		
		
	for (MGSize j = 0; j < mvecvecAdj[i].Size(); ++j)
		MAPPER::LocalToGlobal(this->rDataAdj().rSolAdjoint()[j], mvecvecAdj[i][j]);

		bool bBIN=false;
		IO::WriteSOL writeAdjSol(this->cDataAdj().cSolAdjoint());
		ostringstream oss;
		oss << "_adjoint_" << i << ".sol";
		writeAdjSol.DoWrite( oss.str().c_str(),bBIN );
	}	
	
	MGString sfilename = "_out_grad.txt";
	if (this->ConfigSect().KeyExists( ConfigStr::Solver::Executor::Adjoint::OUTFNAME))
		sfilename = this->ConfigSect().ValueString(ConfigStr::Solver::Executor::Adjoint::OUTFNAME);


	ofstream ofgrad( sfilename.c_str() );
	if (!ofgrad)
		THROW_INTERNAL("Adjoint<DIM,ET,MAPPER>::DoFinal() -- can not open file for writing objective gradients");
	
	ofgrad << this->cDataAdj().cDesignParamCollection().Size() << endl;
	ofgrad << (this->mpObjectiveColl->Size());
	
	ofgrad.setf(ios::scientific);
	for (MGSize i = 0; i < (*this->mpObjectiveColl).Size(); i++)
	{
		if ((*(this->mpObjectiveColl))[i]->cDoAdjoint())
			ofgrad << " " << (*this->mpObjectiveColl)[i]->cDescription();
	}
	ofgrad << endl;

	for (MGSize i = 0; i < this->cDataAdj().cDesignParamCollection().Size(); i++)
	{
		for (MGSize j = 0; j < this->mpObjectiveColl->Size(); j++)
		{
			if ((*(this->mpObjectiveColl))[j]->cDoAdjoint())
				ofgrad << setw(24) << setprecision(16) << (*this->mpObjectiveColl)[j]->cGradObjective()[i] << " ";
		}
		ofgrad << endl;
		
	}

	ofgrad.close();

	MGString sfname_obj = "_out_objective.txt";
	ofstream ofobj(sfname_obj.c_str());
	for (MGSize i = 0; i < (*this->mpObjectiveColl).Size(); i++)
	{
		ofgrad << (*this->mpObjectiveColl)[i]->cDescription() << "\t" << (*this->mpObjectiveColl)[i]->cObjective() << endl;
	}

	ofobj.close();

	//IO::WriteTECSurf writeFuncGradSol(this->cData().cGrid(), &this->cData().cSolFuncGradSol());
	//writeFuncGradSol.DoWrite("_funcgradsol.dat");

	//IO::WriteTEC writeAdj(this->cData().cGrid(), &this->cData().cSolAdjoint());
	//writeAdj.DoWrite("_adj.dat");
	//cout << "writing adjoint - done" << endl;

	//IO::WriteTEC writeFuncGradDesign(this->cData().cGrid(), &this->cData().cSolFuncGradDesign());
	//writeFuncGradDesign.DoWrite("_funcgraddesign.dat");
	//cout << "writing func grad design - done" << endl;
	//
	//IO::WriteTEC writeTangentTec(this->cData().cGrid(), &this->cData().cSolFuncGradDesign());
	//writeTangentTec.DoWrite("_tangent.dat");
	//cout << "writing tangent tec - done" << endl;

	//IO::WriteSOL writeTangent( this->cData().cSolTangent());
	//writeTangent.DoWrite("_tangent.sol",false);
	//cout << "writing tangent sol - done" << endl;




	SolveHessian();
	return false;
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void ExecAdjoint<DIM, ET, MAPPER>::ResiduumGradSol()
{
	this->mpTimeSol->BuildJacobiMatrix();
	mmtxResGradSol = this->mpTimeSol->rJacobiMatrix();
	
	mmtxResGradSolTrans = mmtxResGradSol;
	mmtxResGradSolTrans.Transpose();

	mpreILU.Init(mmtxResGradSolTrans);
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void ExecAdjoint<DIM, ET, MAPPER>::SolveAdjointEqn()
{

	MGSize max_iter = (*this->mpcfgsectimsol).ValueSize(ConfigStr::Solver::Executor::TimeSol::LINMAXITER);
	MGSize m = (*this->mpcfgsectimsol).ValueSize(ConfigStr::Solver::Executor::TimeSol::LINDKSPACE);
	MGFloat gmres_eps = (*this->mpcfgsectimsol).ValueFloat(ConfigStr::Solver::Executor::TimeSol::LINMINEPS);

	for (MGSize i = 0; i < mvecAdj.Size(); i++)
	{
		mvecAdj[i].Init(0.0);
		mvecFuncGradSol[i] = -mvecFuncGradSol[i];
	}

	GMRES< Sparse::Matrix<BSIZE>, Sparse::Vector<BSIZE>, Sparse::PreILU<BSIZE>, SMatrix<201>, SVector<201>, MGFloat >
		(mmtxResGradSolTrans, mvecAdj, mvecFuncGradSol, mpreILU, m, max_iter, gmres_eps);

	cout << resetiosflags(ios_base::scientific);
	cout << "max_iter = " << max_iter << " eps = " << gmres_eps << endl;

	for (MGSize i = 0; i<this->mvecAdj.Size(); ++i)
		for (MGSize j = 0; j<BSIZE; ++j)
		{
			if (ISNAN(this->mvecAdj[i](j)))
			{
				this->mvecAdj[i].Write();
				THROW_INTERNAL("NAN :: i='" << i << "'")
			}
		}

	for (MGSize i = 0; i < mvecAdj.Size(); i++)
		mvecFuncGradSol[i] = -mvecFuncGradSol[i];


}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void ExecAdjoint<DIM, ET, MAPPER>::LocalToGlobal()
{
	//ASSERT(cData().cSolAdjoint().Size() == mvecAdj.Size());
	//ASSERT(cData().cSolFuncGradDesign().Size() == mvecFuncGradDesign.Size());
	
	//for (MGSize i = 0; i < mvecAdj.Size(); ++i)
	//{
	//	MAPPER::LocalToGlobal(this->rData().rSolAdjoint()[i], mvecAdj[i]);
	//	MAPPER::LocalToGlobal(this->rData().rSolFuncGradDesign()[i], mvecFuncGradDesign[i]);
	//}

	//this->cData().cSolAdjoint().CheckNAN();
	//this->cData().cSolFuncGradDesign().CheckNAN();

}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void ExecAdjoint<DIM, ET, MAPPER>::ResiduumGradDesign()
{
	//1. Compute residuals on initial mesh
	//2. Modify grid ( add perturbation to mesh )
	// locate indx of first and last point on selected surface
	// select N points on surface
	// compute perturbation
	// add perturbation
	//3. Compute perturbated residuals
	//4. Modify grid ( remove perturbation )
	//4. Compute residuals gradient wrt mesh perturbation
	
	Sparse::Vector<BSIZE> vecRes;
	Sparse::Vector<BSIZE> vecResPert;
	Sparse::Vector<BSIZE> vecResPertBack;
	vecRes.Resize(mvecAdj.Size());
	vecResPert.Resize(mvecAdj.Size());
	vecResPertBack.Resize(mvecAdj.Size());


	this->mpSpaceSol->Residuum(vecRes);
	for (MGSize i = 0; i < this->cDataAdj().cDesignParamCollection().Size(); i++)
	{
		MGFloat h = this->mpSpaceSol->ConfigSect().ValueFloat(ConfigStr::Solver::Executor::SpaceSol::NUMDIFF_EPS);
		vector<GVec> perturb;

		GVec vec = this->cDataAdj().cDesignParamCollection()[i].cNode().cPos();
		this->rDataAdj().rGridAssistant().ComputePerturbation(vec, perturb);
		for (MGSize j = 0; j < perturb.size(); j++)
			perturb[j] *= h;

		this->cDataAdj().cGridAssistant().FixNodes( perturb );

		this->rDataAdj().rGridAssistant().Perturbate(perturb);
		this->mpSpaceSol->Residuum(vecResPert);
		this->rDataAdj().rGridAssistant().Unperturbate(perturb);

		this->rDataAdj().rGridAssistant().Unperturbate(perturb);
		this->mpSpaceSol->Residuum(vecResPertBack);
		this->rDataAdj().rGridAssistant().Perturbate(perturb);

		for (MGSize j = 0; j < vecResPert.Size(); j++) //todo: use proper fd scheme w.r.t. config file
		{
			//vecResPert[i] = (vecResPert[i] - vecRes[i]) / h; // forward scheme
			vecResPert[j] = (vecResPert[j] - vecResPertBack[j]) / (2 * h); // central scheme
		}

		Sparse::Vector<BSIZE>	row; row.Resize(1);	
		for (MGSize z = 0; z < mvecvecAdj.size(); z++)
		{
			MGFloat objgrad = 0;
			for (MGSize j = 0; j < vecResPert.Size(); j++)
			{
				row[0] = vecResPert[j] * mvecvecAdj[z][j];
				for (MGSize k = 0; k < BSIZE; k++)
					objgrad += row[0](k);

			}
			(*(this->mpObjectiveColl))[z]->rGradObjective()[i] = objgrad;

		}
		

	}

}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
MGFloat ExecAdjoint<DIM, ET, MAPPER>::EvaluateFunctional(const Solution<DIM,ET>& vec, const MGSize& i)
{
	return 0;
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void ExecAdjoint<DIM, ET, MAPPER>::SolveTangentEqn()
{
//	MGSize max_iter = (*mpcfgsectimsol).ValueSize(ConfigStr::Solver::Executor::TimeSol::LINMAXITER);
//	MGSize m = (*mpcfgsectimsol).ValueSize(ConfigStr::Solver::Executor::TimeSol::LINDKSPACE);
//	MGFloat gmres_eps = (*mpcfgsectimsol).ValueFloat(ConfigStr::Solver::Executor::TimeSol::LINMINEPS);


//	Sparse::PreILU<BSIZE>	preILU;
//	preILU.Init(mmtxResGradSol);
//	GMRES< Sparse::Matrix<BSIZE>, Sparse::Vector<BSIZE>, Sparse::PreILU<BSIZE>, SMatrix<201>, SVector<201>, MGFloat >
//		(mmtxResGradSol, mvecTangent, mvecResGradDesign, preILU, m, max_iter, gmres_eps);

//	MGFloat a = 0;
//	for (MGSize i = 0; i < mvecTangent.Size(); i++)
//		for (MGSize j = 0; j < BSIZE; j++)
//			a += mvecFuncGradSol[i](j) * mvecTangent[i](j);


//	cout << "gradient from tangent --------> " << a << endl;
}

void init_Adjoint()
{
	//////////////////////////////////////////////////////////////////////
	// 2D
	//------------------------------------------------------------------//
	// Euler
	static ConcCreator<
		MGString,
		ExecAdjoint<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D, EQN_EULER>::SplitingFull::Block<1> >,
		ExecutorBase<DIM_2D, EQN_EULER >
	>	gCreatorAdjointEuler2D("EXECUTOR_2D_EULER_ADJOINT");
	
	//------------------------------------------------------------------//
	// NS
	static ConcCreator<
		MGString,
		ExecAdjoint<Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D, EQN_NS>::SplitingFull::Block<1> >,
		ExecutorBase<DIM_2D, EQN_NS >
	>	gCreatorAdjointNS2D("EXECUTOR_2D_NS_ADJOINT");

	//------------------------------------------------------------------//
	// RANS_SA frozen turbulence
	static ConcCreator<
		MGString,
		ExecAdjoint<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D, EQN_RANS_SA>::SplitingFlowTurb::Block<1> >,
		ExecutorBase<DIM_2D, EQN_RANS_SA >
	>	gCreatorAdjointRANSSA2D("EXECUTOR_2D_RANS_SA_ADJOINT");
	
	//////////////////////////////////////////////////////////////////////
	// 3D
	//------------------------------------------------------------------//
	// Euler
	static ConcCreator<
		MGString,
		ExecAdjoint<Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D, EQN_EULER>::SplitingFull::Block<1> >,
		ExecutorBase<DIM_3D, EQN_EULER >
	>	gCreatorAdjointEuler3D("EXECUTOR_3D_EULER_ADJOINT");

	//------------------------------------------------------------------//
	// NS
	static ConcCreator<
		MGString,
		ExecAdjoint<Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D, EQN_NS>::SplitingFull::Block<1> >,
		ExecutorBase<DIM_3D, EQN_NS >
	>	gCreatorAdjointNS3D("EXECUTOR_3D_NS_ADJOINT");

	//------------------------------------------------------------------//
	// RANS_SA frozen turbulence
	//static ConcCreator<
	//	MGString,
	//	ExecAdjoint<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D, EQN_RANS_SA>::SplitingFlowTurb::Block<1> >,
	//	ExecutorBase<DIM_3D, EQN_RANS_SA >
	//>	gCreatorAdjointRANSSA3D("EXECUTOR_3D_RANS_SA_ADJOINT");

}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
