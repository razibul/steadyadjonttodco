#ifndef __DATAADJOINT_H__
#define __DATAADJOINT_H__


#include "data.h"

#include "redvc/libredvccommon/gridassistant.h"
#include "redvc/libredvccommon/designparametercollection.h"
#include "redvc/libredvccommon/objectivecollection.h"
#include "redvc/libredvccommon/freezingpointcollection.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class DataAdjoint
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET>
class DataAdjoint : public Data<DIM,ET>
{
public:
	DataAdjoint() : mpFreezingPointsColl(NULL), mpGridAssistant(NULL) {};
	
	~DataAdjoint() 
	{ 
		if ( mpFreezingPointsColl )	delete mpFreezingPointsColl; 
		if ( mpGridAssistant )		delete mpGridAssistant;
	};
	
	virtual void Create(const CfgSection* pgrdcfg, const CfgSection* psolcfg, const CfgSection* pauxcfg, const CfgSection* pdesparcfg, const CfgSection* pobjectivecfg, const CfgSection* pfreezingpointscfg, const CfgSection* pgrdassistcfg, const Physics< DIM, ET >* pphys);
	virtual void Create(const CfgSection* pgrdcfg, const CfgSection* psolcfg, const CfgSection* pauxcfg, const Physics< DIM, ET >* pphys);
	virtual void PostCreateCheck() const;
	virtual void Init();
	
	const Solution<DIM, ET>&	cSolAdjoint() const		{ return mSolAdjoint; }
	Solution<DIM, ET>&			rSolAdjoint()			{ return mSolAdjoint; }

	//const Solution<DIM, ET>&    cSolFuncGradDesign() const	{ return mSolFuncGradDesign; }
	//Solution<DIM, ET>&			rSolFuncGradDesign()		{ return mSolFuncGradDesign; }

	const DesignParameterCollection<DIM>&	cDesignParamCollection() const	{ return mDesignParamColl; }
	DesignParameterCollection<DIM>&			rDesignParamCollection()		{ return mDesignParamColl; }

	const ObjectiveCollection<DIM,ET>&			cObjectiveCollection() const	{ return mObjectiveColl; }
	ObjectiveCollection<DIM,ET>&				rObjectiveCollection()			{ return mObjectiveColl; }

	//const FreezingPointCollection<DIM>&	cFreezingPointCollection() const 	{ return *mpFreezingPointsColl; }
	//FreezingPointCollection<DIM>&			rFreezingPointCollection() 			{ return mFreezingPointsColl; }
	
	const GridAssistant<DIM>	cGridAssistant() const	{ return *mpGridAssistant; }
	GridAssistant<DIM>			rGridAssistant() 		{ return *mpGridAssistant; }
	
private:
	Solution<DIM,ET>		mSolAdjoint;
	Solution<DIM,ET>		mSolFuncGradDesign;
	DesignParameterCollection<DIM>	mDesignParamColl;
	ObjectiveCollection<DIM,ET>		mObjectiveColl;
	FreezingPointCollection<DIM>*	mpFreezingPointsColl;
	GridAssistant<DIM>*		mpGridAssistant;
	
	
};
template <Geom::Dimension DIM, EQN_TYPE ET>
inline void DataAdjoint<DIM, ET>::Create(const CfgSection* pgrdcfg, const CfgSection* psolcfg, const CfgSection* pauxcfg, const Physics< DIM, ET >* pphys)
{
 	Data<DIM,ET>::Create(pgrdcfg,psolcfg,pauxcfg,pphys);
}

template <Geom::Dimension DIM, EQN_TYPE ET>
inline void DataAdjoint<DIM, ET>::Create(const CfgSection* pgrdcfg, const CfgSection* psolcfg, const CfgSection* pauxcfg, const CfgSection* pdesparcfg, const CfgSection* pobjectivecfg, const CfgSection* pfreezingpointscfg, const CfgSection* pgrdassistcfg, const Physics<DIM, ET>* pphys)
{
 	Data<DIM,ET>::Create(pgrdcfg, psolcfg,pauxcfg,pphys);
	
	mSolAdjoint.Create(psolcfg, this->mpPhysics);
	mSolFuncGradDesign.Create(psolcfg, this->mpPhysics);
	mDesignParamColl.Create( pdesparcfg , & this->mGrid );
	mObjectiveColl.Create(pobjectivecfg, & this->mGrid, mDesignParamColl);	

	MGString sdim = Geom::DimToStr( DIM);
	MGString sparallelprefix = "";
	if ( ProcessInfo::IsParallel() )
		sparallelprefix = "MPI_";
	MGString sfreezeptcollkey = sparallelprefix + "FREEZINGPOINTCOLLECTION_" + sdim ;
	cout << "creating '" << sfreezeptcollkey << "'" << endl;
	mpFreezingPointsColl = Singleton< Factory< MGString, Creator<FreezingPointCollection<DIM> > > >::GetInstance()->GetCreator( sfreezeptcollkey)->Create();
	mpFreezingPointsColl->Create(pfreezingpointscfg, & this->mGrid);
	
	
	MGString sgridassistkey = sparallelprefix + "GRIDASSISTANT_" + sdim;
	mpGridAssistant = Singleton< Factory< MGString, Creator<GridAssistant<DIM> > > >::GetInstance()->GetCreator( sgridassistkey)->Create();
	mpGridAssistant->Create( pgrdassistcfg, & this->mGrid, &mDesignParamColl, mpFreezingPointsColl);
}


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void DataAdjoint<DIM,ET>::PostCreateCheck() const
{ 
	Data<DIM,ET>::PostCreateCheck();
	mSolAdjoint.PostCreateCheck();
	mSolFuncGradDesign.PostCreateCheck();
	mDesignParamColl.PostCreateCheck();
	mObjectiveColl.PostCreateCheck();
	mpFreezingPointsColl->PostCreateCheck();
	mpGridAssistant->PostCreateCheck();
	
	if ( ! mpFreezingPointsColl )
		THROW_INTERNAL( "Data<DIM,ET>::PostCreateCheck() -- failed : mpFreezingPointsColl is NULL" );
	if ( ! mpGridAssistant)
		THROW_INTERNAL( "Data<DIM,ET>::PostCreateCheck() -- failed : mpGridAssistant is NULL" );
	
	
}


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void DataAdjoint<DIM,ET>::Init()
{
	Data<DIM,ET>::Init();
	mSolAdjoint.Resize( this->mGrid.SizeNodeTab());
	mSolFuncGradDesign.Resize(this->mGrid.SizeNodeTab());
	
	///////////////////////////
	// design parameter collection
	mDesignParamColl.Init();
	// objective collection
	mObjectiveColl.Init();
	// freezing points collection
	mpFreezingPointsColl->Init();
	mpGridAssistant->Init();
}



























//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif //__DATAADJOINT_H__