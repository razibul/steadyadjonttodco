#ifndef __ADJOINT_H__
#define __ADJOINT_H__

#include "libcoresystem/mgdecl.h" 
#include "redvc/libredvccommon/assistantbase.h"
#include "libextsparse/sparsevector.h"

#include "libredconvergence/timesolverbase.h"
#include "libredconvergence/timesolverimplicit.h"
#include "redvc/libredvccommon/spacesolverbase.h"
#include "libredconvergence/spacesolverfacade.h"

#include "execadjointbase.h"
#include "objectivecollection.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
class ExecAdjoint : public ExecAdjointBase<DIM, ET,MAPPER>
{
public:
	typedef EquationDef<DIM, ET> EqDef;

	enum { ESIZE = EqDef::SIZE };
	enum { ASIZE = EqDef::AUXSIZE };
	enum { BSIZE = MAPPER::VBSIZE };

	typedef CFlowCell<DIM, ESIZE, ASIZE>		CFCell;
	typedef CFlowBFace<DIM, ESIZE, ASIZE>		CFBFace;
	
	typedef typename EquationDef<DIM, ET>::FVec	FVec;
	typedef Geom::Vect<DIM>			GVec;

	ExecAdjoint() {};
	virtual ~ExecAdjoint() {};

	virtual void	Create(const CfgSection* pcfgsec, Physics<DIM, ET>* pphys, Data<DIM, ET>* pdata);
// 	virtual void	Create(const CfgSection* pcfgsec, Physics<DIM, ET>* pphys, Data<DIM, ET>* pdata, const CfgSection* pcfgsectimsol, const CfgSection* pcfgsecspacesol);
	virtual void	PostCreateCheck() const;
	virtual void	Init();
	virtual bool	SolveStep( ConvergenceInfo<ESIZE>& info) { return false; };
	virtual bool	SolveFinal();
	virtual bool	SolveHessian();

protected:
	

private:

	void	ResiduumGradSol();
	void	SolveAdjointEqn();
	void	SolveTangentEqn();

	void	ResiduumGradDesign();
	//void	ComputeMeshPerturbation( const GVec& core, const MGFloat& h, vector<GVec>& perturb );
	MGFloat	EvaluateFunctional(const Solution<DIM,ET>& vec, const MGSize& i);
	void	LocalToGlobal();
	GVec	mDir;

	Sparse::Matrix<BSIZE>	mmtxResGradSol;		//Jacobi Matrix
	Sparse::Matrix<BSIZE>	mmtxResGradSolTrans;//Jacobi Matrix Transposed
	Sparse::PreILU<BSIZE>	mpreILU; //precond. for Jacobi transposed matrix

	vector< Sparse::Vector<BSIZE> >	mvecvecAdj;
	Sparse::Vector<BSIZE>	mvecAdj;
	Sparse::Vector<BSIZE>	mvecFuncGradSol;
	
	//Sparse::Vector<BSIZE>	mvecTangent;
	//Sparse::Vector<BSIZE>	mvecAdj;			// adjoint vector
	//Sparse::Vector<BSIZE>	mvecFuncGradSol;	//gradient of functional w.r.t. solution 
	//Sparse::Vector<BSIZE>	mvecFuncGradDesign;
	//Sparse::Vector<BSIZE>	mvecResGradDesign;
	
	MGFloat		mObjective;
	//vector<MGFloat>		mvecObjectiveGrad;
	//ObjectiveCollection<DIM,ET>*	mpObjectiveColl;

	



	//MGFloat mh = 1e-9;

};
	
	
	
	
	
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



#endif //__ADJOINT_H__
