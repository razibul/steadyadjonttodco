#include "spacesolver.h"
#include "libcorecommon/factory.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void SpaceSolver<DIM,ET,MAPPER>::Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata)
{
	SpaceSolverBase<DIM,ET,MAPPER>::Create( pcfgsec, pphys, pdata);

	MGString sdim = Geom::DimToStr( DIM);
	//MGString seqtype = ::EquationToStr( ET);
	MGString seqtype = EquationDef<DIM,ET>::NameEqnType();

	MGString sschemetype = this->ConfigSect().ValueString( ConfigStr::Solver::Executor::SpaceSol::Type::KEY );
	//MGString sfuntype = this->ConfigSect().ValueString( ConfigStr::Solver::Executor::SpaceSol::FuncType::KEY );

	if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Executor::SpaceSol::NUMDIFF_EPS ) )
		mRelEpsilon = this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::SpaceSol::NUMDIFF_EPS );


	//MGString solkey = "FLOWFUNC_" + sdim + "_" + seqtype + "_" + sschemetype + "_" + sfuntype;
	//cout << "creating '" << solkey << "'" << endl;

	//mpFunc = Singleton< Factory< MGString,Creator< FlowFunc<DIM,ET,BSIZE> > > >::GetInstance()->GetCreator( solkey )->Create();



	const CfgSection* pffunc = & this->ConfigSect().GetSection( ConfigStr::Solver::Executor::SpaceSol::FlowFunc::NAME );

	MGString sfuntype = pffunc->ValueString( ConfigStr::Solver::Executor::SpaceSol::FlowFunc::FuncType::KEY );
	MGString funckey = "FLOWFUNC_" + sdim + "_" + seqtype + "_" + sschemetype + "_" + sfuntype;
	cout << "creating '" << funckey << "'" << endl;

	mpFunc = Singleton< Factory< MGString,Creator< FlowFunc<DIM,ET,BSIZE> > > >::GetInstance()->GetCreator( funckey )->Create();
	mpFunc->Create( pffunc);

	if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Executor::SpaceSol::SMOOTH_DT ) )
		mIterSmoothDT = this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::SpaceSol::SMOOTH_DT );
	else
		mIterSmoothDT = 0;

	cout << "DT smoothing iter = " << mIterSmoothDT << endl;
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void SpaceSolver<DIM,ET,MAPPER>::InitDT( vector<MGFloat>& tabDT, const MGFloat& cfl) const
{
	//vector<MGFloat>	tabdiff( tabDT.size() );
	vector< pair<MGFloat,MGSize> >	tabh;

	MGFloat	vol, v;
	CGeomCell<DIM>	cgcell;

	//tabdiff.assign( tabdiff.size(), 0.0);
	tabDT.assign( tabDT.size(), 0.0);
	tabh.resize( tabDT.size(), pair<MGFloat,MGSize>(0.,0) );


	ASSERT( tabDT.size() == cData().cSolution().Size() );

	CFCell	fcell;
	CFBFace	fbface;

	// grid cells
	for ( MGSize i=0; i< this->cData().cGrid().SizeCellTab(); ++i)
	{
		cgcell = this->cData().cGrid().cCell( i);
		vol = cgcell.Volume() / cgcell.Size();
		//rad = cgcell.InRadius();

		IS_INFNAN_THROW( 1./vol, "");

		if ( vol <= 0.0)
		{
			cout << " problem with cell volume...  id = " << i << " vol = " << vol << endl;
			cout << cgcell.cId(0) << " ";
			cout << cgcell.cId(1) << " ";
			cout << cgcell.cId(2) << " ";
			cout << cgcell.cId(3) << endl;
		}

		//for ( MGSize k=0; k<cgcell.Size(); ++k)
		//{
		//	//if ( cgcell.cId(k) == 4195)
		//	//	cout << "4195 !!!" << endl;

		//	tabDT[cgcell.cId(k)] += vol;

		//	//v = this->cData().cGrid().cCell( i).Vn(k).module();
		//	//tabdiff[cgcell.cId(k)] += v*v /(DIM*DIM*vol*cgcell.Size());
		//}

		MGFloat cellh = 0;
		for ( MGSize k=0; k<cgcell.Size(); ++k)
		{
			MGFloat a = this->cData().cGrid().cCell( i).Vn(k).module();
			MGFloat h = vol / a;

			if ( k==0 || h < cellh )
				cellh = h;
		}

		for ( MGSize k=0; k<cgcell.Size(); ++k)
		{
			tabDT[cgcell.cId(k)] += vol;
			tabh[ cgcell.cId(k) ].first		+= cellh;
			tabh[ cgcell.cId(k) ].second	+= 1;
		}
	}


	const MGFloat min_LV = 1.0e-8;

	//ofstream of( "dt_l.txt");
	for ( MGSize i=0; i<tabDT.size(); ++i)
	{
		vol = tabDT[i];
		
		//MGFloat lold = ::pow( vol, 1.0/DIM);
		MGFloat l = 2.0 * DIM * tabh[i].first / tabh[i].second;

		//MGFloat l = ::fabs( 2*rad );;
		v = this->cPhysics().MaxSpeed( EVec( this->cData().cSolution()[i]) );

		if ( v < ZERO )
		{
			cout << setprecision(17);
			cout << "i = " << i << endl;
			cout << "vol = " << vol << endl;
			cout << "l   = " << l << endl;
			cout << "v   = " << v << endl;

			this->cData().cGrid().cNode(i).cPos().Write();
			THROW_INTERNAL( "max speed < ZERO");
		}

		tabDT[i] = max( l/v, min_LV );

		tabDT[i] *= cfl / vol;

		if ( ISNAN(tabDT[i]) )
			tabDT[i] = cfl * l / vol / 1000.;

		if ( ISNAN(tabDT[i]) || ISINF(tabDT[i]) )
		{
			cout << setprecision(17);
			cout << "i = " << i << endl;
			cout << "vol = " << vol << endl;
			cout << "l   = " << l << endl;
			cout << "v   = " << v << endl;

			this->cData().cGrid().cNode(i).cPos().Write();
		}

		//of << setw(10) << i << " " << lold << " " << l << " " << vol  << " " << v << endl;

		IS_INFNAN_THROW( tabDT[i], "");
	}

	// flow TODOs:
	// TODO :: calc tabDT according to global time step
	// TODO :: correct tabDT according to local preconditioner used in calc.


	////// SMOOTHING
	//if ( mSmoothDT)
	for ( MGSize iter=0; iter<mIterSmoothDT; ++iter)
	{
		tabh.resize( tabDT.size(), pair<MGFloat,MGSize>(0.,0) );
		//tabh.assign( tabh.size(), pair<MGFloat,MGSize>(0.,0) );

		for ( MGSize i=0; i<tabDT.size(); ++i)
			tabh[i] = pair<MGFloat,MGSize>(0.,0);

		for ( MGSize i=0; i< this->cData().cGrid().SizeCellTab(); ++i)
		{
			cgcell = this->cData().cGrid().cCell( i);

			for ( MGSize j=0; j<cgcell.Size(); ++j)
				for ( MGSize k=0; k<cgcell.Size(); ++k)
				{
					tabh[ cgcell.cId(j) ].first		+= tabDT[cgcell.cId(k)];
					tabh[ cgcell.cId(j) ].second	+= 1;
				}
		}

		MGFloat cof = 0.0;
		for ( MGSize i=0; i<tabDT.size(); ++i)
		{
			tabDT[i] = cof*tabDT[i] + (1.0-cof) * (tabh[i].first / tabh[i].second);
		}
	}
}



template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void SpaceSolver<DIM,ET,MAPPER>::Residuum( Sparse::Vector<BSIZE>& vec )
{
	CFCell	cfcell;
	CFBFace	cfbface;

	BVec	tabres[DIM+1];

	//typename EquationDef<DIM,ET>::FVec	tabres[DIM+1];


	ofstream	of( "_dump_res_vec.txt");


	if ( this->cData().cGrid().SizeNodeTab() != vec.Size() || this->cData().cGrid().SizeNodeTab() != this->cData().cSolution().Size() )
		THROW_INTERNAL( "Incompatible sizes of state vectors in SpaceSolver<DIM>::Residuum(...)");

	for ( MGSize i=0; i<vec.Size(); ++i)
		vec[i].Init( 0.0);

	// applying boundary conditions
	for ( MGSize i=0; i<this->cData().cGrid().SizeBFaceTab(); ++i)
	{
		this->GetFBFace( cfbface, i);
		//cout << i << " - " << cfbface.cId(0) << " " << cfbface.cId(1) << endl;

		ResiduumBFace( tabres, cfbface, false);


		for ( MGSize k=0; k<cfbface.Size(); ++k)
			vec[ cfbface.cId(k) ] += Sparse::BlockVector<BSIZE>( tabres[k] );
	}

	of << " -- after bc\n";
	vec.Dump( of);

	// cell residuals
	for ( MGSize i=0; i<this->cData().cGrid().SizeCellTab(); ++i)
	{
		this->GetFCell( cfcell, i);

		cfcell.CalcVn();

		ResiduumCell( tabres, cfcell, false);


		for ( MGSize k=0; k<cfcell.Size(); ++k)
			vec[ cfcell.cId(k) ] += Sparse::BlockVector<BSIZE>( tabres[k] );
	}


// 	for ( MGSize j=0; j<vec.Size(); ++j)
// 	{
// 		BMtx  mtxWQ, mtxWV, mtxVZ, mtxZQ;
// 		EqnEuler<DIM>::CJacobWV( mtxWV, EVec( this->cData().cSolution()[j]) );
// 		EqnEuler<DIM>::CJacobVZ( mtxVZ, EVec( this->cData().cSolution()[j]) );
// 		EqnEuler<DIM>::CJacobZQ( mtxZQ, EVec( this->cData().cSolution()[j]) );
// 
// 		mtxWQ = mtxWV * mtxVZ * mtxZQ;
// 
// 		BMtx  mtxQW, mtxVW, mtxZV, mtxQZ;
// 		EqnEuler<DIM>::CJacobVW( mtxVW, EVec( this->cData().cSolution()[j]) );
// 		EqnEuler<DIM>::CJacobZV( mtxZV, EVec( this->cData().cSolution()[j]) );
// 		EqnEuler<DIM>::CJacobQZ( mtxQZ, EVec( this->cData().cSolution()[j]) );
// 
// 		mtxQW = mtxQZ * mtxZV * mtxVW;
// 
// 		BMtx mtxPI;
// 		EqnEuler<DIM>::PrecLLR( mtxPI, EVec( this->cData().cSolution()[j]), 1. , 1. );
// 
// 		//mtxPI.Invert();
// 		mtxPI = mtxQW * mtxPI * mtxWQ;
// 
// 		vec[j] = Sparse::BlockVector<BSIZE>( mtxPI * BVec( vec[j]) );
// 
// 	}

	//of << " -- final\n";
	//vec.Dump( of);
	//THROW_INTERNAL( "STOP");

}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void SpaceSolver<DIM,ET,MAPPER>::ResJacob( Sparse::Vector<BSIZE>& vec, Sparse::Matrix<BSIZE>& mtx, const vector<MGFloat>& tabDT)
{
	BVec	tabres[DIM+1];
	BMtx	mtxres[DIM+1][DIM+1];

	CFCell	cfcell;
	CFBFace	cfbface;


	mtx.Reset();
	for ( MGSize i=0; i<vec.Size(); ++i)
		vec[i].Init( 0.0);


	if ( this->cData().cGrid().SizeNodeTab() != vec.Size() || this->cData().cGrid().SizeNodeTab() != this->cData().cSolution().Size() )
		THROW_INTERNAL( "Incompatible sizes of state vectors in SpaceSolver<DIM>::Residuum(...)");

	// boundary conditions
	for ( MGSize i=0; i<this->cData().cGrid().SizeBFaceTab(); ++i)
	{
		this->GetFBFace( cfbface, i);

		ResAJacobianBFace( tabres, mtxres, cfbface); //numerical jacobian


		for ( MGSize k=0; k<cfbface.Size(); ++k)
		{
			vec[cfbface.cId(k)] += Sparse::BlockVector<BSIZE>( tabres[k] );
		}

		for ( MGSize j=0; j<cfbface.Size(); ++j)
			for ( MGSize k=0; k<cfbface.Size(); ++k)
			{
				mtx( cfbface.cId(j), cfbface.cId(k) ) += Sparse::BlockMatrix<BSIZE>( mtxres[j][k] );
			}

	}

	// cell residuals
	for ( MGSize i=0; i<this->cData().cGrid().SizeCellTab(); ++i)
	{
		this->GetFCell( cfcell, i);

		cfcell.CalcVn();

		ResAJacobianCell( tabres, mtxres, cfcell); //numerical jacobian

		for ( MGSize k=0; k<cfcell.Size(); ++k)
		{
			vec[cfcell.cId(k)] += Sparse::BlockVector<BSIZE>( tabres[k] );
		}

		for ( MGSize j=0; j<cfcell.Size(); ++j)
			for ( MGSize k=0; k<cfcell.Size(); ++k)
			{
				mtx( cfcell.cId(j), cfcell.cId(k) ) += Sparse::BlockMatrix<BSIZE>( mtxres[j][k] );
			}
	}


	// diagonal elements
	for ( MGSize j=0; j<vec.Size(); ++j)
		for ( MGSize k=0; k<BSIZE; ++k)
		{
			mtx(j,j)(k,k) += 1.0 / tabDT[j];
		}

}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void SpaceSolver<DIM,ET,MAPPER>::ResNumJacob( Sparse::Vector<BSIZE>& vec, Sparse::Matrix<BSIZE>& mtx, const vector<MGFloat>& tabDT, const bool bcorrdiag)
{
	//vector<MGFloat>	tabvol;
	//MGFloat	vol;
	//CGeomCell<DIM>	cgcell;
	//tabvol.resize( this->cData().cGrid().SizeNodeTab(), 0.0);

	//for ( MGSize i=0; i< this->cData().cGrid().SizeCellTab(); ++i)
	//{
	//	cgcell = this->cData().cGrid().cCell( i);
	//	vol = cgcell.Volume() / cgcell.Size();
	//	for ( MGSize k=0; k<cgcell.Size(); ++k)
	//		tabvol[cgcell.cId(k)] += vol;
	//}

	//MGFloat volmin = tabvol[0];
	//for ( MGSize i=1; i<tabvol.size(); ++i)
	//	//if ( volmin > tabvol[i] )
	//		volmin += tabvol[i];

	//volmin /= tabvol.size();

	BVec	tabres[DIM+1];
	BMtx	mtxres[DIM+1][DIM+1];

	CFCell	cfcell;
	CFBFace	cfbface;

	////////////////////////////////////////////////////////////////////////
	//// test cell

	//EVec evtmp;
	//evtmp[0] = 1.4;
	//evtmp[1] = 2.612; // ???
	//evtmp[2] = 0.56;
	//evtmp[3] = 0.;
	//cfcell.rVar(0) = cfcell.rVar(1) = cfcell.rVar(2) = cfcell.rVar(3) = evtmp;

	//cfcell.rNode(0).rPos() = Vect<DIM>( 1., 1.);
	//cfcell.rNode(1).rPos() = Vect<DIM>( 2., 1.);
	//cfcell.rNode(2).rPos() = Vect<DIM>( 1., 2.);

	//cfcell.rType() = DIM+1;
	//cfcell.CalcVn();

	//cfcell.SetJacobian( false);
	//mpFunc->CalcResiduum( cfcell, tabres);

	////////////////////////////////////////////////////////////////////////


	mtx.Reset();
	for ( MGSize i=0; i<vec.Size(); ++i)
		vec[i].Init( 0.0);


	if ( this->cData().cGrid().SizeNodeTab() != vec.Size() || this->cData().cGrid().SizeNodeTab() != this->cData().cSolution().Size() )
		THROW_INTERNAL( "Incompatible sizes of state vectors in SpaceSolver<DIM>::Residuum(...)");

	// boundary conditions
	for ( MGSize i=0; i<this->cData().cGrid().SizeBFaceTab(); ++i)
	{
		this->GetFBFace( cfbface, i);

		//if ( i == 1 )
		//{
		//	mtx(15402,15402).Write();
		//	for ( MGSize k=0; k<cfbface.Size(); ++k)
		//	{
		//		cfbface.Vn().Write();
		//		cout << "inode = " << cfbface.cId(k) << endl;
		//		cfbface.cBNode(k).cVn().Write();
		//		cout << endl;
		//	}

		//	ResNJacobianBFace( tabres, mtxres, cfbface);

		//	THROW_INTERNAL("STOP");
		//}
		//continue;



		ResNJacobianBFace( tabres, mtxres, cfbface); //numerical jacobian


		for ( MGSize k=0; k<cfbface.Size(); ++k)
		{
			vec[cfbface.cId(k)] += Sparse::BlockVector<BSIZE>( tabres[k] );
			//vec[cfbface.cId(k)] += ( volmin / tabvol[cfbface.cId(k)] ) * Sparse::BlockVector<BSIZE>( tabres[k] );

			//if ( cfbface.cId(k) == 70 )
			//{
			//	cout << "iface = " << i << endl;
			//	Sparse::BlockVector<BSIZE>( tabres[k] ).Write();
			//}
		}

		for ( MGSize j=0; j<cfbface.Size(); ++j)
			for ( MGSize k=0; k<cfbface.Size(); ++k)
			{
				mtx( cfbface.cId(j), cfbface.cId(k) ) += Sparse::BlockMatrix<BSIZE>( mtxres[j][k] );
				//mtx( cfbface.cId(j), cfbface.cId(k) ) += ( volmin / tabvol[cfbface.cId(j)] ) * Sparse::BlockMatrix<BSIZE>( mtxres[j][k] );

				//if ( cfbface.cId(j) == 70 && cfbface.cId(k) == 70 )
				//{
				//	cout << "iface = " << i << endl;
				//	Sparse::BlockMatrix<BSIZE>( mtxres[j][k] ).Write();
				//}
			}
	}

	// cell residuals
	for ( MGSize i=0; i<this->cData().cGrid().SizeCellTab(); ++i)
	{
		this->GetFCell( cfcell, i);

		cfcell.CalcVn();

		//if ( i == 77 )
		//	cfcell.Dump( cout);

		ResNJacobianCell( tabres, mtxres, cfcell); //numerical jacobian


		for ( MGSize k=0; k<cfcell.Size(); ++k)
		{
			vec[cfcell.cId(k)] += Sparse::BlockVector<BSIZE>( tabres[k] );
			//vec[cfcell.cId(k)] += ( volmin / tabvol[cfcell.cId(k)] ) * Sparse::BlockVector<BSIZE>( tabres[k] );

			//if ( cfcell.cId(k) == 70 )
			//{
			//	cout << "icell = " << i << endl;
			//	Sparse::BlockVector<BSIZE>( tabres[k] ).Write();
			//}
		}

		for ( MGSize j=0; j<cfcell.Size(); ++j)
			for ( MGSize k=0; k<cfcell.Size(); ++k)
			{
				mtx( cfcell.cId(j), cfcell.cId(k) ) += Sparse::BlockMatrix<BSIZE>( mtxres[j][k] );
				//mtx( cfcell.cId(j), cfcell.cId(k) ) += ( volmin / tabvol[cfcell.cId(j)] ) * Sparse::BlockMatrix<BSIZE>( mtxres[j][k] );

				//if ( cfcell.cId(j) == 70 && cfcell.cId(k) == 70 )
				//{
				//	cout << "icell = " << i << endl;
				//	Sparse::BlockMatrix<BSIZE>( mtxres[j][k] ).Write();
				//}
			}
	}


	//for ( MGSize j=0; j<vec.Size(); ++j)
	//{
	//	BMtx  mtxWQ, mtxWV, mtxVZ, mtxZQ;
	//	EqnEuler<DIM>::CJacobWV( mtxWV, EVec( this->cData().cSolution()[j]) );
	//	EqnEuler<DIM>::CJacobVZ( mtxVZ, EVec( this->cData().cSolution()[j]) );
	//	EqnEuler<DIM>::CJacobZQ( mtxZQ, EVec( this->cData().cSolution()[j]) );

	//	mtxWQ = mtxWV * mtxVZ * mtxZQ;

	//	BMtx  mtxQW, mtxVW, mtxZV, mtxQZ;
	//	EqnEuler<DIM>::CJacobVW( mtxVW, EVec( this->cData().cSolution()[j]) );
	//	EqnEuler<DIM>::CJacobZV( mtxZV, EVec( this->cData().cSolution()[j]) );
	//	EqnEuler<DIM>::CJacobQZ( mtxQZ, EVec( this->cData().cSolution()[j]) );

	//	mtxQW = mtxQZ * mtxZV * mtxVW;

	//	BMtx mtxPI;
	//	EqnEuler<DIM>::PrecLLR( mtxPI, EVec( this->cData().cSolution()[j]), 1. , 1. );

	//	//mtxPI.Invert();
	//	mtxPI = mtxQW * mtxPI * mtxWQ;

	//	vec[j] = Sparse::BlockVector<BSIZE>( mtxPI * BVec( vec[j]) );

	//	for ( MGSize k=0; k<mtx.RowSize(j); ++k)
	//	{
	//		BMtx tmp(BSIZE,BSIZE);
	//		for ( MGSize ir=0;ir<BSIZE;++ir)
	//			for ( MGSize ic=0;ic<BSIZE;++ic)
	//				tmp(ir,ic) = mtx.GetBlock(j,k)(ir,ic);

	//		mtx.GetBlock(j,k) =  Sparse::BlockMatrix<BSIZE>( mtxPI * BMtx( tmp ) );
	//	}

	//}

	//// diagonal elements
	//for ( MGSize j=0; j<vec.Size(); ++j)
	//{
	//	EVec varQ = EVec( this->cData().cSolution()[j]);
	//	EVec varZ = EqnEuler<DIM>::ConservToZ( varQ);
	//	EVec varV = EqnEuler<DIM>::ConservToSimp( varQ);

	//	//FMtx	smtxM_VZ;
	//	//EqnEuler<DIM>::CJacobVZ( smtxM_VZ, mvarZm);
	//	//FVec varV = smtxM_VZ *varZ + mvarVm;


	//	EMtx  mtxWQ, mtxWV, mtxVZ, mtxZQ;
	//	EqnEuler<DIM>::CJacobWV( mtxWV, varV );
	//	EqnEuler<DIM>::CJacobVZ( mtxVZ, varZ );
	//	EqnEuler<DIM>::CJacobZQ( mtxZQ, varZ );

	//	mtxWQ = mtxWV * mtxVZ * mtxZQ;

	//	EMtx  mtxQW, mtxVW, mtxZV, mtxQZ;
	//	EqnEuler<DIM>::CJacobVW( mtxVW, varV );
	//	EqnEuler<DIM>::CJacobZV( mtxZV, varZ );
	//	EqnEuler<DIM>::CJacobQZ( mtxQZ, varZ );

	//	mtxQW = mtxQZ * mtxZV * mtxVW;

	//	MGFloat	q = ::sqrt( EqnEuler<DIM>::SqrU( varV) );
	//	MGFloat	c = ::sqrt( FLOW_K*varV(1)/varV(0) );
	//	MGFloat	M = q/c;

	//	MGFloat	beta = max( /*P_LLR_EPS*/0.001, ::sqrt( ::fabs(M*M-1) ) );
	//	MGFloat	chi = beta/max(M,1.0);

	//	EMtx mtxPI;
	//	EqnEuler<DIM>::PrecLLR( mtxPI, varV, beta, chi);

	//	mtxPI = mtxQW * mtxPI * mtxWQ;

	//	mtxPI.Invert();
	//	mtxPI = mtxQW * mtxPI * mtxWQ;

	//	//mtxPI.InitDelta(BSIZE);

	//	mtxPI *= 1.0 / tabDT[j];
	//	//v = this->cPhysics().MaxSpeed( EVec( this->cData().cSolution()[i]) );

	//	mtx(j,j) += Sparse::BlockMatrix<BSIZE>( mtxPI);

	//}


	// diagonal elements
	if (bcorrdiag)
	{
		for (MGSize j = 0; j < vec.Size(); ++j)
			for (MGSize k = 0; k < BSIZE; ++k)
			{
				mtx(j, j)(k, k) += 1.0 / tabDT[j];
			}
	}
}




//template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
//void SpaceSolver<DIM,ET,MAPPER>::ResiduumCell( BVec tabres[], CFCell& fcell, const bool& bjacobian )
//{
//	fcell.SetJacobian( bjacobian);
//	mpFunc->CalcResiduum( fcell, tabres);
//}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void SpaceSolver<DIM,ET,MAPPER>::ResiduumBFace( BVec tabres[], const CFBFace& fbfac, const bool& bjacobian )
{
	CFCell	fcell;
	BVec	tabcres[DIM+1];

	for ( MGSize k=0; k<fbfac.Size(); ++k)
		tabres[k].Init( 0.0);

	for ( MGSize k=0; k<fbfac.Size(); ++k)
	{
		fbfac.CreateGCell( fcell, k);

		// calc cell h 
		CFCell	nxcell;
		this->GetFCell( nxcell, fbfac.cCellId() );
		MGFloat area = fbfac.Area();
		MGFloat volume = nxcell.Volume();
		MGFloat h = 3. * volume / area;

		//cout << h << endl;

		mBCCalc.ApplyBC( fcell, fbfac, this->cPhysics(), k, h, this->cData().cPeriodicBC() );
		fcell.SetJacobian( bjacobian);
	
		mpFunc->CalcResiduum( fcell, tabcres);

		//////
		//{
		//	cout << "k = " << k << endl;
		//	tabcres[BCIdMapper<DIM>::Id(k)].Write();
		//}
		//////
	
		tabres[k] += tabcres[BCIdMapper<DIM>::Id(k)];

	}
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void SpaceSolver<DIM,ET,MAPPER>::ResAJacobianCell( BVec tabres[], BMtx mtxres[DIM+1][DIM+1], CFCell& fcell )
{
	//fcell.SetJacobian( bjacobian);
	mpFunc->CalcResJacobian( fcell, tabres, mtxres);
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void SpaceSolver<DIM,ET,MAPPER>::ResAJacobianBFace( BVec tabres[], BMtx mtxres[DIM+1][DIM+1], CFBFace& fbfac )
{
	CFCell	fcell;
	BVec	tabcres[DIM+1];
	BMtx	mtxcres[DIM+1][DIM+1];

	for ( MGSize k=0; k<fbfac.Size(); ++k)
	{
		tabres[k].Init( 0.0);

		for ( MGSize j=0; j<fbfac.Size(); ++j)
			mtxres[k][j].Init( 0.0);
	}

	for ( MGSize k=0; k<fbfac.Size(); ++k)
	{
		fbfac.CreateGCell( fcell, k);

		// calc cell h 
		CFCell	nxcell;
		this->GetFCell( nxcell, fbfac.cCellId() );
		MGFloat area = fbfac.Area();
		MGFloat volume = nxcell.Volume();
		MGFloat h = 3. * volume / area;

		//cout << h << endl;

		mBCCalc.ApplyBC(fcell, fbfac, this->cPhysics(), k, h, this->cData().cPeriodicBC() );
		//fcell.SetJacobian( bjacobian);
	
		mpFunc->CalcResJacobian( fcell, tabcres, mtxcres);

		//////
		//{
		//	cout << "k = " << k << endl;
		//	tabcres[BCIdMapper<DIM>::Id(k)].Write();
		//}
		//////
	
		tabres[k] += tabcres[BCIdMapper<DIM>::Id(k)];

		for ( MGSize j=0; j<fbfac.Size(); ++j)
			mtxres[k][j] += mtxcres[BCIdMapper<DIM>::Id(k)][BCIdMapper<DIM>::Id(j)];

	}
}



template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void SpaceSolver<DIM,ET,MAPPER>::ResNJacobianCell( BVec tabres[], BMtx mtxres[DIM+1][DIM+1], CFCell& fcell )
{
	BVec	tabResP[DIM+1];
	BVec	bvect, bvar;
	EVec	evect;


	ResiduumCell( tabres, fcell, true);

	for ( MGSize i=0; i<fcell.Size(); ++i)
		for ( MGSize k=0; k<BVec::SIZE; ++k)
		{
			evect.Init( 0.0);
			bvect.Init( 0.0);
			bvect(k) = mRelEpsilon;

			//MAPPER::GlobalToLocal( bvar, fcell.cVar(i) );
			//bvect(k) = max( fabs( bvar(k) )*mRelEpsilon, mAbsEpsilon );

			MAPPER::LocalToGlobal( evect, bvect);

			fcell.rVar(i) += evect;
			ResiduumCell( tabResP, fcell, true);
			fcell.rVar(i) -= evect;

			for ( MGSize j=0; j<fcell.Size(); ++j)
				for ( MGSize m=0; m<BVec::SIZE; ++m)
				{
					//mtxres[j][i](m,k) = ( tabResP[j](m) - tabres[j](m) ) / (mRelEpsilon);
					mtxres[j][i](m,k) = ( tabResP[j](m) - tabres[j](m) ) / ( bvect(k) );
				}
		}

	ResiduumCell( tabres, fcell, false);
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void SpaceSolver<DIM,ET,MAPPER>::ResNJacobianBFace( BVec tabres[], BMtx mtxres[DIM+1][DIM+1], CFBFace& fbfac )
{
	BVec	tabResP[DIM+1];
	BVec	bvect, bvar;
	EVec	evect;

	ResiduumBFace( tabres, fbfac, true);

	for ( MGSize i=0; i<fbfac.Size(); ++i)
		for ( MGSize k=0; k<BVec::SIZE; ++k)
		{
			evect.Init( 0.0);
			bvect.Init( 0.0);
			bvect(k) = mRelEpsilon;

			//MAPPER::GlobalToLocal( bvar, fbfac.cVar(i) );
			//bvect(k) = max( fabs( bvar(k) )*mRelEpsilon, mAbsEpsilon );

			MAPPER::LocalToGlobal( evect, bvect);

			fbfac.rVar(i) += evect;

			ResiduumBFace( tabResP, fbfac, true);
			fbfac.rVar(i) -= evect;

			for ( MGSize j=0; j<fbfac.Size(); ++j)
				for ( MGSize m=0; m<BVec::SIZE; ++m)
				{
					//mtxres[j][i](m,k) = ( tabResP[j](m) - tabres[j](m) ) / (mRelEpsilon);
					mtxres[j][i](m,k) = ( tabResP[j](m) - tabres[j](m) ) / ( bvect(k) );
				}
		}

	ResiduumBFace( tabres, fbfac, false);
}



template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void SpaceSolver<DIM,ET,MAPPER>::ResJacobStrongBC( Sparse::Vector<BSIZE>& vec, Sparse::Matrix<BSIZE>& vmtx )
{
	CFBFace	cfbface;

	for ( MGSize i=0; i<this->cData().cGrid().SizeBFaceTab(); ++i)
	{
		this->GetFBFace( cfbface, i);

		if ( this->cPhysics().IsStrongBC( cfbface.cSurfId() ) )
		{
			SVector<BSIZE,bool>	vecmask;
			SVector<ESIZE,bool>	varmask;

			for ( MGSize ie=0; ie<ESIZE; ++ie)
				if ( this->cPhysics().IsEqStrongBC( cfbface.cSurfId(), ie ) )
					varmask(ie) = true;
				else
					varmask(ie) = false;

			MAPPER::GlobalToLocal( vecmask, varmask );

			for ( MGSize in=0; in<cfbface.Size(); ++in)
			{
				MGSize idnode = cfbface.cId( in);

				for ( MGSize ie=0; ie<BSIZE; ++ie)
					if ( vecmask(ie) )
					{
						vec[idnode](ie) = 0.0;

						for ( MGSize ib=0; ib<vmtx.RowSize(idnode); ++ib)
						{
							Sparse::BlockMatrix<BSIZE>	block = vmtx.GetBlock( idnode, ib);
							MGSize idcol = vmtx.GetColId( idnode, ib );

							for ( MGSize ic=0; ic<BSIZE; ++ic)
								block(ie,ic) = 0.0;

							if ( idcol == idnode) 
								block(ie,ie) = 1.0;
						}
					}
			}
		}
	}
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void SpaceSolver<DIM,ET,MAPPER>::ApplyStrongBC( Sparse::Vector<BSIZE>& vec)
{
	for ( MGSize i=0; i<this->cData().cGrid().SizeBFaceTab(); ++i)
	{
		CGeomBFace<DIM> bface = this->cData().cGrid().cBFace( i);

		//CFBFace	bface;
		//this->GetFBFace( bface, i);

		if ( this->cPhysics().IsStrongBC( bface.cSurfId() ) )
		{
			for ( MGSize iloc=0; iloc<bface.Size(); ++iloc)
			{
				Vect<DIM> vpos = bface.cNode(iloc).cPos();
				Vect<DIM> vn = bface.cBNode(iloc).cVn();
				Vect<DIM> vnn = vn.versor();

				MGSize inode = bface.cId( iloc );

				typename EquationDef<DIM,ET>::FVec	varQb, var;
				typename EquationDef<DIM,ET>::AVec	varAuxb, varAux( this->cData().cAuxData()[inode] );

				MAPPER::LocalToGlobal( var, vec[inode] );


				// calc cell h 
				CFCell	nxcell;
				this->GetFCell( nxcell, bface.cCellId() );
				MGFloat area = bface.Area();
				MGFloat volume = nxcell.Volume();
				MGFloat h = 3. * volume / area;

				this->cPhysics().ApplyBC(varQb, varAuxb, var, varAux, bface.cSurfId(), vpos, vnn, h, this->cData().cPeriodicBC(), iloc);

				for ( MGSize ie=0; ie<ESIZE; ++ie)
					if ( this->cPhysics().IsEqStrongBC( bface.cSurfId(), ie ) )
						var(ie) = varQb(ie);

				MAPPER::GlobalToLocal( vec[inode], var );

			}
		}
	}
}



void init_SpaceSolver()
{
	// RFR

	static ConcCreator< 
		MGString, 
		SpaceSolver< Geom::DIM_2D, EQN_RFR_EULER, EquationDef<Geom::DIM_2D,EQN_RFR_EULER>::SplitingFull::Block<1> >,
		SpaceSolverBase< Geom::DIM_2D, EQN_RFR_EULER, EquationDef<Geom::DIM_2D,EQN_RFR_EULER>::SplitingFull::Block<1> > 
	>	gCreatorSpaceSolverRfrEulerRDS2D( "SPACESOL_2D_RFR_EULER_RDS");

	static ConcCreator< 
		MGString, 
		SpaceSolver< Geom::DIM_3D, EQN_RFR_EULER, EquationDef<Geom::DIM_3D,EQN_RFR_EULER>::SplitingFull::Block<1> >,
		SpaceSolverBase< Geom::DIM_3D, EQN_RFR_EULER, EquationDef<Geom::DIM_3D,EQN_RFR_EULER>::SplitingFull::Block<1> > 
	>	gCreatorSpaceSolverRfrEulerRDS3D( "SPACESOL_3D_RFR_EULER_RDS");



	// 2D


	// Poisson 

	static ConcCreator< 
		MGString, 
		SpaceSolver< Geom::DIM_2D, EQN_POISSON, EquationDef<Geom::DIM_2D,EQN_POISSON>::SplitingFull::Block<1> >,
		SpaceSolverBase< Geom::DIM_2D, EQN_POISSON, EquationDef<Geom::DIM_2D,EQN_POISSON>::SplitingFull::Block<1> > 
	>	gCreatorSpaceSolverPoissonRDS2D( "SPACESOL_2D_POISSON_RDS");

	// Euler 

	static ConcCreator< 
		MGString, 
		SpaceSolver< Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SplitingFull::Block<1> >,
		SpaceSolverBase< Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SplitingFull::Block<1> > 
	>	gCreatorSpaceSolverEulerRDS2D( "SPACESOL_2D_EULER_RDS");


	// NS

	static ConcCreator< 
		MGString, 
		SpaceSolver< Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SplitingFull::Block<1> >,
		SpaceSolverBase< Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SplitingFull::Block<1> > 
	>	gCreatorSpaceSolverNSRDS2D( "SPACESOL_2D_NS_RDS");


	// RANS SA with splitting

	static ConcCreator< 
		MGString, 
		SpaceSolver< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >,
		SpaceSolverBase< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> > 
	>	gCreatorSpaceSolverRANS1eqFlowRDS2D( "SPACESOL_2D_RANS_SA_RDS");


	static ConcCreator< 
		MGString, 
		SpaceSolver< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >,
		SpaceSolverBase< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> > 
	>	gCreatorSpaceSolverRANS1eqTurbRDS2D( "SPACESOL_2D_RANS_SA_RDS");


	// RANS K-OMEGA

	static ConcCreator< 
		MGString, 
		SpaceSolver< Geom::DIM_2D, EQN_RANS_KOMEGA, EquationDef<Geom::DIM_2D,EQN_RANS_KOMEGA>::SplitingFull::Block<1> >,
		SpaceSolverBase< Geom::DIM_2D, EQN_RANS_KOMEGA, EquationDef<Geom::DIM_2D,EQN_RANS_KOMEGA>::SplitingFull::Block<1> > 
	>	gCreatorSpaceSolverRANSKOMEGARDS2D( "SPACESOL_2D_RANS_KOMEGA_RDS");





	// 3D

	// Poisson 

	static ConcCreator< 
		MGString, 
		SpaceSolver< Geom::DIM_3D, EQN_POISSON, EquationDef<Geom::DIM_3D,EQN_POISSON>::SplitingFull::Block<1> >,
		SpaceSolverBase< Geom::DIM_3D, EQN_POISSON, EquationDef<Geom::DIM_3D,EQN_POISSON>::SplitingFull::Block<1> > 
	>	gCreatorSpaceSolverPoissonRDS3D( "SPACESOL_3D_POISSON_RDS");

	// Euler 

	static ConcCreator< 
		MGString, 
		SpaceSolver< Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SplitingFull::Block<1> >,
		SpaceSolverBase< Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SplitingFull::Block<1> > 
	>	gCreatorSpaceSolverEulerRDS3D( "SPACESOL_3D_EULER_RDS");
	
	// NS

	static ConcCreator< 
		MGString, 
		SpaceSolver< Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SplitingFull::Block<1> >,
		SpaceSolverBase< Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SplitingFull::Block<1> > 
	>	gCreatorSpaceSolverNSRDS3D( "SPACESOL_3D_NS_RDS");

	// RANS SA with splitting

	static ConcCreator< 
		MGString, 
		SpaceSolver< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >,
		SpaceSolverBase< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> > 
	>	gCreatorSpaceSolverRANS1eqFlowRDS3D( "SPACESOL_3D_RANS_SA_RDS");

	static ConcCreator< 
		MGString, 
		SpaceSolver< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >,
		SpaceSolverBase< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> > 
	>	gCreatorSpaceSolverRANS1eqTurbRDS3D( "SPACESOL_3D_RANS_SA_RDS");

	// RANS K-OMEGA

	static ConcCreator< 
		MGString, 
		SpaceSolver< Geom::DIM_3D, EQN_RANS_KOMEGA, EquationDef<Geom::DIM_3D,EQN_RANS_KOMEGA>::SplitingFull::Block<1> >,
		SpaceSolverBase< Geom::DIM_3D, EQN_RANS_KOMEGA, EquationDef<Geom::DIM_3D,EQN_RANS_KOMEGA>::SplitingFull::Block<1> > 
	>	gCreatorSpaceSolverRANSKOMEGARDS3D( "SPACESOL_3D_RANS_KOMEGA_RDS");
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

