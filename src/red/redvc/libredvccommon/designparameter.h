
#ifndef __DESIGNPARAMETER_H__
#define __DESIGNPARAMETER_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "redvc/libredvcgrid/node.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

using namespace Geom;

//////////////////////////////////////////////////////////////////////
// class Design Parameter
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class DesignParameter 
{

public:
	DesignParameter() {};
	virtual ~DesignParameter() {};
	DesignParameter(const Node<DIM>& node, const MGSize& surfid) 
	{ 
	    mNode = node; 
	    msurfId = surfid; 
	    mvalue = 0.0; 
	    
	};
	
	const Node<DIM>&	cNode() const	{ return mNode; }
	Node<DIM>&			rNode()	const	{ return mNode; }

	const MGSize&	cSurfId() const	{ return msurfId; }
	MGSize&			rSurfId()		{ return msurfId; }

	const MGFloat&	cValue() const	{ return mvalue; }
	MGFloat&		rValue()		{ return mvalue; }


private:
	Node<DIM>	mNode;
	MGSize		msurfId;
	MGFloat		mvalue;
};
//////////////////////////////////////////////////////////////////////
	

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif //__DESIGNPARAMETER_H__
