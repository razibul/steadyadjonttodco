#ifndef __TEMPDATA_H__
#define __TEMPDATA_H__

#include "libcoreconfig/configbase.h"
#include "libredcore/equation.h" 
#include "libredphysics/physics.h"

#include "redvc/libredvccommon/tempdatablock.h"

#include "libextsparse/sparsevector.h"

#include "libredcore/processinfo.h"
#include "libcoreio/store.h"
#include "libcoreio/readsol.h"
#include "libcoreio/writesol.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




//////////////////////////////////////////////////////////////////////
// class TempData
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET> 
class TempData : public ConfigBase
{
public:
	TempData() : ConfigBase(), mDim(DIM_NONE)		{}

	virtual void Create(  const Dimension& dim, const CfgSection* pcfgsec);
	virtual void PostCreateCheck() const;
	virtual void Init();

	const TempDataBlock<DIM,ET>&	operator [] ( const  MGSize& i) const	{ return mtabData[i]; }
	TempDataBlock<DIM,ET>&			operator [] ( const  MGSize& i)			{ return mtabData[i]; }

	MGSize		Size() const	{ return mvecQ.Size(); }

	void		Resize( const MGSize& n)	{ return mvecQ.Resize( n); }

private:
	vector< TempDataBlock<DIM,ET> >	mtabData; 
};
//////////////////////////////////////////////////////////////////////



template <MGSize SIZE>
inline void TempData<SIZE>::Create( const Dimension& dim, const CfgSection* pcfgsec)		
{
	mDim = dim;
	ConfigBase::Create( pcfgsec);
}


template <MGSize SIZE>
inline void TempData<SIZE>::PostCreateCheck() const
{ 
	ConfigBase::PostCreateCheck();
}


template <MGSize SIZE>
inline void TempData<SIZE>::Init()
{ 
	bool		bBIN = IO::FileTypeToBool(  ConfigSect().ValueString( ConfigStr::Solver::TempData::FTYPE ) );
	MGString	fname = ConfigSect().ValueString( ConfigStr::Solver::TempData::FNAME );

	fname = ProcessInfo::ModifyFileName( fname);
	
	IO::ReadSOL	reader( *this);

	reader.DoRead( fname, bBIN );
} 



//////////////////////////////////////////////////////////////////////
// class TempData - empty specialization
//////////////////////////////////////////////////////////////////////
template <>
class TempData<0> : public ConfigBase
{
public:
	virtual void Create( const Dimension& dim, const CfgSection* pcfgsec)		{ ConfigBase::Create( pcfgsec); }
	virtual void PostCreateCheck() const										{ ConfigBase::PostCreateCheck(); }

	virtual void Init()		{}

	void		Resize( const MGSize& n)	{}

	const Sparse::BlockVector<0>&	operator [] ( const  MGSize& i) const	{ return mDummy; }

private:
	Sparse::BlockVector<0>	mDummy;
};

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __TEMPDATA_H__ 