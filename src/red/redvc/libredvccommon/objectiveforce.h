#ifndef __OBJECTIVEFORCE_H__
#define __OBJECTIVEFORCE_H__


#include "libcoresystem/mgdecl.h"
#include "libcoreconfig/configbase.h"
#include "libcoregeom/dimension.h"
#include "libredcore/configconst.h"
#include "libextsparse/sparsevector.h"
#include "compflowcell.h"
#include "compflowbface.h"
#include "objectivebase.h"
#include "designparametercollection.h"
#include "solution.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

	
//////////////////////////////////////////////////////////////////////
//	class ObjectiveForce
//////////////////////////////////////////////////////////////////////

template <Geom::Dimension DIM, EQN_TYPE ET>
class ObjectiveForce : public ObjectiveBase<DIM,ET>
{
public:
	enum {
		NONE = 0,
		PRESSURE,
		ENERGY
	};
        typedef GridSimplex<DIM> TGrid;

	typedef EquationDef<DIM,ET>		EqDef;
	typedef Geom::Vect<DIM>			GVec;
	typedef typename EquationDef<DIM, ET>::FVec		EVec;
	
	enum { ESIZE = EqDef::SIZE };
	enum { ASIZE = EqDef::AUXSIZE };
	
	typedef CFlowCell<DIM, ESIZE, ASIZE>		CFCell;
	typedef CFlowBFace<DIM, ESIZE, ASIZE>		CFBFace;

	ObjectiveForce() : ObjectiveBase<DIM,ET>() {};
	virtual ~ObjectiveForce() {};

	virtual void Create(const CfgSection* pcfgsec, const TGrid* pgrd, const DesignParameterCollection<DIM>& desparcoll);
	virtual void PostCreateCheck() const;
	virtual void Init();
	virtual void ComputeGradSolution(const Solution<DIM, ET>& sol);
	virtual void ComputeObjective(const Solution<DIM,ET>& sol);

private:
	void ComputeGradSolutionBFace(const CFBFace& bface);

	MGString mName;
	GVec mDir;
	map<MGInt, bool> mmapSurf;
	
	MGInt mtemptype;
};

template <Geom::Dimension DIM, EQN_TYPE ET>
void ObjectiveForce<DIM,ET>::Create(const CfgSection* pcfgsec, const TGrid* pgrd, const DesignParameterCollection<DIM>& desparcoll)
{
	ObjectiveBase<DIM,ET>::Create(pcfgsec, pgrd, desparcoll);

	mName	= this->ConfigSect().ValueString( ConfigStr::Solver::Objectives::Objective::Type::KEY );
	switch (DIM)
	{
	case DIM_3D:
		mDir.rZ() = this->ConfigSect().ValueFloat(ConfigStr::Solver::Objectives::Objective::DIRZ);
	case DIM_2D:
		mDir.rY() = this->ConfigSect().ValueFloat(ConfigStr::Solver::Objectives::Objective::DIRY);
	case DIM_1D:
		mDir.rX() = this->ConfigSect().ValueFloat(ConfigStr::Solver::Objectives::Objective::DIRX);
	}
	
	if (mDir.module() != 1.0)
		mDir /= mDir.module();
	
	const CfgSection& secsurf = this->ConfigSect().GetSection( ConfigStr::Solver::Objectives::Objective::SURF::KEY );
	CfgSection::const_iterator_rec	citrr;
	for (citrr = secsurf.begin_rec(); citrr != secsurf.end_rec(); ++citrr)
	{
		//cout << (*citrr).first << " " << (*citrr).second << endl;
		MGInt val = std::atoi((*citrr).second.c_str());
		bool valbool = 0;
		switch (val)
		{
		case 0:
			valbool = false;
			break;
		case 1:
			valbool = true;
			break;
		}
		MGInt id = std::atoi((*citrr).first.c_str());
		mmapSurf.insert(std::make_pair(id, valbool));
	}

	if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Objectives::Objective::TEMP_TYPE::NAME ) )
	{
		const MGString mstr = this->ConfigSect().ValueString( ConfigStr::Solver::Objectives::Objective::TEMP_TYPE::NAME );

		if ( strcmp(mstr.c_str(), ConfigStr::Solver::Objectives::Objective::TEMP_TYPE::Value::ENERGY ) == 0 )
			mtemptype = ENERGY;
		if ( strcmp(mstr.c_str(), ConfigStr::Solver::Objectives::Objective::TEMP_TYPE::Value::PRESSURE ) == 0 )
			mtemptype = PRESSURE;
	}
	else
	{
		//mtemptype = NONE;
		cout << "!!!!! ObjectiveForce::OBJ_TEMP_TYPE not defined. Using ENERGY version " << endl;
		mtemptype = ENERGY;
	}

}

template <Geom::Dimension DIM, EQN_TYPE ET>
void ObjectiveForce<DIM,ET>::PostCreateCheck() const
{
	ObjectiveBase<DIM,ET>::PostCreateCheck();
}

template <Geom::Dimension DIM, EQN_TYPE ET>
void ObjectiveForce<DIM,ET>::Init()
{
	ObjectiveBase<DIM,ET>::Init();

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif //__OBJECTIVEFORCE_H__
