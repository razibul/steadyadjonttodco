#ifndef __SOLVER_H__
#define __SOLVER_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreconfig/config.h"
#include "redvc/libredvccommon/solverbase.h"
#include "libredcore/equation.h"

#include "libredphysics/physics.h"
#include "redvc/libredvccommon/data.h"
#include "redvc/libredvccommon/executorbase.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


using namespace Geom;


//////////////////////////////////////////////////////////////////////
// Solver
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, EQN_TYPE ET>
class Solver : public SolverBase
{
public:
	Solver()	{}
	virtual ~Solver();

	virtual void	Create( const CfgSection* pcfgsec);
	virtual void	PostCreateCheck() const;
	virtual void	Init();

	virtual void	Solve();

protected:
	Physics<DIM,ET>		mPhysics;
	Data<DIM,ET>		mData;

	bool	mbWriteTec;
	bool	mbWriteTecSurf;

	vector< ExecutorBase<DIM,ET>* >	mtabExec;
};
//////////////////////////////////////////////////////////////////////


template <Dimension DIM, EQN_TYPE ET>
Solver<DIM,ET>::~Solver()	
{
	for ( typename vector< ExecutorBase<DIM,ET> * >::iterator itr=mtabExec.begin(); itr!=mtabExec.end(); ++itr)
		if ( *itr != 0 )
			delete *itr;
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __SOLVER_H__
