#ifdef WITH_PETSC
#include <mpi.h>
#endif // WITH_PETSC

#include "executor.h"
#include "libcorecommon/factory.h"
#include "libredcore/configconst.h"
#include "libcoreconfig/cfgsection.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
Executor<DIM,ET,MAPPER>::~Executor()	
{
	if ( mpSpaceSol != 0)
		delete mpSpaceSol;
	
	if ( mpTimeSol != 0)
		delete mpTimeSol;
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void Executor<DIM, ET, MAPPER>::CFLCrash()
{
	mpTimeSol->CFLCrash();
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void Executor<DIM,ET,MAPPER>::Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata)
{ 
	ExecutorBase<DIM,ET>::Create( pcfgsec, pphys, pdata);

	MGString sdim = Geom::DimToStr( DIM);
	MGString seqtype = EquationDef<DIM,ET>::NameEqnType();

	// SpaceSolver info
	const CfgSection* pspcsol = & this->ConfigSect().GetSection( ConfigStr::Solver::Executor::SpaceSol::NAME );

	MGString sssoltype = pspcsol->ValueString( ConfigStr::Solver::Executor::SpaceSol::Type::KEY );
	MGString sschemetype = pspcsol->ValueString( ConfigStr::Solver::Executor::SpaceSol::Type::KEY );

	// TimeSolver info
	const CfgSection* ptimsol = & this->ConfigSect().GetSection( ConfigStr::Solver::Executor::TimeSol::NAME );

	MGString stsoltype = ptimsol->ValueString( ConfigStr::Solver::Executor::TimeSol::Type::KEY );

	ostringstream os;
	os << BSIZE;
	MGString sbsize = os.str();

	MGString ssprefix = "";
	if ( stsoltype == MGString( ConfigStr::Solver::Executor::TimeSol::Type::Value::PETSC ) )
		ssprefix = "MPIPETSC_";
	
	// create and init SpaceSolver
	MGString spcsolkey = ssprefix + "SPACESOL_" + sdim + "_" + seqtype + "_" + sschemetype;
	
	cout << "creating '" << spcsolkey << "'" << endl;
	mpSpaceSol = Singleton< Factory< MGString,Creator< SpaceSolverBase<DIM,ET,MAPPER> > > >::GetInstance()->GetCreator( spcsolkey )->Create();
	mpSpaceSol->Create( pspcsol, pphys, pdata);


	// create and init TimeSolver
	MGString timsolkey = "TIMESOL_BSIZE_" + sbsize + "_" + stsoltype;
	cout << "creating '" << timsolkey << "'" << endl;

	mpTimeSol = Singleton< Factory< MGString,Creator< TimeSolverBase<BSIZE> > > >::GetInstance()->GetCreator( timsolkey )->Create();
	mpTimeSol->Create( ptimsol, mpSpaceSol);
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void Executor<DIM,ET,MAPPER>::Init()
{
	mpSpaceSol->Init();
	mpTimeSol->Init();

	mpTimeSol->rLocalSolution().Resize( this->cData().cSolution().Size() );
}



template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
bool Executor<DIM,ET,MAPPER>::SolveStep( ConvergenceInfo<ESIZE>& info)
{
	if ( ProcessInfo::cProcId() == 0 )
		cout << ProcessInfo::Prompt() << Info() << endl;

	// copy solution to local vector
	GlobalToLocal( mpTimeSol->rLocalSolution() );

	//ofstream of( "_dump_q.txt");
	//mpTimeSol->rLocalSolution().Dump( of);
	//THROW_INTERNAL( "STOP");

	// do step
	ConvergenceInfo<BSIZE> locinfo;
	mpTimeSol->DoStep( locinfo);

	// update global convergence info
	info.template Update<MAPPER>( locinfo);

	// copy solution from local vector
	LocalToGlobal( mpTimeSol->rLocalSolution() );

	return true;
}




void init_Executor()
{
	//------------------------------------------------------------------//
	// RFR EULER full
	static ConcCreator< 
		MGString, 
		Executor< Geom::DIM_2D, EQN_RFR_EULER, EquationDef<Geom::DIM_2D,EQN_RFR_EULER>::SplitingFull::Block<1> >, 
		ExecutorBase<Geom::DIM_2D,EQN_RFR_EULER> 
	>	gCreatorExecutorRfrEulerFull2D( "EXECUTOR_2D_RFR_EULER_FULL");

	static ConcCreator< 
		MGString, 
		Executor< Geom::DIM_3D, EQN_RFR_EULER, EquationDef<Geom::DIM_3D,EQN_RFR_EULER>::SplitingFull::Block<1> >, 
		ExecutorBase<Geom::DIM_3D,EQN_RFR_EULER> 
	>	gCreatorExecutorRfrEulerFull3D( "EXECUTOR_3D_RFR_EULER_FULL");


	//////////////////////////////////////////////////////////////////////
	// 2D

	//------------------------------------------------------------------//
	// POISSON full
	static ConcCreator< 
		MGString, 
		Executor< Geom::DIM_2D, EQN_POISSON, EquationDef<Geom::DIM_2D,EQN_POISSON>::SplitingFull::Block<1> >, 
		ExecutorBase<Geom::DIM_2D,EQN_POISSON> 
	>	gCreatorExecutorPoissonFull2D( "EXECUTOR_2D_POISSON_FULL");


	//------------------------------------------------------------------//
	// EULER full
	static ConcCreator< 
		MGString, 
		Executor< Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SplitingFull::Block<1> >, 
		ExecutorBase<Geom::DIM_2D,EQN_EULER> 
	>	gCreatorExecutorEulerFull2D( "EXECUTOR_2D_EULER_FULL");


	//------------------------------------------------------------------//
	// NS full
	static ConcCreator< 
		MGString, 
		Executor< Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SplitingFull::Block<1> >, 
		ExecutorBase<Geom::DIM_2D,EQN_NS> 
	>	gCreatorExecutorNSFull2D( "EXECUTOR_2D_NS_FULL");


	//------------------------------------------------------------------//
	// RANS_SA split
	static ConcCreator< 
		MGString, 
		Executor< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >, 
		ExecutorBase<Geom::DIM_2D,EQN_RANS_SA> 
	>	gCreatorExecutorRANSSANS2D( "EXECUTOR_2D_RANS_SA_FLOW");

	static ConcCreator< 
		MGString, 
		Executor< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >, 
		ExecutorBase<Geom::DIM_2D,EQN_RANS_SA> 
	>	gCreatorExecutorRANSSATurb2D( "EXECUTOR_2D_RANS_SA_TURBULENCE");


	//------------------------------------------------------------------//
	// RANS_KOMEGA full
	static ConcCreator< 
		MGString, 
		Executor< Geom::DIM_2D, EQN_RANS_KOMEGA, EquationDef<Geom::DIM_2D,EQN_RANS_KOMEGA>::SplitingFull::Block<1> >, 
		ExecutorBase<Geom::DIM_2D,EQN_RANS_KOMEGA> 
	>	gCreatorExecutorRANSKOMEGAFull2D( "EXECUTOR_2D_RANS_KOMEGA_FULL");





	//////////////////////////////////////////////////////////////////////
	// 3D

	//------------------------------------------------------------------//
	// POISSON full
	static ConcCreator< 
		MGString, 
		Executor< Geom::DIM_3D, EQN_POISSON, EquationDef<Geom::DIM_3D,EQN_POISSON>::SplitingFull::Block<1> >, 
		ExecutorBase<Geom::DIM_3D,EQN_POISSON> 
	>	gCreatorExecutorPoissonFull3D( "EXECUTOR_3D_POISSON_FULL");

	//------------------------------------------------------------------//
	// EULER full
	static ConcCreator< 
		MGString, 
		Executor< Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SplitingFull::Block<1> >, 
		ExecutorBase<Geom::DIM_3D,EQN_EULER> 
	>	gCreatorExecutorEulerFull3D( "EXECUTOR_3D_EULER_FULL");

	//------------------------------------------------------------------//
	// NS full
	static ConcCreator< 
		MGString, 
		Executor< Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SplitingFull::Block<1> >, 
		ExecutorBase<Geom::DIM_3D,EQN_NS> 
	>	gCreatorExecutorNSFull3D( "EXECUTOR_3D_NS_FULL");

	//------------------------------------------------------------------//
	// RANS_SA split
	static ConcCreator< 
		MGString, 
		Executor< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >, 
		ExecutorBase<Geom::DIM_3D,EQN_RANS_SA> 
	>	gCreatorExecutorRANS1eNS3D( "EXECUTOR_3D_RANS_SA_FLOW");

	static ConcCreator< 
		MGString, 
		Executor< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >, 
		ExecutorBase<Geom::DIM_3D,EQN_RANS_SA> 
	>	gCreatorExecutorRANS1eTurb3D( "EXECUTOR_3D_RANS_SA_TURBULENCE");

	//------------------------------------------------------------------//
	// RANS_KOMEGA full
	static ConcCreator< 
		MGString, 
		Executor< Geom::DIM_3D, EQN_RANS_KOMEGA, EquationDef<Geom::DIM_3D,EQN_RANS_KOMEGA>::SplitingFull::Block<1> >, 
		ExecutorBase<Geom::DIM_3D,EQN_RANS_KOMEGA> 
	>	gCreatorExecutorRANSKOMEGAFull3D( "EXECUTOR_3D_RANS_KOMEGA_FULL");

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

