#ifndef __SOLUTION_H__
#define __SOLUTION_H__

#include "libcoreconfig/configbase.h"
#include "libredphysics/physics.h"

#include "libextsparse/sparsevector.h"

#include "libcoreio/store.h"
#include "libcoreio/readsol.h"
#include "libcoreio/writesol.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class Solution
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, EQN_TYPE ET>
class Solution : public IO::SolFacade, public ConfigBase
{
public:
	enum { ESIZE = EquationDef<DIM,ET>::SIZE };
	typedef EquationDef<DIM,ET>		EqDef;
	typedef typename EqDef::FVec	FVec;
	typedef typename EqDef::AVec	AVec;
	typedef Geom::Vect<DIM>			GVec;


	Solution() : ConfigBase()		{}

	virtual void Create( const CfgSection* pcfgsec, const Physics<DIM,ET>* pphys);
	virtual void PostCreateCheck() const;
	virtual void Init();

	const Sparse::BlockVector<ESIZE>&	operator [] ( const  MGSize& i) const	{ return mvecQ[i]; }
	Sparse::BlockVector<ESIZE>&			operator [] ( const  MGSize& i)			{ return mvecQ[i]; }

	MGSize		Size() const	{ return mvecQ.Size(); }

	void		Resize( const MGSize& n)	{ mvecQ.Resize( n); }

	void		CheckNAN() const			{ CheckIsNAN( mvecQ); }


protected:
	void	CheckIsNAN( const Sparse::Vector<ESIZE>& vec) const;
	void	CheckQ( const Sparse::Vector<ESIZE>& vec) const;

// Solution Facade
protected:
	virtual Dimension	Dim() const						{ return DIM;}

	virtual void	IOGetName( MGString& name) const	{ name = mSolName;}
	virtual void	IOSetName( const MGString& name)	{ mSolName = name;}

	virtual void	DimToUndim( vector<MGFloat>& tab ) const;//	{ mpPhysics->DimToUndim( tab); }
	virtual void	UndimToDim( vector<MGFloat>& tab ) const;//	{ mpPhysics->UndimToDim( tab); }
 
	virtual MGSize	IOSize() const						{ return Size();}			
	virtual MGSize	IOBlockSize() const					{ return ESIZE;}

	virtual void	IOResize( const MGSize& n)			{ mvecQ.Resize( n); }

	virtual void	IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id );

 
private:
	MGString				mSolName; 
	const Physics<DIM,ET>*	mpPhysics;

	Sparse::Vector<ESIZE>	mvecQ; 
};
//////////////////////////////////////////////////////////////////////


template <Dimension DIM, EQN_TYPE ET>
inline void Solution<DIM,ET>::CheckIsNAN( const Sparse::Vector<ESIZE>& vec) const
{
	for ( MGSize i=0; i<vec.Size(); ++i)
		for ( MGSize j=0; j<ESIZE; ++j)
		{
			if ( ISNAN( vec[i](j) ) )
			{
				vec[i].Write();
				THROW_INTERNAL( "NAN :: i='" << i << "'")
			}
		}

		//IS_INFNAN_THROW( vec[i](j), "ERROR in Solution<ESIZE>::CheckIsNAN()");
}


template <Dimension DIM, EQN_TYPE ET>
inline void Solution<DIM,ET>::CheckQ( const Sparse::Vector<ESIZE>& vec) const
{
	//rSpaceSol().IsPhysical( vec);
} 

 
template <Geom::Dimension DIM, EQN_TYPE ET>
inline void Solution<DIM,ET>::IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const
{
	if ( tabx.size() != ESIZE)
		THROW_INTERNAL( "Solution<ESIZE>::IOGetBlockVector - tabx.size() != ESIZE");

	for ( MGSize k=0; k<ESIZE; ++k)
		tabx[k] = mvecQ[id](k);
}


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void Solution<DIM,ET>::IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id )
{
	if ( tabx.size() != ESIZE)
		THROW_INTERNAL( "Solution<ESIZE>::IOSetBlockVector - tabx.size() != ESIZE");

	for ( MGSize k=0; k<ESIZE; ++k)
		mvecQ[id](k) = tabx[k];
}


template <Dimension DIM, EQN_TYPE ET>
inline void Solution<DIM,ET>::Create( const CfgSection* pcfgsec, const Physics<DIM,ET>* pphys)		
{
	mpPhysics = pphys;
	ConfigBase::Create( pcfgsec);
}


template <Dimension DIM, EQN_TYPE ET>
inline void Solution<DIM,ET>::PostCreateCheck() const
{ 
	ConfigBase::PostCreateCheck();
}


template <Dimension DIM, EQN_TYPE ET>
inline void Solution<DIM,ET>::Init()
{ 
} 


template <Dimension DIM, EQN_TYPE ET>
inline void Solution<DIM,ET>::DimToUndim( vector<MGFloat>& tab ) const
{ 
	FVec vin, vout;
	if ( tab.size() != ESIZE)
		THROW_INTERNAL("Solution<DIM,ET>::DimToUndim - tab.size() != ESIZE");

	for ( MGSize i=0; i<ESIZE; ++i)
		vin(i) = tab[i];

	mpPhysics->DimToUndim( vout, vin, mpPhysics->cRefVar() );

	for ( MGSize i=0; i<ESIZE; ++i)
		tab[i] = vout(i);
}

template <Dimension DIM, EQN_TYPE ET>
inline void Solution<DIM,ET>::UndimToDim( vector<MGFloat>& tab ) const
{ 
	FVec vin, vout;
	if ( tab.size() != ESIZE)
		THROW_INTERNAL("Solution<DIM,ET>::UndimToDim - tab.size() != ESIZE");

	for ( MGSize i=0; i<ESIZE; ++i)
		vin(i) = tab[i];

	mpPhysics->UndimToDim( vout, vin, mpPhysics->cRefVar() );

	for ( MGSize i=0; i<ESIZE; ++i)
		tab[i] = vout(i);
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __SOLUTION_H__
