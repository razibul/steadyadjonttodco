#ifndef __COMPFLOWCELL_H__
#define __COMPFLOWCELL_H__

#include "libredcore/equation.h"
#include "libcoregeom/gaussweights.h"
#include "redvc/libredvccommon/compflowent.h"
#include "redvc/libredvcgrid/compgeomcell.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class CFlowCell
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, MGSize EQSIZE, MGSize AUXSIZE>
class CFlowCell : public CFlowEnt<DIM,EQSIZE,AUXSIZE,GridSizes<DIM>::CELL_MAXSIZE>, public CGeomCell<DIM>
{
private:
	typedef CFlowEnt<DIM,EQSIZE,AUXSIZE,GridSizes<DIM>::CELL_MAXSIZE> FEnt;
	typedef CGeomCell<DIM> GCell;

public:
	CFlowCell() : FEnt(), GCell()										{}
	CFlowCell( const CFlowCell& cell) : FEnt( cell), GCell( cell)		{}
	explicit CFlowCell( const GCell& gcell) : FEnt(), GCell( gcell)		{}

	template <class F>
	Vect<DIM>	GradVarOmega( const F& GetVar) const;

	template <class F>
	Vect<DIM>	GradVar( const F& GetVar) const;

	void	WriteTEC( FILE *f);

	void	Dump( ostream& f) const;

};



template <Dimension DIM, MGSize EQSIZE, MGSize AUXSIZE>
template <class F>
inline Vect<DIM> CFlowCell<DIM,EQSIZE,AUXSIZE>::GradVarOmega( const F& GetVar) const
{
	Vect<DIM>	vct(0.0);

	for ( MGSize k=0; k<=DIM; ++k)
	{
		MGFloat var = GetVar( FEnt::cVar( k ) );
		vct += var * this->Vn( k);
	}

	return vct / (MGFloat)DIM;
}


template <Dimension DIM, MGSize EQSIZE, MGSize AUXSIZE>
template <class F>
inline Vect<DIM> CFlowCell<DIM,EQSIZE,AUXSIZE>::GradVar( const F& GetVar) const
{
	Vect<DIM>	vct(0.0);

	for ( MGSize k=0; k<=DIM; ++k)
	{
		MGFloat var = GetVar( FEnt::cVar( k ) );
		vct += var * this->Vn( k);
	}

	return vct / ( (MGFloat)DIM * this->Volume() );
}



template <Dimension DIM, MGSize EQSIZE, MGSize AUXSIZE>
inline void CFlowCell<DIM,EQSIZE,AUXSIZE>::Dump( ostream& f) const
{
	GCell::Dump( f);
	FEnt::Dump( f, this->Size() );
}





////////////////////////////////////////////////////////////////////////
//// class FluxInegrator
////////////////////////////////////////////////////////////////////////
//template <Dimension DIM, class T>
//class FluxInegrator
//{
//	typedef Vect<DIM>									GVec;
//
//public:
//	FluxInegrator( const CGeomCell<DIM>& gcell, const MGSize& order=1 ) : mGCell(gcell), mOrder(order)	{}
//
//	class FluxFunc
//	{
//	public:
//		T operator() ( const T& u, const Vect<DIM>& vn ) { return u; }
//	};
//
//	template <class FLUX>
//	T	IntegrateFlux( const T tabu[DIM+1] )
//	{
//		ASSERT( mGCell.SizeFace() == mGCell.Size() );
//
//		Geom::GaussQuadrature< static_cast<Dimension>(DIM-1) > gauss( mOrder);
//
//		T res;
//		res.Init(0.);
//
//		for ( MGSize ifac=0; ifac<mGCell.SizeFace(); ++ifac)
//		{
//			GVec vn = -1.0 * mGCell.Vn(ifac);
//			MGFloat area = vn.module();
//			vn *= 1./area;
//
//			T flux;
//			flux.Init(0.);
//
//
//			for ( MGSize ipnt=0; ipnt<gauss.Size(); ++ipnt )
//			{
//				T u = tabu[mGCell.cFaceConn(ifac,0)]*gauss.BaryCoordinates(ipnt)[0];
//				for ( MGSize idim=1; idim<DIM; ++idim)
//					u += tabu[ mGCell.cFaceConn(ifac,idim) ] * gauss.BaryCoordinates(ipnt)[idim];
//
//				FLUX func;
//				flux += func( u, vn) * gauss.Weight(ipnt);
//
//				//for ( MGSize idim=0; idim<DIM; ++idim)
//				//	cout << gauss.BaryCoordinates(ipnt)[idim] << " ";
//				//cout << " -- " << gauss.Weight(ipnt) << endl;
//
//			}
//
//			res += area * flux;
//			//res += flux;
//		}
//
//		return res;
//	}
//
//
//private:
//	const CGeomCell<DIM>& mGCell;
//	MGSize mOrder;
//};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __COMPFLOWCELL_H__
