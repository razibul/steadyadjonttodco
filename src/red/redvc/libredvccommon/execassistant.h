#ifndef __EXECASSISTANT_H__
#define __EXECASSISTANT_H__

#include "libredcore/equation.h"
#include "redvc/libredvccommon/executorbase.h"
#include "redvc/libredvccommon/assistantbase.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class ExecAssistant
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET>
class ExecAssistant : public ExecutorBase<DIM,ET>
{
	enum { ESIZE = EquationDef<DIM,ET>::SIZE };

public:

	static MGString	Info()	
	{
		ostringstream os;
		os << "ExecAssistant< " << Geom::DimToStr(DIM) << ", " << EquationDef<DIM,ET>::NameEqnType() << " >";
		return os.str();
	}

	ExecAssistant() : ExecutorBase<DIM,ET>(), mCount(0), mExecFreq(1), mpAssistant(NULL)	{}
	virtual ~ExecAssistant();

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata);
	virtual void	PostCreateCheck() const;
	virtual void	Init();
	virtual bool	SolveStep( ConvergenceInfo<ESIZE>& info);
	virtual bool	SolveFinal();

private:
	MGSize	mCount;
	MGSize	mExecFreq;
	AssistantBase<DIM,ET>*	mpAssistant;
};
//////////////////////////////////////////////////////////////////////


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void ExecAssistant<DIM,ET>::PostCreateCheck() const
{ 
	ExecutorBase<DIM,ET>::PostCreateCheck();

	if ( ! mpAssistant )
		THROW_INTERNAL( Info() << "::PostCreateCheck() -- failed : mpAssistant -> NULL" );
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EXECASSISTANT_H__
