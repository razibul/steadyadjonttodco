#ifndef __PERIODICBCBASE_H__
#define __PERIODICBCBASE_H__

#include "libcorecommon/key.h"
#include "libredcore/equation.h"
//#include "libextsparse/blockvector.h"
#include "libcoreconfig/configbase.h"
#include "libredcore/configconst.h"
//#include "libcoregeom/vect.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
	//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

	enum PrdBCType {
		PRD_TRANS,
		PRD_ANGLE,
		PRD_NONE
	};

	PrdBCType	StrToPrd(const MGString& str);
	MGString BCToStr(const PrdBCType& prd);

	//////////////////////////////////////////////////////////////////////
	// class PeriodicKeyData
	//////////////////////////////////////////////////////////////////////

	template<Geom::Dimension DIM>
	struct PeriodicKeyData
	{
		PeriodicKeyData() : mcellid(0), mprocid(0)	{}

		MGSize				mcellid;
		Key<DIM, MGFloat>	mtabcoord;
		MGSize				mprocid;		//used only in parallel
	};

	//////////////////////////////////////////////////////////////////////
	// class PeriodicBCBase
	//////////////////////////////////////////////////////////////////////

	template<Geom::Dimension DIM, EQN_TYPE ET>
	class PeriodicBCBase
	{
		enum { ESIZE = EquationDef<DIM, ET>::SIZE };

		typedef EquationDef<DIM, ET>		EqDef;
		typedef typename EqDef::FVec		FVec;

	public:
		virtual void GetPeriodicVar(const MGSize& id, FVec& v) const = 0;
		virtual void UpdateSolution() = 0;
		
		void ReadConfig(const CfgSection* pphysect);
		//virtual ~PeriodicBCBase()	{}

	protected:
		struct PrdData
		{
			PrdData(const MGSize& s, const MGSize& t, const Geom::Vect<DIM>& v, const PrdBCType& typ) : src(s), trg(t), vct(v), type(typ)	{}

			PrdBCType type;
			MGSize src;
			MGSize trg;
			Geom::Vect<DIM> vct;
		};

		vector< PrdData> mparams;
	}; 

	template<Geom::Dimension DIM, EQN_TYPE ET>
	void PeriodicBCBase<DIM, ET>::ReadConfig(const CfgSection* pphysect)
	{
		MGSize prdsize = pphysect->GetSectionCount(ConfigStr::Solver::Physics::PERIODIC::NAME);

		for (MGSize i = 0; i < prdsize; ++i)
		{
			const CfgSection& secprd = pphysect->GetSection(ConfigStr::Solver::Physics::PERIODIC::NAME, i);
			MGSize srcid = secprd.ValueSize(ConfigStr::Solver::Physics::PERIODIC::SRC_ID);
			MGSize trgid = secprd.ValueSize(ConfigStr::Solver::Physics::PERIODIC::TRG_ID);
			MGString type = secprd.ValueString(ConfigStr::Solver::Physics::PERIODIC::Type::KEY);

			PrdBCType prdtype = StrToPrd(type);
			switch (prdtype)
			{
			case PRD_TRANS:
			{
				MGFloat trans[] = {
					secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::VCT_X),
					secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::VCT_Y),
					secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::VCT_Z)
				};
				Geom::Vect<DIM> vct;
				for (MGSize j = 0; j < DIM; ++j)
				{
					vct.rX(j) = trans[j];
				}

				mparams.push_back(PrdData(srcid, trgid, vct, prdtype));
				//FindMapping(srcid, trgid, vct);
				//vct *= -1.;
				//FindMapping(trgid, srcid, vct);
				break;
			}
			case PRD_ANGLE:
				THROW_INTERNAL("Angle periodic bc not implemented.");
				break;
			case PRD_NONE:
				//THROW_INTERNAL("Angle periodic bc not implemented.");
				break;
			default:
				THROW_INTERNAL("Unknown periodic bc type.");
				break;
			}
		}
	}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __PERIODICBCBASE_H__