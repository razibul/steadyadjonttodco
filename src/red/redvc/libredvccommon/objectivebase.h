#ifndef __OBJECTIVEBASE_H__
#define __OBJECTIVEBASE_H__

#include "libcoresystem/mgdecl.h"
#include "libcoreconfig/configbase.h"
#include "libcoregeom/dimension.h"
#include "libredcore/equation.h"
#include "redvc/libredvcgrid/gridsimplex.h"
#include "designparametercollection.h"
#include "solution.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class ObjectiveBase
//////////////////////////////////////////////////////////////////////

template <Geom::Dimension DIM, EQN_TYPE ET>
class ObjectiveBase : public ConfigBase
{
public:
	typedef GridSimplex<DIM> TGrid;
	
	typedef EquationDef<DIM, ET>		EqDef;
	typedef Geom::Vect<DIM>			GVec;
	
	enum { ESIZE = EqDef::SIZE };
	enum { ASIZE = EqDef::AUXSIZE };

	static MGString	Info()	{ return "ObjectiveBase"; }

	ObjectiveBase() : ConfigBase() {};
	virtual ~ObjectiveBase() {};

	virtual void Create( const CfgSection* pcfgsec, const TGrid* pgrd, const DesignParameterCollection<DIM>& desparcoll);
	virtual void PostCreateCheck() const;
	virtual void Init();
	virtual void ComputeGradSolution(const Solution<DIM, ET>& sol) {};
	virtual void ComputeObjective(const Solution<DIM,ET>& sol) {};

	const MGFloat&	cObjective() const	{ return mValue; }
	MGFloat&		rObjective()		{ return mValue; }

	const vector<MGFloat>&	cGradObjective() const	{ return mvecGradObjective; }
	vector<MGFloat>&		rGradObjective()		{ return mvecGradObjective; }

	const MGString&	cDescription() const	{ return mDescription; }

	const bool&		cDoAdjoint() const		{ return mbAdjoint; }
	
	const bool&		cIfWriteAdjTec() const 		{ return mbWriteAdjTec; }
	const bool&		cIfWriteAdjTecSurf() const 		{ return mbWriteAdjTecSurf; }
	
	const Sparse::Vector<ESIZE>&	cGradSolution()	const	{ return mvecGradSolution; }
	Sparse::Vector<ESIZE>&			rGradSolution()			{ return mvecGradSolution; }


protected:
	const TGrid*	mpGrid;
	MGString		mDescription;
	MGFloat			mValue;
	bool			mbAdjoint;
	MGSize			mDesParCollSize;
	bool			mbWriteAdjTec;
	bool			mbWriteAdjTecSurf;
	
	vector<MGFloat>			mvecGradObjective;
	Sparse::Vector<ESIZE>	mvecGradSolution; //using ESIZE (equation size instead of BSIZE from MAPPER); possible problem with splitting eqn.
};

template <Geom::Dimension DIM, EQN_TYPE ET>
void ObjectiveBase<DIM, ET>::Create(const CfgSection* pcfgsec, const TGrid* pgrd, const DesignParameterCollection<DIM>& desparcoll)
{
	ConfigBase::Create( pcfgsec );
	mDescription = ConfigSect().ValueString( ConfigStr::Solver::Objectives::Objective::DESCRIPT );
	mbAdjoint = ConfigSect().ValueBool( ConfigStr::Solver::Objectives::Objective::ADJOINT );
	
	mbWriteAdjTec = false;
	if ( this->ConfigSect().KeyExists(ConfigStr::Solver::Objectives::Objective::WRITE_ADJ_TEC) )
		mbWriteAdjTec = this->ConfigSect().ValueBool( ConfigStr::Solver::Objectives::Objective::WRITE_ADJ_TEC);
	
	mbWriteAdjTecSurf = false;
	if ( this->ConfigSect().KeyExists(ConfigStr::Solver::Objectives::Objective::WRITE_ADJ_TECSURF) )
		mbWriteAdjTecSurf = this->ConfigSect().ValueBool( ConfigStr::Solver::Objectives::Objective::WRITE_ADJ_TECSURF);
	
	mpGrid = pgrd;
	mDesParCollSize = desparcoll.Size();
}

template <Geom::Dimension DIM, EQN_TYPE ET>
void ObjectiveBase<DIM,ET>::PostCreateCheck() const
{
	ConfigBase::PostCreateCheck();
}

template <Geom::Dimension DIM, EQN_TYPE ET>
void ObjectiveBase<DIM,ET>::Init()
{
	mValue = 0;
	mvecGradObjective.resize( mDesParCollSize);
	mvecGradSolution.Resize( mpGrid->SizeNodeTab());
	
}
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif //__OBJECTIVEBASE_H__
