#ifndef __SPACESOLVER_H__
#define __SPACESOLVER_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libredcore/equation.h"

#include "redvc/libredvccommon/spacesolverbase.h"

#include "libredphysics/physics.h"
#include "redvc/libredvccommon/data.h"

#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"

#include "redvc/libredvccommon/flowfunc.h"
#include "redvc/libredvccommon/bccalculator.h"

//#include "redvc/libredvcgrid/periodicbc.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class SpaceSolver
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
class SpaceSolver : public SpaceSolverBase<DIM,ET,MAPPER>
{
public:
	enum { ESIZE = EquationDef<DIM,ET>::SIZE };
	enum { ASIZE = EquationDef<DIM,ET>::AUXSIZE };
	enum { BSIZE = MAPPER::VBSIZE };

	typedef CFlowCell<DIM,ESIZE,ASIZE>		CFCell;
	typedef CFlowBFace<DIM,ESIZE,ASIZE>		CFBFace;

	typedef typename EquationDef<DIM,ET>::FVec	EVec;
	typedef typename EquationDef<DIM,ET>::FMtx	EMtx;

	typedef typename EquationTypes<DIM,BSIZE>::Vec	BVec;
	typedef typename EquationTypes<DIM,BSIZE>::Mtx	BMtx;

	//typedef typename PeriodicBC<DIM>::PeriodicKey	PeriodicKey;
	//typedef typename PeriodicBC<DIM>::ColPeriodic	ColPeriodic;

	static MGString	Info()	
	{
		ostringstream os;
		os << "SpaceSolver< " << Geom::DimToStr(DIM) << ", " << EquationDef<DIM,ET>::NameEqnType() << ", " << BSIZE << " >";
		return os.str();
	}

	SpaceSolver() : SpaceSolverBase<DIM,ET,MAPPER>(), mpFunc(NULL), mRelEpsilon(1.0e-4), mAbsEpsilon(1.0e-30)		{}
	virtual ~SpaceSolver();

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata);
	virtual void	PostCreateCheck() const;
	virtual void	Init();

	virtual void	InitDT( vector<MGFloat>& tabdt, const MGFloat& cfl) const;

	virtual void	Residuum( Sparse::Vector<BSIZE>& vec );
	virtual void	ResJacob( Sparse::Vector<BSIZE>& vec, Sparse::Matrix<BSIZE>& vmtx, const vector<MGFloat>& tabDT);
	virtual void	ResNumJacob( Sparse::Vector<BSIZE>& vec, Sparse::Matrix<BSIZE>& vmtx, const vector<MGFloat>& tabDT, const bool bcorrdiag = true);

	virtual void	ResJacobStrongBC( Sparse::Vector<BSIZE>& vec, Sparse::Matrix<BSIZE>& vmtx );
	virtual void	ApplyStrongBC( Sparse::Vector<BSIZE>& vec );

protected:

	void	ResiduumCell( BVec tabres[], CFCell& fcell, const bool& bjacobian );
	void	ResiduumBFace( BVec tabres[], const CFBFace& fbfac, const bool& bjacobian );

	// analytical jacobian - method common for all ETs
	void	ResAJacobianCell( BVec tabres[], BMtx mtxres[DIM+1][DIM+1], CFCell& fcell );
	void	ResAJacobianBFace( BVec tabres[], BMtx mtxres[DIM+1][DIM+1], CFBFace& fbfac ); 

	// numerical jacobian - method common for all ETs
	void	ResNJacobianCell( BVec tabres[], BMtx mtxres[DIM+1][DIM+1], CFCell& fcell );
	void	ResNJacobianBFace( BVec tabres[], BMtx mtxres[DIM+1][DIM+1], CFBFace& fbfac ); 


private:
	MGFloat						mRelEpsilon;
	MGFloat						mAbsEpsilon;

	FlowFunc<DIM,ET,BSIZE>		*mpFunc;
	BCCalculator<DIM,ET>		mBCCalc;

	//bool	mSmoothDT;
	MGSize	mIterSmoothDT;

};
//////////////////////////////////////////////////////////////////////



template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline SpaceSolver<DIM,ET,MAPPER>::~SpaceSolver()
{
	if ( mpFunc != 0 )
		delete mpFunc;
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void SpaceSolver<DIM,ET,MAPPER>::PostCreateCheck() const
{ 
	SpaceSolverBase<DIM,ET,MAPPER>::PostCreateCheck();

	if ( ! mpFunc )
		THROW_INTERNAL( Info() << "::PostCreateCheck() -- failed : mpFunc -> NULL" );
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void SpaceSolver<DIM,ET,MAPPER>::Init() 
{
	mpFunc->Init( this->cPhysics() );
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void SpaceSolver<DIM,ET,MAPPER>::ResiduumCell( BVec tabres[], CFCell& fcell, const bool& bjacobian )
{
	fcell.SetJacobian( bjacobian);
	mpFunc->CalcResiduum( fcell, tabres);
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __SPACESOLVER_H__
