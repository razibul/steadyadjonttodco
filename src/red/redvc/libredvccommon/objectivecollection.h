#ifndef __OBJECTIVECOLLECTION_H__
#define __OBJECTIVECOLLECTION_H__

#include "libcoresystem/mgdecl.h"
#include "libcoreconfig/configbase.h"
#include "libcoregeom/dimension.h"
#include "libcorecommon/factory.h"
#include "redvc/libredvcgrid/gridsimplex.h"
#include "designparametercollection.h"
#include "objectivebase.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class ObjectiveCollection
//////////////////////////////////////////////////////////////////////

template <Geom::Dimension DIM, EQN_TYPE ET>
class ObjectiveCollection : public ConfigBase
{
	typedef GridSimplex<DIM> TGrid;
public:
	ObjectiveCollection() {};
	virtual ~ObjectiveCollection();

	void Create(const CfgSection* pcfgsec, const TGrid* pGrid, const DesignParameterCollection<DIM>& desparcoll);
	void PostCreateCheck() const;
	void Init();
	void Write(ostream& os = cout) const;

	const ObjectiveBase<DIM,ET>*	operator[] (const MGSize& i) const	{ return mvecObjective[i]; }
	ObjectiveBase<DIM,ET>*			operator[] (const MGSize& i)		{ return mvecObjective[i]; }

	MGSize	Size()	const	{ return mvecObjective.size(); }

private:
	vector<ObjectiveBase<DIM,ET>*> mvecObjective;
	MGString msoutfname;
};

template <Geom::Dimension DIM, EQN_TYPE ET>
void ObjectiveCollection<DIM,ET>::Create(const CfgSection* pcfgsec, const TGrid* pGrid, const DesignParameterCollection<DIM>& desparcoll)
{
	ConfigBase::Create(pcfgsec);
	mvecObjective.clear();

	MGSize count = ConfigSect().GetSectionCount(ConfigStr::Solver::Objectives::Objective::NAME);

	MGString sdim = DimToStr(DIM);
	
	for (MGSize i = 0; i < count; i++)
	{
		const CfgSection* pcfgsec = &ConfigSect().GetSection(ConfigStr::Solver::Objectives::Objective::NAME, i);
		MGString type = pcfgsec->ValueString(ConfigStr::Solver::Objectives::Objective::Type::KEY);
		
		MGString eqtype = EquationDef<DIM, ET>::NameEqnType();

		MGString objkey = "OBJECTIVE_" + type + "_" + eqtype + "_" + sdim;
		cout << "creating '" << objkey << "'" << endl;

		ObjectiveBase<DIM,ET>* ptr = Singleton< Factory< MGString, Creator<ObjectiveBase<DIM,ET> > > >::GetInstance()->GetCreator(objkey)->Create();
		ptr->Create( pcfgsec, pGrid, desparcoll );
		
		mvecObjective.push_back(ptr);
	}

	if ( this->ConfigSect().KeyExists(ConfigStr::Solver::Objectives::OUTFNAME) )
		msoutfname = this->ConfigSect().ValueString( ConfigStr::Solver::Objectives::OUTFNAME );
	else
		msoutfname = "_out_obj.txt";
	
}

template <Geom::Dimension DIM, EQN_TYPE ET>
void ObjectiveCollection<DIM,ET>::PostCreateCheck() const
{
	ConfigBase::PostCreateCheck();

	if (mvecObjective.size() == 0)
		THROW_INTERNAL("ObjectiveCollection<DIM>::PostCreateCheck() -- failed : 'mvecObjective.size() == 0'");

	for (MGSize i = 0; i < mvecObjective.size(); i++)
		if (!mvecObjective[i])
			THROW_INTERNAL("ObjectiveCollection<DIM>::PostCreateCheck() --failed : 'mvecObjective[ " << i << "] == NULL'");
	
	for (MGSize i = 0; i < mvecObjective.size(); i++)
		mvecObjective[i]->PostCreateCheck();

}

template <Geom::Dimension DIM, EQN_TYPE ET>
void ObjectiveCollection<DIM,ET>::Init()
{
	for (MGSize i = 0; i < mvecObjective.size(); i++)
		mvecObjective[i]->Init();

}

template <Geom::Dimension DIM, EQN_TYPE ET>
void ObjectiveCollection<DIM, ET>::Write(ostream& os) const
{
	os.setf( ios::scientific );
	if (&os == &cout)
		os << "Objective: ";
	else
		os << Size() << endl;

	for (MGSize i = 0; i < mvecObjective.size(); i++)
		os << setw(24) << setprecision(16) << mvecObjective[i]->cObjective() << " " << mvecObjective[i]->cDescription() << endl;


	if (&os == &cout)
		os << "Objective gradients:" << endl;
	else
	{
		os << mvecObjective[0]->cGradObjective().size() << endl;
		os << Size() << endl;
	}

	for (MGSize i = 0; i < mvecObjective.size(); i++)
	{
		if (mvecObjective[i]->cDoAdjoint())
			os << " " << mvecObjective[i]->cDescription();
	}
	os << endl; 

	for (MGSize i = 0; i < mvecObjective[0]->cGradObjective().size(); i++)
	{
		for (MGSize j = 0; j < Size(); j++)
		{
			if (mvecObjective[j]->cDoAdjoint())
				os << setw(24) << setprecision(16) << mvecObjective[j]->cGradObjective()[i] << " ";
		}
		os << endl;
	}

	//os.setf( ios::scientific );
	//if (&os == &cout)
	//	os << "Objective: ";
	//for (MGSize i = 0; i < mvecObjective.size(); i++)
	//{
	//	if (&os == &cout)
	//		os << mvecObjective[i]->cDescription() << " ";
	//	os << setw(22) << setprecision(16) << mvecObjective[i]->cObjective() << " ";
	//}
	//os << endl;
	//
	//if (&os == &cout)
	//	os << "Objective gradients:" << endl;
	//for (MGSize i = 0; i < mvecObjective[0]->cGradObjective().size(); i++)
	//{
	//	for (MGSize j = 0; j < mvecObjective.size(); j++)
	//		os << setw(22) << setprecision(16) << mvecObjective[j]->cGradObjective()[i] << " ";
	//	os << endl;
	//}
	//os << endl;
}

template <Geom::Dimension DIM, EQN_TYPE ET>
ObjectiveCollection<DIM, ET>::~ObjectiveCollection()
{
	for (typename vector< ObjectiveBase<DIM, ET> * >::iterator itr = mvecObjective.begin(); itr != mvecObjective.end(); ++itr)
		if (*itr != 0)
			delete *itr;

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif //__OBJECTIVECOLLECTION_H__
