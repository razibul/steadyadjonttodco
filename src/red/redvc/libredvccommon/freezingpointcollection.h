#ifndef __FREEZINGPOINTCOLLECTION_H__
#define __FREEZINGPOINTCOLLECTION_H__

#include "libcoreconfig/configbase.h"

#include "libcoregeom/dimension.h"
#include "libcoresystem/mgdecl.h"
#include "libcoregeom/vect.h"

#include "redvc/libredvcgrid/gridsimplex.h"
#include "freezingpoint.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
// class Freezing Points Collection
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class FreezingPointCollection : public ConfigBase
{
public:
	typedef GridSimplex<DIM>	TGrid;
	typedef Geom::Vect<DIM>		GVec;
	
	FreezingPointCollection() {};
	virtual ~FreezingPointCollection() {};
	
	static MGString	Info()
	{
		ostringstream os;
		os << "FreezingPointCollection<" << Geom::DimToStr(DIM) << ">";
		return os.str();
	}
	
	virtual void Create(const CfgSection* pcfgsec, const TGrid *pgrd);
	virtual void PostCreateCheck() const;
	virtual void Init();
	
	const MGSize	Size()	const	{ return mvecFreezingPoint.size(); }
	
	virtual void FindGlobalNearestNode(const MGSize& indx, const MGFloat& dist, const GVec& pos, MGSize* owner_rank, MGSize* owner_indx, GVec* owner_pos)
		{ THROW_INTERNAL("FreezingPointCollection::FindGlobalNearestNode -- not implemented in sequential version !!"); };
	
	const FreezingPoint<DIM>&	operator[] (const MGSize& i) const { return mvecFreezingPoint[i]; }
	
	const vector<FreezingPoint<DIM> >&	cvecFreezingPoint() const	{ return mvecFreezingPoint; }
	
protected:
	
	const TGrid* mpGrid;
	
	vector<FreezingPoint<DIM> >	mvecFreezingPoint;
	
	
};


template <Dimension DIM>
void FreezingPointCollection<DIM>::Create(const CfgSection* pcfgsec, const TGrid *pgrd)
{
	ConfigBase::Create(pcfgsec);
	mpGrid = pgrd;
	
}

template <Dimension DIM>
void FreezingPointCollection<DIM>::PostCreateCheck() const
{
	
	
}


template <Dimension DIM>
void FreezingPointCollection<DIM>::Init()
{
	//const CfgSection& secfreeze = this->ConfigSect().GetSection(ConfigStr::Solver::FreezePoint::NAME);
	CfgSection::const_iterator_rec	citrr;
	for (citrr = this->ConfigSect().begin_rec(); citrr != this->ConfigSect().end_rec(); ++citrr)
	{
		cout << ProcessInfo::Prompt() << (*citrr).first << " " << (*citrr).second << endl;
		MGFloat x = std::atof((*citrr).first.c_str());
		MGFloat y = std::atof((*citrr).second.c_str());
		//mvecFreezingPoints.push_back(GVec(x, y));
		
		MGFloat dist;
		MGSize indx = mpGrid->FindNearestNode( GVec(x,y) , dist );
		GVec pos = mpGrid->cNode(indx).cPos();
		
		GVec vec_temp ( mpGrid->cNode(indx).cPos() );
		mvecFreezingPoint.push_back( FreezingPoint<DIM>( vec_temp, ProcessInfo::cProcId(), indx ) );
	
	}
	
	ofstream offreezepts("_log_freezept.txt");
	for (MGSize i = 0; i < Size(); i++)
	{
		mvecFreezingPoint[i].Write( offreezepts);
	}
	
}

// template <Dimension DIM>
// void FreezingPointCollection<DIM>::FindGlobalNearestNode(const MGSize& indx, const MGFloat& dist, const GVec& pos, MGSize* owner_rank, MGSize* owner_indx, GVec* owner_pos)
// {
// 	vector<MGSize>	tabsendindx( ProcessInfo::cNumProc(), indx );
// 	vector<MGFloat>	tabsenddist( ProcessInfo::cNumProc(), dist );
// 	
// 	vector<MGSize>	tabrecvindx( ProcessInfo::cNumProc(), indx );
// 	vector<MGFloat>	tabrecvdist( ProcessInfo::cNumProc(), dist );
// 	
// 	MPI_Alltoall( &tabsendindx[0], 1, MPI_UNSIGNED, &tabrecvindx[0], 1, MPI_UNSIGNED, MPI_COMM_WORLD);
// 	MPI_Alltoall( &tabsenddist[0], 1, MPI_DOUBLE  , &tabrecvdist[0], 1, MPI_DOUBLE  , MPI_COMM_WORLD);
// 	
// 	MGFloat mindist;
// 	MGSize  minindx,minprocid;
// 	
// 	mindist = tabrecvdist[0];
// 	minindx = tabrecvindx[0];
// 	minprocid = 0;
// 	for (MGSize i=0; i<tabrecvdist.size(); i++)
// 	{
// 		if ( mindist > tabrecvdist[i] )
// 		{
// 			mindist = tabrecvdist[i];
// 			minindx = tabrecvindx[i];
// 			minprocid = i;
// 		}
// 		
// 	}
// 	
// 	*owner_indx = minindx;
// 	*owner_rank = minprocid;
// 	
// 	MGFloat x,y;
// 	if (ProcessInfo::cProcId() == *owner_rank)
// 	{
// 		x = pos.cX();
// 		y = pos.cY();
// 	}
// 	
// 	MPI_Bcast(&x,1,MPI_DOUBLE, *owner_rank, MPI_COMM_WORLD);
// 	MPI_Bcast(&y,1,MPI_DOUBLE, *owner_rank, MPI_COMM_WORLD);
// 	
// 	*owner_pos = GVec(x,y);
// 	
// 	
// }

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif //__FREEZINGPOINTCOLLECTION_H__