#ifndef __SPACESOLVERBASE_H__
#define __SPACESOLVERBASE_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libredcore/equation.h"

#include "libcoreconfig/configbase.h"
#include "libredconvergence/spacesolverfacade.h"

#include "libredphysics/physics.h"
#include "redvc/libredvccommon/data.h"

#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"

#include "redvc/libredvccommon/flowfunc.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class SpaceSolverBase
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
class SpaceSolverBase : public SpaceSolverFacade<MAPPER::VBSIZE>, public ConfigBase
{
public:
	enum { ESIZE = EquationDef<DIM,ET>::SIZE };
	enum { ASIZE = EquationDef<DIM,ET>::AUXSIZE };
	enum { BSIZE = MAPPER::VBSIZE };

	typedef CFlowCell<DIM,ESIZE,ASIZE>		CFCell;
	typedef CFlowBFace<DIM,ESIZE,ASIZE>		CFBFace;

	typedef typename EquationDef<DIM,ET>::FVec		EVec;
	typedef typename EquationDef<DIM,ET>::FGradVec	EGradVec;
	typedef typename EquationDef<DIM,ET>::FMtx		EMtx;

	typedef typename EquationTypes<DIM,BSIZE>::Vec		BVec;
	typedef typename EquationTypes<DIM,BSIZE>::Mtx		BMtx;



	static MGString	Info()	
	{
		ostringstream os;
		os << "SpaceSolverBase< " << Geom::DimToStr(DIM) << ", " << EquationDef<DIM,ET>::NameEqnType() << ", " << BSIZE << " >";
		return os.str();
	}

	SpaceSolverBase() : ConfigBase()		{}
	virtual ~SpaceSolverBase()		{}

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata);
	virtual void	PostCreateCheck() const;
	virtual void	Init() {}

	//virtual void	Residuum( Sparse::Vector<BSIZE>& vec );
	//virtual void	ResNumJacob( Sparse::Vector<BSIZE>& vec, Sparse::Matrix<BSIZE>& vmtx, const vector<MGFloat>& tabDT);

	virtual MGSize	SolutionSize() const					{ return cData().cGrid().SizeNodeTab();}
	virtual void	InitDT( vector<MGFloat>& tabdt, const MGFloat& cfl) const	{}
	virtual void	JacobianPattern() const					{}

	virtual void	ResizeVct( Sparse::Vector<BSIZE>& vct) const;
	virtual void	ResizeMtx( Sparse::Matrix<BSIZE>& mtx) const;


protected:
	const Physics<DIM,ET>&	cPhysics() const	{ return *mpPhysics;}
	const Data<DIM,ET>&		cData() const		{ return *mpData;}

	//Data<DIM, ET>&			rData()				{ return *mpData; }

	void	GetFCell( CFCell& fcell, const MGSize& i ) const;
	void	GetFBFace( CFBFace& fbface, const MGSize& i ) const;


	//void	ResiduumCell( BVec tabres[], const CFCell& fcell );
	//void	ResiduumBFace( BVec tabres[], const CFBFace& fbfac );

	//// numerical jacobian - method common for all ETs
	//void	ResNJacobianCell( BVec tabres[], BMtx mtxres[DIM+1][DIM+1], CFCell& fcell );
	//void	ResNJacobianBFace( BVec tabres[], BMtx mtxres[DIM+1][DIM+1], CFBFace& fbfac ); 

private:
	Physics<DIM,ET>*			mpPhysics;
	Data<DIM,ET>*				mpData;
};
//////////////////////////////////////////////////////////////////////



template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void SpaceSolverBase<DIM,ET,MAPPER>::Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata)
{
	ConfigBase::Create( pcfgsec);

	mpPhysics = pphys;
	mpData = pdata;
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void SpaceSolverBase<DIM,ET,MAPPER>::PostCreateCheck() const
{ 
	ConfigBase::PostCreateCheck();


	if ( ! mpPhysics )
		THROW_INTERNAL( Info() << "::PostCreateCheck() -- failed : mpPhysics -> NULL" );

	if ( ! mpData )
		THROW_INTERNAL( Info() << "::PostCreateCheck() -- failed : mpData -> NULL" );
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void SpaceSolverBase<DIM,ET,MAPPER>::GetFCell( CFCell& fcell, const MGSize& i ) const
{
	cData().cGrid().GetCell( i, fcell );
	for ( MGSize i=0; i<fcell.Size(); ++i)
	{
		fcell.rVar(i) = EVec( cData().cSolution()[ fcell.cId(i) ] ) ;
		fcell.rAux(i) = typename EquationDef<DIM,ET>::AVec( cData().cAuxData()[ fcell.cId(i) ] ) ;
	}

	if ( cData().cSolution().Size() == cData().cSolGradient().Size() )
		for ( MGSize i=0; i<fcell.Size(); ++i)
			fcell.rGradVar(i) = EGradVec( cData().cSolGradient()[ fcell.cId(i) ] ) ;
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void SpaceSolverBase<DIM,ET,MAPPER>::GetFBFace( CFBFace& fbface, const MGSize& i ) const
{ 
	cData().cGrid().GetBFace( i, fbface);
	for ( MGSize i=0; i<fbface.Size(); ++i)
	{
		fbface.rVar(i) = EVec( cData().cSolution()[ fbface.cId(i) ] );
		fbface.rAux(i) = typename EquationDef<DIM,ET>::AVec( cData().cAuxData()[ fbface.cId(i) ] ) ;
	}

	if ( cData().cSolution().Size() == cData().cSolGradient().Size() )
		for ( MGSize i=0; i<fbface.Size(); ++i)
			fbface.rGradVar(i) = EGradVec( cData().cSolGradient()[ fbface.cId(i) ] );
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void SpaceSolverBase<DIM,ET,MAPPER>::ResizeVct( Sparse::Vector<BSIZE>& vct) const
{
	vct.Resize( cData().cGrid().SizeNodeTab() );
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void SpaceSolverBase<DIM,ET,MAPPER>::ResizeMtx( Sparse::Matrix<BSIZE>& mtx) const
{
	CGeomCell<DIM>	cgcell;

	mtx.Resize( cData().cGrid().SizeNodeTab() );



	for ( MGSize i=0; i<cData().cGrid().SizeCellTab(); ++i)
	{
		cgcell = cData().cGrid().cCell( i);

		for ( MGSize j=0; j<cgcell.Size(); ++j)
			for ( MGSize k=0; k<cgcell.Size(); ++k)
				mtx.InsertBlock( cgcell.cId(j), cgcell.cId(k) );

	}
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __SPACESOLVERBASE_H__
