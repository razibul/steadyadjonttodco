#include "designparametercollection.h"
#include "redvc/libredvcgrid/grid.h"
#include "redvc/libredvcgrid/gridsimplex.h"
#include "libredcore/processinfo.h"
#include "libcoreio/store.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
void DesignParameterCollection<DIM>::Create(const CfgSection* pcfgsec, const TGrid* pgrd)
{
	ConfigBase::Create(pcfgsec);
	mpGrid = pgrd;
	mSize = 0;

	// initializing number of parameters on each surface
	const CfgSection& secsurf = this->ConfigSect().GetSection( ConfigStr::Solver::DesignParameter::SURF );
	CfgSection::const_iterator_rec	citrr;
	for (citrr = secsurf.begin_rec(); citrr != secsurf.end_rec(); ++citrr)
	{
		cout << (*citrr).first << " " << (*citrr).second << endl;
		MGInt val = std::atoi( (*citrr).second.c_str());
		MGInt id = std::atoi( (*citrr).first.c_str() );
		mmapSurf.insert(std::make_pair(id, val));
		
		mSize += val;

	}


};

template <Dimension DIM>
void DesignParameterCollection<DIM>::PostCreateCheck() const
{
	ConfigBase::PostCreateCheck();

};

template <Dimension DIM>
void DesignParameterCollection<DIM>::Init()
{
	mvecDesignParameters.clear();
	mvecDesignParameters.reserve(mSize);
	
	if ( this->ConfigSect().KeyExists( ConfigStr::Solver::DesignParameter::FNAME ) )
	{
		MGString fname = this->ConfigSect().ValueString( ConfigStr::Solver::DesignParameter::FNAME);
		
		ifstream ifile;
		ifile.open( fname.c_str(), ios::in );
//		MGFloat x,y;
		MGSize size;
		istringstream is;

		try
		{
			if ( ! ifile)
				THROW_FILE( "can not open the file", fname);
			
			cout << "start reading '" << fname << "'";
			if ( ProcessInfo::IsParallel() )
				cout << " - from " << ProcessInfo::cProcId() << endl;
			else
				cout << endl;
			
			IOSpace::ReadLine( ifile , is );
			is >> size;
			cout << "------ size ----- " << size << endl;
			if ( size != mSize )
			{
				THROW_FILE("DesignParameterCollection<DIM>::Init() -- dimension mismatch",fname);
			}
			else
			{
				for (MGSize i=0; i<size; i++)
				{
					
					IOSpace::ReadLine( ifile , is );
					MGFloat tabx[DIM];
					for (MGSize j = 0; j < DIM; j++)
						is >> tabx[j];
					cout << ProcessInfo::Prompt() << i << " of " << size << " | ";
					for (MGSize j = 0; j < DIM; j++)
						cout << tabx[j] << " ";
					cout << endl;

					GVec vectmp;
					for (MGSize j = 0; j < DIM; j++)
						vectmp.rX(j) = tabx[j];

					const Node<DIM> nodetmp( vectmp);
					mvecDesignParameters.push_back( DesignParameter<DIM>( nodetmp , -1 ) );
					
				}
			}
			
			
			cout << "-- reading finished '" << fname << "'";
			if ( ProcessInfo::IsParallel() )
				cout << " - from " << ProcessInfo::cProcId() << endl;
			else
				cout << endl;
			
			
		}
		catch ( EHandler::Except& e)
		{
			cout << "ERROR: DesignParameterCollection<DIM>::Init - reading file name: " << fname << endl << endl;
			TRACE_EXCEPTION( e);
			TRACE_TO_STDERR( e);
	
			THROW_INTERNAL("END");
		}
		
	}
	else
	{
		if (ProcessInfo::IsParallel())
		{
			THROW_INTERNAL("DesignParameterCollection<DIM>::Init() -- design parameter file MUST be specified in parallel version.");
		}
		else
		{
			map<MGInt,MGInt>::iterator itr;
			for (itr = mmapSurf.begin(); itr != mmapSurf.end(); ++itr )
			{
				MGSize surfid = (*itr).first;
				MGSize size = (*itr).second;

				if ( size != 0 )
				{
				// run function that creates N points equidistributed on surface surfid
					vector<MGSize> nodeids = (*mpGrid).FindFirstLastSurfBNode(surfid);
					MGSize count = (nodeids[1] - nodeids[0]) / size;
					
					for ( MGSize i=0; i<size ; i++)
					{
					MGSize bnodeid = (*mpGrid).cBFace( nodeids[0] + i*count).cBId(0);
					MGSize nodeid = (*mpGrid).cBNode(bnodeid).cPId();
					Node<DIM> nd = (*mpGrid).cNode(nodeid);
					mvecDesignParameters.push_back( DesignParameter<DIM>(nd,surfid) );
					
					}
				}
		}
		}
	}

	if (this->ConfigSect().KeyExists(ConfigStr::Solver::DesignParameter::INITVALFNAME))
	{
		MGString fname = this->ConfigSect().ValueString(ConfigStr::Solver::DesignParameter::INITVALFNAME);

		ifstream ifile;
		ifile.open(fname.c_str(), ios::in);
		//		MGFloat x,y;
		MGSize size;
		istringstream is;

		try
		{
			if (!ifile)
				THROW_FILE("can not open the file", fname);

			cout << "start reading '" << fname << "'";
			if (ProcessInfo::IsParallel())
				cout << " - from " << ProcessInfo::cProcId() << endl;
			else
				cout << endl;

			IOSpace::ReadLine(ifile, is);
			is >> size;
			cout << "------ size ----- " << size << endl;
			if (size != mSize)
			{
				THROW_FILE("DesignParameterCollection<DIM>::Init() -- dimension mismatch", fname);
			}
			else
			{
				for (MGSize i = 0; i<size; i++)
				{
					MGFloat value;
					IOSpace::ReadLine(ifile, is);
					is >> value;
					cout << i << " of " << size << " | " << value << endl;
					mvecDesignParameters[i].rValue() = value;

				}
			}

			cout << "-- reading finished '" << fname << "'";
			if (ProcessInfo::IsParallel())
				cout << " - from " << ProcessInfo::cProcId() << endl;
			else
				cout << endl;


		}
		catch (EHandler::Except& e)
		{
			cout << "ERROR: DesignParameterCollection<DIM>::Init - reading file name: " << fname << endl << endl;
			TRACE_EXCEPTION(e);
			TRACE_TO_STDERR(e);

			THROW_INTERNAL("END");
		}
	}
	else
	{
		MGFloat value = 0.0;
		if (this->ConfigSect().KeyExists(ConfigStr::Solver::DesignParameter::INITVAL))
			value = this->ConfigSect().ValueFloat(ConfigStr::Solver::DesignParameter::INITVAL);

		ASSERT(mSize == mvecDesignParameters.size());
		for (MGSize i = 0; i < mSize; i++)
		{
			mvecDesignParameters[i].rValue() = value;
		}
			
	}

	if ( ProcessInfo::cProcId() == 0 )
	{
		
		ofstream ofdespar("_log_despar.txt");
		ofdespar << mSize << endl;
		for (MGSize i = 0; i < mvecDesignParameters.size(); i++)
		{
			ofdespar << mvecDesignParameters[i].cNode().cPos().cX() << " " << mvecDesignParameters[i].cNode().cPos().cY() << endl;
		}
		ofdespar.close();

	}

};
template <Dimension DIM>
const vector<MGFloat>& DesignParameterCollection<DIM>::cVecValues() 
{
	ASSERT(mSize == mvecDesignParameters.size());
	
	mvecDesignParametersValues.resize(mSize,0.0);
	for (MGSize i = 0; i < mvecDesignParameters.size(); i++)
	{
		mvecDesignParametersValues[i] = mvecDesignParameters[i].cValue();
	}
	return mvecDesignParametersValues;
}
template <Dimension DIM>
void DesignParameterCollection<DIM>::FindGlobalNearestNode(const MGSize& indx, const MGFloat& dist, const GVec& pos, MGSize* owner_rank, MGSize* owner_indx, GVec* owner_pos)
{
	
	
	
}

template <Dimension DIM>
void DesignParameterCollection<DIM>::DistributeParameters()
{
    
    
}


template class DesignParameterCollection<DIM_2D>;
template class DesignParameterCollection<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

