#include "execassistant.h"
#include "libcorecommon/factory.h"
#include "libredcore/configconst.h"
#include "libcoreconfig/cfgsection.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Geom::Dimension DIM, EQN_TYPE ET>
ExecAssistant<DIM,ET>::~ExecAssistant()	
{
	if ( mpAssistant != 0)
		delete mpAssistant;
}


template <Geom::Dimension DIM, EQN_TYPE ET>
void ExecAssistant<DIM,ET>::Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata)
{ 
	ExecutorBase<DIM,ET>::Create( pcfgsec, pphys, pdata);

	MGString sdim = Geom::DimToStr( DIM);
	MGString seqtype = EquationDef<DIM,ET>::NameEqnType();

	MGString stype	= this->ConfigSect().ValueString( ConfigStr::Solver::Executor::Assistant::TYPE );

	if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Executor::Assistant::FREQ ) )
		mExecFreq = this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::Assistant::FREQ );
	else
		mExecFreq = 1;

	// create and init SpaceSolver
	const CfgSection* pcfgast = & this->ConfigSect().GetSection( stype );


	//MGString sssoltype = pspcsol->ValueString( ConfigStr::Solver::ExecAssistant::SpaceSol::Type::KEY );

	MGString astkey = stype + "_" + sdim + "_" + seqtype;
	//cout << "creating '" << astkey << "'" << endl;

	mpAssistant = Singleton< Factory< MGString,Creator< AssistantBase<DIM,ET> > > >::GetInstance()->GetCreator( astkey )->Create();
	mpAssistant->Create( pcfgast, pphys, pdata);
}


template <Geom::Dimension DIM, EQN_TYPE ET>
void ExecAssistant<DIM,ET>::Init()
{
	if ( !mpAssistant )
		THROW_INTERNAL( "mpAssistant - not created!");

	mCount = 0;
	mpAssistant->Init();
}


template <Geom::Dimension DIM, EQN_TYPE ET>
bool ExecAssistant<DIM, ET>::SolveFinal()
{
	if (ProcessInfo::cProcId() == 0)
		cout << ProcessInfo::Prompt() << Info() << endl;

	//mpAssistant->DoFinal();

	return true;
}


template <Geom::Dimension DIM, EQN_TYPE ET>
bool ExecAssistant<DIM,ET>::SolveStep( ConvergenceInfo<ESIZE>& info)
{
	if ( ProcessInfo::cProcId() == 0 )
		cout << ProcessInfo::Prompt() << Info() << endl;
	
	if ( ++mCount % mExecFreq == 0 )
		mpAssistant->Do();

	return true;
}



void init_ExecAssistant()
{

	//////////////////////////////////////////////////////////////////////
	// 2D

	//------------------------------------------------------------------//
	static ConcCreator< 
		MGString, 
		ExecAssistant< Geom::DIM_2D, EQN_POISSON >, 
		ExecutorBase<Geom::DIM_2D,EQN_POISSON> 
	>	gCreatorExecAssistantPoissonFull2D( "EXECUTOR_2D_POISSON_ASSISTANT");

	static ConcCreator< 
		MGString, 
		ExecAssistant< Geom::DIM_2D, EQN_NS >, 
		ExecutorBase<Geom::DIM_2D,EQN_NS> 
	>	gCreatorExecAssistantNSFull2D( "EXECUTOR_2D_NS_ASSISTANT");

	//------------------------------------------------------------------//
	static ConcCreator< 
		MGString, 
		ExecAssistant< Geom::DIM_2D, EQN_RANS_SA >, 
		ExecutorBase<Geom::DIM_2D,EQN_RANS_SA> 
	>	gCreatorExecAssistantRansSAFull2D( "EXECUTOR_2D_RANS_SA_ASSISTANT");


	//////////////////////////////////////////////////////////////////////
	// 3D

	//------------------------------------------------------------------//
	static ConcCreator< 
		MGString, 
		ExecAssistant< Geom::DIM_3D, EQN_NS >, 
		ExecutorBase<Geom::DIM_3D,EQN_NS> 
	>	gCreatorExecAssistantNSFull3D( "EXECUTOR_3D_NS_ASSISTANT");

	//------------------------------------------------------------------//
	static ConcCreator< 
		MGString, 
		ExecAssistant< Geom::DIM_3D, EQN_RANS_SA >, 
		ExecutorBase<Geom::DIM_3D,EQN_RANS_SA> 
	>	gCreatorExecAssistantRansSAFull3D( "EXECUTOR_3D_RANS_SA_ASSISTANT");

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

