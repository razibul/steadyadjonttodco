#ifndef __ADJOINTBASE_H__
#define __ADJOINTBASE_H__

#include "redvc/libredvccommon/executorbase.h"
#include "libredconvergence/timesolverbase.h"
#include "redvc/libredvccommon/spacesolverbase.h"
#include "redvc/libredvccommon/objectivecollection.h"
#include "solution.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
class ExecAdjointBase : public ExecutorBase<DIM,ET>
{
public:
	typedef EquationDef<DIM, ET> EqDef;
	
	enum { ESIZE = EqDef::SIZE };
	enum { ASIZE = EqDef::AUXSIZE };
	enum { BSIZE = MAPPER::VBSIZE };
	
	ExecAdjointBase() : mpTimeSol(NULL), mpSpaceSol(NULL), mpObjectiveColl(NULL), mpDataAdj(NULL) {};
	virtual ~ExecAdjointBase();
	
	virtual void	Create(const CfgSection* pcfgsec, Physics<DIM, ET>* pphys, Data<DIM, ET>* pdata);
// 	virtual void	Create(const CfgSection* pcfgsec, Physics<DIM, ET>* pphys, Data<DIM, ET>* pdata, const CfgSection* pcfgsectimsol, const CfgSection* pcfgsecspacesol);
	virtual void	PostCreateCheck() const  = 0;
	virtual void	Init() = 0;
	virtual bool	SolveStep( ConvergenceInfo<ESIZE>& info) = 0;
	virtual bool	SolveFinal() = 0;
	
	const DataAdjoint<DIM,ET>&			cDataAdj() const	{ return *mpDataAdj;}
	DataAdjoint<DIM,ET>&		rDataAdj()			{ return *mpDataAdj;}
	
protected:
	ObjectiveCollection<DIM,ET>*	mpObjectiveColl;
	
	TimeSolverBase<BSIZE>*	mpTimeSol;
	SpaceSolverBase<DIM, ET, MAPPER>*	mpSpaceSol;
	
	const CfgSection* mpcfgsectimsol;
	const CfgSection* mpcfgsecspcsol;
	
	map<MGInt, bool> mmapBC;
	MGFloat		mh;
	
	DataAdjoint<DIM,ET>* mpDataAdj;
	
private:
	virtual void	ResiduumGradSol() = 0;
	virtual void	SolveAdjointEqn() = 0;
	virtual void	SolveTangentEqn() = 0;

	virtual void	ResiduumGradDesign() = 0;
	virtual MGFloat	EvaluateFunctional(const Solution<DIM,ET>& vec, const MGSize& i) = 0;
	virtual void	LocalToGlobal() = 0;
	
	
};

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
ExecAdjointBase<DIM,ET,MAPPER>::~ExecAdjointBase()
{
	if ( mpTimeSol  )
		delete mpTimeSol;
	
	if ( mpSpaceSol )
		delete mpSpaceSol;
	
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void ExecAdjointBase<DIM,ET,MAPPER>::Create(const CfgSection* pcfgsec, Physics<DIM, ET>* pphys, Data<DIM, ET>* pdata)
{
	//ExecutorBase<DIM,ET,MAPPER>::Create(pcfgsec,pphys,pdata,pcfgsectimsol,pcfgsecspacesol);
	ExecutorBase<DIM,ET>::Create(pcfgsec,pphys,pdata);
	mpDataAdj = dynamic_cast< DataAdjoint<DIM,ET>* >( this->mpData );
	if ( mpDataAdj == NULL )	THROW_INTERNAL("ExecAdjointBase::Create -- mpDataAdj was not properly initialized with mpData !!!");
	
	mpcfgsectimsol = & this->ConfigSect().GetSection( ConfigStr::Solver::Executor::TimeSol::NAME );
	mpcfgsecspcsol = & this->ConfigSect().GetSection( ConfigStr::Solver::Executor::SpaceSol::NAME );
	
	MGString sdim = Geom::DimToStr(DIM);
	MGString seqtype = EquationDef<DIM, ET>::NameEqnType();


	MGString sssoltype = mpcfgsecspcsol->ValueString(ConfigStr::Solver::Executor::SpaceSol::Type::KEY);
	MGString sschemetype = mpcfgsecspcsol->ValueString(ConfigStr::Solver::Executor::SpaceSol::Type::KEY);
	MGString stsoltype = mpcfgsectimsol->ValueString(ConfigStr::Solver::Executor::TimeSol::Type::KEY);


	ostringstream os;
	os << BSIZE;
	MGString sbsize = os.str();

	MGString ssprefix = "";
	if ( stsoltype == MGString( ConfigStr::Solver::Executor::TimeSol::Type::Value::PETSC ) )
		ssprefix = "MPIPETSC_";

	//create and init SpaceSolver
	MGString spcsolkey = ssprefix + "SPACESOL_" + sdim + "_" + seqtype + "_" + sschemetype;

	cout << "creating '" << spcsolkey << "'" << endl;
	mpSpaceSol = Singleton< Factory< MGString, Creator< SpaceSolverBase<DIM, ET, MAPPER> > > >::GetInstance()->GetCreator(spcsolkey)->Create();
	mpSpaceSol->Create(mpcfgsecspcsol, pphys, pdata);

	// create and init TimeSolver
	MGString timsolkey = "TIMESOL_BSIZE_" + sbsize + "_" + stsoltype;
	cout << "creating '" << timsolkey << "'" << endl;

	mpTimeSol = Singleton< Factory< MGString, Creator< TimeSolverBase<BSIZE> > > >::GetInstance()->GetCreator(timsolkey)->Create();
	mpTimeSol->Create(mpcfgsectimsol, mpSpaceSol);

	mpObjectiveColl = &(this->rDataAdj().rObjectiveCollection());
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void ExecAdjointBase<DIM,ET,MAPPER>::PostCreateCheck() const
{
	if (!mpObjectiveColl)
		THROW_INTERNAL("AdjointBase<DIM,ET,MAPPER>::mpObjectiveColl not created !");
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//



#endif //__ADJOINTBASE_H__