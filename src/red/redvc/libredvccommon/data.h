#ifndef __DATA_H__
#define __DATA_H__

//#include "redvc/libredvcgrid/grid.h"
#include "redvc/libredvccommon/periodicbc.h"
//#include "redvc/libredvcgrid/gridsimplex.h"
#include "redvc/libredvccommon/auxdata.h"
//#include "redvc/libredvccommon/solution.h"
#include "redvc/libredvccommon/solgradient.h"
#include "libredcore/equation.h"
#include "libredcore/interface.h"
#include "libredcore/processinfo.h"
#include "libredcore/configconst.h"
#include "libcoreio/readmsh2.h"
#include "libcoreio/readintf.h"
#include "libcorecommon/factory.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Data
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET>
class Data
{
public:
	//typedef Grid<DIM> TGrid;
	typedef GridSimplex<DIM> TGrid;

	enum { ESIZE = EquationDef<DIM,ET>::SIZE };
	enum { ASIZE = EquationDef<DIM,ET>::AUXSIZE };


	Data() : mpPeriodic(NULL) {}

	~Data()
	{
		if ( mpPeriodic) delete mpPeriodic;
	}

	virtual void Create(const CfgSection* pgrdcfg, const CfgSection* psolcfg, const CfgSection* pauxcfg, const Physics<DIM, ET>* pphys);
	virtual void PostCreateCheck() const;
	virtual void Init();

	
	const TGrid&			cGrid() const		{ return mGrid;}
	TGrid&					rGrid()				{ return mGrid; }

	
	const Interface&		cInterface() const	{ return mInterface;}

	
	const Solution<DIM,ET>&	cSolution() const	{ return mSolution;}
	Solution<DIM,ET>&		rSolution()			{ return mSolution;}

	const SolGradient<DIM,ET>&	cSolGradient() const	{ return mSolGradient;}
	SolGradient<DIM,ET>&		rSolGradient()			{ return mSolGradient;}
	
	const AuxData<ASIZE>&	cAuxData() const	{ return mAuxData;}
	AuxData<ASIZE>&			rAuxData()			{ return mAuxData;}

	void	InitSolution(const bool& bfarfield = 0);
	void	RestartSolution();
	
	void	InitGlobId( const vector<MGSize>& tab, const pair<MGSize,MGSize>& range);
	void	InitPeriodic();

	const PeriodicBCBase<DIM, ET>*	cPeriodicBC() const		{ return mpPeriodic; }
	PeriodicBCBase<DIM, ET>*&		rPeriodicBC()			{ return mpPeriodic; }


protected:
	const Physics<DIM,ET>&	cPhysics() const	{ return *mpPhysics;}



protected:
	MGString				mName; 
	const Physics<DIM,ET>*	mpPhysics;

	TGrid					mGrid;
	AuxData<ASIZE>			mAuxData;
	Solution<DIM,ET>		mSolution;

	SolGradient<DIM,ET>		mSolGradient;
	
	Interface				mInterface;		// used only in parallel version; in seq. it is empty
	vector<MGSize>			mtabGlobId;		// used only in parallel version; in seq. it is empty

	PeriodicBCBase<DIM, ET>*	mpPeriodic;
};
//////////////////////////////////////////////////////////////////////


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void Data<DIM, ET>::Create(const CfgSection* pgrdcfg, const CfgSection* psolcfg, const CfgSection* pauxcfg, const Physics<DIM, ET>* pphys)
{ 
	mpPhysics = pphys;

	mGrid.Create(pgrdcfg);
	mAuxData.Create( DIM, pauxcfg);
	mSolution.Create( psolcfg, mpPhysics );
	
}


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void Data<DIM,ET>::PostCreateCheck() const
{ 
	mGrid.PostCreateCheck();
	mSolution.PostCreateCheck();
	mAuxData.PostCreateCheck();
	if ( ! mpPhysics )
		THROW_INTERNAL( "Data<DIM,ET>::PostCreateCheck() -- failed : mpPhysics is NULL" );

}


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void Data<DIM, ET>::InitPeriodic()
{
	PeriodicBCCalc<DIM, ET>* pprd = new PeriodicBCCalc<DIM, ET>;
	pprd->Init(&mGrid, &cSolution(), &mpPhysics->ConfigSect());
	mpPeriodic = pprd;
}

template <Geom::Dimension DIM, EQN_TYPE ET>
inline void Data<DIM, ET>::Init()
{
	///////////////////////////
	// grid
	//mGrid.Init();

	MGString	fname	= mGrid.ConfigSect().ValueString( ConfigStr::Solver::Grid::FNAME );
	bool		bBIN	= IO::FileTypeToBool( mGrid.ConfigSect().ValueString( ConfigStr::Solver::Grid::FType::KEY ) );

	fname = ProcessInfo::ModifyFileName( fname);
	
	if ( ProcessInfo::IsParallel() )
		mInterface.SetDim( DIM);
	
 	// reading mesh and interface
 	IO::ReadMSH2	readerMSH( mGrid);
 	IO::ReadINTF	readerINTF( mInterface);
 
 	ifstream ifile;
 
 	if ( bBIN)
 		ifile.open( fname.c_str(), ios::in | ios::binary);
 	else
 		ifile.open( fname.c_str(), ios::in );
 
 	try
 	{
 		if ( ! ifile)
 			THROW_FILE( "can not open the file", fname);
 
 		cout << "start reading '" << fname << "'";
		if ( ProcessInfo::IsParallel() )
			cout << " - from " << ProcessInfo::cProcId() << endl;
		else
			cout << endl;
		
 		readerMSH.DoRead( ifile, bBIN);
		
 		cout << "-- reading MSH finished '" << fname << "'";
		if ( ProcessInfo::IsParallel() )
			cout << " - from " << ProcessInfo::cProcId() << endl;
		else
			cout << endl;

		// initialize the interface for parallel version
		if ( ProcessInfo::IsParallel() )
		{
			readerINTF.DoRead( ifile, bBIN);
			cout << "-- reading INTF finished '" << fname << "' - from " << ProcessInfo::cProcId() << endl;
		}
 	}
 	catch ( EHandler::Except& e)
 	{
 		cout << "ERROR: Data<DIM,ET>::Init - reading mesh/interf file name: " << fname << endl << endl;
 		TRACE_EXCEPTION( e);
 		TRACE_TO_STDERR( e);
 
 		THROW_INTERNAL("END");
 	}
 
	mGrid.Init();
	mGrid.InitGlobId();
	mGrid.InitNodeCopy();
	

	///////////////////////////
	// periodic bc
	InitPeriodic();
	
	///////////////////////////
	// auxdata
	mAuxData.Resize( mGrid.SizeNodeTab() );
	mAuxData.Init();

	///////////////////////////
	// solution

	// INFO :: vertex-centered solution assumed !!!
	mSolution.Resize( mGrid.SizeNodeTab() );

	// if restart then - read solution from the file 
	if ( mSolution.ConfigSect().ValueBool( ConfigStr::Solver::Solution::RESTART ) )
	{
		RestartSolution();
	}
	else // - init using reference var
	{
		InitSolution();

	}


}

template <Geom::Dimension DIM, EQN_TYPE ET>
inline void Data<DIM, ET>::RestartSolution()
{
	bool		bBIN = IO::FileTypeToBool(mSolution.ConfigSect().ValueString(ConfigStr::Solver::Solution::FTYPE));
	MGString	fname = mSolution.ConfigSect().ValueString(ConfigStr::Solver::Solution::FNAME);

	fname = ProcessInfo::ModifyFileName(fname);

	IO::ReadSOL	reader(mSolution);

	reader.DoRead(fname, bBIN);
}

template <Geom::Dimension DIM, EQN_TYPE ET>
inline void Data<DIM,ET>::InitSolution(const bool& bfarfield)
{
	typename EquationDef<DIM, ET>::FVec	svect;

	mpPhysics->DimToUndim(svect, mpPhysics->cRefVar(), mpPhysics->cRefVar());

	if (!bfarfield)
	{
		const Sparse::BlockVector<ESIZE> bvect(svect);
		for (MGSize i = 0; i<mSolution.Size(); ++i)
			mSolution[i] = bvect;
	}

	// apply BCs to initial soultion (a must for VISCOUS_WALL flows) 
	for (MGSize i = 0; i<mGrid.SizeBFaceTab(); ++i)
	{
		CGeomBFace<DIM> bface = mGrid.cBFace(i);
		for (MGSize iloc = 0; iloc<bface.Size(); ++iloc)
		{
			Vect<DIM> vpos = bface.cNode(iloc).cPos();
			Vect<DIM> vn = bface.cBNode(iloc).cVn();
			Vect<DIM> vnn = vn.versor();

			MGSize inode = bface.cId(iloc);

			typename EquationDef<DIM, ET>::FVec	varQb, var(mSolution[inode]);
			typename EquationDef<DIM, ET>::AVec	varAuxb, varAux(mAuxData[inode]);

			// calc cell h 
			CGeomCell<DIM>	nxcell = mGrid.cCell(bface.cCellId());
			MGFloat area = bface.Area();
			MGFloat volume = nxcell.Volume();
			MGFloat h = DIM * volume / area;

			//cout << bface.cSurfId() << " -->" << volume << " / " << area << " = " << h << endl;

			mpPhysics->ApplyBC(varQb, varAuxb, var, varAux, bface.cSurfId(), vpos, vnn, h, cPeriodicBC(), inode);

			mSolution[inode] = Sparse::BlockVector<ESIZE>(varQb);

		}
	}


}

template <Geom::Dimension DIM, EQN_TYPE ET>
inline void Data<DIM,ET>::InitGlobId( const vector<MGSize>& tab, const pair<MGSize,MGSize>& range)
{
	mGrid.InitGlobId( tab, range);
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __DATA_H__
