#ifndef __FREEZINGPOINT_H__
#define __FREEZINGPOINT_H__

#include "libcoreconfig/configbase.h"
#include "libcoregeom/dimension.h"
#include "libcoresystem/mgdecl.h"
#include "libcoregeom/vect.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

using namespace Geom;


//////////////////////////////////////////////////////////////////////
// class Freezing Points
//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
class FreezingPoint 
{
public:
	typedef Geom::Vect<DIM>		GVec;
	
	FreezingPoint() {};
	virtual ~FreezingPoint() {};
	
	FreezingPoint(const GVec& pos, const MGSize& owner_rank, const MGSize& owner_indx)
	{
		mPos = pos;
		mOwnerRank = owner_rank;
		mOwnerIndx = owner_indx;
	}
		
	const GVec&		cPos() const		{ return mPos; }
	const MGSize&	cOwnerRank() const	{ return mOwnerRank; }
	const MGSize&	cOwnerIndx() const	{ return mOwnerIndx; }
	
	void 	Write( ostream& os ) const	{ os << mPos.cX() << " " << mPos.cY() << " " << cOwnerRank() << " " << cOwnerIndx() << endl; }
	
private:
	GVec	mPos;		// position vector
	MGSize	mOwnerRank; //rank of process that owns node
	MGSize	mOwnerIndx; //local node indx (at owner process)
	
	
};
	
	
	
	
	
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif //__FREEZINGPOINT_H__