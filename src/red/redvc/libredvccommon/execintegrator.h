#ifndef __EXECINTEGRATOR_H__
#define __EXECINTEGRATOR_H__

#include "libredcore/equation.h"
#include "redvc/libredvccommon/executorbase.h"
#include "redvc/libredvccommon/integrator.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class ExecIntegrator
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET>
class ExecIntegrator : public ExecutorBase<DIM,ET>
{
	enum { ESIZE = EquationDef<DIM,ET>::SIZE };

public:

	static MGString	Info()	
	{
		ostringstream os;
		os << "ExecIntegrator< " << Geom::DimToStr(DIM) << ", " << EquationDef<DIM,ET>::NameEqnType() << " >";
		return os.str();
	}

	ExecIntegrator() : ExecutorBase<DIM,ET>(), mpIntegrator(NULL)	{}
	virtual ~ExecIntegrator();

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata);
	virtual void	PostCreateCheck() const;
	virtual void	Init();
	virtual bool	SolveStep( ConvergenceInfo<ESIZE>& info);
	virtual bool	SolveFinal() { return false; };


private:
	Integrator<DIM,ET>*	mpIntegrator;
};
//////////////////////////////////////////////////////////////////////


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void ExecIntegrator<DIM,ET>::PostCreateCheck() const
{ 
	ExecutorBase<DIM,ET>::PostCreateCheck();

//	if ( ! mpSpaceSol )
//		THROW_INTERNAL( Info() << "::PostCreateCheck() -- failed : mpSpaceSol -> NULL" );

//	mpSpaceSol->PostCreateCheck();
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EXECINTEGRATOR_H__
