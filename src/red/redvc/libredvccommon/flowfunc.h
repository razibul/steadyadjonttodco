#ifndef __FLOWFUNC_H__
#define __FLOWFUNC_H__


#include "libredcore/equation.h"
#include "libredphysics/physics.h"
#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"
#include "libcoreconfig/configbase.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class FlowFunc
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET, MGSize BSIZE>
class FlowFunc : public ConfigBase
{
protected:
	enum { ESIZE = EquationDef<DIM,ET>::SIZE };
	enum { ASIZE = EquationDef<DIM,ET>::AUXSIZE };

	typedef CFlowCell<DIM,ESIZE,ASIZE>		CFCell;

	typedef typename EquationDef<DIM,ET>::FVec	EVec;
	typedef typename EquationDef<DIM,ET>::FMtx	EMtx;

	typedef typename EquationTypes<DIM,BSIZE>::Vec	BVec;
	typedef typename EquationTypes<DIM,BSIZE>::Mtx	BMtx;

public:

	static MGString	Info()	
	{
		ostringstream os;
		os << "FlowFunc< " << Geom::DimToStr(DIM) << ", " << EquationDef<DIM,ET>::NameEqnType() << ", " << BSIZE << " >";
		return os.str();
	}

	virtual void	Init( const Physics<DIM,ET>& physics) = 0;

	virtual void	CalcResiduum( const CFCell& cell, BVec tabres[] ) = 0;
	virtual void	CalcResJacobian( const CFCell& cell, BVec tabres[], BMtx mtxres[DIM+1][DIM+1] ) = 0;
};
//////////////////////////////////////////////////////////////////////




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __FLOWFUNC_H__
