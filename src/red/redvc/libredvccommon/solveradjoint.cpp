#include <time.h>

#include "solveradjoint.h"

#include "libcorecommon/factory.h"

#include "libredcore/processinfo.h"
#include "libredcore/configconst.h"
#include "libredconvergence/convergenceinfo.h"

#include "libcoreio/store.h"
#include "libcoreio/writesol.h"
#include "libcoreio/writetec.h"
#include "libcoreio/writetecsurf.h"
#include "libcoreio/writevtk.h"

#include "libcorecommon/stopwatch.h"
#include "libcoresystem/mgexcept.h"
#include "redvc/libredvccommon/dataadjoint.h"
#include "redvc/libredvccommon/data.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//template <Dimension DIM, EQN_TYPE ET>
//Solver<DIM,ET>::~Solver()	
//{
//	for ( typename vector< ExecutorBase<DIM,ET> * >::iterator itr=mtabExec.begin(); itr!=mtabExec.end(); ++itr)
//		if ( *itr != 0 )
//			delete *itr;
//}


template <Dimension DIM, EQN_TYPE ET>
void SolverAdjoint<DIM,ET>::Create( const CfgSection* pcfgsec)
{
	SolverBase::Create( pcfgsec);

	const CfgSection* pphysect = & ConfigSect().GetSection( ConfigStr::Solver::Physics::NAME );
	const CfgSection* pgrdsect = & ConfigSect().GetSection( ConfigStr::Solver::Grid::NAME );
	const CfgSection* pauxsect = & ConfigSect().GetSection( ConfigStr::Solver::AuxData::NAME );
	const CfgSection* psolsect = & ConfigSect().GetSection( ConfigStr::Solver::Solution::NAME );
	
	const CfgSection* pdesparsect = &ConfigSect().GetSection( ConfigStr::Solver::DesignParameter::NAME );
	const CfgSection* pobjecivesect = &ConfigSect().GetSection( ConfigStr::Solver::Objectives::NAME );
	const CfgSection* pfreezingpointsect = &ConfigSect().GetSection( ConfigStr::Solver::FreezePoint::NAME );
	const CfgSection* pgridassistantsect = &ConfigSect().GetSection( ConfigStr::Solver::GridAssistant::NAME );

	mbWriteTec = mbWriteTecSurf = true;

	if ( psolsect->KeyExists( ConfigStr::Solver::Solution::WRITE_TEC ) )
		mbWriteTec = psolsect->ValueBool( ConfigStr::Solver::Solution::WRITE_TEC );

	if ( psolsect->KeyExists( ConfigStr::Solver::Solution::WRITE_TECSURF ) )
		mbWriteTecSurf = psolsect->ValueBool( ConfigStr::Solver::Solution::WRITE_TECSURF );


	mPhysics.Create( pphysect);
	mData.Create( pgrdsect, psolsect, pauxsect, pdesparsect, pobjecivesect, pfreezingpointsect, pgridassistantsect, &mPhysics);


	MGString sdim = ConfigSect().ValueString( ConfigStr::Solver::Dim::KEY );
	MGString seqtype = ConfigSect().ValueString( ConfigStr::Solver::EqType::KEY );

	MGSize count = ConfigSect().GetSectionCount( ConfigStr::Solver::Executor::NAME );

	for ( MGSize i=0; i<count; ++i)
	{
		const CfgSection* cfgsec = &ConfigSect().GetSection( ConfigStr::Solver::Executor::NAME, i);

		MGString sblockdef	= cfgsec->ValueString( ConfigStr::Solver::Executor::BlockDef::KEY );
		
		MGString exeprefix = "";
		if ( sblockdef == MGString(ConfigStr::Solver::Executor::BlockDef::Value::ADJOINT ))
		{
			const CfgSection* ptimsol = & cfgsec->GetSection( ConfigStr::Solver::Executor::TimeSol::NAME );
			MGString stsoltype = ptimsol->ValueString( ConfigStr::Solver::Executor::TimeSol::Type::KEY );
		
			if ( stsoltype == MGString( ConfigStr::Solver::Executor::TimeSol::Type::Value::PETSC) )
				exeprefix = "MPIPETSC_";
		}
		
		MGString exekey = "EXECUTOR_" + sdim + "_" + seqtype + "_" + exeprefix + sblockdef;
		cout << "creating '" << exekey << "'" << endl;


		ExecutorBase<DIM,ET>* ptr = Singleton< Factory< MGString,Creator< ExecutorBase<DIM,ET> > > >::GetInstance()->GetCreator( exekey )->Create( );
		ptr->Create( cfgsec, &mPhysics, &mData);

		mtabExec.push_back( ptr);
	}

}


template <Dimension DIM, EQN_TYPE ET>
void SolverAdjoint<DIM,ET>::PostCreateCheck() const
{
	SolverBase::PostCreateCheck();

	mData.PostCreateCheck();

	if ( mtabExec.size() == 0 )
		THROW_INTERNAL( "Solver<DIM,ET>::PostCreateCheck() -- failed : 'mtabExec.size() == 0'" );

	for ( MGSize i=0; i<mtabExec.size(); ++i)
		if ( ! mtabExec[i] )
			THROW_INTERNAL( "Solver<DIM,ET>::PostCreateCheck() -- failed : 'mtabExec[" << i << "] == NULL'" );

	for ( MGSize i=0; i<mtabExec.size(); ++i)
		mtabExec[i]->PostCreateCheck();
}

template <Dimension DIM, EQN_TYPE ET>
void SolverAdjoint<DIM,ET>::Init()
{
	mPhysics.Init();
	mData.Init();

	for ( MGSize i=0; i<mtabExec.size(); ++i)
		mtabExec[i]->Init();
}

template <Dimension DIM, EQN_TYPE ET>
void SolverAdjoint<DIM, ET>::Solve()
{
	MGInt temp;
	Solve(false, &temp);
}

template <Dimension DIM, EQN_TYPE ET>
void SolverAdjoint<DIM,ET>::Solve(const bool& returnitercount, MGInt* itercount)
{

	for (MGSize i = 0; i<mtabExec.size(); ++i)
		mtabExec[i]->Init();

	MGString	name_sol_dat			= "_sol.dat";
	MGString	name_sol_surf_dat		= "_sol_surf.dat";
	MGString	name_solfin_surf_dat	= "_sol_fin_surf.dat";
	MGString	name_solfin_dat			= "_sol_fin.dat";
	MGString	name_solinit_dat		= "_sol_init.dat";
	MGString	name_sol_sol			= "_sol.sol";
	MGString	name_solfin_sol			= "_sol_fin.sol";
	MGString	name_solinit_sol		= "_sol_init.sol";
	MGString	name_conv_lst			= "_conv.lst";

	bool bConverged;
	
	MGSize	nIter =  ConfigSect().ValueSize( ConfigStr::Solver::MAXNITER );
	MGSize	nStore = ConfigSect().ValueSize( ConfigStr::Solver::NSTORE );
	MGFloat	dConvError = ConfigSect().ValueFloat( ConfigStr::Solver::CONVEPS );

	const CfgSection& solsect = ConfigSect().GetSection( ConfigStr::Solver::Solution::NAME );

	bool		bBIN = IO::FileTypeToBool( solsect.ValueString( ConfigStr::Solver::Solution::FTYPE ) );
	MGString	sSolName = solsect.ValueString( ConfigStr::Solver::Solution::FNAME );

	MGSize itcount = 0;


	IO::WriteSOL		writeSOL( mData.cSolution() );
	IO::WriteTEC		writeTEC( mData.cGrid(), &mData.cSolution() );
	IO::WriteTECSurf	writeTECSurf( mData.cGrid(), &mData.cSolution() );

	bool bcrashed = false;
	do
	{
		cout << endl << "SOLVING" << endl;

		itcount = 0;

		if (mbWriteTec)
			writeTEC.DoWrite("_bef_solving.dat");

		if (mbWriteTecSurf)
			writeTECSurf.DoWrite("_bef_solving_surf.dat");


		StopWatch	stopwatch;

		stopwatch.Start();

		try
		{
			do
			{
				++itcount;
				bConverged = true;

				ConvergenceInfo< EquationDef<DIM,ET>::SIZE > convinfo;

				//MGSize i=0;
				for ( MGSize i=0; i<mtabExec.size(); ++i)
				{
						mtabExec[i]->SolveStep( convinfo);
				}


				MGFloat errResL2_max = MaxNorm( convinfo.mErrResL2);
				MGFloat errResL2_l2 = Norm( convinfo.mErrResL2);

				stopwatch.Mark();

				cout << resetiosflags(ios_base::scientific);
				cout << setprecision(3);
				cout << "iter = " << setw(6) << itcount;
				cout << " time = " << setw(5) << stopwatch.cTotalTime();

				cout << setiosflags(ios_base::scientific);
				cout << " ResL2 = " << errResL2_l2 << " [ " << convinfo.mErrResL2[0];
				for ( MGSize k=1; k<EquationDef<DIM,ET>::SIZE; ++k)
					cout << ", " << convinfo.mErrResL2[k];
				cout << " ] ";
				cout << endl;
				cout << endl;
				
				bcrashed = false;
		
				if ( itcount%nStore == 0 || itcount==nIter)
				{
					cout << endl;
					writeSOL.DoWrite( name_sol_sol.c_str(), bBIN );

					if ( mbWriteTec )
						writeTEC.DoWrite( name_sol_dat.c_str() );

					if ( mbWriteTecSurf )
						writeTECSurf.DoWrite( name_sol_surf_dat.c_str() );
					cout << endl;
				}

				if ( errResL2_l2 < dConvError)
				{
					writeSOL.DoWrite( name_solfin_sol.c_str(), bBIN );
					writeTEC.DoWrite( name_solfin_dat.c_str() );
					writeTECSurf.DoWrite( name_solfin_surf_dat.c_str() );
					//writeVTK.DoWrite("_sol_fin.vtk");
					break;
				}
			}
			while ( itcount < nIter );

			
		}
		catch ( EHandler::ExceptInternal& e )
		{
			cout << endl << "### Solver crashed. Restarting with different CFL." << endl;
			bcrashed = true;
			for (MGSize i = 0; i < mtabExec.size(); i++)
			{
				cout << mtabExec[i]->ConfigSect().Id() << endl;
				if ( (strcmp(mtabExec[i]->ConfigSect().Id().c_str(), "flow") == 0) || (strcmp(mtabExec[i]->ConfigSect().Id().c_str(), "turbulence") == 0) )
				{
					mtabExec[i]->CFLCrash();
				}
			}

			mData.InitSolution();

		}
		catch ( ... )
		{
			throw;
		}

	}
	while ( bcrashed );
	cout << "SOLVING - finished : iter count = " << itcount << endl;
	
	
	*itercount = itcount;

	cout << endl;
	cout << "SOLVING - final executors" << endl;

	for (MGSize i = 0; i<mtabExec.size(); ++i)
	{
		if ( mtabExec[i]->IsFinal() )
			mtabExec[i]->SolveFinal();
	}


}




void init_SolverAdjoint()
{
	static ConcCreator< MGString, SolverAdjoint<DIM_2D,EQN_ADVECT>, SolverBase>			gCreatorSolverAdvect2D( "SOLVER_2D_ADVECT_ADJOINT");
	static ConcCreator< MGString, SolverAdjoint<DIM_2D,EQN_POISSON>, SolverBase>		gCreatorSolverPoisson2D( "SOLVER_2D_POISSON_ADJOINT");
	static ConcCreator< MGString, SolverAdjoint<DIM_2D,EQN_EULER>, SolverBase>			gCreatorSolverEuler2D( "SOLVER_2D_EULER_ADJOINT");
	static ConcCreator< MGString, SolverAdjoint<DIM_2D,EQN_NS>, SolverBase>				gCreatorSolverNS2D( "SOLVER_2D_NS_ADJOINT");
	static ConcCreator< MGString, SolverAdjoint<DIM_2D,EQN_RANS_SA>, SolverBase>		gCreatorSolverRANSSA2D( "SOLVER_2D_RANS_SA_ADJOINT");
	static ConcCreator< MGString, SolverAdjoint<DIM_2D,EQN_RANS_KOMEGA>, SolverBase>	gCreatorSolverRANSKOMEGA2D( "SOLVER_2D_RANS_KOMEGA_ADJOINT");

	static ConcCreator< MGString, SolverAdjoint<DIM_3D,EQN_POISSON>, SolverBase>		gCreatorSolverPoisson3D( "SOLVER_3D_POISSON_ADJOINT");
	static ConcCreator< MGString, SolverAdjoint<DIM_3D,EQN_EULER>, SolverBase>			gCreatorSolverEuler3D( "SOLVER_3D_EULER_ADJOINT");
	static ConcCreator< MGString, SolverAdjoint<DIM_3D,EQN_NS>, SolverBase>				gCreatorSolverNS3D( "SOLVER_3D_NS_ADJOINT");
	static ConcCreator< MGString, SolverAdjoint<DIM_3D,EQN_RANS_SA>, SolverBase>		gCreatorSolverRANSSA3D( "SOLVER_3D_RANS_SA_ADJOINT");
	static ConcCreator< MGString, SolverAdjoint<DIM_3D,EQN_RANS_KOMEGA>, SolverBase>	gCreatorSolverRANSKOMEGA3D( "SOLVER_3D_RANS_KOMEGA_ADJOINT");
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


