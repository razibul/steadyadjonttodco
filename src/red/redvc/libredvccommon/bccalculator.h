#ifndef __BCCALCULATOR_H__
#define __BCCALCULATOR_H__


#include "libredcore/equation.h"
#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"
#include "libredphysics/bcidmapper.h"
#include "libredphysics/physics.h"

#include "redvc/libredvccommon/periodicbcbase.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class BCCalculator
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, EQN_TYPE ET>
class BCCalculator
{
	typedef Vect<DIM>									GVec;
	typedef typename EquationDef<DIM,ET>::FVec			FVec;
	typedef typename EquationDef<DIM,ET>::AVec			AVec;

	typedef CFlowCell<DIM,EquationDef<DIM,ET>::SIZE,EquationDef<DIM,ET>::AUXSIZE>	CFCell;
	typedef CFlowBFace<DIM,EquationDef<DIM,ET>::SIZE,EquationDef<DIM,ET>::AUXSIZE>	CFBFace;

	typedef PeriodicBCBase<DIM, ET>						PeriodBCC;

public:
	void	ApplyBC(CFCell& cell, const CFBFace& face, const Physics<DIM, ET>& phys, const MGSize& id, const MGFloat& dy, const PeriodBCC* pbc);
};
//////////////////////////////////////////////////////////////////////


template <Dimension DIM, EQN_TYPE ET>
inline void BCCalculator<DIM, ET>::ApplyBC(CFCell& cell, const CFBFace& face, const Physics<DIM, ET>& phys, const MGSize& id, const MGFloat& dy, const PeriodBCC* pbc)
{
	GVec	vpos, vn, vnn;
	FVec	varQb;
	AVec	varAuxb;


	vpos = face.cNode(id).cPos();
	vn = face.cBNode(id).cVn();
	vnn = vn.versor();

	// set outlet flag
	if ( phys.cBC( face.cSurfId() ) == BC_OUTLET )
		cell.SetOutlet( true);

	if ( phys.cBC( face.cSurfId() ) == BC_VISCWALL )
		cell.SetViscWall( true);

	if ( phys.cBC( face.cSurfId() ) == BC_INVISCWALL || phys.cBC( face.cSurfId() ) == BC_SYMMETRY )
		cell.SetInviscWall( true);

	// here get proper value varQb
	
	phys.ApplyBC(varQb, varAuxb, face.cVar(id), face.cAux(id), face.cSurfId(), vpos, vnn, dy, pbc, face.cId(id));


// 	for ( MGSize j=0; j<5; ++j)
// 		IS_INFNAN_THROW( varQb(j), "INFNAN varQb problem");

	cell.rVar(0) = varQb;
	cell.rAux(0) = varAuxb;

	for ( MGSize fid=0; fid<DIM; ++fid)
		cell. rVar( BCIdMapper<DIM>::Id(fid) ) = face.cVar( fid);
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __BCCALCULATOR_H__
