#ifndef __EXECUTOR_H__
#define __EXECUTOR_H__

#include "libredcore/equation.h"
#include "redvc/libredvccommon/executorbase.h"
#include "redvc/libredvccommon/spacesolverbase.h"
#include "libredconvergence/timesolverbase.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Executor
//////////////////////////////////////////////////////////////////////
//template <Geom::Dimension DIM, EQN_TYPE ET, MGSize BSIZE>
template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
class Executor : public ExecutorBase<DIM,ET>
{
	enum { ESIZE = EquationDef<DIM,ET>::SIZE };

public:
	enum { BSIZE = MAPPER::VBSIZE };

	static MGString	Info()	
	{
		ostringstream os;
		os << "Executor< " << Geom::DimToStr(DIM) << ", " << EquationDef<DIM,ET>::NameEqnType() << ", " << BSIZE << " >";
		return os.str();
	}

	Executor() : ExecutorBase<DIM,ET>(), mpSpaceSol(NULL), mpTimeSol(NULL)	{}
	virtual ~Executor();

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata);
	virtual void	PostCreateCheck() const;
	virtual void	Init();
	virtual bool	SolveStep( ConvergenceInfo<ESIZE>& info);
	virtual bool	SolveFinal() { return false; };
	
	virtual void	CFLCrash();

protected:
	void	LocalToGlobal( const Sparse::Vector<BSIZE>& loctab);
	void	GlobalToLocal( Sparse::Vector<BSIZE>& loctab);

private:
	SpaceSolverBase<DIM,ET,MAPPER>*	mpSpaceSol;
	TimeSolverBase<BSIZE>*			mpTimeSol;
};
//////////////////////////////////////////////////////////////////////



//template <Geom::Dimension DIM, EQN_TYPE ET, MGSize BSIZE>
//inline void Executor<DIM,ET,BSIZE>::PostCreateCheck() const
template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void Executor<DIM,ET,MAPPER>::PostCreateCheck() const
{ 
	ExecutorBase<DIM,ET>::PostCreateCheck();

	if ( ! mpSpaceSol )
		THROW_INTERNAL( Info() << "::PostCreateCheck() -- failed : mpSpaceSol -> NULL" );

	if ( ! mpTimeSol )
		THROW_INTERNAL( Info() << "::PostCreateCheck() -- failed : mpTimeSol -> NULL" );

	mpSpaceSol->PostCreateCheck();
	mpTimeSol->PostCreateCheck();
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void Executor<DIM,ET,MAPPER>::GlobalToLocal( Sparse::Vector<BSIZE>& loctab)
{
	ASSERT( cData().cSolution().Size() == loctab.Size() );

	for ( MGSize i=0; i<loctab.Size(); ++i)
		MAPPER::GlobalToLocal( loctab[i], ExecutorBase<DIM,ET>::cData().cSolution()[i] );
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void Executor<DIM,ET,MAPPER>::LocalToGlobal( const Sparse::Vector<BSIZE>& loctab)
{
	ASSERT( cData().cSolution().Size() == loctab.Size() );

	for ( MGSize i=0; i<loctab.Size(); ++i)
		MAPPER::LocalToGlobal( ExecutorBase<DIM,ET>::rData().rSolution()[i], loctab[i] );

	this->cData().cSolution().CheckNAN();
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EXECUTOR_H__
