#ifndef __GRIDASSISTANT_H__
#define __GRIDASSISTANT_H__

#include "redvc/libredvcgrid/gridsimplex.h"
//#include "redvc/libredvcgrid/grid.h"
#include "libcoreconfig/configbase.h"
#include "libcoregeom/dimension.h"
#include "libcoregeom/vect.h"
#include "redvc/libredvccommon/designparametercollection.h"
#include "redvc/libredvccommon/freezingpointcollection.h"
#include "redvc/libredvccommon/auxdata.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

//////////////////////////////////////////////////////////////////////
// class GridAssistant
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class GridAssistant : public ConfigBase
{
	//typedef Grid<DIM>	TGrid;
 	typedef GridSimplex<DIM>	TGrid;
	typedef Geom::Vect<DIM>		GVec;
	
public:
	GridAssistant() {};
	virtual ~GridAssistant() {};
	
	static MGString Info()
	{
		ostringstream os;
		os << "GridAssistant< " << Geom::DimToStr(DIM) << " >";
		return os.str();
	}
	
	virtual void Create(const CfgSection* pfgsec, TGrid* pgrd, DesignParameterCollection<DIM>* pdesparcoll, FreezingPointCollection<DIM>* pfreezeptcoll);
	virtual void PostCreateCheck() const;
	virtual void Init();
	
	virtual void	ComputePerturbation(const GVec& core, vector<GVec>& perturb) const;
	virtual MGFloat	ComputePerturbation(const GVec& core, const GVec& node, bool bmodule=true) const;
	virtual void	Perturbate	(const vector< GVec >& perturb) const;
	virtual void	Unperturbate(const vector< GVec >& perturb) const;

	virtual void	RevertToInitial();
	virtual void	FixNodes(vector<GVec>& vec) const;

	virtual void	FixNodesBySum(vector<GVec>& vec ) const;
	virtual void	FixNodesByAux(vector<GVec>& vec) const;
protected:
	DesignParameterCollection<DIM>* mpDesParColl;
	FreezingPointCollection<DIM>* mpFreezePtColl;
	TGrid*	mpGrid;

	AuxData<1>			mAuxFreezing;

	MGFloat msigma, mmu, mscale;
	GVec mdofdir; // direction of perturbation
};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GRIDASSISTANT_H__