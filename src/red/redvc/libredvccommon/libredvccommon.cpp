
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



void init_Solver();
void init_Executor();
void init_ExecIntegrator();
void init_ExecAssistant();
void init_SpaceSolver();
void init_SolGradBuilder();
void init_Adjoint();
void init_SolverAdjoint();
//void init_SolverOptim();
void init_ObjectiveForce();
void init_FreezingPointCollection();
void init_GridAssistant();

void RegisterLib_REDVCCOMMON()
{
	init_Solver();
	init_Executor();
	init_ExecIntegrator();
	init_ExecAssistant();
	init_SpaceSolver();
	init_SolGradBuilder();
	init_Adjoint();
	init_SolverAdjoint();
// 	init_SolverOptim();
	init_ObjectiveForce();
	init_FreezingPointCollection();
	init_GridAssistant();
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

