#ifndef __DESIGNPARAMETERCOLLECTION_H__
#define __DESIGNPARAMETERCOLLECTION_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libcoreconfig/cfgsection.h"
#include "libredcore/configconst.h"
#include "libredphysics/physics.h"

#include "redvc/libredvcgrid/gridsimplex.h"

#include "designparameter.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

using namespace Geom;

//////////////////////////////////////////////////////////////////////
// class Design Parameter Collection
//////////////////////////////////////////////////////////////////////

template<Dimension DIM>
class DesignParameterCollection : public ConfigBase
{
public:
	typedef GridSimplex<DIM>	TGrid;
	typedef Geom::Vect<DIM>		GVec;
	
	DesignParameterCollection() {};
	virtual ~DesignParameterCollection() {};

	void Create(const CfgSection* pcfgsec, const TGrid *pgrd);
	void PostCreateCheck() const;
	void Init();

	const MGSize&	Size()	const	{ return mSize; }
	//MGSize&			rSize()			{ return mSize; }

	//DesignParameter<DIM>& 		operator [] 	( const MGSize& i ) {return mvecDesignParameters[i];}
	const DesignParameter<DIM>&	operator[] 	( const MGSize& i )	const {return mvecDesignParameters[i];}

	const vector<MGFloat>&	cVecValues();

private:
	
	void DistributeParameters();
	void FindGlobalNearestNode(const MGSize& indx, const MGFloat& dist, const GVec& pos, MGSize* owner_rank, MGSize* owner_indx, GVec* owner_pos);
	
	const TGrid* mpGrid;
	
	MGSize mSize;
	vector<DesignParameter<DIM> >	mvecDesignParameters;
	vector< MGFloat >				mvecDesignParametersValues;
	map<MGInt, MGInt>	mmapSurf;
	
};





//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif //__DESIGNPARAMETERCOLLECTION_H__
