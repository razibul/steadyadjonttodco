#ifndef __COMPFLOWBFACE_H__
#define __COMPFLOWBFACE_H__

#include "redvc/libredvccommon/compflowent.h"
#include "redvc/libredvcgrid/compgeombface.h"

#include "libredphysics/bc.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Dimension DIM, MGSize EQSIZE, MGSize AUXSIZE>
class CFlowCell;


//////////////////////////////////////////////////////////////////////
// class CFlowBFace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, MGSize EQSIZE, MGSize AUXSIZE>
class CFlowBFace : public CFlowEnt<DIM,EQSIZE,AUXSIZE,GridSizes<DIM>::BFACE_MAXSIZE>, public CGeomBFace<DIM>
{
private:
	typedef CFlowEnt<DIM,EQSIZE,AUXSIZE,GridSizes<DIM>::BFACE_MAXSIZE> FEnt;
	typedef CGeomBFace<DIM> GBFace;

public:
	CFlowBFace() : FEnt(), GBFace()											{}
	CFlowBFace( const CFlowBFace& bface) : FEnt( bface), GBFace( bface)		{}
	explicit CFlowBFace( const GBFace& gbface) : FEnt(), GBFace( gbface)	{}
};



////////////////////////////////////////////////////////////////////////
//// class CFlowBFace<DIM_2D>
////////////////////////////////////////////////////////////////////////
//template <MGSize EQSIZE>
//class CFlowBFace<DIM_2D,EQSIZE> : public CFlowEnt<EQSIZE,BFACESIZE_EDGE>, 
//								  public CGeomBFace<DIM_2D>
//{
//private:
//	typedef CFlowEnt<EQSIZE,BFACESIZE_EDGE> FEnt;
//	typedef CGeomBFace<DIM_2D> GBFace;
//
//public:
//	CFlowBFace() : FEnt(), GBFace()											{}
//	CFlowBFace( const CFlowBFace& bface) : FEnt( bface), GBFace( bface)		{}
//	explicit CFlowBFace( const GBFace& gbface) : FEnt(), GBFace( gbface)	{}
//
//};
//
//
////////////////////////////////////////////////////////////////////////
//// class CFlowBFace<DIM_3D>
////////////////////////////////////////////////////////////////////////
//template <MGSize EQSIZE>
//class CFlowBFace<DIM_3D,EQSIZE> : public CFlowEnt<EQSIZE,BFACESIZE_QUAD>, 
//								  public CGeomBFace<DIM_3D>
//{
//private:
//	typedef CFlowEnt<EQSIZE,BFACESIZE_QUAD> FEnt;
//	typedef CGeomBFace<DIM_3D> GBFace;
//
//public:
//	CFlowBFace() : FEnt(), GBFace()											{}
//	CFlowBFace( const CFlowBFace& bface) : FEnt( bface), GBFace( bface)		{}
//	explicit CFlowBFace( const GBFace& gbface) : FEnt(), GBFace( gbface)	{}
//
//};





//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __COMPFLOWBFACE_H__
