#ifndef __EXECUTORBASE_H__
#define __EXECUTORBASE_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreconfig/config.h"
#include "libcoreconfig/configbase.h"
#include "libredcore/configconst.h"
#include "libredcore/equation.h"

#include "libredphysics/physics.h"
#include "redvc/libredvccommon/data.h"
#include "redvc/libredvccommon/dataadjoint.h"
#include "libredconvergence/convergenceinfo.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class ExecutorBase
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET>
class ExecutorBase : public ConfigBase
{
	enum { ESIZE = EquationDef<DIM,ET>::SIZE };

public:
	static MGString	Info()	
	{
		ostringstream os;
		os << "ExecutorBase< " << Geom::DimToStr(DIM) << ", " << EquationDef<DIM,ET>::NameEqnType() << " >";
		return os.str();
	}

	ExecutorBase() : mpPhysics(NULL), mpData(NULL)	{}
	virtual ~ExecutorBase()	{}

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata);
	virtual void	PostCreateCheck() const;

	virtual void	Init()										= 0;
	virtual bool	SolveStep( ConvergenceInfo<ESIZE>& info)	= 0;
	virtual bool	SolveFinal()								= 0;
	
	virtual void	CFLCrash() { THROW_INTERNAL("ExecutorBase::CFLCrashed not implemented!!") };

	bool			IsFinal() { return mbIsFinal; }

protected:
	const Data<DIM,ET>&			cData() const	{ return *mpData;}
	Data<DIM,ET>&				rData()			{ return *mpData;}

	Physics<DIM,ET>&			rPhysics()		{ return *mpPhysics;}

	Data<DIM,ET>*				mpData;
private:
	Physics<DIM,ET>*			mpPhysics;
	bool						mbIsFinal;
};
//////////////////////////////////////////////////////////////////////


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void ExecutorBase<DIM,ET>::Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata)
{ 
	ConfigBase::Create( pcfgsec);

	mpPhysics = pphys;
	mpData = pdata;
	
	mbIsFinal = false;
	MGString sblockdef = pcfgsec->ValueString(ConfigStr::Solver::Executor::BlockDef::KEY);
	if (sblockdef == MGString( ConfigStr::Solver::Executor::BlockDef::Value::ADJOINT )) 
	{
			mbIsFinal = true;
	}



}


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void ExecutorBase<DIM,ET>::PostCreateCheck() const
{ 
	ConfigBase::PostCreateCheck();

	if ( !mpPhysics )
		THROW_INTERNAL( Info() << "::PostCreateCheck() -- failed : mpPhysics -> NULL" );

	if ( ! mpData )
		THROW_INTERNAL( Info() << "::PostCreateCheck() -- failed : mpData -> NULL" );
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EXECUTORBASE_H__
