#ifndef __INTEGRATOR_H__
#define __INTEGRATOR_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libredcore/equation.h"

#include "libcoreconfig/configbase.h"
#include "libredphysics/physics.h"
#include "redvc/libredvccommon/data.h"

#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Integrator
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET>
class Integrator : public ConfigBase
{
public:
	enum { ESIZE = EquationDef<DIM,ET>::SIZE };
	enum { ASIZE = EquationDef<DIM,ET>::AUXSIZE };

	typedef CFlowCell<DIM,ESIZE,ASIZE>		CFCell;
	typedef CFlowBFace<DIM,ESIZE,ASIZE>		CFBFace;


	static MGString	Info()	
	{
		ostringstream os;
		os << "Integrator< " << Geom::DimToStr(DIM) << ", " << EquationDef<DIM,ET>::NameEqnType() << " >";
		return os.str();
	}

	Integrator()	{}
	virtual ~Integrator()	{};

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata) = 0;
	virtual void	PostCreateCheck() const = 0;
	virtual void	Init() = 0;
	virtual bool	Do() = 0;

protected:
	const Physics<DIM,ET>&	cPhysics() const	{ return *mpPhysics;}
	const Data<DIM,ET>&		cData() const		{ return *mpData;}

	void	GetFCell( CFCell& fcell, const MGSize& i ) const;
	void	GetFBFace( CFBFace& fbface, const MGSize& i ) const;

private:
	Physics<DIM,ET>*			mpPhysics;
	Data<DIM,ET>*				mpData;
};


template <Geom::Dimension DIM, EQN_TYPE ET>
void Integrator<DIM,ET>::Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata)
{
	ConfigBase::Create( pcfgsec);

	mpPhysics = pphys;
	mpData = pdata;
}


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void Integrator<DIM,ET>::GetFCell( CFCell& fcell, const MGSize& i ) const
{
	cData().cGrid().GetCell( i, fcell );
	for ( MGSize i=0; i<fcell.Size(); ++i)
	{
		fcell.rVar(i) = typename EquationDef<DIM,ET>::FVec( cData().cSolution()[ fcell.cId(i) ] ) ;
		fcell.rAux(i) = typename EquationDef<DIM,ET>::AVec( cData().cAuxData()[ fcell.cId(i) ] ) ;
	}
}


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void Integrator<DIM,ET>::GetFBFace( CFBFace& fbface, const MGSize& i ) const
{ 
	cData().cGrid().GetBFace( i, fbface);
	for ( MGSize i=0; i<fbface.Size(); ++i)
	{
		fbface.rVar(i) = typename EquationDef<DIM,ET>::FVec( cData().cSolution()[ fbface.cId(i) ] );
		fbface.rAux(i) = typename EquationDef<DIM,ET>::AVec( cData().cAuxData()[ fbface.cId(i) ] ) ;
	}
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __INTEGRATOR_H__
