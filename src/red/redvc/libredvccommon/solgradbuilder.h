#ifndef __SOLGRADBUILDER_H__
#define __SOLGRADBUILDER_H__


#include "redvc/libredvccommon/assistantbase.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class SolGradBuilder
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET>
class SolGradBuilder : public AssistantBase<DIM,ET>
{
public:
	enum { ESIZE = EquationDef<DIM,ET>::SIZE };
	enum { ASIZE = EquationDef<DIM,ET>::AUXSIZE };

	typedef CFlowCell<DIM,ESIZE,ASIZE>		CFCell;
	typedef CFlowBFace<DIM,ESIZE,ASIZE>		CFBFace;

	typedef typename EquationDef<DIM,ET>::FVec	FVec;
	typedef Geom::Vect<DIM>			GVec;


	static MGString	Info()	
	{
		ostringstream os;
		os << "SolGradBuilder< " << Geom::DimToStr(DIM) << ", " << EquationDef<DIM,ET>::NameEqnType() << " >";
		return os.str();
	}

	SolGradBuilder()	{}
	virtual ~SolGradBuilder()	{};

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata);
	virtual void	PostCreateCheck() const;
	virtual void	Init();
	virtual bool	Do();

private:
	class GetVar
	{
	public:
		GetVar( const MGSize& i) : ind(i)	{}
		MGFloat operator () ( const FVec& var) const
		{
			return var( ind);
		}
	private:
		MGSize ind;
	};

};


template <Geom::Dimension DIM, EQN_TYPE ET>
void SolGradBuilder<DIM,ET>::Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata)
{
	AssistantBase<DIM,ET>::Create( pcfgsec, pphys, pdata);
}


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void SolGradBuilder<DIM,ET>::PostCreateCheck() const
{ 
	AssistantBase<DIM,ET>::PostCreateCheck();
}


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void SolGradBuilder<DIM,ET>::Init() 
{
	this->rData().rSolGradient().Resize( this->cData().cGrid().SizeNodeTab() );
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __SOLGRADBUILDER_H__
