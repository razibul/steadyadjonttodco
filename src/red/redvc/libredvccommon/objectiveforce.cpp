#include "objectiveforce.h"

#include "libcorecommon/factory.h"
#include "libredcore/equation.h"
#include "libcoresystem/mgconst.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <Geom::Dimension DIM, EQN_TYPE ET>
void ObjectiveForce<DIM,ET>::ComputeGradSolution(const Solution<DIM,ET>& sol)
{
	this->mvecGradSolution.Reset();

	CFBFace cfbface;
	for (MGSize i = 0; i < this->mpGrid->SizeBFaceTab(); i++)
	{
		//check if surfid is adjoint bnd
		this->mpGrid->GetBFace(i, cfbface);
		for (MGSize i = 0; i<cfbface.Size(); ++i) //temporarily copied from spacesolverbase.h
			cfbface.rVar(i) = EVec( sol[cfbface.cId(i)] );

		MGInt surfid = cfbface.cSurfId();

		map<MGInt, bool>::iterator itr = mmapSurf.find(surfid);
		if (itr == mmapSurf.end())
		{
			ostringstream os;
			os << "ObjectiveForce: surfid not found " << surfid;
			THROW_INTERNAL(os.str());
		}

		bool bsurfid = (*itr).second;

		if (bsurfid)
			ComputeGradSolutionBFace(cfbface);

	}
}

template <Geom::Dimension DIM, EQN_TYPE ET>
inline void ObjectiveForce<DIM,ET>::ComputeGradSolutionBFace(const CFBFace& bface)
{
	GVec vn;
	vn = bface.Vn();
	
	if (mtemptype == ENERGY)
	{
		MGFloat d = -(vn*mDir) / (MGFloat)DIM;
		for (MGSize i = 0; i < bface.Size(); i++)
		{
			MGInt indx = bface.cId(i);
			this->mvecGradSolution[indx](EqDef::ID_P) += d;
		}
	}
	if (mtemptype == PRESSURE)
	{
		MGFloat d = -(vn*mDir) / (MGFloat)DIM;
		for (MGSize i = 0; i < bface.Size(); i++)
		{
			const MGFloat rho = bface.cVar(i)(EqDef::ID_RHO);
			if (rho < ZERO)
				THROW_INTERNAL("ObjectiveForce::ComputeGradSolutionBFace() -- Zero density !!!");

			const MGFloat u2 = EqnEuler<DIM>::SqrU(bface.cVar(i));

			MGInt indx = bface.cId(i);
			this->mvecGradSolution[indx](EqDef::ID_RHO) += d*(FLOW_K - 1)*u2 / (2 * rho*rho);
			this->mvecGradSolution[indx](EqDef::ID_P) += d*(FLOW_K - 1);
			switch (DIM)
			{
			case DIM_3D:
				this->mvecGradSolution[indx](EqDef::template U<2>::ID) += -d*(FLOW_K - 1)*bface.cVar(i)[EqDef::template U<2>::ID] / rho;
			case DIM_2D:
				this->mvecGradSolution[indx](EqDef::template U<1>::ID) += -d*(FLOW_K - 1)*bface.cVar(i)[EqDef::template U<1>::ID] / rho;
			case DIM_1D:
				this->mvecGradSolution[indx](EqDef::template U<0>::ID) += -d*(FLOW_K - 1)*bface.cVar(i)[EqDef::template U<0>::ID] / rho;
			}

		}
	}

}
template <Geom::Dimension DIM, EQN_TYPE ET>
void ObjectiveForce<DIM,ET>::ComputeObjective(const Solution<DIM,ET>& sol)
{
	this->mValue = 0;
	for (MGSize i = 0; i < sol.Size(); i++)
		for (MGSize j = 0; j < ESIZE; j++)
			this->mValue += this->mvecGradSolution[i](j) * sol[i](j);

	cout << this->mDescription << " objective value : " << this->mValue << endl;
}

void init_ObjectiveForce()
{
	// 2D
	static ConcCreator<
		MGString,
		ObjectiveForce<DIM_2D,EQN_EULER>,
		ObjectiveBase<DIM_2D,EQN_EULER>
	> gCreatorObjectiveForceEuler2D("OBJECTIVE_FORCE_EULER_2D");

	static ConcCreator<
		MGString,
		ObjectiveForce<DIM_2D, EQN_NS>,
		ObjectiveBase<DIM_2D, EQN_NS>
	> gCreatorObjectiveForceNS2D("OBJECTIVE_FORCE_NS_2D");

	static ConcCreator<
		MGString,
		ObjectiveForce<DIM_2D, EQN_RANS_SA>,
		ObjectiveBase<DIM_2D, EQN_RANS_SA>
	> gCreatorObjectiveForceRANSSA2D("OBJECTIVE_FORCE_RANS_SA_2D");

	// 3D
	static ConcCreator<
		MGString,
		ObjectiveForce<DIM_3D, EQN_EULER>,
		ObjectiveBase<DIM_3D, EQN_EULER>
	> gCreatorObjectiveForceEuler3D("OBJECTIVE_FORCE_EULER_3D");

	static ConcCreator<
		MGString,
		ObjectiveForce<DIM_3D, EQN_NS>,
		ObjectiveBase<DIM_3D, EQN_NS>
	> gCreatorObjectiveForceNS3D("OBJECTIVE_FORCE_NS_3D");

	static ConcCreator<
		MGString,
		ObjectiveForce<DIM_3D, EQN_RANS_SA>,
		ObjectiveBase<DIM_3D, EQN_RANS_SA>
	> gCreatorObjectiveForceRANSSA3D("OBJECTIVE_FORCE_RANS_SA_3D");

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 
