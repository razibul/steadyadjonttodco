#ifdef WITH_NLOPT

#ifndef __SOLVEROPTIM_H__
#define __SOLVEROPTIM_H__

#include "libcoresystem/mgdecl.h"

#include "solveradjoint.h"
#include "data.h"
#include "dataadjoint.h"
#include "libnlopt/api/nlopt.hpp"
#include "libcoreconfig/cfgsection.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// SolverOptim
//////////////////////////////////////////////////////////////////////

template <Dimension DIM, EQN_TYPE ET>
class SolverOptim : public SolverBase
{
	typedef Geom::Vect<DIM>			GVec;
public:
	SolverOptim() {};
	virtual ~SolverOptim() {};

	virtual void	Create(const CfgSection* pcfgsec);
	virtual void	PostCreateCheck() const;
	virtual void	Init();

	virtual void	Solve();

	MGFloat ObjectiveFunc(const vector<double> &x, vector<double>& grad);
	void	ConstraintFunc(unsigned m, double *result, unsigned n, const double *x, double *grad);

protected:
	SolverAdjoint<DIM, ET>	mSolver;
	DataAdjoint<DIM, ET>*	mpData;

private:
	MGFloat mtemph;

	MGString mstralgorithm;
	MGSize mdim;
	MGSize mmaxiter;

	MGFloat mtol;
	MGString mstrtolfunc;
	MGString mstrtoltype;
	bool mbistolf;
	bool mbistolrel;

	MGString mstrtype;
	bool mbistypemax;

	MGFloat mlobnd;
	MGFloat mupbnd;
	bool mbislobnd;
	bool mbisupbnd;

	bool mbisineq;		// inequality constraints: FALSE - not specified, TRUE - specified
	bool mbineqtype;	// FALSE - not increase constraint , TRUE - not decrease constraint
	MGFloat	mineqtol;	// tolerance for nlopt inequality constraint
	bool mbineqval;		// FALSE - initial drag, TRUE - value from config
	MGFloat mineqval;	// VALUE (if given in config)

	nlopt::opt	mOpt;
	nlopt::algorithm malg;

	vector<MGFloat> mvecDesParamValue;
	int mitercount;
	ofstream mLog;
	ofstream mOfItercount;
};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __SOLVEROPTIM_H__

#endif //WITH_NLOPT