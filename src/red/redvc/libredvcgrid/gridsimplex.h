#ifndef __GRIDSIMPLEX_H__
#define __GRIDSIMPLEX_H__

#include "libcoresystem/mgdecl.h"
#include "libcoreio/gridfacade.h"
#include "redvc/libredvcgrid/node.h"
#include "redvc/libredvcgrid/cell.h"
#include "redvc/libredvcgrid/bface.h"
#include "redvc/libredvcgrid/compgeomcell.h"
#include "redvc/libredvcgrid/compgeombface.h"
//#include "redvc/libredvccommon/freezingpointcollection.h"
#include "redvc/libredvccommon/freezingpoint.h"
#include "libcoreconfig/configbase.h"
#include "libredcore/processinfo.h"

#include "libcoregeom/vect.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template<Dimension DIM> class GridAssistant;

//////////////////////////////////////////////////////////////////////
// class GridSimplex
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class GridSimplex : public ConfigBase, public IO::GridBndFacade, public IO::GridReadFacade
{
	friend class GridAssistant<DIM>;
	
	typedef Geom::Vect<DIM>			GVec;
public:
	GridSimplex() : ConfigBase(), mRefLength(1.0)	{}

	virtual	 ~GridSimplex()					{}

	virtual void Create(const CfgSection* pcfgsec);
	virtual void PostCreateCheck() const;
	virtual void Init();

	void	InitGlobId();
	void	InitGlobId( const vector<MGSize>& tab, const pair<MGSize,MGSize> range);


	MGString	Name() const	{ return mGrdName;}

	MGSize	SizeNodeTab() const		{ return mtabNode.size(); }
	MGSize	SizeCellTab() const		{ return mtabCell.size(); }
	MGSize	SizeBNodeTab() const	{ return mtabBNode.size(); }
	MGSize	SizeBFaceTab() const	{ return mtabBFace.size(); }

	MGSize	SizeBFaceAllTab() const	{ return mtabBFaceAll.size(); }

	const pair<MGSize,MGSize>&	cOwnedIds() const				{ return mrangeOwnedNodes; }

	const Node<DIM>&			cNode( const MGSize& i) const;
	CGeomCell<DIM>				cCell( const MGSize& i) const;
	const BoundaryNode<DIM>&	cBNode( const MGSize& i) const;
	CGeomBFace<DIM>				cBFace( const MGSize& i) const;
	
	const Node<DIM>&			cNodeInitial(const MGSize& i) const;
	const BoundaryNode<DIM>&	cBNodeInitial(const MGSize& i) const;

	const CGeomBFace<DIM>&		cBFaceAll(const MGSize& i) const;

	void	GetCell( const MGSize& i, CGeomCell<DIM>& cell) const;
	void	GetBFace( const MGSize& i, CGeomBFace<DIM>& face) const;

	MGSize	CalcBandWidth();

	void	CheckBFaceVn();

	vector<MGSize>	FindFirstLastSurfBNode(const MGSize& surfid) const;

	void			InitNodeCopy();
	MGSize			FindNearestNode(const GVec& pos, MGFloat& dist) const;
	bool	IsOwned(const MGSize& globid) const;
protected:

	virtual Dimension	Dim() const		{ return DIM;}

	virtual MGSize	IOSizeNodeTab() const										{ return mtabNode.size(); }
	virtual MGSize	IOSizeCellTab( const MGSize& type) const					{ ASSERT(type==DIM+1); return mtabCell.size();}
	virtual MGSize	IOSizeBFaceTab( const MGSize& type) const					{ ASSERT(type==DIM); return mtabBFace.size();}

	virtual void	IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	virtual void	IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	virtual void	IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id)const;

	virtual MGSize	IONumCellTypes() const										{ return 1;}
	virtual MGSize	IONumBFaceTypes() const										{ return 1;}
	virtual MGSize	IOCellType( const MGSize& i) const							{ ASSERT(i==0); return DIM+1; }
	virtual MGSize	IOBFaceType( const MGSize& i) const							{ ASSERT(i==0); return DIM;}

	virtual void	IOTabNodeResize( const MGSize& n)							{ mtabNode.resize( n);}
	virtual void	IOTabCellResize( const MGSize& type, const MGSize& n)		{ ASSERT(type==DIM+1); mtabCell.resize( n);}
	virtual void	IOTabBFaceResize( const MGSize& type, const MGSize& n)		{ ASSERT(type==DIM); mtabBFace.resize( n);}

	virtual void	IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id);
	virtual void	IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id );
	virtual void	IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id );
	virtual void	IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id );

	virtual void	IOGetName( MGString& name) const							{ name = mGrdName;}
	virtual void	IOSetName( const MGString& name)							{ mGrdName = name;}

	////////////////////
	virtual MGSize	IOSizeBNodeTab() const										{ return mtabBNode.size();}

	virtual void	IOGetBNodePos( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOGetBNodeVn( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOGetBNodePId( MGSize& idparent, const MGSize& id) const	{ idparent = mtabBNode[id].cPId();}
	virtual void	IOGetBFaceBIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;


private:

	void	GetCCellIds( CGeomCell<DIM>& ccell, const MGSize& i) const;
	void	GetCBFaceIds( CGeomBFace<DIM>& cbface, const MGSize& i) const;
	void	GetCBFaceBndIds( CGeomBFace<DIM>& cbface, const MGSize& i) const;

	void	SetCBFaceBndIds( const CGeomBFace<DIM>& cbface, const MGSize& i);

	void	InitBNodes();
	void	InitNodeVolume();
	void    InitBFaceCells();

private:

	MGString		mGrdName;

	MGFloat			mMaxAngle;
	MGFloat			mRefLength;

	pair<MGSize,MGSize>			mrangeOwnedNodes;
	vector< Node<DIM> >			mtabNode;
	vector< BoundaryNode<DIM> >	mtabBNode;
	vector< Cell<DIM,DIM+1>	>	mtabCell;
	vector< BFace<DIM,DIM> >	mtabBFace;

	vector< Node<DIM> >			mtabNodeInitial;
	vector< BoundaryNode<DIM> >	mtabBNodeInitial;
	vector< CGeomBFace<DIM> >	mtabBFaceAll;
};
//////////////////////////////////////////////////////////////////////


template <Dimension DIM>
inline void GridSimplex<DIM>::GetCCellIds( CGeomCell<DIM>& ccell, const MGSize& i) const
{
	ccell.rType() = DIM+1;
	for ( MGSize k=0; k<ccell.cType(); ++k)
		ccell.rId(k) = mtabCell[i].cId(k);
}



template <Dimension DIM>
inline void GridSimplex<DIM>::GetCBFaceIds( CGeomBFace<DIM>& cbface, const MGSize& i) const
{
	cbface.rType() = DIM;
	for ( MGSize k=0; k<cbface.cType(); ++k)
		cbface.rId(k) = mtabBFace[i].cId(k);
}

template <Dimension DIM>
inline void GridSimplex<DIM>::GetCBFaceBndIds( CGeomBFace<DIM>& cbface, const MGSize& i) const
{
	cbface.rType() = DIM;
	cbface.rSurfId() = mtabBFace[i].cSurfId();
	cbface.rCellId() = mtabBFace[i].cCellId();

	for ( MGSize k=0; k<cbface.cType(); ++k)
	{
		cbface.rId(k) = mtabBFace[i].cId(k);
		cbface.rBId(k) = mtabBFace[i].cBId(k);
	}
}

template <Dimension DIM>
inline void GridSimplex<DIM>::SetCBFaceBndIds( const CGeomBFace<DIM>& cbface, const MGSize& i)
{
	ASSERT( cbface.Size() == DIM);

	for ( MGSize k=0; k<DIM; ++k)
		mtabBFace[i].rBId(k) = cbface.cBId(k);
}





template <Dimension DIM>
inline void GridSimplex<DIM>::GetCell( const MGSize& i, CGeomCell<DIM>& cell) const
{
	GetCCellIds( cell, i);
	for ( MGSize k=0; k<cell.Size(); ++k)
	{
		cell.rGId(k) = mtabNode[cell.cId(k)].cGId();
		cell.rNode(k) = mtabNode[cell.cId(k)];
	}

	cell.CalcVn();
}


template <Dimension DIM>
inline void GridSimplex<DIM>::GetBFace( const MGSize& i, CGeomBFace<DIM>& face) const
{
	GetCBFaceBndIds( face, i);
	for ( MGSize k=0; k<face.Size(); ++k)
	{
		face.rGId(k) = mtabNode[face.cId(k)].cGId();
		face.rNode(k) = mtabNode[face.cId(k)];
		face.rBNode(k) = mtabBNode[face.cBId(k)];
		ASSERT( face.cBNode(k).cPId() == face.cId(k) );
	}
}





template <Dimension DIM>
inline const Node<DIM>& GridSimplex<DIM>::cNode( const MGSize& i) const
{
	return mtabNode[i];
}

template <Dimension DIM>
inline const BoundaryNode<DIM>& GridSimplex<DIM>::cBNode( const MGSize& i) const
{
	return mtabBNode[i];
}

template <Dimension DIM>
inline CGeomCell<DIM> GridSimplex<DIM>::cCell( const MGSize& i) const
{
	CGeomCell<DIM>	cell;
	GetCell( i, cell);
	
	return cell;
}


template <Dimension DIM>
inline CGeomBFace<DIM> GridSimplex<DIM>::cBFace( const MGSize& i) const
{
	CGeomBFace<DIM>	face;
	GetBFace( i, face);

	return face;
}

template <Dimension DIM>
inline const Node<DIM>& GridSimplex<DIM>::cNodeInitial(const MGSize& i) const
{
	return mtabNodeInitial[i];
}

template <Dimension DIM>
inline const BoundaryNode<DIM>& GridSimplex<DIM>::cBNodeInitial(const MGSize& i) const
{
	return mtabBNodeInitial[i];
}

template <Dimension DIM>
inline const CGeomBFace<DIM>& GridSimplex<DIM>::cBFaceAll(const MGSize& i) const
{
	return mtabBFaceAll[i];
}
//////////////////////////////////////////////////////////////////////


template <Dimension DIM>
inline void GridSimplex<DIM>::IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mtabNode[id].cPos().cX(i) * mRefLength;
}


template <Dimension DIM>
inline void GridSimplex<DIM>::IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( type == DIM+1);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<=DIM; ++k)
		tabid[k] = mtabCell[id].cId(k);
}


template <Dimension DIM>
inline void GridSimplex<DIM>::IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( type == DIM);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<DIM; ++k)
		tabid[k] = mtabBFace[id].cId(k);
}

template <Dimension DIM>
inline void GridSimplex<DIM>::IOGetBFaceSurf(MGSize& idsurf, const MGSize& type, const MGSize& id) const
{
	ASSERT( type == DIM);
	idsurf = mtabBFace[id].cSurfId();
}

template <Dimension DIM>
inline void GridSimplex<DIM>::IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id )
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		mtabNode[id].rPos().rX(i) = tabx[i] / mRefLength;
}


template <Dimension DIM>
inline void GridSimplex<DIM>::IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
{
	ASSERT( type == DIM+1);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<=DIM; ++k)
		mtabCell[id].rId(k) = tabid[k];
}


template <Dimension DIM>
inline void GridSimplex<DIM>::IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
{
	ASSERT( type == DIM);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<DIM; ++k)
		mtabBFace[id].rId(k) = tabid[k];
}

template <Dimension DIM>
inline void GridSimplex<DIM>::IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id )
{
	ASSERT( type == DIM);
	mtabBFace[id].rSurfId() = idsurf;
}


//////////////////////

template <Dimension DIM>
inline void GridSimplex<DIM>::IOGetBNodePos( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mtabNode[ mtabBNode[id].cPId() ].cPos().cX(i) * mRefLength;
}

template <Dimension DIM>
inline void GridSimplex<DIM>::IOGetBNodeVn( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mtabBNode[id].cVn().cX( (MGInt)i);
}

template <Dimension DIM>
inline void GridSimplex<DIM>::IOGetBFaceBIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( type == DIM);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<DIM; ++k)
		tabid[k] = mtabBFace[id].cBId(k);
}

//////////////////////

template <Dimension DIM>
inline vector<MGSize> GridSimplex<DIM>::FindFirstLastSurfBNode(const MGSize& surfid) const 
{
	vector<MGSize> indx;

	for (MGSize i = 0; i < mtabBFace.size(); i++)
	{
		if (mtabBFace[i].cSurfId() == surfid)
		{
			indx.push_back(i);

			break;
		}

	}


	for (MGSize i = indx[0]; i < mtabBFace.size(); i++)
	{
		if (mtabBFace[i].cSurfId() != surfid)
		{
			indx.push_back(i - 1);
			break;
		}

	}
	if ( ( indx.size() == 1 ) )
		indx.push_back( mtabBFace.size() - 1);

	return indx;

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GRIDSIMPLEX_H__
