#ifndef __CELLCOLLECTION_H__
#define __CELLCOLLECTION_H__


#include "libcoresystem/mgdecl.h"
#include "redvc/libredvcgrid/cell.h"
#include "redvc/libredvcgrid/compgeomcell.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
// class CellCollection
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class CellCollection
{
public:
	CellCollection()	{ THROW_INTERNAL( "Not implemented !!!");}
};




//////////////////////////////////////////////////////////////////////
// class CellCollection<DIM_2D>
//////////////////////////////////////////////////////////////////////
template <>
class CellCollection<DIM_2D>
{
public:
	MGSize	Size( const MGSize& type) const;

	MGSize	Size() const			{ return mtabTri.size() + mtabQuad.size(); }
	MGSize	NTypes() const			{ return (mtabTri.size() ? 1 : 0) + (mtabQuad.size() ? 1 : 0); }
	MGSize	Type( const MGSize& id) const;

	void	GetCCellIds( CGeomCell<DIM_2D>& ccell, const MGSize& i) const;

	void	GetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	void	SetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id);

	void	Resize( const MGSize& type, const MGSize& n);

private:
	vector< Cell<DIM_2D, CELLSIZE_TRI> >	mtabTri;
	vector< Cell<DIM_2D, CELLSIZE_QUAD> >	mtabQuad;
};



inline 	MGSize	CellCollection<DIM_2D>::Type( const MGSize& id) const
{
	vector<MGSize>	tab;
	if ( mtabTri.size() )
		tab.push_back( CELLSIZE_TRI);

	if ( mtabQuad.size() )
		tab.push_back( CELLSIZE_QUAD);

	if ( id > tab.size() )
		THROW_INTERNAL( "error in CellCollection<DIM_2D>::Type");

	return tab[id];

	MGSize k=0;
	if ( (id+1) == ( k += (mtabTri.size() ? 1 : 0) ) )
		return CELLSIZE_TRI;
	else if ( (id+1) == ( k += (mtabQuad.size() ? 1 : 0) ) )
		return CELLSIZE_QUAD;
	else
		THROW_INTERNAL( "error in CellCollection<DIM_2D>::Type");

	return 0;
}

inline MGSize CellCollection<DIM_2D>::Size( const MGSize& type) const
{
	switch ( type)
	{
	case CELLSIZE_TRI:
		return mtabTri.size();

	case CELLSIZE_QUAD:
		return mtabQuad.size();

	default:
		THROW_INTERNAL( "from CellCollection<DIM_2D>::Size - bad cell type" );
	}

	return 0;
}


//////////////////////////////////////////////////////////////////////
// class CellCollection<DIM_3D>
//////////////////////////////////////////////////////////////////////
template <>
class CellCollection<DIM_3D>
{
public:
	MGSize	Size( const MGSize& type) const;

	MGSize	Size() const	{ return mtabTet.size() + mtabHex.size() + mtabPrism.size() + mtabPiram.size(); }
	MGSize	NTypes() const	{ return  (mtabTet.size() ? 1 : 0)   + (mtabHex.size() ? 1 : 0)
									+ (mtabPrism.size() ? 1 : 0) + (mtabPiram.size() ? 1 : 0); }
	MGSize	Type( const MGSize& id) const;

	void	GetCCellIds( CGeomCell<DIM_3D>& ccell, const MGSize& i) const;

	void	GetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	void	SetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id);

	void	Resize( const MGSize& type, const MGSize& n);


private:
	vector< Cell<DIM_3D, CELLSIZE_TET> >	mtabTet;
	vector< Cell<DIM_3D, CELLSIZE_HEX> >	mtabHex;
	vector< Cell<DIM_3D, CELLSIZE_PRISM> >	mtabPrism;
	vector< Cell<DIM_3D, CELLSIZE_PIRAM> >	mtabPiram;
};


inline 	MGSize	CellCollection<DIM_3D>::Type( const MGSize& id) const
{
	MGSize k=0;
	if ( (id+1) == ( k += mtabTet.size() ? 1 : 0 ) )
		return CELLSIZE_TET;
	else if (  (id+1) == ( k += mtabHex.size() ? 1 : 0 ) )
		return CELLSIZE_HEX;
	else if (  (id+1) == ( k += mtabPrism.size() ? 1 : 0 ) )
		return CELLSIZE_PRISM;
	else if (  (id+1) == ( k += mtabPiram.size() ? 1 : 0 ) )
		return CELLSIZE_PIRAM;
	else
		THROW_INTERNAL( "error in CellCollection<DIM_3D>::Type");

	return 0;
}


inline MGSize CellCollection<DIM_3D>::Size( const MGSize& type) const
{
	switch ( type)
	{
	case CELLSIZE_TET:
		return mtabTet.size();

	case CELLSIZE_HEX:
		return mtabHex.size();

	case CELLSIZE_PRISM:
		return mtabPrism.size();

	case CELLSIZE_PIRAM:
		return mtabPiram.size();

	default:
		THROW_INTERNAL( "from CellCollection<DIM_3D>::Size - bad cell type" );
	}

	return 0;
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CELLCOLLECTION_H__
