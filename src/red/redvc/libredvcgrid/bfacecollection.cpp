#include "bfacecollection.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




//////////////////////////////////////////////////////////////////////
// class BFaceCollection<DIM_2D>
//////////////////////////////////////////////////////////////////////
void BFaceCollection<DIM_2D>::GetCBFaceIds( CGeomBFace<DIM_2D>& cbface, const MGSize& i) const
{
	if ( i < mtabEdge.size() )
	{
		cbface.rType() = BFACESIZE_EDGE;
		for ( MGSize k=0; k<BFACESIZE_EDGE; ++k)
			cbface.rId(k) = mtabEdge[i].cId(k);
	}
	else
		THROW_INTERNAL( "CellCollection<DIM_2D>::GetCellIds: out of range");
}

void BFaceCollection<DIM_2D>::GetCBFaceBndIds( CGeomBFace<DIM_2D>& cbface, const MGSize& i) const
{
	if ( i < mtabEdge.size() )
	{
		cbface.rType() = BFACESIZE_EDGE;
		cbface.rSurfId() = mtabEdge[i].cSurfId();

		for ( MGSize k=0; k<BFACESIZE_EDGE; ++k)
		{
			cbface.rId(k) = mtabEdge[i].cId(k);
			cbface.rBId(k) = mtabEdge[i].cBId(k);
		}
	}
	else
		THROW_INTERNAL( "CellCollection<DIM_2D>::GetCellIds: out of range");
}

void BFaceCollection<DIM_2D>::SetCBFaceBndIds( const CGeomBFace<DIM_2D>& cbface, const MGSize& i)
{
	if ( i < mtabEdge.size() )
	{
		ASSERT( cbface.Size() == BFACESIZE_EDGE);

		for ( MGSize k=0; k<BFACESIZE_EDGE; ++k)
			mtabEdge[i].rBId(k) = cbface.cBId(k);
	}
	else
		THROW_INTERNAL( "CellCollection<DIM_2D>::GetCellIds: out of range");
}


void BFaceCollection<DIM_2D>::GetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( tabid.size() == type);
	switch ( type)
	{
	case BFACESIZE_EDGE:
		for ( MGSize k=0; k<BFACESIZE_EDGE; ++k)
			tabid[k] = mtabEdge[id].cId(k);
		break;

	default:
		THROW_INTERNAL( "from BFaceCollection<DIM_2D>::GetBFaceIds - bad cell type" );
	}
}

void BFaceCollection<DIM_2D>::SetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id)
{
	ASSERT( tabid.size() == type);
	switch ( type)
	{
	case BFACESIZE_EDGE:
		for ( MGSize k=0; k<BFACESIZE_EDGE; ++k)
			mtabEdge[id].rId(k) = tabid[k];
		break;

	default:
		THROW_INTERNAL( "from BFaceCollection<DIM_2D>::SetBFaceIds - bad cell type" );
	}
}

void BFaceCollection<DIM_2D>::GetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id ) const
{
	switch ( type)
	{
	case BFACESIZE_EDGE:
		idsurf = mtabEdge[id].cSurfId();
		break;

	default:
		THROW_INTERNAL( "from BFaceCollection<DIM_2D>::GetBFaceSurf - bad cell type" );
	}
}

void BFaceCollection<DIM_2D>::SetBFaceSurf(  const MGSize& idsurf, const MGSize& type, const MGSize& id )
{
	switch ( type)
	{
	case BFACESIZE_EDGE:
		mtabEdge[id].rSurfId() = idsurf;
		break;

	default:
		THROW_INTERNAL( "from BFaceCollection<DIM_2D>::SetBFaceSurf - bad cell type" );
	}
}

void BFaceCollection<DIM_2D>::GetBFaceBIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( tabid.size() == type);
	switch ( type)
	{
	case BFACESIZE_EDGE:
		for ( MGSize k=0; k<BFACESIZE_EDGE; ++k)
			tabid[k] = mtabEdge[id].cBId(k);
		break;

	default:
		THROW_INTERNAL( "from BFaceCollection<DIM_2D>::GetBFaceBIds - bad cell type" );
	}
}

void BFaceCollection<DIM_2D>::Resize( const MGSize& type, const MGSize& n)
{
	switch ( type)
	{
	case BFACESIZE_EDGE:
		mtabEdge.resize( n);
		break;

	default:
		THROW_INTERNAL( "from BFaceCollection<DIM_2D>::TabReize - bad face type" );
	}
}


//////////////////////////////////////////////////////////////////////
// class BFaceCollection<DIM_3D>
//////////////////////////////////////////////////////////////////////

void BFaceCollection<DIM_3D>::GetCBFaceIds( CGeomBFace<DIM_3D>& cbface, const MGSize& id) const
{
	MGSize i = id;
	if ( i < mtabTri.size() )
	{
		cbface.rType() = BFACESIZE_TRI;
		for ( MGSize k=0; k<BFACESIZE_TRI; ++k)
			cbface.rId(k) = mtabTri[i].cId(k);
	}
	else if ( (i-=mtabTri.size()) < mtabQuad.size() )
	{
		cbface.rType() = BFACESIZE_QUAD;
		for ( MGSize k=0; k<BFACESIZE_QUAD; ++k)
			cbface.rId(k) = mtabQuad[i].cId(k);
	}
	else
		THROW_INTERNAL( "BFaceCollection<DIM_3D>::GetCBFaceIds: out of range");
}

void BFaceCollection<DIM_3D>::GetCBFaceBndIds( CGeomBFace<DIM_3D>& cbface, const MGSize& id) const
{
	MGSize i = id;
	if ( i < mtabTri.size() )
	{
		cbface.rType() = BFACESIZE_TRI;
		cbface.rSurfId() = mtabTri[i].cSurfId();

		for ( MGSize k=0; k<BFACESIZE_TRI; ++k)
		{
			cbface.rId(k) = mtabTri[i].cId(k);
			cbface.rBId(k) = mtabTri[i].cBId(k);
		}
	}
	else if ( (i-=mtabTri.size()) < mtabQuad.size() )
	{
		cbface.rType() = BFACESIZE_QUAD;
		cbface.rSurfId() = mtabQuad[i].cSurfId();

		for ( MGSize k=0; k<BFACESIZE_QUAD; ++k)
		{
			cbface.rId(k) = mtabQuad[i].cId(k);
			cbface.rBId(k) = mtabQuad[i].cBId(k);
		}
	}
	else
		THROW_INTERNAL( "BFaceCollection<DIM_3D>::GetCBFaceBndIds: out of range");
}


void BFaceCollection<DIM_3D>::SetCBFaceBndIds( const CGeomBFace<DIM_3D>& cbface, const MGSize& id)
{
	MGSize i = id;
	if ( i < mtabTri.size() )
	{
		ASSERT( cbface.Size() == BFACESIZE_TRI);

		for ( MGSize k=0; k<BFACESIZE_TRI; ++k)
			mtabTri[i].rBId(k) = cbface.cBId(k);
	}
	else if ( (i-=mtabTri.size()) < mtabQuad.size() )
	{
		ASSERT( cbface.Size() == BFACESIZE_QUAD);

		for ( MGSize k=0; k<BFACESIZE_QUAD; ++k)
			mtabQuad[i].rBId(k) = cbface.cBId(k);
	}
	else
		THROW_INTERNAL( "BFaceCollection<DIM_3D>::SetCBFaceBndIds: out of range");
}

void BFaceCollection<DIM_3D>::GetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( tabid.size() == type);

	switch ( type)
	{
	case BFACESIZE_TRI:
		for ( MGSize k=0; k<BFACESIZE_TRI; ++k)
			tabid[k] = mtabTri[id].cId(k);
		break;

	case BFACESIZE_QUAD:
		for ( MGSize k=0; k<BFACESIZE_QUAD; ++k)
			tabid[k] = mtabQuad[id].cId(k);
		break;

	default:
		THROW_INTERNAL( "from BFaceCollection<DIM_3D>::GetBFaceIds - bad cell type" );
	}
}

void BFaceCollection<DIM_3D>::SetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id)
{
	ASSERT( tabid.size() == type);

	switch ( type)
	{
	case BFACESIZE_TRI:
		for ( MGSize k=0; k<BFACESIZE_TRI; ++k)
			mtabTri[id].rId(k) = tabid[k];
		break;

	case BFACESIZE_QUAD:
		for ( MGSize k=0; k<BFACESIZE_QUAD; ++k)
			mtabQuad[id].rId(k) = tabid[k];
		break;

	default:
		THROW_INTERNAL( "from BFaceCollection<DIM_3D>::SetBFaceIds - bad cell type" );
	}
}

void BFaceCollection<DIM_3D>::GetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id) const
{
	switch ( type)
	{
	case BFACESIZE_TRI:
		idsurf = mtabTri[id].cSurfId();
		break;

	case BFACESIZE_QUAD:
		idsurf = mtabQuad[id].cSurfId();
		break;

	default:
		THROW_INTERNAL( "from BFaceCollection<DIM_3D>::SetBFaceSurf - bad cell type" );
	}
}


void BFaceCollection<DIM_3D>::SetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id)
{
	switch ( type)
	{
	case BFACESIZE_TRI:
		mtabTri[id].rSurfId() = idsurf;
		break;

	case BFACESIZE_QUAD:
		mtabQuad[id].rSurfId() = idsurf;
		break;

	default:
		THROW_INTERNAL( "from BFaceCollection<DIM_3D>::SetBFaceSurf - bad cell type" );
	}
}


void BFaceCollection<DIM_3D>::GetBFaceBIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( tabid.size() == type);

	switch ( type)
	{
	case BFACESIZE_TRI:
		for ( MGSize k=0; k<BFACESIZE_TRI; ++k)
			tabid[k] = mtabTri[id].cBId(k);
		break;

	case BFACESIZE_QUAD:
		for ( MGSize k=0; k<BFACESIZE_QUAD; ++k)
			tabid[k] = mtabQuad[id].cBId(k);
		break;

	default:
		THROW_INTERNAL( "from BFaceCollection<DIM_3D>::GetBFaceBIds - bad cell type" );
	}
}

void BFaceCollection<DIM_3D>::Resize( const MGSize& type, const MGSize& n)
{
	switch ( type)
	{
	case BFACESIZE_TRI:
		mtabTri.resize( n);
		break;

	case BFACESIZE_QUAD:
		mtabQuad.resize( n);
		break;

	default:
		THROW_INTERNAL( "from BFaceCollection<DIM_3D>::TabResize - bad face type" );
	}
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

