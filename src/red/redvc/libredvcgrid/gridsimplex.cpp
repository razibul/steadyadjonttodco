#include "gridsimplex.h"

#include "libredcore/configconst.h"
#include "libredcore/processinfo.h"
#include "libcoreio/store.h"
#include "libcoreio/readmsh2.h"
#include "libcoreio/writemsh2.h"
#include "libcoreio/writetec.h"
#include "libcorecommon/key.h"

#include "libredphysics/bc.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void GridSimplex<DIM>::Create(const CfgSection* pcfgsec)
{
	ConfigBase::Create( pcfgsec);
}

template <Dimension DIM>
void GridSimplex<DIM>::PostCreateCheck() const
{
	ConfigBase::PostCreateCheck();
}

template <Dimension DIM>
void GridSimplex<DIM>::Init()
{
// 	MGString	fname	= ConfigSect().ValueString( ConfigStr::Solver::Grid::FNAME );
// 	bool		bBIN	= IO::FileTypeToBool( ConfigSect().ValueString( ConfigStr::Solver::Grid::FType::KEY ) );
// 
// 	IO::ReadMSH2	reader( *this);
// 	reader.DoRead( fname, bBIN);
// 
// 	IO::WriteMSH2	writer( *this);
// 	writer.DoWrite( "out.msb2", true);
// 	writer.DoWrite( "out.msh2", false);
// 
// 	////////////////////////////////////////

	mMaxAngle = ConfigSect().ValueFloat( ConfigStr::Solver::Grid::MAXANGLE );

	InitBNodes();
	InitNodeVolume();
	InitBFaceCells();

	printf( " band width = %d / %d\n", CalcBandWidth(), SizeNodeTab() );

	///////////////////////////
	CheckBFaceVn();

	//for ( MGSize i=0; i<SizeBFaceTab(); ++i)
	//{
	//	if ( mtabBFace[itr->second].rCellId() = i;
	//}
}

template <Dimension DIM>
void GridSimplex<DIM>::InitGlobId()
{
	for ( MGSize i=0; i<SizeNodeTab(); ++i)
		mtabNode[i].rGId() = i;

	mrangeOwnedNodes.first	= 0;
	mrangeOwnedNodes.second	= SizeNodeTab();
}

template <Dimension DIM>
void GridSimplex<DIM>::InitGlobId( const vector<MGSize>& tab, const pair<MGSize,MGSize> range)
{
	if ( tab.size() != SizeNodeTab() )
		THROW_INTERNAL( "GridSimplex<DIM>::InitGlobId -- bad tab.size()");
		
	for ( MGSize i=0; i<SizeNodeTab(); ++i)
	{
		mtabNode[i].rGId() = tab[i];
		mtabNodeInitial[i].rGId() = tab[i];
	}

	mrangeOwnedNodes = range;
}


template <Dimension DIM>
void GridSimplex<DIM>::CheckBFaceVn()
{
	typedef pair< Key<DIM>, MGSize > MyPair;

	//map< Key<DIM>, MGSize > mapinface;
	CGeomCell<DIM>	gcell;
	Key<DIM>	key;

	//for ( MGSize i=0; i<SizeCellTab(); ++i)
	//{
	//	gcell = cCell( i);

	//	for ( MGSize ifc=0; ifc<DIM+1; ++ifc)
	//	{
	//		for ( MGSize k=0; k<DIM; ++k)
	//			key.rElem(k) = gcell.cId( CGeomCell<DIM>::cFaceConn( ifc, k) ); 

	//		key.Sort();

	//		mapinface.insert( typename map< Key<DIM>, MGSize >::value_type( key,i ) );
	//	}
	//}

	map<MGSize,bool> mapvn;


	for ( MGSize i=0; i<SizeBFaceTab(); ++i)
	{
		CGeomBFace<DIM>		bface = cBFace(i);

		//for ( MGSize k=0; k<DIM; ++k)
		//	key.rElem(k) = bface.cId( k ); 
		//
		//key.Sort();

		//typename map< Key<DIM>, MGSize >::iterator itr = mapinface.find( key);
		//if ( itr == mapinface.end() )
		//	THROW_INTERNAL( "mesh corrupted !!!");

		//gcell = cCell( itr->second );
		gcell = cCell( bface.cCellId() );


		Vect<DIM>	vct(0.0);

		for ( MGSize k=0; k<DIM+1; ++k)
			vct += gcell.cNode(k).cPos();

		vct /= (MGFloat)(DIM+1);

		Vect<DIM>	vctf(0.0);

		for ( MGSize k=0; k<DIM; ++k)
			vctf += bface.cNode(k).cPos();

		vctf /= (MGFloat)(DIM);

		Vect<DIM>	dvct = vct - vctf;

		if ( dvct * bface.Vn() < 0 )
			mapvn[ bface.cSurfId() ] = false;
		else
			mapvn[ bface.cSurfId() ] = true;

	}

	//cout << endl <<  " ---- SURF vn ----" << endl;
	for ( map<MGSize,bool>::iterator ii=mapvn.begin(); ii!=mapvn.end(); ++ii)
		if ( ! ii->second )
			cout << "surf id " << ii->first << " : " << ii->second << endl;
	//cout <<  " ---- OK ----" << endl;
}

template <Dimension DIM>
void GridSimplex<DIM>::InitBNodes()
{
	// boundary node is NOT unique - every face has its own proxy to boundary node
	
	mtabBNode.clear();
	if ( SizeBFaceTab() == 0 )
	{
		//cout << " ------------- SizeBFaceTab() == 0 ---------------" << endl;
		return;
	}


	CGeomBFace<DIM>		bfac;
	Vect<DIM>	vct;

	MGSize	n=0, idcur;

	for ( MGSize i=0; i<SizeBFaceTab(); ++i)
	{
		GetCBFaceIds( bfac, i);
		n += bfac.Size();
	}

	mtabBNode.reserve( n);


	// preparing table of references to bnodes and real nodes (used for averaging)
	idcur = 0;
	for ( MGSize i=0; i<SizeBFaceTab(); ++i)
	{
		GetCBFaceIds( bfac, i);
		for ( MGSize k=0; k<bfac.Size(); ++k)
			bfac.rNode(k) = mtabNode[bfac.cId(k)];
		
		for ( MGSize k=0; k<bfac.Size(); ++k)
		{
			BoundaryNode<DIM> bnod;
			
			bnod.rPId() = bfac.cId(k);
			bnod.rPFId() = i;	// ????
			bnod.rVn() = bfac.Vn();
			mtabBNode.push_back( bnod);

			bfac.rBId(k) = idcur++;
		}

		SetCBFaceBndIds( bfac, i);
	}
	
	vector< pair< MGSize, MGSize> >	tabid;
	tabid.resize( mtabBNode.size() );
	
	for ( MGSize i=0; i<mtabBNode.size(); ++i)
	{
		tabid[i].first = mtabBNode[i].cPId();
		tabid[i].second = i;
	}

	// Averaging bnode vn by taking into account faces with similar vn	
	sort( tabid.begin(), tabid.end() );
	
	vector< pair< MGSize, MGSize> >::iterator	itr, itri, itrb, itre;
	pair< MGSize, MGSize>	elem(0,0);
	
	elem.first = tabid[0].first;
	itre = upper_bound( tabid.begin(), tabid.end(), elem);
	if ( itre == tabid.end() )
		THROW_INTERNAL( "itrb = upper_bound(...) not found");
	
	do
	{
		elem.first = itre->first + 1;
		itrb = itre;		

		itre = lower_bound( tabid.begin(), tabid.end(), elem);

		for ( itr=itrb; itr!=itre; ++itr)
		{
			
			BoundaryNode<DIM>	&pnod = mtabBNode[itr->second];
			CGeomBFace<DIM>		pfac;
			
			GetCBFaceIds( pfac, pnod.cPFId() );
			for ( MGSize k=0; k<pfac.Size(); ++k)
				pfac.rNode(k) = mtabNode[pfac.cId(k)];

			Vect<DIM>	vnpf = pfac.Vn();

			MGFloat				wgh = vnpf.module();
			Vect<DIM>	vsum = vnpf;

			for ( itri=itrb; itri!=itre; ++itri)
			{
				if ( itr != itri)
				{
					BoundaryNode<DIM>	&nod = mtabBNode[itri->second];
					CGeomBFace<DIM>		fac;

					GetCBFaceIds( fac, nod.cPFId() );
					for ( MGSize k=0; k<fac.Size(); ++k)
						fac.rNode(k) = mtabNode[fac.cId(k)];

					Vect<DIM>	vnf = fac.Vn();
				
					if ( vnf.versor() * vnpf.versor() > mMaxAngle )
					{
						wgh += vnf.module();
						vsum += vnf;
					}
				}
			}
			
			vsum /= wgh;
			pnod.rVn() = vsum;
		}

	}
	while ( itre != tabid.end() );

}

template <Dimension DIM>
void GridSimplex<DIM>::InitNodeVolume()
{
	CGeomCell<DIM>	gcell;
	MGFloat	vol, voltot = 0.0;

	for (MGSize i = 0; i < mtabNode.size(); i++)
	{
		mtabNode[i].rVolume() = 0.0;
	}

	for ( MGSize i=0; i<SizeCellTab(); ++i)
	{
		gcell = cCell( i);
		voltot += vol = gcell.Volume();

		if ( vol < 0. )
		{
			cout << "cell id = " << i << endl;
			cout << " [ " << gcell.cId(0);
			for ( MGSize k=1; k<gcell.Size(); ++k)
				cout << ", " << gcell.cId(k);
			cout << " ] " << endl;
			for ( MGSize k=0; k<gcell.Size(); ++k)
				gcell.cNode(k).cPos().Write();
			cout << "volume = " << setprecision(16) << vol << endl;
			//THROW_INTERNAL( "GridSimplex<" << DIM << ">::InitNodeVolume -- Negative cell volume found");
		}
		
		vol /= gcell.Size();
		for ( MGSize k=0; k<gcell.Size(); ++k)
			mtabNode[gcell.cId(k)].rVolume() += vol;
	}

	//cout << "Grid volume (cell sum) = " << setprecision(12) << voltot << endl;

	voltot = 0.0;
	for ( MGSize i=0; i< mtabNode.size(); ++i)
	{
		if ( mtabNode[i].cVolume() < 0. )
			THROW_INTERNAL( "GridSimplex<" << DIM << ">::InitNodeVolume -- Negative node volume found");

		voltot += mtabNode[i].cVolume();
	}

	//cout << "Grid volume (cell sum) = " << setprecision(12) << voltot << endl;
}

template <Dimension DIM>
void GridSimplex<DIM>::InitBFaceCells()
{
	map< Key<DIM>, MGSize>    mapbface;

	for ( MGSize i=0; i<SizeBFaceTab(); ++i)
	{
		mtabBFace[i].rCellId() = SizeCellTab() + 1;
		CGeomBFace<DIM> face;
		GetBFace( i, face);

		Key<DIM> key;
		for ( MGSize in=0; in<DIM; ++in)
			key.rElem(in) = face.cId(in);

		key.Sort();

		if ( mapbface.find( key) == mapbface.end() )
			mapbface.insert( typename map< Key<DIM>, MGSize>::value_type( key, i) );
		else
		{
			cout << "---- echo from :: " << ProcessInfo::cNumProc() << endl;
			cout << "bface already exists inside the 'mapbface'" << endl;
			cout << "Key = ";
			key.Dump();
			cout << endl;
			THROW_INTERNAL( "bface already exists inside the 'mapbface'");
		}

	}

	for ( MGSize i=0; i<SizeCellTab(); ++i)
	{
		CGeomCell<DIM>   gcell;
		GetCell( i, gcell);

		for ( MGSize ifc=0; ifc<gcell.SizeFace(); ++ifc)
		{
			Key<DIM> facekey;
			for ( MGSize in=0; in<DIM; ++in)
				facekey.rElem(in) = gcell.cId( gcell.cFaceConn(ifc,in) );

			facekey.Sort();

			typename map< Key<DIM>, MGSize>::iterator  itr;

			if ( (itr = mapbface.find( facekey)) !=  mapbface.end() )
				mtabBFace[itr->second].rCellId() = i;
		}
	}

	//THROW_INTERNAL( "InitBFaceCells() - not implemented");
}

//template <>
//void GridSimplex<DIM_2D>::InitBFaceCells()
//{
//	map< Key<2>, MGSize>    mapbface;
//
//	for ( MGSize i=0; i<SizeBFaceTab(); ++i)
//	{
//		CGeomBFace<DIM_2D> face;
//		GetBFace( i, face);
//
//		Key<2> key( face.cId(0), face.cId(1) );
//		key.Sort();
//
//		if ( mapbface.find( key) == mapbface.end() )
//			mapbface.insert( map< Key<2>, MGSize>::value_type( key, i) );
//		else
//			THROW_INTERNAL( "bface already exists inside the 'mapbface'");
//
//	}
//
//	for ( MGSize i=0; i<SizeCellTab(); ++i)
//	{
//		CGeomCell<DIM_2D>   gcell;
//		GetCell( i, gcell);
//
//		for ( MGSize ifc=0; ifc<gcell.SizeFace(); ++ifc)
//		{
//			Key<2> facekey( gcell.cId( gcell.cFaceConn(ifc,0)), gcell.cId( gcell.cFaceConn(ifc,1)) );
//			facekey.Sort();
//
//			map< Key<2>, MGSize>::iterator  itr;
//
//			if ( (itr = mapbface.find( facekey)) !=  mapbface.end() )
//				mtabBFace[itr->second].rCellId() = i;
//				//mtabBFace[i].rCellId() = itr->second;
//				//mcolBFace.SetBFaceCell( i, cBFace( itr->second).cType(), itr->second );
//		}
//	}
//
//}


template <Dimension DIM>
MGSize GridSimplex<DIM>::CalcBandWidth()
{
	MGSize	bret=0, b;
	CGeomCell<DIM>	gcell;

	for ( MGSize i=0; i<SizeCellTab(); ++i)
	{
		gcell = cCell( i);
		for ( MGSize j=0; j<gcell.Size(); ++j)
			for ( MGSize k=j+1; k<gcell.Size(); ++k)
			{
				b = ::abs( (MGInt) gcell.cId(k) - (MGInt)gcell.cId(j) );
				if ( b > bret) 
					bret = b;
			}
	}

	return bret;
}

template <Dimension DIM>
void GridSimplex<DIM>::InitNodeCopy()
{
	mtabNodeInitial.reserve(mtabNode.size());
	for (MGSize i = 0; i < mtabNode.size(); i++)
		mtabNodeInitial.push_back(mtabNode[i]);
	

	mtabBNodeInitial.reserve(mtabBNode.size());
	for (MGSize i = 0; i < mtabBNode.size(); i++)
		mtabBNodeInitial.push_back(mtabBNode[i]);


}

template <Dimension DIM>
MGSize GridSimplex<DIM>::FindNearestNode(const GVec& pos, MGFloat& dist) const
{
	dist = (mtabNode[0].cPos() - pos).module();
	MGFloat temp;
	MGSize indx = 0;
	for (MGSize i = 1; i < mtabNode.size(); i++)
	{
		temp = (mtabNode[i].cPos() - pos).module();
		if ( dist > temp )
		{
			//indx = i;
			indx = mtabNode[i].cGId();
			dist = temp;
		}
	}

	return indx;
}

template <Dimension DIM>
bool GridSimplex<DIM>::IsOwned(const MGSize& globid) const
{
	if (globid >= mrangeOwnedNodes.first && globid < mrangeOwnedNodes.second)
	{
		return true;
	}
	else return false;
}

//////////////////////////////////////////////////////////////////////
template class GridSimplex<DIM_2D>;
template class GridSimplex<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

