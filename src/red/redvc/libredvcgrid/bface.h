#ifndef __BFACE_H__
#define __BFACE_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "redvc/libredvcgrid/gridconst.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


using namespace Geom;



//////////////////////////////////////////////////////////////////////
// class BFace
//////////////////////////////////////////////////////////////////////
template<Dimension DIM, MGSize SIZE>
class BFace
{
public:
	MGSize			Size() const					{ return SIZE;}

	const MGSize&	cId( const MGSize& i) const		{ return mtabId[i];}
	const MGSize&	cBId( const MGSize& i) const	{ return mtabBndId[i];}
	const MGSize&	cSurfId() const					{ return mSurfId;}
	const MGSize&   cCellId() const                 { return mCellId;}


	MGSize&			rId( const MGSize& i)			{ return mtabId[i];}
	MGSize&			rBId( const MGSize& i)			{ return mtabBndId[i];}
	MGSize&			rSurfId()						{ return mSurfId;}
	MGSize&         rCellId()                       { return  mCellId;}

private:
	MGSize	mtabId[SIZE];
	MGSize	mtabBndId[SIZE];
	MGSize	mSurfId;
	MGSize  mCellId;
};
//////////////////////////////////////////////////////////////////////



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __BFACE_H__
