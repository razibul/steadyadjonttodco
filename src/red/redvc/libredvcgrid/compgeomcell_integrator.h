#ifndef __COMPGEOMCELL_INTEGRATOR_H__
#define __COMPGEOMCELL_INTEGRATOR_H__

#include "libcoresystem/mgdecl.h"
#include "redvc/libredvcgrid/compgeomcell.h"
#include "libcoregeom/gaussweights.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class CellInegrator
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class CellInegrator
{
	typedef Geom::Vect<DIM>	GVec;

public:
	CellInegrator( const CGeomCell<DIM>& gcell, const MGSize& order=1 ) : mGCell(gcell), mOrder(order)	{}


//--------------------------------------------------------------------
// VolumeIntegral

	template <class TIN, class TIN2, class TOUT, class FUNC>		// TOUT FUNC( GVec pos, TIN u);
	TOUT	VolumeIntegralPlus( const FUNC& func, const TIN tabu[DIM+1], const TIN2 tabs[DIM+1] )
	{
		ASSERT( mGCell.SizeFace() == mGCell.Size() );

		GaussQuadrature<DIM> gquad( mOrder);

		TOUT res;
		for ( MGSize ipnt=0; ipnt<gquad.Size(); ++ipnt)
		{
			Vect<DIM> pos = mGCell.cNode(0).cPos() * gquad.BaryCoordinates(ipnt)[0];
			TIN  u = tabu[0] * gquad.BaryCoordinates(ipnt)[0];
			TIN2 s = tabs[0] * gquad.BaryCoordinates(ipnt)[0];
			for ( MGSize k=1; k<=DIM; ++k)
			{
				pos += mGCell.cNode(k).cPos() * gquad.BaryCoordinates(ipnt)[k];
				u += tabu[k] * gquad.BaryCoordinates(ipnt)[k];
				s += tabs[k] * gquad.BaryCoordinates(ipnt)[k];
			}

			TOUT val = func( pos, u, s);
			res = ipnt==0 ? val * gquad.Weight(ipnt) : res + val * gquad.Weight(ipnt);
			//integ += val * gquad.Weight(ipnt);
		}

		MGFloat vol = mGCell.Volume();
		res *= vol;

		return res;
	}

	template <class TIN, class TOUT, class FUNC>		// TOUT FUNC( GVec pos, TIN u);
	TOUT	VolumeIntegral( const FUNC& func, const TIN tabu[DIM+1] )
	{
		ASSERT( mGCell.SizeFace() == mGCell.Size() );

		GaussQuadrature<DIM> gquad( mOrder);

		TOUT res;
		for ( MGSize ipnt=0; ipnt<gquad.Size(); ++ipnt)
		{
			Vect<DIM> pos = mGCell.cNode(0).cPos() * gquad.BaryCoordinates(ipnt)[0];
			TIN u = tabu[0] * gquad.BaryCoordinates(ipnt)[0];
			for ( MGSize k=1; k<=DIM; ++k)
			{
				pos += mGCell.cNode(k).cPos() * gquad.BaryCoordinates(ipnt)[k];
				u += tabu[k] * gquad.BaryCoordinates(ipnt)[k];
			}

			TOUT val = func( pos, u);
			res = ipnt==0 ? val * gquad.Weight(ipnt) : res + val * gquad.Weight(ipnt);
			//integ += val * gquad.Weight(ipnt);
		}

		MGFloat vol = mGCell.Volume();
		res *= vol;

		return res;
	}


	template <class TIN, class TOUT, class FUNC>		// TOUT FUNC( GVec pos, TIN u);
	TOUT	VolumeIntegral( const TIN tabu[DIM+1] )
	{
		FUNC func;
		return this->VolumeIntegral<TIN,TOUT,FUNC>( func, tabu);
	}

//--------------------------------------------------------------------
// BoundaryIntegral

	template <class TIN, class TOUT, class FUNC>
	TOUT	BoundaryIntegral( const FUNC& func, const TIN tabu[DIM+1] )	// TOUT FUNC( GVec pos, TIN u, GVec vn);
	{
		ASSERT( mGCell.SizeFace() == mGCell.Size() );

		Geom::GaussQuadrature< static_cast<Dimension>(DIM-1) > gquad( mOrder);

		TOUT res;
		for ( MGSize ifac=0; ifac<mGCell.SizeFace(); ++ifac)
		{
			GVec vn = mGCell.Vn(ifac);
			MGFloat area = vn.module();
			vn *= -1./area;

			TOUT flux; 
			for ( MGSize ipnt=0; ipnt<gquad.Size(); ++ipnt )
			{
				Vect<DIM> pos = mGCell.cNode( mGCell.cFaceConn(ifac,0) ).cPos() * gquad.BaryCoordinates(ipnt)[0];
				TIN u = tabu[mGCell.cFaceConn(ifac,0)]*gquad.BaryCoordinates(ipnt)[0];
				for ( MGSize idim=1; idim<DIM; ++idim)
				{
					pos += mGCell.cNode( mGCell.cFaceConn(ifac,idim) ).cPos() * gquad.BaryCoordinates(ipnt)[idim];
					u += tabu[ mGCell.cFaceConn(ifac,idim) ] * gquad.BaryCoordinates(ipnt)[idim];
				}

				TOUT tmp = func( pos, u, vn) * gquad.Weight(ipnt);

				flux = ipnt == 0 ? tmp : tmp + flux;
				//flux += func( pos, u, vn) * gquad.Weight(ipnt);

				//for ( MGSize idim=0; idim<DIM; ++idim)
				//	cout << gauss.BaryCoordinates(ipnt)[idim] << " ";
				//cout << " -- " << gauss.Weight(ipnt) << endl;

			}

			res = ifac==0 ? area * flux : res + area * flux;
			//res += flux;
		}

		return res;
	}


	template <class TIN, class TOUT, class FUNC>
	TOUT	BoundaryIntegral( const TIN tabu[DIM+1] )	// TOUT FUNC( GVec pos, TIN u, GVec vn);
	{
		FUNC func;
		return this->BoundaryIntegral<TIN,TOUT,FUNC>( func, tabu);
	}


private:
	const CGeomCell<DIM>& mGCell;
	MGSize mOrder;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __COMPGEOMCELL_INTEGRATOR_H__

