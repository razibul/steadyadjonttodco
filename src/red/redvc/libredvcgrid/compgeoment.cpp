#include "compgeoment.h"
#include "libcoregeom/geom.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 





template <Dimension DIM>
MGFloat Volume( const Node<DIM> tabnod[], const MGSize& n)
{
	THROW_INTERNAL( "Not implemented");
	return 0.0;
}

template <>
MGFloat Volume<DIM_2D>( const Node<DIM_2D> tabnod[], const MGSize& n)
{
	switch ( n)
	{
	case GEOMSIZE_TRI:
		return Geom::VolumeTRI( tabnod[0].cPos(), tabnod[1].cPos(), tabnod[2].cPos());

	case GEOMSIZE_QUAD:
		return Geom::VolumeQUAD( tabnod[0].cPos(), tabnod[1].cPos(), tabnod[2].cPos(), tabnod[3].cPos());
	}

	ASSERT( 0);
	return 0.0;
}

template <>
MGFloat Volume<DIM_3D>( const Node<DIM_3D> tabnod[], const MGSize& n)
{
	switch ( n)
	{
	case GEOMSIZE_TET:
		return Geom::VolumeTET( tabnod[0].cPos(), tabnod[1].cPos(), tabnod[2].cPos(), tabnod[3].cPos());

	case GEOMSIZE_HEX:
		return Geom::VolumeHEX( tabnod[0].cPos(), tabnod[1].cPos(), tabnod[2].cPos(), tabnod[3].cPos(),
								tabnod[4].cPos(), tabnod[5].cPos(), tabnod[6].cPos(), tabnod[7].cPos() );

	case GEOMSIZE_PIRAM:
		return Geom::VolumePIRAM( tabnod[0].cPos(), tabnod[1].cPos(), tabnod[2].cPos(), tabnod[3].cPos(), 
								tabnod[4].cPos());

	case GEOMSIZE_PRISM:
		return Geom::VolumePRISM( tabnod[0].cPos(), tabnod[1].cPos(), tabnod[2].cPos(), tabnod[3].cPos(), 
								tabnod[4].cPos(), tabnod[5].cPos());
	}

	ASSERT( 0);
	return 0.0;
}





template <Dimension DIM>
MGFloat InRadius( const Node<DIM> tabnod[], const MGSize& n)
{
	THROW_INTERNAL( "Not implemented");
	return 0.0;
}

template <>
MGFloat InRadius<DIM_2D>( const Node<DIM_2D> tabnod[], const MGSize& n)
{
	switch ( n)
	{
	case GEOMSIZE_TRI:
		return Geom::InRadiusTRI( tabnod[0].cPos(), tabnod[1].cPos(), tabnod[2].cPos());

	default:
		return ::sqrt( Volume( tabnod, n));
	}

	ASSERT( 0);
	return 0.0;
}

template <>
MGFloat InRadius<DIM_3D>( const Node<DIM_3D> tabnod[], const MGSize& n)
{
	switch ( n)
	{
	case GEOMSIZE_TET:
		return Geom::InRadiusTET( tabnod[0].cPos(), tabnod[1].cPos(), tabnod[2].cPos(), tabnod[3].cPos());

	default:
		return ::pow( Volume( tabnod, n), 1.0/DIM_3D);
	}

	ASSERT( 0);
	return 0.0;
}



template <Dimension DIM>
MGFloat Area( const Node<DIM> tabnod[], const MGSize& n)
{
	THROW_INTERNAL( "Not implemented");
	return 0.0;
}


template <>
MGFloat Area<DIM_2D>( const Node<DIM_2D> tabnod[], const MGSize& n)
{
	switch ( n)
	{
	case GEOMSIZE_SEG:
		return ( tabnod[0].cPos() - tabnod[1].cPos() ).module();

	}

	ASSERT( 0);
	return 0.0;
}


template <>
MGFloat Area<DIM_3D>( const Node<DIM_3D> tabnod[], const MGSize& n)
{
	switch ( n)
	{
	case GEOMSIZE_TRI:
		return Geom::AreaTRI( tabnod[0].cPos(), tabnod[1].cPos(), tabnod[2].cPos());

	case GEOMSIZE_QUAD:
		return Geom::AreaQUAD( tabnod[0].cPos(), tabnod[1].cPos(), tabnod[2].cPos(), tabnod[3].cPos());
	}

	ASSERT( 0);
	return 0.0;
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

