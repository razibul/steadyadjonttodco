#ifndef __NODE_H__
#define __NODE_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/vect.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



using namespace Geom;




//////////////////////////////////////////////////////////////////////
// class Node
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Node
{
public:
	Node() : mPos(), mVolume(0.0)	{}
	Node( const Vect<DIM>& vct) : mPos( vct), mVolume(0.0)	{}


	const MGSize&		cGId() const	{ return mGlobId; }
	const Vect<DIM>&	cPos() const	{ return mPos;}
	const MGFloat&		cVolume() const	{ return mVolume;}

	MGSize&				rGId() 			{ return mGlobId; }
	Vect<DIM>&			rPos()			{ return mPos;}
	MGFloat&			rVolume()		{ return mVolume;}


	operator	Vect<DIM>()				{ return mPos;}

private:
	MGSize		mGlobId;
	Vect<DIM>	mPos;
	MGFloat		mVolume;
};




//////////////////////////////////////////////////////////////////////
// class BoundaryNode
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class BoundaryNode
{
public:
	BoundaryNode() : mParentId(0), mVn()	{}

	const MGSize&		cPId() const	{ return mParentId;}
	const MGSize&		cPFId() const	{ return mParentFaceId;}
	const Vect<DIM>&	cVn() const		{ return mVn;}

	MGSize&				rPId()			{ return mParentId;}
	MGSize&				rPFId()			{ return mParentFaceId;}
	Vect<DIM>&			rVn()			{ return mVn;}

private:
	MGSize				mParentId;
	MGSize				mParentFaceId;
	Vect<DIM>	mVn;
};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __NODE_H__
