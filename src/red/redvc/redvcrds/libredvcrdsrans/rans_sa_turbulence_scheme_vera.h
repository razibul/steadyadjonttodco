#ifndef __RANS0GALERKINSCHEME_VERA_H__
#define __RANS0GALERKINSCHEME_VERA_H__

#include "redvc/redvcrds/libredvcrdsns/ns_galerkinscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/scpsischeme.h"
#include "redvc/redvcrds/libredvcrdscommon/scldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/scnscheme.h"
#include "libredphysics/ranssaequations.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class RANSSATurbulenceSchemeBase
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class RANSSATurbulenceSchemeBase_verA : public FlowFunc<DIM, EQN_RANS_SA, EquationDef<DIM,EQN_RANS_SA>::SplitingFlowTurb::template Block<2>::VBSIZE >
{
    typedef EquationDef<DIM,EQN_RANS_SA> FEqns;
    typedef FlowFunc<DIM, EQN_RANS_SA, FEqns::SplitingFlowTurb::template Block<2>::VBSIZE > TBase;
    typedef typename TBase::BVec    BVec;
    typedef typename TBase::BMtx    BMtx;


    enum { EQSIZE = FEqns::SIZE };
    enum { AUXSIZE = FEqns::AUXSIZE };

    typedef Vect<DIM>               GVec;
    typedef typename FEqns::FVec    FVec;
    typedef typename FEqns::FMtx    FMtx;

    typedef SVector<DIM>            FSubVec;
    typedef SMatrix<DIM>            FSubMtx;

    typedef CFlowCell<DIM,EQSIZE,AUXSIZE>   CFCell;
    typedef CFlowBFace<DIM,EQSIZE,AUXSIZE>  CFBFace;


    template <MGSize D>
    class GetV
    {
    public:
        MGFloat operator () ( const FVec& var) const
        {
            const FVec varV = EqnRansSA<DIM>::ConservToSimp( var);
            return varV( D);
        }
    };


public:
    virtual void    Init( const Physics<DIM,EQN_RANS_SA>& physics);


protected:

    void    CalcTensor( FSubMtx& mtx, const CFCell& fcell) const;
    void    CalcGradVT( GVec& vec, const CFCell& fcell) const;
    void    CalcGradRho( GVec& vec, const CFCell& fcell) const;
    void    CalcMagnOmega( MGFloat& s, const CFCell& fcell) const;

protected:
    MGFloat     mRe;
    MGFloat     mPr;
    FVec        mRefVar;

    FVec        mvarVm;
};


template <Dimension DIM>
void RANSSATurbulenceSchemeBase_verA<DIM>::Init( const Physics<DIM,EQN_RANS_SA>& physics)
{
    mRe = physics.cRe();
    mPr = physics.cPr();
	physics.DimToUndim( mRefVar, physics.cRefVar(), physics.cRefVar() );
    //mRefVar = physics.cRefVar();
}



template <Dimension DIM>
inline void RANSSATurbulenceSchemeBase_verA<DIM>::CalcGradRho( GVec& vec, const CFCell& fcell) const
{
    vec = fcell.GradVar( GetV< FEqns::ID_RHO >() );
}

template <Dimension DIM>
inline void RANSSATurbulenceSchemeBase_verA<DIM>::CalcGradVT( GVec& vec, const CFCell& fcell) const
{
	vec = fcell.GradVar( GetV< FEqns::ID_K >() );

	//   GVec grad_ro = fcell.GradVar( GetV< FEqns::ID_RHO >() );
 //   GVec grad_rovt  = fcell.GradVar( GetV< FEqns::ID_K >() );
	//vec = ( grad_rovt - mvarVm(FEqns::ID_K) * grad_ro )/  mvarVm(FEqns::ID_RHO);
}

template <>
inline void RANSSATurbulenceSchemeBase_verA<DIM_2D>::CalcMagnOmega( MGFloat& s, const CFCell& fcell) const
{
    GVec gradx = fcell.GradVar( GetV< FEqns::U<0>::ID >() );
    GVec grady = fcell.GradVar( GetV< FEqns::U<1>::ID >() );

    s = ( gradx.cY() - grady.cX() );
    s = sqrt( s*s );
}

template <>
inline void RANSSATurbulenceSchemeBase_verA<DIM_3D>::CalcMagnOmega( MGFloat& s, const CFCell& fcell) const
{
    GVec gradx = fcell.GradVar( GetV< FEqns::U<0>::ID >() );
    GVec grady = fcell.GradVar( GetV< FEqns::U<1>::ID >() );
    GVec gradz = fcell.GradVar( GetV< FEqns::U<2>::ID >() );

    MGFloat s1 = ( grady.cZ() - gradz.cY() );
    MGFloat s2 = ( gradx.cZ() - gradz.cX() );
    MGFloat s3 = ( gradx.cY() - grady.cX() );

	s = sqrt( s1*s1 + s2*s2 + s3*s3 );
}







//////////////////////////////////////////////////////////////////////
// class RANSSATurbulenceScheme
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class SCRDS, class TCommonBase>
class RANSSATurbulenceScheme_verA : public TCommonBase //RANSSATurbulenceSchemeBase_MOD<DIM>
{
    typedef EquationDef<DIM,EQN_RANS_SA> FEqns;
    typedef FlowFunc<DIM, EQN_RANS_SA, FEqns::SplitingFlowTurb::template Block<2>::VBSIZE > TBase;
    typedef typename TBase::BVec    BVec;
    typedef typename TBase::BMtx    BMtx;


    enum { EQSIZE = FEqns::SIZE };
    enum { AUXSIZE = FEqns::AUXSIZE };

    typedef Vect<DIM>               GVec;
    typedef typename FEqns::FVec    FVec;
    typedef typename FEqns::FMtx    FMtx;

    typedef SVector<DIM>            FSubVec;
    typedef SMatrix<DIM>            FSubMtx;

    typedef CFlowCell<DIM,EQSIZE,AUXSIZE>   CFCell;
    typedef CFlowBFace<DIM,EQSIZE,AUXSIZE>  CFBFace;


public:

    virtual void    CalcResiduum( const CFCell& cell, BVec tabres[] );
    virtual void    CalcResJacobian( const CFCell& cell, BVec tabres[], BMtx mtxres[DIM+1][DIM+1] );
};



template <Dimension DIM, class SCRDS, class TCommonBase>
inline void RANSSATurbulenceScheme_verA<DIM,SCRDS,TCommonBase>::CalcResiduum( const CFCell& fcell, BVec tabres[])     
{
	const MGFloat Ret = this->mRe;

    const MGFloat csigma = 2. / 3.;
    const MGFloat cb1 = 0.1355;
    const MGFloat cb2 = 0.622;
    const MGFloat ck  = 0.41;
    const MGFloat cv1 = 7.1;
    const MGFloat cv2 = 0.7;	//2.0 
    const MGFloat cv3 = 0.9;
    const MGFloat cw1 = cb1 / (ck*ck) + (1 + cb2) / csigma;
    const MGFloat cw2 = 0.3;
    const MGFloat cw3 = 2.;

    //for ( MGSize i=0; i<fcell.Size(); ++i)
    //  tabres[i].Init( 1.0e-16);
    //return;


    MGFloat tabK[DIM+1], tabU[DIM+1], tabFi[DIM+1];

    this->mvarVm.Init( 0.0);
    for ( MGSize i=0; i<=DIM; ++i)
        this->mvarVm += EqnRansSA<DIM>::ConservToSimp( fcell.cVar(i) );

    this->mvarVm /= (MGFloat)(DIM+1);

	//mvarZm(FEqns::ID_K)   = max( 0.0, mvarZm(FEqns::ID_K) );
	this->mvarVm(FEqns::ID_RHO) = max( 1.0e-10, this->mvarVm(FEqns::ID_RHO) );
	this->mvarVm(FEqns::ID_P)   = max( 1.0e-10, this->mvarVm(FEqns::ID_P) );

    FVec varQm = EqnRansSA<DIM>::SimpToConserv( this->mvarVm);

    MGFloat dist = 0.0;
    for ( MGSize i=0; i<=DIM; ++i)
        dist += fcell.cAux(i)(0);
    dist /= (MGFloat)(DIM+1);

    //dist = max( dist, 1.0e-12 );
    dist = max( dist, 1.0e-10 );

    const MGFloat volume = fcell.Volume();

	//if ( volume < 0.0 )
	//	THROW_INTERNAL( "negative volume")

    ////////////////////////////////////
    // calc mean molecular mu
    MGFloat c = EqnEulerComm<DIM>::C( varQm);
    MGFloat cinf = EqnEulerComm<DIM>::C( this->mRefVar);
    MGFloat c2 = c*c;
    MGFloat cinf2 = cinf*cinf;

    MGFloat temp = c2 / cinf2;
    MGFloat tempinf = 295.0;
    MGFloat sconst = 110.33;

    MGFloat mu = 1.0;
    mu = sqrt( temp*temp*temp) * ( 1.0 + sconst/tempinf) / (temp + sconst/tempinf);
	mu /= varQm(FEqns::ID_RHO);
	//mu = pow( temp, 0.7 );

    ////////////////////////////////////
    // diffusion
    GVec    vgradVT, vgradRho;

	this->CalcGradVT( vgradVT, fcell);
	this->CalcGradRho( vgradRho, fcell);

    MGFloat vgradVT2 = vgradVT * vgradVT;


    //MGFloat summu = mvarZm(FEqns::ID_RHO) * mu + mvarZm(FEqns::ID_K);
    //MGFloat summu = mvarZm(FEqns::ID_RHO) * mu * Ret / mRe + max( 0.0, mvarZm(FEqns::ID_K) );
    MGFloat summu =  mu * Ret / this->mRe + this->mvarVm(FEqns::ID_K);


    MGFloat tabdres[DIM+1];

    for ( MGSize i=0; i<DIM+1; ++i)
    {
        GVec vn = fcell.Vn( i);
        tabdres[i] = summu * this->mvarVm(FEqns::ID_RHO) * vgradVT * vn / ( DIM * csigma * Ret );     // molecular
    }

	MGFloat diffcorr = summu * (vgradRho * vgradVT) / ( csigma * Ret );
    MGFloat turbdiff = cb2 * this->mvarVm(FEqns::ID_RHO) * vgradVT2 / ( csigma * Ret );    // turbulent

    ////////////////////////////////////
    // production

    MGFloat s;

    this->CalcMagnOmega( s, fcell);


	MGFloat chi = this->mvarVm(FEqns::ID_K) / mu ;

	//cout << chi << endl;
	//THROW_INTERNAL( "STOP");

    //chi = max( chi, 1.0e-4);
    chi = max( chi, 1.0e-4);

    const MGFloat fv1 = chi*chi*chi / (chi*chi*chi + cv1*cv1*cv1);
    const MGFloat fv2 = 1. - chi / (1. + fv1*chi);
    const MGFloat fv3 = 1.;
   
	const MGFloat ct3 = 1.2;
	const MGFloat ct4 = 0.5;
	const MGFloat ft2 = ct3 * exp( -ct4 * chi*chi );

    //const MGFloat fv2 = pow( 1. + chi / cv2, -3.);
    //const MGFloat fv3 = (1. + chi * fv1) * ( 1 - fv2) / chi;

    const MGFloat nik2d2 = max( 0.0, this->mvarVm(FEqns::ID_K) ) / ( ck * ck * dist * dist);

	//
    //MGFloat stilde = s * fv3  +  nik2d2 * fv2 / Ret;
	//stilde = max( stilde, 0.3*s);
	//
	MGFloat sbar = nik2d2 * fv2 / Ret;

	MGFloat stilde;
	if ( sbar >= -cv2*s )
		stilde = s + sbar;
	else
		stilde = s + s* ( cv2*cv2*s + cv3*sbar) / ((cv3 -2.*cv2)*s-sbar);
	//


	MGFloat product = cb1 * ( 1.0 - ft2 ) * stilde * max( 0.0, this->mvarVm(FEqns::ID_K) ) * this->mvarVm(FEqns::ID_RHO);
	//MGFloat product = cb1 *  stilde * mvarZm(FEqns::ID_K) * mvarZm(FEqns::ID_RHO);

    ////////////////////////////////////
    // destruction

    MGFloat cr = nik2d2 / stilde / Ret;
	cr = min( cr, 10.0 );

    const MGFloat cg = cr + cw2 * ( pow(cr,6) - cr);
    const MGFloat fw = cg * pow( (1. + pow(cw3,6))/(pow(cg,6) + pow(cw3,6)), 1./6.);

    MGFloat nid = this->mvarVm(FEqns::ID_K) / dist;
    //MGFloat destruct = ( cw1 * fw - cb1/(ck*ck) * ft2 ) * this->mvarVm(FEqns::ID_RHO) * nid*fabs(nid) / Ret;	// <--- negative eq ???
    MGFloat destruct = ( cw1 * fw - cb1/(ck*ck) * ft2 ) * this->mvarVm(FEqns::ID_RHO) * nid*nid / Ret;	// <--- negative eq ???
	if ( this->mvarVm(FEqns::ID_K) < 0.0)
	{
		//cout << "mvarZm(FEqns::ID_K) is negative" << endl;
		destruct = - cw1  * nid*nid / Ret;
	}
    //MGFloat destruct =  cw1 * fw * nid*nid / Ret;

    ////////////////////////////////////
    // convection
    GVec    vu;

    for ( MGSize i=0; i<DIM; ++i)
        vu.rX(i) = this->mvarVm[i+2];

    for ( MGSize i=0; i<=DIM; ++i)
    {
        GVec vn = fcell.Vn(i);
        tabK[i] = vn * vu / (MGFloat)DIM;
    }


    for ( MGSize i=0; i<=DIM; ++i)
        tabU[i] = fcell.cVar(i)[ FEqns::ID_K ];

    SCRDS::template Calc<MGFloat>( tabK, tabU, tabFi);

    for ( MGSize i=0; i<fcell.Size(); ++i)
        tabres[i](0) = tabFi[i];

 	
    ////////////////////////////////////
	// don't calc visc fluxes if outlet
	if ( fcell.IsOutlet() )
		return;

	for ( MGSize i=0; i<fcell.Size(); ++i)
	{
		tabres[i](0) += tabdres[i];
		tabres[i](0) += diffcorr * volume / MGFloat( DIM + 1 );

		tabres[i](0) -= turbdiff * volume / MGFloat( DIM + 1 );

		//tabres[i](0) -= product  * volume / MGFloat( DIM + 1 );
		//tabres[i](0) += destruct * volume / MGFloat( DIM + 1 );
	}

	if ( ! fcell.IsInviscWall() && ! fcell.IsViscWall() )
	{
		for ( MGSize i=0; i<fcell.Size(); ++i)
		{
			tabres[i](0) -= product  * volume / MGFloat( DIM + 1 );
			tabres[i](0) += destruct * volume / MGFloat( DIM + 1 );
		}
	}

}


template <Dimension DIM, class SCRDS, class TCommonBase>
inline void RANSSATurbulenceScheme_verA<DIM,SCRDS,TCommonBase>::CalcResJacobian( const CFCell& fcell, BVec tabres[], BMtx mtxres[DIM+1][DIM+1])       
{
    THROW_INTERNAL("Not implemented !!!");
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RANS0GALERKINSCHEME_VERA_H__
 
