
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


void init_FlowFuncRansSAFlowRDS();
void init_FlowFuncRansSATurbulenceRDS();
void init_IntegratorRansSAFlowRDS();
void init_SurfAnalyzerRansSAFlowRDS();

void init_FlowFuncRANSKOMEGAFullRDSSMtx();


void RegisterLib_REDVCRDSRANS()
{
	init_FlowFuncRansSAFlowRDS();
	init_FlowFuncRansSATurbulenceRDS();
	init_IntegratorRansSAFlowRDS();
	init_SurfAnalyzerRansSAFlowRDS();
	
	init_FlowFuncRANSKOMEGAFullRDSSMtx();
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

