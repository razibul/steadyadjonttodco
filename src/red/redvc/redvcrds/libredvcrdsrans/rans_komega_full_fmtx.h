#ifndef __RANSKOMEGAFULLFMTX_H__
#define __RANSKOMEGAFULLFMTX_H__


#include "redvc/redvcrds/libredvcrdsrans/rans_komega_full_common.h"
#include "redvc/redvcrds/libredvcrdsns/ns_galerkinscheme.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class RANSKOMEGA_RDSchemeSMtx
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class MTXRDS, class SCRDS>
class RANSKOMEGA_RDSchemeFMtx : public RANSKOMEGACommon<DIM>, public FlowFunc<DIM, EQN_RANS_KOMEGA, EquationDef<DIM,EQN_RANS_KOMEGA>::SIZE>
{
	typedef EquationDef<DIM,EQN_RANS_KOMEGA>	ET;

	enum { EQSIZE = ET::SIZE };
	enum { AUXSIZE = ET::AUXSIZE };

	typedef typename RANSKOMEGACommon<DIM>::GVec		GVec;
	typedef typename RANSKOMEGACommon<DIM>::FVec		FVec;
	typedef typename RANSKOMEGACommon<DIM>::FMtx		FMtx;

	typedef typename RANSKOMEGACommon<DIM>::FViscVec	FViscVec;
	typedef typename RANSKOMEGACommon<DIM>::FViscMtx	FViscMtx;
	typedef typename RANSKOMEGACommon<DIM>::FGradVec	FGradVec;

	typedef SVector<DIM+2>		FSubVec;
	typedef SMatrix<DIM+2>		FSubMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>	CFCell;

public:
	RANSKOMEGA_RDSchemeFMtx()	{}

	virtual void	Init( const Physics<DIM,EQN_RANS_KOMEGA>& physics);

	virtual void	CalcResiduum( const CFCell& fcell, FVec tabres[]);
	virtual void	CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);


protected:
	void	Initialize( const CFCell& fcell);

	void	CalcConvectRes( const CFCell& fcell, FVec tabres[]);
	//void	CalcViscRes( const CFCell& fcell, FVec tabres[]);
	//void	CalcTurbRes( const CFCell& fcell, FVec tabres[]);

	void	CalcSubMtxs( FSubMtx &smtxKp, FSubMtx &smtxKm, const GVec &vn );

private:

	struct MtxRDSCalc : public MTXRDS {}	mtxcalc;
	struct ScRDSCalc : public SCRDS {}	sccalc;

};
//////////////////////////////////////////////////////////////////////



template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RANSKOMEGA_RDSchemeFMtx<DIM,MTXRDS,SCRDS>::CalcSubMtxs( FSubMtx &smtxKp, FSubMtx &smtxKm, const GVec &vn )
{

	MGFloat c = ::sqrt( FLOW_K * this->mvarVm(ET::ID_P) / this->mvarVm(ET::ID_RHO) );
	MGFloat u2 = EqnRansKOMEGA<DIM>::SqrU( this->mvarVm );

	MGFloat q = 0.0;
	for ( MGSize k=0; k<DIM; ++k)
		q += this->mvarVm(ET::template U<0>::ID + k) * vn.cX(k);

	MGFloat lam[3], lamP[3], lamM[3];

	lam[0] = q;
	lam[1] = q + c;
	lam[2] = q - c;


	for ( MGInt i=0; i<3; ++i)
	{

		MGFloat ls = ::fabs( lam[i]);
		//MGFloat e = 0.0;
		//if ( ls < e )
		//{
		//	//ls = (ls*ls/e + e);
		//	ls = e;
		//}

		lamP[i] = 0.5*( lam[i] + ls );
		lamM[i] = 0.5*( lam[i] - ls );
	}

	EqnRansKOMEGA<DIM>::AssembleSubMtxSAV( smtxKp, this->mvarVm, vn, lamP[0], lamP[1], lamP[2] );
	EqnRansKOMEGA<DIM>::AssembleSubMtxSAV( smtxKm, this->mvarVm, vn, lamM[0], lamM[1], lamM[2] );
}


template <Dimension DIM, class MTXRDS, class SCRDS>
void RANSKOMEGA_RDSchemeFMtx<DIM,MTXRDS,SCRDS>::Init( const Physics<DIM,EQN_RANS_KOMEGA>& physics)
{
	RANSKOMEGACommon<DIM>::InitPhysics( physics);
}



template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RANSKOMEGA_RDSchemeFMtx<DIM,MTXRDS,SCRDS>::Initialize( const CFCell& fcell)
{
	RANSKOMEGACommon<DIM>::Initialize( fcell);

//	//----------------
//	FVec var;
//	var(0) = 1.6;
//	var(1) = 2.2;
//	var(2) = 0.6;
//	var(3) = 0.2;
//	var(4) = 0.35;
//	var(5) = 0.22;
//
//	FMtx smtx;
//	EqnRansKOMEGA<DIM>::CJacobVZ( smtx, var);
//	smtx.Write();
//	
//	EqnRansKOMEGA<DIM>::CJacobQV( smtx, var);
//	smtx.Write();
//
////evalf(subs(nx=1/sqrt(5),ny=2/sqrt(5),nz=0,rho=1.6,p=2.2,u=0.6,v=0.2,gamma=1.4,w=0,k=0.35,omega=0.22,subs(c=sqrt(gamma*p/rho), l1=3,l2=2.5,l3=1,
//	GVec vn = GVec(1,2).versor();
//	EqnRansKOMEGA<DIM>::AssembleSubMtxSAV( smtx, var, vn, 3, 2.5, 1 );
//	smtx.Write();
//
//	THROW_INTERNAL("Not implemented !!!");
//
//	FMtx	smtxM_VZ;
//	EqnRansKOMEGA<DIM>::CJacobVZ( smtxM_VZ, this->mvarVm);
//	//----------------

	FMtx	smtxM_VZ;
	EqnRansKOMEGA<DIM>::CJacobVZ( smtxM_VZ, this->mvarVm);

	for ( MGSize i=0; i<=DIM; ++i)
		//EqnRansKOMEGA<DIM>::MultVZ( this->mtabsW[i], smtxM_WZ, tabsV[i] );
		this->mtabsW[i] = smtxM_VZ * this->mtabsZ[i];
}




template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RANSKOMEGA_RDSchemeFMtx<DIM,MTXRDS,SCRDS>::CalcConvectRes( const CFCell& fcell, FVec tabres[])
{
	MGFloat	tabKcoeff[DIM+1];

	MGFloat	tabK[DIM+1], tabO[DIM+1];
	MGFloat	tabFiK[DIM+1], tabFiO[DIM+1];
	FSubMtx	tabKp[DIM+1];
	FSubMtx	tabKm[DIM+1];


	GVec    vu;
	for ( MGSize i=0; i<DIM; ++i)
		vu.rX(i) = this->mvarVm(i + ET::template U<0>::ID);


	for ( MGSize i=0; i<=DIM; ++i)
	{
		FSubMtx	smtxAp, smtxAm;

		GVec vn = fcell.Vn(i).versor();
		MGFloat	vnmod = fcell.Vn(i).module() / (MGFloat)DIM;

		// matrix part
		CalcSubMtxs( smtxAp, smtxAm, vn );

		MtxMultMtxD( tabKp[i], smtxAp, vnmod);
		MtxMultMtxD( tabKm[i], smtxAm, vnmod);

		// scalar part
		tabKcoeff[i] = fcell.Vn(i) * vu / (MGFloat)DIM;
	}

	FSubVec	tabFi[DIM+1];
	FSubVec	tabU[DIM+1];

	for ( MGSize i=0; i<=DIM; ++i)
	{
		for ( MGSize k=0; k<DIM+2; ++k)
			tabU[i](k) = this->mtabsW[i](k);

		tabK[i]	= this->mtabsW[i]( ET::ID_K );
		tabO[i]	= this->mtabsW[i]( ET::ID_OMEGA );
	}

	mtxcalc.Calc( DIM+2, tabKp, tabKm, tabU, tabFi);

	sccalc.Calc( tabKcoeff, tabK, tabFiK);
	sccalc.Calc( tabKcoeff, tabO, tabFiO);

	FVec	tabFifull[DIM+1];

	for ( MGSize i=0; i<=DIM; ++i)
	{
		for ( MGSize k=0; k<DIM+2; ++k)
			tabFifull[i](k) = tabFi[i](k);

		tabFifull[i]( ET::ID_K )		= tabFiK[i];
		tabFifull[i]( ET::ID_OMEGA )	= tabFiO[i];
	}


	FMtx	smtxM_QV;
	EqnRansKOMEGA<DIM>::CJacobQV( smtxM_QV, this->mvarVm);

	for ( MGSize i=0; i<=DIM; ++i)
		//EqnRansKOMEGA<DIM>::MultQW( tabres[i], smtxM_QW, tabFifull[i] );
		tabres[i] = smtxM_QV * tabFifull[i];
}




template <Dimension DIM, class MTXRDS, class SCRDS>
void RANSKOMEGA_RDSchemeFMtx<DIM,MTXRDS,SCRDS>::CalcResiduum( const CFCell& fcell, FVec tabres[])
{
	Initialize( fcell);

	CalcConvectRes( fcell, tabres);

	// don't calc visc fluxes if outlet
	if ( ! fcell.IsOutlet() )
	{
		this->CalcViscRes( fcell, tabres);
		this->CalcTurbRes( fcell, tabres);
	}
}


template <Dimension DIM, class MTXRDS, class SCRDS>
void RANSKOMEGA_RDSchemeFMtx<DIM,MTXRDS,SCRDS>::CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1])
{
	THROW_INTERNAL("Not implemented !!!");
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RANSKOMEGAFULLFMTX_H__
