#include "rans_turbulence_scheme.h"
#include "libcorecommon/factory.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//template class RANSGalerkinScheme<DIM_2D>;


void init_FlowFuncRANS2eqTurbulenceRDS()
{
	static ConcCreator< 
		MGString, 
		RANSTurbulenceScheme<DIM_2D>,
		FlowFunc<Geom::DIM_2D, EQN_RANS_2E, EquationDef<Geom::DIM_2D,EQN_RANS_2E>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRANS2eqTurbRDSLDA2D( "FLOWFUNC_2D_RANS_2E_RDS_MTX_LDA");
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

