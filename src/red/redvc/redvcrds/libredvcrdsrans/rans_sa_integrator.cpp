#include "rans_sa_integrator.h"
#include "libcorecommon/factory.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




template <>
inline void RANSSAIntegrator<DIM_2D>::CalcTensor( FSubMtx& mtx, const CFCell& fcell) const
{
	FVec varZm;
	varZm.Init( 0.0);
	for ( MGSize i=0; i<=DIM_2D; ++i)
		varZm += EqnRansSA<DIM_2D>::ConservToZ( fcell.cVar(i) );

	varZm /= (MGFloat)(DIM_2D+1);

	GVec grad_zro = fcell.GradVar( GetZ< EqDef::ID_RHO >() );
	GVec grad_zv0 = fcell.GradVar( GetZ< EqDef::U<0>::ID >() );
	GVec grad_zv1 = fcell.GradVar( GetZ< EqDef::U<1>::ID >() );


	GVec gradx = 1.0 / ( varZm(EqDef::ID_RHO) * varZm(EqDef::ID_RHO) ) * ( varZm(EqDef::ID_RHO) * grad_zv0 - varZm(EqDef::U<0>::ID) * grad_zro );
	GVec grady = 1.0 / ( varZm(EqDef::ID_RHO) * varZm(EqDef::ID_RHO) ) * ( varZm(EqDef::ID_RHO) * grad_zv1 - varZm(EqDef::U<1>::ID) * grad_zro );

	//GVec gradx = fcell.GradVar( GetVelocity<0>() );
	//GVec grady = fcell.GradVar( GetVelocity<1>() );

	MGFloat trace = gradx.cX() + grady.cY();

	mtx(0,0) = 2*( gradx.cX() - trace/3.0 );
	mtx(1,1) = 2*( grady.cY() - trace/3.0 );

	mtx(1,0) = mtx(0,1) = gradx.cY() + grady.cX();
}

template <>
inline void RANSSAIntegrator<DIM_3D>::CalcTensor( FSubMtx& mtx, const CFCell& fcell) const
{
	FVec varZm;
	varZm.Init( 0.0);
	for ( MGSize i=0; i<=DIM_2D; ++i)
		varZm += EqnRansSA<DIM_2D>::ConservToZ( fcell.cVar(i) );

	varZm /= (MGFloat)(DIM_2D+1);

	GVec grad_zro = fcell.GradVar( GetZ< EqDef::ID_RHO >() );
	GVec grad_zv0 = fcell.GradVar( GetZ< EqDef::U<0>::ID >() );
	GVec grad_zv1 = fcell.GradVar( GetZ< EqDef::U<1>::ID >() );
	GVec grad_zv2 = fcell.GradVar( GetZ< EqDef::U<2>::ID >() );

	GVec gradx = 1.0 / ( varZm(EqDef::ID_RHO) * varZm(EqDef::ID_RHO) ) * ( varZm(EqDef::ID_RHO) * grad_zv0 - varZm(EqDef::U<0>::ID) * grad_zro );
	GVec grady = 1.0 / ( varZm(EqDef::ID_RHO) * varZm(EqDef::ID_RHO) ) * ( varZm(EqDef::ID_RHO) * grad_zv1 - varZm(EqDef::U<1>::ID) * grad_zro );
	GVec gradz = 1.0 / ( varZm(EqDef::ID_RHO) * varZm(EqDef::ID_RHO) ) * ( varZm(EqDef::ID_RHO) * grad_zv2 - varZm(EqDef::U<2>::ID) * grad_zro );

	//GVec gradx = fcell.GradVar( GetVelocity<0>() );
	//GVec grady = fcell.GradVar( GetVelocity<1>() );
	//GVec gradz = fcell.GradVar( GetVelocity<2>() );


	MGFloat trace = gradx.cX() + grady.cY() + gradz.cZ();

	mtx(0,0) = 2*( gradx.cX() - trace/3.0 );
	mtx(1,1) = 2*( grady.cY() - trace/3.0 );
	mtx(2,2) = 2*( gradz.cZ() - trace/3.0 );

	mtx(1,0) = mtx(0,1) = gradx.cY() + grady.cX();
	mtx(2,0) = mtx(0,2) = gradx.cZ() + gradz.cX();
	mtx(1,2) = mtx(2,1) = grady.cZ() + gradz.cY();
}



template <Dimension DIM>
bool RANSSAIntegrator<DIM>::Do()
{
	cout << " ----- echo from -- RANSSAIntegrator<DIM>::Do -----" << endl;

	FVec vref = EqnRansSA<DIM>::ConservToSimp( this->cPhysics().cDefVar( 0) );

	MGFloat u2 = EqnEuler<DIM>::SqrU( vref);
	MGFloat q = 0.5*u2*vref( EqDef::ID_RHO );

	GVec force_press;
	GVec force_friction;

	//FVec		refVar;
	//this->cPhysics().DimToUndim( refVar, this->cPhysics().cRefVar(), this->cPhysics().cRefVar() );



	//for ( MGSize i=0; i<this->cData().cGrid().SizeCellTab(); ++i)
	//{
	//	CFCell	cfcell;
	//	this->GetFCell( cfcell, i);

	//	FVec varQm;// = EqnRansSA<DIM>::ZToConserv( this->mvarZm);

	//	MGFloat c = EqnEulerComm<DIM>::C( varQm);
	//	MGFloat cinf = EqnEulerComm<DIM>::C( this->mRefVar);
	//	MGFloat c2 = c*c;
	//	MGFloat	cinf2 = cinf*cinf;

	//	MGFloat temp = c2 / cinf2;
	//	MGFloat tempinf = 295.0;
	//	MGFloat sconst = 110.33;

	//	MGFloat mu = sqrt( temp*temp*temp) * ( 1.0 + sconst/tempinf) / (temp + sconst/tempinf);
	//	//MGFloat mu = 1.0;

	//	FSubMtx mtxT;
	//	CalcTensor( mtxT, cfcell);

	//}

	for ( MGSize i=0; i<this->cData().cGrid().SizeBFaceTab(); ++i)
	{
		CFBFace	cfbface;
		this->GetFBFace( cfbface, i);


		if ( this->cPhysics().cBC( cfbface.cSurfId() ) ==  BC_VISCWALL )
		{
			MGFloat area = cfbface.Area();
			GVec vn = cfbface.Vn();

			FVec var;
			var.Init( 0.0);

			for ( MGSize i=0; i<cfbface.Size(); ++i)
				var += EqnRansSA<DIM>::ConservToSimp( cfbface.cVar(i) );

			MGFloat p = var( EqDef::ID_P ) / MGFloat( cfbface.Size() );

			force_press -= p * vn;

			///////
			CFCell	cfcell;
			this->GetFCell( cfcell, cfbface.cCellId() );
			//cout << cfbface.cCellId() << endl;

			MGFloat mu = 1.0 / this->cPhysics().cRe();

			FSubMtx mtxT;
			CalcTensor( mtxT, cfcell);

			SVector<DIM> svn, svec;

			for ( MGSize idim=0; idim<DIM; ++idim)
				svn[idim] = vn.cX(idim);

			svec = mtxT * svn;

			GVec fn, ft, f;
			for ( MGSize idim=0; idim<DIM; ++idim)
				ft.rX(idim) += mu * svec[idim];

			fn = (ft * vn) * vn.versor();
			f = ft - fn;

			force_friction += f;
		}

	}

	force_press /= q;
	force_friction /= q;

	force_press.Write();
	force_friction.Write();

	return true;
}


void init_IntegratorRansSAFlowRDS()
{
	static ConcCreator< 
		MGString, 
		RANSSAIntegrator<DIM_2D>,
		Integrator<Geom::DIM_2D, EQN_RANS_SA> 
	>	gCreatorIntegratorRansSA_2D( "INTEGRATOR_2D_RANS_SA");

	static ConcCreator< 
		MGString, 
		RANSSAIntegrator<DIM_3D>,
		Integrator<Geom::DIM_3D, EQN_RANS_SA> 
	>	gCreatorIntegratorRansSA_3D( "INTEGRATOR_3D_RANS_SA");

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

