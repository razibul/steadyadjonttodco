#include "rans_komega_full_smtx.h"
#include "rans_komega_full_fmtx.h"

#include "redvc/redvcrds/libredvcrdscommon/mtxldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/mtxnscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/mtxldanscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/scldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/scnscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/scpsischeme.h"
#include "libcorecommon/factory.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//template class RANSGalerkinScheme<DIM_2D>;


void init_FlowFuncRANSKOMEGAFullRDSSMtx()
{
	static ConcCreator< 
		MGString, 
		RANSKOMEGA_RDSchemeFMtx< DIM_2D, MtxLDAscheme<Geom::DIM_2D>, ScLDAscheme<Geom::DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_KOMEGA, EquationDef<Geom::DIM_2D,EQN_RANS_KOMEGA>::SIZE > 
	>	gCreatorFlowFuncRansKOMEGARdsLDA2D( "FLOWFUNC_2D_RANS_KOMEGA_RDS_MTX_LDA");





	static ConcCreator< 
		MGString, 
		RANSKOMEGA_RDSchemeSMtx< DIM_2D, MtxLDAscheme<Geom::DIM_2D>, ScLDAscheme<Geom::DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_KOMEGA, EquationDef<Geom::DIM_2D,EQN_RANS_KOMEGA>::SIZE > 
	>	gCreatorFlowFuncRansKOMEGARdsLDALDA2D( "FLOWFUNC_2D_RANS_KOMEGA_RDS_SM_LDA_LDA");

	static ConcCreator< 
		MGString, 
		RANSKOMEGA_RDSchemeSMtx< DIM_2D, MtxLDAscheme<Geom::DIM_2D>, ScPSIscheme<Geom::DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_KOMEGA, EquationDef<Geom::DIM_2D,EQN_RANS_KOMEGA>::SIZE > 
	>	gCreatorFlowFuncRansKOMEGARdsLDAPSI2D( "FLOWFUNC_2D_RANS_KOMEGA_RDS_SM_LDA_PSI");

	static ConcCreator< 
		MGString, 
		RANSKOMEGA_RDSchemeSMtx< DIM_2D, MtxLDAscheme<Geom::DIM_2D>, ScNscheme<Geom::DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_KOMEGA, EquationDef<Geom::DIM_2D,EQN_RANS_KOMEGA>::SIZE > 
	>	gCreatorFlowFuncRansKOMEGARdsLDAN2D( "FLOWFUNC_2D_RANS_KOMEGA_RDS_SM_LDA_N");




	static ConcCreator< 
		MGString, 
		RANSKOMEGA_RDSchemeFMtx< DIM_3D, MtxLDAscheme<Geom::DIM_3D>, ScLDAscheme<Geom::DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_KOMEGA, EquationDef<Geom::DIM_3D,EQN_RANS_KOMEGA>::SIZE > 
	>	gCreatorFlowFuncRansKOMEGARdsLDA3D( "FLOWFUNC_3D_RANS_KOMEGA_RDS_MTX_LDA");


	static ConcCreator< 
		MGString, 
		RANSKOMEGA_RDSchemeSMtx< DIM_3D, MtxLDAscheme<Geom::DIM_3D>, ScLDAscheme<Geom::DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_KOMEGA, EquationDef<Geom::DIM_3D,EQN_RANS_KOMEGA>::SIZE > 
	>	gCreatorFlowFuncRansKOMEGARdsLDALDA3D( "FLOWFUNC_3D_RANS_KOMEGA_RDS_SM_LDA_LDA");

	static ConcCreator< 
		MGString, 
		RANSKOMEGA_RDSchemeSMtx< DIM_3D, MtxLDAscheme<Geom::DIM_3D>, ScPSIscheme<Geom::DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_KOMEGA, EquationDef<Geom::DIM_3D,EQN_RANS_KOMEGA>::SIZE > 
	>	gCreatorFlowFuncRansKOMEGARdsLDAPSI3D( "FLOWFUNC_3D_RANS_KOMEGA_RDS_SM_LDA_PSI");


}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

