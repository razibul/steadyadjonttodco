#ifndef __RANSKOMEGAFULLCOMMON_H__
#define __RANSKOMEGAFULLCOMMON_H__

#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"
#include "libredphysics/ranskomegaequations.h"
#include "redvc/libredvccommon/flowfunc.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



namespace TurbulenceSST
{
	const MGFloat beta_star = 0.09;
	const MGFloat kappa = 0.41;
	const MGFloat a1 = 0.3;

	const MGFloat sigma_k1 = 0.85;
	const MGFloat sigma_omega1 = 0.5;
	const MGFloat beta1 = 0.0750;
	const MGFloat alpha1 = 5. / 9.;

	const MGFloat sigma_k2 = 1.0;
	const MGFloat sigma_omega2 = 0.856;
	const MGFloat beta2 = 0.0828;
	const MGFloat alpha2 = 0.44;
}

namespace SST = TurbulenceSST;


template <Dimension DIM>
class FEMWeights
{
};

template <>
class FEMWeights<DIM_2D>
{
public:
	static const MGFloat Quadratic( const MGSize& i, const MGSize& j )					{ return (i==j ? 1./6. : 1./12.); }
	static const MGFloat Cubic( const MGSize& i, const MGSize& j, const MGSize& k )		{ return 0; }
};

template <>
class FEMWeights<DIM_3D>
{
public:
	static const MGFloat Quadratic( const MGSize& i, const MGSize& j )					{ return 0; }
	static const MGFloat Cubic( const MGSize& i, const MGSize& j, const MGSize& k )		{ return 0; }
};



//////////////////////////////////////////////////////////////////////
// class RANSKOMEGACommon
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class RANSKOMEGACommon
{
public:
	typedef EquationDef<DIM,EQN_RANS_KOMEGA>	ET;

	enum { EQSIZE = ET::SIZE };
	enum { AUXSIZE = ET::AUXSIZE };

	typedef Vect<DIM>			GVec;
	typedef typename ET::FVec	FVec;
	typedef typename ET::FMtx	FMtx;

	typedef SVector<DIM>		FViscVec;
	typedef SMatrix<DIM>		FViscMtx;

	typedef SVector< EQSIZE, GVec >		FGradVec;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>	CFCell;

public:
	RANSKOMEGACommon()	{}


	template <MGSize D>
	class GetZ
	{
	public:
		MGFloat operator () ( const FVec& var) const
		{
			const FVec varZ = EqnRansKOMEGA<DIM>::ConservToZ( var);
			return varZ( D);
		}
	};

	template <MGSize D>
	class GetVelocity
	{
	public:
		MGFloat operator () ( const FVec& var) const
		{
			return var( ET::template U<D>::ID) / var( ET::ID_RHO);
		}
	};

	class GetC2
	{
	public:
		MGFloat operator () ( const FVec& var) const
		{
			return EqnRansKOMEGA<DIM>::C2( var);
		}
	};

	MGFloat	MolecularMU( const FVec& varz)
	{
		MGFloat c2 = EqnRansKOMEGA<DIM>::C2( EqnRansKOMEGA<DIM>::ZToConserv( varz) );
		MGFloat cinf2 = EqnRansKOMEGA<DIM>::C2( this->mRefVar);

		MGFloat temp = c2 / cinf2;
		MGFloat tempinf = 295.0;
		MGFloat sconst = 110.33;

		MGFloat mu = sqrt( temp*temp*temp) * ( 1.0 + sconst/tempinf) / (temp + sconst/tempinf);
		return mu;
	}


	//MGFloat	TurbulentMU( const FVec& varz)
	//{
	//	return max( 0., varz(ET::ID_K) ) * varz(ET::ID_RHO) * exp( - varz(ET::ID_OMEGA) / varz(ET::ID_RHO) );
	//}

	MGFloat	Omega( const FVec& varz)
	{
		MGFloat tomega = varz(ET::ID_OMEGA) / varz(ET::ID_RHO);
		MGFloat omega = exp(tomega);
		//omega = max( mOmega0, tomega );
		omega = max( mOmegaAmb, omega );
		return omega;
	}

	MGFloat	OmegaMu( const FVec& varz, const MGFloat& dist)
	{
		MGFloat omega_mu = Omega( varz);
		return max( omega_mu, mVort * BlendingF2(varz,dist) / SST::a1 ); // SST
	}

	MGFloat	TurbulentMU_SST( const FVec& varz, const MGFloat& y)
	{
		const MGFloat omega_mu = OmegaMu( varz, y);
		return max( 0., varz(ET::ID_K) ) * varz(ET::ID_RHO) / omega_mu;
	}

	MGFloat	BlendingF1( const FVec& varz, const MGFloat& dist)
	{
		MGFloat y = max( dist ,1.0e-12);

		MGFloat k = max( 0., varz(ET::ID_K) / (varz(ET::ID_RHO) /** mRe*/) );
		MGFloat omega = Omega( varz);
		MGFloat rho = varz(ET::ID_RHO)*varz(ET::ID_RHO);

		MGFloat arg1 = sqrt(k) / ( SST::beta_star*omega*y);
		MGFloat arg2 = 500.0 / (mRe*y*y*omega);
		MGFloat arg3 = 4.*SST::sigma_omega2*rho*k / (mCDko*y*y);

		MGFloat arg = min( max(arg1,arg2), arg3);
		arg = arg*arg*arg*arg;

		return tanh( arg);
	}


	MGFloat	BlendingF2( const FVec& varz, const MGFloat& dist)
	{
		MGFloat y = max( dist ,1.0e-12);

		MGFloat k = max( 0., varz(ET::ID_K)) / varz(ET::ID_RHO)/* / mRe*/;
		MGFloat omega = Omega( varz);
		MGFloat rho = varz(ET::ID_RHO)*varz(ET::ID_RHO);

		MGFloat arg1 = 2.*sqrt(k) / ( SST::beta_star*omega*y);
		MGFloat arg2 = 500.0 / (mRe*y*y*omega);

		MGFloat arg = max(arg1,arg2);
		arg = arg*arg;

		return tanh( arg);
	}


	void	CalcGradZ( FGradVec& gradz, const CFCell& fcell) const;
	void	CalcGradC2( GVec& vec, const CFCell& fcell) const;
	void	CalcGradK( GVec& vec, const CFCell& fcell) const;
	void	CalcGradOmega( GVec& vec, const CFCell& fcell) const;

	void	CalcGradU( FViscMtx& mtx, const CFCell& fcell) const;
	void	CalcSbarOmega( FViscMtx& mtxs, MGFloat& vort2, const FViscMtx& mtxdu) const;



	void	CalcTurbRes_SSTE( const CFCell& fcell, FVec tabres[]);
	//void	CalcTurbRes_SSTEold( const CFCell& fcell, FVec tabres[]);
	//void	CalcTurbRes_EXP( const CFCell& fcell, FVec tabres[]);


	void	InitPhysics( const Physics<DIM,EQN_RANS_KOMEGA>& physics);
	void	Initialize( const CFCell& fcell);

	void	CalcViscRes( const CFCell& fcell, FVec tabres[]);
	void	CalcTurbRes( const CFCell& fcell, FVec tabres[]);

public:
	FVec		mRefVar;

	MGFloat		mRe;
	MGFloat		mPr;
	MGFloat		mPrT;

	MGFloat		mMu;
	MGFloat		mMuT;

	MGFloat		mOmega0;
	MGFloat		mTOmegaAmb;
	MGFloat		mOmegaAmb;

	MGFloat		mReT;

	MGFloat		mCDko;
	MGFloat		mVort;
	MGFloat		mYm;

	FVec		mvarZm;
	FVec		mvarVm;
	FVec		mvarQm;
	FVec		mtabsZ[DIM+1];
	FVec		mtabsW[DIM+1];

	FGradVec	mGradZ;

	GVec		mGradK;
	GVec		mGradO;

	FViscMtx	mmtxDU;
	FViscMtx	mmtxSbar;
};
//////////////////////////////////////////////////////////////////////


template <Dimension DIM>
inline void RANSKOMEGACommon<DIM>::CalcGradZ( FGradVec& gradz, const CFCell& fcell) const
{
	for ( MGSize ie=0; ie<EQSIZE; ++ie)
		gradz(ie) = GVec(0.0);

	for ( MGSize k=0; k<=DIM; ++k)
	{
		FVec var =  EqnRansKOMEGA<DIM>::ConservToZ( fcell.cVar( k ) );
		for ( MGSize ie=0; ie<EQSIZE; ++ie)
			gradz(ie) += var(ie) * fcell.Vn( k);
	}

	MGFloat vol = (MGFloat)DIM * fcell.Volume();
	for ( MGSize ie=0; ie<EQSIZE; ++ie)
		gradz(ie) /= vol;
}



template <>
inline void RANSKOMEGACommon<DIM_2D>::CalcGradC2( GVec& vec, const CFCell& fcell) const
{
	GVec gvec1 = 1.0 / ( mvarZm(ET::ID_RHO) * mvarZm(ET::ID_RHO) ) * ( mvarZm(ET::ID_RHO) * mGradZ(ET::ID_P) - mvarZm(ET::ID_P) * mGradZ(ET::ID_RHO) );
	GVec gvec2 = 1.0 / ( mvarZm(ET::ID_RHO) * mvarZm(ET::ID_RHO) * mvarZm(ET::ID_RHO) ) * 
				 ( 
				 mvarZm(ET::ID_RHO) * ( mvarZm(ET::U<0>::ID) * mGradZ(ET::U<0>::ID) + mvarZm(ET::U<1>::ID) * mGradZ(ET::U<1>::ID) ) -
				 ( mvarZm(ET::U<0>::ID)*mvarZm(ET::U<0>::ID) + mvarZm(ET::U<1>::ID)*mvarZm(ET::U<1>::ID) ) * mGradZ(ET::ID_RHO)
				 );
	vec = (0.4) * ( gvec1 - gvec2 );
}

template <>
inline void RANSKOMEGACommon<DIM_3D>::CalcGradC2( GVec& vec, const CFCell& fcell) const
{
	GVec gvec1 = 1.0 / ( mvarZm(ET::ID_RHO) * mvarZm(ET::ID_RHO) ) * ( mvarZm(ET::ID_RHO) * mGradZ(ET::ID_P) - mvarZm(ET::ID_P) * mGradZ(ET::ID_RHO) );
	GVec gvec2 = 1.0 / ( mvarZm(ET::ID_RHO) * mvarZm(ET::ID_RHO) * mvarZm(ET::ID_RHO) ) * 
				 ( 
				 mvarZm(ET::ID_RHO) * ( mvarZm(ET::U<0>::ID) * mGradZ(ET::U<0>::ID) + mvarZm(ET::U<1>::ID) * mGradZ(ET::U<1>::ID) + mvarZm(ET::U<2>::ID) * mGradZ(ET::U<2>::ID) ) -
				 ( mvarZm(ET::U<0>::ID)*mvarZm(ET::U<0>::ID) + mvarZm(ET::U<1>::ID)*mvarZm(ET::U<1>::ID) + mvarZm(ET::U<2>::ID)*mvarZm(ET::U<2>::ID) ) * mGradZ(ET::ID_RHO)
				 );
	vec = (0.4) * ( gvec1 - gvec2 );
}



template <Dimension DIM>
inline void RANSKOMEGACommon<DIM>::CalcGradK( GVec& vec, const CFCell& fcell) const
{
	vec = 1.0 / ( mvarZm(ET::ID_RHO) * mvarZm(ET::ID_RHO) ) * ( mvarZm(ET::ID_RHO) *  mGradZ(ET::ID_K) - mvarZm(ET::ID_K) * mGradZ(ET::ID_RHO) );
}

template <Dimension DIM>
inline void RANSKOMEGACommon<DIM>::CalcGradOmega( GVec& vec, const CFCell& fcell) const
{
	vec = 1.0 / ( mvarZm(ET::ID_RHO) * mvarZm(ET::ID_RHO) ) * ( mvarZm(ET::ID_RHO) *  mGradZ(ET::ID_OMEGA) - mvarZm(ET::ID_OMEGA) * mGradZ(ET::ID_RHO) );
}




template <>
inline void RANSKOMEGACommon<DIM_2D>::CalcGradU( FViscMtx& mtx, const CFCell& fcell) const
{
	GVec gradx = 1.0 / ( mvarZm(ET::ID_RHO) * mvarZm(ET::ID_RHO) ) * ( mvarZm(ET::ID_RHO) *  mGradZ(ET::U<0>::ID) - mvarZm(ET::U<0>::ID) * mGradZ(ET::ID_RHO) );
	GVec grady = 1.0 / ( mvarZm(ET::ID_RHO) * mvarZm(ET::ID_RHO) ) * ( mvarZm(ET::ID_RHO) *  mGradZ(ET::U<1>::ID) - mvarZm(ET::U<1>::ID) * mGradZ(ET::ID_RHO) );

	mtx(0,0) = gradx.cX();
	mtx(0,1) = gradx.cY();
	mtx(1,0) = grady.cX();
	mtx(1,1) = grady.cY();
}

template <>
inline void RANSKOMEGACommon<DIM_3D>::CalcGradU( FViscMtx& mtx, const CFCell& fcell) const
{
	GVec gradx = 1.0 / ( mvarZm(ET::ID_RHO) * mvarZm(ET::ID_RHO) ) * ( mvarZm(ET::ID_RHO) *  mGradZ(ET::U<0>::ID) - mvarZm(ET::U<0>::ID) * mGradZ(ET::ID_RHO) );
	GVec grady = 1.0 / ( mvarZm(ET::ID_RHO) * mvarZm(ET::ID_RHO) ) * ( mvarZm(ET::ID_RHO) *  mGradZ(ET::U<1>::ID) - mvarZm(ET::U<1>::ID) * mGradZ(ET::ID_RHO) );
	GVec gradz = 1.0 / ( mvarZm(ET::ID_RHO) * mvarZm(ET::ID_RHO) ) * ( mvarZm(ET::ID_RHO) * mGradZ(ET::U<2>::ID) - mvarZm(ET::U<2>::ID) * mGradZ(ET::ID_RHO) );

	mtx(0,0) = gradx.cX();
	mtx(0,1) = gradx.cY();
	mtx(0,2) = gradx.cZ();
	mtx(1,0) = grady.cX();
	mtx(1,1) = grady.cY();
	mtx(1,2) = grady.cZ();
	mtx(2,0) = gradz.cX();
	mtx(2,1) = gradz.cY();
	mtx(2,2) = gradz.cZ();
}


template <>
inline void RANSKOMEGACommon<DIM_2D>::CalcSbarOmega( FViscMtx& mtxs, MGFloat& vort2, const FViscMtx& mtxdu) const
{
	const MGFloat tr = ( mtxdu(0,0) + mtxdu(1,1) ) * 2./3.;
	mtxs(0,0) = 2. * mtxdu(0,0) - tr;
	mtxs(1,1) = 2. * mtxdu(1,1) - tr;
	mtxs(1,0) = mtxs(0,1) = ( mtxdu(0,1) + mtxdu(1,0) );

	vort2 = mtxdu(0,1) - mtxdu(1,0);
	vort2 *= vort2;
}


template <>
inline void RANSKOMEGACommon<DIM_3D>::CalcSbarOmega( FViscMtx& mtxs, MGFloat& vort2, const FViscMtx& mtxdu) const
{
	const MGFloat tr = ( mtxdu(0,0) + mtxdu(1,1) + mtxdu(2,2) ) * 2./3.;
	mtxs(0,0) = 2.* mtxdu(0,0) - tr;
	mtxs(1,1) = 2.* mtxdu(1,1) - tr;
	mtxs(2,2) = 2.* mtxdu(1,1) - tr;
	mtxs(1,0) = mtxs(0,1) = mtxdu(0,1) + mtxdu(1,0);
	mtxs(2,0) = mtxs(0,2) = mtxdu(0,2) + mtxdu(2,0);
	mtxs(2,1) = mtxs(1,2) = mtxdu(1,2) + mtxdu(2,1);

	const MGFloat oz = mtxdu(0,1) - mtxdu(1,0);
	const MGFloat oy = mtxdu(0,2) - mtxdu(2,0);
	const MGFloat ox = mtxdu(1,2) - mtxdu(2,1);

	vort2 = ox*ox + oy*oy + oz*oz;
}


template <Dimension DIM>
inline void RANSKOMEGACommon<DIM>::InitPhysics( const Physics<DIM,EQN_RANS_KOMEGA>& physics)
{
	this->mRe  = physics.cRe();
	this->mPr  = physics.cPr();
	this->mPrT = physics.cPrT();

	physics.DimToUndim( this->mRefVar, physics.cRefVar(), physics.cRefVar() );
}



template <Dimension DIM>
inline void RANSKOMEGACommon<DIM>::Initialize( const CFCell& fcell)
{
	this->mvarZm.Init( 0.0);
	this->mvarVm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
		this->mvarZm += this->mtabsZ[i] = EqnRansKOMEGA<DIM>::ConservToZ( fcell.cVar(i) );

	this->mvarZm /= (MGFloat)(DIM+1);
	this->mvarVm = EqnRansKOMEGA<DIM>::ZToSimp( this->mvarZm);
	this->mvarQm = EqnRansKOMEGA<DIM>::ZToConserv( this->mvarZm);

	
	this->mMu = MolecularMU( this->mvarZm);
	//cout << "mu = " << mMu << endl;

	CalcGradZ( this->mGradZ, fcell);
	CalcGradU( mmtxDU, fcell);

	CalcSbarOmega( mmtxSbar, mVort, mmtxDU);

	CalcGradK( mGradK, fcell);
	CalcGradOmega( mGradO, fcell);

	mCDko = 2. * mvarVm(ET::ID_RHO) * SST::sigma_omega2 * ( mGradK * mGradO )/*/mRe*/;
	mCDko = max( mCDko, 1.0e-10);


	mTOmegaAmb = mRefVar(ET::ID_OMEGA) / mRefVar(ET::ID_RHO);
	mOmegaAmb = exp( mTOmegaAmb );

	// calc limit for omega
	mOmega0 = 0.0;

	MGFloat omega_r0 = 0.0;
	for ( MGSize i=0; i<DIM; ++i)
		omega_r0 = max( 3./2. * mmtxSbar(i,i), omega_r0 );

	for ( MGSize i=0; i<DIM; ++i)
		for ( MGSize j=i+1; j<DIM; ++j)
		{
			MGFloat delta = mmtxSbar(i,i) - mmtxSbar(j,j);
			delta = delta*delta + 4.*mmtxSbar(i,j)*mmtxSbar(i,j);
			if ( delta < 0.0)
				THROW_INTERNAL( "RANSKOMEGACommon<DIM>::Initialize :: negative delta");
			MGFloat w = 3./4. * ( mmtxSbar(i,i) + mmtxSbar(j,j) + sqrt( delta) );
			omega_r0 = max( w, omega_r0 );
		}

	//mOmega0 = omega_r0;
	//mOmega0 = max( omega_r0, mOmegaAmb );


	// avarege distance from wall
    mYm = 0.0;
    for ( MGSize i=0; i<=DIM; ++i)
        mYm += fcell.cAux(i)(0);
    mYm /= (MGFloat)(DIM+1);

	mVort = sqrt( mVort );

	// avarege muT
	MGFloat mut = 0.0;
	////mut += TurbulentMU_SST( 0.5*(mtabsZ[0] + mtabsZ[1]), 0.5*(fcell.cAux(0)(0) + fcell.cAux(1)(0)) );
	////mut += TurbulentMU_SST( 0.5*(mtabsZ[1] + mtabsZ[2]), 0.5*(fcell.cAux(1)(0) + fcell.cAux(2)(0)) );
	////mut += TurbulentMU_SST( 0.5*(mtabsZ[2] + mtabsZ[0]), 0.5*(fcell.cAux(2)(0) + fcell.cAux(0)(0)) );

	//for ( MGSize i=0; i<DIM+1; ++i)
	//	mut += TurbulentMU_SST( mtabsZ[i], fcell.cAux(i)(0) );

	//mut /= MGFloat(DIM+1);

	mut = TurbulentMU_SST( mvarZm, mYm );

	this->mMuT = mut;
}




template <Dimension DIM>
inline void RANSKOMEGACommon<DIM>::CalcViscRes( const CFCell& fcell, FVec tabres[])
{
	GVec gradc2;

	CalcGradC2( gradc2, fcell);

	FViscMtx mtxT = (mMu + mRe*mMuT) * mmtxSbar;

	for ( MGSize k=0; k<DIM; ++k)
		mtxT(k,k) -= mRe * mvarQm(ET::ID_K) * 2./3.;


	FViscVec	vec;

	for ( MGSize k=0; k<DIM; ++k)
		vec(k) = this->mvarVm(k+2);

	FViscVec	ve = mtxT * vec;

	for ( MGSize i=0; i<DIM; ++i)
		gradc2.rX(i) = ve(i) +  2.5 * ( (this->mMu / mPr) + (mRe * this->mMuT / mPrT) ) * gradc2.cX(i);


	FVec	tabdres[DIM+1];

	for ( MGSize i=0; i<DIM+1; ++i)
	{
		GVec vn = fcell.Vn( i);

		tabdres[i](ET::ID_RHO) = 0.0;
		tabdres[i](ET::ID_K) = 0.0;
		tabdres[i](ET::ID_OMEGA) = 0.0;
		tabdres[i](ET::ID_P) = (gradc2 * vn) / ( DIM * this->mRe );

		for ( MGSize k=0; k<DIM; ++k)
		{
			GVec vv;

			for ( MGSize j=0; j<DIM; ++j)
				vv.rX(j) = mtxT(j,k);

			tabdres[i](k+2) = vv * vn / ( DIM * this->mRe );
		}
	}


	for ( MGSize i=0; i<DIM+1; ++i)
		tabres[i] += tabdres[i];
}


template <Dimension DIM>
inline void RANSKOMEGACommon<DIM>::CalcTurbRes_SSTE( const CFCell& fcell, FVec tabres[])
{
	const MGFloat volume = fcell.Volume();

	///////////////////////////////////////
	// production 
	MGFloat omega_mu = OmegaMu( mvarZm, mYm);
	MGFloat P = 0.0;

	for ( MGSize i=0; i<DIM; ++i)
		for ( MGSize j=0; j<DIM; ++j)
		{
			if ( i == j)
				P += ( mmtxSbar(i,j) - omega_mu*2./3.  ) * mmtxDU(i,j);
			else
				P += mmtxSbar(i,j) * mmtxDU(i,j);
		}

	//MGFloat limitP = 10. * SST::beta_star * omega_mu * exp( mvarVm(ET::ID_OMEGA) );
	MGFloat limitP = 10. * SST::beta_star * omega_mu * mOmega0;
	MGFloat Ptilde = P;//min( P, limitP);


	const MGFloat f1 = BlendingF1( mvarZm, mYm );

	const MGFloat sigma_k		= f1*SST::sigma_k1		+ (1.-f1)*SST::sigma_k2;
	const MGFloat sigma_omega	= f1*SST::sigma_omega1	+ (1.-f1)*SST::sigma_omega2;
	const MGFloat beta			= f1*SST::beta1			+ (1.-f1)*SST::beta2;
	const MGFloat alpha			= f1*SST::alpha1		+ (1.-f1)*SST::alpha2;


	MGFloat productK = 1./(DIM+1) * mMuT * Ptilde  * volume;

	MGFloat productO = 1./(DIM+1) * alpha * mvarVm(ET::ID_RHO) / Omega( mvarZm ) * Ptilde  * volume;
	//MGFloat productO = 1./(DIM+1) * alpha * mvarVm(ET::ID_RHO) / exp( mvarZm(ET::ID_OMEGA) / mvarZm(ET::ID_RHO) ) * Ptilde  * volume;


	///////////////////////////////////////
	// destruction

	MGFloat omega_r = Omega( mvarZm ); //max( exp( mvarZm(ET::ID_OMEGA) / mvarZm(ET::ID_RHO) ), mOmega0 );


	MGFloat destrK = mvarZm(ET::ID_K) / mvarZm(ET::ID_RHO) * omega_r;
	MGFloat destrO = omega_r;

	// ambient corr
	MGFloat aaa = mTOmegaAmb -  mvarZm(ET::ID_OMEGA) / mvarZm(ET::ID_RHO);
	aaa = exp( aaa);

	MGFloat destrKamb = mRefVar(ET::ID_K)/mRefVar(ET::ID_RHO) *  aaa;
	MGFloat destrOamb = mOmegaAmb * aaa;

	//
	destrK = 1./(DIM+1) * SST::beta_star * mvarVm(ET::ID_RHO) * ( destrK - destrKamb )  * volume;
	destrO = 1./(DIM+1) * beta *		   mvarVm(ET::ID_RHO) * ( destrO - destrOamb )  * volume;



	///////////////////////////////////////
	// diffusion
    MGFloat tabdiffK[DIM+1];
    MGFloat tabdiffO[DIM+1];
    MGFloat tabdiffOcross[DIM+1];

    for ( MGSize i=0; i<DIM+1; ++i)
    {
        GVec vn = fcell.Vn( i);

		tabdiffK[i]  = (mMu / mRe + sigma_k     * mMuT) * mGradK * vn / ( DIM );
        tabdiffO[i]  = (mMu / mRe + sigma_omega * mMuT) * mGradO * vn / ( DIM );
		
		tabdiffOcross[i] = (mMu / mRe + sigma_omega * mMuT) * (mGradO * mGradO) * volume / ( MGFloat(DIM+1) );
    }

	///////////////////////////////////////
	// cross difusiond
	MGFloat crossdiff = 2.*(1. - f1) * SST::sigma_omega2 * mvarVm(ET::ID_RHO) / Omega( mvarZm) * (mGradK * mGradO) * volume / ( MGFloat(DIM+1) );


	////////////////////
	// update residuum
	for ( MGSize i=0; i<DIM+1; ++i)
	{
		tabres[i](ET::ID_K)		+= tabdiffK[i];
		tabres[i](ET::ID_OMEGA) += tabdiffO[i];

		tabres[i](ET::ID_OMEGA)	-= tabdiffOcross[i];
		tabres[i](ET::ID_OMEGA)	-= crossdiff;

		tabres[i](ET::ID_P)		+= tabdiffK[i];

		IS_INFNAN_THROW( tabres[i]( ET::ID_K ), "ET::ID_K diffussion" );
		IS_INFNAN_THROW( tabres[i]( ET::ID_OMEGA ), "ET::ID_OMEGA diffussion" );
	}

	IS_INFNAN_THROW( productK, "ET::ID_K source production" );
	IS_INFNAN_THROW( productO, "ET::ID_OMEGA source production" );

	IS_INFNAN_THROW( destrK, "ET::ID_K source destruction" );
	IS_INFNAN_THROW( destrO, "ET::ID_OMEGA source destruction" ); 

	if ( ! fcell.IsInviscWall() && ! fcell.IsViscWall() )
	{
		for ( MGSize i=0; i<DIM+1; ++i)
		{

			tabres[i](ET::ID_K)		-= productK;
			tabres[i](ET::ID_OMEGA)	-= productO;

			tabres[i](ET::ID_K)		+= destrK;
			tabres[i](ET::ID_OMEGA)	+= destrO;

		}
	}

}


//
//template <Dimension DIM>
//inline void RANSKOMEGACommon<DIM>::CalcTurbRes_SSTEold( const CFCell& fcell, FVec tabres[])
//{
//	const MGFloat volume = fcell.Volume();
//
//	///////////////////////////////////////
//	// production 
//	MGFloat omega_mu = OmegaMu( mvarZm, mYm);
//	MGFloat P = 0.0;
//
//	for ( MGSize i=0; i<DIM; ++i)
//		for ( MGSize j=0; j<DIM; ++j)
//		{
//			if ( i == j)
//				P += ( mmtxSbar(i,j) - omega_mu*2./3.  ) * mmtxDU(i,j);
//			else
//				P += mmtxSbar(i,j) * mmtxDU(i,j);
//		}
//
//	MGFloat limitP = 10. * SST::beta_star * omega_mu * exp( mvarVm(ET::ID_OMEGA) );
//	MGFloat Ptilde = min( P, limitP);
//
//
//	const MGFloat f1 = BlendingF1( mvarZm, mYm );
//
//	const MGFloat sigma_k		= f1*SST::sigma_k1		+ (1.-f1)*SST::sigma_k2;
//	const MGFloat sigma_omega	= f1*SST::sigma_omega1	+ (1.-f1)*SST::sigma_omega2;
//	const MGFloat beta			= f1*SST::beta1			+ (1.-f1)*SST::beta2;
//	const MGFloat alpha			= f1*SST::alpha1		+ (1.-f1)*SST::alpha2;
//
//
//    MGFloat tabproductK[DIM+1];
//    MGFloat tabproductO[DIM+1];
//	for ( MGSize i=0; i<=DIM; ++i)
//	{
//		tabproductK[i] = 0.0;
//		tabproductO[i] = 0.0;
//
//		for ( MGSize j=0; j<=DIM; ++j)
//		{
//			MGFloat wgh = FEMWeights<DIM_2D>::Quadratic( i, j);
//			MGFloat locmut = TurbulentMU_SST( mtabsZ[j], fcell.cAux(j)(0) );
//
//			tabproductK[i] += wgh * locmut * Ptilde;
//			tabproductO[i] += wgh * alpha * mtabsZ[j](ET::ID_RHO)*mtabsZ[j](ET::ID_RHO) * exp( - mtabsZ[j](ET::ID_OMEGA) / mtabsZ[j](ET::ID_RHO) ) * Ptilde;
//		}
//
//		//tabproductK[i] = 1./(DIM+1) * mMuT * Ptilde;
//		//tabproductO[i] = 1./(DIM+1) * alpha * mvarVm(ET::ID_RHO) * exp( - mvarVm(ET::ID_OMEGA) ) * Ptilde;
//	}
//
//	///////////////////////////////////////
//	// destruction
//    MGFloat tabdestrK[DIM+1];
//    MGFloat tabdestrO[DIM+1];
//
//
//	for ( MGSize i=0; i<=DIM; ++i)
//	{
//		tabdestrK[i] = 0.0;
//		tabdestrO[i] = 0.0;
//
//		for ( MGSize j=0; j<=DIM; ++j)
//		{
//			for ( MGSize k=0; k<=DIM; ++k)
//			{
//				MGFloat wgh;
//				if ( i==j && j==k )
//					wgh = 1. / 10.;
//				else
//				if ( i!=j && i!=k && j!=k )
//					wgh = 1. / 60.;
//				else
//					wgh = 1. / 30.;
//
//				tabdestrK[i] += wgh * mtabsZ[j](ET::ID_K) / mtabsZ[j](ET::ID_RHO) * exp( mtabsZ[k](ET::ID_OMEGA) / mtabsZ[k](ET::ID_RHO) );
//				tabdestrO[i] += wgh * exp( mtabsZ[j](ET::ID_OMEGA) / mtabsZ[j](ET::ID_RHO) );
//			}
//
//		}
//
//		//tabdestrK[i] = 1./(DIM+1) * mvarZm(ET::ID_K) / mvarZm(ET::ID_RHO) * exp( mvarZm(ET::ID_OMEGA) / mvarZm(ET::ID_RHO) );
//		//tabdestrO[i] = 1./(DIM+1) * exp( mvarZm(ET::ID_OMEGA) / mvarZm(ET::ID_RHO) );
//
//		tabdestrK[i] = SST::beta_star * ( mvarVm(ET::ID_RHO) * tabdestrK[i]  /*-  1./3. * mRefVar(ET::ID_K) * exp( mRefVar(ET::ID_OMEGA)/mRefVar(ET::ID_RHO))*/ );
//		tabdestrO[i] = beta *			( mvarVm(ET::ID_RHO) * tabdestrO[i]  /*-  1./3. * mRefVar(ET::ID_RHO) * exp( mRefVar(ET::ID_OMEGA)/mRefVar(ET::ID_RHO))*/ );
//	}
//
//
//
//	///////////////////////////////////////
//	// diffusion
//    MGFloat tabdiffK[DIM+1];
//    MGFloat tabdiffO[DIM+1];
//
//
//
//    for ( MGSize i=0; i<DIM+1; ++i)
//    {
//        GVec vn = fcell.Vn( i);
//
//		//tabdiffK[i] = 0.0;
//  //      tabdiffO[i] = 0.0;
//
//		//for ( MGSize j=0; j<=DIM; ++j)
//		//{
//		//	MGFloat wgh = (i==j ? 1./6. : 1./12.);
//		//	MGFloat locmut = TurbulentMU( mtabsZ[j] );
//
//		//	tabdiffK[i] += (mMu + sigma_k     * locmut) * gradk * vn / ( DIM * mRe );  // molecular
//		//    tabdiffO[i] += (mMu + sigma_omega * locmut) * grado * vn / ( DIM * mRe );	// molecular
//		//}
//
//		MGFloat locmut = mMuT;
//		//MGFloat locmut = TurbulentMU( mtabsZ[i] );
//
//		tabdiffK[i] = (mMu + sigma_k     * locmut) * mGradK * vn / ( DIM * mRe );  // molecular
//        tabdiffO[i] = (mMu + sigma_omega * locmut) * mGradO * vn / ( DIM * mRe );	// molecular
//
//		tabdiffO[i] -= (mMu + sigma_omega * locmut) * (mGradO * mGradO) * volume / ( MGFloat(DIM+1) * mRe );
//
//    }
//
//	///////////////////////////////////////
//	// cross difusion
//    MGFloat tabcrossdiff[DIM+1];
//    for ( MGSize i=0; i<DIM+1; ++i)
//    {
//		//MGFloat locmut = TurbulentMU( mtabsZ[i] );
//		tabcrossdiff[i] = 2.*(1. - f1) * SST::sigma_omega2 * mvarVm(ET::ID_RHO) * exp( - mvarVm(ET::ID_OMEGA) ) * mGradK * mGradO  * volume / ( MGFloat(DIM+1) * mRe );
//	}
//
//
//	////////////////////
//	// update residuum
//	for ( MGSize i=0; i<DIM+1; ++i)
//	{
//		tabres[i](ET::ID_K)		+= tabdiffK[i];
//		tabres[i](ET::ID_OMEGA) += tabdiffO[i];
//
//		tabres[i](ET::ID_OMEGA)	-= tabcrossdiff[i];
//	}
//
//	if ( ! fcell.IsInviscWall() && ! fcell.IsViscWall() )
//	{
//		for ( MGSize i=0; i<DIM+1; ++i)
//		{
//			//if ( ! fcell.IsJacobian() )
//			{
//				tabres[i](ET::ID_K)		-= tabproductK[i]  * volume;
//				tabres[i](ET::ID_OMEGA)	-= tabproductO[i]  * volume;
//			}
//
//			tabres[i](ET::ID_K)		+= tabdestrK[i]  * volume;
//			tabres[i](ET::ID_OMEGA)	+= tabdestrO[i]  * volume;
//		}
//	}
//
//}
//
//
//template <Dimension DIM>
//inline void RANSKOMEGACommon<DIM>::CalcTurbRes_EXP( const CFCell& fcell, FVec tabres[])
//{
//	const MGFloat beta_star = 0.09;
//	const MGFloat kappa = 0.41;
//
//	const MGFloat sigma_k1 = 0.5; // 0.85;
//	const MGFloat sigma_omega1 = 0.5;
//	const MGFloat beta1 = 0.0750;
//	//const MGFloat gamma1 = beta1 / beta_star - sigma_omega1*kappa*kappa / sqrt(beta_star);
//	const MGFloat gamma1 = 5. / 9.;
//
//	const MGFloat sigma_k2 = 1.0;
//	const MGFloat sigma_omega2 = 0.856;
//	const MGFloat beta2 = 0.0828;
//	//const MGFloat gamma2 = beta2 / beta_star - sigma_omega2*kappa*kappa / sqrt(beta_star);
//	const MGFloat gamma2 = 0.44;
//
//
//	const MGFloat volume = fcell.Volume();
//
//    MGFloat dist = 0.0;
//    for ( MGSize i=0; i<=DIM; ++i)
//        dist += fcell.cAux(i)(0);
//    dist /= (MGFloat)(DIM+1);
//
//    dist = max( dist, 1.0e-12 );
//
//	///////////////////////////////////////
//	// production 
//	MGFloat limitPk = 20. * beta_star * mvarZm(ET::ID_K) * mvarZm(ET::ID_OMEGA);
//
//	MGFloat P = 0.0;
//	MGFloat O = 0.0;
//	for ( MGSize i=0; i<DIM; ++i)
//		for ( MGSize j=0; j<DIM; ++j)
//		{
//			P += (mmtxDU(i,j) + mmtxDU(j,i) ) * mmtxDU(i,j);
//			O += ( mmtxDU(i,j) - mmtxDU(j,i) ) * ( mmtxDU(i,j) - mmtxDU(j,i) );
//		}
//
//	O *= 0.5;
//
//	const MGFloat f1 = BlendingF1( mvarZm, dist, sqrt(O) );
//
//	const MGFloat sigma_k		= f1*sigma_k1		+ (1.-f1)*sigma_k2;
//	const MGFloat sigma_omega	= f1*sigma_omega1	+ (1.-f1)*sigma_omega2;
//	const MGFloat beta			= f1*beta1			+ (1.-f1)*beta2;
//	const MGFloat gamma			= f1*gamma1			+ (1.-f1)*gamma2;
//
//
//	//MGFloat Pbar = min( mMuT*P, 10.*mRe*beta_star * mvarVm(ET::ID_RHO)*mvarVm(ET::ID_K) * exp(mvarVm(ET::ID_OMEGA)) );
//
//	MGFloat Pbar = min( P, 10.*beta_star *exp( 2.*mvarVm(ET::ID_OMEGA)) );
//	//cout << "P = " << P;
//	//cout << "  Pbar = " << Pbar;
//	//cout << endl;
//
//    MGFloat tabproductK[DIM+1];
//    MGFloat tabproductO[DIM+1];
//	for ( MGSize i=0; i<=DIM; ++i)
//	{
//		//tabproductK[i] = 0.0;
//		//tabproductO[i] = 0.0;
//
//		//for ( MGSize j=0; j<=DIM; ++j)
//		//{
//		//	MGFloat wgh = FEMWeights<DIM_2D>::Quadratic( i, j); //(i==j ? 1./6. : 1./12.);
//		//	//MGFloat locmut = TurbulentMU( mtabsZ[j] );
//		//	MGFloat locmut = TurbulentMU_SST( mtabsZ[j], fcell.cAux(j)(0), sqrt( O) );
//
//		//	//MGFloat Olimit = min( O,  10.*beta_star*mtabsZ[j](ET::ID_RHO)*mtabsZ[j](ET::ID_K)*exp( mtabsZ[j](ET::ID_OMEGA) / mtabsZ[j](ET::ID_RHO) ) / locmut );
//		//	//tabproductK[i] += wgh * locmut * Olimit;
//		//	tabproductK[i] += wgh * locmut * O;
//
//		//	//tabproductO[i] += wgh * gamma * mtabsZ[j](ET::ID_RHO) * Olimit * exp( - mtabsZ[j](ET::ID_OMEGA) / mtabsZ[j](ET::ID_RHO) );
//		//	tabproductO[i] += wgh * gamma * mtabsZ[j](ET::ID_RHO) * O * exp( - mtabsZ[j](ET::ID_OMEGA) / mtabsZ[j](ET::ID_RHO) );
//		//}
//
//		tabproductK[i] = 1./(DIM+1) * mMuT * Pbar;
//		tabproductO[i] = 1./(DIM+1) * gamma * mvarVm(ET::ID_RHO) * Pbar * exp( - mvarVm(ET::ID_OMEGA) );
//		//tabproductK[i] = 1./(DIM+1) * mMuT * P;
//		//tabproductO[i] = 1./(DIM+1) * gamma * mvarVm(ET::ID_RHO) * P * exp( - mvarVm(ET::ID_OMEGA) );
//
//		//tabproductK[i] = 1./(DIM+1) * mMuT * O;
//		//tabproductO[i] = 1./(DIM+1) * gamma * mvarVm(ET::ID_RHO) * O * exp( - mvarVm(ET::ID_OMEGA) );
//	}
//
//	///////////////////////////////////////
//	// destruction
//    MGFloat tabdestrK[DIM+1];
//    MGFloat tabdestrO[DIM+1];
//
//	//MGFloat dlimit = 0.;
//	//MGFloat tr = 0.;
//	//for ( MGSize i=0; i<=DIM; ++i)
//	//	tr += mmtxS(i,i);
//
//	//for ( MGSize i=0; i<=DIM; ++i)
//	//	dlimit += 3. * mmtxS(i,i) - tr;
//
//
//	for ( MGSize i=0; i<=DIM; ++i)
//	{
//		tabdestrK[i] = 0.0;
//		tabdestrO[i] = 0.0;
//
//		for ( MGSize j=0; j<=DIM; ++j)
//		{
//			for ( MGSize k=0; k<=DIM; ++k)
//			{
//				MGFloat wgh;
//				if ( i==j && j==k )
//					wgh = 1. / 10.;
//				else
//				if ( i!=j && i!=k && j!=k )
//					wgh = 1. / 60.;
//				else
//					wgh = 1. / 30.;
//
//				tabdestrK[i] += wgh * mtabsZ[j](ET::ID_K) / mtabsZ[j](ET::ID_RHO) * exp( mtabsZ[k](ET::ID_OMEGA) / mtabsZ[k](ET::ID_RHO) );
//				tabdestrO[i] += wgh * exp( mtabsZ[j](ET::ID_OMEGA) / mtabsZ[j](ET::ID_RHO) );
//			}
//
//		}
//
//		//tabdestrK[i] = 1./3. * mvarZm(ET::ID_K) / mvarZm(ET::ID_RHO) * exp( mvarZm(ET::ID_OMEGA) / mvarZm(ET::ID_RHO) );
//		//tabdestrO[i] = 1./3. * exp( mvarZm(ET::ID_OMEGA) / mvarZm(ET::ID_RHO) );
//
//		tabdestrK[i] = beta_star * ( mvarVm(ET::ID_RHO) * tabdestrK[i]  -  1./3. * mRefVar(ET::ID_K) * exp( mRefVar(ET::ID_OMEGA)/mRefVar(ET::ID_RHO)) );
//		tabdestrO[i] = beta *      ( mvarVm(ET::ID_RHO) * tabdestrO[i]  -  1./3. * mRefVar(ET::ID_RHO) * exp( mRefVar(ET::ID_OMEGA)/mRefVar(ET::ID_RHO)) );
//
//		//tabdestrK[i] = 1./3. * beta_star * ( mvarQm(ET::ID_K)   * exp( mvarVm(ET::ID_OMEGA) )  -  mRefVar(ET::ID_K)   * exp( mRefVar(ET::ID_OMEGA)/mRefVar(ET::ID_RHO)) );
//		//tabdestrO[i] = 1./3. * beta *      ( mvarVm(ET::ID_RHO) * exp( mvarVm(ET::ID_OMEGA) )  -  mRefVar(ET::ID_RHO) * exp( mRefVar(ET::ID_OMEGA)/mRefVar(ET::ID_RHO)) );
//
//		//MGFloat eor = exp( mvarVm(ET::ID_OMEGA));
//		////MGFloat eor = min(  exp( mvarVm(ET::ID_OMEGA)), dlimit );
//
//		//tabdestrK[i] = 1./3. * beta_star * ( mvarQm(ET::ID_K)   * eor   -  mRefVar(ET::ID_K)   * exp( mRefVar(ET::ID_OMEGA)/mRefVar(ET::ID_RHO)) );
//		//tabdestrO[i] = 1./3. * beta *      ( mvarVm(ET::ID_RHO) * eor   -  mRefVar(ET::ID_RHO) * exp( mRefVar(ET::ID_OMEGA)/mRefVar(ET::ID_RHO)) );
//	}
//
//
//
//	///////////////////////////////////////
//	// diffusion
//    MGFloat tabdiffK[DIM+1];
//    MGFloat tabdiffO[DIM+1];
//
//	//GVec gradk, grado;
//	//CalcGradK( gradk, fcell);
//	//CalcGradOmega( grado, fcell);
//
//
//    for ( MGSize i=0; i<DIM+1; ++i)
//    {
//        GVec vn = fcell.Vn( i);
//
//		//tabdiffK[i] = 0.0;
//  //      tabdiffO[i] = 0.0;
//
//		//for ( MGSize j=0; j<=DIM; ++j)
//		//{
//		//	MGFloat wgh = (i==j ? 1./6. : 1./12.);
//		//	MGFloat locmut = TurbulentMU( mtabsZ[j] );
//
//		//	tabdiffK[i] += (mMu + sigma_k     * locmut) * gradk * vn / ( DIM * mRe );  // molecular
//		//    tabdiffO[i] += (mMu + sigma_omega * locmut) * grado * vn / ( DIM * mRe );	// molecular
//		//}
//
//		MGFloat locmut = mMuT;
//		//MGFloat locmut = TurbulentMU( mtabsZ[i] );
//
//		tabdiffK[i] = (mMu + sigma_k     * locmut) * mGradK * vn / ( DIM * mRe );  // molecular
//        tabdiffO[i] = (mMu + sigma_omega * locmut) * mGradO * vn / ( DIM * mRe );	// molecular
//
//		tabdiffO[i] -= (mMu + sigma_omega * locmut) * (mGradO * mGradO) * volume / ( MGFloat(DIM+1) * mRe );
//
//    }
//
//	///////////////////////////////////////
//	// cross difusion
//    MGFloat tabcrossdiff[DIM+1];
//    for ( MGSize i=0; i<DIM+1; ++i)
//    {
//		//MGFloat locmut = TurbulentMU( mtabsZ[i] );
//		tabcrossdiff[i] = 2.*(1. - f1) * sigma_omega2 * mvarVm(ET::ID_RHO) * exp( - mvarVm(ET::ID_OMEGA) ) * mGradK * mGradO  * volume / ( MGFloat(DIM+1) * mRe );
//	}
//
//
//	////////////////////
//	// update residuum
//	for ( MGSize i=0; i<DIM+1; ++i)
//	{
//		tabres[i](ET::ID_K)		+= tabdiffK[i];
//		tabres[i](ET::ID_OMEGA) += tabdiffO[i];
//
//		tabres[i](ET::ID_OMEGA)	-= tabcrossdiff[i];
//	}
//
//	if ( ! fcell.IsInviscWall() && ! fcell.IsViscWall() )
//	{
//		for ( MGSize i=0; i<DIM+1; ++i)
//		{
//			//if ( ! fcell.IsJacobian() )
//			{
//				tabres[i](ET::ID_K)		-= tabproductK[i]  * volume;
//				tabres[i](ET::ID_OMEGA)	-= tabproductO[i]  * volume;
//			}
//
//			tabres[i](ET::ID_K)		+= tabdestrK[i]  * volume;
//			tabres[i](ET::ID_OMEGA)	+= tabdestrO[i]  * volume;
//		}
//	}
//
//}





template <Dimension DIM>
inline void RANSKOMEGACommon<DIM>::CalcTurbRes( const CFCell& fcell, FVec tabres[])
{
	CalcTurbRes_SSTE( fcell, tabres);
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RANSKOMEGAFULLCOMMON_H__

