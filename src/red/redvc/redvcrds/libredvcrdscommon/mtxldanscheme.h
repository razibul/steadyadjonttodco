#ifndef __MTXLDANSCHEME_H__
#define __MTXLDANSCHEME_H__

#include "libcoresystem/mgdecl.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


const MGFloat	RDSLDAN_EPS	= 1.0e-4;


template <Dimension DIM>
class MtxLDANscheme
{
public:

	template <class MATRIX, class VECTOR>
	inline void Calc( const MGSize& nmtx, 
		 		  const MATRIX tabKp[DIM+1], const MATRIX tabKm[DIM+1], 
				  const VECTOR tabU[DIM+1], VECTOR tabFi[DIM+1] )
	{//LDA
		MATRIX	tabK[DIM+1];
		MATRIX	mtxK(nmtx,nmtx);
		MATRIX	skp(nmtx,nmtx,0.0);
		MATRIX	skm( nmtx,nmtx, 0.0);
		VECTOR	skmu( nmtx,0.0);
		VECTOR	vtmp(nmtx,0.0);
		VECTOR	fi(nmtx,0.0);
		VECTOR	theta(nmtx,0.0);
		VECTOR	tabFiN[DIM+1];
		VECTOR	tabFiLDA[DIM+1];


		for ( MGSize i=0; i<=DIM; ++i)
		{
			skp += tabKp[i];
			skm += tabKm[i];
			skmu += tabKm[i]*tabU[i];

			mtxK = tabKp[i] + tabKm[i];

			fi += mtxK * tabU[i];
		}

		skp.Invert();
		skm.Invert();
		skmu = skm * skmu;
		vtmp = skp * fi;

		for ( MGSize i=0; i<=DIM; ++i)
		{
			tabFiN[i] = tabKp[i] * ( tabU[i] - skmu);
			tabFiLDA[i] = tabKp[i] * vtmp;
		}

		
		skmu.Init( 0.0);
		for ( MGSize i=0; i<=DIM; ++i)
			for ( MGSize j=0; j<nmtx; ++j)
				//skmu(j) += ::fabs( tabFiN[i](j) );
				skmu(j) = MAX( skmu(j), ::fabs( tabFiN[i](j) ) );	// <-------------------

		MGFloat	dtmp;
		for ( MGSize j=0; j<nmtx; ++j)
		{
			dtmp = MIN( 1.0, ::fabs( fi(j)) / MAX( skmu(j), RDSLDAN_EPS) );	// CHECK zero coefficient
			theta(j) = dtmp;
		}

		//dtmp = theta(0);
		//for ( MGSize j=1; j<nmtx; ++j)
		//	if ( theta(j) > dtmp )
		//		dtmp = theta(j);


		for ( MGSize i=0; i<=DIM; ++i)
		{
			for ( MGSize j=0; j<nmtx; ++j)
				tabFi[i](j) = theta(j)*tabFiN[i](j) + (1.0-theta(j))*tabFiLDA[i](j);
				//tabFi[i](j) = dtmp*tabFiN[i](j) + (1.0-dtmp)*tabFiLDA[i](j);
		}
	}



	template <class MATRIX, class VECTOR>
	inline void Calc( const MGSize& nmtx, 
		 		  const MATRIX tabKp[DIM+1], const MATRIX tabKm[DIM+1], 
				  const VECTOR tabU[DIM+1], VECTOR tabFi[DIM+1], MATRIX mtxJ[DIM+1][DIM+1] )
	{//LDA
		MATRIX	tabK[DIM+1];
		MATRIX	mtxKlda(nmtx,nmtx);
		MATRIX	mtxKn(nmtx,nmtx);
		MATRIX	skp(nmtx,nmtx,0.0);
		MATRIX	skm( nmtx,nmtx, 0.0);
		MATRIX	mtxtmp( nmtx,nmtx, 0.0);
		VECTOR	skmu( nmtx,0.0);
		VECTOR	vtmp(nmtx,0.0);
		VECTOR	fi(nmtx,0.0);
		VECTOR	theta(nmtx,0.0);
		VECTOR	tabFiN[DIM+1];
		VECTOR	tabFiLDA[DIM+1];


		for ( MGSize i=0; i<=DIM; ++i)
		{
			skp += tabKp[i];
			skm += tabKm[i];
			skmu += tabKm[i]*tabU[i];

			tabK[i] = tabKp[i] + tabKm[i];

			fi += tabK[i] * tabU[i];
		}

		skp.Invert();
		skm.Invert();
		skmu = skm * skmu;
		vtmp = skp * fi;

		for ( MGSize i=0; i<=DIM; ++i)
		{
			tabFiN[i] = tabKp[i] * ( tabU[i] - skmu);
			tabFiLDA[i] = tabKp[i] * vtmp;
		}

		
		skmu.Init( 0.0);
		for ( MGSize i=0; i<=DIM; ++i)
			for ( MGSize j=0; j<nmtx; ++j)
				skmu(j) += ::fabs( tabFiN[i](j) );

		MGFloat	dtmp;
		for ( MGSize j=0; j<nmtx; ++j)
		{
			dtmp = MIN( 1.0, ::fabs( fi(j)) / MAX( skmu(j), RDSLDAN_EPS) );	// CHECK zero coefficient
			theta(j) = dtmp;
		}

		//dtmp = theta(0);
		//for ( MGSize j=1; j<nmtx; ++j)
		//	if ( theta(j) > dtmp )
		//		dtmp = theta(j);


		for ( MGSize i=0; i<=DIM; ++i)
		{
			for ( MGSize j=0; j<nmtx; ++j)
				tabFi[i](j) = theta(j)*tabFiN[i](j) + (1.0-theta(j))*tabFiLDA[i](j);
				//tabFi[i](j) = dtmp*tabFiN[i](j) + (1.0-dtmp)*tabFiLDA[i](j);

		}


		// picard jacobian
		skm *= -1.0;

		for ( MGSize i=0; i<=DIM; ++i)
		{
			MtxMultMtxMtx( mtxKn, skm, tabKm[i] );
			MtxMultMtxMtx( mtxKlda, skp, tabK[i] );

			for ( MGSize j=0; j<=DIM; ++j)
			{
				MATRIX	mtx( mtxKn);

				if ( i == j)
				{
					for ( MGSize k=0; k<nmtx; ++k)
						mtx(k,k) += 1.0;
				}

				for ( MGSize k=0; k<nmtx; ++k)
					for ( MGSize n=0; n<nmtx; ++n)
						mtxtmp(k,n) = theta(k)*mtx(k,n) + (1.0-theta(k))*mtxKlda(k,n);
						//mtxtmp(k,n) = dtmp*mtx(k,n) + (1.0-dtmp)*mtxKlda(k,n);


				MtxMultMtxMtx( mtxJ[j][i], tabKp[j], mtxtmp);
			}
		}

	}

};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __MTXLDANSCHEME_H__
