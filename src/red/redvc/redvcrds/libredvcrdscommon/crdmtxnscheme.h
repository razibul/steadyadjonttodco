#ifndef __CRDMTXNSCHEME_H__ 
#define __CRDMTXNSCHEME_H__ 


#include "libcoresystem/mgdecl.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class CRDMtxNscheme
{
public:

	template <class MATRIX, class VECTOR>
	inline void Calc( const MGSize& nmtx, 
		 		  MATRIX tabKp[DIM+1], MATRIX tabKm[DIM+1], 
				  const VECTOR tabU[DIM+1], const VECTOR& crdFi, VECTOR tabFi[DIM+1] )
	{//N

		VECTOR	tabDi[DIM+1];
		MATRIX	tabK[DIM+1];
		MATRIX	mtxK(nmtx,nmtx);
		MATRIX	skp(nmtx,nmtx,0.0);
		VECTOR	uout(nmtx,0.0);
		VECTOR	vtmp(nmtx,0.0);
		VECTOR	fi(nmtx,0.0);

		for ( MGSize i=0; i<=DIM; ++i)
		{
			skp += tabKp[i];
			VecAddMultMtxVec( vtmp, tabKp[i], tabU[i]);
		}

		skp.Invert();

		VecMultMtxVec( fi, skp, crdFi);
		VecMultMtxVec( uout, skp, vtmp);


		for ( MGSize i=0; i<=DIM; ++i)
		{
			VecMultMtxVec( tabFi[i], tabKp[i], fi);
			VecMultMtxVec( tabDi[i], tabKp[i], tabU[i] - uout);
		}

		for ( MGSize i=0; i<=DIM; ++i)
			tabFi[i] += tabDi[i];
	}



	template <class MATRIX, class VECTOR>
	inline void Calc( const MGSize& nmtx, 
		 		  const MATRIX tabKp[DIM+1], const MATRIX tabKm[DIM+1], 
				  const VECTOR tabU[DIM+1], VECTOR tabFi[DIM+1], MATRIX mtxJ[DIM+1][DIM+1] )
	{//N

		THROW_INTERNAL( "NOT IMPLEMENTED");

		MATRIX	tabK[DIM+1];
		MATRIX	mtxK(nmtx,nmtx);
		MATRIX	skp(nmtx,nmtx,0.0);
		VECTOR	fi(nmtx,0.0);
		VECTOR	vtmp(nmtx,0.0);

		for ( MGSize i=0; i<=DIM; ++i)
		{
			skp += tabKp[i];
			MtxSumMtxMtx( tabK[i], tabKp[i], tabKm[i]);
			VecAddMultMtxVec( vtmp, tabK[i], tabU[i]);
		}

		skp.Invert();

		VecMultMtxVec( fi, skp, vtmp);

		for ( MGSize i=0; i<=DIM; ++i)
			VecMultMtxVec( tabFi[i], tabKp[i], fi);


		// picard jacobian
		for ( MGSize i=0; i<=DIM; ++i)
		{
			MtxMultMtxMtx( mtxK, skp, tabK[i] );

			for ( MGSize j=0; j<=DIM; ++j)
				MtxMultMtxMtx( mtxJ[j][i], tabKp[j], mtxK);
		}
	}

};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __CRDMTXNSCHEME_H__ 
