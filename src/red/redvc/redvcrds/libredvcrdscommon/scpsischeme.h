#ifndef __SCPSISCHEME_H__
#define __SCPSISCHEME_H__

#include "scnscheme.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Dimension DIM>
class ScPSIscheme
{
public:

	//template <class T>
	//static void Calc( const T tabk[], const T tabu[], T tabfi[])
	//{// PSI
	//	T	tabb[DIM+1];
	//	T	fi=0.0, sb=0.0;

	//	ScNscheme<DIM>::Calc( tabk, tabu, tabb);

	//	for ( MGSize i=0; i<=DIM; ++i)
	//		fi += tabk[i]*tabu[i];

	//	if ( ::fabs(fi) < ZERO)
	//	//if ( ::fabs(fi) < 1.0e-20)
	//	{
	//		for ( MGSize i=0; i<=DIM; ++i)
	//			tabfi[i] = 0.0;
	//			//tabfi[i] = fi / (DIM+1.);

	//		return;
	//	}

	//	for ( MGSize i=0; i<=DIM; ++i)
	//	{
	//		tabb[i] = MAX( 0, tabb[i]/fi);
	//		sb += tabb[i];
	//	}

	//	for ( MGSize i=0; i<=DIM; ++i)
	//		tabfi[i] = fi * MAX( 0, tabb[i])/sb;
	//}


	template <class T>
	static void Calc( const T tabk[], const T tabu[], T tabfi[])
	{// PSI
		T	tabb[DIM+1];
		T	fi=0.0;

		T	tabkp[DIM+1], tabkm[DIM+1];
		T	skm=0.0, skmu=0.0;
		//T	e = 1.0e-7;

		for ( MGSize i=0; i<=DIM; ++i)
		{
			//tabkp[i] = MAX( e, tabk[i]); 
			//tabkm[i] = MIN( -e, tabk[i]);
			tabkp[i] = MAX( 0, tabk[i]);
			tabkm[i] = MIN( 0, tabk[i]);

			fi += tabk[i]*tabu[i];
			skm += tabkm[i];
			skmu += tabkm[i] * tabu[i];
		}

		T uin = skmu / skm;

		T sb=0;
		T sigfi = SIGN(fi);

		for ( MGSize i=0; i<=DIM; ++i)
		{
			tabb[i] = tabkp[i] * MAX( 0, ( tabu[i] - uin) * sigfi );
			sb += tabb[i];
		}

		if ( ::fabs(sb) < ZERO)
			sb = SIGN(sb) * MAX( ZERO,  ::fabs(sb) );

		for ( MGSize i=0; i<=DIM; ++i)
			tabfi[i] = fi * tabb[i]/sb;
	}


};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __SCPSISCHEME_H__
