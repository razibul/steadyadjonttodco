#ifndef __SCNSCHEME_H__
#define __SCNSCHEME_H__

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Dimension DIM>
class ScNscheme
{
public:

	template <class T>
	static void Calc( const T tabk[], const T tabu[], T tabfi[])
	{// N
		T	tabkp[DIM+1], tabkm[DIM+1];
		T	skm=0.0, skmu=0.0;
		//T	e = 1.0e-7;

		for ( MGSize i=0; i<=DIM; ++i)
		{
			//tabkp[i] = MAX( e, tabk[i]); 
			//tabkm[i] = MIN( -e, tabk[i]);
			tabkp[i] = MAX( 0, tabk[i]);
			tabkm[i] = MIN( 0, tabk[i]);

			skm += tabkm[i];
			skmu += tabkm[i] * tabu[i];
		}

		skmu /= skm;
		for ( MGSize i=0; i<=DIM; ++i)
		{
			tabfi[i] = tabkp[i] * ( tabu[i] - skmu);
		}
	}



};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __SCNSCHEME_H__
