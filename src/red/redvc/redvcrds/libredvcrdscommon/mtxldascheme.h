#ifndef __MTXLDASCHEME_H__
#define __MTXLDASCHEME_H__

#include "libcoresystem/mgdecl.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class MtxLDAscheme
{
public:

	template <class MATRIX, class VECTOR>
	inline void Calc( const MGSize& nmtx, 
		 		  MATRIX tabKp[DIM+1], MATRIX tabKm[DIM+1], 
				  const VECTOR tabU[DIM+1], VECTOR tabFi[DIM+1] )
	{//LDA
		MATRIX	tabK[DIM+1];
		MATRIX	mtxK(nmtx,nmtx);
		MATRIX	skp(nmtx,nmtx,0.0);
		VECTOR	fi(nmtx,0.0);
		VECTOR	vtmp(nmtx,0.0);

		//const MGFloat eps = 1.0e-14;

		for ( MGSize i=0; i<=DIM; ++i)
		{
			//for ( MGSize k=0; k<nmtx; ++k)
			//{
			//	tabKp[i](k,k) += eps;
			//	tabKm[i](k,k) -= eps;
			//}
			MtxSumMtxMtx( tabK[i], tabKp[i], tabKm[i]);
			VecAddMultMtxVec( vtmp, tabK[i], tabU[i]);
			skp += tabKp[i];
		}

		skp.Invert();

		VecMultMtxVec( fi, skp, vtmp);

		for ( MGSize i=0; i<=DIM; ++i)
			VecMultMtxVec( tabFi[i], tabKp[i], fi);
	}



	template <class MATRIX, class VECTOR>
	inline void Calc( const MGSize& nmtx, 
		 		  const MATRIX tabKp[DIM+1], const MATRIX tabKm[DIM+1], 
				  const VECTOR tabU[DIM+1], VECTOR tabFi[DIM+1], MATRIX mtxJ[DIM+1][DIM+1] )
	{//LDA
		MATRIX	tabK[DIM+1];
		MATRIX	mtxK(nmtx,nmtx);
		MATRIX	skp(nmtx,nmtx,0.0);
		VECTOR	fi(nmtx,0.0);
		VECTOR	vtmp(nmtx,0.0);

		for ( MGSize i=0; i<=DIM; ++i)
		{
			skp += tabKp[i];
			MtxSumMtxMtx( tabK[i], tabKp[i], tabKm[i]);
			VecAddMultMtxVec( vtmp, tabK[i], tabU[i]);
		}

		skp.Invert();

		VecMultMtxVec( fi, skp, vtmp);

		for ( MGSize i=0; i<=DIM; ++i)
			VecMultMtxVec( tabFi[i], tabKp[i], fi);


		// picard jacobian
		for ( MGSize i=0; i<=DIM; ++i)
		{
			MtxMultMtxMtx( mtxK, skp, tabK[i] );

			for ( MGSize j=0; j<=DIM; ++j)
				MtxMultMtxMtx( mtxJ[j][i], tabKp[j], mtxK);
		}
	}

};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __MTXLDASCHEME_H__
