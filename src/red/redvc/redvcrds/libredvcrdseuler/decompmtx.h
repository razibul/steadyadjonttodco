#ifndef __DECOMPMTX_H__
#define __DECOMPMTX_H__


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
// class DecompMtx
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class DecompMtx
{
	enum { EQSIZE = EquationDef<DIM,EQN_EULER>::SIZE };
	typedef Vect<DIM>									GVect;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FMtx	FMtx;

public:
	DecompMtx()	{}
	DecompMtx( const FVec& var, const FVec tab[], const GVect tabn[])	{ Init( var, tab, tabn);}

	void	Init( const FVec& var, const FVec tab[], const GVect tabn[]);

	void	Calc( FMtx &smtxKp, FMtx &smtxKm, const GVect &vn, const MGFloat& e, bool& bcorr);

	void	ConvertVtoQ( FVec &vfi, const FVec& vin);
	void	ConvertMtxVtoQ( FMtx &mtxfi, const FMtx& mtxin);

protected:
	void	AssembleMtx( FMtx &mtx, const GVect &vn, const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 );

private:
	FVec	mV;
	MGFloat	mc;
	MGFloat	mU2;

	GVect	mVnShock;
	GVect	mtabVn[DIM+1];
	FVec	mtabsV[DIM+1];
};
//////////////////////////////////////////////////////////////////////





template <Dimension DIM>
inline void DecompMtx<DIM>::Init( const FVec& var, const FVec tab[], const GVect tabn[])
{
	mV = var;
	mc = ::sqrt( FLOW_K*mV(1)/mV(0) );
	mU2 = EqnEuler<DIM>::SqrU(var);

	for ( MGSize i=0; i<=DIM; ++i)
	{
		mtabsV[i] = tab[i];
		mtabVn[i] = tabn[i];
	}

}



template <Dimension DIM>
inline void DecompMtx<DIM>::ConvertVtoQ( FVec& vfi, const FVec& vin)
{
	THROW_INTERNAL( "Not implemented !");
}

template <>
inline void DecompMtx<DIM_2D>::ConvertVtoQ( FVec& vfi, const FVec& vin)
{
	vfi(0) = vin(0);

	vfi(1) =   vin(0) * 0.5 * mU2
			 + vin(1)/( FLOW_K - 1.0) 
			 + vin(2) * mV(0) * mV(2)
			 + vin(3) * mV(0) * mV(3);

	vfi(2) =   vin(0) * mV(2)
			 + vin(2) * mV(0);

	vfi(3) =   vin(0) * mV(3)
			 + vin(3) * mV(0);
}

template <>
inline void DecompMtx<DIM_3D>::ConvertVtoQ( FVec& vfi, const FVec& vin)
{
	vfi(0) = vin(0);

	vfi(1) =   vin(0) * 0.5 * mU2
			 + vin(1)/( FLOW_K - 1.0) 
			 + vin(2) * mV(0) * mV(2)
			 + vin(3) * mV(0) * mV(3)
			 + vin(4) * mV(0) * mV(4);

	vfi(2) =   vin(0) * mV(2)
			 + vin(2) * mV(0);

	vfi(3) =   vin(0) * mV(3)
			 + vin(3) * mV(0);

	vfi(4) =   vin(0) * mV(4)
			 + vin(4) * mV(0);
}


template <>
inline void DecompMtx<DIM_2D>::ConvertMtxVtoQ( FMtx &mtxfi, const FMtx& mtxin)
{
	for ( MGSize i=0; i<mtxin.NCols(); ++i)
	{
		mtxfi(0,i) =   mtxin(0,i);

		mtxfi(1,i) =   mtxin(0,i) * 0.5 * mU2
				+ mtxin(1,i)/( FLOW_K - 1.0) 
				+ mtxin(2,i) * mV(0) * mV(2)
				+ mtxin(3,i) * mV(0) * mV(3);

		mtxfi(2,i) =   mtxin(0,i) * mV(2)
				+ mtxin(2,i) * mV(0);

		mtxfi(3,i) =   mtxin(0,i) * mV(3)
				+ mtxin(3,i) * mV(0);

	}
}


template <>
inline void DecompMtx<DIM_3D>::ConvertMtxVtoQ( FMtx &mtxfi, const FMtx& mtxin)
{
	for ( MGSize i=0; i<mtxin.NCols(); ++i)
	{
		mtxfi(0,i) =   mtxin(0,i);

		mtxfi(1,i) =   mtxin(0,i) * 0.5 * mU2
				+ mtxin(1,i)/( FLOW_K - 1.0) 
				+ mtxin(2,i) * mV(0) * mV(2)
				+ mtxin(3,i) * mV(0) * mV(3)
				+ mtxin(4,i) * mV(0) * mV(4);

		mtxfi(2,i) =   mtxin(0,i) * mV(2)
				+ mtxin(2,i) * mV(0);

		mtxfi(3,i) =   mtxin(0,i) * mV(3)
				+ mtxin(3,i) * mV(0);

		mtxfi(4,i) =   mtxin(0,i) * mV(4)
				+ mtxin(4,i) * mV(0);
	}
}





template <Dimension DIM>
void DecompMtx<DIM>::AssembleMtx( FMtx &mtx, const GVect &vn, const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 )
{
	THROW_INTERNAL( "Not implemented");
}


template <>
inline void DecompMtx<DIM_2D>::AssembleMtx( FMtx &mtx, const GVect &vn, 
										   const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 )
{
	const MGFloat l2m3 = l2 - l3;
	const MGFloat l2p3 = l2 + l3;
	const MGFloat l2p3m1 = 0.5*l2p3 - l1;
	const MGFloat ltmp1 =  0.5*mV(0)*l2m3;

	mtx.Resize( EQSIZE, EQSIZE);
	//mtx.Init( 0.0);


	mtx(0,0) = l1;
	mtx(0,1) = 0.5*(l2p3 - 2*l1)/(mc*mc);
	mtx(0,2) = vn.cX()* ltmp1 /mc;
	mtx(0,3) = vn.cY()* ltmp1 /mc;

	mtx(1,0) = 0.0;
	mtx(1,1) = 0.5*l2p3;
	mtx(1,2) = vn.cX()*mc* ltmp1;
	mtx(1,3) = vn.cY()*mc* ltmp1;

	mtx(2,0) = 0.0;
	mtx(2,1) = 0.5*vn.cX()*l2m3 / ( mc*mV(0) );
	mtx(2,2) = l1*vn.cY()*vn.cY() + 0.5*vn.cX()*vn.cX()*l2p3;
	mtx(2,3) = vn.cX()*vn.cY()*l2p3m1;

	mtx(3,0) = 0.0;
	mtx(3,1) = 0.5*vn.cY()*l2m3 / ( mc*mV(0) );
	mtx(3,2) = vn.cX()*vn.cY()*l2p3m1;
	mtx(3,3) = l1*vn.cX()*vn.cX() + 0.5*vn.cY()*vn.cY()*l2p3;

	//mtx.Resize( DIM_2D+2, DIM_2D+2);
	//mtx.Init( 0.0);

	//mtx(0,0) = l1;
	//mtx(0,1) = 0.5*(l2+l3 - 2*l1)/(mc*mc);
	//mtx(0,2) = 0.5*vn.X()*mV(0)*(l2 - l3)/mc;
	//mtx(0,3) = 0.5*vn.Y()*mV(0)*(l2 - l3)/mc;

	//mtx(1,1) = 0.5*( l2 + l3);
	//mtx(1,2) = 0.5*vn.X()*mc*mV(0)*( l2 - l3);
	//mtx(1,3) = 0.5*vn.Y()*mc*mV(0)*( l2 - l3);

	//mtx(2,1) = 0.5*vn.X()*( l2 - l3) / ( mc*mV(0) );
	//mtx(2,2) = l1*vn.Y()*vn.Y() + 0.5*vn.X()*vn.X()*( l2 + l3);
	//mtx(2,3) = vn.X()*vn.Y()*( -l1 + 0.5*( l2 + l3) );

	//mtx(3,1) = 0.5*vn.Y()*( l2 - l3) / ( mc*mV(0) );
	//mtx(3,2) = vn.X()*vn.Y()*( -l1 + 0.5*( l2 + l3) );
	//mtx(3,3) = l1*vn.X()*vn.X() + 0.5*vn.Y()*vn.Y()*( l2 + l3);
}


template <>
inline void DecompMtx<DIM_3D>::AssembleMtx( FMtx &mtx, const GVect &vn, 
										   const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 )
{
	const MGFloat l2m3 = l2 - l3;
	const MGFloat l2p3 = l2 + l3;
	const MGFloat l2p3m1 = 0.5*l2p3 - l1;
	const MGFloat romc = mc * mV(0);

	const MGFloat ltmp1 =  0.5*mV(0)*l2m3;

	mtx.Resize( EQSIZE, EQSIZE);
	//mtx.Init( 0.0);

	mtx(0,0) = l1;
	mtx(0,1) = 0.5*(l2p3 - 2*l1)/(mc*mc);
	mtx(0,2) = vn.cX()* ltmp1/mc;
	mtx(0,3) = vn.cY()* ltmp1/mc;
	mtx(0,4) = vn.cZ()* ltmp1/mc;

	mtx(1,0) = 0.0;
	mtx(1,1) = 0.5*l2p3;
	mtx(1,2) = 0.5*vn.cX()*romc*l2m3;
	mtx(1,3) = 0.5*vn.cY()*romc*l2m3;
	mtx(1,4) = 0.5*vn.cZ()*romc*l2m3;

	mtx(2,0) = 0.0;
	mtx(2,1) = 0.5*vn.cX()*l2m3 / romc;
	mtx(2,2) = l1*( vn.cY()*vn.cY() + vn.cZ()*vn.cZ() ) + 0.5*vn.cX()*vn.cX()*l2p3;
	mtx(2,3) = vn.cX()*vn.cY()*l2p3m1;
	mtx(2,4) = vn.cX()*vn.cZ()*l2p3m1;

	mtx(3,0) = 0.0;
	mtx(3,1) = 0.5*vn.cY()*l2m3 / romc;
	mtx(3,2) = vn.cX()*vn.cY()*l2p3m1;
	mtx(3,3) = l1*( vn.cX()*vn.cX() + vn.cZ()*vn.cZ() ) + 0.5*vn.cY()*vn.cY()*l2p3;
	mtx(3,4) = vn.cY()*vn.cZ()*l2p3m1;

	mtx(4,0) = 0.0;
	mtx(4,1) = 0.5*vn.cZ()*l2m3 / romc;
	mtx(4,2) = vn.cX()*vn.cZ()*l2p3m1;
	mtx(4,3) = vn.cY()*vn.cZ()*l2p3m1;
	mtx(4,4) = l1*( vn.cX()*vn.cX() + vn.cY()*vn.cY() ) + 0.5*vn.cZ()*vn.cZ()*l2p3;

	//mtx(0,0) = l1;
	//mtx(0,1) = 0.5*(l2+l3 - 2*l1)/(mc*mc);
	//mtx(0,2) = 0.5*vn.X()*mV(0)*(l2 - l3)/mc;
	//mtx(0,3) = 0.5*vn.Y()*mV(0)*(l2 - l3)/mc;
	//mtx(0,4) = 0.5*vn.Z()*mV(0)*(l2 - l3)/mc;

	//mtx(1,1) = 0.5*( l2 + l3);
	//mtx(1,2) = 0.5*vn.X()*mc*mV(0)*( l2 - l3);
	//mtx(1,3) = 0.5*vn.Y()*mc*mV(0)*( l2 - l3);
	//mtx(1,4) = 0.5*vn.Z()*mc*mV(0)*( l2 - l3);

	//mtx(2,1) = 0.5*vn.X()*( l2 - l3) / ( mc*mV(0) );
	//mtx(2,2) = l1*( vn.Y()*vn.Y() + vn.Z()*vn.Z() ) + 0.5*vn.X()*vn.X()*( l2 + l3);
	//mtx(2,3) = vn.X()*vn.Y()*( -l1 + 0.5*( l2 + l3) );
	//mtx(2,4) = vn.X()*vn.Z()*( -l1 + 0.5*( l2 + l3) );

	//mtx(3,1) = 0.5*vn.Y()*( l2 - l3) / ( mc*mV(0) );
	//mtx(3,2) = vn.X()*vn.Y()*( -l1 + 0.5*( l2 + l3) );
	//mtx(3,3) = l1*( vn.X()*vn.X() + vn.Z()*vn.Z() ) + 0.5*vn.Y()*vn.Y()*( l2 + l3);
	//mtx(3,4) = vn.Y()*vn.Z()*( -l1 + 0.5*( l2 + l3) );

	//mtx(4,1) = 0.5*vn.Z()*( l2 - l3) / ( mc*mV(0) );
	//mtx(4,2) = vn.X()*vn.Z()*( -l1 + 0.5*( l2 + l3) );
	//mtx(4,3) = vn.Y()*vn.Z()*( -l1 + 0.5*( l2 + l3) );
	//mtx(4,4) = l1*( vn.X()*vn.X() + vn.Y()*vn.Y() ) + 0.5*vn.Z()*vn.Z()*( l2 + l3);
}



template <Dimension DIM>
inline void DecompMtx<DIM>::Calc( FMtx &smtxKp, FMtx &smtxKm, const GVect &vn, const MGFloat& e, bool& bcorr)
{
	MGFloat l[3], lp[3], lm[3];
	MGFloat q = 0.0;

	for ( MGSize k=0; k<DIM; ++k)
		q += mV(2+k)*vn.cX(k);

	l[0] = q;
	l[1] = q + mc;
	l[2] = q - mc;


	for ( MGSize i=0; i<3; ++i)
	{
		//MGFloat ls = ::fabs( l[i]);
		//if ( ls < e)
		//{
		//	//ls = (ls*ls/e + e);
		//	ls = e;
		//	bcorr = true;
		//}
		////ls = fabs( l[i]); 

		//lp[i] = 0.5*( l[i] + ls );
		//lm[i] = 0.5*( l[i] - ls );

		lp[i] = MAX( l[i],  e );
		lm[i] = MIN( l[i], -e );
	}

	AssembleMtx( smtxKp, vn, lp[0], lp[1], lp[2] );
	AssembleMtx( smtxKm, vn, lm[0], lm[1], lm[2] );

	//AssembleMtx( smtxKp, vn, MAX(l[0],0.0), MAX(l[1],0.0), MAX(l[2],0.0) );
	//AssembleMtx( smtxKm, vn, MIN(l[0],0.0), MIN(l[1],0.0), MIN(l[2],0.0) );
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __DECOMPMTX_H__
