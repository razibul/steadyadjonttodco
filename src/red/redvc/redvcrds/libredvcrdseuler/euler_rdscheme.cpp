#include "Eigen"

#include "euler_rdschemetest.h"
#include "euler_rdschememtx.h"
#include "euler_rdschemehe.h"
#include "euler_rdschemepret.h"

#include "euler_rdschemecrdmtx.h"
#include "euler_rdschemecrdpret.h"

#include "redvc/redvcrds/libredvcrdscommon/mtxldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/mtxnscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/mtxldanscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/crdmtxldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/crdmtxnscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/crdmtxldanscheme.h"

#include "redvc/redvcrds/libredvcrdscommon/scldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/scnscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/scpsischeme.h" 
#include "redvc/redvcrds/libredvcrdscommon/crdscldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/crdscnscheme.h"

#include "libcorecommon/factory.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <MGSize SIZE>
inline void Eigen_Eigen( SMatrix<SIZE>& mtxL, SMatrix<SIZE>& mtxLI, SVector<SIZE>& vecD, const SMatrix<SIZE>& mtxA)
{
	using namespace Eigen;

	typedef Matrix<MGFloat,SIZE,SIZE> EigenMatrix;

	EigenMatrix emtxA;

	for ( MGSize i=0; i<SIZE; ++i)
		for ( MGSize j=0; j<SIZE; ++j)
			emtxA(i,j) = mtxA(i,j);

	EigenSolver<MatrixXd> es(emtxA);

	EigenMatrix P = es.eigenvectors().real();
	EigenMatrix D = es.eigenvalues().real().asDiagonal();

	EigenMatrix Pi = P.inverse();
	emtxA = Pi.transpose() * D * Pi;	

	for ( MGSize i=0; i<SIZE; ++i)
	{
		for ( MGSize j=0; j<SIZE; ++j)
		{
			mtxL(i,j) = Pi(i,j);
			mtxLI(i,j) = P(i,j);
		}
		vecD[i] = D(i,i);
	}
}



template void Eigen_Eigen<3>( SMatrix<3>& mtxL, SMatrix<3>& mtxLI, SVector<3>& vecD, const SMatrix<3>& mtxA);
template void Eigen_Eigen<4>( SMatrix<4>& mtxL, SMatrix<4>& mtxLI, SVector<4>& vecD, const SMatrix<4>& mtxA);

void init_FlowFuncEulerRDS()
{


	static ConcCreator< 
		MGString, 
		RDSchemeCRDPreT< Geom::DIM_2D, CRDMtxNscheme<Geom::DIM_2D>, CRDScNscheme<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSCrdPreTNN2D( "FLOWFUNC_2D_EULER_RDS_CRDPRET_N_N");




	static ConcCreator< 
		MGString, 
		RDSchemeCRDPreT< Geom::DIM_2D, CRDMtxLDAscheme<Geom::DIM_2D>, CRDScLDAscheme<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSCrdPreTLDALDA2D( "FLOWFUNC_2D_EULER_RDS_CRDPRET_LDA_LDA");

	static ConcCreator< 
		MGString, 
		RDSchemeCRDPreT< Geom::DIM_3D, CRDMtxLDAscheme<Geom::DIM_3D>, CRDScLDAscheme<DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSCrdPreTLDALDA3D( "FLOWFUNC_3D_EULER_RDS_CRDPRET_LDA_LDA");




	static ConcCreator< 
		MGString, 
		RDSchemeCRDMtx< Geom::DIM_2D, CRDMtxNscheme<Geom::DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSCrdN2D( "FLOWFUNC_2D_EULER_RDS_CRDMTX_N");

	static ConcCreator< 
		MGString, 
		RDSchemeCRDMtx< Geom::DIM_2D, CRDMtxLDANscheme<Geom::DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSCrdLDAN2D( "FLOWFUNC_2D_EULER_RDS_CRDMTX_LDAN");


	static ConcCreator< 
		MGString, 
		RDSchemeCRDMtx< Geom::DIM_2D, CRDMtxLDAscheme<Geom::DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSCrdLDA2D( "FLOWFUNC_2D_EULER_RDS_CRDMTX_LDA");

	static ConcCreator< 
		MGString, 
		RDSchemeCRDMtx< Geom::DIM_3D, CRDMtxLDAscheme<Geom::DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSCrdLDA3D( "FLOWFUNC_3D_EULER_RDS_CRDMTX_LDA");





	static ConcCreator< 
		MGString, 
		RDSchemePreT< Geom::DIM_2D, MtxLDANscheme<Geom::DIM_2D>, ScPSIscheme<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSPreTLDANPSI2D( "FLOWFUNC_2D_EULER_RDS_PRET_LDAN_PSI");

	static ConcCreator< 
		MGString, 
		RDSchemePreT< Geom::DIM_2D, MtxLDAscheme<Geom::DIM_2D>, ScPSIscheme<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSPreTLDAPSI2D( "FLOWFUNC_2D_EULER_RDS_PRET_LDA_PSI");

	static ConcCreator< 
		MGString, 
		RDSchemePreT< Geom::DIM_2D, MtxLDAscheme<Geom::DIM_2D>, ScLDAscheme<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSPreTLDALDA2D( "FLOWFUNC_2D_EULER_RDS_PRET_LDA_LDA");


	static ConcCreator< 
		MGString, 
		RDSchemePreT< Geom::DIM_3D, MtxLDAscheme<Geom::DIM_3D>, ScLDAscheme<DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSPreTLDALDA3D( "FLOWFUNC_3D_EULER_RDS_PRET_LDA_LDA");

	static ConcCreator< 
		MGString, 
		RDSchemePreT< Geom::DIM_3D, MtxLDAscheme<Geom::DIM_3D>, ScPSIscheme<DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSPreTLDAPSI3D( "FLOWFUNC_3D_EULER_RDS_PRET_LDA_PSI");




	static ConcCreator< 
		MGString, 
		RDSchemeTest< Geom::DIM_2D, MtxLDAscheme<Geom::DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSTestLDA2D( "FLOWFUNC_2D_EULER_RDS_TEST_LDA");


	static ConcCreator< 
		MGString, 
		RDSchemeHE< Geom::DIM_2D, MtxNscheme<Geom::DIM_2D>, ScNscheme<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSHENN2D( "FLOWFUNC_2D_EULER_RDS_HE_N_N");


	static ConcCreator< 
		MGString, 
		RDSchemeHE< Geom::DIM_2D, MtxLDAscheme<Geom::DIM_2D>, ScNscheme<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSHELDAN2D( "FLOWFUNC_2D_EULER_RDS_HE_LDA_N");


	static ConcCreator< 
		MGString, 
		RDSchemeHE< Geom::DIM_2D, MtxLDAscheme<Geom::DIM_2D>, ScPSIscheme<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSHELDAPSI2D( "FLOWFUNC_2D_EULER_RDS_HE_LDA_PSI");


	static ConcCreator< 
		MGString, 
		RDSchemeHE< Geom::DIM_2D, MtxLDANscheme<Geom::DIM_2D>, ScPSIscheme<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSHELDANPSI2D( "FLOWFUNC_2D_EULER_RDS_HE_LDAN_PSI");



	static ConcCreator< 
		MGString, 
		RDSchemeHE< Geom::DIM_2D, MtxLDAscheme<Geom::DIM_2D>, ScLDAscheme<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSHELDALDA2D( "FLOWFUNC_2D_EULER_RDS_HE_LDA_LDA");


	static ConcCreator< 
		MGString, 
		RDSchemeMtx< Geom::DIM_2D, MtxLDAscheme<Geom::DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSLDA2D( "FLOWFUNC_2D_EULER_RDS_MTX_LDA");


	static ConcCreator< 
		MGString, 
		RDSchemeMtx< Geom::DIM_3D, MtxLDAscheme<Geom::DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSLDA3D( "FLOWFUNC_3D_EULER_RDS_MTX_LDA");


	static ConcCreator< 
		MGString, 
		RDSchemeMtx< Geom::DIM_2D, MtxNscheme<Geom::DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSN2D( "FLOWFUNC_2D_EULER_RDS_MTX_N");

	static ConcCreator< 
		MGString, 
		RDSchemeMtx< Geom::DIM_3D, MtxNscheme<Geom::DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSN3D( "FLOWFUNC_3D_EULER_RDS_MTX_N");


	static ConcCreator< 
		MGString, 
		RDSchemeMtx< Geom::DIM_2D, MtxLDANscheme<Geom::DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSLDAN2D( "FLOWFUNC_2D_EULER_RDS_MTX_LDAN");

	static ConcCreator< 
		MGString, 
		RDSchemeMtx< Geom::DIM_3D, MtxLDANscheme<Geom::DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SIZE> 
	>	gCreatorFlowFuncEulerRDSLDAN3D( "FLOWFUNC_3D_EULER_RDS_MTX_LDAN");


}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

