#ifndef __EULERRDSCHEMEMTX_H__
#define __EULERRDSCHEMEMTX_H__



#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"
#include "redvc/libredvccommon/flowfunc.h"

#include "decompmtx.h"

#include "libredphysics/eulerequations.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
// class RDSchemeMtx
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class MTXRDS>
class RDSchemeMtx : public FlowFunc<DIM, EQN_EULER, EquationDef<DIM,EQN_EULER>::SIZE>
{
	enum { EQSIZE = EquationDef<DIM,EQN_EULER>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_EULER>::AUXSIZE };

	typedef Vect<DIM>									GVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FMtx	FMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>				CFCell;

public:
	RDSchemeMtx()	{}
	RDSchemeMtx( const FVec& var) : mRefVar(var)	{}

	virtual void	Init( const Physics<DIM,EQN_EULER>& physics);

	virtual void	CalcResiduum( const CFCell& fcell, FVec tabres[]);
	virtual void	CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

	void	ApplyPrecond( FVec& res, const FVec& vsimpl )	{}

protected:
	void	Initialize( const CFCell& fcell);
	void	FinalizeRes( FVec tabres[]);
	void	FinalizeResJacob( FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

private:
	DecompMtx<DIM>	mDecomp;

	FVec	mRefVar;
	FVec	mvarZm, mvarVm;
	FVec	mtabsV[DIM+1];

	FMtx	mtabKp[DIM+1];
	FMtx	mtabKm[DIM+1];
	FMtx	mtabK[DIM+1];

	FVec	mtabsFi[DIM+1];
	FMtx	mmtxsJ[DIM+1][DIM+1];

	struct RDSCalc : public MTXRDS {} calc;

};
//////////////////////////////////////////////////////////////////////





template <Dimension DIM, class MTXRDS>
void RDSchemeMtx<DIM,MTXRDS>::Init( const Physics<DIM,EQN_EULER>& physics)
{
	physics.DimToUndim( mRefVar, physics.cRefVar(), physics.cRefVar() );
}


template <Dimension DIM, class MTXRDS>
inline void RDSchemeMtx<DIM,MTXRDS>::Initialize( const CFCell& fcell)
{
	FVec	var;

	//mRefVar.Write();
	//THROW_INTERNAL( "STOP");

	//MGFloat	tabv[3] = {2,2,1};
	//Vect<MGFloat,DIM> vvv = fcell.GradVar( tabv);


	mvarZm.Init( 0.0);
	mvarVm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
	{
		var = EqnEuler<DIM>::ConservToZ( fcell.cVar(i) );
		var(EquationDef<DIM,EQN_EULER>::ID_RHO) = max( 1.0e-10, var(EquationDef<DIM,EQN_EULER>::ID_RHO) );
		var(EquationDef<DIM,EQN_EULER>::ID_P)   = max( 1.0e-10, var(EquationDef<DIM,EQN_EULER>::ID_P) );

		mvarZm += var;
		mtabsV[i] = var;
	}

	mvarZm /= (MGFloat)(DIM+1);
	mvarVm = EqnEuler<DIM>::ZToSimp( mvarZm);


	FMtx	smtxM_VZ;
	EqnEuler<DIM>::CJacobVZ( smtxM_VZ, mvarZm);

	for ( MGSize i=0; i<=DIM; ++i)
		mtabsV[i] = smtxM_VZ * mtabsV[i];

	GVec	tabVn[DIM+1];
	for ( MGSize i=0; i<=DIM; ++i)
		tabVn[i] = fcell.Vn(i);

	mDecomp.Init( mvarVm, mtabsV, tabVn);

	FMtx	smtxAp, smtxAm;
	MGFloat	vnmod;
	GVec	vn;
	bool	bc;

	for ( MGSize i=0; i<=DIM; ++i)
	{
		vn = fcell.Vn(i).versor();

		mDecomp.Calc( smtxAp, smtxAm, vn, 1.0e-9, bc );
		//mDecomp.Calc( smtxAp, smtxAm, vn, 1.0e-7, bc );
		//mDecomp.Calc( smtxAp, smtxAm, vn, 0, bc );

		vnmod = fcell.Vn(i).module() / (MGFloat)DIM;


		MtxMultMtxD( mtabKp[i], smtxAp, vnmod);
		MtxMultMtxD( mtabKm[i], smtxAm, vnmod);
		//mtabKp[i] = vnmod * smtxAp;
		//mtabKm[i] = vnmod * smtxAm;

	}
}


template <Dimension DIM, class MTXRDS>
inline void RDSchemeMtx<DIM,MTXRDS>::FinalizeRes( FVec tabres[])		
{
	for ( MGSize i=0; i<=DIM; ++i)
		mDecomp.ConvertVtoQ( tabres[i], mtabsFi[i]);
}

template <Dimension DIM, class MTXRDS>
inline void RDSchemeMtx<DIM,MTXRDS>::FinalizeResJacob( FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
	for ( MGSize i=0; i<=DIM; ++i)
	{
		mDecomp.ConvertVtoQ( tabres[i], mtabsFi[i]);

		for ( MGSize k=0; k<=DIM; ++k)
			mDecomp.ConvertMtxVtoQ( mtxres[i][k], mmtxsJ[i][k]);
	}
}


template <Dimension DIM, class MTXRDS>
inline void RDSchemeMtx<DIM,MTXRDS>::CalcResiduum( const CFCell& fcell, FVec tabres[])		
{

	Initialize( fcell);

	calc.Calc( EQSIZE, mtabKp, mtabKm, mtabsV, mtabsFi);

	FinalizeRes( tabres);

	//for ( MGSize k=0; k<=DIM; ++k)
	//{
	//	FVec varQ = fcell.cVar(k);
	//	FVec varZ = EqnEuler<DIM>::ConservToZ( varQ);
	//	FVec varV = mtabsV[k] + mvarVm; //EqnEuler<DIM>::ConservToSimp( varQ);

	//	//FMtx	smtxM_VZ;
	//	//EqnEuler<DIM>::CJacobVZ( smtxM_VZ, mvarZm);
	//	//FVec varV = smtxM_VZ *varZ + mvarVm;


	//	FMtx  mtxWQ, mtxWV, mtxVZ, mtxZQ;
	//	EqnEuler<DIM>::CJacobWV( mtxWV, varV );
	//	EqnEuler<DIM>::CJacobVZ( mtxVZ, varZ );
	//	EqnEuler<DIM>::CJacobZQ( mtxZQ, varZ );

	//	mtxWQ = mtxWV * mtxVZ * mtxZQ;

	//	FMtx  mtxQW, mtxVW, mtxZV, mtxQZ;
	//	EqnEuler<DIM>::CJacobVW( mtxVW, varV );
	//	EqnEuler<DIM>::CJacobZV( mtxZV, varZ );
	//	EqnEuler<DIM>::CJacobQZ( mtxQZ, varZ );

	//	mtxQW = mtxQZ * mtxZV * mtxVW;

	//	MGFloat	q = ::sqrt( EqnEuler<DIM>::SqrU( varV) );
	//	MGFloat	c = ::sqrt( FLOW_K*varV(1)/varV(0) );
	//	MGFloat	M = q/c;

	//	MGFloat	beta = max( /*P_LLR_EPS*/0.001, ::sqrt( ::fabs(M*M-1) ) );
	//	MGFloat	chi = beta/max(M,1.0);

	//	FMtx mtxP;
	//	EqnEuler<DIM>::PrecLLR( mtxP, varV, beta, chi);

	//	mtxP = mtxQW * mtxP * mtxWQ;

	//	tabres[k] = mtxP * tabres[k];
	//	//cout << "QQQQ";

	//}
}



template <Dimension DIM, class MTXRDS>
inline void RDSchemeMtx<DIM,MTXRDS>::CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
	Initialize( fcell);

	calc.Calc( EQSIZE, mtabKp, mtabKm, mtabsV, mtabsFi, mmtxsJ);

	FinalizeResJacob( tabres, mtxres);

}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

	#endif // __EULERRDSCHEMEMTX_H__
