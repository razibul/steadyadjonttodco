#ifndef __EULERRDSCHEMETEST_H__
#define __EULERRDSCHEMETEST_H__

#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"

#include "libredphysics/eulerequations.h"

#include "euler_rdschememtx.h"
#include "euler_rdschemehe.h"

#include "redvc/libredvccommon/flowfunc.h"

#include "redvc/redvcrds/libredvcrdscommon/scldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/mtxldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/scnscheme.h"
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <MGSize SIZE>
inline void Eigen_Eigen( SMatrix<SIZE>& mtxL, SMatrix<SIZE>& mtxLI, SVector<SIZE>& vecD, const SMatrix<SIZE>& mtxA);

template <class MATRIX, class VECTOR>
inline void Decomp_LWni( MATRIX& mtx, const VECTOR& vs, const Geom::Vect2D& vn)
{
	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(0,0) =   1;

	mtx(1,2) =   vn.cY();
	mtx(1,3) = - vn.cX();

	mtx(2,1) =   1;
	mtx(2,2) =   vn.cX();
	mtx(2,3) =   vn.cY();

	mtx(3,1) =   1;
	mtx(3,2) = - vn.cX();
	mtx(3,3) = - vn.cY();
}

template <class MATRIX, class VECTOR>
inline void Decomp_LWniI( MATRIX& mtx, const VECTOR& vs, const Geom::Vect2D& vn)
{
	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(0,0) =   1;

	mtx(1,2) =   0.5;
	mtx(1,3) =   0.5;

	mtx(2,1) =   vn.cY();
	mtx(2,2) =   0.5*vn.cX();
	mtx(2,3) = - 0.5*vn.cX();

	mtx(3,1) = - vn.cX();
	mtx(3,2) =   0.5*vn.cY();
	mtx(3,3) = - 0.5*vn.cY();
}


template <class MATRIX, class VECTOR>
inline void Decomp_subLWni( MATRIX& mtx, const VECTOR& vs, const Geom::Vect2D& vn)
{
	mtx.Resize( 3, 3);
	mtx.Init( 0);

	mtx(0,1) =   vn.cY();
	mtx(0,2) = - vn.cX();

	mtx(1,0) =   1;
	mtx(1,1) =   vn.cX();
	mtx(1,2) =   vn.cY();

	mtx(2,0) =   1;
	mtx(2,1) = - vn.cX();
	mtx(2,2) = - vn.cY();
}

template <class MATRIX, class VECTOR>
inline void Decomp_subLWniI( MATRIX& mtx, const VECTOR& vs, const Geom::Vect2D& vn)
{
	mtx.Resize( 3, 3);
	mtx.Init( 0);

	mtx(0,1) =   0.5;
	mtx(0,2) =   0.5;

	mtx(1,0) =   vn.cY();
	mtx(1,1) =   0.5*vn.cX();
	mtx(1,2) = - 0.5*vn.cX();

	mtx(2,0) = - vn.cX();
	mtx(2,1) =   0.5*vn.cY();
	mtx(2,2) = - 0.5*vn.cY();
}



template <class MATRIX>
inline void Decomp_subM_XW( MATRIX& mtx, const MGFloat& M, const MGFloat& c, const MGFloat& beta )
{
	mtx.Resize( 3, 3);
	mtx.Init( 0);

	mtx(0,0) =   M*c;
	mtx(0,1) =   M*c;

	mtx(1,0) =   beta;
	mtx(1,2) =   1.0;

	mtx(2,0) =   beta;
	mtx(2,2) = - 1.0;
}

template <class MATRIX>
inline void Decomp_subM_WX( MATRIX& mtx, const MGFloat& M, const MGFloat& c, const MGFloat& beta )
{
	mtx.Resize( 3, 3);
	mtx.Init( 0);

	mtx(0,1) =   0.5*beta;
	mtx(0,2) =   0.5*beta;

	mtx(1,0) =   1.0/(M*c);
	mtx(1,1) = - 0.5/beta;
	mtx(1,2) = - 0.5/beta;

	mtx(2,1) =   0.5;
	mtx(2,2) = - 0.5;
}



template <class MATRIX>
inline void Decomp_subPAXni( MATRIX& mtx, const MGFloat& M, const MGFloat& beta, const Geom::Vect2D& vn)
{
	const MGFloat pp = 0.5*( M*M + beta*beta - 1.0 ) / (beta*beta);
	const MGFloat pm = 0.5*( M*M - beta*beta - 1.0 ) / (beta*beta);

	mtx.Resize( 2, 2);
	mtx.Init( 0);

	mtx(0,0) =   vn.cX()*pp + vn.cY() / beta;
	mtx(1,1) =   vn.cX()*pp - vn.cY() / beta;

	mtx(1,0) = mtx(0,1) = vn.cX()*pm;
}

template <class MATRIX>
inline void Decomp_subPAWni( MATRIX& mtx, const MGFloat& M, const Geom::Vect2D& vn)
{
	mtx.Resize( 3, 3);
	mtx.Init( 0);

	mtx(0,0) = mtx(1,1) = mtx(2,2) =   vn.cX() * M;
	mtx(0,1) = mtx(1,0) = vn.cX(); 
	mtx(0,2) = mtx(2,0) = vn.cY();
}

template <class MATRIX>
inline void Decomp_Pvlr( MATRIX& mtx, const MGFloat& M, const MGFloat& beta)
{
	const MGFloat f = 1.0;
	const MGFloat h = 1.0;
	const MGFloat beta2 = 1.0/(beta*beta);

	mtx.Resize( 3, 3);
	mtx.Init( 0);

	mtx(0,0) =   f*M*M*beta2;
	mtx(1,1) =   h + f*beta2;
	mtx(2,2) =   f;

	mtx(1,0) = mtx(0,1) = - f*M*beta2;
}


template <class MATRIX>
inline void Decomp_PvlrI( MATRIX& mtx, const MGFloat& M, const MGFloat& beta)
{
	const MGFloat f = 1.0;
	const MGFloat h = 1.0;

	mtx.Resize( 3, 3);
	mtx.Init( 0);

	mtx(0,0) =   (beta*beta*h + f) / (f*h*M*M);
	mtx(1,1) =   1/h;
	mtx(2,2) =   1/f;

	mtx(1,0) = mtx(0,1) = 1.0/(M*h);
}


//////////////////////////////////////////////////////////////////////
// class RDSchemeTest
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class MTXRDS>
class RDSchemeTest : public FlowFunc<DIM, EQN_EULER, EquationDef<DIM,EQN_EULER>::SIZE>
{
	enum { EQSIZE = EquationDef<DIM,EQN_EULER>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_EULER>::AUXSIZE };

	typedef Vect<DIM>									GVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FMtx	FMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>				CFCell;

public:
	RDSchemeTest()	{}
	RDSchemeTest( const FVec& var) : mRefVar(var)	{}

	virtual void	Init( const Physics<DIM,EQN_EULER>& physics);

	virtual void	CalcResiduum( const CFCell& fcell, FVec tabres[]);
	virtual void	CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

	void	ApplyPrecond( FVec& res, const FVec& vsimpl )	{}


protected:
	void	Residuum_MtxW( const CFCell& fcell, FVec tabres[]);
	void	Residuum_S_MtxW( const CFCell& fcell, FVec tabres[]);
	void	Residuum_PS_MtxW( const CFCell& fcell, FVec tabres[]);
	void	Residuum_PGenS_MtxW( const CFCell& fcell, FVec tabres[]);

protected:
	void	Initialize( const CFCell& fcell );

private:
	FVec	mRefVar;
	FVec	mvarZm, mvarVm;
	FVec	mtabsZ[DIM+1];

	FMtx	mtabKp[DIM+1];
	FMtx	mtabKm[DIM+1];
	FMtx	mtabK[DIM+1];

	FVec	mtabsFi[DIM+1];
	FMtx	mmtxsJ[DIM+1][DIM+1];


	SMatrix<DIM>	mmtxR;
	FMtx			mmtxM_WZ;
	FMtx			mmtxM_QW;
	FVec			mtabsW[DIM+1];
	FVec	mV;
	MGFloat	mc;

	struct RDSCalc : public MTXRDS {} calc;
};
//////////////////////////////////////////////////////////////////////





template <Dimension DIM, class MTXRDS>
void RDSchemeTest<DIM,MTXRDS>::Init( const Physics<DIM,EQN_EULER>& physics)
{
}

template <Dimension DIM, class MTXRDS>
inline void RDSchemeTest<DIM,MTXRDS>::Residuum_PGenS_MtxW( const CFCell& fcell, FVec tabres[])
{
	typedef ScLDAscheme<DIM>	TScScheme;
	typedef MtxLDAscheme<DIM>	TMtxScheme;

	typedef SVector<EQSIZE-1>	FSubVec;
	typedef SMatrix<EQSIZE-1>	FSubMtx;

	mvarZm.Init( 0.0);
	mvarVm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
		mvarZm += mtabsZ[i] = EqnEuler<DIM>::ConservToZ( fcell.cVar(i) );

	mvarZm /= (MGFloat)(DIM+1);
	
	mvarVm = EqnEuler<DIM>::ZToSimp( mvarZm);

	FMtx	smtxM_QZ, smtxM_ZV, smtxM_VW;
	EqnEuler<DIM>::CJacobQZ( smtxM_QZ, mvarZm);
	EqnEuler<DIM>::CJacobZV( smtxM_ZV, mvarZm);
	EqnEuler<DIM>::CJacobVW( smtxM_VW, mvarVm);

	mmtxM_QW = smtxM_QZ * smtxM_ZV * smtxM_VW;


	FMtx	smtxM_VZ, smtxM_WV, smtxM_XW;
	EqnEuler<DIM>::CJacobVZ( smtxM_VZ, mvarZm);
	EqnEuler<DIM>::CJacobWV( smtxM_WV, mvarVm);

	mmtxM_WZ = smtxM_WV * smtxM_VZ;

	EqnEuler<DIM>::RotMtx( mmtxR, mvarVm);

	for ( MGSize i=0; i<=DIM; ++i)
		mtabsW[i] = mmtxM_WZ * mtabsZ[i];

	MGFloat c = ::sqrt( FLOW_K*mvarVm(1)/mvarVm(0) );
	MGFloat q = sqrt( EqnEuler<DIM>::SqrU(mvarVm) );
	MGFloat M = q/c;


	FSubMtx	smtxAp, smtxAm, smtxP, smtxPI;
	MGFloat	vnmod;
	GVec	vn;
	MGFloat e = 1.0e-9;

	Decomp_Pvlr( smtxP, M, 1.0);

	const MGFloat theta = 0.001;	// <---------------------------------------------
	smtxP(0,0) += theta;
	smtxP(1,1) += theta;
	smtxP(2,2) += theta;

	smtxPI = smtxP;
	smtxPI.Invert();

	GVec tabvnn[DIM+1];

	for ( MGSize i=0; i<=DIM; ++i)
	{
		tabvnn[i] = GVec(0.,0.);
		for ( MGSize ir=0; ir<DIM; ++ir)
			for ( MGSize jr=0; jr<DIM; ++jr)
				tabvnn[i].rX(ir) += mmtxR(ir,jr) * fcell.Vn(i).cX(jr);
	}


	//////////////////////////////////////////////
	// scalar equations 

	MGFloat	tabk[DIM+1], tabfi[DIM+1], tabu[DIM+1];

	TMtxScheme	mtxcalc;

	for ( MGSize i=0; i<=DIM; ++i)
		tabk[i] = tabvnn[i].cX() * q / (MGFloat)DIM;

	//for ( MGSize neq=0; neq<2; ++neq)
	MGSize neq = 0;
	{
		for ( MGSize i=0; i<=DIM; ++i)
			tabu[i] = mtabsW[i](neq);

		TScScheme::Calc( tabk, tabu, tabfi);


		MGFloat fi = 0.0;
		for ( MGSize i=0; i<=DIM; ++i)
			fi += tabfi[i];

		//MGFloat sumbeta = 0.0;
		//MGFloat	tabbeta[DIM+1];
		//for ( MGSize i=0; i<=DIM; ++i)
		//{
		//	tabbeta[i] = SIGN( tabfi[i] * fi ) * min( fabs( tabfi[i]), fabs(fi) );
		//	sumbeta += tabbeta[i];
		//}
		//for ( MGSize i=0; i<=DIM; ++i)
		//	tabbeta[i] /= max( sumbeta, 1.0e-6);
		//
		//for ( MGSize i=0; i<=DIM; ++i)
		//	mtabsFi[i](neq) = tabbeta[i] * fi;

		for ( MGSize i=0; i<=DIM; ++i)
			mtabsFi[i](neq) = tabfi[i];

	}


	//////////////////////////////////////////////
	// matrix subsystem 

	FSubVec	tabsWsub[DIM+1];
	FSubMtx	tabKp[DIM+1];
	FSubMtx	tabKm[DIM+1];

	for ( MGSize i=0; i<=DIM; ++i)
	{
		GVec vnn = tabvnn[i].versor();

		FSubMtx mtxLsub, mtxLIsub;
		Decomp_subLWni( mtxLsub, mvarVm, vnn);
		Decomp_subLWniI( mtxLIsub, mvarVm, vnn);

		FSubMtx mtxPAWsub;
		FSubMtx mtxLX, mtxLXI;
		FSubVec vecDX, vecDXp, vecDXm;

		Decomp_subPAWni( mtxPAWsub, M, vnn);
		mtxPAWsub = c * smtxP * mtxPAWsub;

		//Eigen_( mtxLX, mtxLXI, vecDX, mtxPAWsub );
		//mtxLX.Write();
		//mtxLXI.Write();
		//vecDX.Write();

		Eigen_Eigen( mtxLX, mtxLXI, vecDX, mtxPAWsub );
		//mtxLX.Write();
		//mtxLXI.Write();
		//vecDX.Write();

		//cout << "l1 = " << vecDX[0] << endl;
		//cout << "l2 = " << vecDX[1] << endl;

		for ( MGSize k=0; k<3; ++k)
		{
			vecDXp(k) = MAX( vecDX(k),  e);
			vecDXm(k) = MIN( vecDX(k), -e);
		}


		FSubMtx mtxLp, mtxLm;
		mtxLp.Resize(EQSIZE-1,EQSIZE-1);
		mtxLp.Init(0.0);
		mtxLp(0,0) = vecDXp[0];
		mtxLp(1,1) = vecDXp[1];
		mtxLp(2,2) = vecDXp[2];

		mtxLm.Resize(EQSIZE-1,EQSIZE-1);
		mtxLm.Init(0.0);
		mtxLm(0,0) = vecDXm[0];
		mtxLm(1,1) = vecDXm[1];
		mtxLm(2,2) = vecDXm[2];


		smtxAp = mtxLXI * mtxLp * mtxLX;
		smtxAm = mtxLXI * mtxLm * mtxLX;

		vnmod = tabvnn[i].module() / (MGFloat)DIM;

		MtxMultMtxD( tabKp[i], smtxAp, vnmod);
		MtxMultMtxD( tabKm[i], smtxAm, vnmod);

		for ( MGSize k=0; k<EQSIZE-1; ++k)
			tabsWsub[i](k) = mtabsW[i](k+1);
	}

	FSubVec	tabsFisub[DIM+1];
	
	mtxcalc.Calc( EQSIZE-1, tabKp, tabKm, tabsWsub, tabsFisub);

	for ( MGSize i=0; i<=DIM; ++i)
	{
		FSubVec vtmp = tabsFisub[i];

		//FVec	var = mvarVm + smtxM_VZ * mtabsZ[i];
		FVec	var = EqnEuler<DIM>::ZToSimp( mtabsZ[i]);
		const MGFloat loc_c = ::sqrt( FLOW_K*var(1)/var(0) );
		const MGFloat loc_q = sqrt( EqnEuler<DIM>::SqrU(var) );
		const MGFloat loc_M = loc_q / loc_c;

		Decomp_Pvlr( smtxP, loc_M, 1.);

		vtmp = smtxP * smtxPI * vtmp;
		//vtmp =  smtxPI * vtmp;

		for ( MGSize neq=0; neq<EQSIZE-1; ++neq)
			mtabsFi[i](neq+1) = vtmp(neq);
	}

	for ( MGSize i=0; i<=DIM; ++i)
		tabres[i] = mmtxM_QW * mtabsFi[i];
}


template <Dimension DIM, class MTXRDS>
inline void RDSchemeTest<DIM,MTXRDS>::Residuum_PS_MtxW( const CFCell& fcell, FVec tabres[])
{
	typedef ScLDAscheme<DIM>	TScSchemeS;
	typedef ScLDAscheme<DIM>	TScSchemeH;
	typedef MtxLDAscheme<DIM>	TMtxScheme;

	typedef SVector<EQSIZE-1>	FSub3Vec;
	typedef SMatrix<EQSIZE-1>	FSub3Mtx;

	typedef SVector<EQSIZE-2>	FSub2Vec;
	typedef SMatrix<EQSIZE-2>	FSub2Mtx;


	mvarZm.Init( 0.0);
	mvarVm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
		mvarZm += mtabsZ[i] = EqnEuler<DIM>::ConservToZ( fcell.cVar(i) );

	mvarZm /= (MGFloat)(DIM+1);
	
	mvarVm = EqnEuler<DIM>::ZToSimp( mvarZm);

	FMtx	smtxM_QZ, smtxM_ZV, smtxM_VW;
	EqnEuler<DIM>::CJacobQZ( smtxM_QZ, mvarZm);
	EqnEuler<DIM>::CJacobZV( smtxM_ZV, mvarZm);
	EqnEuler<DIM>::CJacobVW( smtxM_VW, mvarVm);

	mmtxM_QW = smtxM_QZ * smtxM_ZV * smtxM_VW;


	FMtx	smtxM_VZ, smtxM_WV, smtxM_XW;
	EqnEuler<DIM>::CJacobVZ( smtxM_VZ, mvarZm);
	EqnEuler<DIM>::CJacobWV( smtxM_WV, mvarVm);

	mmtxM_WZ = smtxM_WV * smtxM_VZ;

	EqnEuler<DIM>::RotMtx( mmtxR, mvarVm);

	for ( MGSize i=0; i<=DIM; ++i)
		mtabsW[i] = mmtxM_WZ * mtabsZ[i];

	MGFloat c = ::sqrt( FLOW_K*mvarVm(1)/mvarVm(0) );
	MGFloat q = sqrt( EqnEuler<DIM>::SqrU(mvarVm) );


	FSub3Mtx	smtxAp, smtxAm;
	MGFloat	vnmod;
	GVec	vn;

	GVec tabvnn[DIM+1];

	for ( MGSize i=0; i<=DIM; ++i)
	{
		tabvnn[i] = GVec(0.,0.);
		for ( MGSize ir=0; ir<DIM; ++ir)
			for ( MGSize jr=0; jr<DIM; ++jr)
				tabvnn[i].rX(ir) += mmtxR(ir,jr) * fcell.Vn(i).cX(jr);
	}


	//////////////////////////////////////////////
	// scalar equation - entropy

	MGFloat	tabk[DIM+1], tabfi[DIM+1], tabu[DIM+1];

	TMtxScheme	mtxcalc;


	//MGFloat lam = q;
	{
			for ( MGSize i=0; i<=DIM; ++i)
				tabk[i] = tabvnn[i].cX() * q / (MGFloat)DIM;

			//for ( MGSize neq=0; neq<2; ++neq)
			MGSize neq = 0;
			{
				for ( MGSize i=0; i<=DIM; ++i)
					tabu[i] = mtabsW[i](neq);

				TScSchemeS::Calc( tabk, tabu, tabfi);

				for ( MGSize i=0; i<=DIM; ++i)
					mtabsFi[i](neq) = tabfi[i];
			}
	}

	//////////////////////////////////////////////
	// matrix subsystem + preconditioning

	FSub3Vec	tabsWsub[DIM+1];
	FSub3Mtx	tabKp[DIM+1];
	FSub3Mtx	tabKm[DIM+1];

	FSub3Mtx	smtxP, smtxPI;
	FSub3Mtx	smtxM3_XW, smtxM3_WX;
	FSub3Vec	tabsXsub[DIM+1];

	FSub2Mtx	tabKXp[DIM+1];
	FSub2Mtx	tabKXm[DIM+1];
	FSub2Vec	tabsXXsub[DIM+1];

	//mvarVm.Write();
	const MGFloat	e = 1.0e-9;
	const MGFloat	M = q / c;
	const MGFloat	beta = max( sqrt( fabs( 1.0 - M*M) ), 1.0e-3 );
	//const MGFloat	beta = 1.0;
	//const MGFloat	f = 1.0;

	//cout << "M = " << M << ", c = " << c << endl;

	Decomp_subM_XW( smtxM3_XW, M, c, beta);
	Decomp_subM_WX( smtxM3_WX, M, c, beta);

	Decomp_PvlrI( smtxPI, M, beta);

	//smtxP.Write();
	//smtxPI.Write();
	//(smtxPI*smtxP).Write();

	//cout << "smtxM3_XW = " << endl;
	//smtxM3_XW.Write();
	//cout << endl;

	//cout << "smtxM3_WX = " << endl;
	//smtxM3_WX.Write();
	//cout << endl;

	for ( MGSize i=0; i<=DIM; ++i)
	{
		for ( MGSize k=0; k<EQSIZE-1; ++k)
			tabsWsub[i](k) = mtabsW[i](k+1);

		tabsXsub[i] = smtxM3_XW * tabsWsub[i];
	}

	FSub3Vec	tabsFisub[DIM+1];


	//////////////////////////////////////////////
	for ( MGSize i=0; i<=DIM; ++i)
		tabk[i] = tabvnn[i].cX() * q / (MGFloat)DIM;

	//for ( MGSize neq=0; neq<2; ++neq)
	{
		MGSize neq = 0;
		for ( MGSize i=0; i<=DIM; ++i)
			tabu[i] = tabsXsub[i](neq);

		TScSchemeH::Calc( tabk, tabu, tabfi);

		for ( MGSize i=0; i<=DIM; ++i)
			tabsFisub[i](neq) = tabfi[i];
	}


	//////////////////////////////////////////////
	for ( MGSize i=0; i<=DIM; ++i)
	{
		GVec vnn = tabvnn[i].versor();

		//cout << "vnn = " << endl;
		//vnn.Write();
		//cout << endl;

		FSub2Mtx mtxPAXsub;
		FSub2Mtx mtxLX, mtxLXI;
		FSub2Vec vecDX, vecDXp, vecDXm;

		Decomp_subPAXni( mtxPAXsub, M, beta, vnn);

		mtxPAXsub *= M*c;

		//cout << "mtxPAXsub = " << endl;
		//mtxPAXsub.Write();
		//cout << endl;

		Eigen_( mtxLX, mtxLXI, vecDX, mtxPAXsub );

		//cout << "l1 = " << vecDX[0] << endl;
		//cout << "l2 = " << vecDX[1] << endl;

		for ( MGSize k=0; k<DIM; ++k)
		{
			vecDXp(k) = MAX( vecDX(k),  e);
			vecDXm(k) = MIN( vecDX(k), -e);
		}


		FSub2Mtx mtxLp, mtxLm;
		mtxLp.Resize(EQSIZE-2,EQSIZE-2);
		mtxLp.Init(0.0);
		mtxLp(0,0) = vecDXp[0];
		mtxLp(1,1) = vecDXp[1];

		mtxLm.Resize(EQSIZE-2,EQSIZE-2);
		mtxLm.Init(0.0);
		mtxLm(0,0) = vecDXm[0];
		mtxLm(1,1) = vecDXm[1];

		FSub2Mtx smtxAXp, smtxAXm;
		smtxAXp = mtxLXI * mtxLp * mtxLX;
		smtxAXm = mtxLXI * mtxLm * mtxLX;

		//cout << "smtxAXp = " << endl;
		//smtxAXp.Write();
		//cout << endl;
		//cout << "smtxAXm = " << endl;
		//smtxAXm.Write();
		//cout << endl;

		vnmod = tabvnn[i].module() / (MGFloat)DIM;

		MtxMultMtxD( tabKXp[i], smtxAXp, vnmod);
		MtxMultMtxD( tabKXm[i], smtxAXm, vnmod);
		//mtabKp[i] = vnmod * smtxAp;
		//mtabKm[i] = vnmod * smtxAm;

		for ( MGSize k=0; k<EQSIZE-2; ++k)
			tabsXXsub[i](k) = tabsXsub[i](k+1);
	}

	FSub2Vec	tabsFiXsub[DIM+1];
	
	mtxcalc.Calc( EQSIZE-2, tabKXp, tabKXm, tabsXXsub, tabsFiXsub);

	for ( MGSize i=0; i<=DIM; ++i)
		for ( MGSize neq=0; neq<EQSIZE-2; ++neq)
			tabsFisub[i](neq+1) = tabsFiXsub[i](neq);


	for ( MGSize i=0; i<=DIM; ++i)
	{
		FSub3Vec vtmp = smtxM3_WX * tabsFisub[i];

		//FVec	var = mvarVm + smtxM_VZ * mtabsZ[i];
		FVec	var = EqnEuler<DIM>::ZToSimp( mtabsZ[i]);
		const MGFloat loc_c = ::sqrt( FLOW_K*var(1)/var(0) );
		const MGFloat loc_q = sqrt( EqnEuler<DIM>::SqrU(var) );
		const MGFloat loc_M = loc_q / loc_c;

		Decomp_Pvlr( smtxP, loc_M, beta);

		//vtmp = smtxP * smtxPI * vtmp;
		//vtmp =  smtxPI * vtmp;

		for ( MGSize neq=0; neq<EQSIZE-1; ++neq)
			mtabsFi[i](neq+1) = vtmp(neq);
	}


	for ( MGSize i=0; i<=DIM; ++i)
		tabres[i] = mmtxM_QW * mtabsFi[i];
}


template <Dimension DIM, class MTXRDS>
inline void RDSchemeTest<DIM,MTXRDS>::Residuum_S_MtxW( const CFCell& fcell, FVec tabres[])
{
	typedef ScLDAscheme<DIM>	TScScheme;
	typedef MtxLDAscheme<DIM>	TMtxScheme;

	typedef SVector<EQSIZE-1>	FSubVec;
	typedef SMatrix<EQSIZE-1>	FSubMtx;

	mvarZm.Init( 0.0);
	mvarVm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
		mvarZm += mtabsZ[i] = EqnEuler<DIM>::ConservToZ( fcell.cVar(i) );

	mvarZm /= (MGFloat)(DIM+1);
	
	mvarVm = EqnEuler<DIM>::ZToSimp( mvarZm);

	FMtx	smtxM_QZ, smtxM_ZV, smtxM_VW;
	EqnEuler<DIM>::CJacobQZ( smtxM_QZ, mvarZm);
	EqnEuler<DIM>::CJacobZV( smtxM_ZV, mvarZm);
	EqnEuler<DIM>::CJacobVW( smtxM_VW, mvarVm);

	mmtxM_QW = smtxM_QZ * smtxM_ZV * smtxM_VW;


	FMtx	smtxM_VZ, smtxM_WV, smtxM_XW;
	EqnEuler<DIM>::CJacobVZ( smtxM_VZ, mvarZm);
	EqnEuler<DIM>::CJacobWV( smtxM_WV, mvarVm);

	mmtxM_WZ = smtxM_WV * smtxM_VZ;

	EqnEuler<DIM>::RotMtx( mmtxR, mvarVm);

	for ( MGSize i=0; i<=DIM; ++i)
		mtabsW[i] = mmtxM_WZ * mtabsZ[i];

	MGFloat c = ::sqrt( FLOW_K*mvarVm(1)/mvarVm(0) );
	MGFloat q = sqrt( EqnEuler<DIM>::SqrU(mvarVm) );


	FSubMtx	smtxAp, smtxAm;
	MGFloat	vnmod;
	GVec	vn;
	MGFloat e = 1.0e-9;

	GVec tabvnn[DIM+1];

	for ( MGSize i=0; i<=DIM; ++i)
	{
		tabvnn[i] = GVec(0.,0.);
		for ( MGSize ir=0; ir<DIM; ++ir)
			for ( MGSize jr=0; jr<DIM; ++jr)
				tabvnn[i].rX(ir) += mmtxR(ir,jr) * fcell.Vn(i).cX(jr);
	}


	//////////////////////////////////////////////
	// scalar equations 

	MGFloat	tabk[DIM+1], tabfi[DIM+1], tabu[DIM+1];

	TMtxScheme	mtxcalc;

	for ( MGSize i=0; i<=DIM; ++i)
		tabk[i] = tabvnn[i].cX() * q / (MGFloat)DIM;

	//for ( MGSize neq=0; neq<2; ++neq)
	MGSize neq = 0;
	{
		for ( MGSize i=0; i<=DIM; ++i)
			tabu[i] = mtabsW[i](neq);

		TScScheme::Calc( tabk, tabu, tabfi);


		MGFloat fi = 0.0;
		for ( MGSize i=0; i<=DIM; ++i)
			fi += tabfi[i];

		//MGFloat sumbeta = 0.0;
		//MGFloat	tabbeta[DIM+1];
		//for ( MGSize i=0; i<=DIM; ++i)
		//{
		//	tabbeta[i] = SIGN( tabfi[i] * fi ) * min( fabs( tabfi[i]), fabs(fi) );
		//	sumbeta += tabbeta[i];
		//}
		//for ( MGSize i=0; i<=DIM; ++i)
		//	tabbeta[i] /= max( sumbeta, 1.0e-6);
		//
		//for ( MGSize i=0; i<=DIM; ++i)
		//	mtabsFi[i](neq) = tabbeta[i] * fi;

		for ( MGSize i=0; i<=DIM; ++i)
			mtabsFi[i](neq) = tabfi[i];

	}


	//////////////////////////////////////////////
	// matrix subsystem 

	FSubVec	tabsWsub[DIM+1];
	FSubMtx	tabKp[DIM+1];
	FSubMtx	tabKm[DIM+1];

	for ( MGSize i=0; i<=DIM; ++i)
	{
		GVec vnn = tabvnn[i].versor();

		FSubMtx mtxLsub, mtxLIsub;
		Decomp_subLWni( mtxLsub, mvarVm, vnn);
		Decomp_subLWniI( mtxLIsub, mvarVm, vnn);

		{
			MGFloat l[3], lp[3], lm[3];

			l[0] = vnn.cX() * q;
			l[1] = vnn.cX() * q + c;
			l[2] = vnn.cX() * q - c;


			for ( MGInt i=0; i<3; ++i)
			{
				//MGFloat ls = ::fabs( l[i]);
				//if ( ls < e)
				//{
				//	//ls = (ls*ls/e + e);
				//	ls = e;
				//	bcorr = true;
				//}

				//lp[i] = 0.5*( l[i] + ls );
				//lm[i] = 0.5*( l[i] - ls );
				lp[i] = MAX( l[i],  e );
				lm[i] = MIN( l[i], -e );
			}

			FSubMtx mtxLp, mtxLm;
			mtxLp.Resize(EQSIZE-1,EQSIZE-1);
			mtxLp.Init(0.0);
			mtxLp(0,0) = lp[0];
			mtxLp(1,1) = lp[1];
			mtxLp(2,2) = lp[2];

			mtxLm.Resize(EQSIZE-1,EQSIZE-1);
			mtxLm.Init(0.0);
			mtxLm(0,0) = lm[0];
			mtxLm(1,1) = lm[1];
			mtxLm(2,2) = lm[2];

			smtxAp = mtxLIsub * mtxLp * mtxLsub;
			smtxAm = mtxLIsub * mtxLm * mtxLsub;
		}

		//vnmod = fcell.Vn(i).module() / (MGFloat)DIM;
		vnmod = tabvnn[i].module() / (MGFloat)DIM;

		MtxMultMtxD( tabKp[i], smtxAp, vnmod);
		MtxMultMtxD( tabKm[i], smtxAm, vnmod);
		//mtabKp[i] = vnmod * smtxAp;
		//mtabKm[i] = vnmod * smtxAm;

		for ( MGSize k=0; k<EQSIZE-1; ++k)
			tabsWsub[i](k) = mtabsW[i](k+1);
	}

	FSubVec	tabsFisub[DIM+1];
	
	mtxcalc.Calc( EQSIZE-1, tabKp, tabKm, tabsWsub, tabsFisub);

	for ( MGSize i=0; i<=DIM; ++i)
		for ( MGSize k=0; k<EQSIZE-1; ++k)
			mtabsFi[i](k+1) = tabsFisub[i](k);

	for ( MGSize i=0; i<=DIM; ++i)
		tabres[i] = mmtxM_QW * mtabsFi[i];

}



template <Dimension DIM, class MTXRDS>
inline void RDSchemeTest<DIM,MTXRDS>::Residuum_MtxW( const CFCell& fcell, FVec tabres[])
{
	mvarZm.Init( 0.0);
	mvarVm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
		mvarZm += mtabsZ[i] = EqnEuler<DIM>::ConservToZ( fcell.cVar(i) );

	mvarZm /= (MGFloat)(DIM+1);
	
	mvarVm = EqnEuler<DIM>::ZToSimp( mvarZm);

	FMtx	smtxM_QZ, smtxM_ZV, smtxM_VW;
	EqnEuler<DIM>::CJacobQZ( smtxM_QZ, mvarZm);
	EqnEuler<DIM>::CJacobZV( smtxM_ZV, mvarZm);
	EqnEuler<DIM>::CJacobVW( smtxM_VW, mvarVm);

	mmtxM_QW = smtxM_QZ * smtxM_ZV * smtxM_VW;


	FMtx	smtxM_VZ, smtxM_WV, smtxM_XW;
	EqnEuler<DIM>::CJacobVZ( smtxM_VZ, mvarZm);
	EqnEuler<DIM>::CJacobWV( smtxM_WV, mvarVm);

	EqnEuler<DIM>::RotMtx( mmtxR, mvarVm);

	//EqnEuler<DIM>::CJacobXW( smtxM_XW, mvarVm);

	mmtxM_WZ = smtxM_WV * smtxM_VZ;


	for ( MGSize i=0; i<=DIM; ++i)
		mtabsW[i] = mmtxM_WZ * mtabsZ[i];

	MGFloat c = ::sqrt( FLOW_K*mvarVm(1)/mvarVm(0) );


	FMtx	smtxAp, smtxAm;
	MGFloat	vnmod;
	GVec	vn;
	MGFloat e = 1.0e-9;

	for ( MGSize i=0; i<=DIM; ++i)
	{
		vn = fcell.Vn(i).versor();

		GVec vnn(0.,0.);
		for ( MGSize ir=0; ir<DIM; ++ir)
			for ( MGSize jr=0; jr<DIM; ++jr)
				vnn.rX(ir) += mmtxR(ir,jr) * vn.cX(jr);

		FMtx mtxL, mtxLI;
		Decomp_LWni( mtxL, mvarVm, vnn);
		Decomp_LWniI( mtxLI, mvarVm, vnn);

		{
			MGFloat l[3], lp[3], lm[3];
			MGFloat q = sqrt( EqnEuler<DIM>::SqrU(mvarVm) );

			l[0] = vnn.cX() * q;
			l[1] = vnn.cX() * q + c;
			l[2] = vnn.cX() * q - c;


			for ( MGInt i=0; i<3; ++i)
			{
				//MGFloat ls = ::fabs( l[i]);
				//if ( ls < e)
				//{
				//	//ls = (ls*ls/e + e);
				//	ls = e;
				//	bcorr = true;
				//}
				////ls = fabs( l[i]); 

				//lp[i] = 0.5*( l[i] + ls );
				//lm[i] = 0.5*( l[i] - ls );
				lp[i] = MAX( l[i],  e );
				lm[i] = MIN( l[i], -e );
			}

			FMtx mtxLp, mtxLm;
			mtxLp.Resize(EQSIZE,EQSIZE);
			mtxLp.Init(0.0);
			mtxLp(0,0) = mtxLp(1,1) = lp[0];
			mtxLp(2,2) = lp[1];
			mtxLp(3,3) = lp[2];

			mtxLm.Resize(EQSIZE,EQSIZE);
			mtxLm.Init(0.0);
			mtxLm(0,0) = mtxLm(1,1) = lm[0];
			mtxLm(2,2) = lm[1];
			mtxLm(3,3) = lm[2];

			smtxAp = mtxLI * mtxLp * mtxL;
			smtxAm = mtxLI * mtxLm * mtxL;

			//AssembleMtx( smtxKp, vn, lp[0], lp[1], lp[2] );
			//AssembleMtx( smtxKm, vn, lm[0], lm[1], lm[2] );
		}

		//mDecomp.Calc( smtxAp, smtxAm, vn, 1.0e-9, bc );

		vnmod = fcell.Vn(i).module() / (MGFloat)DIM;

		MtxMultMtxD( mtabKp[i], smtxAp, vnmod);
		MtxMultMtxD( mtabKm[i], smtxAm, vnmod);
		//mtabKp[i] = vnmod * smtxAp;
		//mtabKm[i] = vnmod * smtxAm;
	}


	calc.Calc( EQSIZE, mtabKp, mtabKm, mtabsW, mtabsFi);

	for ( MGSize i=0; i<=DIM; ++i)
		tabres[i] = mmtxM_QW * mtabsFi[i];

}


template <Dimension DIM, class MTXRDS>
inline void RDSchemeTest<DIM,MTXRDS>::Initialize( const CFCell& fcell)
{
	mvarZm.Init( 0.0);
	mvarVm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
		mvarZm += mtabsZ[i] = EqnEuler<DIM>::ConservToZ( fcell.cVar(i) );

	mvarZm /= (MGFloat)(DIM+1);
	
	mvarVm = EqnEuler<DIM>::ZToSimp( mvarZm);

	FMtx	smtxM_QZ, smtxM_ZV, smtxM_VW;
	EqnEuler<DIM>::CJacobQZ( smtxM_QZ, mvarZm);
	EqnEuler<DIM>::CJacobZV( smtxM_ZV, mvarZm);
	EqnEuler<DIM>::CJacobVW( smtxM_VW, mvarVm);

	mmtxM_QW = smtxM_QZ * smtxM_ZV * smtxM_VW;


	FMtx	smtxM_VZ, smtxM_WV, smtxM_XW;
	EqnEuler<DIM>::CJacobVZ( smtxM_VZ, mvarZm);
	EqnEuler<DIM>::CJacobWV( smtxM_WV, mvarVm);

	EqnEuler<DIM>::RotMtx( mmtxR, mvarVm);

	//EqnEuler<DIM>::CJacobXW( smtxM_XW, mvarVm);

	mmtxM_WZ = smtxM_WV * smtxM_VZ;


	for ( MGSize i=0; i<=DIM; ++i)
		mtabsW[i] = mmtxM_WZ * mtabsZ[i];

	mV = mvarVm;
	mc = ::sqrt( FLOW_K*mV(1)/mV(0) );


	FMtx	smtxAp, smtxAm;
	MGFloat	vnmod;
	GVec	vn;
	bool	bcorr;
	MGFloat e = 1.0e-9;

	for ( MGSize i=0; i<=DIM; ++i)
	{
		vn = fcell.Vn(i).versor();

		GVec vnn(0.,0.);
		for ( MGSize ir=0; ir<DIM; ++ir)
			for ( MGSize jr=0; jr<DIM; ++jr)
				vnn.rX(ir) += mmtxR(ir,jr) * vn.cX(jr);

		FMtx mtxL, mtxLI;
		Decomp_LWni( mtxL, mvarVm, vnn);
		Decomp_LWniI( mtxLI, mvarVm, vnn);

		{
			MGFloat l[3], lp[3], lm[3];
			MGFloat q = sqrt( EqnEuler<DIM>::SqrU(mvarVm) );

			l[0] = vnn.cX() * q;
			l[1] = vnn.cX() * q + mc;
			l[2] = vnn.cX() * q - mc;


			for ( MGInt i=0; i<3; ++i)
			{

				MGFloat ls = ::fabs( l[i]);
				if ( ls < e)
				{
					//ls = (ls*ls/e + e);
					ls = e;
					bcorr = true;
				}
				//ls = fabs( l[i]); 

				lp[i] = 0.5*( l[i] + ls );
				lm[i] = 0.5*( l[i] - ls );
			}

			FMtx mtxLp, mtxLm;
			mtxLp.Resize(EQSIZE,EQSIZE);
			mtxLp.Init(0.0);
			mtxLp(0,0) = lp[1];
			mtxLp(1,1) = lp[2];
			mtxLp(2,2) = mtxLp(3,3) = lp[0];

			mtxLm.Resize(EQSIZE,EQSIZE);
			mtxLm.Init(0.0);
			mtxLm(0,0) = lm[1];
			mtxLm(1,1) = lm[2];
			mtxLm(2,2) = mtxLm(3,3) = lm[0];

			smtxAp = mtxLI * mtxLp * mtxL;
			smtxAm = mtxLI * mtxLm * mtxL;

			//AssembleMtx( smtxKp, vn, lp[0], lp[1], lp[2] );
			//AssembleMtx( smtxKm, vn, lm[0], lm[1], lm[2] );
		}

		//mDecomp.Calc( smtxAp, smtxAm, vn, 1.0e-9, bc );

		vnmod = fcell.Vn(i).module() / (MGFloat)DIM;

		MtxMultMtxD( mtabKp[i], smtxAp, vnmod);
		MtxMultMtxD( mtabKm[i], smtxAm, vnmod);
		//mtabKp[i] = vnmod * smtxAp;
		//mtabKm[i] = vnmod * smtxAm;
	}
}



template <Dimension DIM, class MTXRDS>
inline void RDSchemeTest<DIM,MTXRDS>::CalcResiduum( const CFCell& fcell, FVec tabres[])		
{
	//static ofstream of("_dump_res.dat");
	//static MGSize id = 0;

	//Residuum_MtxW( fcell, tabres);
	//Residuum_S_MtxW( fcell, tabres);
	//Residuum_PS_MtxW( fcell, tabres);
	Residuum_PGenS_MtxW( fcell, tabres);

	//FVec sumres;
	//for ( MGSize i=0; i<EQSIZE; ++i)
	//{
	//	sumres[i] = 0.0;
	//	for ( MGSize k=0; k<=DIM; ++k)
	//		sumres(i) += tabres[k](i);
	//}

	//of << id++;
	//for ( MGSize i=0; i<EQSIZE; ++i)
	//	of << " " << sumres(i);
	//of << endl;
}



template <Dimension DIM, class MTXRDS>
inline void RDSchemeTest<DIM,MTXRDS>::CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EULERRDSCHEMETEST_H__
