#ifndef __EULERRDSCHEMEPRET_H__
#define __EULERRDSCHEMEPRET_H__

#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"
#include "redvc/libredvccommon/flowfunc.h"


#include "libredphysics/eulerequations.h"

#include "decompturk.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
// class RDSchemePreT
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class MTXRDS, class SCRDS>
class RDSchemePreT : public FlowFunc<DIM, EQN_EULER, EquationDef<DIM,EQN_EULER>::SIZE>
{
	enum { EQSIZE = EquationDef<DIM,EQN_EULER>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_EULER>::AUXSIZE };

	typedef Vect<DIM>									GVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FMtx	FMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>				CFCell;

	typedef SVector<EQSIZE-1>				FSubVec;
	typedef SMatrix<EQSIZE-1>				FSubMtx;


public:
	RDSchemePreT() : mbPrec(true)	{}
	RDSchemePreT( const FVec& var) : mRefVar(var), mbPrec(true)	{}

	virtual void	Init( const Physics<DIM,EQN_EULER>& physics);

	virtual void	CalcResiduum( const CFCell& fcell, FVec tabres[]);
	virtual void	CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

	void	ApplyPrecond( FVec& res, const FVec& vsimpl );


protected:
	void	Initialize( const CFCell& fcell);
	void	FinalizeRes( FVec tabres[]);
	void	FinalizeResJacob( FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

	MGFloat	Beta( const FVec& varV);

private:
	DecompTurk<DIM>	mDecomp;

	bool mbPrec;

	FVec	mRefVar;
	MGFloat mMinf;
	MGFloat mpinf;
	MGFloat mptinf;


	FVec	mvarZm, mvarVm;
	FVec	mtabsW[DIM+1];

	//FMtx	mtabKp[DIM+1];
	//FMtx	mtabKm[DIM+1];
	//FMtx	mtabK[DIM+1];

	FVec	mtabsFi[DIM+1];
	FMtx	mmtxsJ[DIM+1][DIM+1];

	struct RDSMtxCalc : public MTXRDS {}	mtxcalc;
	struct RDSScCalc  : public SCRDS  {}	sccalc;

};
//////////////////////////////////////////////////////////////////////





template <Dimension DIM, class MTXRDS, class SCRDS>
void RDSchemePreT<DIM,MTXRDS,SCRDS>::Init( const Physics<DIM,EQN_EULER>& physics)
{
	physics.DimToUndim( mRefVar, physics.cRefVar(), physics.cRefVar() );
}


template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemePreT<DIM,MTXRDS,SCRDS>::Initialize( const CFCell& fcell)
{
}



template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemePreT<DIM,MTXRDS,SCRDS>::CalcResiduum( const CFCell& fcell, FVec tabres[])		
{
	mbPrec = true;
	if ( fcell.IsViscWall()  )
		mbPrec = false;

	//mbPrec = false;


	mMinf = EqnEulerComm<DIM>::Mach( mRefVar);
	mpinf = EqnEulerComm<DIM>::Pressure( mRefVar);
	mptinf = mpinf * pow( 1. + 0.5*(FLOW_K-1.)*mMinf*mMinf, FLOW_K/(FLOW_K-1.) );


	FVec	mtabsZ[DIM+1];

	mvarZm.Init( 0.0);
	mvarVm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
	{
		FVec var = EqnEuler<DIM>::ConservToZ( fcell.cVar(i) );
		var(EquationDef<DIM,EQN_EULER>::ID_RHO) = max( 1.0e-10, var(EquationDef<DIM,EQN_EULER>::ID_RHO) );
		var(EquationDef<DIM,EQN_EULER>::ID_P)   = max( 1.0e-10, var(EquationDef<DIM,EQN_EULER>::ID_P) );

		mvarZm += var;
		mtabsZ[i] = mtabsW[i] = var;
	}

	mvarZm /= (MGFloat)(DIM+1);
	mvarVm = EqnEuler<DIM>::ZToSimp( mvarZm);

	//mvarVm.Write();

	mDecomp.Init( mvarVm);

	for ( MGSize i=0; i<=DIM; ++i)
		mDecomp.ConvertZtoW( mtabsW[i], mtabsW[i] );


	MGFloat c = ::sqrt( FLOW_K*mvarVm(1)/mvarVm(0) );
	MGFloat q = sqrt( EqnEuler<DIM>::SqrU(mvarVm) );
	MGFloat M = q/c;

	GVec	vecU; 
	for ( MGSize i=0; i<DIM; ++i)
		vecU.rX(i) = mvarVm(i + EquationDef<DIM,EQN_EULER>::template U<0>::ID );


	//cout << Minf << endl;
	//cout << pinf << endl;
	//refV.Write();
	//THROW_INTERNAL( "STOP");

	MGFloat beta = Beta( mvarVm);

	FSubMtx	smtxAp, smtxAm, smtxP, smtxPI;
	MGFloat	vnmod;
	GVec	vn; 
	MGFloat e = 1.0e-9;

	GVec tabvnn[DIM+1];

	for ( MGSize i=0; i<=DIM; ++i)
		tabvnn[i] = fcell.Vn(i);

	//////////////////////////////////////////////
	// scalar equations 

	MGFloat	tabk[DIM+1], tabfi[DIM+1], tabu[DIM+1];

	for ( MGSize i=0; i<=DIM; ++i)
		tabk[i] = tabvnn[i] * vecU / (MGFloat)DIM;

	const MGSize neq = 0;
	{
		for ( MGSize i=0; i<=DIM; ++i)
			tabu[i] = mtabsW[i](neq);

		sccalc.Calc( tabk, tabu, tabfi);


		MGFloat fi = 0.0;
		for ( MGSize i=0; i<=DIM; ++i)
			fi += tabfi[i];

		for ( MGSize i=0; i<=DIM; ++i)
			mtabsFi[i](neq) = tabfi[i];

	}

	//////////////////////////////////////////////
	// matrix subsystem 

	FSubVec	tabsWsub[DIM+1];
	FSubMtx	tabKp[DIM+1];
	FSubMtx	tabKm[DIM+1];

	FSubMtx	tabKpart[4];
	MGFloat tabLam[3], tabLamP[3], tabLamM[3];




	for ( MGSize i=0; i<=DIM; ++i)
	{
		GVec vnn = tabvnn[i].versor();
		MGFloat Mn = vnn * vecU / c;

	//Mn = 0.1;
	//cout << "Mn = " << Mn << endl;
	//cout << "c = " << c << endl;
	//beta=0.01;
	//cout << "beta = " << beta << endl;

		//vnn.rX() = 1./sqrt(14.);
		//vnn.rY() = 2./sqrt(14.);
		//vnn.rZ() = 3./sqrt(14.);

		InitTurkelMatrices<DIM,DIM+1>( tabKpart, tabLam, vnn, Mn, c, beta );

		//tabKpart[0].Write();
		//tabKpart[1].Write();
		//tabKpart[2].Write();

		//cout << tabLam[0] << endl;
		//cout << tabLam[1] << endl;
		//cout << tabLam[2] << endl;

		//( tabLam[0]*tabKpart[0] + tabLam[1]*tabKpart[1] + tabLam[2]*tabKpart[2]).Write();

		for ( MGSize k=0; k<3; ++k)
		{
			tabLamP[k] = MAX( tabLam[k],  e);
			tabLamM[k] = MIN( tabLam[k], -e);
		}


		smtxAp = tabLamP[0]*tabKpart[0] + tabLamP[1]*tabKpart[1] + tabLamP[2]*tabKpart[2];
		smtxAm = tabLamM[0]*tabKpart[0] + tabLamM[1]*tabKpart[1] + tabLamM[2]*tabKpart[2];


		vnmod = tabvnn[i].module() / (MGFloat)DIM;

		MtxMultMtxD( tabKp[i], smtxAp, vnmod);
		MtxMultMtxD( tabKm[i], smtxAm, vnmod);

		for ( MGSize k=0; k<EQSIZE-1; ++k)
			tabsWsub[i](k) = mtabsW[i](k+1);

		tabsWsub[i](0) /= beta;
	}

	FSubVec	tabsFisub[DIM+1];
	
	mtxcalc.Calc( EQSIZE-1, tabKp, tabKm, tabsWsub, tabsFisub);

	//FSubVec locfi;
	//locfi.Init(0.0); 
	//for ( MGSize i=0; i<=DIM; ++i)
	//	 locfi += tabsFisub[i];

	//locfi.Write();

	for ( MGSize i=0; i<=DIM; ++i)
		tabsFisub[i](0) /= beta;


	for ( MGSize i=0; i<=DIM; ++i)
	{
		FSubVec vtmp = tabsFisub[i];

		for ( MGSize neq=0; neq<EQSIZE-1; ++neq)
			mtabsFi[i](neq+1) = vtmp(neq);
	}

	//ofstream off("_fluxdaump.dat", ios_base::app );
	//FVec locfi;
	//locfi.Init(0.0); 
	//for ( MGSize i=0; i<=DIM; ++i)
	//	 locfi += mtabsFi[i];
	//locfi.Write(off);
	//off.close();

	//for ( MGSize i=0; i<=DIM; ++i)
	//{
	//	FVec	var = EqnEuler<DIM>::ZToSimp( mtabsZ[i]);

	//	const MGFloat loc_c = ::sqrt( FLOW_K*var(1)/var(0) );
	//	const MGFloat loc_q = sqrt( EqnEuler<DIM>::SqrU(var) );
	//	const MGFloat loc_M = loc_q / loc_c;

	//	MGFloat loc_beta = min( max( fabs(M), Mlim ), 1.);

	//	mtabsFi[i](1) *= loc_beta*loc_beta;
	//}

	for ( MGSize i=0; i<=DIM; ++i)
	{
		mDecomp.ConvertWtoQ( tabres[i], mtabsFi[i] );

		FVec varV = EqnEuler<DIM>::ConservToSimp( fcell.cVar(i) );
		ApplyPrecond( tabres[i], varV);

		//FMtx mtxPQ;
		//FVec var = EqnEuler<DIM>::ZToSimp( mtabsZ[i]);

		//const MGFloat loc_c = ::sqrt( FLOW_K*var(1)/var(0) );
		//const MGFloat loc_q = sqrt( EqnEuler<DIM>::SqrU(var) );
		//const MGFloat loc_M = loc_q / loc_c;

		//MGFloat loc_beta = min( max( fabs(M), Mlim ), 1.);

		//mDecomp.MtxPQ( mtxPQ, var, loc_beta);

		//tabres[i] = mtxPQ * tabres[i];
	}

	//ofstream off("_fluxdaump.dat", ios_base::app );
	//FVec locfi;
	//locfi.Init(0.0); 
	//for ( MGSize i=0; i<=DIM; ++i)
	//	 locfi += tabres[i];
	//locfi.Write(off);
	//off.close();


}



template <Dimension DIM, class MTXRDS, class SCRDS>
inline MGFloat RDSchemePreT<DIM,MTXRDS,SCRDS>::Beta( const FVec& varV)
{
	//if ( ! mbPrec )
	//	return 0.1;//return 1.0;

	//return 0.5*mMinf;


	const MGFloat Mlim = 0.5*mMinf;

	const MGFloat loc_c = ::sqrt( FLOW_K*varV(1)/varV(0) );
	const MGFloat loc_q = sqrt( EqnEuler<DIM>::SqrU(varV) );
	const MGFloat loc_M = loc_q / loc_c;

	MGFloat p = varV(EquationDef<DIM,EQN_EULER>::ID_P);
	MGFloat Mis = sqrt( fabs( 2.0 / (FLOW_K-1.) * ( pow( mptinf / p, (FLOW_K-1.)/FLOW_K) - 1.0 ) ));
	MGFloat Miscorr = pow( (Mis - loc_M) / Mis, 2.0) * (mMinf - Mis) + Mis;
	//MGFloat Miscorr = Mis;

	//MGFloat loc_beta = min( max( max( fabs(loc_M), Mlim ), Miscorr ), 1.);
	MGFloat loc_beta = min( max( fabs(loc_M), Mlim ), 1.);

	return loc_beta;
}


template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemePreT<DIM,MTXRDS,SCRDS>::ApplyPrecond( FVec& res, const FVec& varV )
{
	//return;
	//if ( ! mbPrec )
	//	return;

	MGFloat loc_beta = Beta( varV);

	FMtx mtxPQ;

	mDecomp.MtxPQ( mtxPQ, varV, loc_beta);

	res = mtxPQ * res;
}


template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemePreT<DIM,MTXRDS,SCRDS>::CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
	THROW_INTERNAL("Not implemented !!!");
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __EULERRDSCHEMEPRET_H__

