#ifndef __EULERRDSCHEMECRDMTX_H__
#define __EULERRDSCHEMECRDMTX_H__


#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"
#include "redvc/libredvccommon/flowfunc.h"
#include "redvc/libredvcgrid/compgeomcell_integrator.h"

#include "decompmtx.h"

#include "libredphysics/eulerequations.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Dimension DIM>
class FluxFunc_Simp
{
public:
	typedef EquationDef<DIM,EQN_EULER> Eqn;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;

	FVec operator() ( const Vect<DIM>& pos, const FVec& var, const Vect<DIM>& vn ) const
	{ 
		FVec varV = var;

		Vect<DIM> vecU;
		for ( MGSize i=0; i<DIM; ++i)
			vecU.rX(i) = varV(i + Eqn::template U<0>::ID );

		MGFloat U = vn * vecU;
		MGFloat U2 = vecU*vecU;

		FVec flux;

		flux(Eqn::ID_RHO) = U * varV(Eqn::ID_RHO);
		flux(Eqn::ID_P)   = U/(FLOW_K-1.) * ( 0.5*U2*(FLOW_K-1.)*varV(Eqn::ID_RHO) + FLOW_K*varV(Eqn::ID_P) );

		for ( MGSize i=0; i<DIM; ++i)
			flux( i + Eqn::template U<0>::ID ) = U * varV(Eqn::ID_RHO) * vecU.cX(i)  +  vn.cX(i) * varV(Eqn::ID_P);

		return flux; 
	}
};


template <Dimension DIM>
class FluxFunc_Roe
{
public:
	typedef EquationDef<DIM,EQN_EULER> Eqn;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;

	FVec operator() ( const Vect<DIM>& pos, const FVec& var, const Vect<DIM>& vn ) const
	{ 
		FVec varV = EqnEuler<DIM>::ZToSimp( var );

		Vect<DIM> vecU;
		for ( MGSize i=0; i<DIM; ++i)
			vecU.rX(i) = varV(i + Eqn::template U<0>::ID );

		MGFloat U = vn * vecU;
		MGFloat U2 = vecU*vecU;

		FVec flux;

		flux(Eqn::ID_RHO) = U * varV(Eqn::ID_RHO);
		flux(Eqn::ID_P)   = U/(FLOW_K-1.) * ( 0.5*U2*(FLOW_K-1.)*varV(Eqn::ID_RHO) + FLOW_K*varV(Eqn::ID_P) );

		for ( MGSize i=0; i<DIM; ++i)
			flux( i + Eqn::template U<0>::ID ) = U * varV(Eqn::ID_RHO) * vecU.cX(i)  +  vn.cX(i) * varV(Eqn::ID_P);

		return flux; 
	}
}; 


//////////////////////////////////////////////////////////////////////
// class RDSchemeCRDMtx
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class MTXRDS>
class RDSchemeCRDMtx : public FlowFunc<DIM, EQN_EULER, EquationDef<DIM,EQN_EULER>::SIZE>
{
	enum { EQSIZE = EquationDef<DIM,EQN_EULER>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_EULER>::AUXSIZE };

	typedef Vect<DIM>									GVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FMtx	FMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>				CFCell;

public:
	RDSchemeCRDMtx()	{}
	RDSchemeCRDMtx( const FVec& var) : mRefVar(var)	{}

	virtual void	Init( const Physics<DIM,EQN_EULER>& physics);

	virtual void	CalcResiduum( const CFCell& fcell, FVec tabres[]);
	virtual void	CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

	void	ApplyPrecond( FVec& res, const FVec& vsimpl )	{}

protected:
	void	Initialize( const CFCell& fcell);
	void	FinalizeRes( FVec tabres[]);
	void	FinalizeResJacob( FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

private:
	DecompMtx<DIM>	mDecomp;

	FVec	mRefVar;
	FVec	mvarVm;
	FVec	mtabsV[DIM+1];

	FMtx	mtabKp[DIM+1];
	FMtx	mtabKm[DIM+1];
	FMtx	mtabK[DIM+1];

	FVec	mtabsFi[DIM+1];
	FMtx	mmtxsJ[DIM+1][DIM+1];

	struct RDSCalc : public MTXRDS {} calc;

};
//////////////////////////////////////////////////////////////////////





template <Dimension DIM, class MTXRDS>
void RDSchemeCRDMtx<DIM,MTXRDS>::Init( const Physics<DIM,EQN_EULER>& physics)
{
	physics.DimToUndim( mRefVar, physics.cRefVar(), physics.cRefVar() );

	if ( this->ConfigSect().KeyExists( "FNC_PARM" ) )
	{
		MGString param = this->ConfigSect().ValueString( "FNC_PARM" );
		cout << "------------------- PARAM = " << param << endl;
	}
	
}


template <Dimension DIM, class MTXRDS>
inline void RDSchemeCRDMtx<DIM,MTXRDS>::Initialize( const CFCell& fcell)
{
	FVec	var;

	//mRefVar.Write();
	//THROW_INTERNAL( "STOP");

	//MGFloat	tabv[3] = {2,2,1};
	//Vect<MGFloat,DIM> vvv = fcell.GradVar( tabv);


	mvarVm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
	{
		var = EqnEuler<DIM>::ConservToSimp( fcell.cVar(i) );
		var(EquationDef<DIM,EQN_EULER>::ID_RHO) = max( 1.0e-10, var(EquationDef<DIM,EQN_EULER>::ID_RHO) );
		var(EquationDef<DIM,EQN_EULER>::ID_P)   = max( 1.0e-10, var(EquationDef<DIM,EQN_EULER>::ID_P) );

		mvarVm += var;
		mtabsV[i] = var;
	}

	mvarVm /= (MGFloat)(DIM+1);


	GVec	tabVn[DIM+1];
	for ( MGSize i=0; i<=DIM; ++i)
		tabVn[i] = fcell.Vn(i);

	mDecomp.Init( mvarVm, mtabsV, tabVn);

	FMtx	smtxAp, smtxAm;
	MGFloat	vnmod;
	GVec	vn;
	bool	bc;

	for ( MGSize i=0; i<=DIM; ++i)
	{
		vn = fcell.Vn(i).versor();

		mDecomp.Calc( smtxAp, smtxAm, vn, 1.0e-9, bc );
		//mDecomp.Calc( smtxAp, smtxAm, vn, 1.0e-7, bc );
		//mDecomp.Calc( smtxAp, smtxAm, vn, 0, bc );

		vnmod = fcell.Vn(i).module() / (MGFloat)DIM;


		MtxMultMtxD( mtabKp[i], smtxAp, vnmod);
		MtxMultMtxD( mtabKm[i], smtxAm, vnmod);
		//mtabKp[i] = vnmod * smtxAp;
		//mtabKm[i] = vnmod * smtxAm;

	}
}


template <Dimension DIM, class MTXRDS>
inline void RDSchemeCRDMtx<DIM,MTXRDS>::FinalizeRes( FVec tabres[])		
{
	for ( MGSize i=0; i<=DIM; ++i)
		mDecomp.ConvertVtoQ( tabres[i], mtabsFi[i]);
}

template <Dimension DIM, class MTXRDS>
inline void RDSchemeCRDMtx<DIM,MTXRDS>::FinalizeResJacob( FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
	for ( MGSize i=0; i<=DIM; ++i)
	{
		mDecomp.ConvertVtoQ( tabres[i], mtabsFi[i]);

		for ( MGSize k=0; k<=DIM; ++k)
			mDecomp.ConvertMtxVtoQ( mtxres[i][k], mmtxsJ[i][k]);
	}
}


template <Dimension DIM, class MTXRDS>
inline void RDSchemeCRDMtx<DIM,MTXRDS>::CalcResiduum( const CFCell& fcell, FVec tabres[])		
{

	Initialize( fcell);

	//FVec fluxSimp = FluxInegrator< DIM, typename FluxFunc_Simp<DIM>::FVec >( fcell, 3).template IntegrateFlux< FluxFunc_Simp<DIM> >( mtabsV);
	FVec fluxSimp = CellInegrator< DIM >( fcell, 3).template BoundaryIntegral< FVec, FVec, FluxFunc_Simp<DIM> >( mtabsV);

	FMtx	smtxM_VQ;
	EqnEuler<DIM>::CJacobVQ( smtxM_VQ, mvarVm);

	FVec flux = smtxM_VQ * fluxSimp;

	calc.Calc( EQSIZE, mtabKp, mtabKm, mtabsV, flux, mtabsFi);

	FinalizeRes( tabres);
}



template <Dimension DIM, class MTXRDS>
inline void RDSchemeCRDMtx<DIM,MTXRDS>::CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
	Initialize( fcell);

	calc.Calc( EQSIZE, mtabKp, mtabKm, mtabsV, mtabsFi, mmtxsJ);

	FinalizeResJacob( tabres, mtxres);

}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//   

#endif // __EULERRDSCHEMECRDMTX_H__
