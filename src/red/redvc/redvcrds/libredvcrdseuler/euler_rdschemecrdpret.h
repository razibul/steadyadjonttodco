#ifndef __EULERRDSCHEMECRDPRET_H__
#define __EULERRDSCHEMECRDPRET_H__

#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"
#include "redvc/libredvccommon/flowfunc.h"


#include "libredphysics/eulerequations.h"

#include "decompturk.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class FuncConvert_Ent
{
public:
	typedef EquationDef<DIM,EQN_EULER> Eqn;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;

	void	ToSimple( FVec& vout, const FVec& vin)
	{
		vout = vin;
		MGFloat s = vout(0);
		vout(0) = pow( vout(1)/s, -FLOW_K);
		//vout(0) = pow( vout(1)*exp(-s), -FLOW_K);

		//vout = vin;
		//MGFloat s = vout(0);
		//MGFloat c2 = vout(1);
		//vout(0) = pow( FLOW_K*exp(s)/c2, FLOW_K-1);
		//vout(1) = vout(0)*c2 / FLOW_K;
	}

	void	FromConserv( FVec& vout, const FVec& vin)
	{
		vout = EqnEuler<DIM>::ConservToSimp( vin );
		MGFloat rho = vout(0);

		vout(0) = vout(1) / pow(rho,FLOW_K);

		//vout(0) = log( vout(1) / pow(rho,FLOW_K) );
		//vout(0) = log(vout(1)) - FLOW_K*log(rho);

		//vout = EqnEuler<DIM>::ConservToSimp( vin );
		//MGFloat rho = vout(0);
		//MGFloat p = vout(1);

		//vout(0) = log(p) - FLOW_K*log(rho);
		//vout(1) = FLOW_K*p/rho;
	}
};

template <Dimension DIM>
class FuncConvert_Simple
{
public:
	typedef EquationDef<DIM,EQN_EULER> Eqn;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;

	void	ToSimple( FVec& vout, const FVec& vin)
	{
		vout = vin;
	}

	void	FromConserv( FVec& vout, const FVec& vin)
	{
		vout = EqnEuler<DIM>::ConservToSimp( vin );
	}
};

template <Dimension DIM>
class FuncConvert_Roe
{
public:
	typedef EquationDef<DIM,EQN_EULER> Eqn;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;

	void	ToSimple( FVec& vout, const FVec& vin)
	{
		vout = EqnEuler<DIM>::ZToSimp( vin );
	}

	void	FromConserv( FVec& vout, const FVec& vin)
	{
		vout = EqnEuler<DIM>::ConservToZ( vin );
	}
};


template <Dimension DIM>
class FuncConvert_Conserv
{
public:
	typedef EquationDef<DIM,EQN_EULER> Eqn;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;

	void	ToSimple( FVec& vout, const FVec& vin)
	{
		vout = EqnEuler<DIM>::ConservToSimp( vin );
	}

	void	FromConserv( FVec& vout, const FVec& vin)
	{
		vout = vin;
	}
};


template <Dimension DIM, class FUNC>
class FluxFuncVar
{
public:
	typedef EquationDef<DIM,EQN_EULER> Eqn;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;

	//FVec operator() ( const FVec& var, const Vect<DIM>& vn ) 
	FVec operator() ( const Vect<DIM>& pos, const FVec& var, const Vect<DIM>& vn ) const
	{ 
		FVec varV = var;

		FUNC().ToSimple( varV, var);

		Vect<DIM> vecU;
		for ( MGSize i=0; i<DIM; ++i)
			vecU.rX(i) = varV(i + Eqn::template U<0>::ID );

		MGFloat U = vn * vecU;
		MGFloat U2 = vecU*vecU;

		FVec flux;

		flux(Eqn::ID_RHO) = U * varV(Eqn::ID_RHO);
		flux(Eqn::ID_P)   = U/(FLOW_K-1.) * ( 0.5*U2*(FLOW_K-1.)*varV(Eqn::ID_RHO) + FLOW_K*varV(Eqn::ID_P) );

		for ( MGSize i=0; i<DIM; ++i)
			flux( i + Eqn::template U<0>::ID ) = U * varV(Eqn::ID_RHO) * vecU.cX(i)  +  vn.cX(i) * varV(Eqn::ID_P);

		return flux; 
	}
};


//////////////////////////////////////////////////////////////////////
// class RDSchemePreT
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class MTXRDS, class SCRDS>
class RDSchemeCRDPreT : public FlowFunc<DIM, EQN_EULER, EquationDef<DIM,EQN_EULER>::SIZE>
{
	enum { EQSIZE = EquationDef<DIM,EQN_EULER>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_EULER>::AUXSIZE };

	typedef Vect<DIM>									GVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FMtx	FMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>				CFCell;

	typedef SVector<EQSIZE-1>				FSubVec;
	typedef SMatrix<EQSIZE-1>				FSubMtx;


public:
	RDSchemeCRDPreT() : mbPrec(true)	{}
	RDSchemeCRDPreT( const FVec& var) : mRefVar(var), mbPrec(true)	{}

	virtual void	Init( const Physics<DIM,EQN_EULER>& physics);

	virtual void	CalcResiduum( const CFCell& fcell, FVec tabres[]);
	virtual void	CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

	void	ApplyPrecond( FVec& res, const FVec& vsimpl );


protected:
	void	Initialize( const CFCell& fcell);
	void	FinalizeRes( FVec tabres[]);
	void	FinalizeResJacob( FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

	MGFloat	Beta( const FVec& varV);

private:
	//DecompCRDTurk<DIM>	mDecomp;
	DecompTurk<DIM>	mDecomp;

	bool mbPrec;

	FVec	mRefVar;
	MGFloat mMinf;
	MGFloat mpinf;
	MGFloat mptinf;


	FVec	mvarZm;
	FVec	mvarVm;
	FVec	mtabsW[DIM+1];

	//FMtx	mtabKp[DIM+1];
	//FMtx	mtabKm[DIM+1];
	//FMtx	mtabK[DIM+1];

	FVec	mtabsFi[DIM+1];
	FMtx	mmtxsJ[DIM+1][DIM+1];

	struct RDSMtxCalc : public MTXRDS {}	mtxcalc;
	struct RDSScCalc  : public SCRDS  {}	sccalc;

};
//////////////////////////////////////////////////////////////////////





template <Dimension DIM, class MTXRDS, class SCRDS>
void RDSchemeCRDPreT<DIM,MTXRDS,SCRDS>::Init( const Physics<DIM,EQN_EULER>& physics)
{
	physics.DimToUndim( mRefVar, physics.cRefVar(), physics.cRefVar() );
}


template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemeCRDPreT<DIM,MTXRDS,SCRDS>::Initialize( const CFCell& fcell)
{
}


template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemeCRDPreT<DIM,MTXRDS,SCRDS>::CalcResiduum( const CFCell& fcell, FVec tabres[])		
{
	typedef FuncConvert_Simple<DIM> FConvert;
	//typedef FuncConvert_Roe<DIM> FConvert;
	//typedef FuncConvert_Conserv<DIM> FConvert;
	//typedef FuncConvert_Ent<DIM> FConvert;


	mbPrec = true;
	if ( fcell.IsViscWall()  )
		mbPrec = false;

	//mbPrec = false;


	mMinf = EqnEulerComm<DIM>::Mach( mRefVar);
	mpinf = EqnEulerComm<DIM>::Pressure( mRefVar);
	mptinf = mpinf * pow( 1. + 0.5*(FLOW_K-1.)*mMinf*mMinf, FLOW_K/(FLOW_K-1.) );

	FVec	varTmp;
	FVec	tabsV[DIM+1];

	mvarVm.Init( 0.0);
	varTmp.Init(0.0);
	for ( MGSize i=0; i<=DIM; ++i)
	{
		FConvert().FromConserv( tabsV[i], fcell.cVar(i) );
		varTmp += tabsV[i];
	}

	varTmp /= (MGFloat)(DIM+1);
	FConvert().ToSimple( mvarVm, varTmp);

	//mvarVm.Write();

	mDecomp.Init( mvarVm);

	//FVec fluxSimp = FluxInegrator< DIM, typename FluxFunc_Simp<DIM>::FVec >( fcell, 4).template IntegrateFlux< FluxFunc_Simp<DIM> >( tabsV);
	
	//FVec fluxSimp = FluxInegrator< DIM, typename FluxFuncVar< DIM, FConvert >::FVec >( fcell, 4).template IntegrateFlux< FluxFuncVar< DIM, FConvert > >( tabsV);

	FVec fluxSimp = CellInegrator< DIM >( fcell, 4).template BoundaryIntegral< FVec, FVec, FluxFuncVar< DIM, FConvert > >( tabsV);

	mDecomp.ConvertQtoW( fluxSimp, fluxSimp );

	//for ( MGSize i=0; i<=DIM; ++i)
	//	mDecomp.ConvertZtoW( mtabsW[i], mtabsW[i] );

	FMtx mtxM_WV;
	CJacobWxV<DIM,FMtx,FVec>( mtxM_WV, mvarVm);

	for ( MGSize i=0; i<=DIM; ++i)
	{
		FVec var;
		FConvert().ToSimple( var, tabsV[i]);
		mtabsW[i] = mtxM_WV * var;
	}


	MGFloat c = ::sqrt( FLOW_K*mvarVm(1)/mvarVm(0) );
	MGFloat q = sqrt( EqnEuler<DIM>::SqrU(mvarVm) );
	MGFloat M = q/c;

	GVec	vecU; 
	for ( MGSize i=0; i<DIM; ++i)
		vecU.rX(i) = mvarVm(i + EquationDef<DIM,EQN_EULER>::template U<0>::ID );


	//cout << Minf << endl;
	//cout << pinf << endl;
	//refV.Write();
	//THROW_INTERNAL( "STOP");

	MGFloat beta = Beta( mvarVm);

	FSubMtx	smtxAp, smtxAm, smtxP, smtxPI;
	MGFloat	vnmod;
	GVec	vn; 
	MGFloat e = 1.0e-9;

	GVec tabvnn[DIM+1];

	for ( MGSize i=0; i<=DIM; ++i)
		tabvnn[i] = fcell.Vn(i);

	//////////////////////////////////////////////
	// scalar equations 

	MGFloat	tabk[DIM+1], tabfi[DIM+1], tabu[DIM+1];

	for ( MGSize i=0; i<=DIM; ++i)
		tabk[i] = tabvnn[i] * vecU / (MGFloat)DIM;

	const MGSize neq = 0;
	{
		for ( MGSize i=0; i<=DIM; ++i)
			tabu[i] = mtabsW[i](neq);


		//ScLDAscheme<Geom::DIM_2D>().Calc( tabk, tabu, tabfi);
		sccalc.Calc( tabk, tabu, fluxSimp(0), tabfi);


		MGFloat fi = 0.0;
		for ( MGSize i=0; i<=DIM; ++i)
			fi += tabfi[i];

		for ( MGSize i=0; i<=DIM; ++i)
			mtabsFi[i](neq) = tabfi[i];

	}

	//////////////////////////////////////////////
	// matrix subsystem 


	FSubVec	subfluxW;
	FSubVec	tabsWsub[DIM+1];
	FSubMtx	tabKp[DIM+1];
	FSubMtx	tabKm[DIM+1];

	FSubMtx	tabKpart[4];
	MGFloat tabLam[3], tabLamP[3], tabLamM[3];




	for ( MGSize i=0; i<=DIM; ++i)
	{
		GVec vnn = tabvnn[i].versor();
		MGFloat Mn = vnn * vecU / c;

	//Mn = 0.1;
	//cout << "Mn = " << Mn << endl;
	//cout << "c = " << c << endl;
	//beta=0.01;
	//cout << "beta = " << beta << endl;

		//vnn.rX() = 1./sqrt(14.);
		//vnn.rY() = 2./sqrt(14.);
		//vnn.rZ() = 3./sqrt(14.);

		InitTurkelMatrices<DIM,DIM+1>( tabKpart, tabLam, vnn, Mn, c, beta );

		//tabKpart[0].Write();
		//tabKpart[1].Write();
		//tabKpart[2].Write();

		//cout << tabLam[0] << endl;
		//cout << tabLam[1] << endl;
		//cout << tabLam[2] << endl;

		//( tabLam[0]*tabKpart[0] + tabLam[1]*tabKpart[1] + tabLam[2]*tabKpart[2]).Write();

		for ( MGSize k=0; k<3; ++k)
		{
			tabLamP[k] = MAX( tabLam[k],  e);
			tabLamM[k] = MIN( tabLam[k], -e);
		}


		smtxAp = tabLamP[0]*tabKpart[0] + tabLamP[1]*tabKpart[1] + tabLamP[2]*tabKpart[2];
		smtxAm = tabLamM[0]*tabKpart[0] + tabLamM[1]*tabKpart[1] + tabLamM[2]*tabKpart[2];


		vnmod = tabvnn[i].module() / (MGFloat)DIM;

		MtxMultMtxD( tabKp[i], smtxAp, vnmod);
		MtxMultMtxD( tabKm[i], smtxAm, vnmod);

		for ( MGSize k=0; k<EQSIZE-1; ++k)
			tabsWsub[i](k) = mtabsW[i](k+1);

		tabsWsub[i](0) /= beta;
	}

	for ( MGSize neq=0; neq<EQSIZE-1; ++neq)
		subfluxW(neq) = fluxSimp(neq+1);
	subfluxW(0) *= beta;


	FSubVec	tabsFisub[DIM+1];
	
	//MtxLDAscheme<Geom::DIM_2D> locmtxcalc;
	//locmtxcalc.Calc( EQSIZE-1, tabKp, tabKm, tabsWsub, tabsFisub);
	mtxcalc.Calc( EQSIZE-1, tabKp, tabKm, tabsWsub, subfluxW, tabsFisub);

	//FSubVec locfi;
	//locfi.Init(0.0); 
	//for ( MGSize i=0; i<=DIM; ++i)
	//	 locfi += tabsFisub[i];

	//locfi.Write();

	for ( MGSize i=0; i<=DIM; ++i)
		tabsFisub[i](0) /= beta;


	for ( MGSize i=0; i<=DIM; ++i)
	{
		FSubVec vtmp = tabsFisub[i];

		for ( MGSize neq=0; neq<EQSIZE-1; ++neq)
			mtabsFi[i](neq+1) = vtmp(neq);
	}

	//ofstream off("_fluxdaump.dat", ios_base::app );
	//FVec locfi;
	//locfi.Init(0.0); 
	//for ( MGSize i=0; i<=DIM; ++i)
	//	 locfi += mtabsFi[i];
	//locfi.Write(off);
	//fluxSimp.Write(off);
	//off << ":::::::::::::::::::::::::" << endl;
	//off.close();

	//ofstream off("_fluxdaump.dat", ios_base::app );
	//FVec locfi;
	//locfi.Init(0.0); 
	//for ( MGSize i=0; i<=DIM; ++i)
	//	 locfi += mtabsFi[i];
	//locfi.Write(off);
	//off.close();

	//for ( MGSize i=0; i<=DIM; ++i)
	//{
	//	FVec	var = EqnEuler<DIM>::ZToSimp( mtabsZ[i]);

	//	const MGFloat loc_c = ::sqrt( FLOW_K*var(1)/var(0) );
	//	const MGFloat loc_q = sqrt( EqnEuler<DIM>::SqrU(var) );
	//	const MGFloat loc_M = loc_q / loc_c;

	//	MGFloat loc_beta = min( max( fabs(M), Mlim ), 1.);

	//	mtabsFi[i](1) *= loc_beta*loc_beta;
	//}

	for ( MGSize i=0; i<=DIM; ++i)
	{
		mDecomp.ConvertWtoQ( tabres[i], mtabsFi[i] );

		FVec varV = EqnEuler<DIM>::ConservToSimp( fcell.cVar(i) );
		ApplyPrecond( tabres[i], varV);

		//FMtx mtxPQ;
		//FVec var = EqnEuler<DIM>::ZToSimp( mtabsZ[i]);

		//const MGFloat loc_c = ::sqrt( FLOW_K*var(1)/var(0) );
		//const MGFloat loc_q = sqrt( EqnEuler<DIM>::SqrU(var) );
		//const MGFloat loc_M = loc_q / loc_c;

		//MGFloat loc_beta = min( max( fabs(M), Mlim ), 1.);

		//mDecomp.MtxPQ( mtxPQ, var, loc_beta);

		//tabres[i] = mtxPQ * tabres[i];
	}


}


/*
template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemeCRDPreT<DIM,MTXRDS,SCRDS>::CalcResiduum( const CFCell& fcell, FVec tabres[])		
{
	mbPrec = true;
	//if ( fcell.IsViscWall()  )
	//	mbPrec = false;

	//mbPrec = false;


	mMinf = EqnEulerComm<DIM>::Mach( mRefVar);
	mpinf = EqnEulerComm<DIM>::Pressure( mRefVar);
	mptinf = mpinf * pow( 1. + 0.5*(FLOW_K-1.)*mMinf*mMinf, FLOW_K/(FLOW_K-1.) );


	FVec	tabsV[DIM+1];

	mvarVm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
	{
		FVec var = EqnEuler<DIM>::ConservToSimp( fcell.cVar(i) );
		var(EquationDef<DIM,EQN_EULER>::ID_RHO) = max( 1.0e-10, var(EquationDef<DIM,EQN_EULER>::ID_RHO) );
		var(EquationDef<DIM,EQN_EULER>::ID_P)   = max( 1.0e-10, var(EquationDef<DIM,EQN_EULER>::ID_P) );

		mvarVm += var;
		tabsV[i] = var;
	}

	mvarVm /= (MGFloat)(DIM+1);
	//mvarVm.Write();

	///////////////

	FVec	mvarZm, mtabsZ[DIM+1];

	mvarZm.Init( 0.0);
	mvarVm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
	{
		FVec var = EqnEuler<DIM>::ConservToZ( fcell.cVar(i) );
		var(EquationDef<DIM,EQN_EULER>::ID_RHO) = max( 1.0e-10, var(EquationDef<DIM,EQN_EULER>::ID_RHO) );
		var(EquationDef<DIM,EQN_EULER>::ID_P)   = max( 1.0e-10, var(EquationDef<DIM,EQN_EULER>::ID_P) );

		mvarZm += var;
		mtabsZ[i] = mtabsW[i] = var;
	}

	mvarZm /= (MGFloat)(DIM+1);
	mvarVm = EqnEuler<DIM>::ZToSimp( mvarZm);


	mDecomp.Init( mvarVm);


	FVec fluxSimp = FluxInegrator< DIM, typename FluxFunc_Simp<DIM>::FVec >( fcell, 4).template IntegrateFlux< FluxFunc_Simp<DIM> >( tabsV);
	
	FVec fluxW;
	mDecomp.ConvertQtoW( fluxW, fluxSimp);

	ofstream off("_fluxdaump_W.dat", ios_base::app );
	fluxW.Write(off);
	off.close();


	for ( MGSize i=0; i<=DIM; ++i)
		mDecomp.ConvertVtoW( mtabsW[i], tabsV[i] );


	MGFloat c = ::sqrt( FLOW_K*mvarVm(1)/mvarVm(0) );
	MGFloat q = sqrt( EqnEuler<DIM>::SqrU(mvarVm) );
	MGFloat M = q/c;

	GVec	vecU; 
	for ( MGSize i=0; i<DIM; ++i)
		vecU.rX(i) = mvarVm(i + EquationDef<DIM,EQN_EULER>::template U<0>::ID );


	//cout << Minf << endl;
	//cout << pinf << endl;
	//refV.Write();
	//THROW_INTERNAL( "STOP");

	MGFloat beta = Beta( mvarVm);
	//MGFloat beta = Beta( mvarVm);

	FSubMtx	smtxAp, smtxAm, smtxP, smtxPI;
	MGFloat	vnmod;
	GVec	vn; 
	MGFloat e = 1.0e-9;

	GVec tabvnn[DIM+1];

	for ( MGSize i=0; i<=DIM; ++i)
		tabvnn[i] = fcell.Vn(i);

	//////////////////////////////////////////////
	// scalar equations 

	MGFloat	tabk[DIM+1], tabfi[DIM+1], tabu[DIM+1];

	for ( MGSize i=0; i<=DIM; ++i)
		tabk[i] = tabvnn[i] * vecU / (MGFloat)DIM;

	const MGSize neq = 0;
	{
		for ( MGSize i=0; i<=DIM; ++i)
			tabu[i] = mtabsW[i](neq);

		ScLDAscheme<Geom::DIM_2D> locsccalc;
		locsccalc.Calc( tabk, tabu, tabfi);
		//sccalc.Calc( tabk, tabu, fluxW(neq), tabfi);


		MGFloat fi = 0.0;
		for ( MGSize i=0; i<=DIM; ++i)
			fi += tabfi[i];

		for ( MGSize i=0; i<=DIM; ++i)
			mtabsFi[i](neq) = tabfi[i];

	}

	//////////////////////////////////////////////
	// matrix subsystem 

	FSubVec	subfluxW;
	FSubVec	tabsWsub[DIM+1];
	FSubMtx	tabKp[DIM+1];
	FSubMtx	tabKm[DIM+1];

	FSubMtx	tabKpart[3];
	MGFloat tabLam[3], tabLamP[3], tabLamM[3];


	for ( MGSize i=0; i<=DIM; ++i)
	{
		GVec vnn = tabvnn[i].versor();
		MGFloat Mn = vnn * vecU / c;

	//Mn = 0.1;
	//cout << "Mn = " << Mn << endl;
	//cout << "c = " << c << endl;
	//beta=0.01;
	//cout << "beta = " << beta << endl;

		//vnn.rX() = 1./sqrt(14.);
		//vnn.rY() = 2./sqrt(14.);
		//vnn.rZ() = 3./sqrt(14.);

		InitTurkelMatrices<DIM,DIM+1>( tabKpart, tabLam, vnn, Mn, c, beta );

		//tabKpart[0].Write();
		//tabKpart[1].Write();
		//tabKpart[2].Write();

		//cout << tabLam[0] << endl;
		//cout << tabLam[1] << endl;
		//cout << tabLam[2] << endl;

		//( tabLam[0]*tabKpart[0] + tabLam[1]*tabKpart[1] + tabLam[2]*tabKpart[2]).Write();

		for ( MGSize k=0; k<3; ++k)
		{
			tabLamP[k] = MAX( tabLam[k],  e);
			tabLamM[k] = MIN( tabLam[k], -e);
		}


		smtxAp = tabLamP[0]*tabKpart[0] + tabLamP[1]*tabKpart[1] + tabLamP[2]*tabKpart[2];
		smtxAm = tabLamM[0]*tabKpart[0] + tabLamM[1]*tabKpart[1] + tabLamM[2]*tabKpart[2];


		vnmod = tabvnn[i].module() / (MGFloat)DIM;

		MtxMultMtxD( tabKp[i], smtxAp, vnmod);
		MtxMultMtxD( tabKm[i], smtxAm, vnmod);

		for ( MGSize k=0; k<EQSIZE-1; ++k)
			tabsWsub[i](k) = mtabsW[i](k+1);

		tabsWsub[i](0) /= beta;
	}

	for ( MGSize neq=0; neq<EQSIZE-1; ++neq)
		subfluxW(neq) = fluxW(neq+1);
	//subfluxW(0) /= beta;

	FSubVec	tabsFisub[DIM+1];
	
	MtxLDAscheme<Geom::DIM_2D> locmtxcalc;
	locmtxcalc.Calc( EQSIZE-1, tabKp, tabKm, tabsWsub, tabsFisub);
	//FSubVec locfi;
	//locfi.Init(0.0); 
	//for ( MGSize i=0; i<=DIM; ++i)
	//	 locfi += tabsFisub[i];

	//locfi.Write(); subfluxW.Write();

	//mtxcalc.Calc( EQSIZE-1, tabKp, tabKm, tabsWsub, subfluxW, tabsFisub);

	for ( MGSize i=0; i<=DIM; ++i)
		tabsFisub[i](0) /= beta;

	for ( MGSize i=0; i<=DIM; ++i)
	{
		FSubVec vtmp = tabsFisub[i];

		for ( MGSize neq=0; neq<EQSIZE-1; ++neq)
			mtabsFi[i](neq+1) = vtmp(neq);
	}

	//for ( MGSize i=0; i<=DIM; ++i)
	//{
	//	FVec	var = EqnEuler<DIM>::ZToSimp( mtabsZ[i]);

	//	const MGFloat loc_c = ::sqrt( FLOW_K*var(1)/var(0) );
	//	const MGFloat loc_q = sqrt( EqnEuler<DIM>::SqrU(var) );
	//	const MGFloat loc_M = loc_q / loc_c;

	//	MGFloat loc_beta = min( max( fabs(M), Mlim ), 1.);

	//	mtabsFi[i](1) *= loc_beta*loc_beta;
	//}


	//ofstream offf("_fluxdaump.dat", ios_base::app );
	//FVec locfi;
	//locfi.Init(0.0); 
	//for ( MGSize i=0; i<=DIM; ++i)
	//	 locfi += mtabsFi[i];
	//locfi.Write(offf);
	//offf.close();

	for ( MGSize i=0; i<=DIM; ++i)
	{
		mDecomp.ConvertWtoQ( tabres[i], mtabsFi[i] );

		FVec varV = EqnEuler<DIM>::ConservToSimp( fcell.cVar(i) );
		ApplyPrecond( tabres[i], varV);

		//FMtx mtxPQ;
		//FVec var = EqnEuler<DIM>::ZToSimp( mtabsZ[i]);

		//const MGFloat loc_c = ::sqrt( FLOW_K*var(1)/var(0) );
		//const MGFloat loc_q = sqrt( EqnEuler<DIM>::SqrU(var) );
		//const MGFloat loc_M = loc_q / loc_c;

		//MGFloat loc_beta = min( max( fabs(M), Mlim ), 1.);

		//mDecomp.MtxPQ( mtxPQ, var, loc_beta);

		//tabres[i] = mtxPQ * tabres[i];
	}

	ofstream ooff("_fluxdaump.dat", ios_base::app );
	FVec locfi;
	locfi.Init(0.0); 
	for ( MGSize i=0; i<=DIM; ++i)
		 locfi += tabres[i];
	locfi.Write(ooff);
	ooff.close();

}


*/

template <Dimension DIM, class MTXRDS, class SCRDS>
inline MGFloat RDSchemeCRDPreT<DIM,MTXRDS,SCRDS>::Beta( const FVec& varV)
{
	//if ( ! mbPrec )
	//	return 0.1;//1.0;

	return mMinf;

	return 0.5*mMinf;

	return 1.0;


	const MGFloat Mlim = 0.5*mMinf;

	const MGFloat loc_c = ::sqrt( FLOW_K*varV(1)/varV(0) );
	const MGFloat loc_q = sqrt( EqnEuler<DIM>::SqrU(varV) );
	const MGFloat loc_M = loc_q / loc_c;

	MGFloat p = varV(EquationDef<DIM,EQN_EULER>::ID_P);
	MGFloat Mis = sqrt( fabs( 2.0 / (FLOW_K-1.) * ( pow( mptinf / p, (FLOW_K-1.)/FLOW_K) - 1.0 ) ));
	MGFloat Miscorr = pow( (Mis - loc_M) / Mis, 2.0) * (mMinf - Mis) + Mis;
	//MGFloat Miscorr = Mis;

	//MGFloat loc_beta = min( max( max( fabs(loc_M), Mlim ), Miscorr ), 1.);
	MGFloat loc_beta = min( max( fabs(loc_M), Mlim ), 1.);

	return loc_beta;
}


template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemeCRDPreT<DIM,MTXRDS,SCRDS>::ApplyPrecond( FVec& res, const FVec& varV )
{
	//if ( ! mbPrec )
	//	return;

	MGFloat loc_beta = Beta( varV);

	//loc_beta = 0.5;
	FMtx mtxPQ;

	mDecomp.MtxPQ( mtxPQ, varV, loc_beta);

	res = mtxPQ * res;
}


template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemeCRDPreT<DIM,MTXRDS,SCRDS>::CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
	THROW_INTERNAL("Not implemented !!!");
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __EULERRDSCHEMECRDPRET_H__

