#ifndef __DECOMPTURK_H__
#define __DECOMPTURK_H__


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM, class MATRIX, class VECTOR>
inline void CJacobWxV( MATRIX& mtx, const VECTOR& v)
{
	MGFloat	q = ::sqrt( EqnEuler<DIM>::SqrU( v) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );

	mtx.Resize( DIM+2, DIM+2);
	mtx.Init( 0);

	mtx(0,0) = -c*c;
	mtx(0,1) = 1;
	
	mtx(1,1) = 1/v(0)/c;

	for ( MGSize i=2; i<DIM+2; ++i)
		mtx(i,i) = 1.0;
}

template <Dimension DIM, class MATRIX, class VECTOR>
inline void CJacobVWx( MATRIX& mtx, const VECTOR& v)
{
	MGFloat	q = ::sqrt( EqnEuler<DIM>::SqrU( v) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );

	mtx.Resize( DIM+2, DIM+2);
	mtx.Init( 0);

	mtx(0,0) = -1.0/(c*c);
	mtx(0,1) = v(0)/c;
	
	mtx(1,1) = v(0)*c;

	for ( MGSize i=2; i<DIM+2; ++i)
		mtx(i,i) = 1.0;
}


template <Dimension DIM, MGSize N>
inline void InitTurkelMatrices( SMatrix<N> tabK[], MGFloat tabLam[], const Geom::Vect<DIM>& vn, const MGFloat& Mn, const MGFloat& c, const MGFloat& beta )
{
	MGFloat beta2 = beta*beta;
	MGFloat beta2minus = beta2 - 1.0;
	MGFloat beta2plus  = beta2 + 1.0;

	MGFloat SQRT = sqrt( beta2minus*beta2minus*Mn*Mn/beta2 + 4.0);
	MGFloat betaSQRT = beta * SQRT;

	MGFloat tP  = 1.0 / SQRT;
	MGFloat tMm = - 0.5 * ( Mn*beta2minus / betaSQRT  - 1.0 );
	MGFloat tMp =   0.5 * ( Mn*beta2minus / betaSQRT  + 1.0 );

	tabLam[0] = Mn*c;
	tabLam[1] = 0.5*c*( Mn*beta2plus + betaSQRT );
	tabLam[2] = 0.5*c*( Mn*beta2plus - betaSQRT );

	tabK[0].Resize(N,N);
	tabK[1].Resize(N,N);
	tabK[2].Resize(N,N);


	tabK[0](0,0) = 0.0;
	tabK[1](0,0) = tMp;
	tabK[2](0,0) = tMm;

	for ( MGSize i=1; i<=DIM; ++i)
	{
		tabK[0](i,0) = tabK[0](0,i) = 0.0;
		tabK[0](i,i) = ( 1.0 - vn.cX(i-1)*vn.cX(i-1) );;

		for ( MGSize j=i+1; j<=DIM; ++j)
			tabK[0](i,j) = tabK[0](j,i) = - vn.cX(i-1) * vn.cX(j-1);

		tabK[1](i,0) = tabK[1](0,i) =   tP * vn.cX(i-1);
		tabK[2](i,0) = tabK[2](0,i) = - tP * vn.cX(i-1);

		for ( MGSize j=i; j<=DIM; ++j)
		{
			tabK[1](i,j) = tabK[1](j,i) = tMm * vn.cX(i-1) * vn.cX(j-1);
			tabK[2](i,j) = tabK[2](j,i) = tMp * vn.cX(i-1) * vn.cX(j-1);
		}
	}

}



//////////////////////////////////////////////////////////////////////
// class DecompTurk
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class DecompTurk
{
	enum { EQSIZE = EquationDef<DIM,EQN_EULER>::SIZE };

    typedef EquationDef<DIM,EQN_EULER> FEqns;
	typedef Vect<DIM>									GVect;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FMtx	FMtx;

public:
	DecompTurk()	{}
	DecompTurk( const FVec& var)	{ Init( var);}

	void	Init( const FVec& var);

	void	Calc( FMtx &smtxKp, FMtx &smtxKm, const GVect &vn, const MGFloat& e, bool& bcorr);

	void	ConvertZtoW( FVec &vfi, const FVec& vin);
	void	ConvertWtoQ( FVec &vfi, const FVec& vin);

	void	ConvertQtoW( FVec &vfi, const FVec& vin);

	void	MtxPQ( FMtx& mtxPq, const FVec& vsimp, const MGFloat& beta);

protected:
	void	AssembleMtx( FMtx &mtx, const GVect &vn, const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 );

private:
	FMtx	mmtxM_WZ;
	FMtx	mmtxM_QW;

	FMtx	mmtxM_WQ;

};
//////////////////////////////////////////////////////////////////////



template <Dimension DIM>
void DecompTurk<DIM>::Init( const FVec& var)
{
	FVec varZ, varV;

	varV = var;
	varZ = EqnEuler<DIM>::SimpToZ( varV);


	FMtx	smtxM_QZ, smtxM_ZV, smtxM_VW;

	EqnEuler<DIM>::CJacobQZ( smtxM_QZ, varZ);
	EqnEuler<DIM>::CJacobZV( smtxM_ZV, varZ);
	CJacobVWx<DIM,FMtx,FVec>( smtxM_VW, varV);

	mmtxM_QW = smtxM_QZ * smtxM_ZV * smtxM_VW;


	FMtx	smtxM_VZ, smtxM_WV, smtxM_XW;
	EqnEuler<DIM>::CJacobVZ( smtxM_VZ, varZ);
	CJacobWxV<DIM,FMtx,FVec>( smtxM_WV, varV);

	mmtxM_WZ = smtxM_WV * smtxM_VZ;


	mmtxM_WQ = mmtxM_QW;
	mmtxM_WQ.Invert();
}


template <Dimension DIM>
void DecompTurk<DIM>::ConvertZtoW( FVec &vfi, const FVec& vin)
{
	vfi = mmtxM_WZ * vin;
}

template <Dimension DIM>
void DecompTurk<DIM>::ConvertWtoQ( FVec &vfi, const FVec& vin)
{
	vfi = mmtxM_QW * vin;
}


template <Dimension DIM>
void DecompTurk<DIM>::ConvertQtoW( FVec &vfi, const FVec& vin)
{
	vfi = mmtxM_WQ * vin;
}


template <Dimension DIM>
void DecompTurk<DIM>::MtxPQ( FMtx& mtxPq, const FVec& vsimp, const MGFloat& beta)
{
	MGFloat tabR[EQSIZE], tabC[EQSIZE];

	mtxPq.Resize( EQSIZE, EQSIZE, 0.0);

	MGFloat c2 = FLOW_K * vsimp( FEqns::ID_P ) / vsimp( FEqns::ID_RHO );
	MGFloat u2 = EqnEuler<DIM>::SqrU( vsimp);
	//MGFloat M2 = u2 / c2;

	MGFloat H = 0.5*u2 + c2 / (FLOW_K-1.);
	MGFloat b2kc2 = (beta*beta - 1.0)*(FLOW_K-1.) / c2;


	tabR[0] = 0.5*u2; 
	tabC[1] = H;
	tabR[1] = tabC[0] = 1.0;

	for ( MGSize i=0; i<=DIM; ++i)
	{
		tabR[i + FEqns::template U<0>::ID] = - vsimp( i + FEqns::template U<0>::ID );
		tabC[i + FEqns::template U<0>::ID] =   vsimp( i + FEqns::template U<0>::ID );
	}


	for ( MGSize i=0; i<EQSIZE; ++i)
		for ( MGSize j=0; j<EQSIZE; ++j)
			mtxPq(i,j) = tabC[i] * tabR[j] * b2kc2;

	for ( MGSize i=0; i<EQSIZE; ++i)
		mtxPq(i,i) += 1.0;

}


//////////////////////////////////////////////////////////////////////
// class DecompCRDTurk
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class DecompCRDTurk
{
	enum { EQSIZE = EquationDef<DIM,EQN_EULER>::SIZE };

    typedef EquationDef<DIM,EQN_EULER> FEqns;
	typedef Vect<DIM>									GVect;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FMtx	FMtx;

public:
	DecompCRDTurk()	{}
	DecompCRDTurk( const FVec& var)	{ Init( var);}

	void	Init( const FVec& var);

	void	Calc( FMtx &smtxKp, FMtx &smtxKm, const GVect &vn, const MGFloat& e, bool& bcorr);

	void	ConvertVtoW( FVec &vfi, const FVec& vin);
	void	ConvertWtoQ( FVec &vfi, const FVec& vin);
	void	ConvertQtoW( FVec &vfi, const FVec& vin);

	void	MtxPQ( FMtx& mtxPq, const FVec& vsimp, const MGFloat& beta);

protected:
	void	AssembleMtx( FMtx &mtx, const GVect &vn, const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 );

private:
	FMtx	mmtxM_WV;
	FMtx	mmtxM_QW;
	FMtx	mmtxM_WQ;
};
//////////////////////////////////////////////////////////////////////



template <Dimension DIM>
void DecompCRDTurk<DIM>::Init( const FVec& var)
{
	FVec varZ, varV;

	varV = var;
	varZ = EqnEuler<DIM>::SimpToZ( varV);


	FMtx	smtxM_QZ, smtxM_ZV, smtxM_VW;

	EqnEuler<DIM>::CJacobQZ( smtxM_QZ, varZ);
	EqnEuler<DIM>::CJacobZV( smtxM_ZV, varZ);
	CJacobVWx<DIM,FMtx,FVec>( smtxM_VW, varV);

	mmtxM_QW = smtxM_QZ * smtxM_ZV * smtxM_VW;

	mmtxM_WQ = mmtxM_QW;
	mmtxM_WQ.Invert();

	CJacobWxV<DIM,FMtx,FVec>( mmtxM_WV, varV);
}


template <Dimension DIM>
void DecompCRDTurk<DIM>::ConvertVtoW( FVec &vfi, const FVec& vin)
{
	vfi = mmtxM_WV * vin;
}

template <Dimension DIM>
void DecompCRDTurk<DIM>::ConvertWtoQ( FVec &vfi, const FVec& vin)
{
	vfi = mmtxM_QW * vin;
}

template <Dimension DIM>
void DecompCRDTurk<DIM>::ConvertQtoW( FVec &vfi, const FVec& vin)
{
	vfi = mmtxM_WQ * vin;
}


template <Dimension DIM>
void DecompCRDTurk<DIM>::MtxPQ( FMtx& mtxPq, const FVec& vsimp, const MGFloat& beta)
{
	MGFloat tabR[EQSIZE], tabC[EQSIZE];

	mtxPq.Resize( EQSIZE, EQSIZE, 0.0);

	MGFloat c2 = FLOW_K * vsimp( FEqns::ID_P ) / vsimp( FEqns::ID_RHO );
	MGFloat u2 = EqnEuler<DIM>::SqrU( vsimp);
	//MGFloat M2 = u2 / c2;

	MGFloat H = 0.5*u2 + c2 / (FLOW_K-1.);
	MGFloat b2kc2 = (beta*beta - 1.0)*(FLOW_K-1.) / c2;


	tabR[0] = 0.5*u2; 
	tabC[1] = H;
	tabR[1] = tabC[0] = 1.0;

	for ( MGSize i=0; i<=DIM; ++i)
	{
		tabR[i + FEqns::template U<0>::ID] = - vsimp( i + FEqns::template U<0>::ID );
		tabC[i + FEqns::template U<0>::ID] =   vsimp( i + FEqns::template U<0>::ID );
	}


	for ( MGSize i=0; i<EQSIZE; ++i)
		for ( MGSize j=0; j<EQSIZE; ++j)
			mtxPq(i,j) = tabC[i] * tabR[j] * b2kc2;

	for ( MGSize i=0; i<EQSIZE; ++i)
		mtxPq(i,i) += 1.0;

}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __DECOMPTURK_H__
