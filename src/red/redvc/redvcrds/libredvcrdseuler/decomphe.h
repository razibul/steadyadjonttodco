#ifndef __DECOMPHE_H__
#define __DECOMPHE_H__


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
/// class DecompHE
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class DecompHE
{
	enum { EQSIZE = EquationDef<DIM,EQN_EULER>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_EULER>::AUXSIZE };

	typedef Vect<DIM>									GVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FMtx	FMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>				CFCell;

	typedef SVector<EQSIZE-2>			FSubVec;
	typedef SMatrix<EQSIZE-2>			FSubMtx;

public:
	DecompHE()	{}
	DecompHE( const FVec& var)		{ Init( var);}

	void	Init( const FVec& var);

	GVec	Lambda();

	void	Calc( FSubMtx &smtxKp, FSubMtx &smtxKm, const GVec &vn, const MGFloat& e);

	void	ConvertVtoX( FVec& vfi, const FVec& vin);
	void	ConvertXtoQ( FVec& vfi, const FVec& vin, const FVec& vno);
	void	ConvertMtxXtoQ( FMtx &mtxfi, const FMtx& mtxin);

protected:
	void	AssembleMtx( FSubMtx &mtxA, const FSubMtx &mtxLi, const FSubMtx &mtxL, const FSubVec &vecD);

private:
	FVec	mV;
	MGFloat	mc;
	MGFloat	mq;
	MGFloat	mM;
	MGFloat	mBeta;
	MGFloat	mChi;

	FMtx	mMtx1, mMtx2;
	FMtx	mMtxM_XV;
	FSubMtx	mMtxR;
	FSubMtx	mMtxPAX[DIM];
};




template <Dimension DIM>
inline void DecompHE<DIM>::Init( const FVec& var)
{
	mV = var;

	mc = ::sqrt( FLOW_K*mV(1)/mV(0) );
	mq = ::sqrt( EqnEuler<DIM>::SqrU( mV) );
	mM = mq/mc;
	mBeta = max( P_LLR_EPS, ::sqrt( fabs(mM*mM-1) ) );
	mChi = mBeta/MAX( mM, 1.0);

	FVec	varZm;
	FMtx	smtxM_ZV, smtxM_VZ, smtxM_ZQ, smtxM_QZ;
	FMtx	smtxM_WV, smtxM_VW, smtxM_XW, smtxM_WX;
	FMtx	smtxP, smtxPI;

	varZm = EqnEuler<DIM>::SimpToZ( mV);

	EqnEuler<DIM>::CJacobZV( smtxM_ZV, varZm);
	EqnEuler<DIM>::CJacobQZ( smtxM_QZ, varZm);

	EqnEuler<DIM>::CJacobVW( smtxM_VW, mV);
	EqnEuler<DIM>::CJacobWX( smtxM_WX, mV, mBeta);

	EqnEuler<DIM>::PrecInvLLR( smtxPI, mV, mBeta, mChi);

	EqnEuler<DIM>::RotMtx( mMtxR, mV);

	for ( MGSize i=0; i<DIM; ++i)
		EqnEuler<DIM>::SubPAXni( i, mMtxPAX[i], mV, mBeta, mChi);

	//smtxM_QZ.Write();
	//smtxM_ZV.Write();
	//smtxM_VW.Write();
	//printf( "qqq\n");

	//mMtx1 = smtxM_QZ * smtxM_ZV * smtxM_VW;
	//mMtx2 = /*smtxPI */ smtxM_WX;	// with preconditioning !!!
	////mMtx2 = smtxPI * smtxM_WX;

	mMtx1 = smtxM_QZ * smtxM_ZV * smtxM_VW;
	//mMtx2 = smtxM_WX;	// with cell preconditioning !!!
	mMtx2 = smtxPI * smtxM_WX; // with no / node preconditioning


	EqnEuler<DIM>::CJacobXW( smtxM_XW, mV, mBeta);
	EqnEuler<DIM>::CJacobWV( smtxM_WV, mV);

	mMtxM_XV = smtxM_XW * smtxM_WV;
}



template<>
inline DecompHE<DIM_2D>::GVec DecompHE<DIM_2D>::Lambda()	
{ 
	return GVec( mV(2),mV(3));
}

template<>
inline DecompHE<DIM_3D>::GVec DecompHE<DIM_3D>::Lambda()	
{ 
	return GVec( mV(2),mV(3),mV(4));
}



template <Dimension DIM>
inline void DecompHE<DIM>::ConvertXtoQ( FVec& vfi, const FVec& vin, const FVec& vno)
{
	FMtx	smtxP, smtxM_VW;
	FMtx	vect, vectVav, vectQav;
	FVec	vectE;

	//vin.Write();

	FVec vnom = vno + mV;

	MGFloat	q = ::sqrt( EqnEuler<DIM>::SqrU( vnom) );
	MGFloat	c = ::sqrt( FLOW_K*vnom(1)/vnom(0) );
	MGFloat	M = q/c;

	MGFloat	beta = max( P_LLR_EPS, ::sqrt( ::fabs(M*M-1) ) );
	MGFloat	chi = beta/max(M,1.0);

	EqnEuler<DIM>::PrecLLR( smtxP, vnom, beta, chi);


	//vfi = mMtx1 * mMtx2 * vin;
	vfi = mMtx1 * smtxP * mMtx2 * vin; // node preconditioning

	//THROW_INTERNAL("BREAK");
}




template <Dimension DIM>
inline void DecompHE<DIM>::ConvertVtoX( FVec& vfi, const FVec& vin)
{
	//MGFloat kx = mq<ZERO ? 1.0 : mV(2)/mq;	// <----------
	//MGFloat ky = mq<ZERO ? 0.0 : mV(3)/mq; 

	//vfi(0) = -mc*mc* vin(0) + vin(1);
	//vfi(1) = vin(1) + mV(0)*( vin(2)*mV(2) + vin(3)*mV(3) );
	////vfi(2) = mBeta/mV(0)/mq*vin(1)  + ( - ky*vin(2) + kx*vin(3) );
	////vfi(3) = mBeta/mV(0)/mq*vin(1)  + (   ky*vin(2) - kx*vin(3) );

	////vfi(0) = -mc*mc* vin(0) + vin(1);
	////vfi(1) = vin(1) + mV(0)*( vin(2)*mV(2) + vin(3)*mV(3) );
	//vfi(2) = mBeta/mV(0)/mq*vin(1)  + ( - vin(2)*mV(3) + vin(3)*mV(2) )/mq;
	//vfi(3) = mBeta/mV(0)/mq*vin(1)  + (   vin(2)*mV(3) - vin(3)*mV(2) )/mq;

	//FMtx	smtxM_WV, smtxM_XW;
	//EqnEuler<DIM>::CJacobXW( smtxM_XW, mV, mBeta);
	//EqnEuler<DIM>::CJacobWV( smtxM_WV, mV);

	vfi = mMtxM_XV * vin;
}

//template <>
//inline void DecompHE<DIM_2D>::ConvertVtoX( const FVec& vin, FVec& vfi)
//{
//	MGFloat kx = mq<ZERO ? 1.0 : mV(2)/mq;	// <----------
//	MGFloat ky = mq<ZERO ? 0.0 : mV(3)/mq; 
//
//	vfi(0) = -mc*mc* vin(0) + vin(1);
//	vfi(1) = vin(1) + mV(0)*( vin(2)*mV(2) + vin(3)*mV(3) );
//	//vfi(2) = mBeta/mV(0)/mq*vin(1)  + ( - ky*vin(2) + kx*vin(3) );
//	//vfi(3) = mBeta/mV(0)/mq*vin(1)  + (   ky*vin(2) - kx*vin(3) );
//
//	//vfi(0) = -mc*mc* vin(0) + vin(1);
//	//vfi(1) = vin(1) + mV(0)*( vin(2)*mV(2) + vin(3)*mV(3) );
//	vfi(2) = mBeta/mV(0)/mq*vin(1)  + ( - vin(2)*mV(3) + vin(3)*mV(2) )/mq;
//	vfi(3) = mBeta/mV(0)/mq*vin(1)  + (   vin(2)*mV(3) - vin(3)*mV(2) )/mq;
//
//	FMtx	smtxM_WV, smtxM_XW;
//	EqnEuler<DIM_2D>::CJacobXW( smtxM_XW, mV, mBeta);
//	EqnEuler<DIM_2D>::CJacobWV( smtxM_WV, mV);
//
//	vfi = smtxM_XW * smtxM_WV * vin;
//}



template <Dimension DIM>
inline void DecompHE<DIM>::Calc( FSubMtx &smtxKp, FSubMtx &smtxKm, const GVec &vn, const MGFloat& e)
{
	FSubMtx	smtx, smtxA;
	FSubMtx	smtxDiag, smtxDm, smtxDp, smtxL, smtxLI, smtxAp, smtxAm;
	FSubVec	vecD, vecDm, vecDp;

	smtxDp.Resize( DIM, DIM);
	smtxDm.Resize( DIM, DIM);


	smtx.Resize(DIM,1);
	for ( MGSize i=0; i<DIM; ++i)
		smtx(i,0) = vn.cX( (MGInt)i);

	smtx = mMtxR * smtx;

	smtxA.Resize( DIM, DIM );
	smtxA.Init( 0.0);
	for ( MGSize i=0; i<DIM; ++i)
		smtxA += mMtxPAX[i]*smtx(i,0);


	Eigen_<FSubVec,FSubMtx>( smtxL, smtxLI, vecD, smtxA);



	for ( MGSize i=0; i<DIM; ++i)
		vecDp(i) = MAX( vecD(i), 0.0);

	AssembleMtx( smtxAp, smtxLI, smtxL, vecDp);


	for ( MGSize i=0; i<DIM; ++i)
		vecDm(i) = MIN( vecD(i), 0.0);

	AssembleMtx( smtxAm, smtxLI, smtxL, vecDm);

	smtxKp = smtxAp;
	smtxKm = smtxAm;


}


template <Dimension DIM>
inline void DecompHE<DIM>::AssembleMtx( FSubMtx &mtxA, const FSubMtx &mtxLi, 
									    const FSubMtx &mtxL, const FSubVec &vecD)
{
	THROW_INTERNAL("Not implemented");
}

template <>
inline void DecompHE<DIM_2D>::AssembleMtx( FSubMtx &mtxA, const FSubMtx &mtxLi, 
									    const FSubMtx &mtxL, const FSubVec &vecD)
{
	mtxA.Resize(DIM_2D,DIM_2D);

	mtxA(0,0) = vecD(0)*mtxLi(0,0)*mtxL(0,0) + vecD(1)*mtxLi(0,1)*mtxL(1,0);
	mtxA(0,1) = vecD(0)*mtxLi(0,0)*mtxL(0,1) + vecD(1)*mtxLi(0,1)*mtxL(1,1);

	mtxA(1,0) = vecD(0)*mtxLi(1,0)*mtxL(0,0) + vecD(1)*mtxLi(1,1)*mtxL(1,0);
	mtxA(1,1) = vecD(0)*mtxLi(1,0)*mtxL(0,1) + vecD(1)*mtxLi(1,1)*mtxL(1,1);
}

template <>
inline void DecompHE<DIM_3D>::AssembleMtx( FSubMtx &mtxA, const FSubMtx &mtxLi, 
									    const FSubMtx &mtxL, const FSubVec &vecD)
{
	mtxA.Resize(DIM_3D,DIM_3D);

	mtxA(0,0) = vecD(0)*mtxLi(0,0)*mtxL(0,0) + vecD(1)*mtxLi(0,1)*mtxL(1,0) + vecD(2)*mtxLi(0,2)*mtxL(2,0);
	mtxA(0,1) = vecD(0)*mtxLi(0,0)*mtxL(0,1) + vecD(1)*mtxLi(0,1)*mtxL(1,1) + vecD(2)*mtxLi(0,2)*mtxL(2,1);
	mtxA(0,2) = vecD(0)*mtxLi(0,0)*mtxL(0,2) + vecD(1)*mtxLi(0,1)*mtxL(1,2) + vecD(2)*mtxLi(0,2)*mtxL(2,2);

	mtxA(1,0) = vecD(0)*mtxLi(1,0)*mtxL(0,0) + vecD(1)*mtxLi(1,1)*mtxL(1,0) + vecD(2)*mtxLi(1,2)*mtxL(2,0);
	mtxA(1,1) = vecD(0)*mtxLi(1,0)*mtxL(0,1) + vecD(1)*mtxLi(1,1)*mtxL(1,1) + vecD(2)*mtxLi(1,2)*mtxL(2,1);
	mtxA(1,2) = vecD(0)*mtxLi(1,0)*mtxL(0,2) + vecD(1)*mtxLi(1,1)*mtxL(1,2) + vecD(2)*mtxLi(1,2)*mtxL(2,2);

	mtxA(2,0) = vecD(0)*mtxLi(2,0)*mtxL(0,0) + vecD(1)*mtxLi(2,1)*mtxL(1,0) + vecD(2)*mtxLi(2,2)*mtxL(2,0);
	mtxA(2,1) = vecD(0)*mtxLi(2,0)*mtxL(0,1) + vecD(1)*mtxLi(2,1)*mtxL(1,1) + vecD(2)*mtxLi(2,2)*mtxL(2,1);
	mtxA(2,2) = vecD(0)*mtxLi(2,0)*mtxL(0,2) + vecD(1)*mtxLi(2,1)*mtxL(1,2) + vecD(2)*mtxLi(2,2)*mtxL(2,2);

}

/*
template <>
inline void DecompHE<DIM_2D>::Calc( FSubMtx& smtxKp, FSubMtx& smtxKm, const GVec&vn, const MGFloat& e)
{
	FSubMtx	smtx, smtxR, smtxA, smtxPAX, smtxPBX;
	FSubMtx	smtxDiag, smtxDm, smtxDp, smtxL, smtxLI, smtxAp, smtxAm;

	smtxDp.Resize( 2, 2);
	smtxDm.Resize( 2, 2);

	EqnEuler<DIM_2D>::RotMtx( smtxR, mV);

	smtx.Resize(2,1);
	smtx(0,0) = vn.X();
	smtx(1,0) = vn.Y();

	smtx = smtxR * smtx;

	EqnEuler<DIM_2D>::SubPAXn( smtxPAX, mV, mBeta, mChi);
	EqnEuler<DIM_2D>::SubPBXn( smtxPBX, mV, mBeta, mChi);
	smtxA = smtxPAX*smtx(0,0) + smtxPBX*smtx(1,0);

	//Flow::SubPAX_2D( smtxPAX, mV, mBeta, mChi);
	//Flow::SubPBX_2D( smtxPBX, mV, mBeta, mChi);
	//smtxA = smtxPAX*vn.X() + smtxPBX*vn.Y();

	Flow::Eigen2x2( smtxL, smtxLI, smtxDiag, smtxA);


	smtxDp(0,0) = MAX( smtxDiag(0,0), 0.0);
	smtxDp(1,1) = MAX( smtxDiag(1,1), 0.0);

	smtxAp = smtxLI * smtxDp * smtxL;


	smtxDm(0,0) = MIN( smtxDiag(0,0), 0.0);
	smtxDm(1,1) = MIN( smtxDiag(1,1), 0.0);

	smtxAm = smtxLI * smtxDm * smtxL;

	smtxKp = smtxAp;
	smtxKm = smtxAm;
}
*/


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __DECOMPHE_H__
