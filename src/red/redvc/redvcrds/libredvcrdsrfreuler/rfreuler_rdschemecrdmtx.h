#ifndef __RFR_EULERRDSCHEMECRDMTX_H__
#define __RFR_EULERRDSCHEMECRDMTX_H__


#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"
#include "redvc/libredvccommon/flowfunc.h"
#include "redvc/libredvcgrid/compgeomcell_integrator.h"


#include "libredphysics/eulerequations.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




template <Dimension DIM>
class RfrFluxFunc_Simp
{
public:
	typedef EquationDef<DIM,EQN_RFR_EULER> Eqn;
	typedef typename EquationDef<DIM,EQN_RFR_EULER>::FVec	FVec;


	RfrFluxFunc_Simp( const Physics<DIM,EQN_RFR_EULER>* pphys) : mpPhysics(pphys) {}

	FVec operator() ( const Vect<DIM>& pos, const FVec& var, const Vect<DIM>& vn ) const
	{ 
		FVec varV = var;

		Vect<DIM> vecU;
		for ( MGSize i=0; i<DIM; ++i)
			vecU.rX(i) = varV(i + Eqn::template U<0>::ID );

		MGFloat U = vn * vecU;
		MGFloat U2 = vecU*vecU;

		Vect<DIM> vrelu = mpPhysics->cRfrDef().BkgVelocity( pos );
		MGFloat relU = vn * vrelu;

		FVec flux;

		flux(Eqn::ID_RHO) = (U - relU) * varV(Eqn::ID_RHO);
		flux(Eqn::ID_P)   = (U - relU) * ( varV(Eqn::ID_P) / (FLOW_K-1.) + 0.5*U2*varV(Eqn::ID_RHO) )  + U * varV(Eqn::ID_P);

		for ( MGSize i=0; i<DIM; ++i)
			flux( i + Eqn::template U<0>::ID ) = (U - relU) * varV(Eqn::ID_RHO) * vecU.cX(i)  +  vn.cX(i) * varV(Eqn::ID_P);

		//FVec fluxo;
		//fluxo(Eqn::ID_RHO) = U * varV(Eqn::ID_RHO);
		//fluxo(Eqn::ID_P)   = U/(FLOW_K-1.) * ( 0.5*U2*(FLOW_K-1.)*varV(Eqn::ID_RHO) + FLOW_K*varV(Eqn::ID_P) );

		//for ( MGSize i=0; i<DIM; ++i)
		//	fluxo( i + Eqn::template U<0>::ID ) = U * varV(Eqn::ID_RHO) * vecU.cX(i)  +  vn.cX(i) * varV(Eqn::ID_P);

		return flux; 
	}

private:
	const Physics<DIM,EQN_RFR_EULER>* mpPhysics;

};



//////////////////////////////////////////////////////////////////////
// class RfrRDSchemeCRDMtx
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class MTXRDS>
class RfrRDSchemeCRDMtx : public FlowFunc<DIM, EQN_RFR_EULER, EquationDef<DIM,EQN_RFR_EULER>::SIZE>
{
	enum { EQSIZE = EquationDef<DIM,EQN_RFR_EULER>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_RFR_EULER>::AUXSIZE };

	typedef EquationDef<DIM,EQN_RFR_EULER> ETEuler;

	typedef Vect<DIM>									GVec;
	typedef typename EquationDef<DIM,EQN_RFR_EULER>::FVec	FVec;
	typedef typename EquationDef<DIM,EQN_RFR_EULER>::FMtx	FMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>				CFCell;

public:
	RfrRDSchemeCRDMtx()	{}
	RfrRDSchemeCRDMtx( const FVec& var) : mRefVar(var)	{}

	virtual void	Init( const Physics<DIM,EQN_RFR_EULER>& physics);

	virtual void	CalcResiduum( const CFCell& fcell, FVec tabres[]);
	virtual void	CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

	void	ApplyPrecond( FVec& res, const FVec& vsimpl )	{}

protected:
	void	Initialize( const CFCell& fcell);
	void	FinalizeRes( FVec tabres[]);
	void	FinalizeResJacob( FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

private:
	const Physics<DIM,EQN_RFR_EULER>* mpPhysics;

	RfrDecompMtx<DIM>	mDecomp;

	//MGFloat	mRefOmega;

	FVec	mRefVar;
	FVec	mvarVm;
	FVec	mtabsV[DIM+1];

	FMtx	mtabKp[DIM+1];
	FMtx	mtabKm[DIM+1];
	FMtx	mtabK[DIM+1];

	FVec	mtabsFi[DIM+1];
	FMtx	mmtxsJ[DIM+1][DIM+1];

	struct RDSCalc : public MTXRDS {} calc;

};
//////////////////////////////////////////////////////////////////////





template <Dimension DIM, class MTXRDS>
void RfrRDSchemeCRDMtx<DIM,MTXRDS>::Init( const Physics<DIM,EQN_RFR_EULER>& physics)
{
	mpPhysics = &physics;

	physics.DimToUndim( mRefVar, physics.cRefVar(), physics.cRefVar() );

	MGFloat uref = ::sqrt( EqnEuler<DIM>::SqrU( physics.cRefVar() ) ) / physics.cRefVar()( ETEuler::ID_RHO);
	MGFloat oref = uref / physics.cRefLength();

	//mRefVelocity = physics.cRefVelocity() / uref;
	//mRefOmega = physics.cRefOmega() / oref;

}


template <Dimension DIM, class MTXRDS>
inline void RfrRDSchemeCRDMtx<DIM,MTXRDS>::Initialize( const CFCell& fcell)
{
	FVec	var;

	//mRefVar.Write();
	//THROW_INTERNAL( "STOP");

	//MGFloat	tabv[3] = {2,2,1};
	//Vect<MGFloat,DIM> vvv = fcell.GradVar( tabv);


	GVec center(0.0);
	mvarVm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
	{
		var = EqnEuler<DIM>::ConservToSimp( fcell.cVar(i) );
		var(EquationDef<DIM,EQN_RFR_EULER>::ID_RHO) = max( 1.0e-10, var(EquationDef<DIM,EQN_RFR_EULER>::ID_RHO) );
		var(EquationDef<DIM,EQN_RFR_EULER>::ID_P)   = max( 1.0e-10, var(EquationDef<DIM,EQN_RFR_EULER>::ID_P) );

		mvarVm += var;
		mtabsV[i] = var;
		center += fcell.cNode(i).cPos();
	}

	mvarVm /= (MGFloat)(DIM+1);
	center /= (MGFloat)(DIM+1);


	GVec	tabVn[DIM+1];
	for ( MGSize i=0; i<=DIM; ++i)
		tabVn[i] = fcell.Vn(i);

	//mDecomp.Init( mvarVm, mtabsV, tabVn);
	GVec vrelu = mpPhysics->cRfrDef().BkgVelocity( center );
	mDecomp.Init( mvarVm, vrelu, mtabsV, tabVn);

	FMtx	smtxAp, smtxAm;
	MGFloat	vnmod;
	GVec	vn;
	bool	bc;

	for ( MGSize i=0; i<=DIM; ++i)
	{
		vn = fcell.Vn(i).versor();

		mDecomp.Calc( smtxAp, smtxAm, vn, 1.0e-9, bc );
		//mDecomp.Calc( smtxAp, smtxAm, vn, 1.0e-7, bc );
		//mDecomp.Calc( smtxAp, smtxAm, vn, 0, bc );

		vnmod = fcell.Vn(i).module() / (MGFloat)DIM;


		MtxMultMtxD( mtabKp[i], smtxAp, vnmod);
		MtxMultMtxD( mtabKm[i], smtxAm, vnmod);
		//mtabKp[i] = vnmod * smtxAp;
		//mtabKm[i] = vnmod * smtxAm;
	}
}


template <Dimension DIM, class MTXRDS>
inline void RfrRDSchemeCRDMtx<DIM,MTXRDS>::FinalizeRes( FVec tabres[])		
{
	for ( MGSize i=0; i<=DIM; ++i)
		mDecomp.ConvertVtoQ( tabres[i], mtabsFi[i]);
}

template <Dimension DIM, class MTXRDS>
inline void RfrRDSchemeCRDMtx<DIM,MTXRDS>::FinalizeResJacob( FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
	for ( MGSize i=0; i<=DIM; ++i)
	{
		mDecomp.ConvertVtoQ( tabres[i], mtabsFi[i]);

		for ( MGSize k=0; k<=DIM; ++k)
			mDecomp.ConvertMtxVtoQ( mtxres[i][k], mmtxsJ[i][k]);
	}
}


template <Dimension DIM, class MTXRDS>
inline void RfrRDSchemeCRDMtx<DIM,MTXRDS>::CalcResiduum( const CFCell& fcell, FVec tabres[])		
{
	Initialize( fcell);

	//FVec fluxSimp = FluxInegrator< DIM, typename FluxFunc_Simp<DIM>::FVec >( fcell, 3).template IntegrateFlux< FluxFunc_Simp<DIM> >( mtabsV);

	RfrFluxFunc_Simp<DIM> fluxfunc( mpPhysics); 
	FVec fluxSimp = CellInegrator< DIM >( fcell, 5).template BoundaryIntegral< FVec, FVec, RfrFluxFunc_Simp<DIM> >( fluxfunc, mtabsV);

	FMtx	smtxM_VQ;
	EqnEuler<DIM>::CJacobVQ( smtxM_VQ, mvarVm);

	FVec flux = smtxM_VQ * fluxSimp;

	calc.Calc( EQSIZE, mtabKp, mtabKm, mtabsV, flux, mtabsFi);
	FinalizeRes( tabres);


	FVec src;
	src.Init(0.0);

	GVec velo;
	for ( MGSize i=0; i<DIM; ++i)
		velo.rX(i) = mvarVm(i+2);

	GVec oxv = mpPhysics->cRfrDef().OmegaCross( velo );

	for ( MGSize i=0; i<DIM; ++i)
		src(i+2) = mvarVm(0) * oxv.cX(i) * fcell.Volume();


	//for ( MGSize k=0; k<=DIM; ++k)
	//{
	//	tabres[k](2) -= src(2) / 3.;
	//	tabres[k](3) -= src(3) / 3.;
	//}

	for ( MGSize k=0; k<=DIM; ++k)
	{
		tabres[k] -= src / (DIM+1);
	}

}



template <Dimension DIM, class MTXRDS>
inline void RfrRDSchemeCRDMtx<DIM,MTXRDS>::CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
	Initialize( fcell);

	calc.Calc( EQSIZE, mtabKp, mtabKm, mtabsV, mtabsFi, mmtxsJ);

	FinalizeResJacob( tabres, mtxres);
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//   

#endif // __RFR_EULERRDSCHEMECRDMTX_H__
