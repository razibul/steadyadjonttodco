#ifndef __RFR_EULERRDSCHEMEMTX_H__
#define __RFR_EULERRDSCHEMEMTX_H__



#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"
#include "redvc/libredvccommon/flowfunc.h"

#include "libredphysics/eulerequations.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class RfrDecompMtx
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class RfrDecompMtx
{
	enum { EQSIZE = EquationDef<DIM,EQN_RFR_EULER>::SIZE };
	typedef Vect<DIM>										GVect;
	typedef typename EquationDef<DIM,EQN_RFR_EULER>::FVec	FVec;
	typedef typename EquationDef<DIM,EQN_RFR_EULER>::FMtx	FMtx;

public:
	RfrDecompMtx()	{}
	RfrDecompMtx( const FVec& var, const FVec tab[], const GVect tabn[])	{ Init( var, tab, tabn);}

	void	Init( const FVec& var, const GVect& relu, const FVec tab[], const GVect tabn[]);

	void	Calc( FMtx &smtxKp, FMtx &smtxKm, const GVect &vn, const MGFloat& e, bool& bcorr);

	void	ConvertVtoQ( FVec &vfi, const FVec& vin);
	void	ConvertMtxVtoQ( FMtx &mtxfi, const FMtx& mtxin);

protected:
	void	AssembleMtx( FMtx &mtx, const GVect &vn, const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 );

private:
	FVec	mV;
	MGFloat	mc;
	MGFloat	mU2;
	GVect	mRelU;

	GVect	mVnShock;
	GVect	mtabVn[DIM+1];
	FVec	mtabsV[DIM+1];
};
//////////////////////////////////////////////////////////////////////





template <Dimension DIM>
inline void RfrDecompMtx<DIM>::Init( const FVec& var, const GVect& relu, const FVec tab[], const GVect tabn[])
{
	mV = var;
	mc = ::sqrt( FLOW_K*mV(1)/mV(0) );
	mU2 = EqnEuler<DIM>::SqrU(var);

	mRelU = relu;

	for ( MGSize i=0; i<=DIM; ++i)
	{
		mtabsV[i] = tab[i];
		mtabVn[i] = tabn[i];
	}

}



template <Dimension DIM>
inline void RfrDecompMtx<DIM>::ConvertVtoQ( FVec& vfi, const FVec& vin)
{
	THROW_INTERNAL( "Not implemented !");
}

template <>
inline void RfrDecompMtx<DIM_2D>::ConvertVtoQ( FVec& vfi, const FVec& vin)
{
	vfi(0) = vin(0);

	vfi(1) =   vin(0) * 0.5 * mU2
			 + vin(1)/( FLOW_K - 1.0) 
			 + vin(2) * mV(0) * mV(2)
			 + vin(3) * mV(0) * mV(3);

	vfi(2) =   vin(0) * mV(2)
			 + vin(2) * mV(0);

	vfi(3) =   vin(0) * mV(3)
			 + vin(3) * mV(0);
}

template <>
inline void RfrDecompMtx<DIM_3D>::ConvertVtoQ( FVec& vfi, const FVec& vin)
{
	vfi(0) = vin(0);

	vfi(1) =   vin(0) * 0.5 * mU2
			 + vin(1)/( FLOW_K - 1.0) 
			 + vin(2) * mV(0) * mV(2)
			 + vin(3) * mV(0) * mV(3)
			 + vin(4) * mV(0) * mV(4);

	vfi(2) =   vin(0) * mV(2)
			 + vin(2) * mV(0);

	vfi(3) =   vin(0) * mV(3)
			 + vin(3) * mV(0);

	vfi(4) =   vin(0) * mV(4)
			 + vin(4) * mV(0);
}


template <>
inline void RfrDecompMtx<DIM_2D>::ConvertMtxVtoQ( FMtx &mtxfi, const FMtx& mtxin)
{
	for ( MGSize i=0; i<mtxin.NCols(); ++i)
	{
		mtxfi(0,i) =   mtxin(0,i);

		mtxfi(1,i) =   mtxin(0,i) * 0.5 * mU2
				+ mtxin(1,i)/( FLOW_K - 1.0) 
				+ mtxin(2,i) * mV(0) * mV(2)
				+ mtxin(3,i) * mV(0) * mV(3);

		mtxfi(2,i) =   mtxin(0,i) * mV(2)
				+ mtxin(2,i) * mV(0);

		mtxfi(3,i) =   mtxin(0,i) * mV(3)
				+ mtxin(3,i) * mV(0);

	}
}


template <>
inline void RfrDecompMtx<DIM_3D>::ConvertMtxVtoQ( FMtx &mtxfi, const FMtx& mtxin)
{
	for ( MGSize i=0; i<mtxin.NCols(); ++i)
	{
		mtxfi(0,i) =   mtxin(0,i);

		mtxfi(1,i) =   mtxin(0,i) * 0.5 * mU2
				+ mtxin(1,i)/( FLOW_K - 1.0) 
				+ mtxin(2,i) * mV(0) * mV(2)
				+ mtxin(3,i) * mV(0) * mV(3)
				+ mtxin(4,i) * mV(0) * mV(4);

		mtxfi(2,i) =   mtxin(0,i) * mV(2)
				+ mtxin(2,i) * mV(0);

		mtxfi(3,i) =   mtxin(0,i) * mV(3)
				+ mtxin(3,i) * mV(0);

		mtxfi(4,i) =   mtxin(0,i) * mV(4)
				+ mtxin(4,i) * mV(0);
	}
}





template <Dimension DIM>
void RfrDecompMtx<DIM>::AssembleMtx( FMtx &mtx, const GVect &vn, const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 )
{
	THROW_INTERNAL( "Not implemented");
}


template <>
inline void RfrDecompMtx<DIM_2D>::AssembleMtx( FMtx &mtx, const GVect &vn, 
										   const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 )
{
	const MGFloat l2m3 = l2 - l3;
	const MGFloat l2p3 = l2 + l3;
	const MGFloat l2p3m1 = 0.5*l2p3 - l1;
	const MGFloat ltmp1 =  0.5*mV(0)*l2m3;

	mtx.Resize( EQSIZE, EQSIZE);
	//mtx.Init( 0.0);


	mtx(0,0) = l1;
	mtx(0,1) = 0.5*(l2p3 - 2*l1)/(mc*mc);
	mtx(0,2) = vn.cX()* ltmp1 /mc;
	mtx(0,3) = vn.cY()* ltmp1 /mc;

	mtx(1,0) = 0.0;
	mtx(1,1) = 0.5*l2p3;
	mtx(1,2) = vn.cX()*mc* ltmp1;
	mtx(1,3) = vn.cY()*mc* ltmp1;

	mtx(2,0) = 0.0;
	mtx(2,1) = 0.5*vn.cX()*l2m3 / ( mc*mV(0) );
	mtx(2,2) = l1*vn.cY()*vn.cY() + 0.5*vn.cX()*vn.cX()*l2p3;
	mtx(2,3) = vn.cX()*vn.cY()*l2p3m1;

	mtx(3,0) = 0.0;
	mtx(3,1) = 0.5*vn.cY()*l2m3 / ( mc*mV(0) );
	mtx(3,2) = vn.cX()*vn.cY()*l2p3m1;
	mtx(3,3) = l1*vn.cX()*vn.cX() + 0.5*vn.cY()*vn.cY()*l2p3;

	//mtx.Resize( DIM_2D+2, DIM_2D+2);
	//mtx.Init( 0.0);

	//mtx(0,0) = l1;
	//mtx(0,1) = 0.5*(l2+l3 - 2*l1)/(mc*mc);
	//mtx(0,2) = 0.5*vn.X()*mV(0)*(l2 - l3)/mc;
	//mtx(0,3) = 0.5*vn.Y()*mV(0)*(l2 - l3)/mc;

	//mtx(1,1) = 0.5*( l2 + l3);
	//mtx(1,2) = 0.5*vn.X()*mc*mV(0)*( l2 - l3);
	//mtx(1,3) = 0.5*vn.Y()*mc*mV(0)*( l2 - l3);

	//mtx(2,1) = 0.5*vn.X()*( l2 - l3) / ( mc*mV(0) );
	//mtx(2,2) = l1*vn.Y()*vn.Y() + 0.5*vn.X()*vn.X()*( l2 + l3);
	//mtx(2,3) = vn.X()*vn.Y()*( -l1 + 0.5*( l2 + l3) );

	//mtx(3,1) = 0.5*vn.Y()*( l2 - l3) / ( mc*mV(0) );
	//mtx(3,2) = vn.X()*vn.Y()*( -l1 + 0.5*( l2 + l3) );
	//mtx(3,3) = l1*vn.X()*vn.X() + 0.5*vn.Y()*vn.Y()*( l2 + l3);
}


template <>
inline void RfrDecompMtx<DIM_3D>::AssembleMtx( FMtx &mtx, const GVect &vn, 
										   const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 )
{
	const MGFloat l2m3 = l2 - l3;
	const MGFloat l2p3 = l2 + l3;
	const MGFloat l2p3m1 = 0.5*l2p3 - l1;
	const MGFloat romc = mc * mV(0);

	const MGFloat ltmp1 =  0.5*mV(0)*l2m3;

	mtx.Resize( EQSIZE, EQSIZE);
	//mtx.Init( 0.0);

	mtx(0,0) = l1;
	mtx(0,1) = 0.5*(l2p3 - 2*l1)/(mc*mc);
	mtx(0,2) = vn.cX()* ltmp1/mc;
	mtx(0,3) = vn.cY()* ltmp1/mc;
	mtx(0,4) = vn.cZ()* ltmp1/mc;

	mtx(1,0) = 0.0;
	mtx(1,1) = 0.5*l2p3;
	mtx(1,2) = 0.5*vn.cX()*romc*l2m3;
	mtx(1,3) = 0.5*vn.cY()*romc*l2m3;
	mtx(1,4) = 0.5*vn.cZ()*romc*l2m3;

	mtx(2,0) = 0.0;
	mtx(2,1) = 0.5*vn.cX()*l2m3 / romc;
	mtx(2,2) = l1*( vn.cY()*vn.cY() + vn.cZ()*vn.cZ() ) + 0.5*vn.cX()*vn.cX()*l2p3;
	mtx(2,3) = vn.cX()*vn.cY()*l2p3m1;
	mtx(2,4) = vn.cX()*vn.cZ()*l2p3m1;

	mtx(3,0) = 0.0;
	mtx(3,1) = 0.5*vn.cY()*l2m3 / romc;
	mtx(3,2) = vn.cX()*vn.cY()*l2p3m1;
	mtx(3,3) = l1*( vn.cX()*vn.cX() + vn.cZ()*vn.cZ() ) + 0.5*vn.cY()*vn.cY()*l2p3;
	mtx(3,4) = vn.cY()*vn.cZ()*l2p3m1;

	mtx(4,0) = 0.0;
	mtx(4,1) = 0.5*vn.cZ()*l2m3 / romc;
	mtx(4,2) = vn.cX()*vn.cZ()*l2p3m1;
	mtx(4,3) = vn.cY()*vn.cZ()*l2p3m1;
	mtx(4,4) = l1*( vn.cX()*vn.cX() + vn.cY()*vn.cY() ) + 0.5*vn.cZ()*vn.cZ()*l2p3;

	//mtx(0,0) = l1;
	//mtx(0,1) = 0.5*(l2+l3 - 2*l1)/(mc*mc);
	//mtx(0,2) = 0.5*vn.X()*mV(0)*(l2 - l3)/mc;
	//mtx(0,3) = 0.5*vn.Y()*mV(0)*(l2 - l3)/mc;
	//mtx(0,4) = 0.5*vn.Z()*mV(0)*(l2 - l3)/mc;

	//mtx(1,1) = 0.5*( l2 + l3);
	//mtx(1,2) = 0.5*vn.X()*mc*mV(0)*( l2 - l3);
	//mtx(1,3) = 0.5*vn.Y()*mc*mV(0)*( l2 - l3);
	//mtx(1,4) = 0.5*vn.Z()*mc*mV(0)*( l2 - l3);

	//mtx(2,1) = 0.5*vn.X()*( l2 - l3) / ( mc*mV(0) );
	//mtx(2,2) = l1*( vn.Y()*vn.Y() + vn.Z()*vn.Z() ) + 0.5*vn.X()*vn.X()*( l2 + l3);
	//mtx(2,3) = vn.X()*vn.Y()*( -l1 + 0.5*( l2 + l3) );
	//mtx(2,4) = vn.X()*vn.Z()*( -l1 + 0.5*( l2 + l3) );

	//mtx(3,1) = 0.5*vn.Y()*( l2 - l3) / ( mc*mV(0) );
	//mtx(3,2) = vn.X()*vn.Y()*( -l1 + 0.5*( l2 + l3) );
	//mtx(3,3) = l1*( vn.X()*vn.X() + vn.Z()*vn.Z() ) + 0.5*vn.Y()*vn.Y()*( l2 + l3);
	//mtx(3,4) = vn.Y()*vn.Z()*( -l1 + 0.5*( l2 + l3) );

	//mtx(4,1) = 0.5*vn.Z()*( l2 - l3) / ( mc*mV(0) );
	//mtx(4,2) = vn.X()*vn.Z()*( -l1 + 0.5*( l2 + l3) );
	//mtx(4,3) = vn.Y()*vn.Z()*( -l1 + 0.5*( l2 + l3) );
	//mtx(4,4) = l1*( vn.X()*vn.X() + vn.Y()*vn.Y() ) + 0.5*vn.Z()*vn.Z()*( l2 + l3);
}



template <Dimension DIM>
inline void RfrDecompMtx<DIM>::Calc( FMtx &smtxKp, FMtx &smtxKm, const GVect &vn, const MGFloat& e, bool& bcorr)
{
	MGFloat l[3], lp[3], lm[3];
	MGFloat q = 0.0;

	for ( MGSize k=0; k<DIM; ++k)
		q += mV(2+k)*vn.cX(k);

	MGFloat relu = mRelU * vn;

	l[0] = q - relu;
	l[1] = q - relu + mc;
	l[2] = q - relu - mc;


	for ( MGSize i=0; i<3; ++i)
	{
		//MGFloat ls = ::fabs( l[i]);
		//if ( ls < e)
		//{
		//	//ls = (ls*ls/e + e);
		//	ls = e;
		//	bcorr = true;
		//}
		////ls = fabs( l[i]); 

		//lp[i] = 0.5*( l[i] + ls );
		//lm[i] = 0.5*( l[i] - ls );

		lp[i] = MAX( l[i],  e );
		lm[i] = MIN( l[i], -e );
	}

	AssembleMtx( smtxKp, vn, lp[0], lp[1], lp[2] );
	AssembleMtx( smtxKm, vn, lm[0], lm[1], lm[2] );

	//AssembleMtx( smtxKp, vn, MAX(l[0],0.0), MAX(l[1],0.0), MAX(l[2],0.0) );
	//AssembleMtx( smtxKm, vn, MIN(l[0],0.0), MIN(l[1],0.0), MIN(l[2],0.0) );
}




//////////////////////////////////////////////////////////////////////
// class RfrRDSchemeMtx
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class MTXRDS>
class RfrRDSchemeMtx : public FlowFunc<DIM, EQN_RFR_EULER, EquationDef<DIM,EQN_RFR_EULER>::SIZE>
{
	enum { EQSIZE = EquationDef<DIM,EQN_RFR_EULER>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_RFR_EULER>::AUXSIZE };

	typedef EquationDef<DIM,EQN_RFR_EULER> ETEuler;

	typedef Vect<DIM>										GVec;
	typedef typename EquationDef<DIM,EQN_RFR_EULER>::FVec	FVec;
	typedef typename EquationDef<DIM,EQN_RFR_EULER>::FMtx	FMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>					CFCell;

public:
	RfrRDSchemeMtx()	{}
	RfrRDSchemeMtx( const FVec& var) : mRefVar(var)	{}

	virtual void	Init( const Physics<DIM,EQN_RFR_EULER>& physics);

	virtual void	CalcResiduum( const CFCell& fcell, FVec tabres[]);
	virtual void	CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

	void	ApplyPrecond( FVec& res, const FVec& vsimpl )	{}

protected:
	void	Initialize( const CFCell& fcell);
	void	FinalizeRes( FVec tabres[]);
	void	FinalizeResJacob( FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

private:
	const Physics<DIM,EQN_RFR_EULER>* mpPhysics;

	RfrDecompMtx<DIM>	mDecomp;

	//GVec	mRefVelocity;
	//MGFloat	mRefOmega;

	FVec	mRefVar;
	FVec	mvarZm, mvarVm;
	FVec	mtabsV[DIM+1];
	FVec	mtabsZ[DIM+1];

	FMtx	mtabKp[DIM+1];
	FMtx	mtabKm[DIM+1];
	FMtx	mtabK[DIM+1];

	FVec	mtabsFi[DIM+1];
	FMtx	mmtxsJ[DIM+1][DIM+1];

	struct RDSCalc : public MTXRDS {} calc;

};
//////////////////////////////////////////////////////////////////////





template <Dimension DIM, class MTXRDS>
void RfrRDSchemeMtx<DIM,MTXRDS>::Init( const Physics<DIM,EQN_RFR_EULER>& physics)
{
	mpPhysics = &physics;

	physics.DimToUndim( mRefVar, physics.cRefVar(), physics.cRefVar() );

	MGFloat uref = ::sqrt( EqnEuler<DIM>::SqrU( physics.cRefVar() ) ) / physics.cRefVar()( ETEuler::ID_RHO);
	MGFloat oref = uref / physics.cRefLength();

	//mRefVelocity = physics.cRefVelocity() / uref;
	//mRefOmega = physics.cRefOmega() / oref;

	//cout << physics.cRefVelocity() << endl;
	//cout << physics.cRefOmega() << endl;
	//cout << physics.cRefAxisOrig() << endl;
	//cout << physics.cRefAxisDir() << endl;
	//
	//cout << endl;
	//cout << mRefVelocity << endl;
	//cout << mRefOmega << endl;
}


template <Dimension DIM, class MTXRDS>
inline void RfrRDSchemeMtx<DIM,MTXRDS>::Initialize( const CFCell& fcell)
{
	FVec	var;

	//mRefVar.Write();
	//THROW_INTERNAL( "STOP");

	//MGFloat	tabv[3] = {2,2,1};
	//Vect<MGFloat,DIM> vvv = fcell.GradVar( tabv);

	GVec center(0.0);

	mvarZm.Init( 0.0);
	mvarVm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
	{
		var = EqnEuler<DIM>::ConservToZ( fcell.cVar(i) );
		var(EquationDef<DIM,EQN_RFR_EULER>::ID_RHO) = max( 1.0e-10, var(EquationDef<DIM,EQN_RFR_EULER>::ID_RHO) );
		var(EquationDef<DIM,EQN_RFR_EULER>::ID_P)   = max( 1.0e-10, var(EquationDef<DIM,EQN_RFR_EULER>::ID_P) );

		mvarZm += var;
		mtabsZ[i] = mtabsV[i] = var;
		center += fcell.cNode(i).cPos();
	}

	mvarZm /= (MGFloat)(DIM+1);
	mvarVm = EqnEuler<DIM>::ZToSimp( mvarZm);
	center /= (MGFloat)(DIM+1);


	FMtx	smtxM_VZ;
	EqnEuler<DIM>::CJacobVZ( smtxM_VZ, mvarZm);

	for ( MGSize i=0; i<=DIM; ++i)
		mtabsV[i] = smtxM_VZ * mtabsV[i];

	GVec	tabVn[DIM+1];
	for ( MGSize i=0; i<=DIM; ++i)
		tabVn[i] = fcell.Vn(i);

	GVec vrelu = mpPhysics->cRfrDef().BkgVelocity( center );
	mDecomp.Init( mvarVm, vrelu, mtabsV, tabVn);

	FMtx	smtxAp, smtxAm;
	MGFloat	vnmod;
	GVec	vn;
	bool	bc;

	for ( MGSize i=0; i<=DIM; ++i)
	{
		vn = fcell.Vn(i).versor();

		mDecomp.Calc( smtxAp, smtxAm, vn, 1.0e-9, bc );
		//mDecomp.Calc( smtxAp, smtxAm, vn, 1.0e-7, bc );
		//mDecomp.Calc( smtxAp, smtxAm, vn, 0, bc );

		vnmod = fcell.Vn(i).module() / (MGFloat)DIM;


		MtxMultMtxD( mtabKp[i], smtxAp, vnmod);
		MtxMultMtxD( mtabKm[i], smtxAm, vnmod);
		//mtabKp[i] = vnmod * smtxAp;
		//mtabKm[i] = vnmod * smtxAm;

	}
}


template <Dimension DIM, class MTXRDS>
inline void RfrRDSchemeMtx<DIM,MTXRDS>::FinalizeRes( FVec tabres[])		
{
	for ( MGSize i=0; i<=DIM; ++i)
		mDecomp.ConvertVtoQ( tabres[i], mtabsFi[i]);
}

template <Dimension DIM, class MTXRDS>
inline void RfrRDSchemeMtx<DIM,MTXRDS>::FinalizeResJacob( FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
	for ( MGSize i=0; i<=DIM; ++i)
	{
		mDecomp.ConvertVtoQ( tabres[i], mtabsFi[i]);

		for ( MGSize k=0; k<=DIM; ++k)
			mDecomp.ConvertMtxVtoQ( mtxres[i][k], mmtxsJ[i][k]);
	}
}


template <Dimension DIM, class MTXRDS>
inline void RfrRDSchemeMtx<DIM,MTXRDS>::CalcResiduum( const CFCell& fcell, FVec tabres[])		
{

	Initialize( fcell);

	calc.Calc( EQSIZE, mtabKp, mtabKm, mtabsV, mtabsFi);

	FinalizeRes( tabres);

	//class SourceFunc
	//{
	//public:
	//	SourceFunc( const MGFloat& o=0.0) : mRefOmega(o) {}

	//	FVec operator() ( const Vect<DIM>& pos, const FVec& var, const MGFloat& s ) const
	//	{
	//		FVec res;
	//		res.Init(0.0);
	//		for ( MGSize i=2; i<EQSIZE; ++i )
	//			res(i) = mRefOmega * var(0) * var(i) * s;

	//		return res;
	//	}

	//	FVec operator() ( const Vect<DIM>& pos, const FVec& var ) const
	//	{
	//		FVec res;
	//		res.Init(0.0);
	//		for ( MGSize i=2; i<EQSIZE; ++i )
	//			res(i) = mRefOmega * var(0) * var(i);

	//		return res;
	//	}
	//private:
	//	MGFloat	mRefOmega;
	//};

	//SourceFunc srcfunc( mRefOmega);


	//FVec srctab[DIM+1];
	//for ( MGSize i=0; i<=DIM; ++i )
	//{
	//	MGFloat tabs[DIM+1];
	//	for ( MGSize k=0; k<=DIM; ++k)
	//		tabs[k] = k==i ? 1 : 0;

	//	srctab[i] = 
	//		CellInegrator< DIM >( fcell, 3).template VolumeIntegralPlus< FVec, MGFloat, FVec, SourceFunc >
	//		( srcfunc, mtabsZ, tabs);

	//}


	//FVec srcint = CellInegrator< DIM >( fcell, 3).template VolumeIntegral< FVec, FVec, SourceFunc >( srcfunc, mtabsZ);
	//srcint /= 3.;

	//FVec src;
	//src.Init(0.0);

	//src(2) = - mvarZm(0) * mvarZm(3) * mRefOmega * fcell.Volume() / 3.;
	//src(3) =   mvarZm(0) * mvarZm(2) * mRefOmega * fcell.Volume() / 3.;

	//for ( MGSize k=0; k<=DIM; ++k)
	//{
	//	//tabres[k](2) -= src(2);
	//	//tabres[k](3) -= src(3);

	//	tabres[k](2) -= -srcint(3);
	//	tabres[k](3) -= srcint(2);

	//	//tabres[k](2) -= -srctab[k](3);
	//	//tabres[k](3) -= srctab[k](2);
	//}

	////////////
	FVec src;
	src.Init(0.0);

	GVec velo;
	for ( MGSize i=0; i<DIM; ++i)
		velo.rX(i) = mvarVm(i+2);

	GVec oxv = mpPhysics->cRfrDef().OmegaCross( velo );

	for ( MGSize i=0; i<DIM; ++i)
		src(i+2) = mvarVm(0) * oxv.cX(i) * fcell.Volume();

	//for ( MGSize k=0; k<=DIM; ++k)
	//{
	//	tabres[k](2) -= src(2) / 3.;
	//	tabres[k](3) -= src(3) / 3.;
	//}

	for ( MGSize k=0; k<=DIM; ++k)
	{
		tabres[k] -= src / (DIM+1);
	}

}



template <Dimension DIM, class MTXRDS>
inline void RfrRDSchemeMtx<DIM,MTXRDS>::CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
	Initialize( fcell);

	calc.Calc( EQSIZE, mtabKp, mtabKm, mtabsV, mtabsFi, mmtxsJ);

	FinalizeResJacob( tabres, mtxres);

}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RFR_EULERRDSCHEMEMTX_H__

