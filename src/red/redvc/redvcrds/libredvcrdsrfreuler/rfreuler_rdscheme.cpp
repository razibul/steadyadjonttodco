#include "rfreuler_rdschememtx.h"
#include "rfreuler_rdschemecrdmtx.h"

#include "redvc/redvcrds/libredvcrdscommon/mtxldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/mtxnscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/mtxldanscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/crdmtxldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/crdmtxnscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/crdmtxldanscheme.h"

#include "libcorecommon/factory.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



void init_FlowFuncRfrEulerRDS()
{

	static ConcCreator< 
		MGString, 
		RfrRDSchemeCRDMtx< Geom::DIM_2D, CRDMtxLDAscheme<Geom::DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_RFR_EULER, EquationDef<Geom::DIM_2D,EQN_RFR_EULER>::SIZE> 
	>	gCreatorFlowFuncRfrEulerRDSCrdLDA2D( "FLOWFUNC_2D_RFR_EULER_RDS_CRDMTX_LDA");

	static ConcCreator< 
		MGString, 
		RfrRDSchemeCRDMtx< Geom::DIM_3D, CRDMtxLDAscheme<Geom::DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_RFR_EULER, EquationDef<Geom::DIM_3D,EQN_RFR_EULER>::SIZE> 
	>	gCreatorFlowFuncRfrEulerRDSCrdLDA3D( "FLOWFUNC_3D_RFR_EULER_RDS_CRDMTX_LDA");




	static ConcCreator< 
		MGString, 
		RfrRDSchemeMtx< Geom::DIM_2D, MtxLDAscheme<Geom::DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_RFR_EULER, EquationDef<Geom::DIM_2D,EQN_RFR_EULER>::SIZE> 
	>	gCreatorFlowFuncRfrEulerRDSLDA2D( "FLOWFUNC_2D_RFR_EULER_RDS_MTX_LDA");


	static ConcCreator< 
		MGString, 
		RfrRDSchemeMtx< Geom::DIM_3D, MtxLDAscheme<Geom::DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_RFR_EULER, EquationDef<Geom::DIM_3D,EQN_RFR_EULER>::SIZE> 
	>	gCreatorFlowFuncRfrEulerRDSLDA3D( "FLOWFUNC_3D_RFR_EULER_RDS_MTX_LDA");


	//static ConcCreator< 
	//	MGString, 
	//	RDSchemeMtx< Geom::DIM_2D, MtxNscheme<Geom::DIM_2D> >,
	//	FlowFunc<Geom::DIM_2D, EQN_RFR_EULER, EquationDef<Geom::DIM_2D,EQN_RFR_EULER>::SIZE> 
	//>	gCreatorFlowFuncRfrEulerRDSN2D( "FLOWFUNC_2D_RFR_EULER_RDS_MTX_N");

	//static ConcCreator< 
	//	MGString, 
	//	RDSchemeMtx< Geom::DIM_3D, MtxNscheme<Geom::DIM_3D> >,
	//	FlowFunc<Geom::DIM_3D, EQN_RFR_EULER, EquationDef<Geom::DIM_3D,EQN_RFR_EULER>::SIZE> 
	//>	gCreatorFlowFuncRfrEulerRDSN3D( "FLOWFUNC_3D_RFR_EULER_RDS_MTX_N");


	//static ConcCreator< 
	//	MGString, 
	//	RDSchemeMtx< Geom::DIM_2D, MtxLDANscheme<Geom::DIM_2D> >,
	//	FlowFunc<Geom::DIM_2D, EQN_RFR_EULER, EquationDef<Geom::DIM_2D,EQN_RFR_EULER>::SIZE> 
	//>	gCreatorFlowFuncRfrEulerRDSLDAN2D( "FLOWFUNC_2D_RFR_EULER_RDS_MTX_LDAN");

	//static ConcCreator< 
	//	MGString, 
	//	RDSchemeMtx< Geom::DIM_3D, MtxLDANscheme<Geom::DIM_3D> >,
	//	FlowFunc<Geom::DIM_3D, EQN_RFR_EULER, EquationDef<Geom::DIM_3D,EQN_RFR_EULER>::SIZE> 
	//>	gCreatorFlowFuncRfrEulerRDSLDAN3D( "FLOWFUNC_3D_RFR_EULER_RDS_MTX_LDAN");


}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

