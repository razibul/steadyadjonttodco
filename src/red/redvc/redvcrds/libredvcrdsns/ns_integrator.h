#ifndef __NSINTEGRATOR_H__ 
#define __NSINTEGRATOR_H__ 

#include "redvc/libredvccommon/integrator.h"
#include "libredphysics/eulerequations.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class NSIntegrator 
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class NSIntegrator : public Integrator<DIM, EQN_NS>
{
	typedef EquationDef<DIM,EQN_NS> EqDef;

	enum { EQSIZE = EquationDef<DIM,EQN_NS>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_NS>::AUXSIZE };

	typedef Vect<DIM>				GVec;
	typedef typename EqDef::FVec	FVec;
	typedef typename EqDef::FMtx	FMtx;

	typedef SVector<DIM>			FSubVec;
	typedef SMatrix<DIM>			FSubMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>	CFCell;
	typedef CFlowBFace<DIM,EQSIZE,AUXSIZE>	CFBFace;

public:

	virtual ~NSIntegrator ()	{};

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,EQN_NS>* pphys, Data<DIM,EQN_NS>* pdata);
	virtual void	PostCreateCheck() const;
	virtual void	Init();
	virtual bool	Do();

protected:

	template <MGSize D>
	class GetZ
	{
	public:
		MGFloat operator () ( const FVec& var) const
		{
			const FVec varZ = EqnEuler<DIM>::ConservToZ( var);
			return varZ( D);
		}
	};

	void	CalcTensor( FSubMtx& mtx, const CFCell& fcell) const;

private:
	FVec		mRefVar;
};


template <Dimension DIM>
void NSIntegrator <DIM>::Create( const CfgSection* pcfgsec, Physics<DIM,EQN_NS>* pphys, Data<DIM,EQN_NS>* pdata)
{
	Integrator<DIM, EQN_NS>::Create( pcfgsec, pphys, pdata);
}

template <Dimension DIM>
void NSIntegrator <DIM>::PostCreateCheck() const
{
}

template <Dimension DIM>
void NSIntegrator <DIM>::Init()
{
	this->cPhysics().DimToUndim( mRefVar, this->cPhysics().cRefVar(), this->cPhysics().cRefVar() );
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __NSINTEGRATOR_H__ 
