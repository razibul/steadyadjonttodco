#include "ns_galerkinscheme.h"
#include "ns_galerkincrdscheme.h"

#include "libcorecommon/factory.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



void init_FlowFuncNSRDS()
{
	static ConcCreator<
		MGString,
		GalerkinCRDScheme< Geom::DIM_2D, RDSchemeCRDPreT< Geom::DIM_2D, CRDMtxLDAscheme<Geom::DIM_2D>, CRDScLDAscheme<DIM_2D> > >,
		FlowFunc<Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SIZE>
	>	gCreatorFlowFuncNSlamRDSPretCrdLDA2D( "FLOWFUNC_2D_NS_RDS_PRET_CRDMTX_LDA");

	static ConcCreator<
		MGString,
		GalerkinCRDScheme< Geom::DIM_3D, RDSchemeCRDPreT< Geom::DIM_3D, CRDMtxLDAscheme<Geom::DIM_3D>, CRDScLDAscheme<DIM_3D> > >,
		FlowFunc<Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SIZE>
	>	gCreatorFlowFuncNSlamRDSPretCrdLDA3D( "FLOWFUNC_3D_NS_RDS_PRET_CRDMTX_LDA");



	static ConcCreator<
		MGString,
		GalerkinCRDScheme< Geom::DIM_2D, RDSchemeCRDMtx< DIM_2D, CRDMtxLDAscheme<DIM_2D> > >,
		FlowFunc<Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SIZE>
	>	gCreatorFlowFuncNSlamRDSCrdLDA2D( "FLOWFUNC_2D_NS_RDS_CRDMTX_LDA");

	static ConcCreator<
		MGString,
		GalerkinCRDScheme< Geom::DIM_3D, RDSchemeCRDMtx< DIM_3D, CRDMtxLDAscheme<DIM_3D> > >,
		FlowFunc<Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SIZE>
	>	gCreatorFlowFuncNSlamRDSCrdLDA3D( "FLOWFUNC_3D_NS_RDS_CRDMTX_LDA");


	//// 2D
	//static ConcCreator<
	//	MGString,
	//	GalerkinScheme< Geom::DIM_2D, RDSchemeTest< Geom::DIM_2D, MtxLDAscheme<Geom::DIM_2D> > >,
	//	FlowFunc<Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SIZE>
	//>	gCreatorFlowFuncNSlamRDSTest2D( "FLOWFUNC_2D_NS_RDS_TEST");

	static ConcCreator<
		MGString,
		GalerkinScheme< Geom::DIM_2D, RDSchemePreT< Geom::DIM_2D, MtxLDAscheme<Geom::DIM_2D>, ScPSIscheme<DIM_2D> > >,
		FlowFunc<Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SIZE>
	>	gCreatorFlowFuncNSlamRDSPreTLDAPSI2D( "FLOWFUNC_2D_NS_RDS_PRET_LDA_PSI");

	static ConcCreator<
		MGString,
		GalerkinScheme< Geom::DIM_2D, RDSchemePreT< Geom::DIM_2D, MtxLDAscheme<Geom::DIM_2D>, ScLDAscheme<DIM_2D> > >,
		FlowFunc<Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SIZE>
	>	gCreatorFlowFuncNSlamRDSPreTLDALDA2D( "FLOWFUNC_2D_NS_RDS_PRET_LDA_LDA");



	static ConcCreator<
		MGString,
		GalerkinScheme< Geom::DIM_2D, RDSchemeHE< Geom::DIM_2D, MtxLDAscheme<Geom::DIM_2D>, ScPSIscheme<DIM_2D> > >,
		FlowFunc<Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SIZE>
	>	gCreatorFlowFuncNSlamRDSHELDAPSI2D( "FLOWFUNC_2D_NS_RDS_HE_LDA_PSI");

	static ConcCreator<
		MGString,
		GalerkinScheme< Geom::DIM_2D, RDSchemeMtx< DIM_2D, MtxLDAscheme<DIM_2D> > >,
		FlowFunc<Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SIZE>
	>	gCreatorFlowFuncNSlamRDSLDA2D( "FLOWFUNC_2D_NS_RDS_MTX_LDA");

	static ConcCreator<
		MGString,
		GalerkinScheme< Geom::DIM_2D, RDSchemeMtx< DIM_2D, MtxLDANscheme<DIM_2D> > >,
		FlowFunc<Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SIZE>
	>	gCreatorFlowFuncNSlamRDSLDAN2D( "FLOWFUNC_2D_NS_RDS_MTX_LDAN");
	

	// 3D
	static ConcCreator<
		MGString,
		GalerkinScheme< Geom::DIM_3D, RDSchemePreT< Geom::DIM_3D, MtxLDAscheme<Geom::DIM_3D>, ScPSIscheme<DIM_3D> > >,
		FlowFunc<Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SIZE>
	>	gCreatorFlowFuncNSlamRDSPreTLDAPSI3D( "FLOWFUNC_3D_NS_RDS_PRET_LDA_PSI");

	static ConcCreator<
		MGString,
		GalerkinScheme< Geom::DIM_3D, RDSchemePreT< Geom::DIM_3D, MtxLDAscheme<Geom::DIM_3D>, ScLDAscheme<DIM_3D> > >,
		FlowFunc<Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SIZE>
	>	gCreatorFlowFuncNSlamRDSPreTLDALDA3D( "FLOWFUNC_3D_NS_RDS_PRET_LDA_LDA");

	static ConcCreator<
		MGString,
		GalerkinScheme< Geom::DIM_3D, RDSchemeMtx< DIM_3D, MtxLDAscheme<DIM_3D> > >,
		FlowFunc<Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SIZE>
	>	gCreatorFlowFuncNSlamRDSLDA3D( "FLOWFUNC_3D_NS_RDS_MTX_LDA");

	static ConcCreator<
		MGString,
		GalerkinScheme< Geom::DIM_3D, RDSchemeMtx< DIM_3D, MtxLDANscheme<DIM_3D> > >,
		FlowFunc<Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SIZE>
	>	gCreatorFlowFuncNSlamRDSLDAN3D( "FLOWFUNC_3D_NS_RDS_MTX_LDAN");
	
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

