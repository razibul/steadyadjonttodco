#ifndef __RFR_NSGALERKINSCHEME_H__
#define __RFR_NSGALERKINSCHEME_H__

#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"
#include "libredphysics/eulerequations.h"
#include "redvc/redvcrds/libredvcrdsrfreuler/rfreuler_rdschememtx.h"
#include "redvc/redvcrds/libredvcrdsrfreuler/rfreuler_rdschemecrdmtx.h"

#include "redvc/redvcrds/libredvcrdscommon/crdmtxldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/mtxldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/mtxnscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/mtxldanscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/scldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/scnscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/scpsischeme.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




//////////////////////////////////////////////////////////////////////
// class GalerkinSchemeBase
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class RfrGalerkinSchemeBase : public FlowFunc<DIM, EQN_RFR_NS, EquationDef<DIM,EQN_RFR_NS>::SIZE>
{
	typedef EquationDef<DIM,EQN_RFR_NS> FEqns;

	enum { EQSIZE = EquationDef<DIM,EQN_RFR_NS>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_RFR_NS>::AUXSIZE };

	typedef Vect<DIM>				GVec;
	typedef typename FEqns::FVec	FVec;
	typedef typename FEqns::FMtx	FMtx;
	typedef SVector<DIM>			FSubVec;
	typedef SMatrix<DIM>			FSubMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>	CFCell;
	typedef CFlowBFace<DIM,EQSIZE,AUXSIZE>	CFBFace;




	template <MGSize D>
	class GetZ
	{
	public:
		MGFloat operator () ( const FVec& var) const
		{
			const FVec varZ = EqnEuler<DIM>::ConservToZ( var);
			return varZ( D);
		}
	};

	template <MGSize D>
	class GetVelocity
	{
	public:
		MGFloat operator () ( const FVec& var) const
		{
			typedef typename FEqns::template U<D> FEqns_UD;
			return var( FEqns_UD::ID) / var( FEqns::ID_RHO);
		}
	};

	class GetC2
	{
	public:
		MGFloat operator () ( const FVec& var) const
		{
			MGFloat c = EqnEulerComm<DIM>::C( var);
			return c*c;
		}
	};


public:
	RfrGalerkinSchemeBase() : mMuT(0.0)	{}

	RfrGalerkinSchemeBase( const MGFloat& re, const MGFloat& pr, const FVec& var, const MGFloat& mut=0.0) 
		: mRe(re), mPr(pr), mRefVar(var), mMuT(mut)	{}

	virtual void Init( const Physics<DIM,EQN_RFR_NS>& physics);

	//virtual void	CalcResiduum( const CFCell& fcell, FVec tabres[]);
	//virtual void	CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);


protected:

	void	CalcTensor( FSubMtx& mtx, const CFCell& fcell) const;
	void	CalcGradC2( GVec& vec, const CFCell& fcell) const;

protected:
	MGFloat		mMuT;
	MGFloat		mRe;
	MGFloat		mPr;
	FVec		mRefVar;
	FVec		mvarZm;
	
};
//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void RfrGalerkinSchemeBase<DIM>::Init( const Physics<DIM,EQN_RFR_NS>& physics)
{
	mRe = physics.cRe();
	mPr = physics.cPr();

	physics.DimToUndim( mRefVar, physics.cRefVar(), physics.cRefVar() );
	//mRefVar = physics.cRefVar();
}


template <>
inline void RfrGalerkinSchemeBase<DIM_2D>::CalcGradC2( GVec& vec, const CFCell& fcell) const
{
	GVec grad_zro = fcell.GradVar( GetZ< FEqns::ID_RHO >() );
	GVec grad_zh  = fcell.GradVar( GetZ< FEqns::ID_P >() );
	GVec grad_zv0 = fcell.GradVar( GetZ< FEqns::U<0>::ID >() );
	GVec grad_zv1 = fcell.GradVar( GetZ< FEqns::U<1>::ID >() );

	GVec gvec1 = 1.0 / ( mvarZm(FEqns::ID_RHO) * mvarZm(FEqns::ID_RHO) ) * ( mvarZm(FEqns::ID_RHO) * grad_zh - mvarZm(FEqns::ID_P) * grad_zro );
	GVec gvec2 = 1.0 / ( mvarZm(FEqns::ID_RHO) * mvarZm(FEqns::ID_RHO) * mvarZm(FEqns::ID_RHO) ) * 
				 ( 
				 mvarZm(FEqns::ID_RHO) * ( mvarZm(FEqns::U<0>::ID) * grad_zv0 + mvarZm(FEqns::U<1>::ID) * grad_zv1 ) -
				 ( mvarZm(FEqns::U<0>::ID)*mvarZm(FEqns::U<0>::ID) + mvarZm(FEqns::U<1>::ID)*mvarZm(FEqns::U<1>::ID) ) * grad_zro
				 );
	vec = (0.4) * ( gvec1 - gvec2 );
}

template <>
inline void RfrGalerkinSchemeBase<DIM_3D>::CalcGradC2( GVec& vec, const CFCell& fcell) const
{
	GVec grad_zro = fcell.GradVar( GetZ< FEqns::ID_RHO >() );
	GVec grad_zh  = fcell.GradVar( GetZ< FEqns::ID_P >() );
	GVec grad_zv0 = fcell.GradVar( GetZ< FEqns::U<0>::ID >() );
	GVec grad_zv1 = fcell.GradVar( GetZ< FEqns::U<1>::ID >() );
	GVec grad_zv2 = fcell.GradVar( GetZ< FEqns::U<2>::ID >() );

	GVec gvec1 = 1.0 / ( mvarZm(FEqns::ID_RHO) * mvarZm(FEqns::ID_RHO) ) * ( mvarZm(FEqns::ID_RHO) * grad_zh - mvarZm(FEqns::ID_P) * grad_zro );
	GVec gvec2 = 1.0 / ( mvarZm(FEqns::ID_RHO) * mvarZm(FEqns::ID_RHO) * mvarZm(FEqns::ID_RHO) ) * 
				 ( 
				 mvarZm(FEqns::ID_RHO) * ( mvarZm(FEqns::U<0>::ID) * grad_zv0 + mvarZm(FEqns::U<1>::ID) * grad_zv1 + mvarZm(FEqns::U<2>::ID) * grad_zv2 ) -
				 ( mvarZm(FEqns::U<0>::ID)*mvarZm(FEqns::U<0>::ID) + mvarZm(FEqns::U<1>::ID)*mvarZm(FEqns::U<1>::ID) + mvarZm(FEqns::U<2>::ID)*mvarZm(FEqns::U<2>::ID) ) * grad_zro
				 );
	vec = (0.4) * ( gvec1 - gvec2 );
}

template <>
inline void RfrGalerkinSchemeBase<DIM_2D>::CalcTensor( FSubMtx& mtx, const CFCell& fcell) const
{
	GVec grad_zro = fcell.GradVar( GetZ< FEqns::ID_RHO >() );
	GVec grad_zv0 = fcell.GradVar( GetZ< FEqns::U<0>::ID >() );
	GVec grad_zv1 = fcell.GradVar( GetZ< FEqns::U<1>::ID >() );


	GVec gradx = 1.0 / ( mvarZm(FEqns::ID_RHO) * mvarZm(FEqns::ID_RHO) ) * ( mvarZm(FEqns::ID_RHO) * grad_zv0 - mvarZm(FEqns::U<0>::ID) * grad_zro );
	GVec grady = 1.0 / ( mvarZm(FEqns::ID_RHO) * mvarZm(FEqns::ID_RHO) ) * ( mvarZm(FEqns::ID_RHO) * grad_zv1 - mvarZm(FEqns::U<1>::ID) * grad_zro );

	//GVec gradx = fcell.GradVar( GetVelocity<0>() );
	//GVec grady = fcell.GradVar( GetVelocity<1>() );

	MGFloat trace = gradx.cX() + grady.cY();

	mtx(0,0) = 2*( gradx.cX() - trace/3.0 );
	mtx(1,1) = 2*( grady.cY() - trace/3.0 );

	mtx(1,0) = mtx(0,1) = gradx.cY() + grady.cX();
}

template <>
inline void RfrGalerkinSchemeBase<DIM_3D>::CalcTensor( FSubMtx& mtx, const CFCell& fcell) const
{
	GVec grad_zro = fcell.GradVar( GetZ< FEqns::ID_RHO >() );
	GVec grad_zv0 = fcell.GradVar( GetZ< FEqns::U<0>::ID >() );
	GVec grad_zv1 = fcell.GradVar( GetZ< FEqns::U<1>::ID >() );
	GVec grad_zv2 = fcell.GradVar( GetZ< FEqns::U<2>::ID >() );

	GVec gradx = 1.0 / ( mvarZm(FEqns::ID_RHO) * mvarZm(FEqns::ID_RHO) ) * ( mvarZm(FEqns::ID_RHO) * grad_zv0 - mvarZm(FEqns::U<0>::ID) * grad_zro );
	GVec grady = 1.0 / ( mvarZm(FEqns::ID_RHO) * mvarZm(FEqns::ID_RHO) ) * ( mvarZm(FEqns::ID_RHO) * grad_zv1 - mvarZm(FEqns::U<1>::ID) * grad_zro );
	GVec gradz = 1.0 / ( mvarZm(FEqns::ID_RHO) * mvarZm(FEqns::ID_RHO) ) * ( mvarZm(FEqns::ID_RHO) * grad_zv2 - mvarZm(FEqns::U<2>::ID) * grad_zro );

	//GVec gradx = fcell.GradVar( GetVelocity<0>() );
	//GVec grady = fcell.GradVar( GetVelocity<1>() );
	//GVec gradz = fcell.GradVar( GetVelocity<2>() );


	MGFloat trace = gradx.cX() + grady.cY() + gradz.cZ();

	mtx(0,0) = 2*( gradx.cX() - trace/3.0 );
	mtx(1,1) = 2*( grady.cY() - trace/3.0 );
	mtx(2,2) = 2*( gradz.cZ() - trace/3.0 );

	mtx(1,0) = mtx(0,1) = gradx.cY() + grady.cX();
	mtx(2,0) = mtx(0,2) = gradx.cZ() + gradz.cX();
	mtx(1,2) = mtx(2,1) = grady.cZ() + gradz.cY();
}





//////////////////////////////////////////////////////////////////////
// class RfrGalerkinScheme
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class MTXRDS>
class RfrGalerkinScheme : public RfrGalerkinSchemeBase<DIM>
{
	typedef EquationDef<DIM,EQN_RFR_NS> FEqns;

	enum { EQSIZE = EquationDef<DIM,EQN_RFR_NS>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_RFR_NS>::AUXSIZE };

	typedef Vect<DIM>				GVec;
	typedef typename FEqns::FVec	FVec;
	typedef typename FEqns::FMtx	FMtx;
	typedef SVector<DIM>			FSubVec;
	typedef SMatrix<DIM>			FSubMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>	CFCell;
	typedef CFlowBFace<DIM,EQSIZE,AUXSIZE>	CFBFace;



public:
	RfrGalerkinScheme() : RfrGalerkinSchemeBase<DIM>()	{}

	RfrGalerkinScheme( const MGFloat& re, const MGFloat& pr, const FVec& var, const MGFloat& mut=0.0) : RfrGalerkinSchemeBase<DIM>( re, pr, var, mut)	{}


	virtual void	CalcResiduum( const CFCell& fcell, FVec tabres[]);
	virtual void	CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

};
//////////////////////////////////////////////////////////////////////


template <Dimension DIM, class MTXRDS>
inline void RfrGalerkinScheme<DIM,MTXRDS>::CalcResiduum( const CFCell& fcell, FVec tabres[])		
{
	//printf( "NS !!!!!\n");
	
	//RDSchemeMtx< DIM, MtxLDAscheme<DIM> > func;
	//func.CalcResiduum( fcell, tabres);

	MTXRDS func( this->mRefVar);
	func.CalcResiduum( fcell, tabres);

	// don't calc visc fluxes if outlet
	if ( fcell.IsOutlet() )
		return;

	////////////////////
	MGFloat	mu = 1.0;

	FVec	var, varVm, varQm;

	this->mvarZm.Init( 0.0);
	varVm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
		this->mvarZm += EqnEuler<DIM>::ConservToZ( fcell.cVar(i) );

	this->mvarZm /= (MGFloat)(DIM+1);
	varVm = EqnEuler<DIM>::ZToSimp( this->mvarZm);
	varQm = EqnEuler<DIM>::ZToConserv( this->mvarZm);


	//FVec vav = fcell.cVar(0);
	//for ( MGSize i=1; i<fcell.Size(); ++i)
	//	vav += fcell.cVar(i);

	//vav /= static_cast<MGFloat>( fcell.Size());

	MGFloat c = EqnEulerComm<DIM>::C( varQm);
	MGFloat cinf = EqnEulerComm<DIM>::C( this->mRefVar);
	MGFloat c2 = c*c;
	MGFloat	cinf2 = cinf*cinf;

	MGFloat temp = c2 / cinf2;
	MGFloat tempinf = 295.0;
	MGFloat sconst = 110.33;


	//cout << c << " " << cinf << endl;
	//cout << temp << endl;

	//varVm.Write();
	//varQm.Write();
	//mRefVar.Write();
	////mu = 1.0;
	mu = sqrt( temp*temp*temp) * ( 1.0 + sconst/tempinf) / (temp + sconst/tempinf);
	//cout << "mu = " << mu << endl;
	//mu = 1;
	//cout << mu << endl;
	//mu = pow( temp, 0.7 );
	//cout << mu << endl;
	//THROW_INTERNAL("STOP");

	//FILE *f = fopen( "_visc.dump", "at");
	//fprintf( f, "%16.10lg  %16.10lg  %16.10lg  %16.10lg\n", c, cinf, temp, mu);
	//fclose( f);


	FSubMtx mtxT;
	GVec gradc2;

	this->CalcTensor( mtxT, fcell);
	this->CalcGradC2( gradc2, fcell);
	
	//gradc2 = fcell.GradVar( GetC2() );
	FSubVec	vec;

	//for ( MGSize k=0; k<DIM; ++k)
	//{
	//	vec(k) = 0.0;
	//	for ( MGSize i=0; i<=DIM; ++i)
	//		vec(k) += fcell.cVar(i)(k+2) / fcell.cVar(i)(0);
	//}
	//vec /= (MGFloat)(DIM+1);

	for ( MGSize k=0; k<DIM; ++k)
		vec(k) = varVm(k+2);

	FSubVec	ve;

	ve = mtxT * vec;

	for ( MGSize i=0; i<DIM; ++i)
		gradc2.rX(i) = ve(i) + 2.5 / this->mPr * gradc2.cX(i);


	FVec	tabdres[DIM+1];

	for ( MGSize i=0; i<DIM+1; ++i)
	{
		GVec vn = fcell.Vn( i);

		tabdres[i](0) = 0.0;
		tabdres[i](1) = (mu + this->mMuT) * gradc2 * vn / DIM / this->mRe;

		for ( MGSize k=0; k<DIM; ++k)
		{
			GVec vv;

			for ( MGSize j=0; j<DIM; ++j)
				vv.rX(j) = mtxT(j,k);

			tabdres[i](k+2) = (mu + this->mMuT) * vv * vn / DIM / this->mRe;
		}
	}

	for ( MGSize i=0; i<DIM+1; ++i)
		for ( MGSize k=0; k<EQSIZE; ++k)
			if ( ISNAN( tabdres[i](k) ) || ISINF( tabdres[i](k) ) )
				tabdres[i](k) = 0.0;


	//for ( MGSize i=0; i<DIM+1; ++i)
	//	tabres[i] += tabdres[i];

	for ( MGSize i=0; i<DIM+1; ++i)
	{
		FVec varV = EqnEuler<DIM>::ConservToSimp( fcell.cVar(i) );
		func.ApplyPrecond( tabdres[i], varV);

		tabres[i] += tabdres[i];
	}

}


template <Dimension DIM, class MTXRDS>
inline void RfrGalerkinScheme<DIM,MTXRDS>::CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
	THROW_INTERNAL("Not implemented !!!");
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RFR_NSGALERKINSCHEME_H__
