#include "libcoresystem/mgdecl.h"

#include "libcorecommon/factory.h"
#include "libcoreconfig/config.h"

#include "libred/setup.h"
#include "libred/master.h"



INIT_VERSION("0.1.1","'new layout - wip'");


using namespace RED;


int main( int argc, char* argv[])
{
// 	cout << "test" << endl;
// 	
// 	typedef EquationDef<Geom::DIM_2D,EQN_NS> TEq;
// 
// 	cout << TEq::SIZE << endl;
// 	cout << TEq::ID_RHO << endl;
// 	cout << TEq::ID_P << endl;
// 	cout << TEq::U<0>::ID << endl;
// 	cout << TEq::U<1>::ID << endl;
// 
// 
// 	return 1;


	try
	{
		CheckTypeSizes();
		RegisterLibraries();


		cout << "ver.: " << VERSION_NUMBER << "  build: " << VERSION_DATE << " " << VERSION_TIME << endl << endl;
		///////////////////////////////////////////////////////////////////////

		//Singleton< FactoryList<MGString> >::GetInstance()->PrintInfo();

		Master	master;
		
		if ( argc == 2)
			master.ReadConfig( argv[1]);
		else
			master.ReadConfig( "solv.cfg");

		master.Create();
		master.Init();
		master.Solve();

	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		fprintf( stderr, "%s\n", e.what() );
	}
	catch( ...) 
	{
		cout << "unknow problem ::  ..." << endl;
	}

	return 0;
}
