#include <mpi.h>

#ifdef WITH_TRILINOS

#include "Ifpack_ConfigDefs.h"

#include "Epetra_MpiComm.h"

#include "Epetra_CrsGraph.h"

#include "Epetra_CrsMatrix.h"
#include "Epetra_VbrMatrix.h"
#include "Epetra_VbrRowMatrix.h"
#include "Epetra_MultiVector.h"
#include "Epetra_LinearProblem.h"

#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "AztecOO.h"
#include "Ifpack.h"
#include "Ifpack_AdditiveSchwarz.h"

#endif // WITH_TRILINOS

int main ( int argc, char* argv[] )
{
    int  numtasks, rank, rc;

    rc = MPI_Init ( &argc, &argv );
    if ( rc != MPI_SUCCESS )
    {
        printf ( "Error starting MPI program. Terminating.\n" );
        MPI_Abort ( MPI_COMM_WORLD, rc );
    }

    MPI_Comm_size ( MPI_COMM_WORLD, &numtasks );
    MPI_Comm_rank ( MPI_COMM_WORLD, &rank );


    ///////////////////////////////////////////////////////////////////////
    cout << "Number of tasks = " << numtasks << " My rank = " << rank << endl;
    rank == 0 ? cout << "compiled WITH_TRILINOS"<< endl : "";
    cout << endl;
    ///////////////////////////////////////////////////////////////////////

    Epetra_MpiComm Comm ( MPI_COMM_WORLD );
    Comm.PrintInfo ( std::cout );
    std::cout << std::endl;
    MPI_Barrier ( MPI_COMM_WORLD );

    int NumMyElements;
    std::vector<int> nnz_per_row;
    if ( Comm.MyPID() == 0 )
    {
        NumMyElements = 2;
        nnz_per_row.resize ( NumMyElements );
        nnz_per_row[0] = 2;
        nnz_per_row[1] = 3;
    }
    if ( Comm.MyPID() == 1 )
    {
        NumMyElements = 3;
        nnz_per_row.resize ( NumMyElements );
        nnz_per_row[0] = 3;
        nnz_per_row[1] = 3;
        nnz_per_row[2] = 2;
    }

    // Construct a Map that puts same number of equations on each processor
    Epetra_BlockMap rowmap ( 5, NumMyElements, 2, 0, Comm ); //block has a size of 2
    int NumGlobalElements = rowmap.NumGlobalElements();

    //create a graph
    Epetra_CrsGraph graph ( Copy, rowmap, &nnz_per_row[0], true );

    // set row nonzero elements
    int err = 0;
    for ( int i=0; i<NumMyElements; i++ )
    {
        int GlobalRow = rowmap.GID ( i );
        std::vector<int> indices;
        if ( GlobalRow == 0 )
        {
            indices.resize ( 2 );
            indices[0] = 0;
            indices[1] = 1;
        }
        else if ( GlobalRow == NumGlobalElements-1 )
        {
            indices.resize ( 2 );
            indices[0] = NumGlobalElements-2;
            indices[1] = NumGlobalElements-1;
        }
        else
        {
            indices.resize ( 3 );
            indices[0] = GlobalRow-1;
            indices[1] = GlobalRow;
            indices[2] = GlobalRow+1;
        }

        err = graph.InsertGlobalIndices ( GlobalRow, nnz_per_row[i], &indices[0] );
        std::cout << "Process " << Comm.MyPID() << " inserts to global row " << GlobalRow << " Local row " << i << " | err: " << err << std::endl;
    }

    graph.FillComplete();
    graph.Print(std::cout);

    //Epetra_CrsMatrix A ( Copy, graph );
    Epetra_VbrMatrix A ( Copy, graph);

    Epetra_VbrRowMatrix Arow( &A );

    vector<double> blockvec(4,0);
    blockvec[0] = 1; blockvec[1] = 2; blockvec[2] = 3; blockvec[3] = 4;
    for ( int i=0; i<NumMyElements; i++ )
    {
        int GlobalRow = rowmap.GID ( i );

        std::vector<int> indices;
        if ( GlobalRow == 0 )
        {
            indices.resize ( 2 );
            indices[0] = 0;
            indices[1] = 1;
        }
        else if ( GlobalRow == NumGlobalElements-1 )
        {
            indices.resize ( 2 );
            indices[0] = NumGlobalElements-2;
            indices[1] = NumGlobalElements-1;
        }
        else
        {
            indices.resize ( 3 );
            indices[0] = GlobalRow-1;
            indices[1] = GlobalRow;
            indices[2] = GlobalRow+1;
        }

        err = A.BeginReplaceGlobalValues(GlobalRow, indices.size(), &(indices[0]));
        cout << err << endl;
        for(int i=0; i<indices.size(); ++i)
        {
        //    err = A.DirectSubmitBlockEntry(GlobalRow, indices[i], &blockvec[0], 2, 2, 2, true); // This puts blocs COLUMN by column

            Epetra_SerialDenseMatrix dmat(2,2,false);
            //dmat.Scale();
            dmat(0,0)=GlobalRow; dmat(0,1)=2;
            dmat(1,0)=3; dmat(1,1)=4;
            err = A.SubmitBlockEntry( dmat );
            cout << err << endl;

            std::cout << "Process " << Comm.MyPID() << " summs into to global row " << GlobalRow << " Local row " << i << " | err: " << err << std::endl;
        }
        err = A.EndSubmitEntries();
        cout << err << endl;
    }

    A.Print ( std::cout );

    int aa[2] = {rowmap.GID(0), rowmap.GID(1)};
    double bb[2];
    bb[0] = 5; bb[1] = 10;
    Epetra_Vector a(rowmap, true);
    a.Print(std::cout);

    err = a.ReplaceGlobalValues(2, 0, bb, aa );
    bb[0] = 50; bb[1] = 100;
    err = a.ReplaceGlobalValues(2, 1, bb, aa );

    aa[0] = 4;
    bb[0] = 500; bb[1] = 1000;
    err = a.ReplaceGlobalValues(1, 0, bb, aa );

    bb[0] = 5000; bb[1] = 1000;
    err = a.ReplaceGlobalValues(1, 1, bb, aa );

    cout << err << endl;
    a.Print(std::cout);

    a.PutScalar(1.0);
    a.Print(std::cout);


    MPI_Finalize() ;

    return ( EXIT_SUCCESS );
}
// kate: indent-mode cstyle; indent-width 4; replace-tabs on; ;
