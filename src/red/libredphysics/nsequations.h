#ifndef __EQNNS_H__
#define __EQNNS_H__

#include "eulerequations.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class EqnRansSA 
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class EqnNS
{
public:
	typedef EquationDef<DIM,EQN_NS> TEqDef;

	enum { EQSIZE = TEqDef::SIZE };

	typedef Vect<DIM>				GVec;
	typedef typename TEqDef::FVec	FVec;
	typedef typename TEqDef::FMtx	FMtx;

	typedef SVector<EQSIZE,GVec>	FGradVec;

	typedef SVector<DIM>			FSubVec;
	typedef SMatrix<DIM>			FSubMtx;

public:
	static void CalcTensor( FSubMtx& mtx, const FVec& vecq, const FGradVec& vecgrad);
};



template <>
inline void EqnNS<DIM_2D>::CalcTensor( FSubMtx& mtx, const FVec& vecq, const FGradVec& vecgrad)
{
	const GVec& grad_ro = vecgrad( TEqDef::ID_RHO );
	const GVec& grad_rv0 = vecgrad( TEqDef::U<0>::ID );
	const GVec& grad_rv1 = vecgrad( TEqDef::U<1>::ID );

	GVec gradx = 1.0 / ( vecq(TEqDef::ID_RHO) * vecq(TEqDef::ID_RHO) ) * ( vecq(TEqDef::ID_RHO) * grad_rv0 - vecq(TEqDef::U<0>::ID) * grad_ro );
	GVec grady = 1.0 / ( vecq(TEqDef::ID_RHO) * vecq(TEqDef::ID_RHO) ) * ( vecq(TEqDef::ID_RHO) * grad_rv1 - vecq(TEqDef::U<1>::ID) * grad_ro );

	MGFloat trace = gradx.cX() + grady.cY();

	mtx(0,0) = 2*( gradx.cX() - trace/3.0 );
	mtx(1,1) = 2*( grady.cY() - trace/3.0 );

	mtx(1,0) = mtx(0,1) = gradx.cY() + grady.cX();
}

template <>
inline void EqnNS<DIM_3D>::CalcTensor( FSubMtx& mtx, const FVec& vecq, const FGradVec& vecgrad)
{
	const GVec& grad_ro = vecgrad( TEqDef::ID_RHO );
	const GVec& grad_rv0 = vecgrad( TEqDef::U<0>::ID );
	const GVec& grad_rv1 = vecgrad( TEqDef::U<1>::ID );
	const GVec& grad_rv2 = vecgrad( TEqDef::U<2>::ID );

	GVec gradx = 1.0 / ( vecq(TEqDef::ID_RHO) * vecq(TEqDef::ID_RHO) ) * ( vecq(TEqDef::ID_RHO) * grad_rv0 - vecq(TEqDef::U<0>::ID) * grad_ro );
	GVec grady = 1.0 / ( vecq(TEqDef::ID_RHO) * vecq(TEqDef::ID_RHO) ) * ( vecq(TEqDef::ID_RHO) * grad_rv1 - vecq(TEqDef::U<1>::ID) * grad_ro );
	GVec gradz = 1.0 / ( vecq(TEqDef::ID_RHO) * vecq(TEqDef::ID_RHO) ) * ( vecq(TEqDef::ID_RHO) * grad_rv2 - vecq(TEqDef::U<2>::ID) * grad_ro );

	MGFloat trace = gradx.cX() + grady.cY() + gradz.cZ();

	mtx(0,0) = 2*( gradx.cX() - trace/3.0 );
	mtx(1,1) = 2*( grady.cY() - trace/3.0 );
	mtx(2,2) = 2*( gradz.cZ() - trace/3.0 );

	mtx(1,0) = mtx(0,1) = gradx.cY() + grady.cX();
	mtx(2,0) = mtx(0,2) = gradx.cZ() + gradz.cX();
	mtx(1,2) = mtx(2,1) = grady.cZ() + gradz.cY();
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EQNNS_H__
