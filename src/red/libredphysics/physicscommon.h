#ifndef __PHYSICSCOMMON_H__
#define __PHYSICSCOMMON_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreconfig/configbase.h"
#include "libredcore/configconst.h"
#include "libredcore/configconst.h"
#include "libredcore/equation.h"
#include "libredphysics/bc.h"
#include "libcoregeom/vect.h"

#include "redvc/libredvccommon/periodicbcbase.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
//	class PhysicsCommon
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET>
class PhysicsCommon : public ConfigBase
{
public:
	enum { ESIZE = EquationDef<DIM,ET>::SIZE };
	typedef EquationDef<DIM,ET>		EqDef;
	typedef typename EqDef::FVec	FVec;
	typedef typename EqDef::FMtx	FMtx;
	typedef typename EqDef::AVec	AVec;
	typedef Geom::Vect<DIM>			GVec;

	typedef PeriodicBCBase<DIM, ET>	PrdBCBase;
	//typedef typename PeriodicBC<DIM>::PeriodicKey	PeriodicKey;
	//typedef typename PeriodicBC<DIM>::ColPeriodic	ColPeriodic;
public:
	PhysicsCommon()	{}

	virtual void Create( const CfgSection* pcfgsec);
	virtual void PostCreateCheck() const;
	virtual void Init();

	// used as a reference for dim undim operations - DON'T use it inside BC calculations!
	const FVec&		cRefVar() const		{ return mRefVec;}

	//void	DimToUndim( vector<MGFloat>& tab ) const;
	//void	UndimToDim( vector<MGFloat>& tab ) const;

	virtual void	DimToUndim( FVec& vout, const FVec& vin, const FVec& vref ) const	{ vout = vin; }
	virtual void	UndimToDim( FVec& vout, const FVec& vin, const FVec& vref ) const	{ vout = vin; }


	bool			IsStrongBC( const MGSize& id) const						{ return false; }
	bool			IsEqStrongBC( const MGSize& id, const MGSize& ie) const	{ return false; }

	const FVec&		cDefVar( const MGSize& id) const	{ return (*mmapVar.find( id)).second;}
	const BC&		cBC( const MGSize& id) const		{ return (*mmapBC.find( id)).second.first;}
	const MGSize&	cVarId( const MGSize& id) const		{ return (*mmapBC.find( id)).second.second;}

	void			MatrixLocPrecond( FMtx& mtx, const FVec& vec) const		{ mtx.InitDelta(ESIZE); }

protected:
	virtual void	InitFVec( FVec& vec, const CfgSection& sec) {}// = 0;

protected:
	FVec		mRefVec;

	map<MGSize,FVec>				mmapVar;
	map< MGSize,pair<BC,MGSize> >	mmapBC;
 
};
//////////////////////////////////////////////////////////////////////



template <Geom::Dimension DIM, EQN_TYPE ET>
inline void PhysicsCommon<DIM,ET>::Create( const CfgSection* pcfgsec)		
{ 
	ConfigBase::Create( pcfgsec);
}


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void PhysicsCommon<DIM,ET>::PostCreateCheck() const
{ 
	ConfigBase::PostCreateCheck();

	// do check
}

template <Geom::Dimension DIM, EQN_TYPE ET>
inline void PhysicsCommon<DIM,ET>::Init()
{ 
	const CfgSection& secbc = ConfigSect().GetSection( ConfigStr::Solver::Physics::BC::KEY );
	CfgSection::const_iterator_sec	citrs;
	CfgSection::const_iterator_rec	citrr;

	map<MGString,MGSize>	mapvar;
	MGSize	surfid, id, idcur = 1;
	MGString	key, name;
	//char	key[128], name[512];
	FVec	vec;
	BC		bc;

	istringstream	is;

	for ( citrs = ConfigSect().begin_sec(); citrs!=ConfigSect().end_sec(); ++citrs)
	{
		cout << (*citrs).second.Name() << " " << (*citrs).second.Id() << endl;

		//is.clear();
		//is.str( (*citrs).second.Name() );
		//is >> key >> name;

		key = (*citrs).second.Name();
		name = (*citrs).second.Id();


		if ( key ==  MGString( ConfigStr::Solver::Physics::Var::NAME ) )
		{
			cout << "key = '" << key << "' name = '" << name << "'" << endl;

			if ( name == MGString( ConfigStr::Solver::Physics::Var::REFERENCE ) )
				id = 0;
			else
				id = idcur++;

			InitFVec( vec, (*citrs).second);

			mapvar.insert( typename map<MGString,MGSize>::value_type( name, id) );
			mmapVar.insert( typename map<MGSize,FVec>::value_type( id, vec) );
		}
	}

	cout << endl << "parsing secion BC" << endl << endl;

	for ( citrr=secbc.begin_rec(); citrr!= secbc.end_rec(); ++citrr)
	{
		is.clear();
		is.str( (*citrr).first );
		is >> surfid;

		is.clear();
		is.str( (*citrr).second );
		is >> key >> name;

		cout << "key = \"" << surfid << "\" name = \"" << (*citrr).second << "\"" << endl;

		bc = StrToBC( key);
		id = 0;

		if ( bc == BC_NONE)
			THROW_INTERNAL( "unknown boundary condition found in BC section");
		
		if ( bc == BC_DEF )
		{
			map<MGString,MGSize>::iterator	itr = mapvar.find( name);

			if ( itr == mapvar.end() )
			{
				ostringstream os;
				os << "var name from BC def not found: " << (*citrr).first;
				THROW_INTERNAL( os.str());
			}

			id = (*itr).second;
		}

		mmapBC.insert( map< MGSize,pair<BC,MGSize> >::value_type( surfid, pair<BC,MGSize>( bc, id) ) );
	}


	if ( mmapVar.find( 0) == mmapVar.end() )
		THROW_INTERNAL( "Reference variable not defined");

	mRefVec = (*mmapVar.find( 0)).second;

	typename map<MGSize,FVec>::iterator	iv;
	for ( iv = mmapVar.begin(); iv != mmapVar.end(); ++iv)
	{
		cout << "varid = " << (*iv).first << endl;

		DimToUndim( (*iv).second, (*iv).second, mRefVec);

		(*iv).second.Write();
	}

}


//template <Geom::Dimension DIM, EQN_TYPE ET>
//inline void PhysicsCommon<DIM,ET>::DimToUndim( vector<MGFloat>& tab ) const
//{ 
//	FVec vin, vout;
//	if ( tab.size() != ESIZE)
//		THROW_INTERNAL("PhysicsCommon<DIM,ET>::DimToUndim - tab.size() != ESIZE");
//
//	for ( MGSize i=0; i<ESIZE; ++i)
//		vin(i) = tab[i];
//
//	DimToUndim( vout, vin, cRefVar() );
//
//	for ( MGSize i=0; i<ESIZE; ++i)
//		tab[i] = vout(i);
//}
//
//template <Geom::Dimension DIM, EQN_TYPE ET>
//inline void PhysicsCommon<DIM,ET>::UndimToDim( vector<MGFloat>& tab ) const
//{ 
//	FVec vin, vout;
//	if ( tab.size() != ESIZE)
//		THROW_INTERNAL("PhysicsCommon<DIM,ET>::UndimToDim - tab.size() != ESIZE");
//
//	for ( MGSize i=0; i<ESIZE; ++i)
//		vin(i) = tab[i];
//
//	UndimToDim( vout, vin, cRefVar() );
//
//	for ( MGSize i=0; i<ESIZE; ++i)
//		tab[i] = vout(i);
//}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __PHYSICSCOMMON_H__
