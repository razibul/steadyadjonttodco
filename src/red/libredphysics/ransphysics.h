#ifndef __RANSPHYSICS_H__
#define __RANSPHYSICS_H__


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Physics for RANS with 2 turbulence equations
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class Physics<DIM,EQN_RANS_2E> : public PhysicsCommon<DIM,EQN_RANS_2E>
{
	enum { ESIZE = PhysicsCommon<DIM,EQN_RANS_2E>::ESIZE };

	typedef typename PhysicsCommon<DIM,EQN_RANS_2E>::EqDef	EqDef;
	typedef typename PhysicsCommon<DIM,EQN_RANS_2E>::FVec	FVec;
	typedef typename PhysicsCommon<DIM,EQN_RANS_2E>::AVec	AVec;
	typedef typename PhysicsCommon<DIM,EQN_RANS_2E>::GVec	GVec;

public:
	Physics()	: PhysicsCommon<DIM,EQN_RANS_2E>()	{}

	virtual void Create( const CfgSection* pcfgsec);
	virtual void PostCreateCheck() const;
	virtual void Init();

	void	ApplyBC(FVec& vecout, AVec& avecout, const FVec& vecin, const AVec& avecin, const MGSize& surfid, const GVec& vpos, const GVec& vnn, const MGFloat& dy, const typename PhysicsCommon<DIM,EQN_RANS_2E>::PrdBCBase* pbc, const MGSize& nodeid) const;

	template <class VECTOR>
	bool	IsPhysical( const VECTOR& vec);

	MGFloat				MaxSpeed( const FVec& vec) const;
	const MGFloat&		cRefLength() const					{ return mRefLength;}
	const MGFloat&		cRe() const							{ return mRefRe;}
	const MGFloat&		cPr() const							{ return mRefPr;} 

protected:
	void	InitFVec( FVec& vec, const CfgSection& sec);

private:
	MGFloat		mRefLength;
	MGFloat		mRefRe;
	MGFloat		mRefPr;

};
//////////////////////////////////////////////////////////////////////


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_RANS_2E>::Create( const CfgSection* pcfgsec)		
{ 
	PhysicsCommon<DIM,EQN_RANS_2E>::Create( pcfgsec);
}


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_RANS_2E>::PostCreateCheck() const
{ 
	PhysicsCommon<DIM,EQN_RANS_2E>::PostCreateCheck();

	// do check
}


template <Geom::Dimension DIM>
inline MGFloat Physics<DIM,EQN_RANS_2E>::MaxSpeed( const FVec& vec) const
{ 
	return ::sqrt( EqnEuler<DIM>::SqrU( vec) ) + EqnEulerComm<DIM>::C( vec);
}



template <Geom::Dimension DIM>
template <class VECTOR>
bool Physics<DIM,EQN_RANS_2E>::IsPhysical( const VECTOR& vec)
{
	return true;
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RANSPHYSICS_H__



