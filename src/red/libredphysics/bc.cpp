#include "bc.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


const char	STRBC_FARFIELD[]	= "FARFIELD";
const char	STRBC_INVISCWALL[]	= "INVISCWALL";
const char	STRBC_SYMMETRY[]	= "SYMMETRY";
const char	STRBC_VISCWALL[]	= "VISCWALL";
const char	STRBC_OUTLET[]		= "OUTLET";
const char	STRBC_EXTRAPOL[]	= "EXTRAPOL";
const char	STRBC_DEF[]			= "DEF";
const char	STRBC_PERIODIC[]	= "PERIODIC";

BC	StrToBC( const MGString& str)
{
	if ( strcmp( str.c_str(), STRBC_FARFIELD ) == 0)
		return BC_FARFIELD;
	else if ( strcmp( str.c_str(), STRBC_INVISCWALL ) == 0)
		return BC_INVISCWALL;
	else if ( strcmp( str.c_str(), STRBC_VISCWALL ) == 0)
		return BC_VISCWALL;
	else if ( strcmp( str.c_str(), STRBC_SYMMETRY ) == 0)
		return BC_SYMMETRY;
	else if ( strcmp( str.c_str(), STRBC_OUTLET ) == 0)
		return BC_OUTLET;
	else if ( strcmp( str.c_str(), STRBC_EXTRAPOL ) == 0)
		return BC_EXTRAPOL;
	else if ( strcmp( str.c_str(), STRBC_DEF ) == 0)
		return BC_DEF;
	else if (strcmp(str.c_str(), STRBC_PERIODIC) == 0)
		return BC_PERIODIC;
	else 
		return BC_NONE;
}


MGString BCToStr( const BC& bc)
{
	switch ( bc)
	{
	case BC_FARFIELD:
		return STRBC_FARFIELD;

	case BC_INVISCWALL:
		return STRBC_INVISCWALL;

	case BC_VISCWALL:
		return STRBC_SYMMETRY;

	case BC_SYMMETRY:
		return STRBC_SYMMETRY;

	case BC_OUTLET:
		return STRBC_OUTLET;

	case BC_EXTRAPOL:
		return STRBC_EXTRAPOL;

	case BC_DEF:
		return STRBC_DEF;

	case BC_PERIODIC:
		return STRBC_PERIODIC;

	default:
		THROW_INTERNAL( "Unknown BC");

	}
	return "";
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

