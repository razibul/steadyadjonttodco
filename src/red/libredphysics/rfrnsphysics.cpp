#include "libredcore/configconst.h"
#include "libredphysics/physics.h"
#include "libredphysics/eulerequations.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

	
//////////////////////////////////////////////////////////////////////
//	class Physics for Navier-Stokes
//////////////////////////////////////////////////////////////////////


// HACK :: this the same as for Euler...
template <Geom::Dimension DIM>
void Physics<DIM,EQN_RFR_NS>::InitFVec( FVec& vec, const CfgSection& sec)
{
	FVec	vs( ESIZE, 0.0);

	if ( strcmp( sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY ).c_str(), ConfigStr::Solver::Physics::Var::Type::Value::MRPA ) == 0 )
	{
		MGFloat ma = sec.ValueFloat( ConfigStr::Solver::Physics::Var::MACH );
		MGFloat alpha = M_PI / 180.0 * sec.ValueFloat( ConfigStr::Solver::Physics::Var::ALPHA );
		MGFloat ro =  sec.ValueFloat( ConfigStr::Solver::Physics::Var::RO );
		MGFloat p =  sec.ValueFloat( ConfigStr::Solver::Physics::Var::P );

		MGFloat c = ::sqrt( FLOW_K * p / ro);

		vs( EqDef::ID_RHO) = ro;
		vs( EqDef::ID_P) = p;
		vs( EqDef::template U<0>::ID) = cos( alpha) * ma * c;
		vs( EqDef::template U<1>::ID) = sin( alpha) * ma * c;
		if ( DIM >= Geom::DIM_3D)
			vs( EqDef::template U<2>::ID) = 0;

		//vs.Write();
	}
	else if ( strcmp( sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY).c_str(), ConfigStr::Solver::Physics::Var::Type::Value::RPUV) == 0 )
	{
		vs( EqDef::ID_RHO) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::RO);;
		vs( EqDef::ID_P) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::P);
		vs( EqDef::template U<0>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::UX );
		vs( EqDef::template U<1>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::UY );
		if ( DIM >= Geom::DIM_3D)
			vs( EqDef::template U<2>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::UZ );

		//vs.Write();
	}
	else if ( strcmp( sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY).c_str(), ConfigStr::Solver::Physics::Var::Type::Value::RRERURV) == 0 )
	{
		vec( EqDef::ID_RHO) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::RO);;
		vec( EqDef::ID_P) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROE);
		vec( EqDef::template U<0>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROUX );
		vec( EqDef::template U<1>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROUY );
		if ( DIM >= Geom::DIM_3D)
			vs( EqDef::template U<2>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROUZ);

		vs = EqnEuler<DIM>::ConservToSimp( vs);
		//vs.Write();
	}
	else
	{
		THROW_INTERNAL( "unrecognized type of variable: '" << sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY) << "'" );
	}

	vec = EqnEuler<DIM>::SimpToConserv( vs);
	//vec.Write();
}


template <Geom::Dimension DIM>
void Physics<DIM,EQN_RFR_NS>::Init()
{
	//if ( strcmp( sec.Name().c_str(), STR_CFGSEC_SPACE_NAME) != 0)
	//	THROW_INTERNAL( "Bad section used for CfgSpaceSolver");

	mRefLength = this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::REF_LENGTH );
	mRefRe = this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::REF_REYNOLDS );
	mRefPr = this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::REF_PRANDTL );

	cout << endl << "Re = " << mRefRe << endl;
	cout << "Prandtl = " << mRefPr << endl << endl;

	// parent class Init()
	PhysicsCommon<DIM,EQN_RFR_NS>::Init();

	mRFRdef.Init();

	cout << ConfigStr::Solver::Physics::REF_TRANSL_VELO		<< " = " << mRFRdef.mVelocity << endl;
	cout << ConfigStr::Solver::Physics::REF_ROT_OMEGA		<< " = " << mRFRdef.mRotOmega << endl;
	cout << ConfigStr::Solver::Physics::REF_ROT_AXIS_DIR	<< " = " << mRFRdef.mRotAxisDir << endl;
	cout << ConfigStr::Solver::Physics::REF_ROT_AXIS_ORIG	<< " = " << mRFRdef.mRotAxisOrig << endl;

	mRFRdef.ToUndim( this->cRefVar() );

}
 

template <Geom::Dimension DIM>
bool Physics<DIM,EQN_RFR_NS>::IsStrongBC( const MGSize& id) const
{
	BC	bc = this->cBC( id);

	switch ( bc)
	{
	case BC_VISCWALL:
	//case BC_OUTLET:
			return true;
	}

	return false;
}


template <Geom::Dimension DIM>
bool Physics<DIM,EQN_RFR_NS>::IsEqStrongBC( const MGSize& id, const MGSize& ie) const
{
	BC	bc = this->cBC( id);

	switch ( bc)
	{
	case BC_VISCWALL:
		if ( ie >= EqDef::template U<0>::ID && ie < EqDef::template U<DIM>::ID )
			return true;


	//case BC_OUTLET:
	//	if ( ie == EqDef::ID_P )
	//		return true;
	}

	return false;
}


template <Geom::Dimension DIM>
void Physics<DIM, EQN_RFR_NS>::ApplyBC(FVec& vecout, AVec& avecout, const FVec& vecin, const AVec& avecin, const MGSize& surfid, const GVec& vpos, const GVec& vnn, const MGFloat& dy, const typename PhysicsCommon<DIM,EQN_RFR_NS>::PrdBCBase* pbc, const MGSize& nodeid) const
{
	MGFloat	vmod, vvmod;
	GVec	vt, vv, vvn;

	BC	bc = this->cBC( surfid);
	MGSize	varid = this->cVarId( surfid);


	switch ( bc)
	{
	case BC_VISCWALL:
		{
			GVec vrelu = mRFRdef.BkgVelocity( vpos); 

			vecout = EqnEuler<DIM>::ConservToSimp( vecin );

			for ( MGSize k=0; k<DIM; ++k)
			{
				//vecout(2+k) = 0;
				vecout(2+k) = vrelu.cX(k);
				vecout(2+k) = max( vecout(2+k), 1.0e-20 );
			}

			//for ( MGSize k=0; k<DIM; ++k)
			//	vv.rX((MGInt)k) = vecin(2+k);
			//vecout(1) += 0.5*vecout(0)*(vv*vv);

			vecout = EqnEuler<DIM>::SimpToConserv( vecout);

			break; 
		}
	case BC_SYMMETRY:
	case BC_INVISCWALL:
		{
			//for ( MGSize k=0; k<DIM; ++k)
			//	vv.rX((MGInt)k) = vecin(2+k);

			//vmod = vv.module();
			//vvn = (vv*vnn)*vnn;
			//vv = vv - vvn;

			//vvmod = vv.module();
			//if ( vvmod > ZERO )
			//	vv *= vmod / vvmod;


			//vecout(0) = vecin(0);
			//vecout(1) = vecin(1);

			//for ( MGSize k=0; k<DIM; ++k)
			//	vecout(2+k) = vv.cX( (MGInt)k );

			//break;

			GVec vrelu = mRFRdef.BkgVelocity( vpos); // cRefVelocity() / ( ::sqrt( EqnEuler<DIM>::SqrU( cRefVar() ) ) / cRefVar()( 0) );

			for ( MGSize k=0; k<DIM; ++k)
				vv.rX((MGInt)k) = vecin(2+k);

			vv -= vecin(0)*vrelu;

			vmod = vv.module();
			vvn = (vv*vnn)*vnn;
			vv = vv - vvn;

			vvmod = vv.module();
			if ( vvmod > ZERO )
				vv *= vmod / vvmod;

			vv += vecin(0)*vrelu;

			vecout(0) = vecin(0);
			vecout(1) = vecin(1);

			for ( MGSize k=0; k<DIM; ++k)
				vecout(2+k) = vv.cX( (MGInt)k );

			break;

		}
	case BC_FARFIELD:
		{
			vecout = this->cDefVar( 0);
			break;
		}
	case BC_OUTLET:
		{
			FVec vref = EqnEuler<DIM>::ConservToSimp( this->cDefVar( 0) );
			vecout    = EqnEuler<DIM>::ConservToSimp( vecin );

			vecout(1) = vref(1); 

			vecout = EqnEuler<DIM>::SimpToConserv( vecout);
			break;
		}
	case BC_EXTRAPOL:
		{
			FVec vref = EqnEuler<DIM>::ConservToSimp( this->cDefVar( 0) );
			vecout    = EqnEuler<DIM>::ConservToSimp( vecin );

			vecout(1) = vref(1); 

			//if ( vpos.cY() < 5.0e-7)
			//{
			//	for ( MGSize k=0; k<DIM; ++k)
			//		vecout(2+k) *= 1.0e-10;
			//}

			vecout = EqnEuler<DIM>::SimpToConserv( vecout);
			break;
		}
	case BC_PERIODIC:
	{
			FVec tmp;
			pbc->GetPeriodicVar(nodeid, tmp);
			for (MGSize i = 0; i < ESIZE; ++i)
			{
				vecout(i) = tmp(i);
			}
			break;
	}
	case BC_DEF:
		{
			vecout = this->cDefVar( varid);
			break;
		}
	default:
		{
			char sbuf[1024];
			sprintf( sbuf, "BC not implemented! %d", bc);
			THROW_INTERNAL( sbuf);
		}
	}

}


//////////////////////////////////////////////////////////////////////
template class Physics<Geom::DIM_2D, EQN_RFR_NS>;
template class Physics<Geom::DIM_3D, EQN_RFR_NS>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

