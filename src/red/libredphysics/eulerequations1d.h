#ifndef __EQNEULER1D_H__
#define __EQNEULER1D_H__



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <class VECTOR> 
inline MGFloat EqnEuler<Geom::DIM_1D>::SqrU( const VECTOR& var)
{
	return	var( ETEuler::U<0>::ID) * var( ETEuler::U<0>::ID);
}

// Conversion of the variables	
template <class VECTOR> 
inline VECTOR EqnEuler<Geom::DIM_1D>::SimpToConserv( const VECTOR& vsmp)
{
	VECTOR vcns;
	MGFloat u2 = SqrU( vsmp);

	vcns( ETEuler::ID_RHO)     = vsmp( ETEuler::ID_RHO);
	vcns( ETEuler::ID_P)     = vsmp( ETEuler::ID_P) / (FLOW_K-1) + 0.5 * vsmp( ETEuler::ID_RHO) * u2;
	vcns( ETEuler::U<0>::ID) = vsmp( ETEuler::ID_RHO) * vsmp( ETEuler::U<0>::ID);

	return vcns;
}



template <class VECTOR>	
inline VECTOR EqnEuler<Geom::DIM_1D>::ConservToSimp( const VECTOR& vcns)
{
	VECTOR vsmp;
	MGFloat u2 = SqrU( vcns);

	vsmp( ETEuler::ID_RHO)     = vcns(  ETEuler::ID_RHO);
	vsmp( ETEuler::ID_P)     = (FLOW_K - 1)*(vcns( ETEuler::ID_P) - 0.5*u2/vcns(  ETEuler::ID_RHO) );
	vsmp( ETEuler::U<0>::ID) = vcns( ETEuler::U<0>::ID) / vcns(  ETEuler::ID_RHO);

	return vsmp;
}


template <class VECTOR>	
inline VECTOR EqnEuler<Geom::DIM_1D>::ConservToZ( const VECTOR& vcns)
{
	VECTOR vz;
	MGFloat u2 = SqrU( vcns);

	vz( ETEuler::ID_RHO)     = ::sqrt( vcns( ETEuler::ID_RHO) );
	vz( ETEuler::ID_P)     = ( FLOW_K*vcns( ETEuler::ID_P) - (FLOW_K-1)*0.5*u2/vcns( ETEuler::ID_RHO) ) / vz( ETEuler::ID_RHO);
	vz( ETEuler::U<0>::ID) = vcns( ETEuler::U<0>::ID)/vz( ETEuler::ID_RHO);

	return vz;
}


template <class VECTOR>	
inline VECTOR EqnEuler<Geom::DIM_1D>::ZToConserv( const VECTOR& vz)
{
	VECTOR vcns;
	MGFloat u2 = SqrU( vz);

	vcns( ETEuler::ID_RHO)     = vz( ETEuler::ID_RHO)*vz( ETEuler::ID_RHO);
	vcns( ETEuler::ID_P)     = ( vz( ETEuler::ID_RHO)*vz( ETEuler::ID_P) + (FLOW_K-1)*0.5*u2 )/FLOW_K;
	vcns( ETEuler::U<0>::ID) = vz( ETEuler::U<0>::ID)*vz( ETEuler::ID_RHO);

	return vcns;
}


template <class VECTOR>	
inline VECTOR EqnEuler<Geom::DIM_1D>::SimpToZ( const VECTOR& vsmp)
{
	VECTOR vz;
	MGFloat u2 = SqrU( vsmp);

	vz( ETEuler::ID_RHO)     = ::sqrt( vsmp( ETEuler::ID_RHO));
	vz( ETEuler::ID_P)     = vz( ETEuler::ID_RHO)*( FLOW_K/(FLOW_K-1) * vsmp( ETEuler::ID_P)/vsmp( ETEuler::ID_RHO) + 0.5*u2 );
	vz( ETEuler::U<0>::ID) = vsmp( ETEuler::U<0>::ID)*vz( ETEuler::ID_RHO);

	return vz;
}


template <class VECTOR>	
inline VECTOR EqnEuler<Geom::DIM_1D>::ZToSimp( const VECTOR& vz)
{
	VECTOR vsmp;
	MGFloat u2 = SqrU( vz);
	
	vsmp( ETEuler::ID_RHO)     = vz( ETEuler::ID_RHO)*vz( ETEuler::ID_RHO);
	vsmp( ETEuler::ID_P)     = (FLOW_K-1)/FLOW_K * ( vz( ETEuler::ID_RHO)*vz( ETEuler::ID_P) - 0.5*u2 );
	vsmp( ETEuler::U<0>::ID) = vz( ETEuler::U<0>::ID)/vz( ETEuler::ID_RHO);

	return vsmp;
}

// Jacobian matrices in primitive variables
template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::JacobAV( MATRIX& mtx, const VECTOR& vsmp)
{
	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0.0);

	mtx(0,0) = vsmp(2);
	mtx(1,1) = vsmp(2);
	mtx(2,2) = vsmp(2);

	mtx(0,2) = vsmp(0);
	mtx(1,2) = FLOW_K*vsmp(1);
	mtx(2,1) = 1.0/vsmp(0);
}



////

template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::DecompL( MATRIX& mtx, const VECTOR& vs, const Geom::Vect2D& vn)
{
	MGFloat c = ::sqrt( FLOW_K*vs(1)/vs(0) );

	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0);

	mtx(0,0) = 1;
	mtx(0,1) = -1.0/(c*c);
	mtx(2,1) = 1.0/(vs(0)*c);
	mtx(2,2) = 1;
}

template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::DecompLI( MATRIX& mtx, const VECTOR& vs, const Geom::Vect2D& vn)
{
	MGFloat c = ::sqrt( FLOW_K*vs(1)/vs(0) );

	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0);

	mtx(0,0) = 1;
	mtx(0,2) = mtx(0,3) = vs(0)*0.5/c;

	mtx(1,2) = mtx(1,3) = 0.5*vs(0)*c;

	mtx(2,2) = 0.5;
}


// Conversion Jacobian matrices
template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::CJacobZV( MATRIX& mtx, const VECTOR& vz)
{
	MGFloat u2 = SqrU( vz);

	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0);

	mtx(2,2) = vz(0);
	mtx(1,2) = vz(2);
	mtx(2,0) = 0.5*vz(2)/(vz(0)*vz(0));


	mtx(0,0) = 0.5/vz(0);
	mtx(1,0) = 0.5*( u2/(vz(0)*vz(0)*vz(0)) - vz(1)/(vz(0)*vz(0)) );
	mtx(1,1) = FLOW_K/((FLOW_K-1)*vz(0));
}

template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::CJacobVZ( MATRIX& mtx, const VECTOR& vz)
{
	MGFloat	z1_inv = 1.0/vz(0);
	MGFloat frac_k = (FLOW_K - 1)/FLOW_K;

	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0);

	mtx(2,2) = z1_inv;
	mtx(1,2) = -frac_k*vz(2);
	mtx(2,0) = -vz(2)/(vz(0)*vz(0));

	mtx(0,0) = 2*vz(0);
	mtx(1,0) = frac_k*vz(1);
	mtx(1,1) = frac_k*vz(0);
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::CJacobZQ( MATRIX& mtx, const VECTOR& vz)
{
	MGFloat	z1_inv = 1.0/vz(0);
	MGFloat u2 = SqrU( vz);

	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0);

	mtx(2,2) = z1_inv;
	mtx(1,2) = -(FLOW_K - 1)*vz(2)/(vz(0)*vz(0));
	mtx(2,0) = -0.5*vz(2)/(vz(0)*vz(0));

	mtx(0,0) = 0.5*z1_inv;
	mtx(1,0) = 0.5*( (FLOW_K-1)*u2/(vz(0)*vz(0)*vz(0)) - vz(1)/(vz(0)*vz(0)) );
	mtx(1,1) = FLOW_K*z1_inv;
}


//template<>
template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::CJacobQZ( MATRIX& mtx, const VECTOR& vz)
{
	MGFloat z1_inv = 1.0/vz(0);
	MGFloat frac_k = (FLOW_K - 1)/FLOW_K;

	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0);

	mtx(2,2) = vz(0);
	mtx(1,2) = frac_k*vz(2);
	mtx(2,0) = vz(2);

	mtx(3,3) = vz(0);
	mtx(1,3) = frac_k*vz(3);
	mtx(3,0) = vz(3);

	mtx(0,0) = 2*vz(0);
	mtx(1,0) = vz(1)/FLOW_K;
	mtx(1,1) = vz(0)/FLOW_K;
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::CJacobQV( MATRIX& mtx, const VECTOR& vs)
{
	MGFloat	U2 = SqrU(vs);

	mtx.Resize( 3, 3);
	mtx.Init( 0);

	mtx(0,0) = 1.;

	mtx(1,0) = 0.5 * U2;
	mtx(1,1) = 1.0 / (FLOW_K - 1.);
	mtx(1,2) = vs(0)*vs(2);

	mtx(2,0) = vs(2);

	mtx(2,2) = vs(0);
}



template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::CJacobWV( MATRIX& mtx, const VECTOR& v)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat kx = q<ZERO ? 1.0 : v(2)/q;	// <----------
	MGFloat ky = q<ZERO ? 0.0 : v(3)/q; 

	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0);

	mtx(0,0) = -c*c;
	mtx(0,1) = 1;
	
	mtx(1,1) = 1/v(0)/c;

	mtx(2,2) = kx;
	mtx(2,3) = ky;
	mtx(3,2) = -ky;
	mtx(3,3) = kx;
}



template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::CJacobVW( MATRIX& mtx, const VECTOR& v)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat kx = q<ZERO ? 1.0 : v(2)/q;	// <----------
	MGFloat ky = q<ZERO ? 0.0 : v(3)/q; 

	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0);

	mtx(0,0) = -1.0/(c*c);
	mtx(0,1) = v(0)/c;
	
	mtx(1,1) = v(0)*c;

	mtx(2,2) = kx;
	mtx(2,3) = -ky;
	mtx(3,2) = ky;
	mtx(3,3) = kx;
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::CJacobXW( MATRIX& mtx, const VECTOR& v, const MGFloat& beta)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat M = q/c;
	M = M<MACH_ZERO ? MACH_ZERO : M;

	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0);

	mtx(0,0) = 1;

	mtx(1,1) = v(0)*c;
	mtx(1,2) = v(0)*q;
	
	mtx(2,1) = beta/M;
	mtx(2,3) = 1;

	mtx(3,1) = beta/M;
	mtx(3,3) = -1;
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::CJacobWX( MATRIX& mtx, const VECTOR& v, const MGFloat& beta)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat M = q/c; 
	M = M<MACH_ZERO ? MACH_ZERO : M;

	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0);

	mtx(0,0) = 1;

	mtx(1,2) = 0.5*M/beta;
	mtx(1,3) = 0.5*M/beta;
	
	mtx(2,1) = 1.0/(v(0)*q);
	mtx(2,2) = -0.5/beta;
	mtx(2,3) = -0.5/beta;

	mtx(3,2) = 0.5;
	mtx(3,3) = -0.5;
}





template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::PrecLLR( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat M = q/c; 
	M = M<MACH_ZERO ? MACH_ZERO : M;

	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0);

	mtx(0,0) = 1.0;
	
	mtx(1,1) = chi*M*M/(beta*beta);
	mtx(1,2) = -chi*M/(beta*beta);

	mtx(2,1) = -chi*M/(beta*beta);
	mtx(2,2) = 1.0 + chi/(beta*beta);

	mtx(3,3) = chi;
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::PrecInvLLR( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat M = q/c; 
	M = M<MACH_ZERO ? MACH_ZERO : M;

	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0);

	mtx(0,0) = 1.0;
	
	mtx(1,1) = (beta*beta + chi)/(chi*M*M);
	mtx(1,2) = 1.0/M;

	mtx(2,1) = 1.0/M;
	mtx(2,2) = 1.0;

	mtx(3,3) = 1.0/chi;
}

template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_1D>::RotMtx( MATRIX& mtx, const VECTOR& v)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat kx = q<ZERO ? 1.0 : v(2)/q;	// <----------
	MGFloat ky = q<ZERO ? 0.0 : v(3)/q; 

	mtx.Resize( 1, 1);
	mtx.Init( 0);
	mtx(0,0) = 1.0;
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EQNEULER1D_H__
