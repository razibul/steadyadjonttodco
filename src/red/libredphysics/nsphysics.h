#ifndef __NSPHYSICS_H__
#define __NSPHYSICS_H__


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Physics for Navier-Stokes
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class Physics<DIM,EQN_NS> : public PhysicsCommon<DIM,EQN_NS>
{
	enum { ESIZE = PhysicsCommon<DIM,EQN_NS>::ESIZE };

	typedef typename PhysicsCommon<DIM,EQN_NS>::EqDef	EqDef;
	typedef typename PhysicsCommon<DIM,EQN_NS>::FVec	FVec;
	typedef typename PhysicsCommon<DIM,EQN_NS>::AVec	AVec;
	typedef typename PhysicsCommon<DIM,EQN_NS>::GVec	GVec;

public:
	Physics()	: PhysicsCommon<DIM,EQN_NS>()	{}

	virtual void Create( const CfgSection* pcfgsec);
	virtual void PostCreateCheck() const;
	virtual void Init();

	virtual void	DimToUndim( FVec& vout, const FVec& vin, const FVec& vref ) const;
	virtual void	UndimToDim( FVec& vout, const FVec& vin, const FVec& vref ) const;

	bool			IsStrongBC( const MGSize& id) const;
	bool			IsEqStrongBC( const MGSize& id, const MGSize& ie) const;

	void	ApplyBC(FVec& vecout, AVec& avecout, const FVec& vecin, const AVec& avecin, const MGSize& surfid, const GVec& vpos, const GVec& vnn, const MGFloat& dy, const typename PhysicsCommon<DIM,EQN_NS>::PrdBCBase* pbc, const MGSize& nodeid) const;

	template <class VECTOR>
	bool	IsPhysical( const VECTOR& vec);

	MGFloat				MaxSpeed( const FVec& vec) const;
	const MGFloat&		cRefLength() const					{ return mRefLength;}
	const MGFloat&		cRe() const							{ return mRefRe;}
	const MGFloat&		cPr() const							{ return mRefPr;} 

protected:
	void	InitFVec( FVec& vec, const CfgSection& sec);

private:
	MGFloat		mRefLength;
	MGFloat		mRefRe;
	MGFloat		mRefPr;

};
//////////////////////////////////////////////////////////////////////


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_NS>::Create( const CfgSection* pcfgsec)		
{ 
	PhysicsCommon<DIM,EQN_NS>::Create( pcfgsec);
}


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_NS>::PostCreateCheck() const
{ 
	PhysicsCommon<DIM,EQN_NS>::PostCreateCheck();

	// do check
}

template <Geom::Dimension DIM>
inline MGFloat Physics<DIM,EQN_NS>::MaxSpeed( const FVec& vec) const
{ 
	return ::sqrt( EqnEuler<DIM>::SqrU( vec) ) + EqnEulerComm<DIM>::C( vec);
}

template <Geom::Dimension DIM>
template <class VECTOR>
bool Physics<DIM,EQN_NS>::IsPhysical( const VECTOR& vec)
{
	return true;
}

template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_NS>::DimToUndim( FVec& vout, const FVec& vin, const FVec& vref ) const
{
	vout = EqnEulerComm<DIM>::DimToUndim( vin, vref);
}


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_NS>::UndimToDim( FVec& vout, const FVec& vin, const FVec& vref ) const
{
	vout = EqnEulerComm<DIM>::UndimToDim( vin, vref);
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __NSPHYSICS_H__



