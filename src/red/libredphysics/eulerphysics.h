#ifndef __EULERPHYSICS_H__
#define __EULERPHYSICS_H__

#include "libredphysics/eulerequations.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Physics for Euler
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class Physics<DIM,EQN_EULER> : public PhysicsCommon<DIM,EQN_EULER>
{
	enum { ESIZE = PhysicsCommon<DIM,EQN_EULER>::ESIZE };

	typedef typename PhysicsCommon<DIM,EQN_EULER>::EqDef	EqDef;
	typedef typename PhysicsCommon<DIM,EQN_EULER>::FVec		FVec;
	typedef typename PhysicsCommon<DIM,EQN_EULER>::AVec		AVec;
	typedef typename PhysicsCommon<DIM,EQN_EULER>::GVec		GVec;

public:
	Physics()	: PhysicsCommon<DIM,EQN_EULER>()	{}

	virtual void Create( const CfgSection* pcfgsec);
	virtual void PostCreateCheck() const;
	virtual void Init();

	virtual void	DimToUndim( FVec& vout, const FVec& vin, const FVec& vref ) const;
	virtual void	UndimToDim( FVec& vout, const FVec& vin, const FVec& vref ) const;

	void	ApplyBC(FVec& vecout, AVec& avecout, const FVec& vecin, const AVec& avecin, const MGSize& surfid, const GVec& vpos, const GVec& vnn, const MGFloat& dy, const typename PhysicsCommon<DIM,EQN_EULER>::PrdBCBase* pbc, const MGSize& nodeid) const;

	template <class VECTOR>
	bool	IsPhysical( const VECTOR& vec);

	MGFloat				MaxSpeed( const FVec& vec) const;
	const MGFloat&		cRefLength() const					{ return mRefLength;}

protected:
	void	InitFVec( FVec& vec, const CfgSection& sec);

private:
	MGFloat		mRefLength;

};
//////////////////////////////////////////////////////////////////////



template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_EULER>::Create( const CfgSection* pcfgsec)		
{ 
	PhysicsCommon<DIM,EQN_EULER>::Create( pcfgsec);
}


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_EULER>::PostCreateCheck() const
{ 
	PhysicsCommon<DIM,EQN_EULER>::PostCreateCheck();

	// do check
}


template <Geom::Dimension DIM>
inline MGFloat Physics<DIM,EQN_EULER>::MaxSpeed( const FVec& vec) const
{ 
	return ::sqrt( EqnEuler<DIM>::SqrU( vec) ) + EqnEulerComm<DIM>::C( vec);
}

template <Geom::Dimension DIM>
template <class VECTOR>
bool Physics<DIM,EQN_EULER>::IsPhysical( const VECTOR& vec)
{
	return true;
}
	

template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_EULER>::DimToUndim( FVec& vout, const FVec& vin, const FVec& vref ) const
{
	vout = EqnEulerComm<DIM>::DimToUndim( vin, vref);
}


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_EULER>::UndimToDim( FVec& vout, const FVec& vin, const FVec& vref ) const
{
	vout = EqnEulerComm<DIM>::UndimToDim( vin, vref);
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EULERPHYSICS_H__
