#ifndef __RANSKOMEGAPHYSICS_H__
#define __RANSKOMEGAPHYSICS_H__


#include "ranskomegaequations.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Physics for RANS with 1 turbulence equations
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class Physics<DIM,EQN_RANS_KOMEGA> : public PhysicsCommon<DIM,EQN_RANS_KOMEGA>
{
	enum { ESIZE = PhysicsCommon<DIM,EQN_RANS_KOMEGA>::ESIZE };

	typedef typename PhysicsCommon<DIM,EQN_RANS_KOMEGA>::EqDef	EqDef;
	typedef typename PhysicsCommon<DIM,EQN_RANS_KOMEGA>::FVec	FVec;
	typedef typename PhysicsCommon<DIM,EQN_RANS_KOMEGA>::AVec	AVec;
	typedef typename PhysicsCommon<DIM,EQN_RANS_KOMEGA>::GVec	GVec;

public:
	Physics()	: PhysicsCommon<DIM,EQN_RANS_KOMEGA>()	{}

	virtual void Create( const CfgSection* pcfgsec);
	virtual void PostCreateCheck() const;
	virtual void Init();

	virtual void	DimToUndim( FVec& vout, const FVec& vin, const FVec& vref ) const;
	virtual void	UndimToDim( FVec& vout, const FVec& vin, const FVec& vref ) const;


	bool			IsStrongBC( const MGSize& id) const;
	bool			IsEqStrongBC( const MGSize& id, const MGSize& ie) const;

	void	ApplyBC(FVec& vecout, AVec& avecout, const FVec& vecin, const AVec& avecin, const MGSize& surfid, const GVec& vpos, const GVec& vnn, const MGFloat& dy, const typename PhysicsCommon<DIM,EQN_RANS_KOMEGA>::PrdBCBase* pbc, const MGSize& nodeid) const;

	template <class VECTOR>
	bool	IsPhysical( const VECTOR& vec);

	MGFloat				MaxSpeed( const FVec& vec) const;
	const MGFloat&		cRefLength() const					{ return mRefLength;}
	const MGFloat&		cRe() const							{ return mRefRe;}
	const MGFloat&		cPr() const							{ return mRefPr;} 
	const MGFloat&		cPrT() const						{ return mRefPrT;} 

protected:
	void	InitFVec( FVec& vec, const CfgSection& sec);

private:
	MGFloat		mRefLength;
	MGFloat		mRefRe;
	MGFloat		mRefPr;
	MGFloat		mRefPrT;

};
//////////////////////////////////////////////////////////////////////


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_RANS_KOMEGA>::Create( const CfgSection* pcfgsec)		
{ 
	PhysicsCommon<DIM,EQN_RANS_KOMEGA>::Create( pcfgsec);
}


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_RANS_KOMEGA>::PostCreateCheck() const
{ 
	PhysicsCommon<DIM,EQN_RANS_KOMEGA>::PostCreateCheck();

	// do check
}


template <Geom::Dimension DIM>
inline MGFloat Physics<DIM,EQN_RANS_KOMEGA>::MaxSpeed( const FVec& vec) const
{ 
	MGFloat uomega = 0.;
	//MGFloat uomega = exp( vec( EqDef::ID_OMEGA) / vec( EqDef::ID_RHO) );// * cRefLength();

	MGFloat uconvect = ::sqrt( EqnRansKOMEGA<DIM>::SqrU( vec) ) + EqnRansKOMEGA<DIM>::C( vec);
	return max( uomega, uconvect);
}



template <Geom::Dimension DIM>
template <class VECTOR>
bool Physics<DIM,EQN_RANS_KOMEGA>::IsPhysical( const VECTOR& vec)
{
	return true;
}



template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_RANS_KOMEGA>::DimToUndim( FVec& vout, const FVec& vin, const FVec& vref ) const
{
	//vout = vin;

	MGFloat u2 = EqnRansKOMEGA<DIM>::SqrU( vref);
	MGFloat u = ::sqrt( u2);

	//vout = EqnRansKOMEGA<DIM>::DimToUndim( vin, vref);
	vout( EqDef::ID_RHO)	= vin( EqDef::ID_RHO) / vref( EqDef::ID_RHO);
	vout( EqDef::ID_P)		= vin( EqDef::ID_P) * vref( EqDef::ID_RHO) / u2;
	for ( MGSize i=EqDef::template U<0>::ID; i<EqDef::template U<0>::ID + DIM; ++i)
		vout( i) = vin(i) / u;

	vout( EqDef::ID_K)		= /*mRefRe * */vin( EqDef::ID_K) / vref( EqDef::ID_RHO);
	vout( EqDef::ID_OMEGA)	= vin( EqDef::ID_OMEGA) / vref( EqDef::ID_RHO);

	//vout( EqDef::ID_K)	= mRefRe * vin( EqDef::ID_K) * vref( EqDef::ID_RHO) / u2;
	//vout( EqDef::ID_OMEGA)	=  ( vin( EqDef::ID_OMEGA) + vin( EqDef::ID_RHO) * log( cRefLength()/u ) ) / vref( EqDef::ID_RHO);

}

template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_RANS_KOMEGA>::UndimToDim( FVec& vout, const FVec& vin, const FVec& vref ) const
{
	//vout = vin;

	MGFloat u2 = EqnRansKOMEGA<DIM>::SqrU( vref);
	MGFloat u = ::sqrt( u2);

	//vout = EqnRansKOMEGA<DIM>::UndimToDim( vin, vref);
	vout( EqDef::ID_RHO)	= vin( EqDef::ID_RHO) * vref( EqDef::ID_RHO);
	vout( EqDef::ID_P)	= vin( EqDef::ID_P) * u2 / vref( EqDef::ID_RHO);
	for ( MGSize i=EqDef::template U<0>::ID; i<EqDef::template U<0>::ID + DIM; ++i)
		vout( i) = vin(i) * u;

	vout( EqDef::ID_K)		= vin( EqDef::ID_K) * vref( EqDef::ID_RHO);// / mRefRe;
	vout( EqDef::ID_OMEGA)	= vin( EqDef::ID_OMEGA) * vref( EqDef::ID_RHO);

	//vout( EqDef::ID_K)	= vin( EqDef::ID_K) * u2 / vref( EqDef::ID_RHO) / mRefRe;
	//vout( EqDef::ID_OMEGA)	=  ( vin( EqDef::ID_OMEGA) - vin( EqDef::ID_RHO) * log( cRefLength()/u ) ) * vref( EqDef::ID_RHO);
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RANSKOMEGAPHYSICS_H__



