#ifndef __EQNEULER_H__
#define __EQNEULER_H__

#include "eulerconst.h"
#include "libcoregeom/vect.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


const MGFloat MACH_ZERO = 1.0e-7;




//////////////////////////////////////////////////////////////////////
// class EqnEuler - dummy class needed for specialization
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class EqnEuler
{
};


//////////////////////////////////////////////////////////////////////
// class EqnEuler for 1D operations
//////////////////////////////////////////////////////////////////////
template <>
class EqnEuler<Geom::DIM_1D>
{
	typedef EquationDef<Geom::DIM_1D,EQN_EULER> ETEuler;

public:
	template <class VECTOR> static MGFloat SqrU( const VECTOR& var);

	// Conversion of the variables	
	template <class VECTOR> static VECTOR SimpToConserv( const VECTOR& vsmp);
	template <class VECTOR>	static VECTOR ConservToSimp( const VECTOR& vcns);
	template <class VECTOR>	static VECTOR ConservToZ( const VECTOR& vcns);
	template <class VECTOR>	static VECTOR ZToConserv( const VECTOR& vz);
	template <class VECTOR>	static VECTOR SimpToZ( const VECTOR& vsmp);
	template <class VECTOR>	static VECTOR ZToSimp( const VECTOR& vz);

	// Jacobian matrices in primitive variables
	template <class MATRIX, class VECTOR> static void JacobAV( MATRIX& mtx, const VECTOR& vsmp);

	// Decompostion matrices
	template <class MATRIX, class VECTOR> static void DecompL( MATRIX& mtx, const VECTOR& vs, const Geom::Vect2D& vn);
	template <class MATRIX, class VECTOR> static void DecompLI( MATRIX& mtx, const VECTOR& vs, const Geom::Vect2D& vn);

	// Conversion Jacobian matrices
	template <class MATRIX, class VECTOR> static void CJacobZV( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobVZ( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobZQ( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobQZ( MATRIX& mtx, const VECTOR& vz);

	template <class MATRIX, class VECTOR> static void CJacobQV( MATRIX& mtx, const VECTOR& vz);

	template <class MATRIX, class VECTOR> static void CJacobWV( MATRIX& mtx, const VECTOR& v);
	template <class MATRIX, class VECTOR> static void CJacobVW( MATRIX& mtx, const VECTOR& v);
	template <class MATRIX, class VECTOR> static void CJacobXW( MATRIX& mtx, const VECTOR& v, const MGFloat& beta);
	template <class MATRIX, class VECTOR> static void CJacobWX( MATRIX& mtx, const VECTOR& v, const MGFloat& beta);

	template <class MATRIX, class VECTOR> static void PrecLLR( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);
	template <class MATRIX, class VECTOR> static void PrecInvLLR( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);

	template <class MATRIX, class VECTOR> static void RotMtx( MATRIX& mtx, const VECTOR& v);
};


//////////////////////////////////////////////////////////////////////
// class EqnEuler for 2D operations
//////////////////////////////////////////////////////////////////////
template <>
class EqnEuler<Geom::DIM_2D>
{
	typedef EquationDef<Geom::DIM_2D,EQN_EULER> ETEuler;

public:
	template <class VECTOR> static MGFloat SqrU( const VECTOR& var);

	// Conversion of the variables	
	template <class VECTOR> static VECTOR SimpToConserv( const VECTOR& vsmp);
	template <class VECTOR>	static VECTOR ConservToSimp( const VECTOR& vcns);
	template <class VECTOR>	static VECTOR ConservToZ( const VECTOR& vcns);
	template <class VECTOR>	static VECTOR ZToConserv( const VECTOR& vz);
	template <class VECTOR>	static VECTOR SimpToZ( const VECTOR& vsmp);
	template <class VECTOR>	static VECTOR ZToSimp( const VECTOR& vz);

	// Jacobian matrices in primitive variables
	template <class MATRIX, class VECTOR> static void JacobAV( MATRIX& mtx, const VECTOR& vsmp);
	template <class MATRIX, class VECTOR> static void JacobBV( MATRIX& mtx, const VECTOR& vsmp);

	// Decompostion matrices
	template <class MATRIX, class VECTOR> static void DecompL( MATRIX& mtx, const VECTOR& vs, const Geom::Vect2D& vn);
	template <class MATRIX, class VECTOR> static void DecompLI( MATRIX& mtx, const VECTOR& vs, const Geom::Vect2D& vn);

	// Conversion Jacobian matrices
	template <class MATRIX, class VECTOR> static void CJacobZV( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobVZ( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobZQ( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobQZ( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobVQ( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobQV( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobWV( MATRIX& mtx, const VECTOR& v);
	template <class MATRIX, class VECTOR> static void CJacobVW( MATRIX& mtx, const VECTOR& v);
	template <class MATRIX, class VECTOR> static void CJacobXW( MATRIX& mtx, const VECTOR& v, const MGFloat& beta);
	template <class MATRIX, class VECTOR> static void CJacobWX( MATRIX& mtx, const VECTOR& v, const MGFloat& beta);

	template <class MATRIX, class VECTOR> static void PrecLLR( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);
	template <class MATRIX, class VECTOR> static void PrecInvLLR( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);

	template <class MATRIX, class VECTOR> static void RotMtx( MATRIX& mtx, const VECTOR& v);

	template <class MATRIX, class VECTOR> static void SubPAXni( const MGSize& i, MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);
	template <class MATRIX, class VECTOR> static void SubPAXn( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);
	template <class MATRIX, class VECTOR> static void SubPBXn( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);

	template <class MATRIX, class VECTOR> static void SubPAX( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);
	template <class MATRIX, class VECTOR> static void SubPBX( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);

};




//////////////////////////////////////////////////////////////////////
// class EqnEuler for 3D operations
//////////////////////////////////////////////////////////////////////
template <>
class EqnEuler<Geom::DIM_3D>
{
	typedef EquationDef<Geom::DIM_3D,EQN_EULER> ETEuler;

public:
	template <class VECTOR> static MGFloat SqrU( const VECTOR& var);

	// Conversion of the variables	
	template <class VECTOR> static VECTOR SimpToConserv( const VECTOR& vsmp);
	template <class VECTOR>	static VECTOR ConservToSimp( const VECTOR& vcns);
	template <class VECTOR>	static VECTOR ConservToZ( const VECTOR& vcns);
	template <class VECTOR>	static VECTOR ZToConserv( const VECTOR& vz);
	template <class VECTOR>	static VECTOR SimpToZ( const VECTOR& vsmp);
	template <class VECTOR>	static VECTOR ZToSimp( const VECTOR& vz);

	// Jacobian matrices in primitive variables
	template <class MATRIX, class VECTOR> static void JacobAV( MATRIX& mtx, const VECTOR& vsmp);
	template <class MATRIX, class VECTOR> static void JacobBV( MATRIX& mtx, const VECTOR& vsmp);
	template <class MATRIX, class VECTOR> static void JacobCV( MATRIX& mtx, const VECTOR& vsmp);

	// Decompostion matrices
	template <class MATRIX, class VECTOR> static void DecompL( MATRIX& mtx, const VECTOR& vs, const Geom::Vect3D& vn);
	template <class MATRIX, class VECTOR> static void DecompLI( MATRIX& mtx, const VECTOR& vs, const Geom::Vect3D& vn);

	// Conversion Jacobian matrices
	template <class MATRIX, class VECTOR> static void CJacobZV( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobVZ( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobZQ( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobQZ( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobVQ( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobQV( MATRIX& mtx, const VECTOR& vz);
	template <class MATRIX, class VECTOR> static void CJacobWV( MATRIX& mtx, const VECTOR& v);
	template <class MATRIX, class VECTOR> static void CJacobVW( MATRIX& mtx, const VECTOR& v);
	template <class MATRIX, class VECTOR> static void CJacobXW( MATRIX& mtx, const VECTOR& v, const MGFloat& beta);
	template <class MATRIX, class VECTOR> static void CJacobWX( MATRIX& mtx, const VECTOR& v, const MGFloat& beta);

	template <class MATRIX, class VECTOR> static void PrecLLR( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);
	template <class MATRIX, class VECTOR> static void PrecInvLLR( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);

	template <class MATRIX, class VECTOR> static void RotMtx( MATRIX& mtx, const VECTOR& v);

	template <class MATRIX, class VECTOR> static void SubPAXni( const MGSize& i, MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);
	template <class MATRIX, class VECTOR> static void SubPAXn( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);
	template <class MATRIX, class VECTOR> static void SubPBXn( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);
	template <class MATRIX, class VECTOR> static void SubPCXn( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi);


};



//////////////////////////////////////////////////////////////////////
// class EqnEulerComm
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class EqnEulerComm
{
	typedef EquationDef<DIM,EQN_EULER> ETEuler;

public:

	template <class VECTOR> static VECTOR DimToUndim( const VECTOR& vin, const VECTOR& vref)
	{
		VECTOR vout;
		MGFloat u2 = EqnEuler<DIM>::SqrU( vref);

		vout( ETEuler::ID_RHO)	= vin( ETEuler::ID_RHO) / vref( ETEuler::ID_RHO);
		vout( ETEuler::ID_P)	= vin( ETEuler::ID_P) * vref( ETEuler::ID_RHO) / u2;
		u2 = ::sqrt( u2);
		for ( MGSize i=2; i<DIM+2; ++i)
			vout( i) = vin(i) / u2;

		//return vout;
		return vin;
	}

	template <class VECTOR> static VECTOR UndimToDim( const VECTOR& vin, const VECTOR& vref)
	{
		VECTOR vout;
		MGFloat u2 = EqnEuler<DIM>::SqrU( vref);

		vout( ETEuler::ID_RHO)	= vin( ETEuler::ID_RHO) * vref( ETEuler::ID_RHO);
		vout( ETEuler::ID_P)	= vin( ETEuler::ID_P) * u2 / vref( ETEuler::ID_RHO);
		u2 = ::sqrt( u2);
		for ( MGSize i=2; i<DIM+2; ++i)
			vout( i) = vin(i) * u2;

		//return vout;
		return vin;
	}




	template <class VECTOR> static MGFloat Ro( const VECTOR& v)
	{
		return v( ETEuler::ID_RHO );
	}

	template <class VECTOR> static MGFloat RoE( const VECTOR& v)
	{
		return v( ETEuler::ID_P );
	}

	template <class VECTOR> static MGFloat Pressure( const VECTOR& var)
	{
		return (FLOW_K - 1)*( var( ETEuler::ID_P) - 0.5*EqnEuler<DIM>::SqrU(var)/var( ETEuler::ID_RHO) );
	}

	template <class VECTOR> static MGFloat Mach( const VECTOR& var)
	{
		MGFloat v = EqnEuler<DIM>::SqrU(var);
		MGFloat p = (FLOW_K - 1)*( var( ETEuler::ID_P) - 0.5*v/var( ETEuler::ID_RHO) );
		v = ::sqrt( v)/var( ETEuler::ID_RHO);
		MGFloat c = ::sqrt( FLOW_K*p/var( ETEuler::ID_RHO) );
		return v/c;
	}

	template <class VECTOR> static MGFloat Entropy( const VECTOR& var)
	{
		MGFloat p = Pressure( var);
		return 1/FLOW_K * log( p/pow( var( ETEuler::ID_RHO), FLOW_K ) );
	}

	template <class VECTOR> static MGFloat C( const VECTOR& var)
	{
		MGFloat p = Pressure( var);
		return ::sqrt( FLOW_K*p/var( ETEuler::ID_RHO) );
	}

	template <class VECTOR> static bool IsPhysical( const VECTOR& var)
	{
		if ( Ro( var) < 0 )
		{
			printf( "Negative density\n");
			return false;
		}

		if ( Pressure( var) < 0 )
		{
			printf( "Negative pressure\n");
			return false;
		}

		return true;
	}
};

template <class MATRIX>
inline void Eigen( MATRIX& mtxL, MATRIX& mtxLI, MATRIX& mtxD, const MATRIX& mtxA)
{
	//ASSERT( mtxA.NRows() == 2 && mtxA.NCols() == 2 );

	
	if( ::fabs( mtxA(0,1) - mtxA(1,0)) > ZERO )
	{
		mtxA.Write();
		THROW_INTERNAL( "Eigen matrix A is NOT symetric");
	}
	
	MATRIX	tmpA = mtxA;

	//mtxL.Resize( 2, 2);
	//mtxL.Init( 0);
	//mtxLI.Resize( 2, 2);
	//mtxLI.Init( 0);
	//mtxD.Resize( 2, 2);
	//mtxD.Init( 0);

	tmpA.Decompose( mtxD, mtxLI);
	mtxL = mtxLI;
	mtxL.Invert();
}


template <class VECTOR, class MATRIX>
inline void Eigen_( MATRIX& mtxL, MATRIX& mtxLI, VECTOR& vecD, const MATRIX& mtxA)
{
	//ASSERT( mtxA.NRows() == 2 && mtxA.NCols() == 2 );

	
	if( ::fabs( mtxA(0,1) - mtxA(1,0)) > ZERO )
	{
		mtxA.Write();
		THROW_INTERNAL( "Eigen matrix A is NOT symetric");
	}
	
	MATRIX	tmpA = mtxA;

	//mtxL.Resize( 2, 2);
	//mtxL.Init( 0);
	//mtxLI.Resize( 2, 2);
	//mtxLI.Init( 0);
	//mtxD.Resize( 2, 2);
	//mtxD.Init( 0);

	tmpA.Decompose( vecD, mtxLI);
	mtxL = mtxLI;
	mtxL.Invert();
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#include "eulerequations1d.h"
#include "eulerequations2d.h"
#include "eulerequations3d.h"


#endif // __EQNEULER_H__
