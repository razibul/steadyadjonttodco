#ifndef __EQNEULER2D_H__
#define __EQNEULER2D_H__



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <class VECTOR> 
inline MGFloat EqnEuler<Geom::DIM_2D>::SqrU( const VECTOR& var)
{
	return	var( ETEuler::U<0>::ID) * var( ETEuler::U<0>::ID) + 
			var( ETEuler::U<1>::ID) * var( ETEuler::U<1>::ID);
}

// Conversion of the variables	
template <class VECTOR> 
inline VECTOR EqnEuler<Geom::DIM_2D>::SimpToConserv( const VECTOR& vsmp)
{
	VECTOR vcns;
	MGFloat u2 = SqrU( vsmp);

	vcns( ETEuler::ID_RHO)   = vsmp( ETEuler::ID_RHO);
	vcns( ETEuler::ID_P)     = vsmp( ETEuler::ID_P) / (FLOW_K-1) + 0.5 * vsmp( ETEuler::ID_RHO) * u2;
	vcns( ETEuler::U<0>::ID) = vsmp( ETEuler::ID_RHO) * vsmp( ETEuler::U<0>::ID);
	vcns( ETEuler::U<1>::ID) = vsmp( ETEuler::ID_RHO) * vsmp( ETEuler::U<1>::ID);

	return vcns;
}



template <class VECTOR>	
inline VECTOR EqnEuler<Geom::DIM_2D>::ConservToSimp( const VECTOR& vcns)
{
	VECTOR vsmp;
	MGFloat u2 = SqrU( vcns);

	vsmp( ETEuler::ID_RHO)     = vcns(  ETEuler::ID_RHO);
	vsmp( ETEuler::ID_P)     = (FLOW_K - 1)*(vcns( ETEuler::ID_P) - 0.5*u2/vcns(  ETEuler::ID_RHO) );
	vsmp( ETEuler::U<0>::ID) = vcns( ETEuler::U<0>::ID) / vcns(  ETEuler::ID_RHO);
	vsmp( ETEuler::U<1>::ID) = vcns( ETEuler::U<1>::ID) / vcns(  ETEuler::ID_RHO);

	return vsmp;
}


template <class VECTOR>	
inline VECTOR EqnEuler<Geom::DIM_2D>::ConservToZ( const VECTOR& vcns)
{
	VECTOR vz;
	MGFloat u2 = SqrU( vcns);

	vz( ETEuler::ID_RHO)     = ::sqrt( vcns( ETEuler::ID_RHO) );
	vz( ETEuler::ID_P)     = ( FLOW_K*vcns( ETEuler::ID_P) - (FLOW_K-1)*0.5*u2/vcns( ETEuler::ID_RHO) ) / vz( ETEuler::ID_RHO);
	vz( ETEuler::U<0>::ID) = vcns( ETEuler::U<0>::ID)/vz( ETEuler::ID_RHO);
	vz( ETEuler::U<1>::ID) = vcns( ETEuler::U<1>::ID)/vz( ETEuler::ID_RHO);

	return vz;
}


template <class VECTOR>	
inline VECTOR EqnEuler<Geom::DIM_2D>::ZToConserv( const VECTOR& vz)
{
	VECTOR vcns;
	MGFloat u2 = SqrU( vz);

	vcns( ETEuler::ID_RHO)     = vz( ETEuler::ID_RHO)*vz( ETEuler::ID_RHO);
	vcns( ETEuler::ID_P)     = ( vz( ETEuler::ID_RHO)*vz( ETEuler::ID_P) + (FLOW_K-1)*0.5*u2 )/FLOW_K;
	vcns( ETEuler::U<0>::ID) = vz( ETEuler::U<0>::ID)*vz( ETEuler::ID_RHO);
	vcns( ETEuler::U<1>::ID) = vz( ETEuler::U<1>::ID)*vz( ETEuler::ID_RHO);

	return vcns;
}


template <class VECTOR>	
inline VECTOR EqnEuler<Geom::DIM_2D>::SimpToZ( const VECTOR& vsmp)
{
	VECTOR vz;
	MGFloat u2 = SqrU( vsmp);

	vz( ETEuler::ID_RHO)     = ::sqrt( vsmp( ETEuler::ID_RHO));
	vz( ETEuler::ID_P)     = vz( ETEuler::ID_RHO)*( FLOW_K/(FLOW_K-1) * vsmp( ETEuler::ID_P)/vsmp( ETEuler::ID_RHO) + 0.5*u2 );
	vz( ETEuler::U<0>::ID) = vsmp( ETEuler::U<0>::ID)*vz( ETEuler::ID_RHO);
	vz( ETEuler::U<1>::ID) = vsmp( ETEuler::U<1>::ID)*vz( ETEuler::ID_RHO);

	return vz;
}


template <class VECTOR>	
inline VECTOR EqnEuler<Geom::DIM_2D>::ZToSimp( const VECTOR& vz)
{
	VECTOR vsmp;
	MGFloat u2 = SqrU( vz);
	
	vsmp( ETEuler::ID_RHO)     = vz( ETEuler::ID_RHO)*vz( ETEuler::ID_RHO);
	vsmp( ETEuler::ID_P)     = (FLOW_K-1)/FLOW_K * ( vz( ETEuler::ID_RHO)*vz( ETEuler::ID_P) - 0.5*u2 );
	vsmp( ETEuler::U<0>::ID) = vz( ETEuler::U<0>::ID)/vz( ETEuler::ID_RHO);
	vsmp( ETEuler::U<1>::ID) = vz( ETEuler::U<1>::ID)/vz( ETEuler::ID_RHO);

	return vsmp;
}

// Jacobian matrices in primitive variables
template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::JacobAV( MATRIX& mtx, const VECTOR& vsmp)
{
	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0.0);

	mtx(0,0) = vsmp(2);
	mtx(1,1) = vsmp(2);
	mtx(2,2) = vsmp(2);
	mtx(3,3) = vsmp(2);

	mtx(0,2) = vsmp(0);
	mtx(1,2) = FLOW_K*vsmp(1);
	mtx(2,1) = 1.0/vsmp(0);
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::JacobBV( MATRIX& mtx, const VECTOR& vsmp)
{
	mtx.Resize( ETEuler::SIZE, ETEuler::SIZE);
	mtx.Init( 0.0);

	mtx(0,0) = vsmp(3);
	mtx(1,1) = vsmp(3);
	mtx(2,2) = vsmp(3);
	mtx(3,3) = vsmp(3);

	mtx(0,3) = vsmp(0);
	mtx(1,3) = FLOW_K*vsmp(1);
	mtx(3,1) = 1.0/vsmp(0);
}


////

template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::DecompL( MATRIX& mtx, const VECTOR& vs, const Geom::Vect2D& vn)
{
	MGFloat c = ::sqrt( FLOW_K*vs(1)/vs(0) );

	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(0,0) = 1;
	mtx(0,1) = -1.0/(c*c);
	mtx(1,2) = vn.cY();
	mtx(1,3) = -vn.cX();
	mtx(3,1) = mtx(2,1) = 1.0/(vs(0)*c);
	mtx(2,2) = vn.cX();
	mtx(2,3) = vn.cY();
	mtx(3,2) = -vn.cX();
	mtx(3,3) = -vn.cY();
}

template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::DecompLI( MATRIX& mtx, const VECTOR& vs, const Geom::Vect2D& vn)
{
	MGFloat c = ::sqrt( FLOW_K*vs(1)/vs(0) );

	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(0,0) = 1;
	mtx(0,2) = mtx(0,3) = vs(0)*0.5/c;

	mtx(1,2) = mtx(1,3) = 0.5*vs(0)*c;

	mtx(2,1) = vn.cY();
	mtx(2,2) = 0.5*vn.cX();
	mtx(2,3) = -0.5*vn.cX();

	mtx(3,1) = -vn.cX();
	mtx(3,2) = 0.5*vn.cY();
	mtx(3,3) = -0.5*vn.cY();
}


// Conversion Jacobian matrices
template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::CJacobZV( MATRIX& mtx, const VECTOR& vz)
{
	MGFloat u2 = vz(2)*vz(2) + vz(3)*vz(3);

	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(2,2) = vz(0);
	mtx(1,2) = vz(2);
	mtx(2,0) = 0.5*vz(2)/(vz(0)*vz(0));

	mtx(3,3) = vz(0);
	mtx(1,3) = vz(3);
	mtx(3,0) = 0.5*vz(3)/(vz(0)*vz(0));

	mtx(0,0) = 0.5/vz(0);
	mtx(1,0) = 0.5*( u2/(vz(0)*vz(0)*vz(0)) - vz(1)/(vz(0)*vz(0)) );
	mtx(1,1) = FLOW_K/((FLOW_K-1)*vz(0));
}

template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::CJacobVZ( MATRIX& mtx, const VECTOR& vz)
{
	MGFloat	z1_inv = 1.0/vz(0);
	MGFloat frac_k = (FLOW_K - 1)/FLOW_K;

	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(2,2) = z1_inv;
	mtx(1,2) = -frac_k*vz(2);
	mtx(2,0) = -vz(2)/(vz(0)*vz(0));

	mtx(3,3) = z1_inv;
	mtx(1,3) = -frac_k*vz(3);
	mtx(3,0) = -vz(3)/(vz(0)*vz(0));

	mtx(0,0) = 2*vz(0);
	mtx(1,0) = frac_k*vz(1);
	mtx(1,1) = frac_k*vz(0);
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::CJacobZQ( MATRIX& mtx, const VECTOR& vz)
{
	MGFloat	z1_inv = 1.0/vz(0);
	MGFloat u2 = vz(2)*vz(2) + vz(3)*vz(3);

	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(2,2) = z1_inv;
	mtx(1,2) = -(FLOW_K - 1)*vz(2)/(vz(0)*vz(0));
	mtx(2,0) = -0.5*vz(2)/(vz(0)*vz(0));

	mtx(3,3) = z1_inv;
	mtx(1,3) = -(FLOW_K - 1)*vz(3)/(vz(0)*vz(0));
	mtx(3,0) = -0.5*vz(3)/(vz(0)*vz(0));

	mtx(0,0) = 0.5*z1_inv;
	mtx(1,0) = 0.5*( (FLOW_K-1)*u2/(vz(0)*vz(0)*vz(0)) - vz(1)/(vz(0)*vz(0)) );
	mtx(1,1) = FLOW_K*z1_inv;
}


//template<>
template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::CJacobQZ( MATRIX& mtx, const VECTOR& vz)
{
	MGFloat z1_inv = 1.0/vz(0);
	MGFloat frac_k = (FLOW_K - 1)/FLOW_K;

	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(2,2) = vz(0);
	mtx(1,2) = frac_k*vz(2);
	mtx(2,0) = vz(2);

	mtx(3,3) = vz(0);
	mtx(1,3) = frac_k*vz(3);
	mtx(3,0) = vz(3);

	mtx(0,0) = 2*vz(0);
	mtx(1,0) = vz(1)/FLOW_K;
	mtx(1,1) = vz(0)/FLOW_K;
}

template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::CJacobQV( MATRIX& mtx, const VECTOR& vs)
{
	MGFloat	U2 = SqrU(vs);

	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(0,0) = 1.;

	mtx(1,0) = 0.5 * U2;
	mtx(1,1) = 1.0 / (FLOW_K - 1.);
	mtx(1,2) = vs(0)*vs(2);
	mtx(1,3) = vs(0)*vs(3);

	mtx(2,0) = vs(2);
	mtx(3,0) = vs(3);

	mtx(2,2) = vs(0);
	mtx(3,3) = vs(0);
}

template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::CJacobVQ( MATRIX& mtx, const VECTOR& vs)
{
	MGFloat	U2 = SqrU(vs);

	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(0,0) = 1.;

	mtx(1,0) = 0.5 * U2 * (FLOW_K - 1.);
	mtx(1,1) = (FLOW_K - 1.);
	mtx(1,2) = -vs(2)*(FLOW_K - 1.);
	mtx(1,3) = -vs(3)*(FLOW_K - 1.);;

	mtx(2,0) = -vs(2)/vs(0);
	mtx(3,0) = -vs(3)/vs(0);

	mtx(2,2) = mtx(3,3) = 1./vs(0);
}



template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::CJacobWV( MATRIX& mtx, const VECTOR& v)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat kx = q<ZERO ? 1.0 : v(2)/q;	// <----------
	MGFloat ky = q<ZERO ? 0.0 : v(3)/q; 

	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(0,0) = -c*c;
	mtx(0,1) = 1;
	
	mtx(1,1) = 1/v(0)/c;

	mtx(2,2) = kx;
	mtx(2,3) = ky;
	mtx(3,2) = -ky;
	mtx(3,3) = kx;
}



template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::CJacobVW( MATRIX& mtx, const VECTOR& v)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat kx = q<ZERO ? 1.0 : v(2)/q;	// <----------
	MGFloat ky = q<ZERO ? 0.0 : v(3)/q; 

	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(0,0) = -1.0/(c*c);
	mtx(0,1) = v(0)/c;
	
	mtx(1,1) = v(0)*c;

	mtx(2,2) = kx;
	mtx(2,3) = -ky;
	mtx(3,2) = ky;
	mtx(3,3) = kx;
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::CJacobXW( MATRIX& mtx, const VECTOR& v, const MGFloat& beta)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat M = q/c;
	M = M<MACH_ZERO ? MACH_ZERO : M;

	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(0,0) = 1;

	mtx(1,1) = v(0)*c;
	mtx(1,2) = v(0)*q;
	
	mtx(2,1) = beta/M;
	mtx(2,3) = 1;

	mtx(3,1) = beta/M;
	mtx(3,3) = -1;
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::CJacobWX( MATRIX& mtx, const VECTOR& v, const MGFloat& beta)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat M = q/c; 
	M = M<MACH_ZERO ? MACH_ZERO : M;

	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(0,0) = 1;

	mtx(1,2) = 0.5*M/beta;
	mtx(1,3) = 0.5*M/beta;
	
	mtx(2,1) = 1.0/(v(0)*q);
	mtx(2,2) = -0.5/beta;
	mtx(2,3) = -0.5/beta;

	mtx(3,2) = 0.5;
	mtx(3,3) = -0.5;
}





template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::PrecLLR( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat M = q/c; 
	M = M<MACH_ZERO ? MACH_ZERO : M;

	//MGFloat beta = sqrt( fabs( 1. - M*M) );
	//MGFloat chi = min( beta, beta/M);

	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(0,0) = 1.0;
	
	mtx(1,1) = chi*M*M/(beta*beta);
	mtx(1,2) = -chi*M/(beta*beta);

	mtx(2,1) = -chi*M/(beta*beta);
	mtx(2,2) = 1.0 + chi/(beta*beta);

	mtx(3,3) = chi;
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::PrecInvLLR( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat M = q/c; 
	M = M<MACH_ZERO ? MACH_ZERO : M;

	//MGFloat beta = sqrt( fabs( 1. - M*M) );
	//MGFloat chi = min( beta, beta/M);

	mtx.Resize( 4, 4);
	mtx.Init( 0);

	mtx(0,0) = 1.0;
	
	mtx(1,1) = (beta*beta + chi)/(chi*M*M);
	mtx(1,2) = 1.0/M;

	mtx(2,1) = 1.0/M;
	mtx(2,2) = 1.0;

	mtx(3,3) = 1.0/chi;
}

template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::RotMtx( MATRIX& mtx, const VECTOR& v)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat kx = q<ZERO ? 1.0 : v(2)/q;	// <----------
	MGFloat ky = q<ZERO ? 0.0 : v(3)/q; 

	mtx.Resize( 2, 2);
	mtx.Init( 0);
	mtx(0,0) = mtx(1,1) = kx;
	mtx(0,1) = ky;
	mtx(1,0) = -ky;
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::SubPAXn( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat M = q/c; 
	MGFloat	beta2 = beta*beta;
	MGFloat	nup = (M*M - 1 + beta2)/(2*beta2);
	MGFloat	num = (M*M - 1 - beta2)/(2*beta2);

	mtx.Resize( 2, 2);
	mtx.Init( 0);

	mtx(0,0) = mtx(1,1) = q*chi*nup;
	mtx(0,1) = mtx(1,0) = q*chi*num;
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::SubPBXn( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );

	mtx.Resize( 2, 2);
	mtx.Init( 0);

	mtx(0,0) =   q*chi / beta;
	mtx(1,1) = - q*chi / beta;
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::SubPAX( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat M = q/c; 

	mtx.Resize( 2, 2);
	mtx.Init( 0);

	mtx(0,0) = 0.5*( (M*M-1)/(beta*beta) + 1)*chi*v(2) - chi*v(3)/beta;
	mtx(1,0) = mtx(0,1) = 0.5*( (M*M-1)/(beta*beta) - 1)*chi*v(2);

	//mtx(1,0) = 0.5*( (M*M-1)/(beta*beta) - 1)*chi*v(2);
	mtx(1,1) = 0.5*( (M*M-1)/(beta*beta) + 1)*chi*v(2) + chi*v(3)/beta;
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::SubPBX( MATRIX& mtx, const VECTOR& v, const MGFloat& beta, const MGFloat& chi)
{
	MGFloat	q = ::sqrt( v(2)*v(2) + v(3)*v(3) );
	MGFloat c = ::sqrt( FLOW_K *v(1)/v(0) );
	MGFloat M = q/c; 

	mtx.Resize( 2, 2);
	mtx.Init( 0);

	mtx(0,0) = 0.5*( (M*M-1)/(beta*beta) + 1)*chi*v(3) + chi*v(2)/beta;
	mtx(1,0) = mtx(0,1) = 0.5*( (M*M-1)/(beta*beta) - 1)*chi*v(3);

	//mtx(1,0) = 0.5*( (M*M-1)/(beta*beta) - 1)*chi*v(3);
	mtx(1,1) = 0.5*( (M*M-1)/(beta*beta) + 1)*chi*v(3) - chi*v(2)/beta;
}


template <class MATRIX, class VECTOR>
inline void EqnEuler<Geom::DIM_2D>::SubPAXni( const MGSize& i, MATRIX& mtx, const VECTOR& v, 
								   const MGFloat& beta, const MGFloat& chi)
{
	switch (i)
	{
	case 0:
		SubPAXn( mtx, v, beta, chi);
		return;
	case 1:
		SubPBXn( mtx, v, beta, chi);
		return;
	default:
		THROW_INTERNAL( "Bad index in SubPAXni");
	}
}



////////////////////////////////////////////



template <class MATRIX>
inline void _Eigen2x2( MATRIX& mtxL, MATRIX& mtxLI, MATRIX& mtxD, const MATRIX& mtxA)
{
	ASSERT( mtxA.NRows() == 2 && mtxA.NCols() == 2 );

	MGFloat	da = mtxA(1,1) - mtxA(0,0);
	MGFloat	root = ::sqrt( da*da + 4.0*mtxA(0,1)*mtxA(1,0) );

	mtxL.Resize( 2, 2);
	mtxL.Init( 0);
	mtxLI.Resize( 2, 2);
	mtxLI.Init( 0);
	mtxD.Resize( 2, 2);
	mtxD.Init( 0);

	if ( ::fabs( mtxA(1,0)) > ZERO)
	{
		mtxD(0,0) = 0.5*( mtxA(1,1) + mtxA(0,0) + root);
		mtxD(1,1) = 0.5*( mtxA(1,1) + mtxA(0,0) - root);

		mtxLI(0,0) = -0.5*( da - root )/mtxA(1,0);
		mtxLI(0,1) = -0.5*( da + root )/mtxA(1,0);
		mtxLI(1,0) = 1;
		mtxLI(1,1) = 1;

		mtxL(0,0) = mtxA(1,0);
		mtxL(0,1) = 0.5*( da + root );
		mtxL(1,0) = - mtxA(1,0);
		mtxL(1,1) = -0.5*( da - root );
		mtxL = mtxL * (1.0/root);
	}
	else
	{
		if ( ::fabs( mtxA(0,1) - mtxA(1,0)) > ZERO)
			THROW_INTERNAL( "Eigen problem 2x2 error");

		mtxD(0,0) = mtxA(0,0);
		mtxD(1,1) = mtxA(1,1);

		mtxLI(0,0) = 1.0;
		mtxLI(1,1) = 1.0;

		mtxL(0,0) = 1.0;
		mtxL(1,1) = 1.0;
	}

}


template <class MATRIX>
inline void Eigen2x2( MATRIX& mtxL, MATRIX& mtxLI, MATRIX& mtxD, const MATRIX& mtxA)
{
	ASSERT( mtxA.NRows() == 2 && mtxA.NCols() == 2 );

	
	if( ::fabs( mtxA(0,1) - mtxA(1,0)) > ZERO )
		THROW_INTERNAL( "Eigen2x2 matrix A is NOT symetric");
	
	MATRIX	tmpA = mtxA;

	//mtxL.Resize( 2, 2);
	//mtxL.Init( 0);
	//mtxLI.Resize( 2, 2);
	//mtxLI.Init( 0);
	//mtxD.Resize( 2, 2);
	//mtxD.Init( 0);

	tmpA.Decompose( mtxD, mtxLI);
	mtxL = mtxLI;
	mtxL.Invert();
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EQNEULER2D_H__
