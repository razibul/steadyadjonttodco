#ifndef __RANSSAPHYSICS_H__
#define __RANSSAPHYSICS_H__

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Physics for RANS with 1 turbulence equations
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class Physics<DIM,EQN_RANS_SA> : public PhysicsCommon<DIM,EQN_RANS_SA>
{
	enum { ESIZE = PhysicsCommon<DIM,EQN_RANS_SA>::ESIZE };

	typedef typename PhysicsCommon<DIM,EQN_RANS_SA>::EqDef	EqDef;
	typedef typename PhysicsCommon<DIM,EQN_RANS_SA>::FVec	FVec;
	typedef typename PhysicsCommon<DIM,EQN_RANS_SA>::AVec	AVec;
	typedef typename PhysicsCommon<DIM,EQN_RANS_SA>::GVec	GVec;

public:
	Physics()	: PhysicsCommon<DIM,EQN_RANS_SA>()	{}

	virtual void Create( const CfgSection* pcfgsec);
	virtual void PostCreateCheck() const;
	virtual void Init();

	virtual void	DimToUndim( FVec& vout, const FVec& vin, const FVec& vref ) const;
	virtual void	UndimToDim( FVec& vout, const FVec& vin, const FVec& vref ) const;

	void	ApplyBC(FVec& vecout, AVec& avecout, const FVec& vecin, const AVec& avecin, const MGSize& surfid, const GVec& vpos, const GVec& vnn, const MGFloat& dy, const typename PhysicsCommon<DIM,EQN_RANS_SA>::PrdBCBase* pbc, const MGSize& nodeid) const;

	template <class VECTOR>
	bool	IsPhysical( const VECTOR& vec);

	MGFloat				MaxSpeed( const FVec& vec) const;
	const MGFloat&		cRefLength() const					{ return mRefLength;}
	const MGFloat&		cRe() const							{ return mRefRe;}
	const MGFloat&		cPr() const							{ return mRefPr;} 

protected:
	void	InitFVec( FVec& vec, const CfgSection& sec);

private:
	MGFloat		mRefLength;
	MGFloat		mRefArea;
	MGFloat		mRefRe;
	MGFloat		mRefPr;

};
//////////////////////////////////////////////////////////////////////


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_RANS_SA>::Create( const CfgSection* pcfgsec)		
{ 
	PhysicsCommon<DIM,EQN_RANS_SA>::Create( pcfgsec);
}


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_RANS_SA>::PostCreateCheck() const
{ 
	PhysicsCommon<DIM,EQN_RANS_SA>::PostCreateCheck();

	// do check
}


template <Geom::Dimension DIM>
inline MGFloat Physics<DIM,EQN_RANS_SA>::MaxSpeed( const FVec& vec) const
{ 
	return ::sqrt( EqnEuler<DIM>::SqrU( vec) ) + EqnEulerComm<DIM>::C( vec);
}



template <Geom::Dimension DIM>
template <class VECTOR>
bool Physics<DIM,EQN_RANS_SA>::IsPhysical( const VECTOR& vec)
{
	return true;
}


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_RANS_SA>::DimToUndim( FVec& vout, const FVec& vin, const FVec& vref ) const
{
	//vout = vin;
	//return;

	typedef	typename EquationDef<DIM,EQN_RANS_SA>::SplitingFlowTurb::template Block<1> TMapper;
	typedef typename TMapper::FVec	TFlowVec;

	TFlowVec vflowref, vflowin, vflowout;

	TMapper::GlobalToLocal( vflowref, vref);
	TMapper::GlobalToLocal( vflowin, vin);

	vflowout = EqnEulerComm<DIM>::DimToUndim( vflowin, vflowref);

	vout = vin;
	TMapper::LocalToGlobal( vout, vflowout);
	//vout = vin;
}



template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_RANS_SA>::UndimToDim( FVec& vout, const FVec& vin, const FVec& vref ) const
{
	//vout = vin;
	//return;

	typedef	typename EquationDef<DIM,EQN_RANS_SA>::SplitingFlowTurb::template Block<1> TMapper;
	typedef typename TMapper::FVec	TFlowVec;

	TFlowVec vflowref, vflowin, vflowout;

	TMapper::GlobalToLocal( vflowref, vref);
	TMapper::GlobalToLocal( vflowin, vin);

	vflowout = EqnEulerComm<DIM>::UndimToDim( vflowin, vflowref);

	vout = vin;
	TMapper::LocalToGlobal( vout, vflowout);

	//vout = EqnEulerComm<Geom::DIM_2D>::UndimToDim( vin, vref);
	//vout = vin;
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RANSSAPHYSICS_H__



