#ifndef __EQNRANSKOMEGA_H__
#define __EQNRANSKOMEGA_H__

#include "libredphysics/eulerconst.h"
#include "libredcore/equation.h"
#include "libcoregeom/vect.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class EqnRansKOMEGA
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class EqnRansKOMEGA
{
public:
	typedef EquationDef<DIM,EQN_RANS_KOMEGA>	ET;
	typedef Geom::Vect<DIM>						GVect;



	template <class VECTOR> static MGFloat SqrU( const VECTOR& var);


	template <class VECTOR> static VECTOR DimToUndim( const VECTOR& vin, const VECTOR& vref);
	template <class VECTOR> static VECTOR UndimToDim( const VECTOR& vin, const VECTOR& vref);

	// Conversion of the variables	
	template <class VECTOR> static VECTOR SimpToConserv( const VECTOR& vsmp);
	template <class VECTOR>	static VECTOR ConservToSimp( const VECTOR& vcns);

	template <class VECTOR>	static VECTOR ConservToZ( const VECTOR& vcns);
	template <class VECTOR>	static VECTOR ZToConserv( const VECTOR& vz);

	template <class VECTOR>	static VECTOR SimpToZ( const VECTOR& vsmp);
	template <class VECTOR>	static VECTOR ZToSimp( const VECTOR& vz);


	// Assemble Jacobian matrices
	template <class MATRIX>				  static void	AssembleSubMtxSAW( MATRIX &mtx, const GVect &vn, const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 );
	template <class MATRIX, class VECTOR> static void	AssembleSubMtxSAV( MATRIX &mtx, const VECTOR& var, const GVect &vn, const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 );

	// Conversion Jacobian matrices
	template <class MATRIX, class VECTOR> static void CJacobQW( MATRIX& mtx, const VECTOR& vsmp);
	template <class MATRIX, class VECTOR> static void CJacobWZ( MATRIX& mtx, const VECTOR& vsmp);
	template <class MATRIX, class VECTOR> static void CJacobQV( MATRIX& mtx, const VECTOR& vsmp);
	template <class MATRIX, class VECTOR> static void CJacobVZ( MATRIX& mtx, const VECTOR& vsmp);

	template <class MATRIX, class VECTOR> static void MultQW( VECTOR& vout, const MATRIX& mtxWZ, const VECTOR& vin);
	template <class MATRIX, class VECTOR> static void MultWZ( VECTOR& vout, const MATRIX& mtxWZ, const VECTOR& vin);


	template <class VECTOR> static MGFloat Pressure( const VECTOR& vq)
	{
		return (FLOW_K - 1)*( vq( ET::ID_P) - 0.5*SqrU(vq)/vq( ET::ID_RHO)/* - vq( ET::ID_K)*/ );
	}

	template <class VECTOR> static MGFloat C2( const VECTOR& vq)
	{
		MGFloat p = Pressure( vq);
		return FLOW_K * p / vq( ET::ID_RHO);
	}

	template <class VECTOR> static MGFloat C( const VECTOR& vq)
	{
		return ::sqrt( C2( vq) );
	}

};
//////////////////////////////////////////////////////////////////////

template <Geom::Dimension DIM>
template <class VECTOR> 
inline VECTOR EqnRansKOMEGA<DIM>::DimToUndim( const VECTOR& vin, const VECTOR& vref)
{
	//THROW_INTERNAL("not implemented");
	VECTOR vout;
	MGFloat u2 = SqrU( vref);
	MGFloat u = ::sqrt( u2);

	vout( ET::ID_RHO)	= vin( ET::ID_RHO) / vref( ET::ID_RHO);
	vout( ET::ID_P)	= vin( ET::ID_P) * vref( ET::ID_RHO) / u2;
	for ( MGSize i=ET::template U<0>::ID; i<ET::template U<0>::ID + DIM; ++i)
		vout( i) = vin(i) / u;

	vout( ET::ID_K)	= vin( ET::ID_K) * vref( ET::ID_RHO) / u2;
	
	//vout( ET::ID_OMEGA)	= vin( ET::ID_OMEGA) / vref( ET::ID_RHO);
	//vout( ET::ID_OMEGA)	= ( vin( ET::ID_OMEGA) /*- log(u)*/) / vref( ET::ID_RHO);

	vout( ET::ID_OMEGA)	= vin( ET::ID_OMEGA);

	return vout;
}


template <Geom::Dimension DIM>
template <class VECTOR> 
inline VECTOR EqnRansKOMEGA<DIM>::UndimToDim( const VECTOR& vin, const VECTOR& vref)
{
	//THROW_INTERNAL("not implemented");
	VECTOR vout;
	MGFloat u2 = SqrU( vref);
	MGFloat u = ::sqrt( u2);

	vout( ET::ID_RHO)	= vin( ET::ID_RHO) * vref( ET::ID_RHO);
	vout( ET::ID_P)	= vin( ET::ID_P) * u2 / vref( ET::ID_RHO);
	for ( MGSize i=ET::template U<0>::ID; i<ET::template U<0>::ID + DIM; ++i)
		vout( i) = vin(i) * u;

	vout( ET::ID_K)	= vin( ET::ID_K) * u2 / vref( ET::ID_RHO);

	//vout( ET::ID_OMEGA)	= vin( ET::ID_OMEGA) * vref( ET::ID_RHO);
	//vout( ET::ID_OMEGA)	= vin( ET::ID_OMEGA) * vref( ET::ID_RHO) /*+ log(u)*/ ;

	vout( ET::ID_OMEGA)	= vin( ET::ID_OMEGA);

	return vout;
}


//////////////////////////////////////////////////////////////////////

template <> 
template <class VECTOR> 
inline MGFloat EqnRansKOMEGA<Geom::DIM_2D>::SqrU( const VECTOR& var)
{
	return	var( ET::U<0>::ID) * var( ET::U<0>::ID) + 
			var( ET::U<1>::ID) * var( ET::U<1>::ID);
}

template <> 
template <class VECTOR> 
inline MGFloat EqnRansKOMEGA<Geom::DIM_3D>::SqrU( const VECTOR& var)
{
	return	var( ET::U<0>::ID) * var( ET::U<0>::ID) + 
			var( ET::U<1>::ID) * var( ET::U<1>::ID) +
			var( ET::U<2>::ID) * var( ET::U<2>::ID);
}


template <> 
template <class VECTOR> 
inline VECTOR EqnRansKOMEGA<Geom::DIM_2D>::SimpToConserv( const VECTOR& vsmp)
{
	VECTOR vcns;
	MGFloat u2 = SqrU( vsmp);

	vcns( ET::ID_RHO)   = vsmp( ET::ID_RHO);
	vcns( ET::ID_P)     = vsmp( ET::ID_P) / (FLOW_K-1) + 0.5 * vsmp( ET::ID_RHO) * u2 /*+ vsmp( ET::ID_RHO) * vsmp( ET::ID_K)*/;
	vcns( ET::U<0>::ID) = vsmp( ET::ID_RHO) * vsmp( ET::U<0>::ID);
	vcns( ET::U<1>::ID) = vsmp( ET::ID_RHO) * vsmp( ET::U<1>::ID);
	vcns( ET::ID_K)		= vsmp( ET::ID_RHO) * vsmp( ET::ID_K);
	vcns( ET::ID_OMEGA) = vsmp( ET::ID_RHO) * vsmp( ET::ID_OMEGA);

	return vcns;
}

template <> 
template <class VECTOR> 
inline VECTOR EqnRansKOMEGA<Geom::DIM_3D>::SimpToConserv( const VECTOR& vsmp)
{
	VECTOR vcns;
	MGFloat u2 = SqrU( vsmp);

	vcns( ET::ID_RHO)   = vsmp( ET::ID_RHO);
	vcns( ET::ID_P)     = vsmp( ET::ID_P) / (FLOW_K-1) + 0.5 * vsmp( ET::ID_RHO) * u2 + vsmp( ET::ID_RHO) * vsmp( ET::ID_K);
	vcns( ET::U<0>::ID) = vsmp( ET::ID_RHO) * vsmp( ET::U<0>::ID);
	vcns( ET::U<1>::ID) = vsmp( ET::ID_RHO) * vsmp( ET::U<1>::ID);
	vcns( ET::U<2>::ID) = vsmp( ET::ID_RHO) * vsmp( ET::U<2>::ID);
	vcns( ET::ID_K)		= vsmp( ET::ID_RHO) * vsmp( ET::ID_K);
	vcns( ET::ID_OMEGA) = vsmp( ET::ID_RHO) * vsmp( ET::ID_OMEGA);

	return vcns;
}


template <> 
template <class VECTOR> 
inline VECTOR EqnRansKOMEGA<Geom::DIM_2D>::ConservToSimp( const VECTOR& vcns)
{
	VECTOR vsmp;
	MGFloat u2 = SqrU( vcns);

	vsmp( ET::ID_RHO)   = vcns( ET::ID_RHO);
	vsmp( ET::ID_P)     = (FLOW_K - 1)*( vcns( ET::ID_P) - 0.5*u2/vcns( ET::ID_RHO) /*- vcns( ET::ID_K)*/ );
	vsmp( ET::U<0>::ID) = vcns( ET::U<0>::ID) / vcns( ET::ID_RHO);
	vsmp( ET::U<1>::ID) = vcns( ET::U<1>::ID) / vcns( ET::ID_RHO);
	vsmp( ET::ID_K)     = vcns( ET::ID_K)     / vcns( ET::ID_RHO);
	vsmp( ET::ID_OMEGA) = vcns( ET::ID_OMEGA) / vcns( ET::ID_RHO);

	return vsmp;
}

template <> 
template <class VECTOR> 
inline VECTOR EqnRansKOMEGA<Geom::DIM_3D>::ConservToSimp( const VECTOR& vcns)
{
	VECTOR vsmp;
	MGFloat u2 = SqrU( vcns);

	vsmp( ET::ID_RHO)   = vcns( ET::ID_RHO);
	vsmp( ET::ID_P)     = (FLOW_K - 1)*( vcns( ET::ID_P) - 0.5*u2/vcns( ET::ID_RHO) - vcns( ET::ID_K) );
	vsmp( ET::U<0>::ID) = vcns( ET::U<0>::ID) / vcns( ET::ID_RHO);
	vsmp( ET::U<1>::ID) = vcns( ET::U<1>::ID) / vcns( ET::ID_RHO);
	vsmp( ET::U<2>::ID) = vcns( ET::U<2>::ID) / vcns( ET::ID_RHO);
	vsmp( ET::ID_K)     = vcns( ET::ID_K)     / vcns( ET::ID_RHO);
	vsmp( ET::ID_OMEGA) = vcns( ET::ID_OMEGA) / vcns( ET::ID_RHO);

	return vsmp;
}


template <> 
template <class VECTOR> 
inline VECTOR EqnRansKOMEGA<Geom::DIM_2D>::ConservToZ( const VECTOR& vcns)
{
	VECTOR vz;
	MGFloat u2 = SqrU( vcns);

	vz( ET::ID_RHO)   = ::sqrt( vcns( ET::ID_RHO) );
	vz( ET::ID_P)     = ( FLOW_K*vcns( ET::ID_P) - (FLOW_K-1)*( 0.5*u2/vcns( ET::ID_RHO) /*+ vcns( ET::ID_K)*/ ) ) / vz( ET::ID_RHO);
	vz( ET::U<0>::ID) = vcns( ET::U<0>::ID) / vz( ET::ID_RHO);
	vz( ET::U<1>::ID) = vcns( ET::U<1>::ID) / vz( ET::ID_RHO);
	vz( ET::ID_K)     = vcns( ET::ID_K)     / vz( ET::ID_RHO);
	vz( ET::ID_OMEGA) = vcns( ET::ID_OMEGA) / vz( ET::ID_RHO);

	return vz;
}


template <> 
template <class VECTOR> 
inline VECTOR EqnRansKOMEGA<Geom::DIM_3D>::ConservToZ( const VECTOR& vcns)
{
	VECTOR vz;
	MGFloat u2 = SqrU( vcns);

	vz( ET::ID_RHO)   = ::sqrt( vcns( ET::ID_RHO) );
	vz( ET::ID_P)     = ( FLOW_K*vcns( ET::ID_P) - (FLOW_K-1)*( 0.5*u2/vcns( ET::ID_RHO) + vcns( ET::ID_K) ) ) / vz( ET::ID_RHO);
	vz( ET::U<0>::ID) = vcns( ET::U<0>::ID) / vz( ET::ID_RHO);
	vz( ET::U<1>::ID) = vcns( ET::U<1>::ID) / vz( ET::ID_RHO);
	vz( ET::U<2>::ID) = vcns( ET::U<2>::ID) / vz( ET::ID_RHO);
	vz( ET::ID_K)     = vcns( ET::ID_K)     / vz( ET::ID_RHO);
	vz( ET::ID_OMEGA) = vcns( ET::ID_OMEGA) / vz( ET::ID_RHO);

	return vz;
}


template <> 
template <class VECTOR> 
inline VECTOR EqnRansKOMEGA<Geom::DIM_2D>::ZToConserv( const VECTOR& vz)
{
	VECTOR vcns;
	MGFloat u2 = SqrU( vz);

	vcns( ET::ID_RHO)   = vz( ET::ID_RHO) * vz( ET::ID_RHO);
	vcns( ET::ID_P)     = ( vz( ET::ID_RHO)*vz( ET::ID_P) + (FLOW_K-1)*( 0.5*u2 /*+ vz( ET::ID_RHO)*vz( ET::ID_K)*/ ) )/FLOW_K;
	vcns( ET::U<0>::ID) = vz( ET::U<0>::ID) * vz( ET::ID_RHO);
	vcns( ET::U<1>::ID) = vz( ET::U<1>::ID) * vz( ET::ID_RHO);
	vcns( ET::ID_K)     = vz( ET::ID_K)     * vz( ET::ID_RHO);
	vcns( ET::ID_OMEGA) = vz( ET::ID_OMEGA) * vz( ET::ID_RHO);

	return vcns;
}

template <> 
template <class VECTOR> 
inline VECTOR EqnRansKOMEGA<Geom::DIM_3D>::ZToConserv( const VECTOR& vz)
{
	VECTOR vcns;
	MGFloat u2 = SqrU( vz);

	vcns( ET::ID_RHO)   = vz( ET::ID_RHO) * vz( ET::ID_RHO);
	vcns( ET::ID_P)     = ( vz( ET::ID_RHO)*vz( ET::ID_P) + (FLOW_K-1)*( 0.5*u2 + vz( ET::ID_RHO)*vz( ET::ID_K) ) )/FLOW_K;
	vcns( ET::U<0>::ID) = vz( ET::U<0>::ID) * vz( ET::ID_RHO);
	vcns( ET::U<1>::ID) = vz( ET::U<1>::ID) * vz( ET::ID_RHO);
	vcns( ET::U<2>::ID) = vz( ET::U<2>::ID) * vz( ET::ID_RHO);
	vcns( ET::ID_K)     = vz( ET::ID_K)     * vz( ET::ID_RHO);
	vcns( ET::ID_OMEGA) = vz( ET::ID_OMEGA) * vz( ET::ID_RHO);

	return vcns;
}



template <> 
template <class VECTOR> 
inline VECTOR EqnRansKOMEGA<Geom::DIM_2D>::SimpToZ( const VECTOR& vsmp)
{
	VECTOR vz;
	MGFloat u2 = SqrU( vsmp);

	vz( ET::ID_RHO)   = ::sqrt( vsmp( ET::ID_RHO));
	vz( ET::ID_P)     = vz( ET::ID_RHO) * ( FLOW_K/(FLOW_K-1) * vsmp( ET::ID_P)/vsmp( ET::ID_RHO) /*+ 0.5*u2 + vsmp( ET::ID_K)*/  );
	vz( ET::U<0>::ID) = vsmp( ET::U<0>::ID) * vz( ET::ID_RHO);
	vz( ET::U<1>::ID) = vsmp( ET::U<1>::ID) * vz( ET::ID_RHO);
	vz( ET::ID_K)     = vsmp( ET::ID_K) * vz( ET::ID_RHO);
	vz( ET::ID_OMEGA) = vsmp( ET::ID_OMEGA) * vz( ET::ID_RHO);

	return vz;
}

template <> 
template <class VECTOR> 
inline VECTOR EqnRansKOMEGA<Geom::DIM_3D>::SimpToZ( const VECTOR& vsmp)
{
	VECTOR vz;
	MGFloat u2 = SqrU( vsmp);

	vz( ET::ID_RHO)   = ::sqrt( vsmp( ET::ID_RHO));
	vz( ET::ID_P)     = vz( ET::ID_RHO) * ( FLOW_K/(FLOW_K-1) * vsmp( ET::ID_P)/vsmp( ET::ID_RHO) + 0.5*u2 + vsmp( ET::ID_K)  );
	vz( ET::U<0>::ID) = vsmp( ET::U<0>::ID) * vz( ET::ID_RHO);
	vz( ET::U<1>::ID) = vsmp( ET::U<1>::ID) * vz( ET::ID_RHO);
	vz( ET::U<2>::ID) = vsmp( ET::U<2>::ID) * vz( ET::ID_RHO);
	vz( ET::ID_K)     = vsmp( ET::ID_K) * vz( ET::ID_RHO);
	vz( ET::ID_OMEGA) = vsmp( ET::ID_OMEGA) * vz( ET::ID_RHO);

	return vz;
}


template <> 
template <class VECTOR> 
inline VECTOR EqnRansKOMEGA<Geom::DIM_2D>::ZToSimp( const VECTOR& vz)
{
	VECTOR vsmp;
	MGFloat u2 = SqrU( vz);
	
	vsmp( ET::ID_RHO)   = vz( ET::ID_RHO) * vz( ET::ID_RHO);
	vsmp( ET::ID_P)     = (FLOW_K-1)/FLOW_K * ( vz( ET::ID_RHO)*vz( ET::ID_P) - 0.5*u2 /*- vz( ET::ID_RHO)*vz( ET::ID_K)*/ );
	vsmp( ET::U<0>::ID) = vz( ET::U<0>::ID) / vz( ET::ID_RHO);
	vsmp( ET::U<1>::ID) = vz( ET::U<1>::ID) / vz( ET::ID_RHO);
	vsmp( ET::ID_K)     = vz( ET::ID_K)     / vz( ET::ID_RHO);
	vsmp( ET::ID_OMEGA) = vz( ET::ID_OMEGA) / vz( ET::ID_RHO);

	return vsmp;
}

template <> 
template <class VECTOR> 
inline VECTOR EqnRansKOMEGA<Geom::DIM_3D>::ZToSimp( const VECTOR& vz)
{
	VECTOR vsmp;
	MGFloat u2 = SqrU( vz);
	
	vsmp( ET::ID_RHO)   = vz( ET::ID_RHO) * vz( ET::ID_RHO);
	vsmp( ET::ID_P)     = (FLOW_K-1)/FLOW_K * ( vz( ET::ID_RHO)*vz( ET::ID_P) - 0.5*u2 - vz( ET::ID_RHO)*vz( ET::ID_K) );
	vsmp( ET::U<0>::ID) = vz( ET::U<0>::ID) / vz( ET::ID_RHO);
	vsmp( ET::U<1>::ID) = vz( ET::U<1>::ID) / vz( ET::ID_RHO);
	vsmp( ET::U<2>::ID) = vz( ET::U<2>::ID) / vz( ET::ID_RHO);
	vsmp( ET::ID_K)     = vz( ET::ID_K)     / vz( ET::ID_RHO);
	vsmp( ET::ID_OMEGA) = vz( ET::ID_OMEGA) / vz( ET::ID_RHO);

	return vsmp;
}

//////////////////////////////////////////////////////////////////////

template <> 
template <class MATRIX>
inline void EqnRansKOMEGA<Geom::DIM_2D>::AssembleSubMtxSAW( MATRIX &mtx, const GVect &vn, const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 )
{
	MGFloat xi1 = 0.5*(l2 + l3);
	MGFloat xi2 = 0.5*(l2 - l3);
	MGFloat xi3 = xi1 - l1;

	mtx.Resize( 3, 3);
	//mtx.Init( 0.0);

	mtx(0,0) = xi1;
	mtx(1,1) = xi3 * vn.cX() * vn.cX() + l1 ;
	mtx(2,2) = xi3 * vn.cY() * vn.cY() + l1 ;

	mtx(1,0) = mtx(0,1) = xi2 * vn.cX();
	mtx(2,0) = mtx(0,2) = xi2 * vn.cY();

	mtx(2,1) = mtx(1,2) = xi3 * vn.cX() * vn.cY();
}

template <> 
template <class MATRIX>
inline void EqnRansKOMEGA<Geom::DIM_3D>::AssembleSubMtxSAW( MATRIX &mtx, const GVect &vn, const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 )
{
	MGFloat xi1 = 0.5*(l2 + l3);
	MGFloat xi2 = 0.5*(l2 - l3);
	MGFloat xi3 = xi1 - l1;

	mtx.Resize( 4, 4);
	//mtx.Init( 0.0);

	mtx(0,0) = xi1;
	mtx(1,1) = xi3 * vn.cX() * vn.cX() + l1 ;
	mtx(2,2) = xi3 * vn.cY() * vn.cY() + l1 ;
	mtx(3,3) = xi3 * vn.cZ() * vn.cZ() + l1 ;

	mtx(1,0) = mtx(0,1) = xi2 * vn.cX();
	mtx(2,0) = mtx(0,2) = xi2 * vn.cY();
	mtx(3,0) = mtx(0,3) = xi2 * vn.cZ();

	mtx(2,1) = mtx(1,2) = xi3 * vn.cX() * vn.cY();
	mtx(3,1) = mtx(1,3) = xi3 * vn.cX() * vn.cZ();
	mtx(3,2) = mtx(2,3) = xi3 * vn.cY() * vn.cZ();
}


template <> 
template <class MATRIX, class VECTOR>
void EqnRansKOMEGA<Geom::DIM_2D>::AssembleSubMtxSAV( MATRIX &mtx, const VECTOR& var, const GVect &vn, const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 )
{
	MGFloat l2m3 = l2 - l3;
	MGFloat	l2p3 = l2 + l3;
	MGFloat l2p3m1 = 0.5*l2p3 - l1;
	MGFloat ltmp1 =  0.5*var(ET::ID_RHO)*l2m3;
	MGFloat c2 = FLOW_K * var(ET::ID_P) / var(ET::ID_RHO);
	MGFloat c = ::sqrt( c2);

	mtx.Resize( 4, 4);
	//mtx.Init( 0.0);


	mtx(0,0) = l1;
	mtx(0,1) = 0.5*(l2p3 - 2*l1)/c2;
	mtx(0,2) = vn.cX()* ltmp1 /c;
	mtx(0,3) = vn.cY()* ltmp1 /c;

	mtx(1,0) = 0.0;
	mtx(1,1) = 0.5*l2p3;
	mtx(1,2) = vn.cX()*c* ltmp1;
	mtx(1,3) = vn.cY()*c* ltmp1;

	mtx(2,0) = 0.0;
	mtx(2,1) = 0.5*vn.cX()*l2m3 / ( c*var(ET::ID_RHO) );
	mtx(2,2) = l1*vn.cY()*vn.cY() + 0.5*vn.cX()*vn.cX()*l2p3;
	mtx(2,3) = vn.cX()*vn.cY()*l2p3m1;

	mtx(3,0) = 0.0;
	mtx(3,1) = 0.5*vn.cY()*l2m3 / ( c*var(ET::ID_RHO) );
	mtx(3,2) = vn.cX()*vn.cY()*l2p3m1;
	mtx(3,3) = l1*vn.cX()*vn.cX() + 0.5*vn.cY()*vn.cY()*l2p3;
}

template <> 
template <class MATRIX, class VECTOR>
void EqnRansKOMEGA<Geom::DIM_3D>::AssembleSubMtxSAV( MATRIX &mtx, const VECTOR& var, const GVect &vn, const MGFloat &l1, const MGFloat &l2, const MGFloat &l3 )
{
	MGFloat l2m3 = l2 - l3;
	MGFloat	l2p3 = l2 + l3;
	MGFloat l2p3m1 = 0.5*l2p3 - l1;
	MGFloat c2 = FLOW_K * var(ET::ID_P) / var(ET::ID_RHO);
	MGFloat c = ::sqrt( c2);
	MGFloat	romc = c * var(ET::ID_RHO);


	MGFloat ltmp1 =  0.5*var(ET::ID_RHO)*l2m3;

	mtx.Resize( 5, 5);
	//mtx.Init( 0.0);

	mtx(0,0) = l1;
	mtx(0,1) = 0.5*(l2p3 - 2*l1) /c2;
	mtx(0,2) = vn.cX()* ltmp1 /c;
	mtx(0,3) = vn.cY()* ltmp1 /c;
	mtx(0,4) = vn.cZ()* ltmp1 /c;

	mtx(1,0) = 0.0;
	mtx(1,1) = 0.5*l2p3;
	mtx(1,2) = 0.5*vn.cX()*romc*l2m3;
	mtx(1,3) = 0.5*vn.cY()*romc*l2m3;
	mtx(1,4) = 0.5*vn.cZ()*romc*l2m3;

	mtx(2,0) = 0.0;
	mtx(2,1) = 0.5*vn.cX()*l2m3 / romc;
	mtx(2,2) = l1*( vn.cY()*vn.cY() + vn.cZ()*vn.cZ() ) + 0.5*vn.cX()*vn.cX()*l2p3;
	mtx(2,3) = vn.cX()*vn.cY()*l2p3m1;
	mtx(2,4) = vn.cX()*vn.cZ()*l2p3m1;

	mtx(3,0) = 0.0;
	mtx(3,1) = 0.5*vn.cY()*l2m3 / romc;
	mtx(3,2) = vn.cX()*vn.cY()*l2p3m1;
	mtx(3,3) = l1*( vn.cX()*vn.cX() + vn.cZ()*vn.cZ() ) + 0.5*vn.cY()*vn.cY()*l2p3;
	mtx(3,4) = vn.cY()*vn.cZ()*l2p3m1;

	mtx(4,0) = 0.0;
	mtx(4,1) = 0.5*vn.cZ()*l2m3 / romc;
	mtx(4,2) = vn.cX()*vn.cZ()*l2p3m1;
	mtx(4,3) = vn.cY()*vn.cZ()*l2p3m1;
	mtx(4,4) = l1*( vn.cX()*vn.cX() + vn.cY()*vn.cY() ) + 0.5*vn.cZ()*vn.cZ()*l2p3;

}


//////////////////////////////////////////////////////////////////////

template <> 
template <class MATRIX, class VECTOR>
inline void EqnRansKOMEGA<Geom::DIM_2D>::CJacobQW( MATRIX& mtx, const VECTOR& vsmp)
{
	MGFloat u2 = SqrU( vsmp);
	MGFloat c2 = FLOW_K * vsmp( ET::ID_P) / vsmp( ET::ID_RHO);
	MGFloat c = ::sqrt( c2 );

	mtx.Resize( 6, 6);
	mtx.Init( 0);

	mtx(0,0) = -1.0 / c2;
	mtx(0,1) = vsmp( ET::ID_RHO) / c;

	mtx(1,0) = - ( 0.5*u2 /*+ vsmp( ET::ID_K)*/ ) / c2;
	mtx(1,1) = vsmp( ET::ID_RHO)*( 0.5*u2 /*+ vsmp( ET::ID_K)*/ ) / c  + vsmp( ET::ID_RHO)*c / (FLOW_K-1);
	mtx(1,2) = vsmp( ET::ID_RHO)*vsmp( ET::U<0>::ID);
	mtx(1,3) = vsmp( ET::ID_RHO)*vsmp( ET::U<1>::ID);
	mtx(1,4) = vsmp( ET::ID_RHO);

	mtx(2,0) = - vsmp( ET::U<0>::ID) / c2;
	mtx(2,1) = vsmp( ET::U<0>::ID) * vsmp( ET::ID_RHO) / c;
	mtx(2,2) = vsmp( ET::ID_RHO);

	mtx(3,0) = - vsmp( ET::U<1>::ID) / c2;
	mtx(3,1) = vsmp( ET::U<1>::ID) * vsmp( ET::ID_RHO) / c;
	mtx(3,3) = vsmp( ET::ID_RHO);

	mtx(4,0) = - vsmp( ET::ID_K) / c2;
	mtx(4,1) = vsmp( ET::ID_K) * vsmp( ET::ID_RHO) / c;
	mtx(4,4) = vsmp( ET::ID_RHO);

	mtx(5,0) = - vsmp( ET::ID_OMEGA) / c2;
	mtx(5,1) = vsmp( ET::ID_OMEGA) * vsmp( ET::ID_RHO) / c;
	mtx(5,5) = vsmp( ET::ID_RHO);
}


template <> 
template <class MATRIX, class VECTOR>
inline void EqnRansKOMEGA<Geom::DIM_3D>::CJacobQW( MATRIX& mtx, const VECTOR& vsmp)
{
	MGFloat u2 = SqrU( vsmp);
	MGFloat c2 = FLOW_K * vsmp( ET::ID_P) / vsmp( ET::ID_RHO);
	MGFloat c = ::sqrt( c2 );

	mtx.Resize( 7, 7);
	mtx.Init( 0);

	mtx(0,0) = -1.0 / c2;
	mtx(0,1) = vsmp( ET::ID_RHO) / c;

	mtx(1,0) = - ( 0.5*u2 + vsmp( ET::ID_K) ) / c2;
	mtx(1,1) = vsmp( ET::ID_RHO)*( 0.5*u2 + vsmp( ET::ID_K) ) / c  + vsmp( ET::ID_RHO)*c / (FLOW_K-1);
	mtx(1,2) = vsmp( ET::ID_RHO)*vsmp( ET::U<0>::ID);
	mtx(1,3) = vsmp( ET::ID_RHO)*vsmp( ET::U<1>::ID);
	mtx(1,4) = vsmp( ET::ID_RHO)*vsmp( ET::U<2>::ID);
	mtx(1,5) = vsmp( ET::ID_RHO);

	mtx(2,0) = - vsmp( ET::U<0>::ID) / c2;
	mtx(2,1) = vsmp( ET::U<0>::ID) * vsmp( ET::ID_RHO) / c;
	mtx(2,2) = vsmp( ET::ID_RHO);

	mtx(3,0) = - vsmp( ET::U<1>::ID) / c2;
	mtx(3,1) = vsmp( ET::U<1>::ID) * vsmp( ET::ID_RHO) / c;
	mtx(3,3) = vsmp( ET::ID_RHO);

	mtx(4,0) = - vsmp( ET::U<2>::ID) / c2;
	mtx(4,1) = vsmp( ET::U<2>::ID) * vsmp( ET::ID_RHO) / c;
	mtx(4,4) = vsmp( ET::ID_RHO);

	mtx(5,0) = - vsmp( ET::ID_K) / c2;
	mtx(5,1) = vsmp( ET::ID_K) * vsmp( ET::ID_RHO) / c;
	mtx(5,5) = vsmp( ET::ID_RHO);

	mtx(6,0) = - vsmp( ET::ID_OMEGA) / c2;
	mtx(6,1) = vsmp( ET::ID_OMEGA) * vsmp( ET::ID_RHO) / c;
	mtx(6,6) = vsmp( ET::ID_RHO);
}


template <> 
template <class MATRIX, class VECTOR>
inline void EqnRansKOMEGA<Geom::DIM_2D>::CJacobWZ( MATRIX& mtx, const VECTOR& vsmp)
{
	MGFloat u2 = SqrU( vsmp);
	MGFloat c2 = FLOW_K * vsmp( ET::ID_P) / vsmp( ET::ID_RHO);
	MGFloat c = ::sqrt( c2 );
	MGFloat sqrt_rho = ::sqrt( vsmp( ET::ID_RHO) );
	MGFloat g1 = (FLOW_K-1)/FLOW_K * sqrt_rho;
	MGFloat g2 = (FLOW_K-1) / ( FLOW_K * sqrt_rho * c );

	mtx.Resize( 6, 6);
	mtx.Init( 0);

	mtx(0,0) = - g1 * ( (FLOW_K / (FLOW_K-1) + 1) *c2 - 0.5*u2 );
	mtx(0,1) =   g1;
	mtx(0,2) = - g1 * vsmp( ET::U<0>::ID);
	mtx(0,3) = - g1 * vsmp( ET::U<1>::ID);
	mtx(0,4) = 0.0;//- g1;

	mtx(1,0) =   g2 * ( 0.5*u2 + c2/(FLOW_K-1) );
	mtx(1,1) =   g2;
	mtx(1,2) = - g2 * vsmp( ET::U<0>::ID);
	mtx(1,3) = - g2 * vsmp( ET::U<1>::ID);
	mtx(1,4) = 0.;//- g2;

	mtx(2,0) = - vsmp( ET::U<0>::ID) / sqrt_rho;
	mtx(2,2) = 1.0 / sqrt_rho;

	mtx(3,0) = - vsmp( ET::U<1>::ID) / sqrt_rho;
	mtx(3,3) = 1.0 / sqrt_rho;

	mtx(4,0) = - vsmp( ET::ID_K) / sqrt_rho;
	mtx(4,4) = 1.0 / sqrt_rho;

	mtx(5,0) = - vsmp( ET::ID_OMEGA) / sqrt_rho;
	mtx(5,5) = 1.0 / sqrt_rho;
}

template <> 
template <class MATRIX, class VECTOR>
inline void EqnRansKOMEGA<Geom::DIM_3D>::CJacobWZ( MATRIX& mtx, const VECTOR& vsmp)
{
	MGFloat u2 = SqrU( vsmp);
	MGFloat c2 = FLOW_K * vsmp( ET::ID_P) / vsmp( ET::ID_RHO);
	MGFloat c = ::sqrt( c2 );
	MGFloat sqrt_rho = ::sqrt( vsmp( ET::ID_RHO) );
	MGFloat g1 = (FLOW_K-1)/FLOW_K * sqrt_rho;
	MGFloat g2 = (FLOW_K-1) / ( FLOW_K * sqrt_rho * c );

	mtx.Resize( 7, 7);
	mtx.Init( 0);

	mtx(0,0) = - g1 * ( (FLOW_K / (FLOW_K-1) + 1) *c2 - 0.5*u2 );
	mtx(0,1) =   g1;
	mtx(0,2) = - g1 * vsmp( ET::U<0>::ID);
	mtx(0,3) = - g1 * vsmp( ET::U<1>::ID);
	mtx(0,4) = - g1 * vsmp( ET::U<2>::ID);
	mtx(0,5) = - g1;

	mtx(1,0) =   g2 * ( 0.5*u2 + c2/(FLOW_K-1) );
	mtx(1,1) =   g2;
	mtx(1,2) = - g2 * vsmp( ET::U<0>::ID);
	mtx(1,3) = - g2 * vsmp( ET::U<1>::ID);
	mtx(1,4) = - g2 * vsmp( ET::U<2>::ID);
	mtx(1,5) = - g2;

	mtx(2,0) = - vsmp( ET::U<0>::ID) / sqrt_rho;
	mtx(2,2) = 1.0 / sqrt_rho;

	mtx(3,0) = - vsmp( ET::U<1>::ID) / sqrt_rho;
	mtx(3,3) = 1.0 / sqrt_rho;

	mtx(4,0) = - vsmp( ET::U<2>::ID) / sqrt_rho;
	mtx(4,4) = 1.0 / sqrt_rho;

	mtx(5,0) = - vsmp( ET::ID_K) / sqrt_rho;
	mtx(5,5) = 1.0 / sqrt_rho;

	mtx(6,0) = - vsmp( ET::ID_OMEGA) / sqrt_rho;
	mtx(6,6) = 1.0 / sqrt_rho;
}


template <> 
template <class MATRIX, class VECTOR>
inline void EqnRansKOMEGA<Geom::DIM_2D>::MultWZ( VECTOR& vout, const MATRIX& mtxWZ, const VECTOR& vin)
{
	vout(0) = mtxWZ(0,0)*vin(0) + mtxWZ(0,1)*vin(1) + mtxWZ(0,2)*vin(2) + mtxWZ(0,3)*vin(3) + mtxWZ(0,4)*vin(4);
	vout(1) = mtxWZ(1,0)*vin(0) + mtxWZ(1,1)*vin(1) + mtxWZ(1,2)*vin(2) + mtxWZ(1,3)*vin(3) + mtxWZ(1,4)*vin(4);
	vout(2) = mtxWZ(2,0)*vin(0) + mtxWZ(2,2)*vin(2);
	vout(3) = mtxWZ(3,0)*vin(0) + mtxWZ(3,3)*vin(3);
	vout(4) = mtxWZ(4,0)*vin(0) + mtxWZ(4,4)*vin(4);
	vout(5) = mtxWZ(5,0)*vin(0) + mtxWZ(5,5)*vin(5);
}


template <> 
template <class MATRIX, class VECTOR>
inline void EqnRansKOMEGA<Geom::DIM_3D>::MultWZ( VECTOR& vout, const MATRIX& mtxWZ, const VECTOR& vin)
{
	vout(0) = mtxWZ(0,0)*vin(0) + mtxWZ(0,1)*vin(1) + mtxWZ(0,2)*vin(2) + mtxWZ(0,3)*vin(3) + mtxWZ(0,4)*vin(4) + mtxWZ(0,5)*vin(5);
	vout(1) = mtxWZ(1,0)*vin(0) + mtxWZ(1,1)*vin(1) + mtxWZ(1,2)*vin(2) + mtxWZ(1,3)*vin(3) + mtxWZ(1,4)*vin(4) + mtxWZ(1,5)*vin(5);
	vout(2) = mtxWZ(2,0)*vin(0) + mtxWZ(2,2)*vin(2);
	vout(3) = mtxWZ(3,0)*vin(0) + mtxWZ(3,3)*vin(3);
	vout(4) = mtxWZ(4,0)*vin(0) + mtxWZ(4,4)*vin(4);
	vout(5) = mtxWZ(5,0)*vin(0) + mtxWZ(5,5)*vin(5);
	vout(6) = mtxWZ(6,0)*vin(0) + mtxWZ(6,6)*vin(6);
}

template <> 
template <class MATRIX, class VECTOR>
inline void EqnRansKOMEGA<Geom::DIM_2D>::MultQW( VECTOR& vout, const MATRIX& mtxWZ, const VECTOR& vin)
{
	vout(0) = mtxWZ(0,0)*vin(0) + mtxWZ(0,1)*vin(1);
	vout(1) = mtxWZ(1,0)*vin(0) + mtxWZ(1,1)*vin(1) + mtxWZ(1,2)*vin(2) + mtxWZ(1,3)*vin(3) + mtxWZ(1,4)*vin(4);
	vout(2) = mtxWZ(2,0)*vin(0) + mtxWZ(2,1)*vin(1) + mtxWZ(2,2)*vin(2);
	vout(3) = mtxWZ(3,0)*vin(0) + mtxWZ(3,1)*vin(1) + mtxWZ(3,3)*vin(3);
	vout(4) = mtxWZ(4,0)*vin(0) + mtxWZ(4,1)*vin(1) + mtxWZ(4,4)*vin(4);
	vout(5) = mtxWZ(5,0)*vin(0) + mtxWZ(5,1)*vin(1) + mtxWZ(5,5)*vin(5);
}

template <> 
template <class MATRIX, class VECTOR>
inline void EqnRansKOMEGA<Geom::DIM_3D>::MultQW( VECTOR& vout, const MATRIX& mtxWZ, const VECTOR& vin)
{
	vout(0) = mtxWZ(0,0)*vin(0) + mtxWZ(0,1)*vin(1);
	vout(1) = mtxWZ(1,0)*vin(0) + mtxWZ(1,1)*vin(1) + mtxWZ(1,2)*vin(2) + mtxWZ(1,3)*vin(3) + mtxWZ(1,4)*vin(4) + mtxWZ(1,5)*vin(5);
	vout(2) = mtxWZ(2,0)*vin(0) + mtxWZ(2,1)*vin(1) + mtxWZ(2,2)*vin(2);
	vout(3) = mtxWZ(3,0)*vin(0) + mtxWZ(3,1)*vin(1) + mtxWZ(3,3)*vin(3);
	vout(4) = mtxWZ(4,0)*vin(0) + mtxWZ(4,1)*vin(1) + mtxWZ(4,4)*vin(4);
	vout(5) = /*mtxWZ(5,0)*vin(0) + mtxWZ(5,1)*vin(1) +*/ mtxWZ(5,5)*vin(5);
	vout(6) = mtxWZ(6,0)*vin(0) + mtxWZ(6,1)*vin(1) + mtxWZ(6,6)*vin(6);
}




template <> 
template <class MATRIX, class VECTOR>
inline void EqnRansKOMEGA<Geom::DIM_2D>::CJacobQV( MATRIX& mtx, const VECTOR& vsmp)
{
	MGFloat u2 = SqrU( vsmp);
	MGFloat c2 = FLOW_K * vsmp( ET::ID_P) / vsmp( ET::ID_RHO);
	MGFloat c = ::sqrt( c2 );

	mtx.Resize( 6, 6);
	mtx.Init( 0);

	mtx(0,0) = 1.0;

	mtx(1,0) = 0.5*u2;// + vsmp( ET::ID_K);
	mtx(1,1) = 1.0 / (FLOW_K-1);
	mtx(1,2) = vsmp( ET::ID_RHO)*vsmp( ET::U<0>::ID);
	mtx(1,3) = vsmp( ET::ID_RHO)*vsmp( ET::U<1>::ID);
	mtx(1,4) = vsmp( ET::ID_RHO);

	mtx(2,0) = vsmp( ET::U<0>::ID);
	mtx(2,2) = vsmp( ET::ID_RHO);

	mtx(3,0) = vsmp( ET::U<1>::ID);
	mtx(3,3) = vsmp( ET::ID_RHO);

	mtx(4,0) = vsmp( ET::ID_K);
	mtx(4,4) = vsmp( ET::ID_RHO);

	mtx(5,0) = vsmp( ET::ID_OMEGA);
	mtx(5,5) = vsmp( ET::ID_RHO);
}


template <> 
template <class MATRIX, class VECTOR>
inline void EqnRansKOMEGA<Geom::DIM_3D>::CJacobQV( MATRIX& mtx, const VECTOR& vsmp)
{
	MGFloat u2 = SqrU( vsmp);
	MGFloat c2 = FLOW_K * vsmp( ET::ID_P) / vsmp( ET::ID_RHO);
	MGFloat c = ::sqrt( c2 );

	mtx.Resize( 7, 7);
	mtx.Init( 0);

	mtx(0,0) = 1.0;

	mtx(1,0) = 0.5*u2 + vsmp( ET::ID_K);
	mtx(1,1) = 1.0 / (FLOW_K-1);
	mtx(1,2) = vsmp( ET::ID_RHO)*vsmp( ET::U<0>::ID);
	mtx(1,3) = vsmp( ET::ID_RHO)*vsmp( ET::U<1>::ID);
	mtx(1,4) = vsmp( ET::ID_RHO)*vsmp( ET::U<2>::ID);
	mtx(1,5) = vsmp( ET::ID_RHO);

	mtx(2,0) = vsmp( ET::U<0>::ID);
	mtx(2,2) = vsmp( ET::ID_RHO);

	mtx(3,0) = vsmp( ET::U<1>::ID);
	mtx(3,3) = vsmp( ET::ID_RHO);

	mtx(4,0) = vsmp( ET::U<2>::ID);
	mtx(4,4) = vsmp( ET::ID_RHO);

	mtx(5,0) = vsmp( ET::ID_K);
	mtx(5,5) = vsmp( ET::ID_RHO);

	mtx(6,0) = vsmp( ET::ID_OMEGA);
	mtx(6,6) = vsmp( ET::ID_RHO);
}


template <> 
template <class MATRIX, class VECTOR>
inline void EqnRansKOMEGA<Geom::DIM_2D>::CJacobVZ( MATRIX& mtx, const VECTOR& vsmp)
{
	VECTOR vz = SimpToZ( vsmp);

	MGFloat	z1_inv = 1.0/vz(0);
	MGFloat frac_k = (FLOW_K - 1)/FLOW_K;

	mtx.Resize( 6, 6);
	mtx.Init( 0);

	mtx(0,0) = 2*vz(0);

	mtx(1,0) =   frac_k*( vz(1) - vz(4) );
	mtx(1,1) =   frac_k*vz(0);
	mtx(1,2) = - frac_k*vz(2);
	mtx(1,3) = - frac_k*vz(3);
	mtx(1,4) = - frac_k*vz(0);

	mtx(2,0) = -vz(2)/(vz(0)*vz(0));
	mtx(2,2) = z1_inv;

	mtx(3,0) = -vz(3)/(vz(0)*vz(0));
	mtx(3,3) = z1_inv;

	mtx(4,0) = -vz(4)/(vz(0)*vz(0));
	mtx(4,4) = z1_inv;

	mtx(5,0) = -vz(5)/(vz(0)*vz(0));
	mtx(5,5) = z1_inv;
}


template <> 
template <class MATRIX, class VECTOR>
inline void EqnRansKOMEGA<Geom::DIM_3D>::CJacobVZ( MATRIX& mtx, const VECTOR& vsmp)
{
	VECTOR vz = SimpToZ( vsmp);

	MGFloat	z1_inv = 1.0/vz(0);
	MGFloat frac_k = (FLOW_K - 1)/FLOW_K;

	mtx.Resize( 7, 7);
	mtx.Init( 0);

	mtx(0,0) = 2*vz(0);

	mtx(1,0) =   frac_k*( vz(1) - vz(5) );
	mtx(1,1) =   frac_k*vz(0);
	mtx(1,2) = - frac_k*vz(2);
	mtx(1,3) = - frac_k*vz(3);
	mtx(1,4) = - frac_k*vz(4);
	mtx(1,5) = - frac_k*vz(0);

	mtx(2,0) = -vz(2)/(vz(0)*vz(0));
	mtx(2,2) = z1_inv;

	mtx(3,0) = -vz(3)/(vz(0)*vz(0));
	mtx(3,3) = z1_inv;

	mtx(4,0) = -vz(4)/(vz(0)*vz(0));
	mtx(4,4) = z1_inv;

	mtx(5,0) = -vz(5)/(vz(0)*vz(0));
	mtx(5,5) = z1_inv;

	mtx(6,0) = -vz(6)/(vz(0)*vz(0));
	mtx(6,6) = z1_inv;
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EQNRANSKOMEGA_H__
