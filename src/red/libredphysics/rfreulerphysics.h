#ifndef __RFREULERPHYSICS_H__
#define __RFREULERPHYSICS_H__

#include "libredphysics/eulerequations.h"
#include "libcoreconfig/configbase.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class RfrDefinition  -- def. of the rotating frame of reference
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class RfrDefinition : public ConfigBase
{
public:
	typedef Geom::Vect<DIM>		GVec;
	typedef typename PhysicsCommon<DIM,EQN_RFR_EULER>::FVec		FVec;

	RfrDefinition() : mVelocity(0.0), mRotOmega(0.0), mRotAxisOrig(0.0), mRotAxisDir(0.0)		{}

	void Create( const CfgSection* pcfgsec)
	{ 
		ConfigBase::Create( pcfgsec);
	}

	void Init();


	void	ToUndim( const FVec& vref )
	{
		MGFloat refu =  ::sqrt( EqnEuler<DIM>::SqrU( vref ) ) / vref( 0);

		mVelocity /= refu;
		mRotOmega /= refu;
	}

	void	ToDim( const FVec& vref )
	{
		MGFloat refu =  ::sqrt( EqnEuler<DIM>::SqrU( vref ) ) / vref( 0);

		mVelocity *= refu;
		mRotOmega *= refu;
	}


	GVec	BkgVelocity( const GVec& pos) const
	{
		GVec vel = mVelocity + OmegaCross( pos - mRotAxisOrig );
		return vel;
	}

	GVec	OmegaCross( const GVec& v) const;

//private:
	GVec	mVelocity;
	MGFloat	mRotOmega;
	GVec	mRotAxisOrig;
	GVec	mRotAxisDir;
};







//////////////////////////////////////////////////////////////////////
//	class Physics for RFR_Euler
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class Physics<DIM,EQN_RFR_EULER> : public PhysicsCommon<DIM,EQN_RFR_EULER>
{
	enum { ESIZE = PhysicsCommon<DIM,EQN_RFR_EULER>::ESIZE };

	typedef typename PhysicsCommon<DIM,EQN_RFR_EULER>::EqDef	EqDef;
	typedef typename PhysicsCommon<DIM,EQN_RFR_EULER>::FVec		FVec;
	typedef typename PhysicsCommon<DIM,EQN_RFR_EULER>::AVec		AVec;
	typedef typename PhysicsCommon<DIM,EQN_RFR_EULER>::GVec		GVec;

public:
	Physics()	: PhysicsCommon<DIM,EQN_RFR_EULER>()	{}

	virtual void Create( const CfgSection* pcfgsec);
	virtual void PostCreateCheck() const;
	virtual void Init();

	virtual void	DimToUndim( FVec& vout, const FVec& vin, const FVec& vref ) const;
	virtual void	UndimToDim( FVec& vout, const FVec& vin, const FVec& vref ) const;

	void	ApplyBC(FVec& vecout, AVec& avecout, const FVec& vecin, const AVec& avecin, const MGSize& surfid, const GVec& vpos, const GVec& vnn, const MGFloat& dy, const typename PhysicsCommon<DIM,EQN_RFR_EULER>::PrdBCBase* pbc, const MGSize& nodeid) const;

	template <class VECTOR>
	bool	IsPhysical( const VECTOR& vec);

	MGFloat				MaxSpeed( const FVec& vec) const;
	const MGFloat&		cRefLength() const					{ return mRefLength;}

	const RfrDefinition<DIM>& cRfrDef() const				{ return mRFRdef; }

	//const GVec&			cRefVelocity() const				{ return mRefVelocity;}
	//const MGFloat&		cRefOmega() const					{ return mRefRotOmega;}
	//const Geom::Vect3D&	cRefAxisOrig() const				{ return mRefRotAxisOrig;}
	//const Geom::Vect3D&	cRefAxisDir() const					{ return mRefRotAxisDir;}

	//GVec	BkgVelocity( const GVec& pos) const;

protected:
	void	InitFVec( FVec& vec, const CfgSection& sec);

private:
	MGFloat			mRefLength;

	//GVec			mRefVelocity;
	//MGFloat			mRefRotOmega;
	//Geom::Vect3D	mRefRotAxisOrig;
	//Geom::Vect3D	mRefRotAxisDir;

	RfrDefinition<DIM>	mRFRdef;

};
//////////////////////////////////////////////////////////////////////



template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_RFR_EULER>::Create( const CfgSection* pcfgsec)		
{ 
	PhysicsCommon<DIM,EQN_RFR_EULER>::Create( pcfgsec);

	const CfgSection* prfrsect = & this->ConfigSect().GetSection( ConfigStr::Solver::Physics::RFR_Def::NAME );

	mRFRdef.Create( prfrsect );
}


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_RFR_EULER>::PostCreateCheck() const
{ 
	PhysicsCommon<DIM,EQN_RFR_EULER>::PostCreateCheck();

	// do check
}


template <Geom::Dimension DIM>
inline MGFloat Physics<DIM,EQN_RFR_EULER>::MaxSpeed( const FVec& vec) const
{ 
	return ::sqrt( EqnEuler<DIM>::SqrU( vec) ) + EqnEulerComm<DIM>::C( vec);
}

template <Geom::Dimension DIM>
template <class VECTOR>
bool Physics<DIM,EQN_RFR_EULER>::IsPhysical( const VECTOR& vec)
{
	return true;
}
	

template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_RFR_EULER>::DimToUndim( FVec& vout, const FVec& vin, const FVec& vref ) const
{
	vout = EqnEulerComm<DIM>::DimToUndim( vin, vref);
}


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_RFR_EULER>::UndimToDim( FVec& vout, const FVec& vin, const FVec& vref ) const
{
	vout = EqnEulerComm<DIM>::UndimToDim( vin, vref);
}


//template <>
//inline Geom::Vect2D	Physics<Geom::DIM_2D,EQN_RFR_EULER>::BkgVelocity( const GVec& pos) const
//{
//	Geom::Vect3D vpos( pos.cX(), pos.cY(), 0 );
//	Geom::Vect3D vrot = mRefRotOmega * mRefRotAxisDir.versor() % ( vpos - mRefRotAxisOrig );
//
//	GVec vel = mRefVelocity + GVec( vrot.cX(), vrot.cY() );
//
//	MGFloat refu =  ::sqrt( EqnEuler<Geom::DIM_2D>::SqrU( cRefVar() ) ) / cRefVar()( 0);
//
//	return vel / refu;
//}
//
//template <>
//inline Geom::Vect3D	Physics<Geom::DIM_3D,EQN_RFR_EULER>::BkgVelocity( const GVec& pos) const
//{
//	GVec vel = mRefVelocity + mRefRotOmega * mRefRotAxisDir.versor() % (pos - mRefRotAxisOrig);
//	MGFloat refu =  ::sqrt( EqnEuler<Geom::DIM_2D>::SqrU( cRefVar() ) ) / cRefVar()( 0);
//	return vel / refu;
//}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EULERPHYSICS_H__ 

