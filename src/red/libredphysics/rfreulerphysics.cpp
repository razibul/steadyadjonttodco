#include "libredcore/configconst.h"
#include "libredphysics/physics.h"
#include "libredphysics/eulerequations.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
//	class RfrDefinition  -- def. of the rotating frame of reference
//////////////////////////////////////////////////////////////////////

template <>
void RfrDefinition<Geom::DIM_2D>::Init()
{
	if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Physics::RFR_Def::RFR_TRANSL_VELO ) )
	{
		vector<MGFloat> tab;
		tab = this->ConfigSect().ValueVectorFLOAT( ConfigStr::Solver::Physics::RFR_Def::RFR_TRANSL_VELO );
		if ( tab.size() != 2)
			THROW_INTERNAL( "RFR_TRANSL_VELO - expecting 2 coordinates");

		mVelocity = GVec( tab[0], tab[1] );
	}

	if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Physics::RFR_Def::RFR_ROT_OMEGA ) )
	{
		mRotOmega = this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::RFR_Def::RFR_ROT_OMEGA );

		vector<MGFloat> tab;
		tab = this->ConfigSect().ValueVectorFLOAT( ConfigStr::Solver::Physics::RFR_Def::RFR_ROT_AXIS_ORIG );
		if ( tab.size() != 2)
			THROW_INTERNAL( "RFR_ROT_AXIS_ORIG - expecting 2 coordinates");

		mRotAxisOrig = GVec( tab[0], tab[1] );
	}
}

template <>
void RfrDefinition<Geom::DIM_3D>::Init()
{
	if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Physics::RFR_Def::RFR_TRANSL_VELO ) )
	{
		vector<MGFloat> tab;
		tab = this->ConfigSect().ValueVectorFLOAT( ConfigStr::Solver::Physics::RFR_Def::RFR_TRANSL_VELO );
		if ( tab.size() != 3)
			THROW_INTERNAL( "RFR_TRANSL_VELO - expecting 3 coordinates");

		mVelocity = GVec( tab[0], tab[1], tab[2] );
	}

	if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Physics::RFR_Def::RFR_ROT_OMEGA ) )
	{
		mRotOmega = this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::RFR_Def::RFR_ROT_OMEGA );

		vector<MGFloat> tab;
		tab = this->ConfigSect().ValueVectorFLOAT( ConfigStr::Solver::Physics::RFR_Def::RFR_ROT_AXIS_ORIG );
		if ( tab.size() != 3)
			THROW_INTERNAL( "RFR_ROT_AXIS_ORIG - expecting 3 coordinates");

		mRotAxisOrig = GVec( tab[0], tab[1], tab[2] );

		tab = this->ConfigSect().ValueVectorFLOAT( ConfigStr::Solver::Physics::RFR_Def::RFR_ROT_AXIS_DIR );
		if ( tab.size() != 3)
			THROW_INTERNAL( "RFR_ROT_AXIS_DIR - expecting 3 coordinates");

		mRotAxisDir = GVec( tab[0], tab[1], tab[2] );
	}
}


template <>
Geom::Vect2D RfrDefinition<Geom::DIM_2D>::OmegaCross( const GVec& v) const
{
	GVec vres = mRotOmega * GVec( -v.cY(), v.cX() );
	return vres;
}

template <>
Geom::Vect3D RfrDefinition<Geom::DIM_3D>::OmegaCross( const GVec& v) const
{
	GVec vres = mRotOmega * mRotAxisDir.versor() % v;
	return vres;
}






//////////////////////////////////////////////////////////////////////
//	class Physics for RFR_Euler
//////////////////////////////////////////////////////////////////////


template <Geom::Dimension DIM>
void Physics<DIM,EQN_RFR_EULER>::InitFVec( FVec& vec, const CfgSection& sec)
{
	FVec	vs( ESIZE, 0.0);

	if ( strcmp( sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY ).c_str(), ConfigStr::Solver::Physics::Var::Type::Value::MRPA ) == 0 )
	{
		MGFloat ma = sec.ValueFloat( ConfigStr::Solver::Physics::Var::MACH );
		MGFloat alpha = M_PI / 180.0 * sec.ValueFloat( ConfigStr::Solver::Physics::Var::ALPHA );
		MGFloat ro =  sec.ValueFloat( ConfigStr::Solver::Physics::Var::RO );
		MGFloat p =  sec.ValueFloat( ConfigStr::Solver::Physics::Var::P );

		MGFloat c = ::sqrt( FLOW_K * p / ro);

		vs( EqDef::ID_RHO) = ro;
		vs( EqDef::ID_P) = p;
		vs( EqDef::template U<0>::ID) = cos( alpha) * ma * c;
		vs( EqDef::template U<1>::ID) = sin( alpha) * ma * c;
		if ( DIM >= Geom::DIM_3D)
			vs( EqDef::template U<2>::ID) = 0;

		//vs.Write();
	}
	else if ( strcmp( sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY).c_str(), ConfigStr::Solver::Physics::Var::Type::Value::RPUV) == 0 )
	{
		vs( EqDef::ID_RHO) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::RO);;
		vs( EqDef::ID_P) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::P);
		vs( EqDef::template U<0>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::UX );
		vs( EqDef::template U<1>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::UY );
		if ( DIM >= Geom::DIM_3D)
			vs( EqDef::template U<2>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::UZ );

		//vs.Write();
	}
	else if ( strcmp( sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY).c_str(), ConfigStr::Solver::Physics::Var::Type::Value::RRERURV) == 0 )
	{
		vec( EqDef::ID_RHO) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::RO);;
		vec( EqDef::ID_P) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROE);
		vec( EqDef::template U<0>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROUX );
		vec( EqDef::template U<1>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROUY );
		if ( DIM >= Geom::DIM_3D)
			vs( EqDef::template U<2>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROUZ);

		vs = EqnEuler<DIM>::ConservToSimp( vs);
		//vs.Write();
	}
	else
	{
		THROW_INTERNAL( "unrecognized type of variable: '" << sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY) << "'" );
	}

	vec = EqnEuler<DIM>::SimpToConserv( vs);
	//vec.Write();
}


template <Geom::Dimension DIM>
void Physics<DIM,EQN_RFR_EULER>::Init()
{
	// parent class Init()
	PhysicsCommon<DIM,EQN_RFR_EULER>::Init();

	mRefLength = this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::REF_LENGTH );

	//if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Physics::REF_TRANSL_VELO ) )
	//{
	//	vector<MGFloat> tab;
	//	tab = this->ConfigSect().ValueVectorFLOAT( ConfigStr::Solver::Physics::REF_TRANSL_VELO );
	//	for ( MGSize i=0; i<DIM; ++i)
	//		mRefVelocity.rX(i) = tab[i];

	//}
	//else
	//{
	//	mRefVelocity = GVec( 0.0);
	//}

	//if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Physics::REF_ROT_OMEGA ) )
	//{
	//	mRefRotOmega = this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::REF_ROT_OMEGA );

	//	vector<MGFloat> tab;
	//	tab = this->ConfigSect().ValueVectorFLOAT( ConfigStr::Solver::Physics::REF_ROT_AXIS_ORIG );
	//	if ( tab.size() != 3)
	//		THROW_INTERNAL( "REF_ROT_AXIS_ORIG - expected 3 coordinates");

	//	for ( MGSize i=0; i<3; ++i)
	//		mRefRotAxisOrig.rX(i) = tab[i];

	//	tab = this->ConfigSect().ValueVectorFLOAT( ConfigStr::Solver::Physics::REF_ROT_AXIS_DIR );
	//	if ( tab.size() != 3)
	//		THROW_INTERNAL( "REF_ROT_AXIS_ORIG - expected 3 coordinates");

	//	for ( MGSize i=0; i<3; ++i)
	//		mRefRotAxisDir.rX(i) = tab[i];
	//}
	//else
	//{
	//	mRefRotOmega = 0.0;
	//	mRefRotAxisDir = Geom::Vect3D( 0,0,0);
	//	mRefRotAxisOrig = Geom::Vect3D( 0,0,0);
	//}

	mRFRdef.Init();

	cout << ConfigStr::Solver::Physics::REF_TRANSL_VELO		<< " = " << mRFRdef.mVelocity << endl;
	cout << ConfigStr::Solver::Physics::REF_ROT_OMEGA		<< " = " << mRFRdef.mRotOmega << endl;
	cout << ConfigStr::Solver::Physics::REF_ROT_AXIS_DIR	<< " = " << mRFRdef.mRotAxisDir << endl;
	cout << ConfigStr::Solver::Physics::REF_ROT_AXIS_ORIG	<< " = " << mRFRdef.mRotAxisOrig << endl;

	mRFRdef.ToUndim( this->cRefVar() );
}
 

template <Geom::Dimension DIM>
void Physics<DIM, EQN_RFR_EULER>::ApplyBC(FVec& vecout, AVec& avecout, const FVec& vecin, const AVec& avecin, const MGSize& surfid, const GVec& vpos, const GVec& vnn, const MGFloat& dy, const typename PhysicsCommon<DIM,EQN_RFR_EULER>::PrdBCBase* pbc, const MGSize& nodeid) const
{
	MGFloat	vmod, vvmod;
	GVec	vt, vv, vvn;

	BC	bc = this->cBC( surfid);
	MGSize	varid = this->cVarId( surfid);


	switch ( bc)
	{
	case BC_VISCWALL:
		{
			vecout(0) = vecin(0);
			vecout(1) = vecin(1);

			for ( MGSize k=0; k<DIM; ++k)
				//vecout(2+k) = 0;
				vecout(2+k) = -vecin(2+k);

			THROW_INTERNAL( "there should be _no_ viscous wall BCs for Euler" );
			break;
		}
	case BC_SYMMETRY:
	case BC_INVISCWALL:
		{
			//GVec vrelu = BkgVelocity( vpos); // cRefVelocity() / ( ::sqrt( EqnEuler<DIM>::SqrU( cRefVar() ) ) / cRefVar()( 0) );

			GVec vrelu = mRFRdef.BkgVelocity( vpos); // cRefVelocity() / ( ::sqrt( EqnEuler<DIM>::SqrU( cRefVar() ) ) / cRefVar()( 0) );

			for ( MGSize k=0; k<DIM; ++k)
				vv.rX((MGInt)k) = vecin(2+k);

			vv -= vecin(0)*vrelu;

			vmod = vv.module();
			vvn = (vv*vnn)*vnn;
			vv = vv - vvn;

			vvmod = vv.module();
			if ( vvmod > ZERO )
				vv *= vmod / vvmod;

			vv += vecin(0)*vrelu;

			vecout(0) = vecin(0);
			vecout(1) = vecin(1);

			for ( MGSize k=0; k<DIM; ++k)
				vecout(2+k) = vv.cX( (MGInt)k );

			break;
		}
	case BC_FARFIELD:
		{
			vecout = this->cDefVar( 0);


			//GVec vdir = vpos.versor();
			//MGFloat r = vpos.module();
			//MGFloat vel = r;

			//vecout(2) = + vdir.cY() * vel *vecout(0); 
			//vecout(3) = - vdir.cX() * vel *vecout(0); 
			break;
		}
	case BC_EXTRAPOL:
		{
			vecout = vecin;
			break;
		}
	case BC_DEF:
		{
			vecout = this->cDefVar( varid);
			break;
		}
	case BC_PERIODIC:
		{
			FVec tmp;
			pbc->GetPeriodicVar(nodeid, tmp);
			for (MGSize i = 0; i < ESIZE; ++i)
			{
				vecout(i) = tmp(i);
			}
			break;
		}
	default:
		{
			//char sbuf[1024];
			//sprintf( sbuf, "BC not implemented! %d", bc);
			//THROW_INTERNAL( sbuf);
			THROW_INTERNAL( "BC not implemented! bc = '"<<bc<<"' surfi = '"<<surfid<<"'" );
		}
	}
}


//////////////////////////////////////////////////////////////////////
template class Physics<Geom::DIM_2D, EQN_RFR_EULER>;
template class Physics<Geom::DIM_3D, EQN_RFR_EULER>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 
 
