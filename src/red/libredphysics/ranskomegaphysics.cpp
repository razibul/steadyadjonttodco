#include "libredcore/configconst.h"
#include "libredphysics/physics.h"
#include "libredphysics/ranskomegaequations.h"
#include "libredphysics/ranskomegaphysics.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Physics for RANS with 2 turbulence equations
//////////////////////////////////////////////////////////////////////


// HACK :: this the same as for Euler...
template <Geom::Dimension DIM>
void Physics<DIM,EQN_RANS_KOMEGA>::InitFVec( FVec& vec, const CfgSection& sec)
{
	//typedef	EquationDef<Geom::DIM_2D,EQN_RANS_KOMEGA>::SplitingFlowTurb::Block<1> TMapper;
	//typedef	EquationDef<Geom::DIM_2D,EQN_RANS_KOMEGA>::FVec	TFlowVec;

	//TFlowVec
	FVec vs, vout;

	vec.Init( 0.0);

	if ( strcmp( sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY ).c_str(), ConfigStr::Solver::Physics::Var::Type::Value::MRPA ) == 0 )
	{
		MGFloat ma = sec.ValueFloat( ConfigStr::Solver::Physics::Var::MACH );
		MGFloat alpha = M_PI / 180.0 * sec.ValueFloat( ConfigStr::Solver::Physics::Var::ALPHA );
		MGFloat ro =  sec.ValueFloat( ConfigStr::Solver::Physics::Var::RO );
		MGFloat p =  sec.ValueFloat( ConfigStr::Solver::Physics::Var::P );

		MGFloat c = ::sqrt( FLOW_K * p / ro);

		vs( EqDef::ID_RHO) = ro;
		vs( EqDef::ID_P) = p;
		vs( EqDef::template U<0>::ID) = cos( alpha) * ma * c;
		vs( EqDef::template U<1>::ID) = sin( alpha) * ma * c;
		if ( DIM >= Geom::DIM_3D)
			vs( EqDef::template U<2>::ID) = 0;

		vs( EqDef::ID_K) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::K );
		vs( EqDef::ID_OMEGA) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::OMEGA );

		//vs.Write();
	}
	else if ( strcmp( sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY).c_str(), ConfigStr::Solver::Physics::Var::Type::Value::RPUV) == 0 )
	{
		vs( EqDef::ID_RHO) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::RO);;
		vs( EqDef::ID_P) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::P);
		vs( EqDef::template U<0>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::UX );
		vs( EqDef::template U<1>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::UY );
		if ( DIM >= Geom::DIM_3D)
			vs( EqDef::template U<2>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::UZ );

		vs( EqDef::ID_K) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::K );
		vs( EqDef::ID_OMEGA) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::OMEGA );

		//vs.Write();
	}
	else if ( strcmp( sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY).c_str(), ConfigStr::Solver::Physics::Var::Type::Value::RRERURV) == 0 )
	{
		vec( EqDef::ID_RHO) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::RO);;
		vec( EqDef::ID_P) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROE);
		vec( EqDef::template U<0>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROUX );
		vec( EqDef::template U<1>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROUY );
		if ( DIM >= Geom::DIM_3D)
			vs( EqDef::template U<2>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROUZ);

		vec( EqDef::ID_K) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::K );
		vec( EqDef::ID_OMEGA) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::OMEGA );

		vs = EqnRansKOMEGA<DIM>::ConservToSimp( vs);
		//vs.Write();
	}
	else
	{
		THROW_INTERNAL( "unrecognized type of variable: '" << sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY) << "'" );
	}


	vout = EqnRansKOMEGA<DIM>::SimpToConserv( vs);
	//vec.Write();

	vec = vout;
//	TMapper::LocalToGlobal( vec, vout);

}


template <Geom::Dimension DIM>
void Physics<DIM,EQN_RANS_KOMEGA>::Init()
{
	//if ( strcmp( sec.Name().c_str(), STR_CFGSEC_SPACE_NAME) != 0)
	//	THROW_INTERNAL( "Bad section used for CfgSpaceSolver");

	mRefLength	= this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::REF_LENGTH );
	mRefRe		= this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::REF_REYNOLDS );
	mRefPr		= this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::REF_PRANDTL );
	mRefPrT		= this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::REF_PRANDTL_TURB );

	cout << endl << "Re = " << mRefRe << endl;
	cout << "Prandtl = " << mRefPr << endl;
	cout << "Prandtl_turb = " << mRefPrT << endl << endl;

	// parent class Init()
	PhysicsCommon<DIM,EQN_RANS_KOMEGA>::Init();
}


template <Geom::Dimension DIM>
bool Physics<DIM,EQN_RANS_KOMEGA>::IsStrongBC( const MGSize& id) const
{
	BC	bc = this->cBC( id);

	switch ( bc)
	{
	case BC_VISCWALL:
	//case BC_OUTLET:
	//case BC_DEF:
		return true;
	}

	return false;
}


template <Geom::Dimension DIM>
bool Physics<DIM,EQN_RANS_KOMEGA>::IsEqStrongBC( const MGSize& id, const MGSize& ie) const
{
	BC	bc = this->cBC( id);

	switch ( bc)
	{
	case BC_VISCWALL:
		if ( ie == EqDef::ID_K || ie == EqDef::ID_OMEGA || ( ie >= EqDef::template U<0>::ID && ie < EqDef::template U<DIM>::ID ) )
			return true;


	//case BC_OUTLET:
	//	if ( ie == EqDef::ID_P )
	//		return true;
	}

	return false;
}



template <Geom::Dimension DIM>
void Physics<DIM, EQN_RANS_KOMEGA>::ApplyBC(FVec& vecout, AVec& avecout, const FVec& vecin, const AVec& avecin, const MGSize& surfid, const GVec& vpos, const GVec& vnn, const MGFloat& dy, const typename PhysicsCommon<DIM,EQN_RANS_KOMEGA>::PrdBCBase* pbc, const MGSize& nodeid) const
{
	MGFloat	vmod, vvmod;
	GVec	vt, vv, vvn;

	BC	bc = this->cBC( surfid);
	MGSize	varid = this->cVarId( surfid);

	avecout = avecin;

	switch ( bc)
	{
	case BC_VISCWALL:
		{
			vecout = EqnRansKOMEGA<DIM>::ConservToSimp( vecin );

			for ( MGSize k=0; k<DIM; ++k)
			{
				vecout( EqDef::template U<0>::ID + k ) *= 1.0e-10;
				vecout( EqDef::template U<0>::ID + k ) = max( vecout( EqDef::template U<0>::ID + k ), 1.0e-10 );
			}

			//for ( MGSize k=0; k<DIM; ++k)
			//	vecout(2+k) *= 1.0e-10;

			vecout( EqDef::ID_K ) = 0.0;
			
			MGFloat y = dy;
			//cout << y << endl;
			//MGFloat y = 1.0e-6;

			//FVec vref = EqnRansKOMEGA<DIM>::ConservToSimp( this->cDefVar( 0) );
			//MGFloat u2 = EqnRansKOMEGA<DIM>::SqrU( vref);
			//MGFloat u = ::sqrt( u2);

			vecout( EqDef::ID_OMEGA ) = log( 1.e-5 * 60.0 /* * u * cRefLength()*/ / ( 0.075 * cRe() * y*y) );

			vecout = EqnRansKOMEGA<DIM>::SimpToConserv( vecout);

			break; 
		}
	case BC_SYMMETRY:
	case BC_INVISCWALL:
		{
			for ( MGSize k=0; k<DIM; ++k)
				vv.rX(k) = vecin( EqDef::template U<0>::ID + k );

			vmod = vv.module();
			vvn = (vv*vnn)*vnn;
			vv = vv - vvn;

			vvmod = vv.module();
			//if ( vvmod > ZERO )
			//	vv *= vmod / vvmod;


			vecout( EqDef::ID_RHO ) = vecin( EqDef::ID_RHO );
			vecout( EqDef::ID_P ) = vecin( EqDef::ID_P );
			vecout( EqDef::ID_K ) = vecin( EqDef::ID_K );
			vecout( EqDef::ID_OMEGA ) = vecin( EqDef::ID_OMEGA );

			for ( MGSize k=0; k<DIM; ++k)
				vecout( EqDef::template U<0>::ID + k ) = vv.cX( k );

			break;
		}
	case BC_FARFIELD:
		{
			vecout = this->cDefVar( 0);

			for ( MGSize k=0; k<DIM; ++k)
				vv.rX(k) = vecin( EqDef::template U<0>::ID + k );

			if ( vv * vnn < 0.0 )
			{
				vecout( EqDef::ID_K ) = vecin( EqDef::ID_K );
				vecout( EqDef::ID_OMEGA ) = vecin( EqDef::ID_OMEGA );
			}

			break;
		}
	case BC_OUTLET:
		{
			FVec vref = EqnRansKOMEGA<DIM>::ConservToSimp( this->cDefVar( 0) );
			vecout = EqnRansKOMEGA<DIM>::ConservToSimp( vecin );

			vecout( EqDef::ID_P ) = vref( EqDef::ID_P ); 

			vecout = EqnRansKOMEGA<DIM>::SimpToConserv( vecout);
			break;
		}
	case BC_EXTRAPOL:
		{
			FVec vref = EqnRansKOMEGA<DIM>::ConservToSimp( this->cDefVar( 0) );
			vecout = EqnRansKOMEGA<DIM>::ConservToSimp( vecin );
			//vecout(0) = min( vref(0), vecout(0) ); 
			vecout(1) = vref(1); 
			vecout = EqnRansKOMEGA<DIM>::SimpToConserv( vecout);
			//vecout = vecin;
			break;
		}
	case BC_PERIODIC:
	{
			FVec tmp;
			pbc->GetPeriodicVar(nodeid, tmp);
			for (MGSize i = 0; i < ESIZE; ++i)
			{
				vecout(i) = tmp(i);
			}
			break;
	}
	case BC_DEF:
		{
			vecout = this->cDefVar( varid);
			break;
		}
	default:
		{
			char sbuf[1024];
			sprintf( sbuf, "BC not implemented! %d", bc);
			THROW_INTERNAL( sbuf);
		}
	}

}


//////////////////////////////////////////////////////////////////////
template class Physics<Geom::DIM_2D, EQN_RANS_KOMEGA>;
template class Physics<Geom::DIM_3D, EQN_RANS_KOMEGA>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

