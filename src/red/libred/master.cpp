#include "master.h"
#include "libcorecommon/factory.h"
#include "libredcore/configconst.h"
#include "libredcore/processinfo.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


Master::~Master()	
{
	for ( vector<SolverBase*>::iterator itr=mtabSolver.begin(); itr!=mtabSolver.end(); ++itr)
		if ( *itr != 0 )
			delete *itr;
}


void Master::ReadConfig( const MGString& fname)	
{ 
	mConfig.ReadCFG( fname);
	mConfig.WriteCFG( "_dump.cfg");
}



void Master::Create()
{
	cout << ConfigStr::Solver::NAME << " count = " << mConfig.GetSectionCount( ConfigStr::Solver::NAME ) << endl;

	MGSize count = mConfig.GetSectionCount( ConfigStr::Solver::NAME );

	for ( MGSize i=0; i<count; ++i)
	{
		const CfgSection* cfgsec = &mConfig.GetSection( ConfigStr::Solver::NAME, i);

		MGString sdim		= cfgsec->ValueString( ConfigStr::Solver::Dim::KEY );
		MGString seqtype	= cfgsec->ValueString( ConfigStr::Solver::EqType::KEY );
		MGString ssoltype	= cfgsec->ValueString( ConfigStr::Solver::SolType::KEY );

		MGString solkey = ProcessInfo::Prefix() + "SOLVER_" + sdim + "_" + seqtype + "_" + ssoltype;
		cout << "creating '" << solkey << "'" << endl;


		SolverBase* ptr = Singleton< Factory< MGString,Creator<SolverBase> > >::GetInstance()->GetCreator( solkey )->Create( );
		ptr->Create( cfgsec);

		mtabSolver.push_back( ptr);
	}

	// do postcreate check
	if ( mtabSolver.size() == 0 )
		THROW_INTERNAL( "Master::PostCreateCheck() -- failed : 'mtabSolver.size() == 0'" );

	for ( MGSize i=0; i<mtabSolver.size(); ++i)
		if ( ! mtabSolver[i] )
			THROW_INTERNAL( "Solver<DIM,ET>::PostCreateCheck() -- failed : 'mtabSolver[" << i << "] == NULL'" );

	for ( MGSize i=0; i<mtabSolver.size(); ++i)
		mtabSolver[i]->PostCreateCheck();
}


void Master::Init()
{
	for ( MGSize i=0; i<mtabSolver.size(); ++i)
		mtabSolver[i]->Init();
}


void Master::Solve()
{
	for ( MGSize i=0; i<mtabSolver.size(); ++i)
		mtabSolver[i]->Solve();
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

