#include "setup.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


void RegisterLib_REDCONVERGENCE();
void RegisterLib_REDVCCOMMON();

void RegisterLib_REDVCRDSPoisson();
void RegisterLib_REDVCRDSEULER();
void RegisterLib_REDVCRDSNS();
void RegisterLib_REDVCRDSRANS();

void RegisterLib_REDVCRDSRFREULER();

void RegisterLibraries()
{
	RegisterLib_REDCONVERGENCE();
	RegisterLib_REDVCCOMMON();
	RegisterLib_REDVCRDSPoisson();
	RegisterLib_REDVCRDSEULER();
	RegisterLib_REDVCRDSNS();
	RegisterLib_REDVCRDSRANS();

	RegisterLib_REDVCRDSRFREULER();
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

