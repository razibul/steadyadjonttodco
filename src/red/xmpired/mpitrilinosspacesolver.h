#ifndef __MPITRILINOS_SPACESOLVER_H__
#define __MPITRILINOS_SPACESOLVER_H__


#include "redvc/libredvccommon/spacesolver.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

//////////////////////////////////////////////////////////////////////
//  class MPITRILINOSSpaceSolver
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
class MPITRILINOSSpaceSolver : public SpaceSolver<DIM,ET,MAPPER>
{
public:
    enum { ESIZE = EquationDef<DIM,ET>::SIZE };
    enum { ASIZE = EquationDef<DIM,ET>::AUXSIZE };
    enum { BSIZE = MAPPER::VBSIZE };

    typedef CFlowCell<DIM,ESIZE,ASIZE>      CFCell;
    typedef CFlowBFace<DIM,ESIZE,ASIZE>     CFBFace;

    typedef typename EquationDef<DIM,ET>::FVec  EVec;
    typedef typename EquationDef<DIM,ET>::FMtx  EMtx;

    typedef typename EquationTypes<DIM,BSIZE>::Vec  BVec;
    typedef typename EquationTypes<DIM,BSIZE>::Mtx  BMtx;



    static MGString Info()
    {
        ostringstream os;
        os << "MPISpaceSolver< " << Geom::DimToStr(DIM) << ", " << EquationDef<DIM,ET>::NameEqnType() << ", " << BSIZE << " >";
        return os.str();
    }

    MPITRILINOSSpaceSolver() : SpaceSolver<DIM,ET,MAPPER>()        {}
    //virtual ~MPITRILINOSSpaceSolver()    {};

    virtual void    Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata);
    virtual void    PostCreateCheck() const;
    virtual void    Init();

    // PETSc
    virtual void    TRILINOSInitOwnedIdRange( pair<MGSize,MGSize>& range, vector<MGSize>& tabg) const;
    virtual void	TRILINOSResizeGraph( const Epetra_BlockMap& map, Epetra_CrsGraph*& graph) const;

    virtual void    TRILINOSResizeVct( Vec& vct) const;

    virtual void    TRILINOSResiduum( Vec& vec )       {}
    virtual void    TRILINOSResNumJacob( Vec& vec, Mat& vmtx, const vector<MGFloat>& tabDT);

    virtual void    TRILINOSResJacobStrongBC( Vec& vec, Mat& vmtx )        {}
    virtual void    TRILINOSApplyStrongBC( Vec& vec )      {}
    
};


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void MPITRILINOSSpaceSolver<DIM,ET,MAPPER>::PostCreateCheck() const
{
    SpaceSolver<DIM,ET,MAPPER>::PostCreateCheck();

}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void MPITRILINOSSpaceSolver<DIM,ET,MAPPER>::Init()
{
    SpaceSolver<DIM,ET,MAPPER>::Init();
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


#endif // __MPITRILINOS_SPACESOLVER_H__

