#include <mpi.h>
#include <petscksp.h>
#include <petscmat.h>

#include "mpipetscspacesolver.h"
#include "libcorecommon/factory.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
//MPIPETScSpaceSolver<DIM,ET,MAPPER>::~MPIPETScSpaceSolver()
//{
//}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPIPETScSpaceSolver<DIM,ET,MAPPER>::Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata)
{
	SpaceSolver<DIM,ET,MAPPER>::Create( pcfgsec, pphys, pdata);
}



template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPIPETScSpaceSolver<DIM,ET,MAPPER>::PETScInitOwnedIdRange( pair<MGSize,MGSize>& range, vector<MGSize>& tabg) const
{ 
	range = this->cData().cGrid().cOwnedIds(); 

	tabg.resize( this->cData().cGrid().SizeNodeTab() );
	for ( MGSize i=0; i<tabg.size(); ++i)
		tabg[i] = this->cData().cGrid().cNode(i).cGId();
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPIPETScSpaceSolver<DIM,ET,MAPPER>::PETScResizeVct( Vec& vct) const		
{
	const pair<MGSize,MGSize> range = this->cData().cGrid().cOwnedIds();
	MGSize nnodes = range.second - range.first;

	VecCreate( PETSC_COMM_WORLD, &vct );
	VecSetBlockSize( vct, BSIZE);
	VecSetSizes( vct, BSIZE * nnodes, PETSC_DECIDE);
	VecSetFromOptions( vct);
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPIPETScSpaceSolver<DIM,ET,MAPPER>::PETScResizeMtx( Mat& mtx) const		
{
	vector<vector<MGSize> >	nodeConn( this->cData().cGrid().SizeNodeTab() );
	
	for ( MGSize ic=0; ic<this->cData().cGrid().SizeCellTab(); ++ic)
	{
 		const CGeomCell<DIM>& cgcell = this->cData().cGrid().cCell( ic );
 		for ( MGSize i=0; i<cgcell.Size(); ++i)
		{
 			for ( MGSize j=0; j<cgcell.Size(); ++j)
			{
 // 				if(j==i) continue;                    // poza samym wezlem
 				nodeConn[ cgcell.cId(i) ].push_back( cgcell.cId(j) );
 			}
 		}
	}
	
	for(MGSize in=0; in<nodeConn.size(); ++in)
	{
		sort( nodeConn[in].begin(), nodeConn[in].end() );
		nodeConn[in].erase( unique( nodeConn[in].begin(), nodeConn[in].end() ), nodeConn[in].end() );
	}

	MGSize nownodes = this->cData().cGrid().cOwnedIds().second - this->cData().cGrid().cOwnedIds().first;
	
	vector<PetscInt> diag( nownodes, 0);
	vector<PetscInt> offdiag( nownodes, 0);
	pair<MGSize, MGSize> nodeRange = this->cData().cGrid().cOwnedIds();

	cout << ProcessInfo::Prompt();
	cout << " nodeRange = " << nodeRange.first << " - " << nodeRange.second << endl;

	
	//for ( MGSize in=nodeRange.first; in<nodeRange.second; ++in)
	for ( MGSize locid = 0; locid < this->cData().cGrid().SizeNodeTab(); ++locid)
	{
		MGSize globid = this->cData().cGrid().cNode(locid).cGId();
		MGSize locglobid = globid - nodeRange.first;
		
 		if ( globid < nodeRange.first || globid >= nodeRange.second )
 			continue;
 		
 		for ( MGSize i=0; i<nodeConn[ locid ].size(); ++i)
 		{
 			MGSize globidnxt = this->cData().cGrid().cNode( nodeConn[ locid ][i] ).cGId();
 			
// 			//if( this->mpGridMPI->GetOwnerProcNodeLocalId( nodeConn[ locId ][i] ) == ProcessInfo::cProcId() )
 			if ( globidnxt >= nodeRange.first && globidnxt < nodeRange.second )
 			{
 				++diag[locglobid];
 				continue;
 			}
 			else
 			{
 				++offdiag[locglobid];
 			}
 		}
 		
 		if ( diag[locglobid] == 0 )
 			THROW_INTERNAL( "MPIPETScSpaceSolver<DIM,ET,MAPPER>::PETScResizeMtx -- diag[globid] == 0" );
	}

    //THROW_INTERNAL(" MatCreateMPIBAIJ does not compile on some platforms (?) uncomment to use ");
	//                        mpi_comm, bsize, m-local rows  , n-local columns,M              , N              , d_nz         , d_nnz     , o_nz         , o_nnz
#ifdef WITH_LIB_PETSC_OLD_NAMES
  	MatCreateMPIBAIJ( PETSC_COMM_WORLD, BSIZE, BSIZE*nownodes, BSIZE*nownodes, PETSC_DETERMINE, PETSC_DETERMINE, PETSC_DEFAULT, &(diag[0]), PETSC_DEFAULT, &(offdiag[0]), &(mtx));
#else
    MatCreateBAIJ( PETSC_COMM_WORLD, BSIZE, BSIZE*nownodes, BSIZE*nownodes, PETSC_DETERMINE, PETSC_DETERMINE, PETSC_DEFAULT, &(diag[0]), PETSC_DEFAULT, &(offdiag[0]), &(mtx));
#endif
    
}



template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPIPETScSpaceSolver<DIM,ET,MAPPER>::PETScResNumJacob( Vec& vec, Mat& vmtx, const vector<MGFloat>& tabDT, const bool bcorrdiag)
{
	vector<MGFloat>	tabvol;
	MGFloat	vol;
	CGeomCell<DIM>	cgcell;
	tabvol.resize( this->cData().cGrid().SizeNodeTab(), 0.0);

	for ( MGSize i=0; i< this->cData().cGrid().SizeCellTab(); ++i)
	{
		cgcell = this->cData().cGrid().cCell( i);
		vol = cgcell.Volume() / cgcell.Size();
		for ( MGSize k=0; k<cgcell.Size(); ++k)
			tabvol[cgcell.cId(k)] += vol;
	}

	MGFloat volmin = tabvol[0];
	for ( MGSize i=1; i<tabvol.size(); ++i)
		//if ( volmin > tabvol[i] )
			volmin += tabvol[i];

	volmin /= tabvol.size();

	BVec	tabres[DIM+1];
	BMtx	mtxres[DIM+1][DIM+1];

	CFCell	cfcell;
	CFBFace	cfbface;
	
	pair<MGSize, MGSize> nodeRange = this->cData().cGrid().cOwnedIds();

	MatZeroEntries( vmtx);
	VecZeroEntries( vec);

	PetscInt index[DIM+1];

	PetscScalar y[ (1+DIM) * BSIZE ];
	PetscScalar vv[ BSIZE * BSIZE ];
	
	// boundary conditions
	for ( MGSize i=0; i<this->cData().cGrid().SizeBFaceTab(); ++i)
	{
		this->GetFBFace( cfbface, i);

		this->ResNJacobianBFace( tabres, mtxres, cfbface); //numerical jacobian

		for ( MGSize k=0; k<cfbface.Size(); ++k)
		{
			index[k] = this->cData().cGrid().cNode( cfbface.cId(k) ).cGId();

			if ( index[k] < nodeRange.first || index[k] >= nodeRange.second )
				for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
					y[k*BSIZE+ieq] = 0.0;
 			else	
				for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
					y[k*BSIZE+ieq] = tabres[k](ieq);
					//y[k*BSIZE+ieq] = ( volmin / tabvol[cfbface.cId(k)] ) * tabres[k](ieq);
		}
		
		VecSetValuesBlocked( vec, DIM, index, y, ADD_VALUES);

		for ( MGSize j=0; j<cfbface.Size(); ++j)
			for ( MGSize k=0; k<cfbface.Size(); ++k)
			{
				PetscInt irow = this->cData().cGrid().cNode( cfbface.cId(j) ).cGId();
				PetscInt icol = this->cData().cGrid().cNode( cfbface.cId(k) ).cGId();

				if ( icol < nodeRange.first || icol >= nodeRange.second )
					for( MGSize idx=0, m=0; m<BSIZE; ++m)
						for( MGSize l=0; l<BSIZE; ++l)
						{
							ASSERT( idx < BSIZE*BSIZE );
							vv[idx] = 0.0;
							++idx;
						}
				else
					for( MGSize idx=0, m=0; m<BSIZE; ++m)
						for( MGSize l=0; l<BSIZE; ++l)
						{
							ASSERT( idx < BSIZE*BSIZE );
							vv[idx] = mtxres[j][k](m, l);
							//vv[idx] = ( volmin / tabvol[cfbface.cId(j)] ) * mtxres[j][k](m, l);
							++idx;
						}

				MatSetValuesBlocked( vmtx, 1, &irow, 1, &icol, vv, ADD_VALUES);
			}
	}


	// cell residuals
	for ( MGSize i=0; i<this->cData().cGrid().SizeCellTab(); ++i)
	{
		this->GetFCell( cfcell, i);

		cfcell.CalcVn();

		this->ResNJacobianCell( tabres, mtxres, cfcell); //numerical jacobian

		for ( MGSize k=0; k<cfcell.Size(); ++k)
		{
			index[k] = this->cData().cGrid().cNode( cfcell.cId(k) ).cGId();

			if ( index[k] < nodeRange.first || index[k] >= nodeRange.second )
				for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
					y[k*BSIZE+ieq] = 0.0;
 			else
				for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
					y[k*BSIZE+ieq] = tabres[k](ieq);
					//y[k*BSIZE+ieq] = ( volmin / tabvol[cfcell.cId(k)] ) * tabres[k](ieq);
		}
		
		VecSetValuesBlocked( vec, DIM+1, index, y, ADD_VALUES);

		for ( MGSize j=0; j<cfcell.Size(); ++j)
			for ( MGSize k=0; k<cfcell.Size(); ++k)
			{
				PetscInt irow = this->cData().cGrid().cNode( cfcell.cId(j) ).cGId();
				PetscInt icol = this->cData().cGrid().cNode( cfcell.cId(k) ).cGId();

				if ( icol < nodeRange.first || icol >= nodeRange.second )
					for( MGSize idx=0, m=0; m<BSIZE; ++m)
						for( MGSize l=0; l<BSIZE; ++l)
						{
							ASSERT( idx < BSIZE*BSIZE );
							vv[idx] = 0.0;
							++idx;
						}
				else
					for( MGSize idx=0, m=0; m<BSIZE; ++m)
						for( MGSize l=0; l<BSIZE; ++l)
						{
							ASSERT( idx < BSIZE*BSIZE );
							vv[idx] = mtxres[j][k](m, l);
							//vv[idx] = ( volmin / tabvol[cfcell.cId(j)] ) * mtxres[j][k](m, l);
							++idx;
						}

				MatSetValuesBlocked( vmtx, 1, &irow, 1, &icol, vv, ADD_VALUES);
			}
	}

	//diagonal elements
	if (bcorrdiag)
	{
		for ( MGSize i=0; i<this->cData().cGrid().SizeNodeTab(); ++i)
		{
			MGSize globid = this->cData().cGrid().cNode(i).cGId();

	 		if ( globid < nodeRange.first || globid >= nodeRange.second )
	 			continue;
		
			PetscScalar val = 1.0 /  tabDT[i];
			//PetscScalar val = 1.0 * volmin /  tabDT[i] / tabvol[i];
			PetscInt irow =  BSIZE * globid;
		
			for ( MGSize k=0; k<BSIZE; ++k)
			{
				MatSetValues( vmtx, 1, &irow, 1, &irow, &val, ADD_VALUES);
				++irow;
			}
		}
	}

	MatAssemblyBegin( vmtx, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd( vmtx, MAT_FINAL_ASSEMBLY);

	VecAssemblyBegin( vec);
	VecAssemblyEnd( vec);

// 	PetscViewer    viewer = PETSC_VIEWER_STDOUT_WORLD;
// 	PetscViewerASCIIOpen(PETSC_COMM_WORLD, "mtx_petc.dat", &viewer);
// 	PetscViewerSetFormat(viewer, PETSC_VIEWER_ASCII_DENSE);
// 	VecView(mRHS, viewer);
// 	MatView(mA, viewer );
//
// 	THROW_INTERNAL("STOP");
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPIPETScSpaceSolver<DIM,ET,MAPPER>::PETScResiduum( Vec& vec )
{
	vector<MGFloat>	tabvol;
	MGFloat	vol;
	CGeomCell<DIM>	cgcell;
	tabvol.resize( this->cData().cGrid().SizeNodeTab(), 0.0);

	for ( MGSize i=0; i< this->cData().cGrid().SizeCellTab(); ++i)
	{
		cgcell = this->cData().cGrid().cCell( i);
		vol = cgcell.Volume() / cgcell.Size();
		for ( MGSize k=0; k<cgcell.Size(); ++k)
			tabvol[cgcell.cId(k)] += vol;
	}

	MGFloat volmin = tabvol[0];
	for ( MGSize i=1; i<tabvol.size(); ++i)
		//if ( volmin > tabvol[i] )
			volmin += tabvol[i];

	volmin /= tabvol.size();

	BVec	tabres[DIM+1];
	BMtx	mtxres[DIM+1][DIM+1];

	CFCell	cfcell;
	CFBFace	cfbface;
	
	pair<MGSize, MGSize> nodeRange = this->cData().cGrid().cOwnedIds();
	
	VecZeroEntries( vec);

	PetscInt index[DIM+1];

	PetscScalar y[ (1+DIM) * BSIZE ];
	PetscScalar vv[ BSIZE * BSIZE ];
	
	// boundary conditions
	for ( MGSize i=0; i<this->cData().cGrid().SizeBFaceTab(); ++i)
	{
		this->GetFBFace( cfbface, i);

		this->ResiduumBFace(tabres,cfbface,false);

		for ( MGSize k=0; k<cfbface.Size(); ++k)
		{
			index[k] = this->cData().cGrid().cNode( cfbface.cId(k) ).cGId();

			if ( index[k] < nodeRange.first || index[k] >= nodeRange.second )
				for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
					y[k*BSIZE+ieq] = 0.0;
 			else	
				for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
					y[k*BSIZE+ieq] = tabres[k](ieq);
					//y[k*BSIZE+ieq] = ( volmin / tabvol[cfbface.cId(k)] ) * tabres[k](ieq);
		}
		
		VecSetValuesBlocked( vec, DIM, index, y, ADD_VALUES);

	}


	// cell residuals
	for ( MGSize i=0; i<this->cData().cGrid().SizeCellTab(); ++i)
	{
		this->GetFCell( cfcell, i);

		cfcell.CalcVn();

		this->ResiduumCell( tabres, cfcell, false);

		for ( MGSize k=0; k<cfcell.Size(); ++k)
		{
			index[k] = this->cData().cGrid().cNode( cfcell.cId(k) ).cGId();

			if ( index[k] < nodeRange.first || index[k] >= nodeRange.second )
				for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
					y[k*BSIZE+ieq] = 0.0;
 			else
				for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
					y[k*BSIZE+ieq] = tabres[k](ieq);
					//y[k*BSIZE+ieq] = ( volmin / tabvol[cfcell.cId(k)] ) * tabres[k](ieq);
		}
		
		VecSetValuesBlocked( vec, DIM+1, index, y, ADD_VALUES);
	}

	VecAssemblyBegin( vec);
	VecAssemblyEnd( vec);
	
}

void init_MPIPETScSpaceSolver()
{
	//////////////////////////////////////////////////////////////////////
	// 2D
	
	// Euler
	static ConcCreator<
		MGString,
		MPIPETScSpaceSolver< Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SplitingFull::Block<1> >,
		SpaceSolverBase< Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SplitingFull::Block<1> >
	>	gCreatorMPIPETScSpaceSolverEulerRDS2D( "MPIPETSC_SPACESOL_2D_EULER_RDS");

	// NS
	static ConcCreator<
		MGString,
		MPIPETScSpaceSolver< Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SplitingFull::Block<1> >,
		SpaceSolverBase< Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SplitingFull::Block<1> >
	>	gCreatorMPIPETScSpaceSolverNSRDS2D( "MPIPETSC_SPACESOL_2D_NS_RDS");

	// RANS SA with splitting
	static ConcCreator<
		MGString,
		MPIPETScSpaceSolver< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >,
		SpaceSolverBase< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >
	>	gCreatorMPIPETScSpaceSolverRANS1eqFlowRDS2D( "MPIPETSC_SPACESOL_2D_RANS_SA_RDS");

	static ConcCreator<
		MGString,
		MPIPETScSpaceSolver< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >,
		SpaceSolverBase< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >
	>	gCreatorMPIPETScSpaceSolverRANS1eqTurbRDS2D( "MPIPETSC_SPACESOL_2D_RANS_SA_RDS");

	
	//////////////////////////////////////////////////////////////////////
	// 3D

	// Euler
 	static ConcCreator<
 		MGString,
 		MPIPETScSpaceSolver< Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SplitingFull::Block<1> >,
 		SpaceSolverBase< Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SplitingFull::Block<1> >
 	>	gCreatorMPIPETScSpaceSolverEulerRDS3D( "MPIPETSC_SPACESOL_3D_EULER_RDS");
 
	// NS
	static ConcCreator<
		MGString,
		MPIPETScSpaceSolver< Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SplitingFull::Block<1> >,
		SpaceSolverBase< Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SplitingFull::Block<1> >
	>	gCreatorMPIPETScSpaceSolverNSRDS3D( "MPIPETSC_SPACESOL_3D_NS_RDS");

	// RANS SA with splitting
	static ConcCreator<
		MGString,
		MPIPETScSpaceSolver< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >,
		SpaceSolverBase< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >
	>	gCreatorMPIPETScSpaceSolverRANS1eqFlowRDS3D( "MPIPETSC_SPACESOL_3D_RANS_SA_RDS");

	static ConcCreator<
		MGString,
		MPIPETScSpaceSolver< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >,
		SpaceSolverBase< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >
	>	gCreatorMPIPETScSpaceSolverRANS1eqTurbRDS3D( "MPIPETSC_SPACESOL_3D_RANS_SA_RDS");

}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

