//#ifdef WITH_NLOPT

#ifndef __MPISOLVEROPTIM_H__
#define __MPISOLVEROPTIM_H__

#include "libcoresystem/mgdecl.h"


#include "redvc/libredvccommon/data.h"
#include "redvc/libredvccommon/dataadjoint.h"
#include "redvc/libredvccommon/solveradjoint.h"
#include "redvc/libredvccommon/solveroptim.h"
#include "libnlopt/api/nlopt.hpp"
#include "libcoreconfig/cfgsection.h"
#include "communicator.h"

#include "mpisolveradjoint.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

//////////////////////////////////////////////////////////////////////
// SolverOptim
//////////////////////////////////////////////////////////////////////

template <Dimension DIM, EQN_TYPE ET>
class MPISolverOptim : public SolverBase //MPISolver<DIM,ET>
{
	enum {
		FINISHED = 0,
		OBJECTIVEFUNC,
		CONSTRAINTFUNC
	};
	
	typedef Geom::Vect<DIM>		GVec;
	//typedef SolverBase	TBaseSolver
	typedef SolverBase TBaseSolver;
	
public:
	MPISolverOptim() {};
	virtual ~MPISolverOptim() {};
	
	virtual void	Create( const CfgSection* pcfgsec);
	virtual void	PostCreateCheck() const;
	virtual void	Init();
	
	virtual void	Solve();
	
	MGFloat ObjectiveFunc(const vector<double> &x, vector<double>& grad);
	void	ConstraintFunc(unsigned m, double *result, unsigned n, const double *x, double *grad);
	
protected:

private:
	MPISolverAdjoint<DIM,ET> mMPISolver;
	DataAdjoint<DIM,ET>*	  mpData;
	
	MGFloat mtemph;

	//from SolverOptim class
	MGString mstralgorithm;
	MGSize mdim;
	MGSize mmaxiter;

	MGFloat mtol;
	MGString mstrtolfunc;
	MGString mstrtoltype;
	bool mbistolf;
	bool mbistolrel;

	MGString mstrtype;
	bool mbistypemax;

	MGFloat mlobnd;
	MGFloat mupbnd;
	bool mbislobnd;
	bool mbisupbnd;

	bool mbisineq;		// inequality constraints: FALSE - not specified, TRUE - specified
	bool mbineqtype;	// FALSE - not increase constraint , TRUE - not decrease constraint
	MGFloat	mineqtol;	// tolerance for nlopt inequality constraint
	bool mbineqval;		// FALSE - initial drag, TRUE - value from config
	MGFloat mineqval;	// VALUE (if given in config)

	nlopt::opt	mOpt;
	nlopt::algorithm malg;

	vector<MGFloat> mvecDesParamValue;
	int mitercount;
	ofstream mLog;
	ofstream mOfItercount;
	
};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

#endif //#ifdef __MPISOLVEROPTIM_H__

//#endif //WITH_NLOPT