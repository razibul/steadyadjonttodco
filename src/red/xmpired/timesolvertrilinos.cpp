#include "timesolvertrilinos.h"
#include "libcorecommon/factory.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


void init_MPITrilinosTimeSolverImplicit()
{
    static ConcCreator< MGString, TimeSolverImplicitTrilinos<1>, TimeSolverBase<1> >   gCreatorTimeSolverImplicitTRILINOSBSize1( "TIMESOL_BSIZE_1_TRILINOS");
    static ConcCreator< MGString, TimeSolverImplicitTrilinos<2>, TimeSolverBase<2> >   gCreatorTimeSolverImplicitTRILINOSBSize2( "TIMESOL_BSIZE_2_TRILINOS");
    static ConcCreator< MGString, TimeSolverImplicitTrilinos<4>, TimeSolverBase<4> >   gCreatorTimeSolverImplicitTRILINOSBSize4( "TIMESOL_BSIZE_4_TRILINOS");
    static ConcCreator< MGString, TimeSolverImplicitTrilinos<5>, TimeSolverBase<5> >   gCreatorTimeSolverImplicitTRILINOSBSize5( "TIMESOL_BSIZE_5_TRILINOS");
    static ConcCreator< MGString, TimeSolverImplicitTrilinos<6>, TimeSolverBase<6> >   gCreatorTimeSolverImplicitTRILINOSBSize6( "TIMESOL_BSIZE_6_TRILINOS");
    static ConcCreator< MGString, TimeSolverImplicitTrilinos<7>, TimeSolverBase<7> >   gCreatorTimeSolverImplicitTRILINOSBSize7( "TIMESOL_BSIZE_7_TRILINOS");
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//