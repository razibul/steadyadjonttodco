#include "mpigridassistant.h"
#include "libcorecommon/factory.h"

#include <mpi.h>
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <Dimension DIM>
void MPIGridAssistant<DIM>::Create(const CfgSection* pcfgsec, TGrid* pgrd, DesignParameterCollection<DIM>* pdesparcoll, FreezingPointCollection<DIM>* pfreezeptcoll)
{
	GridAssistant<DIM>::Create( pcfgsec, pgrd, pdesparcoll, pfreezeptcoll);
}
template <Dimension DIM>
void MPIGridAssistant<DIM>::PostCreateCheck() const
{
	GridAssistant<DIM>::PostCreateCheck();
}

template <Dimension DIM>
void MPIGridAssistant<DIM>::Init()
{
	GridAssistant<DIM>::Init();
}

template<Dimension DIM>
void MPIGridAssistant<DIM>::FixNodesBySum(vector<GVec>& vec) const
{
	//works only when freezed nodes aren't close to each other

	vector<MGFloat>	value;	
	// wypelnic value -> wartosc w wezlach zamrazanych
	MGFloat temp_val;
	
	for (MGSize i = 0; i < this->mpFreezePtColl->Size(); i++)
	{
		if (ProcessInfo::cProcId() == (*this->mpFreezePtColl)[i].cOwnerIndx())
		{
			temp_val = vec[ (*this->mpFreezePtColl)[i].cOwnerIndx() ].cY();
		}
		MPI_Bcast( &temp_val, 1, MPI_DOUBLE, (*this->mpFreezePtColl)[i].cOwnerIndx() , MPI_COMM_WORLD );
		
		value.push_back(temp_val);
	}
	
	//MGFloat sigma = 0.1;
	MGFloat sigma = sqrt(1./12);
	
	for ( MGSize i=0; i < vec.size(); i++)
	{
		const GVec pos = this->mpGrid->cNode(i).cPos();
		for (MGSize j = 0; j < this->mpFreezePtColl->Size(); j++)
		{
			const MGFloat dist = this->ComputePerturbation((*this->mpFreezePtColl)[j].cPos(), pos, false);
			const GVec tmpvec = value[j] * dist;

			vec[i] -= tmpvec;
		}
		
	}
	
// 	for (MGSize i = 0; i < indx.size(); i++)
// 		value.push_back(vec[indx[i]].cY());
// 
// 	//MGFloat sigma = 0.1;
// 	MGFloat sigma = sqrt(1./12);
// 
// 	for (MGSize i = 0; i < vec.size(); i++)
// 	{
// 		GVec pos = cNode(i).cPos();
// 		for (MGSize j = 0; j < freeze.size(); j++)
// 		{
// 			MGFloat dist = (pos - freeze[j]).module();
// 			vec[i].rY() -= value[j] * exp(-dist*dist / (2 * sigma*sigma));
// 		}
// 	}

}

// template <Dimension DIM>
// void MPIGridAssistant<DIM>::RevertToInitial()
// {
// 	ASSERT(mpGrid->mtabNodeInitial.size() == mpGrid->mtabNode.size());
// 	ASSERT(mpGrid->mtabBNodeInitial.size() == mpGrid->mtabBNode.size());
// 	
// 	for (MGSize i = 0; i < this->mpGrid->mtabNodeInitial.size(); i++)
// 		this->mpGrid->mtabNode[i] = this->mpGrid->mtabNodeInitial[i];
// 	
// 	for (MGSize i = 0; i < mpGrid->mtabBNodeInitial.size(); i++)
// 		mpGrid->mtabBNode[i] = mpGrid->mtabBNodeInitial[i];
// }

void init_MPIGridAssistant()
{
	static ConcCreator<
		MGString,
		MPIGridAssistant< Geom::DIM_2D >,
		GridAssistant< Geom::DIM_2D >
	> gCreatorMPIGridAssistant2D ( "MPI_GRIDASSISTANT_2D" );
	
	static ConcCreator<
		MGString,
		MPIGridAssistant< Geom::DIM_3D >,
		GridAssistant< Geom::DIM_3D >
	> gCreatorMPIGridAssistant3D ( "MPI_GRIDASSISTANT_3D" );
	
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 
