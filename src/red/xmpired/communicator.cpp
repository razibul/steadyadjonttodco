#include <mpi.h>

#include "communicator.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


MGSize Communicator::InterfSize() const
{
	MGSize n=0;

	for ( MGSize i=0; i<mReceive.mtabProc.size(); ++i)
		n += mReceive.mtabProc[i].mtabNodeId.size();

	return n;
}

void Communicator::Init( const Interface& intf)
{
	InitReceiver( intf);
	InitSender();

	//mSend.Dump( mProcInfo.ModifyFileName( "sdump.txt") );

	cout << "Communicator initialization is finished" << endl << flush;

}


void Communicator::InitReceiver( const Interface& intf)
{
	map<MGSize,MGSize>	maptmp;
	map<MGSize,MGSize>::iterator	itr;

	MGSize k=0;

	cout << "intf.Size() = " << intf.Size() << endl;
	for ( MGSize i=0; i<intf.Size(); ++i)
	{
		InterfInfo info = intf.cInfo( i);
		
		if ( (itr = maptmp.find( info.mIdRemProc ) ) == maptmp.end() )
		{
			mReceive.mtabProc.push_back( CommData( info.mIdRemProc ) );
			maptmp.insert( map<MGSize,MGSize>::value_type( info.mIdRemProc, k) );
			++k;
		}
	}


	for ( MGSize i=0; i<intf.Size(); ++i)
	{
		InterfInfo info = intf.cInfo( i);
		
		if ( (itr = maptmp.find( info.mIdRemProc ) ) == maptmp.end() )
			THROW_INTERNAL( "inconsistent interface data - from Receiver::Init");
		
		MGSize k = (*itr).second;

		mReceive.mtabProc[k].mtabNodeId.push_back( pair<MGSize,MGSize>( info.mIdLocNode, info.mIdRemNode ) );
	}
		
	cout << "numb. of neighbours = " << mReceive.mtabProc.size() << endl;
	for ( MGSize i=0; i<mReceive.mtabProc.size(); ++i)
		cout	<< " procid = " << mReceive.mtabProc[i].mProcId
				<< "; mtabData[k].mtabNodeId.size = " << mReceive.mtabProc[i].mtabNodeId.size() << endl;

}


void Communicator::InitSender()
{
	vector<MGSize>		tabdata( ProcessInfo::cNumProc(), 0 );
	vector<MGSize>		tabdatarec( ProcessInfo::cNumProc(), 0);

	for ( MGSize i=0; i<mReceive.mtabProc.size(); ++i)
		tabdata[ mReceive.mtabProc[i].mProcId ] = mReceive.mtabProc[i].mtabNodeId.size();

	// set up sender communication neighbours and size of interface
	MPI_Alltoall( &tabdata[0], 1, MPI_UNSIGNED, &tabdatarec[0], 1, MPI_UNSIGNED, MPI_COMM_WORLD);


	ostringstream os;
	os << " from " << ProcessInfo::cProcId() << " : ";
	for ( MGSize j=0; j<tabdatarec.size(); ++j)
		os << " " << tabdatarec[j];
	cout << os.str() << endl;


	MGSize k=0;
	for ( MGSize i=0; i< tabdatarec.size(); ++i)
	{
		if ( tabdatarec[i] > 0 )
		{
			mSend.mtabProc.push_back( CommData( i) );
			mSend.mtabProc[k].mtabNodeId.resize( tabdatarec[i] );
			++k;
		}
	}


	// exchange interface data between receiver and sender
	MGInt	tag = 1;
	vector<MPI_Request>	tabreqs( mSend.mtabProc.size() + mReceive.mtabProc.size() );
	vector<MPI_Status>	tabstats( mSend.mtabProc.size() + mReceive.mtabProc.size() );

	for ( MGSize i=0; i<mSend.mtabProc.size(); ++i)
		MPI_Irecv( &mSend.mtabProc[i].mtabNodeId[0], 2*mSend.mtabProc[i].mtabNodeId.size(), MPI_UNSIGNED, mSend.mtabProc[i].mProcId, tag, MPI_COMM_WORLD, &tabreqs[i]);
	
	for ( MGSize i=0; i<mReceive.mtabProc.size(); ++i)
		MPI_Isend( &mReceive.mtabProc[i].mtabNodeId[0], 2*mReceive.mtabProc[i].mtabNodeId.size(), MPI_UNSIGNED, mReceive.mtabProc[i].mProcId, tag, MPI_COMM_WORLD, &tabreqs[i+mSend.mtabProc.size()]);

	MPI_Waitall( tabreqs.size(), &tabreqs[0], &tabstats[0]);

	for ( MGSize i=0; i<mSend.mtabProc.size(); ++i)
	{
		for ( MGSize k=0; k<mSend.mtabProc[i].mtabNodeId.size(); ++k)
		{
			MGSize tmp = mSend.mtabProc[i].mtabNodeId[k].first;
			mSend.mtabProc[i].mtabNodeId[k].first = mSend.mtabProc[i].mtabNodeId[k].second;
			mSend.mtabProc[i].mtabNodeId[k].second = tmp;
		}
	}


}


MGFloat Communicator::CollectEps( const MGFloat& eps)
{
	vector<MGFloat>		tabdata( ProcessInfo::cNumProc(), 0.0 );
	vector<MGFloat>		tabdatarec( ProcessInfo::cNumProc(), 0.0);

	for ( MGSize i=0; i<tabdata.size(); ++i)
		tabdata[i] = eps;

	MPI_Alltoall( &tabdata[0], 1, MPI_DOUBLE, &tabdatarec[0], 1, MPI_DOUBLE, MPI_COMM_WORLD);

	vector<MGFloat>::iterator itr = max_element( tabdatarec.begin(), tabdatarec.end() );

	return (*itr);
}



void Communicator::SetTabDNode( vector<MGSize>& tab) const
{
	MGSize	size = 0;
	for ( MGSize i=0; i<mReceive.mtabProc.size(); ++i)
		size += mReceive.mtabProc[i].mtabNodeId.size();

	tab.reserve( size);

	for ( MGSize i=0; i<mReceive.mtabProc.size(); ++i)
		for ( MGSize k=0; k<mReceive.mtabProc[i].mtabNodeId.size(); ++k)
			tab.push_back( mReceive.mtabProc[i].mtabNodeId[k].first );
}



void Communicator::ResizeData( IO::SolFacade& solfac)	
{
	MGSize	nblock = solfac.IOBlockSize();

	for ( MGSize i=0; i<mSend.mtabProc.size(); ++i)
		mSend.mtabProc[i].mtabData.resize( nblock * mSend.mtabProc[i].mtabNodeId.size() );

	for ( MGSize i=0; i<mReceive.mtabProc.size(); ++i)
		mReceive.mtabProc[i].mtabData.resize( nblock * mReceive.mtabProc[i].mtabNodeId.size() );
}


void Communicator::GetData( IO::SolFacade& solfac)		
{
	//cout << "from MPI communicator : GetData" << endl << flush;

	MGSize	nblock = solfac.IOBlockSize();
	vector<MGFloat>	tab(nblock);

	for ( MGSize i=0; i<mReceive.mtabProc.size(); ++i)
	{
		for ( MGSize k=0; k<mReceive.mtabProc[i].mtabNodeId.size(); ++k)
		{
			MGSize id = mReceive.mtabProc[i].mtabNodeId[k].first;

			for ( MGSize j=0; j<nblock; ++j)
				tab[j] = mReceive.mtabProc[i].mtabData[j + k*nblock];

			solfac.IOSetBlockVector( tab, id);
		}
	}

}

void Communicator::SetData( IO::SolFacade& solfac)		
{
// 	cout << "from MPI communicator : SetData" << endl << flush; 

	MGSize	nblock = solfac.IOBlockSize();
	vector<MGFloat>	tab(nblock);

//  	cout << ProcessInfo::Prompt() << "nblock = " << nblock << endl;
//  	cout << ProcessInfo::Prompt() << "mSend.mtabProc.size() = " << mSend.mtabProc.size() << endl;

	
	for ( MGSize i=0; i<mSend.mtabProc.size(); ++i)
	{
//  		cout << ProcessInfo::Prompt() << "mSend.mtabProc[i].mtabData.size() = " << mSend.mtabProc[i].mtabData.size() << endl;
//  		cout << ProcessInfo::Prompt() << "mSend.mtabProc[i].mtabNodeId.size() = " << mSend.mtabProc[i].mtabNodeId.size() << endl;

		for ( MGSize k=0; k<mSend.mtabProc[i].mtabNodeId.size(); ++k)
		{
			MGSize id = mSend.mtabProc[i].mtabNodeId[k].first;

			solfac.IOGetBlockVector( tab, id);

 			for ( MGSize j=0; j<nblock; ++j)
 				mSend.mtabProc[i].mtabData[j + k*nblock] = tab[j];
		}
	}
}

void Communicator::ExchangeData()	
{ 
	//cout << "from MPI communicator" << endl << flush; 

	static MGInt	tag = 0;
	if ( ++tag > 10000 )
		tag = 1;
	
	vector<MPI_Request>	tabreqs( mSend.mtabProc.size() + mReceive.mtabProc.size() );
	vector<MPI_Status>	tabstats( mSend.mtabProc.size() + mReceive.mtabProc.size() );

	for ( MGSize i=0; i<mReceive.mtabProc.size(); ++i)
		MPI_Irecv( &mReceive.mtabProc[i].mtabData[0], mReceive.mtabProc[i].mtabData.size(), MPI_DOUBLE, mReceive.mtabProc[i].mProcId, tag, MPI_COMM_WORLD, &tabreqs[i]);

	for ( MGSize i=0; i<mSend.mtabProc.size(); ++i)
		MPI_Isend( &mSend.mtabProc[i].mtabData[0], mSend.mtabProc[i].mtabData.size(), MPI_DOUBLE, mSend.mtabProc[i].mProcId, tag, MPI_COMM_WORLD, &tabreqs[i+mReceive.mtabProc.size()]);
	

	MPI_Waitall( tabreqs.size(), &tabreqs[0], &tabstats[0]);
	//MPI_Barrier( MPI_COMM_WORLD);

	//mReceive.Dump( mProcInfo.ModifyFileName( "receive.dump") );
	//mSend.Dump( mProcInfo.ModifyFileName( "send.dump") );

	//THROW_INTERNAL("");
}


void CommDataTab::Dump( const MGString& name)
{
	ofstream of( name.c_str() );

	for ( MGSize ip=0; ip<mtabProc.size(); ++ip)
	{
		MGSize nblock = mtabProc[ip].mtabData.size() / mtabProc[ip].mtabNodeId.size();


		of << "---------------------------------" << endl;
		of << "proc id = " << mtabProc[ip].mProcId << endl;
		of << mtabProc[ip].mtabNodeId.size() << endl;
		of << mtabProc[ip].mtabData.size() << endl;
		of << nblock << endl;

		

		for ( MGSize i=0; i<mtabProc[ip].mtabNodeId.size(); ++i)
		{
			of <<  mtabProc[ip].mtabNodeId[i].first << "  -  " << mtabProc[ip].mtabNodeId[i].second << endl;
		}

		for ( MGSize i=0; i<mtabProc[ip].mtabNodeId.size(); ++i)
		{
			for ( MGSize k=0; k<nblock; ++k)
				of << mtabProc[ip].mtabData[i*nblock + k] << " ";
			of << endl;
		}
	}
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


