#ifndef __MPIPETSC_SPACESOLVER_H__
#define __MPIPETSC_SPACESOLVER_H__


#include "redvc/libredvccommon/spacesolver.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

//////////////////////////////////////////////////////////////////////
//	class MPIPETScSpaceSolver
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
class MPIPETScSpaceSolver : public SpaceSolver<DIM,ET,MAPPER>
{
public:
	enum { ESIZE = EquationDef<DIM,ET>::SIZE };
	enum { ASIZE = EquationDef<DIM,ET>::AUXSIZE };
	enum { BSIZE = MAPPER::VBSIZE };

	typedef CFlowCell<DIM,ESIZE,ASIZE>		CFCell;
	typedef CFlowBFace<DIM,ESIZE,ASIZE>		CFBFace;

	typedef typename EquationDef<DIM,ET>::FVec	EVec;
	typedef typename EquationDef<DIM,ET>::FMtx	EMtx;

	typedef typename EquationTypes<DIM,BSIZE>::Vec	BVec;
	typedef typename EquationTypes<DIM,BSIZE>::Mtx	BMtx;



	static MGString	Info()
	{
		ostringstream os;
		os << "MPISpaceSolver< " << Geom::DimToStr(DIM) << ", " << EquationDef<DIM,ET>::NameEqnType() << ", " << BSIZE << " >";
		return os.str();
	}

	MPIPETScSpaceSolver() : SpaceSolver<DIM,ET,MAPPER>()		{}
	//virtual ~MPIPETScSpaceSolver()	{};

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata);
	virtual void	PostCreateCheck() const;
	virtual void	Init();

	// PETSc
	virtual void	PETScInitOwnedIdRange( pair<MGSize,MGSize>& range, vector<MGSize>& tabg) const;

	virtual void	PETScResizeVct( Vec& vct) const;
	virtual void	PETScResizeMtx( Mat& mtx) const;

	virtual void	PETScResiduum( Vec& vec );
	virtual void	PETScResNumJacob( Vec& vec, Mat& vmtx, const vector<MGFloat>& tabDT, const bool bcorrdiag = true);

	virtual void	PETScResJacobStrongBC( Vec& vec, Mat& vmtx )		{}
	virtual void	PETScApplyStrongBC( Vec& vec )		{}
	
};


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void MPIPETScSpaceSolver<DIM,ET,MAPPER>::PostCreateCheck() const
{
	SpaceSolver<DIM,ET,MAPPER>::PostCreateCheck();

}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
inline void MPIPETScSpaceSolver<DIM,ET,MAPPER>::Init()
{
	SpaceSolver<DIM,ET,MAPPER>::Init();
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


#endif // __MPIPETSC_SPACESOLVER_H__

