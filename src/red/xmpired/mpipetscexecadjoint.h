#ifndef __MPIPETSCADJOINT_H__
#define __MPIPETSCADJOINT_H__

#include "redvc/libredvccommon/execadjointbase.h"
#include <petscconf.h>
#include <petscksp.h>

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
class MPIPETScExecAdjoint : public ExecAdjointBase<DIM,ET,MAPPER>
{
public:
	typedef EquationDef<DIM, ET> EqDef;
	
	enum { ESIZE = EqDef::SIZE };
	enum { ASIZE = EqDef::AUXSIZE };
	enum { BSIZE = MAPPER::VBSIZE };
	
	typedef Geom::Vect<DIM>			GVec;
	
	MPIPETScExecAdjoint() {};
	virtual ~MPIPETScExecAdjoint() {};
	
	virtual void	Create(const CfgSection* pcfgsec, Physics<DIM, ET>* pphys, Data<DIM, ET>* pdata);
// 	virtual void	Create(const CfgSection* pcfgsec, Physics<DIM, ET>* pphys, Data<DIM, ET>* pdata, const CfgSection* pcfgsectimsol, const CfgSection* pcfgsecspacesol);
	virtual void	PostCreateCheck() const;
	virtual void	Init();
	virtual bool	SolveStep( ConvergenceInfo<ESIZE>& info);
	virtual bool	SolveFinal();
	
private:
	void	ResiduumGradSol();
	void	SolveAdjointEqn() {};
	void	SolveTangentEqn() {};

	void	SolveAdjointEqn(Vec vecRHS, Vec& vecX);
	void	SolveTangentEqn(Vec vecRHS, Vec& vecX);

	void	SolveHessian();
	void	PerturbateSolution(Vec vec, MGFloat scale = 1);

	void	ResiduumGradDesign() {};
	void	ResiduumGradDesign(const MGSize& i, Vec& vec);
	MGFloat	EvaluateFunctional(const Solution<DIM,ET>& vec, const MGSize& i);
	void	LocalToGlobal();
	
	void	ObjectiveGradSol(const MGSize& i);
	void	CheckVecNAN(Vec& invec, const MGString& str);
	
	GVec	mDir;
	
	// ownership info
	pair<MGSize,MGSize>	mRange;
	vector<MGSize>		mtabGlobId;
	
	KSP		mKsp;
	
	Mat		mmtxResGradSol; //Jacobi Matrix
	//Mat		mmtxResGradSolTrans; //Jacobi Matrix
	PC		mPC; //precond matrix
	
	vector<Vec>	mvecvecAdj;
	Vec		mvecAdj;
	Vec 	mvecFuncGradSol;
	
	MGFloat mObjective;
	
	
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

#endif //__MPIPETSCADJOINT_H__BuildJacobiMatrix();
	