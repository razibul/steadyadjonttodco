#ifndef __MPI_SOLVER_H__
#define __MPI_SOLVER_H__


#include "redvc/libredvccommon/solver.h"
#include "communicator.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


using namespace Geom;


//////////////////////////////////////////////////////////////////////
// MPISolver
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, EQN_TYPE ET>
class MPISolver : public Solver<DIM,ET>
{
public:
	typedef Solver<DIM,ET>	TBaseSolver;
	
	MPISolver() : TBaseSolver()		{}
	virtual ~MPISolver()			{}

	virtual void	Create( const CfgSection* pcfgsec);
	virtual void	PostCreateCheck() const;
	virtual void	Init();

	virtual void	Solve();

private:
	Communicator			mComm;
	//PeriodicInterface<DIM>	mPrdIntf;
};
//////////////////////////////////////////////////////////////////////



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

#endif // __MPI_SOLVER_H__

