#ifndef __MPI_GRIDINFO_H__
#define __MPI_GRIDINFO_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libredcore/processinfo.h"

#include "redvc/libredvcgrid/gridsimplex.h"
#include "libredcore/interface.h"

#include "communicator.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


template <Dimension DIM>
class MPIGridInfo
{
public:
	typedef GridSimplex<DIM> TGrid;
    
    MPIGridInfo( const TGrid& grid, const Communicator& comm) : mGrid(grid), mComm(comm)		{}

	const vector<MGSize>&		cTabId() const		{ return mtabIdGlob; }
	const pair<MGSize,MGSize>&	cOwnedIds() const	{ return mrangeOwnedIds; }

	void BuildGlobId();

	
protected:
	void ComputeRanges( vector<MGSize>& vtxdist);
	//const void PrintNodes() const;

private:
	const TGrid&			mGrid;
	const Communicator& 	mComm;
	
	pair<MGSize,MGSize>	mrangeOwnedIds;
	vector<MGSize>		mtabIdGlob;
};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


#endif // __MPI_GRIDINFO_H__
