#ifndef __COMMUNICATOR_H__
#define __COMMUNICATOR_H__

#include <mpi.h>

#include "libcoresystem/mgdecl.h"
#include "libredcore/processinfo.h"
#include "libredcore/interface.h"
#include "libcoreio/solfacade.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class CommData
//////////////////////////////////////////////////////////////////////
class CommData
{
public:
	CommData( const MGSize& pid) : mProcId(pid)		{}


	MGSize	mProcId;

	vector< pair<MGSize,MGSize> >	mtabNodeId;		// idlocal, idremote
	vector<MGFloat>					mtabData;
};

////////////////////////////////////////////////////////////////////////
//// class CommDataPeriodic
////////////////////////////////////////////////////////////////////////
//class CommDataPeriodic
//{
//public:
//	CommDataPeriodic(const MGSize& pid) : mProcId(pid)		{}
//
//
//	MGSize	mProcId;
//
//	vector< pair<MGSize, MGSize> >	mtabNodeId;		// idlocal, idremotecell
//	vector<MGFloat>					mtabData;
//};

//////////////////////////////////////////////////////////////////////
// class CommDataTab
//////////////////////////////////////////////////////////////////////
class CommDataTab
{
friend class Communicator;
	
public:
	void	Dump( const MGString& name);

private:
	vector<CommData>	mtabProc;
};






//////////////////////////////////////////////////////////////////////
// class Communicator
//////////////////////////////////////////////////////////////////////
class Communicator
{
public:
	Communicator()		{}

	void	Init( const Interface& intf);
	void	SetTabDNode( vector<MGSize>& tab) const;


	void	InitReceiver( const Interface& intf);
	void	InitSender();

	void	InitReceiverPrd(const Interface& intf);
	void	InitSenderPrd();

	MGSize	InterfSize() const;

	void	ResizeData( IO::SolFacade& solfac);
	void	GetData( IO::SolFacade& solfac);
	void	SetData( IO::SolFacade& solfac);
	void	ExchangeData();
	MGFloat	CollectEps( const MGFloat& eps);

	const vector<CommData>&	cTabSend() const		{ return mSend.mtabProc; }
	const vector<CommData>&	cTabReceive() const		{ return mReceive.mtabProc; }
	
protected:
	CommDataTab	mSend;
	CommDataTab	mReceive;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __COMMUNICATOR_H__
