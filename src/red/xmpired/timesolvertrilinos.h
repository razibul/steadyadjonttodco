#ifndef __TIMESOLVERIMPLICITTRILINOS_H__
#define __TIMESOLVERIMPLICITTRILINOS_H__

#include "libredcore/processinfo.h"
#include "libredconvergence/timesolverbase.h"

#include "libredcore/configconst.h"
#include "libredconvergence/cflmanager.h"

/*<< Trilinos stuff*/
#include "AztecOO.h"

#include "Epetra_MpiComm.h"
#include "Epetra_BlockMap.h"
#include "Epetra_CrsGraph.h"

#include "Epetra_VbrMatrix.h"
#include "Epetra_VbrRowMatrix.h"

#include "Epetra_MultiVector.h"
#include "Epetra_LinearProblem.h"

#include "Teuchos_ParameterList.hpp"

#include "Ifpack_ConfigDefs.h"
#include "Ifpack.h"
#include "Ifpack_AdditiveSchwarz.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
//  class TimeSolverImplicit
//////////////////////////////////////////////////////////////////////
template <MGSize BSIZE>
class TimeSolverImplicitTrilinos : public TimeSolverBase<BSIZE>
{
    typedef typename Sparse::Vector<BSIZE>::BlockVec    BlockVec;
    
public:
    TimeSolverImplicitTrilinos() :
        mp_epetra_mpi_comm(NULL),
        mp_epetra_map(NULL),
        mp_epetra_csr_graph(NULL),
        mp_A(NULL),
        mp_DQ(NULL),
        mp_RHS(NULL),
        TimeSolverBase<BSIZE>() {}
    virtual ~TimeSolverImplicitTrilinos()
    {
        if(mp_epetra_mpi_comm != NULL) delete mp_epetra_mpi_comm;
        if(mp_epetra_csr_graph != NULL) delete mp_epetra_csr_graph;
        if(mp_epetra_map != NULL) delete mp_epetra_map;
        if(mp_A != NULL) delete mp_A;
        if(mp_DQ != NULL) delete mp_DQ;
        if(mp_RHS != NULL) delete mp_RHS;
    }

    virtual void    Create( const CfgSection* pcfgsec, SpaceSolverFacade<BSIZE>* pssol);
    virtual void    PostCreateCheck() const;
    virtual void    Init();

    virtual void    DoStep( ConvergenceInfo<BSIZE>& info);

protected:
    void    VecBlockNorm( Vec& vec, BlockVec& res);
    
private:
    vector<MGFloat> mtabLocDT;

    CFLManager      mCFLManager;

    // ownership info
    pair<MGSize,MGSize> mRange;
    vector<MGSize>      mtabGlobId;

    // Trilinos stuff << There seems not to be empty defoult constructors ...
    Epetra_MpiComm   * mp_epetra_mpi_comm;
    Epetra_BlockMap  * mp_epetra_map;
    Epetra_CrsGraph  * mp_epetra_csr_graph;
    Epetra_VbrMatrix * mp_A;
    Epetra_Vector    * mp_DQ;
    Epetra_Vector    * mp_RHS;
};


template <MGSize BSIZE>
void TimeSolverImplicitTrilinos<BSIZE>::Create( const CfgSection* pcfgsec, SpaceSolverFacade<BSIZE>* pssol)
{
    TimeSolverBase<BSIZE>::Create( pcfgsec, pssol);
}

template <MGSize BSIZE>
void TimeSolverImplicitTrilinos<BSIZE>::PostCreateCheck() const
{
    TimeSolverBase<BSIZE>::PostCreateCheck();

    // do check
}


template <MGSize BSIZE>
void TimeSolverImplicitTrilinos<BSIZE>::Init()
{
    const MGSize nsol = this->cSpaceSol().SolutionSize();

    this->mvecQ.Resize( nsol);
    mtabLocDT.resize( nsol);


    MGSize nstart = 0;
    if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Executor::TimeSol::NCFLSTART ) )
        nstart = this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::TimeSol::NCFLSTART );


    mCFLManager.Init( this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::TimeSol::CFL),
                      this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::TimeSol::MAXCFL ),
                      this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::TimeSol::NCFLGROWTH ),
                      nstart);

    mCFLManager.Reset();

    ////-----Initialize Epetra stuff
    this->mp_epetra_mpi_comm = new Epetra_MpiComm( MPI_COMM_WORLD );
    this->rSpaceSol().TRILINOSInitOwnedIdRange( mRange, mtabGlobId );

    this->mp_epetra_map = new Epetra_BlockMap( 0, mRange.second - mRange.first, BSIZE, 0, *mp_epetra_mpi_comm );
    this->mp_DQ = new Epetra_Vector( *mp_epetra_map );
    this->mp_RHS = new Epetra_Vector( *mp_epetra_map ); this->mp_RHS->PutScalar(0.0);

    this->rSpaceSol().TRILINOSResizeGraph(*mp_epetra_map, mp_epetra_csr_graph);
    //
    //     KSPCreate( PETSC_COMM_WORLD, &mKsp );
    //     //KSPSetOperators( mKsp, mA, mA, SAME_NONZERO_PATTERN );
    //     KSPSetOperators( mKsp, mA, mA, DIFFERENT_NONZERO_PATTERN );
}


template <MGSize BSIZE>
void TimeSolverImplicitTrilinos<BSIZE>::VecBlockNorm( Vec& vec, BlockVec& res)
{
    THROW_INTERNAL(" not implemented ");
}


template <MGSize BSIZE>
void TimeSolverImplicitTrilinos<BSIZE>::DoStep( ConvergenceInfo<BSIZE>& info)
{
    THROW_INTERNAL(" not implemented ");
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

#endif // __TIMESOLVERIMPLICITTRILINOS_H__

