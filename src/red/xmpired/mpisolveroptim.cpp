#include "mpisolveroptim.h"

#include "libcorecommon/factory.h"

#include "mpigridinfo.h"
#include "libcoreio/writetec.h"
#include "libcoreio/writetecsurf.h"
#include "libcoreio/writemsh2.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


template <Dimension DIM, EQN_TYPE ET>
MGFloat gObjectiveFunc(const vector<double> &x, vector<double>& grad, void *data)
{
	MPISolverOptim<DIM,ET>* solopt = static_cast< MPISolverOptim<DIM,ET>* > (data);
	return solopt->ObjectiveFunc(x, grad);

}

template <Dimension DIM, EQN_TYPE ET>
void gInequalityConstraint(unsigned m, double *result, unsigned n, const double *x, double *grad, void *data)
{
	MPISolverOptim<DIM, ET>* solopt = static_cast< MPISolverOptim<DIM, ET>* > (data);
	return solopt->ConstraintFunc(m,result,n,x,grad);
	
}

template <Dimension DIM, EQN_TYPE ET>
void MPISolverOptim<DIM,ET>::Create( const CfgSection* pcfgsec )
{
	MPI_Barrier( MPI_COMM_WORLD );
	cout << ProcessInfo::Prompt() << "MPISolverOptim::Create()" << endl;
	MPI_Barrier( MPI_COMM_WORLD );
	
	TBaseSolver::Create( pcfgsec);
	mMPISolver.Create( pcfgsec);
	mpData = &( mMPISolver.rData() );
	
	//from SolverOptim class
	// config
	mstralgorithm = this->ConfigSect().ValueString(ConfigStr::Solver::Optimizer::ALGORITHM);
	mstrtolfunc = this->ConfigSect().ValueString(ConfigStr::Solver::Optimizer::TolFunc::KEY);
	mstrtoltype = this->ConfigSect().ValueString(ConfigStr::Solver::Optimizer::TolType::KEY);
	mstrtype = this->ConfigSect().ValueString(ConfigStr::Solver::Optimizer::Type::KEY);
	mtol = this->ConfigSect().ValueFloat(ConfigStr::Solver::Optimizer::TOL);
	
	// bounds 
	if (this->ConfigSect().KeyExists(ConfigStr::Solver::Optimizer::LOBND))
	{
		mbislobnd = true;
		mlobnd = this->ConfigSect().ValueFloat(ConfigStr::Solver::Optimizer::LOBND);
	}
	else
	{
		mbislobnd = false;
		mlobnd = 0;
	}

	if (this->ConfigSect().KeyExists(ConfigStr::Solver::Optimizer::UPBND))
	{
		mbisupbnd = true;
		mupbnd = this->ConfigSect().ValueFloat(ConfigStr::Solver::Optimizer::UPBND);
	}
	else
	{
		mbisupbnd = false;
		mupbnd = 0;
	}

	// dimension of optimization problem -> number of design params
	mdim = this->ConfigSect().ValueSize(ConfigStr::Solver::Optimizer::OPT_DIM);

	// algorithm initialization
	if (strcmp(mstralgorithm.c_str(), "MMA") == 0)
		malg = nlopt::LD_MMA;
	else if (strcmp(mstralgorithm.c_str(), "LBFGS") == 0)
		malg = nlopt::LD_LBFGS;
	else THROW_INTERNAL("Optimizer::Bad NLOpt algorithm!");

	//max. iterations
	mmaxiter = this->ConfigSect().ValueSize(ConfigStr::Solver::Optimizer::MAXITER);

	// stop criteria
	if (strcmp(mstrtolfunc.c_str(), ConfigStr::Solver::Optimizer::TolFunc::Value::FTOL) == 0)
		mbistolf = true; // ftol
	else mbistolf = false; // xtol

	if (strcmp(mstrtoltype.c_str(), ConfigStr::Solver::Optimizer::TolType::Value::REL) == 0)
		mbistolrel = true;
	else mbistolrel = false;

	//inequality constraints 
	mbisineq = false;
	const CfgSection& ineqsect = this->ConfigSect().GetSection(ConfigStr::Solver::Optimizer::Inequality::KEY);
	if (ineqsect.KeyExists( ConfigStr::Solver::Optimizer::Inequality::Type::KEY))
		mbisineq = true;
		
	if ( ineqsect.KeyExists( ConfigStr::Solver::Optimizer::Inequality::Type::KEY ) )
	{	
		if ( strcmp( ineqsect.ValueString( ConfigStr::Solver::Optimizer::Inequality::Type::KEY).c_str(), 
						ConfigStr::Solver::Optimizer::Inequality::Type::Value::NOTINCREASE ) == 0 )
			mbineqtype = false;
		else if ( strcmp( ineqsect.ValueString(ConfigStr::Solver::Optimizer::Inequality::Type::KEY).c_str(),
							ConfigStr::Solver::Optimizer::Inequality::Type::Value::NOTDECREASE) == 0)
			mbineqtype = true;
		else
			THROW_INTERNAL("MPISolverOptim<DIM,ET>::Create() - Bad inequality type ( NOTINCREASE / NOTDECREASE ) ");
	}
	
	
	if (ineqsect.KeyExists(ConfigStr::Solver::Optimizer::Inequality::TOL))
		mineqtol = ineqsect.ValueFloat(ConfigStr::Solver::Optimizer::Inequality::TOL);
	else
		mineqtol = 1e-8; //some arbitrary value, set without any investigation

	if (ineqsect.KeyExists(ConfigStr::Solver::Optimizer::Inequality::Value::KEY))
	{
		mbineqval = true;
		mineqval = ineqsect.ValueFloat(ConfigStr::Solver::Optimizer::Inequality::Value::KEY);
	}
	else mbineqval = false;
	
	
	mtemph = this->ConfigSect().ValueFloat(ConfigStr::Solver::Optimizer::TEMPH);
	
}

template <Dimension DIM, EQN_TYPE ET>
void MPISolverOptim<DIM,ET>::PostCreateCheck() const
{
	MPI_Barrier( MPI_COMM_WORLD );
	cout << ProcessInfo::Prompt() << "MPISolverOptim::PostCreateCheck()" << endl;
	MPI_Barrier( MPI_COMM_WORLD );
	
	TBaseSolver::PostCreateCheck();
	mMPISolver.PostCreateCheck();
	
}

template <Dimension DIM, EQN_TYPE ET>
void MPISolverOptim<DIM,ET>::Init()
{
	MPI_Barrier( MPI_COMM_WORLD );
	cout << ProcessInfo::Prompt() << "MPISolverOptim::Init()" << endl;
	MPI_Barrier( MPI_COMM_WORLD );
	
	mMPISolver.Init();
	
	mOpt = nlopt::opt( malg, mdim );
	
	if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Optimizer::MAXITER ) )
		mOpt.set_maxeval( mmaxiter );

	if ( mbislobnd )
		mOpt.set_lower_bounds( mlobnd );
	if ( mbisupbnd )
		mOpt.set_upper_bounds( mupbnd );

	if ( mbistolf )
		mbistolrel ? mOpt.set_ftol_rel(mtol) : mOpt.set_ftol_abs(mtol);
	else
		mbistolrel ? mOpt.set_xtol_rel(mtol) : mOpt.set_xtol_abs(mtol);

	//type: min / max 
	if (strcmp(mstrtype.c_str(), ConfigStr::Solver::Optimizer::Type::Value::MAX) == 0)
		mOpt.set_max_objective(gObjectiveFunc<DIM,ET>, this);
	if (strcmp(mstrtype.c_str(), ConfigStr::Solver::Optimizer::Type::Value::MIN) == 0)
		mOpt.set_min_objective(gObjectiveFunc<DIM,ET>, this);

	if (mMPISolver.cData().cObjectiveCollection().Size() == 2) 
	{
		vector<MGFloat> tol_constr;
		tol_constr.push_back(mineqtol);

		mOpt.add_inequality_mconstraint( gInequalityConstraint<DIM,ET>, this, tol_constr);
	}


	vector<MGFloat> tmp = mpData->rDesignParamCollection().cVecValues();
	ASSERT(mdim == tmp.size());
	mvecDesParamValue.resize(tmp.size(), 0.0);
	for (MGSize i = 0; i < tmp.size(); i++)
		mvecDesParamValue[i] = tmp[i];

	mitercount = 0;
	if ( ProcessInfo::cProcId() == 0)
	{
		mLog.open("_optim_conv.txt", ios::out );
		if (!mLog.is_open())
		{
			THROW_INTERNAL("MPISolverOptim<DIM,ET>::Init() - log file not opened.")
		}
		
		mLog << mdim << endl;
		mOfItercount.open("_itcount.txt");
	}
	
	
	
}
// 
template <Dimension DIM, EQN_TYPE ET>
void MPISolverOptim<DIM,ET>::Solve()
{
	MPI_Barrier( MPI_COMM_WORLD );
	cout << ProcessInfo::Prompt() << "MPISolverOptim::Solve()" << endl;
	MPI_Barrier( MPI_COMM_WORLD );
	
	MGFloat optim_value;
	if ( ProcessInfo::cProcId() == 0)
	{

		if ((!this->ConfigSect().KeyExists(ConfigStr::Solver::HESSIAN)) || (!this->ConfigSect().ValueBool(ConfigStr::Solver::HESSIAN)))
		{
			cout << "######################################################" << endl;
			cout << "## STARTING OPTIMIZATION PROCESS ##" << endl;
			cout << "######################################################" << endl;

			nlopt::result result = mOpt.optimize(mvecDesParamValue, optim_value);

			MGInt value = FINISHED;
			MPI_Bcast(&value, 1, MPI_INT, 0, MPI_COMM_WORLD);
			
			cout << "###########################################" << endl;
			cout << "optimization -- finished" << endl;
			cout << "final objective value: " << optim_value << endl;
			cout << "design parameters:" << endl;
			for (MGSize i = 0; i < mvecDesParamValue.size(); i++)
				cout << i << " " << mvecDesParamValue[i] << endl;
			
			cout << "nlopt result: " << result << endl;	
		}
		else
		{
			if (this->ConfigSect().ValueBool(ConfigStr::Solver::HESSIAN_FD2ND))
			{

				cout << "######################################################" << endl;
				cout << "## TESTING 2ND DERIVATIVE      ##" << endl;
				cout << "######################################################" << endl;

				MGFloat d2f = 0.0;
				MGFloat d2grad = 0.0;

				MGFloat objzero = 0.0;
				MGFloat objplus = 0.0;
				MGFloat objminus = 0.0;
				
				MGFloat gradplus = 0.0;
				MGFloat gradminus = 0.0;

				MGFloat gradzero = 0.0;
				MGFloat d1grad = 0.0;

				vector<MGFloat> vecgrad(mvecDesParamValue.size(), 0.0);
				int i = 0; //design parameter number				

				cout << "######################################################" << endl;
				cout << "## d2f/ds2(s)   zero calculation: " << endl;
				cout << "######################################################" << endl;
				objzero = ObjectiveFunc(mvecDesParamValue, vecgrad);
				gradzero = vecgrad[i];

				cout << "######################################################" << endl;
				cout << "## d2f/ds2(s+h) forward calculation: " << endl;
				cout << "######################################################" << endl;
				

				MGFloat h = mtemph;
				mvecDesParamValue[i] += h;
				objplus = ObjectiveFunc(mvecDesParamValue, vecgrad);
				gradplus = vecgrad[i];
				mvecDesParamValue[i] -= h;
				
				cout << "######################################################" << endl;
				cout << "## d2f/ds2(s-h) backward calculation: " << endl;
				cout << "######################################################" << endl;
				mvecDesParamValue[i] -= h;
				objminus = ObjectiveFunc(mvecDesParamValue, vecgrad);
				gradminus = vecgrad[i];
				mvecDesParamValue[i] += h;



				d2f = (objplus - 2*objzero + objminus)/ ( h * h );
				d2grad = (gradplus - gradminus) / (2*h);

				d1grad = (objplus - objminus) / (2 * h);

				cout << "######################################################" << endl;
				cout << "## d2f/ds2 (objective)      value: " << setw(23) << setprecision(16) << d2f << endl;
				cout << "## d2f/ds2 (gradient-based) value: " << setw(23) << setprecision(16) << d2grad << endl;
				cout << "## d2f/ds2 relative_error   value: " << setw(23) << setprecision(16) << fabs(d2f - d2grad) / d2grad << endl;
				cout << "######################################################" << endl;
				cout << "## GRADIENT VERIFICATION (objective based) " << endl;
				cout << "## df/ds (objective-based) value: " << setw(23) << setprecision(16) << d1grad << endl;
				cout << "## df/ds (adjoint-based)   value: " << setw(23) << setprecision(16) << gradzero << endl;
				cout << "## df/ds relative_error value: " << setw(23) << setprecision(16) << fabs(d1grad-gradzero)/gradzero << endl;
				cout << "######################################################" << endl;

			}
			else
			{

				cout << "######################################################" << endl;
				cout << "## TESTING HESSIAN MATRIX ##" << endl;
				cout << "######################################################" << endl;

				// Hessian matrix testing
				vector< vector< MGFloat> > mtxHessian;
				mtxHessian.resize(mpData->cDesignParamCollection().Size());
				for (MGSize i = 0; i < mtxHessian.size(); i++)
					mtxHessian[i].resize(mpData->cDesignParamCollection().Size(), 0.0);

				ofstream of("_convprim_hessian.txt");

				for (MGSize i = 0; i < mvecDesParamValue.size(); i++)
				{
					cout << "######################################################" << endl;
					cout << "## design parameter: " << i + 1 << " of " << mvecDesParamValue.size() << endl;
					cout << "######################################################" << endl;
					vector<MGFloat> vecgrad(mvecDesParamValue.size(), 0.0);

					cout << "######################################################" << endl;
					cout << "## df/ds(s+h) forward calculation: " << i + 1 << " of " << mvecDesParamValue.size() << endl;
					cout << "######################################################" << endl;

					MGFloat h = mtemph;
					mvecDesParamValue[i] += h;
					ObjectiveFunc(mvecDesParamValue, vecgrad);
					mvecDesParamValue[i] -= h;

					for (MGSize j = 0; j < vecgrad.size(); j++)
						mtxHessian[i][j] = vecgrad[j];

					cout << "######################################################" << endl;
					cout << "## df/ds(s-h) backward calculation: " << i + 1 << " of " << mvecDesParamValue.size() << endl;
					cout << "######################################################" << endl;

					mvecDesParamValue[i] -= h;
					ObjectiveFunc(mvecDesParamValue, vecgrad);
					mvecDesParamValue[i] += h;

					for (MGSize j = 0; j < vecgrad.size(); j++)
						mtxHessian[i][j] = (mtxHessian[i][j] - vecgrad[j]) / (2 * h);

					for (MGSize j = 0; j < mtxHessian[i].size(); j++)
						of << setw(23) << setprecision(16) << mtxHessian[i][j];
					of << endl;

				}

				cout << "######################################################" << endl;
				cout << "## writing results to file                          ##" << endl;
				cout << "######################################################" << endl;

				//ofstream of("_convprim_hessian.txt");
				//for (MGSize i = 0; i < mtxHessian.size(); i++)
				//{
				//	for (MGSize j = 0; j < mtxHessian[i].size(); j++)
				//		of << setw(23) << setprecision(16) << mtxHessian[i][j];
				//	of << endl;
				//}

				cout << "######################################################" << endl;
				cout << "## writing results to file -- done                  ##" << endl;
				cout << "######################################################" << endl;
			}

		}


		MGInt value = FINISHED;
		MPI_Bcast(&value, 1, MPI_INT, 0, MPI_COMM_WORLD);
	}
	else
	{
		bool bfinished = false;
		do
		{
			MGInt value;
			MPI_Bcast(&value, 1, MPI_INT, 0, MPI_COMM_WORLD);
			
			vector<double> xx(mdim,0.0), grad(mdim,0.0);
			switch ( value )
			{
				case OBJECTIVEFUNC:
					ObjectiveFunc(xx,grad);
					break;
				case CONSTRAINTFUNC:
					ConstraintFunc(0,NULL,0,NULL,NULL);
					break;
				case FINISHED:
					bfinished = true;
					break;
			}
			
		
		}
		while ( !bfinished );
		cout << ProcessInfo::Prompt() << "optimization -- finished" << endl;
	}

}




template <Dimension DIM, EQN_TYPE ET>
void MPISolverOptim<DIM, ET>::ConstraintFunc(unsigned m, double *result, unsigned n, const double *x, double *grad)
{
	cout << ProcessInfo::Prompt() << "MPISolverOptim::ConstraintFunc()" << endl;
	if ( ProcessInfo::cProcId() == 0 )
	{
		MGInt value = CONSTRAINTFUNC;
		MPI_Bcast(&value, 1, MPI_INT, 0, MPI_COMM_WORLD);
	}	
	
	if ( ProcessInfo::cProcId() == 0 )
	{
		if (!mbineqval) // if max/min value of objective was not specified, pick the initial value
		{
			mineqval = mpData->cObjectiveCollection()[1]->cObjective();
			mbineqval = true;
		}

		MGInt multiplier = 1; //default nlopt constraint is c(x) <= 0; for having c(x) >= 0 multiplication of value and gradients is required
		if ( mbineqtype == true )
			multiplier = -1;

		for (MGSize i = 0; i < m; i++)
		{
			result[i] = multiplier * (mpData->cObjectiveCollection()[i+1]->cObjective() - mineqval ); //first objective is optimized, following ones are constraints
			
			for (MGSize j = 0; j < mpData->cObjectiveCollection()[i+1]->cGradObjective().size(); j++)
				grad[i*n + j] = multiplier * mpData->cObjectiveCollection()[i + 1]->cGradObjective()[j];

		}
		
		cout << "#### constraints functions: " << endl;;
		for (MGSize i = 0; i < m; i++)
		{
			//cout << i+1 << ": " << setw(16) << result[i] << endl;
			cout << i + 1 << " : " << mpData->cObjectiveCollection()[i + 1]->cDescription() << " : " << setw(16) << setprecision(16) << result[i] << endl;
		}
	}
	
}

template <Dimension DIM, EQN_TYPE ET>
MGFloat MPISolverOptim<DIM, ET>::ObjectiveFunc(const vector<double> &xx, vector<double>& grad)
{

	if ( ProcessInfo::cProcId() == 0 )
	{
		MGInt value = OBJECTIVEFUNC;
		MPI_Bcast(&value, 1, MPI_INT, 0, MPI_COMM_WORLD);
	}	
	
	
	cout << ProcessInfo::Prompt() << "MPISolverOptim::ObjectiveFunc()" << endl;
	MGSize procid_root = 0;
	vector<double> x(xx);
	MPI_Bcast( &x[0] , x.size() , MPI_DOUBLE , procid_root , MPI_COMM_WORLD );
	
	mitercount = 1; //mitercount++;
	cout << endl << ProcessInfo::Prompt() << " ################# optimization iteration: " << mitercount << "  ################# ";
	IOSpace::WriteTECSurf writeTec(mMPISolver.cData().cGrid(), &( mMPISolver.cData().cSolution() ) ) ;
	
	if ( ProcessInfo::cProcId() == 0)
	{
		mLog << mitercount << endl;
		mLog.precision(16);
		for (MGSize i = 0; i < mdim; i++)
			mLog << x[i] << endl;
		mLog << endl;
	}
	
	vector<GVec> vecTotalPerturb(mMPISolver.cData().cGrid().SizeNodeTab(), 0.0);
	for (MGSize i = 0; i < mdim; i++)
	{
		vector<GVec> vecPerturb;
		mMPISolver.cData().cGridAssistant().ComputePerturbation(mMPISolver.cData().cDesignParamCollection()[i].cNode().cPos(), vecPerturb);

		for (MGSize j = 0; j < vecPerturb.size(); j++)
			vecTotalPerturb[j] += vecPerturb[j] * x[i];

// 		for (MGSize i = 0; i < vecPerturb.size(); i++)
// 			mpData->rSolution()[i](0) = vecPerturb[i].cY();
// 		writeTec.DoWrite(ProcessInfo::ModifyFileName("_zzzzz.dat").c_str() ); //writing perturbation field for current morphing point as V01 variable

	}

// 	for (MGSize i = 0; i < vecTotalPerturb.size(); i++)
// 		mMPISolver.rData().rSolution()[i](0) = vecTotalPerturb[i].cY();
// 	writeTec.DoWrite(ProcessInfo::ModifyFileName("_zzzzz.dat").c_str() ); //writing total perturbation field 

	mMPISolver.cData().cGridAssistant().FixNodes( vecTotalPerturb );
	
	//for (MGSize i = 0; i < vecTotalPerturb.size(); i++)
	//	mpData->rSolution()[i](1) = vecTotalPerturb[i].cY();
	//writeTec.DoWrite("_zzzzz.dat"); //writing total perturbation field after freezing points

	mMPISolver.rData().rGridAssistant().Perturbate(vecTotalPerturb);

	if ( 1 != mitercount )
	{
		bool bsolprev = false;
		if (ConfigSect().KeyExists( ConfigStr::Solver::Optimizer::SOLPREV))
			bsolprev = ConfigSect().ValueBool(ConfigStr::Solver::Optimizer::SOLPREV);

		mMPISolver.rData().InitSolution( bsolprev );
	}
	
	//writeTec.DoWrite(ProcessInfo::ModifyFileName("_bef_perturbated.dat").c_str() );
	MGInt solveritcount;
	mMPISolver.Solve(true,&solveritcount);
		
	mOfItercount << mitercount << " " << solveritcount << endl;

	//ostringstream oss; oss << "output/perturbated_" << setw(3) << setfill('0') << mitercount << ".dat";
	//writeTec.DoWrite( ProcessInfo::ModifyFileName( oss.str() ).c_str() );
	
	mpData->rGridAssistant().RevertToInitial();
		
	MGFloat objective = mpData->cObjectiveCollection()[0]->cObjective();
	vector<MGFloat> objgrad = mpData->cObjectiveCollection()[0]->cGradObjective();
	
	if (ProcessInfo::cProcId() == 0)
	{
		mpData->cObjectiveCollection().Write(mLog);
		mpData->cObjectiveCollection().Write();
		
		grad.clear();
		for (MGSize i = 0; i < mdim; i++)
			grad.push_back(objgrad[i]);	
	}
		
	return objective;
	
	
	
}



void init_MPISolverOptim()
{
	static ConcCreator< MGString , MPISolverOptim<DIM_2D, EQN_EULER>, SolverBase>		gCreatorMPISolverOptimEuler2D("MPISOLVER_2D_EULER_OPTIM");
	static ConcCreator< MGString, MPISolverOptim<DIM_2D, EQN_NS>, SolverBase>			gCreatorMPISolverOptimNS2D("MPISOLVER_2D_NS_OPTIM");
	static ConcCreator< MGString, MPISolverOptim<DIM_2D, EQN_RANS_SA>, SolverBase>		gCreatorMPISolverOptimRANSSA2D("MPISOLVER_2D_RANS_SA_OPTIM");

	static ConcCreator< MGString, MPISolverOptim<DIM_3D, EQN_EULER>, SolverBase>		gCreatorMPISolverOptimEuler3D("MPISOLVER_3D_EULER_OPTIM");
	static ConcCreator< MGString, MPISolverOptim<DIM_3D, EQN_NS>, SolverBase>			gCreatorMPISolverOptimNS3D("MPISOLVER_3D_NS_OPTIM");
	static ConcCreator< MGString, MPISolverOptim<DIM_3D, EQN_RANS_SA>, SolverBase>		gCreatorMPISolverOptimRANSSA3D("MPISOLVER_3D_RANS_SA_OPTIM");
	
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
