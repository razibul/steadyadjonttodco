#include <mpi.h>
#include <petscsys.h>
#include "mpipetscexecadjoint.h"
#include "libredcore/processinfo.h"
#include "libcoreio/writetec.h"
#include "libcoreio/writetecsurf.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
void MPIPETScExecAdjoint<DIM,ET,MAPPER>::Create(const CfgSection* pcfgsec, Physics<DIM, ET>* pphys, Data<DIM, ET>* pdata)
{
	ExecAdjointBase<DIM,ET,MAPPER>::Create(pcfgsec, pphys, pdata);
}

// template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
// void MPIPETScExecAdjoint<DIM,ET,MAPPER>::Create(const CfgSection* pcfgsec, Physics<DIM, ET>* pphys, Data<DIM, ET>* pdata, const CfgSection* pcfgsectimsol, const CfgSection* pcfgsecspacesol)
// {
// 	ExecAdjointBase<DIM,ET,MAPPER>::Create(pcfgsec, pphys, pdata, pcfgsectimsol, pcfgsecspacesol);
// }

template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
void MPIPETScExecAdjoint<DIM,ET,MAPPER>::PostCreateCheck() const
{
	ExecAdjointBase<DIM,ET,MAPPER>::PostCreateCheck();
}

template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
void MPIPETScExecAdjoint<DIM,ET,MAPPER>::Init()
{
	this->mpSpaceSol->Init();
	this->mpTimeSol->Init();
	
	this->mpTimeSol->rLocalSolution().Resize( this->cData().cSolution().Size() );
	
	const MGSize nsol = this->cData().cSolution().Size();

	this->mh = 1e-6;

	// petsc initialization
	this->mpSpaceSol->PETScInitOwnedIdRange( mRange, mtabGlobId );
	this->mpSpaceSol->PETScResizeVct( mvecFuncGradSol );
	this->mpSpaceSol->PETScResizeVct( mvecAdj );
	mvecvecAdj.clear();
	mvecvecAdj.resize( this->mpObjectiveColl->Size() );
	for ( MGInt i=0; i< mvecvecAdj.size(); i++)
		this->mpSpaceSol->PETScResizeVct( mvecvecAdj[i] );
	
	this->mpSpaceSol->PETScResizeMtx( mmtxResGradSol );
	
	KSPCreate( PETSC_COMM_WORLD, &mKsp );
// 	KSPSetOperators( mKsp, mmtxResGradSol, mmtxResGradSol, DIFFERENT_NONZERO_PATTERN );
	
}

template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
bool MPIPETScExecAdjoint<DIM,ET,MAPPER>::SolveStep( ConvergenceInfo<ESIZE>& info)
{
	return false;
}

template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
bool MPIPETScExecAdjoint<DIM,ET,MAPPER>::SolveFinal()
{
	ResiduumGradSol();
	
	for (MGSize i = 0; i < (this->mpObjectiveColl)->Size(); i++)
	{
		ObjectiveGradSol(i);
		const MGFloat objective = EvaluateFunctional( this->cData().cSolution() , i );
		( *( this->mpObjectiveColl ) )[i]->rObjective() = objective;
		
		if ( ( *( this->mpObjectiveColl ) )[i]->cDoAdjoint() )
		{
			if ( ProcessInfo::cProcId() == 0 )
				cout << "Adjoint for objective " << i+1 << " of " << (this->mpObjectiveColl)->Size() << " : " << (*(this->mpObjectiveColl))[i]->cDescription() << endl;
			
			VecScale(mvecFuncGradSol, -1.0);
			SolveAdjointEqn(mvecFuncGradSol,mvecAdj);
			VecScale(mvecFuncGradSol, -1.0);

			VecCopy(mvecAdj,mvecvecAdj[i]);
			
		}

	}
	
	for (MGSize i = 0; i < this->cDataAdj().cDesignParamCollection().Size(); i++)
	{
		Vec vecResGradDes;
		this->mpSpaceSol->PETScResizeVct(vecResGradDes);
		ResiduumGradDesign(i,vecResGradDes);
		for (MGSize z = 0; z < mvecvecAdj.size(); z++)
		{
			MGFloat objgrad = 0;
			VecDot(vecResGradDes, mvecvecAdj[z], &objgrad);

			(*(this->mpObjectiveColl))[z]->rGradObjective()[i] = objgrad;
		}
	}

	if (ProcessInfo::cProcId() == 0)
	{
		MGString sfilename = "_out_grad.txt";
		if (this->ConfigSect().KeyExists(ConfigStr::Solver::Executor::Adjoint::OUTFNAME))
			sfilename = this->ConfigSect().ValueString(ConfigStr::Solver::Executor::Adjoint::OUTFNAME);

		ofstream ofgrad(sfilename.c_str());
		if (!ofgrad)
			THROW_INTERNAL("Adjoint<DIM,ET,MAPPER>::DoFinal() -- can not open file for writing objective gradients");

		ofgrad << this->cDataAdj().cDesignParamCollection().Size() << endl;
		ofgrad << (this->mpObjectiveColl->Size());

		ofgrad.setf(ios::scientific);
		for (MGSize i = 0; i < (*this->mpObjectiveColl).Size(); i++)
		{
			if ((*(this->mpObjectiveColl))[i]->cDoAdjoint())
				ofgrad << " " << (*this->mpObjectiveColl)[i]->cDescription();
		}
		ofgrad << endl;

		for (MGSize i = 0; i < this->cDataAdj().cDesignParamCollection().Size(); i++)
		{
			for (MGSize j = 0; j < this->mpObjectiveColl->Size(); j++)
			{
				if ((*(this->mpObjectiveColl))[j]->cDoAdjoint())
					ofgrad << setw(24) << setprecision(16) << (*this->mpObjectiveColl)[j]->cGradObjective()[i] << " ";
			}
			ofgrad << endl;

		}

		ofgrad.close();

		MGString sfname_obj = "_out_objective.txt";

		ofstream ofobj(sfname_obj.c_str());
		for (MGSize i = 0; i < (*this->mpObjectiveColl).Size(); i++)
		{
			ofobj << (*this->mpObjectiveColl)[i]->cDescription() << "\t" << setw(24) << setprecision(16) << (*this->mpObjectiveColl)[i]->cObjective() << endl;
		}

		ofobj.close();
	}
	
	
	
	for ( MGSize z=0; z<mvecvecAdj.size(); z++)
	{
		const bool mbWriteAdjTec = (*(this->mpObjectiveColl))[z]->cIfWriteAdjTec();
		const bool mbWriteAdjTecSurf = (*(this->mpObjectiveColl))[z]->cIfWriteAdjTecSurf();
		
		if ( mbWriteAdjTec || mbWriteAdjTecSurf )
		{		
			//writing to file
			PetscInt nsize;
			PetscScalar  *ptab;
			VecGetLocalSize( mvecvecAdj[z], &nsize);
			VecGetArray( mvecvecAdj[z], &ptab);
			for ( MGSize i=0; i<nsize; i++)
				this->rDataAdj().rSolAdjoint()[i/BSIZE][i%BSIZE] = ptab[i];
				
			VecRestoreArray( mvecvecAdj[z], &ptab);
			
			if ( mbWriteAdjTec )
			{
				IO::WriteTEC writeAdj( this->cData().cGrid(), &this->cDataAdj().cSolAdjoint() );
				ostringstream oss;
				oss << "_adjoint_" << z << ".dat";
				writeAdj.DoWrite( ProcessInfo::ModifyFileName( oss.str() ).c_str() );
			}
			if ( mbWriteAdjTec )
			{
				IO::WriteTECSurf writeAdjSurf( this->cData().cGrid(), &this->cDataAdj().cSolAdjoint() );
				ostringstream oss_surf;
				oss_surf << "_adjoint_surf_" << z << ".dat";
				writeAdjSurf.DoWrite( ProcessInfo::ModifyFileName( oss_surf.str() ).c_str() );
			}
			
			
			
			
		}
		
	}
	
	SolveHessian();

// 	PetscInt nsize;
// 	PetscScalar  *ptab;
// 	VecGetLocalSize( mvecFuncGradSol, &nsize);
// 	VecGetArray( mvecFuncGradSol, &ptab);
// 	for ( MGSize i=0; i<nsize; i++)
// 		this->rData().rSolAdjoint()[i/BSIZE][i%BSIZE] = ptab[i];
// 
// 	VecRestoreArray( mvecFuncGradSol, &ptab);
// 	
// 	IO::WriteTEC writefuncgradsol( this->cData().cGrid(), &this->cData().cSolAdjoint() );
// 	ostringstream oss;
// 	oss << "_funcgradsol.dat";
// 	writefuncgradsol.DoWrite( ProcessInfo::ModifyFileName( oss.str() ).c_str() );
		
	// copy solution for computing objective value
	// optional writing adjoint to file
	
	return false;
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPIPETScExecAdjoint<DIM, ET, MAPPER>::SolveHessian()
{
	if (ProcessInfo::cProcId() == 0)
	{
		cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << endl;
		cout << "@@ MPIPETScExecAdjoint::SolveHessian @@" << endl;
		cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << endl;
	}

	const MGSize desparsize = this->cDataAdj().cDesignParamCollection().Size();
	vector< vector<MGFloat> > mtxHess;
	mtxHess.resize(desparsize);
	for (MGSize i = 0; i < mtxHess.size(); i++)
		mtxHess[i].resize(desparsize, 0.0);

	for (MGSize i = 0; i < desparsize; i++)
	{
		//direction \beta is assumed as versor for each design variable;
		if (ProcessInfo::cProcId() == 0)
			cout << "@@ design parameter : " << i + 1 << " / " << desparsize << endl;

		//jacobian mtx and for initial problem
		ResiduumGradSol();
		ObjectiveGradSol(0); //for test only first objective is considered

		Vec vecRHS;	this->mpSpaceSol->PETScResizeVct(vecRHS);
		ResiduumGradDesign(i, vecRHS);
		VecScale(vecRHS, -1.);

		Vec vecb; this->mpSpaceSol->PETScResizeVct(vecb);
		VecZeroEntries(vecb);
		
		// solving eqn for b 
		if (ProcessInfo::cProcId() == 0)	cout << "@@ Solving Tangent Eqn for b" << endl;
		SolveTangentEqn(vecRHS, vecb);

		//building RHS for a eqn. 
		Vec vecResPert;		this->mpSpaceSol->PETScResizeVct(vecResPert);
		Vec vecResPertBack;	this->mpSpaceSol->PETScResizeVct(vecResPertBack);

		VecZeroEntries(vecRHS);
		VecZeroEntries(vecResPert);
		VecZeroEntries(vecResPertBack);

		MGFloat h = this->mpSpaceSol->ConfigSect().ValueFloat(ConfigStr::Solver::Executor::SpaceSol::NUMDIFF_EPS);
		vector<GVec> perturb;
		GVec vec = this->cDataAdj().cDesignParamCollection()[i].cNode().cPos();
		this->rDataAdj().rGridAssistant().ComputePerturbation(vec, perturb);
		for (MGSize j = 0; j < perturb.size(); j++)
			perturb[j] *= h;
		this->cDataAdj().cGridAssistant().FixNodes(perturb);

		// forward calculation -- rhs for a
		this->rDataAdj().rGridAssistant().Perturbate(perturb);
		PerturbateSolution(vecb, h);
		ResiduumGradSol(); // derivative of residual wrt flow solution
		ObjectiveGradSol(0); // derivative of objective wrt flow solution
		MatMultTransposeAdd(mmtxResGradSol, mvecvecAdj[0], mvecFuncGradSol, vecResPert); // residuals from adjoint eqn (forward)
		PerturbateSolution(vecb, -1.*h);
		this->rDataAdj().rGridAssistant().Unperturbate(perturb);

		// backward calculation -- rhs for a
		PerturbateSolution(vecb, -1.*h);
		this->rDataAdj().rGridAssistant().Unperturbate(perturb);
		ResiduumGradSol(); // derivative of residual wrt flow solution
		ObjectiveGradSol(0); // derivative of objective wrt flow solution
		MatMultTransposeAdd(mmtxResGradSol, mvecvecAdj[0], mvecFuncGradSol, vecResPertBack); // residuals from adjoint eqn (backward)
		PerturbateSolution(vecb, h);
		this->rDataAdj().rGridAssistant().Perturbate(perturb);

		VecWAXPY(vecRHS, -1., vecResPertBack, vecResPert); //rhs for 2nd adjoint eqn (for a variable)
		VecScale(vecRHS, 1. / (2 * h));
		VecScale(vecRHS, -1.); //it is written explicitly in equation

		Vec veca;	this->mpSpaceSol->PETScResizeVct(veca);
		VecZeroEntries(veca);
		
		if (ProcessInfo::cProcId() == 0)	cout << "@@ Solving Adjoint Eqn for a"; 
		SolveAdjointEqn(vecRHS, veca);
		if (ProcessInfo::cProcId() == 0)	cout << "@@ Solving Adjoint Eqn for a -- done" << endl;

		for (MGSize k = 0; k < desparsize; k++)
		{	
			Vec vecRes;		this->mpSpaceSol->PETScResizeVct(vecRes);
			Vec vecResBeta;	this->mpSpaceSol->PETScResizeVct(vecResBeta);
			
			VecZeroEntries(vecRes);
			VecZeroEntries(vecResBeta);
			
			Vec vecResPert;		this->mpSpaceSol->PETScResizeVct(vecResPert);
			Vec vecResPertBack;	this->mpSpaceSol->PETScResizeVct(vecResPertBack);

			// forward calculation -- hessian entry
			MGFloat val_fwd = 0.0;
			this->rDataAdj().rGridAssistant().Perturbate(perturb);
			PerturbateSolution(vecb, h);
			ResiduumGradDesign(k, vecResPert);
			VecDot(mvecvecAdj[0], vecResPert, &val_fwd);
			PerturbateSolution(vecb, -1.*h);
			this->rDataAdj().rGridAssistant().Unperturbate(perturb);

			// backward calculation -- hessian entry
			MGFloat val_bwd = 0.0;
			PerturbateSolution(vecb, -1.*h);
			this->rDataAdj().rGridAssistant().Unperturbate(perturb);
			ResiduumGradDesign(k, vecResPertBack);
			VecDot(mvecvecAdj[0], vecResPertBack, &val_bwd);
			this->rDataAdj().rGridAssistant().Perturbate(perturb);
			PerturbateSolution(vecb, h);
			
			PetscScalar petscval_fd = (val_fwd - val_bwd) / (2 * h);

			//VecWAXPY(vecResBeta, -1., vecResPertBack, vecResPert); //contribution to hessian mtx
			//VecScale(vecResBeta, 1. / (2 * h));

			ResiduumGradDesign(k, vecRes); //without perturbing solution
			
			PetscScalar petscval = 0.0;
			VecDot(veca, vecRes, &petscval);
			MGFloat val = petscval_fd + petscval;
			
			mtxHess[i][k] = val;

			if (ProcessInfo::cProcId() == 0)
			{
				cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << endl;
				cout << "@@		Hessian Matrix                @@" << endl;
				cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << endl;
				cout << endl << endl;
				for (int ii = 0; ii < desparsize; ii++)
				{
					for (int jj = 0; jj < desparsize; jj++)
					{
						cout << setw(24) << setprecision(16) << mtxHess[ii][jj] << " ";
					}
					cout << endl;
				}
				cout << endl << endl;
			}

		}


	}
	
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPIPETScExecAdjoint<DIM, ET, MAPPER>::PerturbateSolution(Vec vec, MGFloat scale)
{
	PetscScalar *ptab;
	VecGetArray(vec, &ptab);
	
	PetscInt nsize = 0;
	VecGetLocalSize(vec, &nsize);
	MGSize indx = 0;

	for (MGSize i = 0; i < nsize; i++)
	{
		this->rData().rSolution()[i/BSIZE][i%BSIZE] += scale * ptab[indx];
	}


	/*for (MGSize i = 0; i < this->cData().cSolution().Size(); i++)
	{
		MGSize globid = mtabGlobId[i];

		if (globid < mRange.first || globid >= mRange.second)
			continue;

		for (MGSize ieq = 0; ieq < BSIZE; ieq++)
		{
			this->rData().rSolution()[i][ieq] += scale * ptab[indx];
			++indx;
		}

	}*/
	VecRestoreArray(vec, &ptab);

	//for (MGSize i = 0; i < this->cData().cSolution().Size(); i++)
	//{
	//	if ((mtabGlobId[i] < mRange.first) || (mtabGlobId[i] >= mRange.second))
	//		continue;

	//	vector<PetscInt> vecindx(BSIZE, 0);
	//	vector<PetscScalar> vecval(BSIZE, 0.0);

	//	for (MGSize ieq = 0; ieq < BSIZE; ieq++)
	//		vecindx[ieq] = mtabGlobId[i] * BSIZE + ieq;

	//	VecGetValues(vec, vecindx.size(), &vecindx[0], &vecval[0]);

	//	for (MGSize ieq = 0; ieq < BSIZE; ieq++) //todo: BSIZE should be replaced by ESIZE
	//	{
	//		this->rData().rSolution()[i][ieq] += scale * vecval[ieq];

	//		////VecGetValues(vec, liczba indeksow, tablica z indeksami, tablica wartosci)
	//		//switch (boverwrite)
	//		//{
	//		//case false:
	//		//	this->rData().rSolution()[i][ieq] += scale * vecval[ieq];
	//		//	break;
	//		//case true:
	//		//	this->rData().rSolution()[i][ieq] = scale * vecval[ieq];
	//		//	break;
	//		//}
	//		

	//	}


	//}

}


template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
void MPIPETScExecAdjoint<DIM,ET,MAPPER>::ObjectiveGradSol( const MGSize& i)
{
	(*(this->mpObjectiveColl))[i]->ComputeGradSolution( this->cData().cSolution() );
	
	const MGSize nsol = this->cData().cSolution().Size();
	//Sparse::Vector<BSIZE>* vec;
	//vec = &(*(this->mpObjectiveColl))[i]->rGradSolution();
	
	Sparse::Vector<ESIZE>* vecESIZE;
	vecESIZE = &(*(this->mpObjectiveColl))[i]->rGradSolution();
	
	Sparse::Vector<BSIZE> vec;
	vec.Resize(this->cData().cSolution().Size());

	for (MGSize j = 0; j < vec.Size(); j++)
		MAPPER::GlobalToLocal(vec[j], (*vecESIZE)[j]);


	MGSize nodecount = mRange.second - mRange.first;
	vector<PetscInt> vecindex ; //( nodecount ) ;
	vector<PetscScalar> vecval ;//( nodecount*BSIZE );
	vecindex.reserve( nodecount*BSIZE);
	vecval.reserve( nodecount*BSIZE);
	
	MGSize count=0;
	
	for ( MGSize z=0; z< vec.Size() ; z++ )
	{ 
		if ( ( mtabGlobId[z] < mRange.first) || ( mtabGlobId[z] >= mRange.second ) )
			continue;
		
		for ( MGSize ieq=0; ieq < BSIZE ; ieq++ )
		{
			MGFloat tmp = vec[z](ieq);
			vecval.push_back( tmp );
			vecindex.push_back( mtabGlobId[z]*BSIZE+ieq );
			count++;
		}
		
	}

	VecZeroEntries(mvecFuncGradSol);
	VecSetValues(mvecFuncGradSol,count,&vecindex[0],&vecval[0],INSERT_VALUES);
	
	VecAssemblyBegin(mvecFuncGradSol);
	VecAssemblyEnd(mvecFuncGradSol);
	
	CheckVecNAN(mvecFuncGradSol,"mvecFuncGradSol");
	
	//VecView(mvecFuncGradSol,PETSC_VIEWER_DRAW_WORLD);
	
	
}

template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
void MPIPETScExecAdjoint<DIM,ET,MAPPER>::ResiduumGradSol()
{
	MPI_Barrier( MPI_COMM_WORLD );
	MatZeroEntries( mmtxResGradSol);

	this->mpTimeSol->BuildJacobiMatrix();
	mmtxResGradSol = this->mpTimeSol->rJacobiMatrixPETSC();
	// copy reference to matrix from timesol
	
	MatAssemblyBegin(mmtxResGradSol,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(mmtxResGradSol,MAT_FINAL_ASSEMBLY);
}

template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
void MPIPETScExecAdjoint<DIM,ET,MAPPER>::SolveAdjointEqn(Vec vecRHS, Vec& vecX)
{
	MGSize max_iter = (*this->mpcfgsectimsol).ValueSize(ConfigStr::Solver::Executor::TimeSol::LINMAXITER);
	MGSize m = (*this->mpcfgsectimsol).ValueSize(ConfigStr::Solver::Executor::TimeSol::LINDKSPACE);
	MGFloat gmres_eps = (*this->mpcfgsectimsol).ValueFloat(ConfigStr::Solver::Executor::TimeSol::LINMINEPS);

	VecZeroEntries( vecX );
	
	//solve ksp
	
	PetscInt	its, nlocal, first;
	KSP         *subksp;     /* array of local KSP contexts on this processor */
	PC          pc;           /* PC context */
	PC          subpc;
	
 	KSPSetType( mKsp, KSPGMRES);
	
	KSPGetPC( mKsp, &pc );
	PCSetType( pc, PCBJACOBI );

	KSPSetOperators( mKsp, mmtxResGradSol, mmtxResGradSol, DIFFERENT_NONZERO_PATTERN );
	
    KSPSetTolerances( mKsp, gmres_eps, PETSC_DEFAULT, PETSC_DEFAULT, max_iter);
    KSPGMRESSetRestart( mKsp, m);
	
    KSPSetFromOptions( mKsp);
	KSPSetUp( mKsp);

	PCBJacobiGetSubKSP( pc, &nlocal, &first, &subksp);
	for ( MGSize i=0; i<nlocal; i++) 
	{
		KSPGetPC( subksp[i], &subpc );
	    KSPSetTolerances( subksp[i], PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT, 1);
		PCSetType( subpc, PCILU );	
		PCFactorSetLevels( subpc, 0 );
	}
	
	KSPSolveTranspose(mKsp, vecRHS, vecX);
	
	MGFloat gmres_epsnew;
    PetscInt iter = 0;
	KSPGetResidualNorm( mKsp, &gmres_epsnew );
    KSPGetIterationNumber( mKsp, &iter);
	
	
	if ( ProcessInfo::cProcId() == 0 )
	{
		cout << resetiosflags(ios_base::scientific);
		cout << "max_iter = " << iter << " eps = " << gmres_epsnew << endl;
	}
	
	CheckVecNAN(vecX, "MPIPETScExecAdjoint::SolveAdjointEqn()::vecX");
	
}


template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
void MPIPETScExecAdjoint<DIM,ET,MAPPER>::SolveTangentEqn(Vec vecRHS, Vec& vecX)
{
	// old version -- using same linear solver from petsc for every single eqn.
	MGSize max_iter = (*this->mpcfgsectimsol).ValueSize(ConfigStr::Solver::Executor::TimeSol::LINMAXITER);
	MGSize m = (*this->mpcfgsectimsol).ValueSize(ConfigStr::Solver::Executor::TimeSol::LINDKSPACE);
	MGFloat gmres_eps = (*this->mpcfgsectimsol).ValueFloat(ConfigStr::Solver::Executor::TimeSol::LINMINEPS);

	VecZeroEntries(vecX);

	//solve ksp

	PetscInt	its, nlocal, first;
	KSP         *subksp;     /* array of local KSP contexts on this processor */
	PC          pc;           /* PC context */
	PC          subpc;

	KSPSetType(mKsp, KSPGMRES);

	KSPGetPC(mKsp, &pc);
	PCSetType(pc, PCBJACOBI);

	KSPSetOperators(mKsp, mmtxResGradSol, mmtxResGradSol, DIFFERENT_NONZERO_PATTERN);

	KSPSetTolerances(mKsp, gmres_eps, PETSC_DEFAULT, PETSC_DEFAULT, max_iter);
	KSPGMRESSetRestart(mKsp, m);

	KSPSetFromOptions(mKsp);
	KSPSetUp(mKsp);

	PCBJacobiGetSubKSP(pc, &nlocal, &first, &subksp);
	for (MGSize i = 0; i<nlocal; i++)
	{
		KSPGetPC(subksp[i], &subpc);
		KSPSetTolerances(subksp[i], PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT, 1);
		PCSetType(subpc, PCILU);
		PCFactorSetLevels(subpc, 0);
	}

	KSPSolve(mKsp, vecRHS, vecX);

	MGFloat gmres_epsnew;
	PetscInt iter = 0;
	KSPGetResidualNorm(mKsp, &gmres_epsnew);
	KSPGetIterationNumber(mKsp, &iter);


	if (ProcessInfo::cProcId() == 0)
	{
		cout << resetiosflags(ios_base::scientific);
		cout << "max_iter = " << iter << " eps = " << gmres_epsnew << endl;
	}
	CheckVecNAN(vecX, "MPIPETScExecAdjoint::SolveTangentEqn()::vecX");
	
}


template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
void MPIPETScExecAdjoint<DIM,ET,MAPPER>::ResiduumGradDesign(const MGSize& i, Vec& vec)
{
	Vec vecRes;
	Vec vecResPert;
	Vec vecResPertBack;
	this->mpSpaceSol->PETScResizeVct( vecRes);
	this->mpSpaceSol->PETScResizeVct( vecResPert );
	this->mpSpaceSol->PETScResizeVct( vecResPertBack );

	this->mpSpaceSol->PETScResiduum(vecRes);

	MGFloat h = this->mpSpaceSol->ConfigSect().ValueFloat(ConfigStr::Solver::Executor::SpaceSol::NUMDIFF_EPS);
	vector<GVec> perturb;

	GVec vecpos = this->cDataAdj().cDesignParamCollection()[i].cNode().cPos();
	this->rDataAdj().rGridAssistant().ComputePerturbation(vecpos, perturb);
	for (MGSize j = 0; j < perturb.size(); j++)
		perturb[j] *= h;

	this->cDataAdj().cGridAssistant().FixNodes(perturb);

	this->rDataAdj().rGridAssistant().Perturbate(perturb);
	this->mpSpaceSol->PETScResiduum(vecResPert);
	this->rDataAdj().rGridAssistant().Unperturbate(perturb);

	this->rDataAdj().rGridAssistant().Unperturbate(perturb);
	this->mpSpaceSol->PETScResiduum(vecResPertBack);
	this->rDataAdj().rGridAssistant().Perturbate(perturb);
	
	VecWAXPY(vecRes, -1., vecResPertBack, vecResPert);
	VecScale(vecRes, 1. / (2 * h));

	VecCopy(vecRes, vec);
	
}


template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
MGFloat MPIPETScExecAdjoint<DIM,ET,MAPPER>::EvaluateFunctional(const Solution<DIM,ET>& vec, const MGSize& n )
{
	MGFloat tempObjective = 0;
	
	Sparse::Vector<ESIZE>* grad;
	grad = &(*(this->mpObjectiveColl))[n]->rGradSolution();

	for ( MGSize i=0; i < vec.Size(); i++)
	{
		if ( ( mtabGlobId[i] < mRange.first) || ( mtabGlobId[i] >= mRange.second ) )
			continue;
		
		for ( MGSize j=0; j < ESIZE; j++)
		{
			if ( ISNAN( vec[i][j] ) )
			{
				cout << ProcessInfo::Prompt() << "vec[ " << i << " ][ " << j << " ] -- NAN" << endl;
				THROW_INTERNAL("MPIPETScAdjoint<DIM,ET>::EvaluateFunctional() -- solution is NAN='" << i << "'" << " 'ieq'" << j << "'" );
				break;
			}
			if ( ISNAN( (*grad)[i][j] ) )
			{
				cout << ProcessInfo::Prompt() << "grad[ " << i << " ][ " << j << " ] -- NAN" << endl;
				THROW_INTERNAL("MPIPETScAdjoint<DIM,ET>::EvaluateFunctional() -- ObjGradSol is NAN='" << i << "'" << " 'ieq'" << j << "'");
				break;
			}
			tempObjective += vec[i][j] * (*grad)[i][j];
			
		}
		
	}
		
	MPI_Allreduce(&tempObjective,&mObjective,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	return mObjective;
	
}

template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
void MPIPETScExecAdjoint<DIM,ET,MAPPER>::LocalToGlobal()
{
	
}
template <Geom::Dimension DIM, EQN_TYPE ET,class MAPPER>
void MPIPETScExecAdjoint<DIM,ET,MAPPER>::CheckVecNAN(Vec& invec, const MGString& str)
{
	PetscInt nsize;
    PetscScalar  *ptab;
	VecGetLocalSize( invec, &nsize);
	VecGetArray( invec, &ptab);
	for ( MGSize i=0; i<nsize; i++)
		if ( ISNAN( ptab[i]) )
			THROW_INTERNAL( "MPIPETScAdjoint<DIM,ET>::CheckVecNAN() -- " << str.c_str() << " is NAN='" << i/BSIZE << "'" << " ieq='" << i%BSIZE << "'");
	VecRestoreArray( invec, &ptab);
}

void init_MPIPETScExecAdjoint()
{
	static ConcCreator<
		MGString,
		MPIPETScExecAdjoint<Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D, EQN_EULER>::SplitingFull::Block<1> >,
		ExecutorBase<DIM_2D, EQN_EULER >
	>	gCreatorMPIPETScExecAdjointEuler2D("EXECUTOR_2D_EULER_MPIPETSC_ADJOINT");
	
	static ConcCreator<
		MGString,
		MPIPETScExecAdjoint<Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D, EQN_NS>::SplitingFull::Block<1> >,
		ExecutorBase<DIM_2D, EQN_NS >
	>	gCreatorMPIPETScExecAdjointNS2D("EXECUTOR_2D_NS_MPIPETSC_ADJOINT");

	static ConcCreator<
		MGString,
		MPIPETScExecAdjoint<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D, EQN_RANS_SA>::SplitingFlowTurb::Block<1> >,
		ExecutorBase<DIM_2D, EQN_RANS_SA >
	>	gCreatorMPIPETScExecAdjointRANSSA2D("EXECUTOR_2D_RANS_SA_MPIPETSC_ADJOINT");

	static ConcCreator<
		MGString,
		MPIPETScExecAdjoint<Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D, EQN_EULER>::SplitingFull::Block<1> >,
		ExecutorBase<DIM_3D, EQN_EULER >
	>	gCreatorMPIPETScExecAdjointEuler3D("EXECUTOR_3D_EULER_MPIPETSC_ADJOINT");

	static ConcCreator<
		MGString,
		MPIPETScExecAdjoint<Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D, EQN_NS>::SplitingFull::Block<1> >,
		ExecutorBase<DIM_3D, EQN_NS >
	>	gCreatorMPIPETScExecAdjointNS3D("EXECUTOR_3D_NS_MPIPETSC_ADJOINT");

	static ConcCreator<
		MGString,
		MPIPETScExecAdjoint<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D, EQN_RANS_SA>::SplitingFlowTurb::Block<1> >,
		ExecutorBase<DIM_3D, EQN_RANS_SA >
	>	gCreatorMPIPETScExecAdjointRANSSA3D("EXECUTOR_3D_RANS_SA_MPIPETSC_ADJOINT");
	
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
