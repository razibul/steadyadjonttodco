#include "mpifreezingpointcollection.h"
#include "redvc/libredvccommon/freezingpointcollection.h"
#include "libcorecommon/factory.h"

#include <mpi.h>

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 
	
template <Dimension DIM>
void MPIFreezingPointCollection<DIM>::Create(const CfgSection* pcfgsec, const TGrid *pgrd)
{
	FreezingPointCollection<DIM>::Create(pcfgsec,pgrd);
}

template <Dimension DIM>
void MPIFreezingPointCollection<DIM>::PostCreateCheck() const
{
	FreezingPointCollection<DIM>::PostCreateCheck();
}

template <Dimension DIM>
void MPIFreezingPointCollection<DIM>::Init()
{
	CfgSection::const_iterator_rec	citrr;
	for (citrr = this->ConfigSect().begin_rec(); citrr != this->ConfigSect().end_rec(); ++citrr)
	{
		cout << ProcessInfo::Prompt() << (*citrr).first << " " << (*citrr).second << endl;
		MGFloat x = std::atof((*citrr).first.c_str());
		MGFloat y = std::atof((*citrr).second.c_str());
		//mvecFreezingPoints.push_back(GVec(x, y));
		
		MGFloat dist;
		MGSize indx = this->mpGrid->FindNearestNode( GVec(x,y) , dist );
		GVec pos = this->mpGrid->cNode(indx).cPos();
		GVec temp_pos;
		MGSize temp_rank, temp_indx;

		FindGlobalNearestNode(indx, dist, pos, &temp_rank, &temp_indx, &temp_pos);
		this->mvecFreezingPoint.push_back( FreezingPoint<DIM>( temp_pos, temp_rank, temp_indx) );
		
	}
	
	ofstream offreezepts("_log_freezept.txt");
	for (MGSize i = 0; i < this->Size(); i++)
	{
		this->mvecFreezingPoint[i].Write( offreezepts);
	}
	
}
	
template <Dimension DIM>
void MPIFreezingPointCollection<DIM>::FindGlobalNearestNode(const MGSize& indx, const MGFloat& dist, const GVec& pos, MGSize* owner_rank, MGSize* owner_indx, GVec* owner_pos)
{
	vector<MGSize>	tabsendindx( ProcessInfo::cNumProc(), indx );
	vector<MGFloat>	tabsenddist( ProcessInfo::cNumProc(), dist );
	
	vector<MGSize>	tabrecvindx( ProcessInfo::cNumProc(), indx );
	vector<MGFloat>	tabrecvdist( ProcessInfo::cNumProc(), dist );
	
	MPI_Alltoall( &tabsendindx[0], 1, MPI_UNSIGNED, &tabrecvindx[0], 1, MPI_UNSIGNED, MPI_COMM_WORLD);
	MPI_Alltoall( &tabsenddist[0], 1, MPI_DOUBLE  , &tabrecvdist[0], 1, MPI_DOUBLE  , MPI_COMM_WORLD);
	
	MGFloat mindist;
	MGSize  minindx,minprocid;
	
	mindist = tabrecvdist[0];
	minindx = tabrecvindx[0];
	minprocid = 0;
	for (MGSize i=0; i<tabrecvdist.size(); i++)
	{
		if ( mindist > tabrecvdist[i] )
		{
			mindist = tabrecvdist[i];
			minindx = tabrecvindx[i];
			minprocid = i;
		}
		
	}
	
	*owner_indx = minindx;
	*owner_rank = minprocid;
	
	MGFloat x,y;
	if (ProcessInfo::cProcId() == *owner_rank)
	{
		x = pos.cX();
		y = pos.cY();
	}
	
	MPI_Bcast(&x,1,MPI_DOUBLE, *owner_rank, MPI_COMM_WORLD);
	MPI_Bcast(&y,1,MPI_DOUBLE, *owner_rank, MPI_COMM_WORLD);
	
	*owner_pos = GVec(x,y);
	
}
	
void init_MPIFreezingPointCollection()
{
	//2D
	
	static ConcCreator<
		MGString,
		MPIFreezingPointCollection< Geom::DIM_2D >,
		FreezingPointCollection< Geom::DIM_2D >
	> gCreatorMPIFreezingPointCollection2D( "MPI_FREEZINGPOINTCOLLECTION_2D" );
	
	//3D
	
	static ConcCreator<
		MGString,
		MPIFreezingPointCollection< Geom::DIM_3D >,
		FreezingPointCollection< Geom::DIM_3D >
	> gCreatorMPIFreezingPointCollection3D( "MPI_FREEZINGPOINTCOLLECTION_3D" );
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 
