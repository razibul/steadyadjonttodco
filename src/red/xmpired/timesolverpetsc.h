#ifndef __TIMESOLVERIMPLICITPETSC_H__
#define __TIMESOLVERIMPLICITPETSC_H__

#include <petscconf.h>
#include <petscksp.h>

#include "libredcore/processinfo.h"
#include "libredconvergence/timesolverbase.h"

#include "libredcore/configconst.h"
#include "libredconvergence/cflmanager.h"




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
//	class TimeSolverImplicit
//////////////////////////////////////////////////////////////////////
template <MGSize BSIZE>
class TimeSolverImplicitPETSC : public TimeSolverBase<BSIZE>
{
	typedef typename Sparse::Vector<BSIZE>::BlockVec	BlockVec;
	
public:
	TimeSolverImplicitPETSC() : TimeSolverBase<BSIZE>()		{}

	virtual void	Create( const CfgSection* pcfgsec, SpaceSolverFacade<BSIZE>* pssol);
	virtual void	PostCreateCheck() const;
	virtual void	Init();

	virtual void	DoStep( ConvergenceInfo<BSIZE>& info);
	
	virtual void	BuildJacobiMatrix();
	//virtual Sparse::Matrix<BSIZE>&	rJacobiMatrix() { THROW_INTERNAL("") };
	virtual void	CFLCrash();
	
	const Mat&	cJacobiMatrixPETSC() const	{ return mA; };
	Mat&		rJacobiMatrixPETSC() 		{ return mA; };
	
protected:
	void	VecBlockNorm( Vec& vec, BlockVec& res);
	
private:
	vector<MGFloat>	mtabLocDT;

	CFLManager		mCFLManager;

	// ownership info
	pair<MGSize,MGSize>	mRange;
	vector<MGSize>		mtabGlobId;

	// PETSc stuff
	KSP				mKsp;
	Mat				mA;
	Vec				mDQ;
	Vec				mRHS;
};


template <MGSize BSIZE>
void TimeSolverImplicitPETSC<BSIZE>::Create( const CfgSection* pcfgsec, SpaceSolverFacade<BSIZE>* pssol)
{
	TimeSolverBase<BSIZE>::Create( pcfgsec, pssol);
}

template <MGSize BSIZE>
void TimeSolverImplicitPETSC<BSIZE>::PostCreateCheck() const
{
	TimeSolverBase<BSIZE>::PostCreateCheck();

	// do check
}


template <MGSize BSIZE>
void TimeSolverImplicitPETSC<BSIZE>::Init()
{
	const MGSize nsol = this->cSpaceSol().SolutionSize();

	this->mvecQ.Resize( nsol);
 	mtabLocDT.resize( nsol);


	MGSize nstart = 0;
	if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Executor::TimeSol::NCFLSTART ) )
		nstart = this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::TimeSol::NCFLSTART );


	mCFLManager.Init( this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::TimeSol::CFL),
		this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::TimeSol::MAXCFL ),
		this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::TimeSol::NCFLGROWTH ),
		nstart);

	mCFLManager.Reset();

	////-----Initialize PETSC
	//PetscInitialize( NULL, NULL, PETSC_NULL, PETSC_NULL); // PETSC start - this should be the first call to PETSc

	this->rSpaceSol().PETScInitOwnedIdRange( mRange, mtabGlobId );

	this->rSpaceSol().PETScResizeVct( mRHS);
	this->rSpaceSol().PETScResizeVct( mDQ);
 	this->rSpaceSol().PETScResizeMtx( mA);

    KSPCreate( PETSC_COMM_WORLD, &mKsp );
    //KSPSetOperators( mKsp, mA, mA, SAME_NONZERO_PATTERN );
    KSPSetOperators( mKsp, mA, mA, DIFFERENT_NONZERO_PATTERN );
}

template <MGSize BSIZE>
void TimeSolverImplicitPETSC<BSIZE>::CFLCrash()
{
	MGSize nstart = 0;
	if (this->ConfigSect().KeyExists(ConfigStr::Solver::Executor::TimeSol::NCFLSTART))
		nstart = this->ConfigSect().ValueSize(ConfigStr::Solver::Executor::TimeSol::NCFLSTART);

	mCFLManager.Init(this->ConfigSect().ValueFloat(ConfigStr::Solver::Executor::TimeSol::CFL),
		mCFLManager.cCFL(),
		2 * mCFLManager.cCount(), //this->ConfigSect().ValueSize(ConfigStr::Solver::Executor::TimeSol::NCFLGROWTH),
		nstart );

	mCFLManager.Reset();
	
}

template <MGSize BSIZE>
void TimeSolverImplicitPETSC<BSIZE>::BuildJacobiMatrix()
{
	MGFloat cfl = mCFLManager.cCFL();
	this->rSpaceSol().InitDT(mtabLocDT, cfl);

	MGString sjacobtype = ConfigStr::Solver::Executor::TimeSol::JacobType::Value::NUMERICAL;
	if (this->ConfigSect().KeyExists(ConfigStr::Solver::Executor::TimeSol::JacobType::KEY))
		sjacobtype = this->ConfigSect().ValueString(ConfigStr::Solver::Executor::TimeSol::JacobType::KEY);

	// compute residuum & jacobian
	this->rSpaceSol().PETScResNumJacob( mRHS, mA, mtabLocDT,false);

	
}

template <MGSize BSIZE>
void TimeSolverImplicitPETSC<BSIZE>::VecBlockNorm( Vec& vec, BlockVec& res)
{
	PetscInt nsize;
    PetscScalar  *ptab;
    
	VecGetLocalSize( vec, &nsize);
	VecGetArray( vec, &ptab);
	for ( MGSize i=0; i<nsize; i++)
	{
	}

	VecRestoreArray( vec, &ptab);
}


template <MGSize BSIZE>
void TimeSolverImplicitPETSC<BSIZE>::DoStep( ConvergenceInfo<BSIZE>& info)
{
    //CheckQ( mvecQ); // checking for non-physical values
    //CheckIsNAN( mvecQ); //checking for NAN
   
	MGFloat cfl = mCFLManager.cCFL();
	mCFLManager.Push();

	//MGFloat cfl 		= this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::TimeSol::CFL );
	MGSize max_iter 	= this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::TimeSol::LINMAXITER );
	MGSize m 			= this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::TimeSol::LINDKSPACE );
	MGFloat gmres_eps 	= this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::TimeSol::LINMINEPS );

	this->rSpaceSol().InitDT( mtabLocDT, cfl);

    // compute residuum & jacobian
	this->rSpaceSol().PETScResNumJacob( mRHS, mA, mtabLocDT);
	//this->rSpaceSol().PETScResJacobStrongBC( mRHS, mA );

	// check RHS for NAN
	PetscInt nsize;
    PetscScalar  *ptabrhs;
    
	int ifRestart = 0;
	int indxIsnan = 0;
	VecGetLocalSize( mRHS, &nsize);
	VecGetArray( mRHS, &ptabrhs);
	for ( MGSize i=0; i<nsize; i++)
		if ( ISNAN( ptabrhs[i]) )
		{
			indxIsnan = i;
			ifRestart = 1;
			break;
			//THROW_INTERNAL( "TimeSolverImplicitPETSC<BSIZE>::DoStep -- mRHS is NAN :: i='" << i/BSIZE << "'" << " ieq='" << i%BSIZE << "'");			
		}

	VecRestoreArray( mRHS, &ptabrhs);

	MPI_Allreduce(&ifRestart,&ifRestart,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);
	if ( ifRestart != 0 )
		THROW_INTERNAL( "TimeSolverImplicitPETSC<BSIZE>::DoStep -- mRHS is NAN :: i='" << indxIsnan/BSIZE << "'" << " ieq='" << indxIsnan%BSIZE << "'");
	
	// init solver
    VecZeroEntries( mDQ);	// mvecDQ[i].Init( 0.0);
    VecScale( mRHS, -1.0);	// mvecRes[i] = - mvecRes[i];

	PetscInt	its, nlocal, first;
	KSP         *subksp;     /* array of local KSP contexts on this processor */
	PC          pc;           /* PC context */
	PC          subpc;
 	 
	KSPSetType( mKsp, KSPGMRES);
	
	KSPGetPC( mKsp, &pc );
	PCSetType( pc, PCBJACOBI );

    KSPSetTolerances( mKsp, gmres_eps, PETSC_DEFAULT, PETSC_DEFAULT, max_iter);
    KSPGMRESSetRestart( mKsp, m);
	
    KSPSetFromOptions( mKsp);
	KSPSetUp( mKsp);

	PCBJacobiGetSubKSP( pc, &nlocal, &first, &subksp);
	for ( MGSize i=0; i<nlocal; i++) 
	{
		KSPGetPC( subksp[i], &subpc );
	    KSPSetTolerances( subksp[i], PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT, 1);
		PCSetType( subpc, PCILU );	
		PCFactorSetLevels( subpc, 0 );
	}
   
	
    KSPSolve( mKsp, mRHS, mDQ);
   
	MGFloat gmres_epsnew;
    PetscInt iter = 0;

	KSPGetResidualNorm( mKsp, &gmres_epsnew );
    KSPGetIterationNumber( mKsp, &iter);

	if ( ProcessInfo::cProcId() == 0 )
	{
		cout << resetiosflags(ios_base::scientific);
		cout << "cfl = " << cfl << " max_iter = " << iter << " eps = " << gmres_epsnew << endl;
	}

   
//  for ( MGSize i=0; i< mvecQ.Size(); ++i)
//      mvecQ[i] += mvecDQ[i];
//

    PetscScalar  *ptab;
    VecGetArray( mDQ, &ptab); //continous array of MY results

	MGSize index = 0;
    for ( MGSize i=0; i<this->mvecQ.Size(); ++i)
    {
		MGSize globid = mtabGlobId[i];

		if ( globid < mRange.first || globid >= mRange.second )
			continue;

        for( MGSize ieq=0; ieq<BSIZE; ++ieq)
		{
			if ( ISNAN( ptab[index]) )
				THROW_INTERNAL( "TimeSolverImplicitPETSC<BSIZE>::DoStep -- vecDQ is NAN :: i='" << i << "'" << " ieq='" << ieq << "'");
			
            this->mvecQ[i](ieq) += ptab[index];
            ++index;
        }
    }

	VecRestoreArray( mDQ, &ptab );

	
	this->rSpaceSol().ApplyStrongBC( this->mvecQ );

	typename Sparse::Vector<BSIZE>::BlockVec	vecRes;


	MGFloat errRes, errDQ;

    VecNorm( mDQ, NORM_2, &errDQ );
    VecNorm( mRHS, NORM_2, &errRes );

	if ( ISNAN( errRes ) )
		THROW_INTERNAL( "TimeSolverImplicitPETSC<BSIZE>::DoStep -- errRes norm is NAN" )
			
	////vecRes = Sparse::BlockDot( mvecDQ, mvecDQ );
	//vecRes = Sparse::BlockDot( mvecRes, mvecRes );

	VecSetBlockSize( mRHS, BSIZE);
	for ( PetscInt i=0; i<(PetscInt)BSIZE; ++i)
	{
		PetscReal rnorm;
		VecStrideNorm( mRHS, i, NORM_2, &rnorm);
		info.mErrResL2[i] = rnorm;
		VecStrideNorm( mDQ, i, NORM_INFINITY, &rnorm);
		info.mErrDQLinf[i] = rnorm;
	}
		
		
//	for ( MGSize i=0; i<BSIZE; ++i)
//	{
//		info.mErrDQL2[i] = info.mErrResL2[i] = errRes;
//		info.mErrDQLinf[i] = info.mErrResLinf[i] = 0.0;
//	}

//	info.mErrDQL2 = vecRes;

}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

#endif // __TIMESOLVERIMPLICITPETSC_H__

