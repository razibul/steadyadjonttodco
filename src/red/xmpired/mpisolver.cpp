#include <mpi.h>

#include "mpisolver.h"

#include "libcorecommon/factory.h"
#include "libcorecommon/stopwatch.h"
#include "libredcore/processinfo.h"
#include "libredcore/configconst.h"
#include "libredconvergence/convergenceinfo.h"

#include "libcoreio/store.h"
#include "libcoreio/writesol.h"
#include "libcoreio/writetec.h"
#include "libcoreio/writetecsurf.h"

#include "mpigridinfo.h"

#include "mpiperiodicbc.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//



template <Dimension DIM, EQN_TYPE ET>
void MPISolver<DIM,ET>::Create( const CfgSection* pcfgsec)
{
	TBaseSolver::Create( pcfgsec);
}


template <Dimension DIM, EQN_TYPE ET>
void MPISolver<DIM,ET>::PostCreateCheck() const
{
	TBaseSolver::PostCreateCheck();
}

template <Dimension DIM, EQN_TYPE ET>
void MPISolver<DIM,ET>::Init()
{
	this->mPhysics.Init();
	this->mData.Init();

	MPIPeriodicBC<DIM, ET>* pprd = new MPIPeriodicBC<DIM, ET>;
	pprd->Init(&this->mData.rGrid(), &this->mData.rSolution(), &this->mPhysics.ConfigSect());
	this->mData.rPeriodicBC() = pprd;
	//this->mData.InitPeriodic();

	//mPrdIntf.Init(&this->mData.cGrid(), &this->mPhysics.ConfigSect());

	mComm.Init( this->mData.cInterface() );

	MPIGridInfo<DIM>	ginfo( this->mData.cGrid(), mComm );

	ginfo.BuildGlobId();
	this->mData.InitGlobId( ginfo.cTabId(), ginfo.cOwnedIds() );

	cout << ProcessInfo::Prompt();
	cout << this->mData.cGrid().cOwnedIds().first << " - " << this->mData.cGrid().cOwnedIds().second << endl;
	cout << "done" << endl;

	for ( MGSize i=0; i<this->mtabExec.size(); ++i)
		this->mtabExec[i]->Init();

	MPI_Barrier( MPI_COMM_WORLD);

// 	mComm.SetTabDNode( tabDNode);
// 	mpTimeSol->InitDeadNodes( tabDNode);
 
}

template <Dimension DIM, EQN_TYPE ET>
void MPISolver<DIM,ET>::Solve()
{
	cout << endl << "SOLVING" << endl;

	mComm.ResizeData( this->mData.rSolution() );

	MGString	name_befsol_dat			= ProcessInfo::ModifyFileName( "_bef_solving.dat" );
	MGString	name_befsol_surf_dat		= ProcessInfo::ModifyFileName( "_bef_solving_surf.dat" );
	
	MGString	name_sol_dat			= ProcessInfo::ModifyFileName( "_sol.dat" );
	MGString	name_sol_surf_dat		= ProcessInfo::ModifyFileName( "_sol_surf.dat" );
	MGString	name_solfin_surf_dat	= ProcessInfo::ModifyFileName( "_sol_fin_surf.dat" );
	MGString	name_solfin_dat			= ProcessInfo::ModifyFileName( "_sol_fin.dat" );
	MGString	name_solinit_dat		= ProcessInfo::ModifyFileName( "_sol_init.dat" );
	MGString	name_sol_sol			= ProcessInfo::ModifyFileName( "_sol.sol" );
	MGString	name_solfin_sol			= ProcessInfo::ModifyFileName( "_sol_fin.sol" );
	MGString	name_solinit_sol		= ProcessInfo::ModifyFileName( "_sol_init.sol" );
	MGString	name_conv_lst			= ProcessInfo::ModifyFileName( "_conv.lst" );

	bool bConverged;

	MGSize	nIter =  this->ConfigSect().ValueSize( ConfigStr::Solver::MAXNITER );
	MGSize	nStore = this->ConfigSect().ValueSize( ConfigStr::Solver::NSTORE );
	MGFloat	dConvError = this->ConfigSect().ValueFloat( ConfigStr::Solver::CONVEPS );

	const CfgSection& solsect = this->ConfigSect().GetSection( ConfigStr::Solver::Solution::NAME );

	bool		bBIN = IO::FileTypeToBool( solsect.ValueString( ConfigStr::Solver::Solution::FTYPE ) );
	MGString	sSolName = solsect.ValueString( ConfigStr::Solver::Solution::FNAME );

	MGSize itcount = 0;


	IO::WriteSOL		writeSOL( this->mData.rSolution() );
	IO::WriteTEC		writeTEC( this->mData.cGrid(), &this->mData.rSolution() );
	IO::WriteTECSurf	writeTECSurf( this->mData.cGrid(), &this->mData.rSolution() );

	//writeTEC.DoWrite( ProcessInfo::ModifyFileName( "_bef_solving.dat").c_str() );
	//writeTECSurf.DoWrite( ProcessInfo::ModifyFileName( "_bef_solving_surf.dat").c_str() );
	if ( this->mbWriteTec )
    writeTEC.DoWrite( name_befsol_dat.c_str() );

	if ( this->mbWriteTecSurf )
		writeTECSurf.DoWrite( name_befsol_surf_dat.c_str() ); 

	StopWatch	stopwatch;

	stopwatch.Start();

	do
	{
		++itcount;
		bConverged = true;

		ConvergenceInfo< EquationDef<DIM,ET>::SIZE > convinfo;

		//MGSize i=0;
		for ( MGSize i=0; i<this->mtabExec.size(); ++i)
		{
			this->mtabExec[i]->SolveStep( convinfo);
		}

		// MPI - exchange the data
		mComm.SetData( this->mData.rSolution() );
		mComm.ExchangeData();
		mComm.GetData( this->mData.rSolution() );

		//cout << ProcessInfo::Prompt() << "finish COMM" << endl;
		
		MGFloat errResL2_max = MaxNorm( convinfo.mErrResL2);
		MGFloat errResL2_l2 = Norm( convinfo.mErrResL2);

		stopwatch.Mark();


		if ( ProcessInfo::cProcId() == 0 )
		{
			//cout << ProcessInfo::Prompt();
			cout << resetiosflags(ios_base::scientific);
			cout << setprecision(3);
			cout << "iter = " << setw(6) << itcount;
			cout << " time = " << setw(5) << stopwatch.cTotalTime();

			cout << setiosflags(ios_base::scientific);
			cout << " ResL2 = " << errResL2_l2 << " [ " << convinfo.mErrResL2[0];
			for ( MGSize k=1; k<EquationDef<DIM,ET>::SIZE; ++k)
				cout << ", " << convinfo.mErrResL2[k];
			cout << " ] ";
			cout << endl;
		}

		// MPI - get current eps
		errResL2_l2 = mComm.CollectEps( errResL2_l2);

		//if ( ProcessInfo::cProcId() == 0 )
		//	cout << ProcessInfo::Prompt() << "eps = " << errResL2_l2 << "  -  " << dConvError << endl << endl;


		if ( itcount%nStore == 0 || itcount==nIter)
		{
			MPI_Barrier( MPI_COMM_WORLD );
			
			cout << ProcessInfo::Prompt();
			writeSOL.DoWrite( name_sol_sol.c_str(), bBIN );
			
			if ( this->mbWriteTec )
				writeTEC.DoWrite( name_sol_dat.c_str() );

			if ( this->mbWriteTecSurf )
				writeTECSurf.DoWrite( name_sol_surf_dat.c_str() ); 			

			cout << endl;

			MPI_Barrier( MPI_COMM_WORLD );
		}

		if ( errResL2_l2 < dConvError)
		{
			cout << ProcessInfo::Prompt();
			writeSOL.DoWrite( name_solfin_sol.c_str(), bBIN );
			writeTEC.DoWrite( name_solfin_dat.c_str() );
			writeTECSurf.DoWrite( name_solfin_surf_dat.c_str() );

			break;
		}
	}
	while ( itcount < nIter );

	cout << ProcessInfo::Prompt();
	cout << "SOLVING - finished : iter count = " << itcount << endl;}

void init_MPISolver()
{
	static ConcCreator< MGString, MPISolver<DIM_2D,EQN_EULER>, SolverBase>			gCreatorMPISolverEuler2D( "MPISOLVER_2D_EULER_DEFAULT");
	static ConcCreator< MGString, MPISolver<DIM_2D,EQN_NS>, SolverBase>			gCreatorMPISolverNS2D( "MPISOLVER_2D_NS_DEFAULT"); 
	static ConcCreator< MGString, MPISolver<DIM_2D,EQN_RANS_SA>, SolverBase>		gCreatorMPISolverRANSSA2D( "MPISOLVER_2D_RANS_SA_DEFAULT"); 

	static ConcCreator< MGString, MPISolver<DIM_3D,EQN_EULER>, SolverBase>			gCreatorMPISolverEuler3D( "MPISOLVER_3D_EULER_DEFAULT");
	static ConcCreator< MGString, MPISolver<DIM_3D,EQN_NS>, SolverBase>			gCreatorMPISolverNS3D( "MPISOLVER_3D_NS_DEFAULT"); 
	static ConcCreator< MGString, MPISolver<DIM_3D,EQN_RANS_SA>, SolverBase>		gCreatorMPISolverRANSSA3D( "MPISOLVER_3D_RANS_SA_DEFAULT"); 
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

