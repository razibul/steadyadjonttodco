#ifndef __MPIGRIDASSISTANT_H__
#define __MPIGRIDASSISTANT_H__

#include "redvc/libredvcgrid/gridsimplex.h"
//#include "grid.h"

#include "libcoreconfig/configbase.h"
#include "libcoregeom/dimension.h"
#include "libcoregeom/vect.h"

#include "redvc/libredvccommon/gridassistant.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

//////////////////////////////////////////////////////////////////////
// class MPIGridAssistant
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class MPIGridAssistant : public GridAssistant<DIM>
{
	//typedef Grid<DIM>	TGrid;
 	typedef GridSimplex<DIM>	TGrid;
	typedef Geom::Vect<DIM>		GVec;
	 
public:
	MPIGridAssistant() {};
	virtual ~MPIGridAssistant() {};
	
	virtual void Create(const CfgSection* pcfgsec, TGrid* pgrd, DesignParameterCollection<DIM>* pdesparcoll, FreezingPointCollection<DIM>* pfreezeptcoll);
	virtual void PostCreateCheck() const;
	virtual void Init();
	
// 	virtual void	ComputePerturbation(const GVec& core, vector<GVec>& perturb) const;
// 	virtual MGFloat	ComputePerturbation(const GVec& core, const GVec& node) const;
// 	virtual void	Perturbate	(const vector< GVec >& perturb) const;
// 	virtual void	Unperturbate(const vector< GVec >& perturb) const;

// 	virtual void	RevertToInitial();
	virtual void	FixNodesBySum(vector<GVec>& vec) const;
	
private:
// 	TGrid*	mpGrid;
};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __MPIGRIDASSISTANT_H__