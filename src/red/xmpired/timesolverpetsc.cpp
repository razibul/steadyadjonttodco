#include "timesolverpetsc.h"
#include "libcorecommon/factory.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//



//ConcCreator< MGString, TimeSolverExplicit< EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE > >, TimeSolverBase< EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE > > >
//	gCreatorTimeSolverEulerExplicit( "TIMESOL_2D_EULER_EXPLICIT");



void init_MPIPETScTimeSolverImplicit()
{
	static ConcCreator< MGString, TimeSolverImplicitPETSC<1>, TimeSolverBase<1> >	gCreatorTimeSolverImplicitPETSCBSize1( "TIMESOL_BSIZE_1_PETSC");
	static ConcCreator< MGString, TimeSolverImplicitPETSC<2>, TimeSolverBase<2> >	gCreatorTimeSolverImplicitPETSCBSize2( "TIMESOL_BSIZE_2_PETSC");
	static ConcCreator< MGString, TimeSolverImplicitPETSC<4>, TimeSolverBase<4> >	gCreatorTimeSolverImplicitPETSCBSize4( "TIMESOL_BSIZE_4_PETSC");
	static ConcCreator< MGString, TimeSolverImplicitPETSC<5>, TimeSolverBase<5> >	gCreatorTimeSolverImplicitPETSCBSize5( "TIMESOL_BSIZE_5_PETSC");
	static ConcCreator< MGString, TimeSolverImplicitPETSC<6>, TimeSolverBase<6> >	gCreatorTimeSolverImplicitPETSCBSize6( "TIMESOL_BSIZE_6_PETSC");
	static ConcCreator< MGString, TimeSolverImplicitPETSC<7>, TimeSolverBase<7> >	gCreatorTimeSolverImplicitPETSCBSize7( "TIMESOL_BSIZE_7_PETSC");
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

