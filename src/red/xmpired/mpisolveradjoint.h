#ifndef __MPI_SOLVER_H__
#define __MPI_SOLVER_H__


#include "redvc/libredvccommon/solveradjoint.h"
#include "communicator.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


using namespace Geom;


//////////////////////////////////////////////////////////////////////
// MPISolver
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, EQN_TYPE ET>
class MPISolverAdjoint : public SolverAdjoint<DIM,ET>
{
public:
	typedef SolverAdjoint<DIM,ET>	TBaseSolver;
	
	MPISolverAdjoint() : TBaseSolver()		{}
	virtual ~MPISolverAdjoint()			{}

	virtual void	Create( const CfgSection* pcfgsec);
	virtual void	PostCreateCheck() const;
	virtual void	Init();

	virtual void	Solve();
	virtual void 	Solve(const bool& returnitercount, MGInt* itercount);
	
protected:
	Communicator			mComm;
};
//////////////////////////////////////////////////////////////////////



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

#endif // __MPI_SOLVER_H__

