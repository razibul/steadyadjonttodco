#ifndef __MPI_PERIODICBC_H__
#define __MPI_PERIODICBC_H__

//#include "libcoresystem/mgdecl.h"
//#include "libcoregeom/dimension.h"
//
//#include "libredcore/processinfo.h"
//
#include "redvc/libredvcgrid/gridsimplex.h"
//#include "libredcore/interface.h"
//
//#include "communicator.h"

#include "libredcore/equation.h"
#include "redvc/libredvccommon/periodicbcbase.h"

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_intersection.h"

#include <fstream>

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

	template<Dimension DIM>
	struct LocInterface
	{
		MGSize	tabnode[DIM];
		MGFloat	tabcoord[DIM];
		//MGSize remprocid;
	};

	template<Dimension DIM>
	struct LocBFace
	{
		Vect<DIM>	mpos[DIM];
	};

	template<typename T>
	class MPIAllGather
	{/*
		template<typename V, typename DUMMY = void>
		struct MPITypeSwitch
		{
			static MPI_Datatype type();
		};

		template<typename DUMMY>
		struct MPITypeSwitch<MGSize, DUMMY>
		{
			static MPI_Datatype type()	{ return MPI_INT; }
		};

		template<typename DUMMY>
		struct MPITypeSwitch<MGFloat, DUMMY>
		{
			static MPI_Datatype type()	{ return MPI_DOUBLE; }
		};*/
		static MPI_Datatype	mpitype()		{ return MPI_DATATYPE_NULL; }
	public:
		static void DoAllGather(vector<T>& out, vector<T>& in, vector<MGSize>& sizetab)
		{
			vector<MGSize> outsize(ProcessInfo::cNumProc(), out.size());
			vector<MGSize> insize(ProcessInfo::cNumProc(), 0);

			MPI_Alltoall(&outsize[0], 1, MPI_UNSIGNED, &insize[0], 1, MPI_UNSIGNED, MPI_COMM_WORLD);

			MGSize maxsize = *std::max_element(insize.begin(), insize.end());
			MGSize sizeglob = ProcessInfo::cNumProc() * maxsize;

			vector<MGSize> sizeoutoffset(ProcessInfo::cNumProc(), out.size());
			sizetab.resize(ProcessInfo::cNumProc(), 0);
			MPI_Alltoall(&sizeoutoffset[0], 1, MPI_UNSIGNED, &sizetab[0], 1, MPI_UNSIGNED, MPI_COMM_WORLD);

			out.resize(maxsize, 0);

			cout << "Proc: " << ProcessInfo::cProcId() << "\tbef: " << out.size() << "\taft: " << sizeglob << endl;

			in.resize(sizeglob, 0);
			//MPI_Allgather(&out[0], maxsize, MPITypeSwitch<T>::type(), &in[0], maxsize, MPITypeSwitch<T>::type(), MPI_COMM_WORLD);
			MPI_Allgather(&out[0], maxsize, mpitype(), &in[0], maxsize, mpitype(), MPI_COMM_WORLD);
		}
	};

	template<>
	MPI_Datatype	MPIAllGather<MGSize>::mpitype()		{ return MPI_INT; }
	template<>
	MPI_Datatype	MPIAllGather<MGFloat>::mpitype()	{ return MPI_DOUBLE; }

	//////////////////////////////////////////////////////////////////////
	// class MPIPeriodicBC
	//////////////////////////////////////////////////////////////////////

	template<Dimension DIM, EQN_TYPE ET>
	class MPIPeriodicBC : public PeriodicBCBase<DIM, ET>
	{
	public:
		enum { ESIZE = EquationDef<DIM, ET>::SIZE };

		typedef GridSimplex<DIM>			TGrid;
		typedef EquationDef<DIM, ET>		EqDef;
		typedef typename EqDef::FVec		FVec;

		typedef KeyData<MGSize, PeriodicKeyData<DIM> >	PeriodicKey;
		typedef vector<PeriodicKey>						ColPeriodic;

		void Init(TGrid* grid, Solution<DIM, ET>* sol, const CfgSection* pphysect);

		virtual void GetPeriodicVar(const MGSize& id, FVec& v) const;
		virtual void UpdateSolution();

	private:
		void InitLocInterface();
		void FindMapping(
			const MGSize& surfid,
			const Vect<DIM>& vct,
			const MGFloat& angle,
			vector< KeyData<MGSize, pair<MGSize, Vect<DIM> > > >& globtabkey,
			vector < KeyData<MGSize, Key<DIM> >	>& globtabbfacekey);

		void CollectSurf(
			const MGSize& surfid,
			vector< KeyData<MGSize, pair<MGSize, Vect<DIM> > > >& globtabkey,
			vector < KeyData<MGSize, Key<DIM> >	>& globtabbfacekey);

		void SetGlobalInterface(
			vector< KeyData<MGSize, pair<MGSize, Vect<DIM> > > >& globtabkey,
			vector < KeyData<MGSize, Key<DIM> >	>& globtabbfacekey);

		ColPeriodic										mcolperiodic;
		TGrid*											mpGrid;
		Solution<DIM, ET>*								mpsol;
		vector< KeyData<MGSize, LocInterface<DIM> > >	mcollocinterace;
	};

	template<Dimension DIM, EQN_TYPE ET>
	void MPIPeriodicBC<DIM, ET>::Init(TGrid* grid, Solution<DIM, ET>* sol, const CfgSection* pphysect)
	{
		mpsol = sol;
		mpGrid = grid;
		this->ReadConfig(pphysect);
		InitLocInterface();
	}

	template<Dimension DIM, EQN_TYPE ET>
	void MPIPeriodicBC<DIM, ET>::GetPeriodicVar(const MGSize& id, FVec& v) const
	{
		typename vector< KeyData<MGSize, LocInterface<DIM> > >::const_iterator citr = lower_bound(mcollocinterace.begin(), mcollocinterace.end(), KeyData<MGSize, LocInterface<DIM> >(id));
		if ((citr != mcollocinterace.end()) && (citr->first == id) )
		{
			for (MGSize i = 0; i < ESIZE; ++i)
			{
				v[i] = 0.;
			}

			for (MGSize i = 0; i < DIM; ++i)
			{
				for (MGSize j = 0; j < ESIZE; ++j)
				{
				//	v[j] += mlocsol[citr->second.tabnode[i]][j] * citr->second.tabcoord[i];
				}
			}
		}
		else
		{
			THROW_INTERNAL("MPIPeriodicBC::GetPeriodicVar id not found");
		}
	}

	template<Dimension DIM, EQN_TYPE ET>
	void MPIPeriodicBC<DIM, ET>::CollectSurf(
		const MGSize& surfid,
		vector< KeyData<MGSize, pair<MGSize, Vect<DIM> > > >& globtabkey,
		vector < KeyData<MGSize, Key<DIM> >	>& globtabbfacekey)
	{
		for (MGSize inx = 0; inx < mpGrid->SizeBFaceTab(); ++inx)
		{
			CGeomBFace<DIM> bface = mpGrid->cBFace(inx);
			if (bface.cSurfId() == surfid)
			{
			//	bool isfullowned = true;
				MGSize tmp[DIM];
				for (MGSize i = 0; i < DIM; ++i)
				{
					MGSize globnodeid = bface.cGId(i);
					if (mpGrid->IsOwned(globnodeid))
					{
						KeyData<MGSize, pair<MGSize, Vect<DIM> > > key;
						key.first = globnodeid;
						key.second.first = ProcessInfo::cProcId();
						key.second.second = bface.cNode(i).cPos();

						globtabkey.push_back(key);

						tmp[i] = globnodeid;
					}
					//else
			//		{
			//			isfullowned = false;
			//		}
				}
			//	if (isfullowned)
			//	{
					KeyData<MGSize, Key<DIM> > key;
					key.first = inx;
					for (MGSize i = 0; i < DIM; ++i)
					{
						key.second.rElem(i) = tmp[i];
					}
					globtabbfacekey.push_back(key);
			//	}
			}
		}

		sort(globtabkey.begin(), globtabkey.end());
		typename vector< KeyData<MGSize, pair<MGSize, Vect<DIM> > > >::iterator uitr = unique(globtabkey.begin(), globtabkey.end());
		globtabkey.erase(uitr, globtabkey.end());

		//ostringstream oss3;

		//oss3 << "dump_globtabkey_" << ProcessInfo::cProcId();
		//ofstream of3;
		////MGString str(oss.str());
		//of3.open(oss3.str().c_str());

		//for (MGSize i = 0; i < globtabkey.size(); i++)
		//{
		//	of3 << globtabkey[i].first << "\t" << globtabkey[i].second.first << "\t" << globtabkey[i].second.second.cX(0) << "\t" << globtabkey[i].second.second.cX(1) << endl;
		//}
		//of3.close();

		//////////////////////////////////////////////////////////////
		vector< MGFloat>	globtabpos;
		vector< MGSize>		globtabnode;
		vector< MGSize>		globtabbface;

		vector< MGFloat>	tabpos;
		vector< MGSize>		tabnode;
		vector< MGSize>		tabbface;

		for (MGSize i = 0; i < globtabkey.size(); ++i)
		{
			tabnode.push_back(globtabkey[i].first);
			tabnode.push_back(globtabkey[i].second.first); //block by global node id and process id: [globid procid] [globid2 procid2]
			for (MGSize j = 0; j < DIM; ++j)
			{
				tabpos.push_back(globtabkey[i].second.second.cX(j)); //block by DIM e.g.: [x y z] [x2 y2 z2] ...
			}
		}

		for (MGSize i = 0; i < globtabbfacekey.size(); ++i)
		{
			tabbface.push_back(globtabbfacekey[i].first);
			for (MGSize j = 0; j < DIM; ++j)
			{
				tabbface.push_back(globtabbfacekey[i].second.cElem(j));
			}
		}

		vector<MGSize> tabnodesize;
		MPIAllGather<MGSize>::DoAllGather(tabnode, globtabnode, tabnodesize);
		vector<MGSize> tabpossize;
		MPIAllGather<MGFloat>::DoAllGather(tabpos, globtabpos, tabpossize);
		vector<MGSize> tabbfacesize;
		MPIAllGather<MGSize>::DoAllGather(tabbface, globtabbface, tabbfacesize);

		//MGSize bfacesizecheck = std::accumulate(tabbfacesize.begin(), tabbfacesize.end(), 0);

		//for (size_t i = 0; i < tabbfacesize.size(); i++)
		//{
		//	cout << "Proc:\t" << ProcessInfo::cProcId() << "\tsize: " << tabbfacesize[0] << " " << tabbfacesize[1] << " " << tabbfacesize[2] << " " << globtabbface.size() << endl;
		//cout << "Glob size: " << globtabbface.size() << endl;
		//}


		globtabbfacekey.clear();
		MGSize maxsize = globtabbface.size() / ProcessInfo::cNumProc();

		vector<MGSize>::const_iterator it = globtabbface.begin();

		//	ostringstream oss;
		//	oss << "dump_3" << ProcessInfo::cProcId();
		//	ofstream of;
		//	of.open(oss.str().c_str());
		//	cout << tabbfacesize.size() << endl;
		//	of << globtabbface.size() << endl << endl;
		//	MGSize cc = 0;
		for (MGSize i = 0; i < tabbfacesize.size(); ++i)
		{
			//cout << "SIZE: " << globtabbface.end() - it << endl;
			//cout << "BEF" << endl;
			MGSize count = 0;
			while (count < tabbfacesize[i])
			{
				KeyData<MGSize, Key<DIM> > key;
				key.first = *it++;
				++count;
				//		++cc;
				for (MGSize j = 0; j < DIM; ++j)
				{
					key.second.rElem(j) = *it++;
					++count;
					//		++cc;
				}
				globtabbfacekey.push_back(key);
			}
			//	of << "PROC: " << ProcessInfo::cProcId() << "SIZE: " << tabbfacesize[i] << "\tCOUNT: " << count << "\tIT: " << it - globtabbface.begin()<< "\tCC: "<< cc << endl;
			//cout << "AFT" << endl;
			//cout << endl << endl;
			//cout << "Process:\t" << ProcessInfo::cProcId() << " " << count << " " << maxsize - tabbfacesize[i] << endl;
			it += maxsize - tabbfacesize[i];
		}

		globtabkey.clear();
		maxsize = globtabnode.size() / ProcessInfo::cNumProc();

		it = globtabnode.begin();
		//vector<MGFloat>::const_iterator itpos = globtabpos.begin();

		for (MGSize i = 0; i < tabnodesize.size(); ++i)
		{
			MGSize count = 0;
			while (count < tabnodesize[i])
			{
				KeyData<MGSize, pair<MGSize, Vect<DIM> > > key;
				key.first = *it++;
				++count;

				key.second.first = *it++;
				++count;

				globtabkey.push_back(key);
			}
			it += maxsize - tabnodesize[i];
		}

		maxsize = globtabpos.size() / ProcessInfo::cNumProc();

		//it = globtabnode.begin();
		vector<MGFloat>::const_iterator itpos = globtabpos.begin();

		MGSize idx = 0;
		for (MGSize i = 0; i < tabpossize.size(); ++i)
		{
			MGSize count = 0;
			while (count < tabpossize[i])
			{
				for (MGSize j = 0; j < DIM; j++)
				{
					globtabkey[idx].second.second.rX(j) = *itpos++;
					++count;
				}
				++idx;
			}
			itpos += maxsize - tabpossize[i];
		}

		sort(globtabkey.begin(), globtabkey.end());
		typename vector< KeyData<MGSize, pair<MGSize, Vect<DIM> > > >::iterator uit = unique(globtabkey.begin(), globtabkey.end());
		globtabkey.erase(uit, globtabkey.end());

		sort(globtabbfacekey.begin(), globtabbfacekey.end());
		typename vector < KeyData<MGSize, Key<DIM> > >::iterator uitbface = unique(globtabbfacekey.begin(), globtabbfacekey.end());
		globtabbfacekey.erase(uitbface, globtabbfacekey.end());
	}

	template<Dimension DIM, EQN_TYPE ET>
	void MPIPeriodicBC<DIM, ET>::InitLocInterface()
	{


	//	for (MGSize i = 0; i < this->mparams.size(); ++i)
		{
			switch (this->mparams[0].type)
			{
			case PRD_TRANS:
			{
				MGSize srcid = this->mparams[0].src;
				MGSize trgid = this->mparams[0].trg;
				Vect<DIM> vct = this->mparams[0].vct;

				vector< KeyData<MGSize, pair<MGSize, Vect<DIM> > > >	globtabkey; //glob node id, owner proc id, position
				vector < KeyData<MGSize, Key<DIM> >	>					globtabbfacekey; //loc face id, glob node ids
				CollectSurf(trgid, globtabkey, globtabbfacekey);




				//ostringstream oss2;

				//oss2 << "dump_globkey_" << ProcessInfo::cProcId();
				//ofstream of2;
				////MGString str(oss.str());
				//of2.open(oss2.str().c_str());

				//for (vector<MGSize>::const_iterator it = globtabnode.begin();
				//	it < globtabnode.end();
				//	++it)
				//{
				//	of2 << *it << endl;
				//}


				//for (size_t i = 0; i < globtabkey.size(); ++i)
				//{
				//	//of2 << "Proc:\t" << ProcessInfo::cProcId() << "\tkey " << globtabbface[i];
				//	//++i;
				//	//for (MGSize j = 0; j < DIM; ++j, ++i)
				//	//{
				//	//	of2 << " " << globtabbface[i];
				//	//}
				//	//of2 << endl;
				//	of2 << "UProc:\t" << ProcessInfo::cProcId() << "\tkey " << globtabkey[i].first << "\tPROC: " << globtabkey[i].second.first
				//		<< " " << globtabkey[i].second.second.cX(0) << " " << globtabkey[i].second.second.cX(1) << endl;
				//}
				//of2.close();

			//	of << cc << endl << endl;
			//	of.close();

				//ostringstream oss2;

				//oss2 << "dump" << ProcessInfo::cProcId();
				//ofstream of2;
				////MGString str(oss.str());
				//of2.open(oss2.str().c_str());

				//for (size_t i = 0; i < globtabbfacekey.size(); ++i) 
				//{
				//	//of2 << "Proc:\t" << ProcessInfo::cProcId() << "\tkey " << globtabbface[i];
				//	//++i;
				//	//for (MGSize j = 0; j < DIM; ++j, ++i)
				//	//{
				//	//	of2 << " " << globtabbface[i];
				//	//}
				//	//of2 << endl;
				//	of2 << "Proc:\t" << ProcessInfo::cProcId() << "\tkey " << globtabbfacekey[i].first
				//		<< " " << globtabbfacekey[i].second.cElem(0) << " " << globtabbfacekey[i].second.cElem(1) << endl;
				//}
				//of2.close();
				//////////////////////////////////////////////////////////////

				break;
			}
			case PRD_ANGLE:
				THROW_INTERNAL("Angle periodic bc not implemented.");
				break;
			case PRD_NONE:
				//THROW_INTERNAL("Angle periodic bc not implemented.");
				break;
			default:
				THROW_INTERNAL("Unknown periodic bc type.");
				break;
			}
		}
	}

	template<Dimension DIM, EQN_TYPE ET>
	void MPIPeriodicBC<DIM, ET >::FindMapping(
		const MGSize& surfid,
		const Vect<DIM>& vct, 
		const MGFloat& angle,
		vector< KeyData<MGSize, pair<MGSize, Vect<DIM> > > >& globtabkey,
		vector < KeyData<MGSize, Key<DIM> >	>& globtabbfacekey)
	{
		typedef KeyData<MGSize, pair<MGSize, Vect<DIM> > >	GlobKey;
		typedef vector< GlobKey >							ColGlobKey;
		
		typedef HFGeom::GVector<MGFloat, DIM> Vector;

		for (MGSize inx = 0; inx < mpGrid->SizeBFaceTab(); ++inx)
		{
			//for (typename vector< BFace<DIM, DIM> >::const_iterator citr = mpGrid->c mtabBFace.begin();
			//	citr < mtabBFace.end();
			//	++citr)
			//{
			CGeomBFace<DIM> bface = mpGrid->cBFace(inx);

			if (bface.cSurfId() == surfid)
			{
				for (MGSize trgbfaceid = 0; trgbfaceid < globtabbfacekey.size(); ++trgbfaceid)
				{
					Vector tabpos[DIM];
					MGSize trgcellid = trgbfaceid;	//id IN vector, not id in grid (local nor global)
				//	MGSize trgcellid = globtabbfacekey[trgbfaceid].first;	//not unique...
					for (MGSize i = 0; i < DIM; ++i)
					{
						const MGSize trgnodeid = globtabbfacekey[trgbfaceid].second.cElem(i);

						typename ColGlobKey::const_iterator cit = lower_bound(globtabkey.begin(), globtabkey.end(), GlobKey(trgnodeid));
						if ((cit != globtabkey.end()) && (cit->first == trgnodeid))
						{
							for (MGSize j = 0; j < DIM; ++j)
							{
								tabpos[i].rX(j) = cit->second.second.cX(j);
							}
						}
						else
						{
							THROW_INTERNAL("Unknown nodeid in MPIPeriodicBC<DIM, ET >::FindMapping\n");
						}
					}

					MGFloat maxedgesize = 0.;
					for (MGSize i = 0; i < DIM - 1; ++i)
					{
						MGFloat len = 0.;
						Vector tmp(tabpos[i] - tabpos[(i + 1) % DIM]);
						for (MGSize ii = 0; ii < DIM; ++ii)
						{
							len += tmp.cX(ii)*tmp.cX(ii);
						}
						len = sqrt(len);
						if (len > maxedgesize)
						{
							maxedgesize = len;
						}
					}

					HFGeom::SubSimplex<MGFloat, DIM - 1, DIM> subb((typename HFGeom::SubSimplex<MGFloat, DIM - 1, DIM>::ARRAY(tabpos)));

					for (MGSize i = 0; i < bface.Size(); ++i)
					{
						Vector pos;
						const MGSize& nodeid = bface.cId(i);

						for (MGSize j = 0; j < DIM; j++)
						{
							pos.rX(j) = mpGrid->cNode(nodeid).cPos().cX(j) + vct.cX(j);
						}

						HFGeom::SubSimplex<MGFloat, 0, DIM> suba((typename  HFGeom::SubSimplex<MGFloat, 0, DIM>::ARRAY(pos)));

						HFGeom::IntersectionProx<MGFloat, MGFloat, 0, DIM - 1, DIM> inter(suba, subb);
						inter.Execute();

						MGFloat	tabB[DIM];
						inter.GetBBaryCoords(tabB);

						bool bin = true;
						for (MGSize i = 0; i<DIM; ++i)
						{
							tabB[i] /= inter.cDet();
							//cout << tabB[i] << "\t";
							if (tabB[i] < -maxedgesize*1e-4)
							{
								bin = false;
							}
						}
						//cout << endl;
						if (bin)
						{
							PeriodicKey key;
							key.first = nodeid;
							//key.second.first = trginx;
							key.second.mcellid = trgcellid;
							for (MGSize j = 0; j < DIM; ++j)
							{
								//key.second.second.rElem(j) = tabB[j];
								key.second.mtabcoord.rElem(j) = tabB[j];
							}

							mcolperiodic.push_back(key);
						}
					}					
				}
			}
		}
		//bar.Finish();
	}

	template<Dimension DIM, EQN_TYPE ET>
	void MPIPeriodicBC<DIM, ET>::SetGlobalInterface(
		vector< KeyData<MGSize, pair<MGSize, Vect<DIM> > > >& globtabkey,
		vector< KeyData<MGSize, Key<DIM> >	>& globtabbfacekey)
	{
		typedef KeyData<MGSize, pair<MGSize, Vect<DIM> > >	GlobKey;
		typedef vector< GlobKey >							ColGlobKey;

		vector< KeyData<MGSize, MGSize> >	nodemap;

		for (MGSize  i = 0; i < mcolperiodic.size(); ++i)
		{
			const PeriodicKey& key = mcolperiodic[i];

			//MGSize localnodeid = key.first;
			MGSize bfaceid = key.second.mcellid;

			KeyData< MGSize, LocInterface<DIM> >	interfkey;
			interfkey.first = key.first;

			for (MGSize j = 0; j < DIM; ++j)
			{
				MGSize globnodeid = globtabbfacekey[bfaceid].second.cElem(j);
				typename ColGlobKey::const_iterator cit = lower_bound(globtabkey.begin(), globtabkey.end(), GlobKey(globnodeid));
				if ((cit != globtabkey.end()) && (cit->first == globnodeid))
				{
					nodemap.push_back(KeyData<MGSize, MGSize>(cit - globtabkey.begin(), 0));
					interfkey.second.tabnode[j] = cit - globtabkey.begin();
					interfkey.second.tabcoord[j] = key.second.mtabcoord.rElem(j);
				}
				else
				{
					THROW_INTERNAL("Error in MPIPeriodicBC<DIM, ET>::SetGlobalInterface: nodeid not found\n");
				}
			}
			mcollocinterace.push_back(interfkey);
		}

		MGSize sizenodetabold = mpGrid->IOSizeNodeTab();
		mpGrid->IOTabNodeResize(sizenodetabold + nodemap.size());
		mpsol->Resize(sizenodetabold + nodemap.size());

		for (MGSize i = 0; i < nodemap.size(); ++i)
		{
			nodemap[i].second = sizenodetabold + i;
		}

		sort(nodemap.begin(), nodemap.end());
		typename vector< KeyData<MGSize, MGSize> >::iterator it = unique(nodemap.begin(), nodemap.end());
		nodemap.erase(it, nodemap.end());

		for (MGSize i = 0; i < mcollocinterace.size(); ++i)
		{
			for (MGSize j = 0; j < DIM; ++j)
			{
				MGSize nodeid = mcollocinterace[i].second.tabnode[j];
			}
			
		}
	}

	template<Dimension DIM, EQN_TYPE ET>
	void MPIPeriodicBC<DIM, ET>::UpdateSolution()
	{

	}
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


#endif // __MPI_PERIODICBC_H__
