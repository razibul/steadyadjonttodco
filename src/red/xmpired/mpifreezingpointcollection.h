#ifndef __MPIFREEZINGPOINTCOLLECTION_H__
#define __MPIFREEZINGPOINTCOLLECTION_H__

#include "redvc/libredvccommon/freezingpointcollection.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
// class Freezing Points Collection
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class MPIFreezingPointCollection : public FreezingPointCollection<DIM>
{
public:
	typedef GridSimplex<DIM>	TGrid;
	typedef Geom::Vect<DIM>		GVec;
	
	MPIFreezingPointCollection() {};
	virtual ~MPIFreezingPointCollection() {};
	
// 	static MGString	Info()
// 	{
// 		ostringstream os;
// 		os << "FreezingPointCollection< " << Geom::DimToStr(DIM) << " >";
// 		return os.str();
// 	}
	
	virtual void Create(const CfgSection* pcfgsec, const TGrid *pgrd);
	virtual void PostCreateCheck() const;
	virtual void Init();
	
	virtual void FindGlobalNearestNode(const MGSize& indx, const MGFloat& dist, const GVec& pos, MGSize* owner_rank, MGSize* owner_indx, GVec* owner_pos);
	
private:
	
	
};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif //__MPIFREEZINGPOINTCOLLECTION_H__