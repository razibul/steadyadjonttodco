#include "mpisetup.h"
#include "libred/setup.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


void init_MPISolver();
void init_MPISolverAdjoint();
void init_MPIPETScSpaceSolver();
void init_MPIPETScTimeSolverImplicit();
void init_MPITrilinosTimeSolverImplicit();
void init_MPITRILINOSSpaceSolver();


void init_MPISolverOptim();
void init_MPISolverAdjoint();
void init_MPIPETScExecAdjoint();
void init_MPIFreezingPointCollection();
void init_MPIGridAssistant();

void RegisterLib_MPI()
{
	init_MPISolver();
	init_MPISolverAdjoint();
	init_MPIFreezingPointCollection();
	init_MPIGridAssistant();
	
#ifdef WITH_PETSC
	init_MPIPETScSpaceSolver();
	init_MPIPETScTimeSolverImplicit();
#endif // WITH_PETSC

#ifdef WITH_TRILINOS
    init_MPITRILINOSSpaceSolver();
    init_MPITrilinosTimeSolverImplicit();
#endif // WITH_TRILINOS

	
//#ifdef WITH_NLOPT
	init_MPISolverOptim();
	init_MPIPETScExecAdjoint();
//#endif //WITH_NLOPT
	
}


void MPIRegisterLibraries()
{
	RegisterLibraries();
	RegisterLib_MPI();
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

