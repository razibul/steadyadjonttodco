#include "mpitrilinosspacesolver.h"
#include "libcorecommon/factory.h"

/*<< Trilinos includes */
#include "Epetra_CrsGraph.h"
#include "Epetra_BlockMap.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPITRILINOSSpaceSolver<DIM,ET,MAPPER>::Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata)
{
    SpaceSolver<DIM,ET,MAPPER>::Create( pcfgsec, pphys, pdata);
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPITRILINOSSpaceSolver<DIM,ET,MAPPER>::TRILINOSInitOwnedIdRange( pair<MGSize,MGSize>& range, vector<MGSize>& tabg) const
{ 
    range = this->cData().cGrid().cOwnedIds();

    tabg.resize( this->cData().cGrid().SizeNodeTab() );
    for ( MGSize i=0; i<tabg.size(); ++i)
        tabg[i] = this->cData().cGrid().cNode(i).cGId();
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPITRILINOSSpaceSolver<DIM,ET,MAPPER>::TRILINOSResizeVct( Vec& vct) const        
{
    THROW_INTERNAL(" Not implemented ");
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPITRILINOSSpaceSolver<DIM,ET,MAPPER>::TRILINOSResizeGraph( const Epetra_BlockMap& map, Epetra_CrsGraph*& graph ) const
{
    vector<vector<int> > nodeConn( this->cData().cGrid().SizeNodeTab() );
    for ( MGSize ic=0; ic<this->cData().cGrid().SizeCellTab(); ++ic)
    {
        const CGeomCell<DIM>& cgcell = this->cData().cGrid().cCell( ic );
        for ( MGSize i=0; i<cgcell.Size(); ++i)
        {
            for ( MGSize j=0; j<cgcell.Size(); ++j)
            {
                 nodeConn[ cgcell.cId(i) ].push_back( cgcell.cId(j) );
            }
        }
    }

    for(MGSize in=0; in<nodeConn.size(); ++in)
    {
        sort( nodeConn[in].begin(), nodeConn[in].end() );
        nodeConn[in].erase( unique( nodeConn[in].begin(), nodeConn[in].end() ), nodeConn[in].end() );
    }

    std::vector<int> nnz_per_row(nodeConn.size());
    std::vector<int>::iterator iter1 = nnz_per_row.begin();
    vector<vector<int> >::iterator iter2 = nodeConn.begin();
    for( ; iter2 != nodeConn.end(); ++iter1, ++iter2)
    {
        *iter1 = iter2->size();
    }

    graph = new Epetra_CrsGraph ( Copy, map, nnz_per_row.data(), true );
    iter1 = nnz_per_row.begin();
    iter2 = nodeConn.begin();
    int err = 0;
    int GlobalRow = 0;
    for( ; iter2 != nodeConn.end(); ++iter1, iter2)
    {
        GlobalRow = this->cData().cGrid().cNode( nodeConn.end() - iter2 ).cGId();
        err = graph->InsertMyIndices( GlobalRow, *iter1, iter2->data() );
        std::cout << "Process " << Comm.MyPID() << " inserts to global row " << GlobalRow << " Local row " << i << " | err: " << err << std::endl;
    }
    graph.FillComplete();
}



template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPITRILINOSSpaceSolver<DIM,ET,MAPPER>::TRILINOSResNumJacob( Vec& vec, Mat& vmtx, const vector<MGFloat>& tabDT)
{
    THROW_INTERNAL(" Not implemented ");
}



void init_MPITRILINOSSpaceSolver()
{
    //////////////////////////////////////////////////////////////////////
    // 2D
    
    // Euler
    static ConcCreator<
        MGString,
        MPITRILINOSSpaceSolver< Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SplitingFull::Block<1> >,
        SpaceSolverBase< Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SplitingFull::Block<1> >
    >   gCreatorMPITRILINOSSpaceSolverEulerRDS2D( "MPITRILINOS_SPACESOL_2D_EULER_RDS");

    // NS
    static ConcCreator<
        MGString,
        MPITRILINOSSpaceSolver< Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SplitingFull::Block<1> >,
        SpaceSolverBase< Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SplitingFull::Block<1> >
    >   gCreatorMPITRILINOSSpaceSolverNSRDS2D( "MPITRILINOS_SPACESOL_2D_NS_RDS");

    // RANS SA with splitting
    static ConcCreator<
        MGString,
        MPITRILINOSSpaceSolver< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >,
        SpaceSolverBase< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >
    >   gCreatorMPITRILINOSSpaceSolverRANS1eqFlowRDS2D( "MPITRILINOS_SPACESOL_2D_RANS_SA_RDS");

    static ConcCreator<
        MGString,
        MPITRILINOSSpaceSolver< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >,
        SpaceSolverBase< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >
    >   gCreatorMPITRILINOSSpaceSolverRANS1eqTurbRDS2D( "MPITRILINOS_SPACESOL_2D_RANS_SA_RDS");

    
    //////////////////////////////////////////////////////////////////////
    // 3D

    // Euler
    static ConcCreator<
        MGString,
        MPITRILINOSSpaceSolver< Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SplitingFull::Block<1> >,
        SpaceSolverBase< Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SplitingFull::Block<1> >
    >   gCreatorMPITRILINOSSpaceSolverEulerRDS3D( "MPITRILINOS_SPACESOL_3D_EULER_RDS");
 
    // NS
    static ConcCreator<
        MGString,
        MPITRILINOSSpaceSolver< Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SplitingFull::Block<1> >,
        SpaceSolverBase< Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SplitingFull::Block<1> >
    >   gCreatorMPITRILINOSSpaceSolverNSRDS3D( "MPITRILINOS_SPACESOL_3D_NS_RDS");

    // RANS SA with splitting
    static ConcCreator<
        MGString,
        MPITRILINOSSpaceSolver< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >,
        SpaceSolverBase< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >
    >   gCreatorMPITRILINOSSpaceSolverRANS1eqFlowRDS3D( "MPITRILINOS_SPACESOL_3D_RANS_SA_RDS");

    static ConcCreator<
        MGString,
        MPITRILINOSSpaceSolver< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >,
        SpaceSolverBase< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >
    >   gCreatorMPITRILINOSSpaceSolverRANS1eqTurbRDS3D( "MPITRILINOS_SPACESOL_3D_RANS_SA_RDS");

}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

