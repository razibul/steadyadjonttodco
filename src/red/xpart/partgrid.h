#ifndef __PARTGRID_H__
#define __PARTGRID_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libcoregeom/vect.h"

#include "redvc/libredvcgrid/cell.h"
#include "redvc/libredvcgrid/bface.h"

#include "libcoreio/gridfacade.h"
#include "libcoreio/intffacade.h"

#include "partsol.h"


template <Dimension DIM>
class DePartitioner;


//////////////////////////////////////////////////////////////////////
// class InterfInfo
//////////////////////////////////////////////////////////////////////
class InterfInfo
{
public:
	MGSize	mIdLocNode;
	MGSize	mIdRemProc;
	MGSize	mIdRemNode;
};


//////////////////////////////////////////////////////////////////////
// class PartNode
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class PartNode : public Vect<DIM>
{
public:
};



//////////////////////////////////////////////////////////////////////
// class PartGrid
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class PartGrid : public IO::GridBndFacade, public IO::GridReadFacade/*, public IO::GridWriteFacade*/, public IO::IntfFacade
{
	friend class DePartitioner<DIM>;

public:
	PartGrid( const MGString& name="-") : mGrdName( name)	{}
	virtual	~PartGrid()		{}

	const MGString&	Name() const	{ return mGrdName;}

	MGSize&			rNProc()		{ return mNProc;}
	const MGSize&	cNProc() const	{ return mNProc;}

	MGSize	SizeNodeTab() const		{ return mtabNode.size(); }
	MGSize	SizeCellTab() const		{ return mtabCell.size(); }
	MGSize	SizeInCellTab() const	{ return mtabInCell.size(); }
	MGSize	SizeExCellTab() const	{ return mtabExCell.size(); }
	MGSize	SizeBFaceTab() const	{ return mtabBFace.size(); }

	const PartNode<DIM>&			cNode( const MGSize& i) const	{ return mtabNode[i];}
	const RED::Cell<DIM,DIM+1>&		cCell( const MGSize& i) const	{ return mtabCell[i];}
	const RED::BFace<DIM,DIM>&		cBFace( const MGSize& i) const	{ return mtabBFace[i];}

	//PartSol&		rSolution()			{ return mSolution; }
	//const PartSol&	cSolution() const	{ return mSolution; }

	vector<PartSol>&		rTabSolution()			{ return mtabSolution; }
	const vector<PartSol>&	cTabSolution() const	{ return mtabSolution; }


///////
	void	PushBackInCell( const RED::Cell<DIM,DIM+1>& cell)		{ mtabInCell.push_back( cell);}
	void	PushBackExCell( const RED::Cell<DIM,DIM+1>& cell)		{ mtabExCell.push_back( cell);}

	void	PushBackBFace( const RED::BFace<DIM,DIM>& bface)		{ mtabBFace.push_back( bface);}

	void	PushBackIntfNode( const pair<MGSize,MGSize>& par)		{ mtabInfo.push_back( par);}


	void	CreateNodeMap();
	void	ResetCellIds();
	void	ResetBFaceIds();
	void	SetupNodes( const PartGrid<DIM>& grd);
	void	UpdateInterface( vector< PartGrid<DIM> >& tabgrd);

protected:
	MGSize	LocalNodeId( const MGSize& id);

	RED::Cell<DIM,DIM+1>&		rCell( const MGSize& i)		{ return mtabCell[i];}
	RED::BFace<DIM,DIM>&		rBFace( const MGSize& i) 	{ return mtabBFace[i];}

protected:

	// grid facade
	virtual Dimension	Dim() const											{ return DIM;}

	virtual MGSize	IOSizeNodeTab() const									{ return mtabNode.size(); }
	virtual MGSize	IOSizeCellTab( const MGSize& type) const;
	virtual MGSize	IOSizeBFaceTab( const MGSize& type) const;

	virtual void	IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	virtual void	IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	virtual void	IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id) const;

	virtual MGSize	IONumCellTypes() const									{ return 1;}
	virtual MGSize	IONumBFaceTypes() const									{ return 1;}
	virtual MGSize	IOCellType( const MGSize& i) const;
	virtual MGSize	IOBFaceType( const MGSize& i) const;

	virtual void	IOTabNodeResize( const MGSize& n)						{ mtabNode.resize( n);}
	virtual void	IOTabCellResize( const MGSize& type, const MGSize& n);
	virtual void	IOTabBFaceResize( const MGSize& type, const MGSize& n);

	virtual void	IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id);
	virtual void	IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id );
	virtual void	IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id );
	virtual void	IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id );

	virtual void	IOGetName( MGString& name) const						{ name = mGrdName;}
	virtual void	IOSetName( const MGString& name)						{ mGrdName = name;}

	// grid bnd facade
	virtual MGSize	IOSizeBNodeTab() const	{ return 0;}

	virtual void	IOGetBNodePos( vector<MGFloat>& tabx, const MGSize& id) const	{}
	virtual void	IOGetBNodeVn( vector<MGFloat>& tabx, const MGSize& id) const	{}
	virtual void	IOGetBNodePId( MGSize& idparent, const MGSize& id) const		{}
	virtual void	IOGetBFaceBIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const {}

	
	// interface facade
	virtual MGSize	IOGetProcNum() const				{ return mNProc;}
	virtual void	IOSetProcNum( const MGSize& n)		{ mNProc = n;}

	virtual MGSize	IOTabIntfSize() const				{ return mtabInterf.size();}
	virtual void	IOTabIntfResize( const MGSize& n)	{ mtabInterf.resize( n);}

	virtual void	IOGetIds( vector<MGSize>& tabid, const MGSize& id) const;
	virtual void	IOSetIds( const vector<MGSize>& tabid, const MGSize& id );


private:

	MGSize			mNProc;
	MGString		mGrdName;

	vector< PartNode<DIM> >			mtabNode;
	vector< RED::Cell<DIM,DIM+1> >	mtabCell;
	vector< RED::BFace<DIM,DIM> >	mtabBFace;
	vector< InterfInfo >			mtabInterf;

	vector< RED::Cell<DIM,DIM+1> >	mtabInCell;
	vector< RED::Cell<DIM,DIM+1> >	mtabExCell;

	vector< pair<MGSize,MGSize> >	mtabInfo;
	map<MGSize,MGSize>				mmapNode;

	//PartSol							mSolution;
	vector<PartSol>					mtabSolution;
};
//////////////////////////////////////////////////////////////////////



template <Dimension DIM>
MGSize PartGrid<DIM>::IOSizeCellTab( const MGSize& type) const				
{ 
	if ( type == (DIM+1) )
		return mtabCell.size();

	return 0;
}


template <Dimension DIM>
MGSize PartGrid<DIM>::IOSizeBFaceTab( const MGSize& type) const				
{ 
	if ( type == DIM )
		return mtabBFace.size();

	return 0;
}

template <Dimension DIM>
MGSize PartGrid<DIM>::IOCellType( const MGSize& i) const						
{ 
	if ( i == 0 )
		return DIM+1;

	THROW_INTERNAL( "PartGrid<DIM>::IOCellType - too many types");

	return 0;
}


template <Dimension DIM>
MGSize PartGrid<DIM>::IOBFaceType( const MGSize& i) const						
{ 
	if ( i == 0 )
		return DIM;

	THROW_INTERNAL( "PartGrid<DIM>::IOBFaceType - too many types");

	return 0;
}

template <Dimension DIM>
void PartGrid<DIM>::IOTabCellResize( const MGSize& type, const MGSize& n)	
{ 
	if ( type == (DIM+1) )
		mtabCell.resize( n);
}


template <Dimension DIM>
void PartGrid<DIM>::IOTabBFaceResize( const MGSize& type, const MGSize& n)	
{ 
	if ( type == DIM )
		mtabBFace.resize( n);
}



template <Dimension DIM>
void PartGrid<DIM>::IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mtabNode[id].cX(i);
}


template <Dimension DIM>
void PartGrid<DIM>::IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	if ( type != (DIM+1) )
		THROW_INTERNAL("only simplex type cells are implemented !");
	
	ASSERT( tabid.size() == (DIM+1) );

	for ( MGSize i=0; i<=DIM; ++i)
		tabid[i] = mtabCell[id].cId(i);
}


template <Dimension DIM>
void PartGrid<DIM>::IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	if ( type != (DIM) )
		THROW_INTERNAL("only simplex type faces are implemented !");
	
	ASSERT( tabid.size() == (DIM) );

	for ( MGSize i=0; i<DIM; ++i)
		tabid[i] = mtabBFace[id].cId(i);
}


template <Dimension DIM>
void PartGrid<DIM>::IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id) const
{
	if ( type != (DIM) )
		THROW_INTERNAL("only simplex type faces are implemented !");

	idsurf = mtabBFace[id].cSurfId();
}


template <Dimension DIM>
void PartGrid<DIM>::IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id)
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		mtabNode[id].rX(i) = tabx[i];
}


template <Dimension DIM>
void PartGrid<DIM>::IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
{
	if ( type != (DIM+1) )
		THROW_INTERNAL("only simplex type cells are implemented !");
	
	ASSERT( tabid.size() == (DIM+1) );

	for ( MGSize i=0; i<=DIM; ++i)
		mtabCell[id].rId(i) = tabid[i];
}


template <Dimension DIM>
void PartGrid<DIM>::IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
{
	if ( type != (DIM) )
		THROW_INTERNAL("only simplex type faces are implemented !");
	
	ASSERT( tabid.size() == (DIM) );

	for ( MGSize i=0; i<DIM; ++i)
		mtabBFace[id].rId(i) = tabid[i];
}


template <Dimension DIM>
void PartGrid<DIM>::IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id )
{
	if ( type != (DIM) )
		THROW_INTERNAL("only simplex type faces are implemented !");

	mtabBFace[id].rSurfId() = idsurf;
}



template <Dimension DIM>
void PartGrid<DIM>::IOGetIds( vector<MGSize>& tabid, const MGSize& id) const	
{
	tabid[0] = mtabInterf[id].mIdLocNode;
	tabid[1] = mtabInterf[id].mIdRemProc;
	tabid[2] = mtabInterf[id].mIdRemNode;
}


template <Dimension DIM>
void PartGrid<DIM>::IOSetIds( const vector<MGSize>& tabid, const MGSize& id )	
{
	mtabInterf[id].mIdLocNode = tabid[0];
	mtabInterf[id].mIdRemProc = tabid[1];
	mtabInterf[id].mIdRemNode = tabid[2];
}


#endif // __PARTGRID_H__
