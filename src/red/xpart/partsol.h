#ifndef __PARTSOL_H__
#define __PARTSOL_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libcoregeom/vect.h"

#include "libcoreio/store.h"
#include "libcoreio/readsol.h"
#include "libcoreio/writesol.h"


//////////////////////////////////////////////////////////////////////
// class PartSol
//////////////////////////////////////////////////////////////////////
class PartSol : public IO::SolFacade
{
public:
	PartSol() : mDim(DIM_NONE), mNBlock(0)		{}


//	const Sparse::BlockVector<SIZE>&	operator [] ( const  MGSize& i) const	{ return mvecQ[i]; }
//	Sparse::BlockVector<SIZE>&			operator [] ( const  MGSize& i)			{ return mvecQ[i]; }

	MGSize		BlockSize() const				{ return mNBlock; }
	MGSize		Size() const					{ return mtabQ.size() / mNBlock; }

	void		SetDim( const Dimension& dim)	{ mDim=dim; }
	void		SetBlockSize( const MGSize& n)	{ mNBlock=n; }
	void		Resize( const MGSize& n)		{ return mtabQ.resize( n * mNBlock ); }

// Facade
	virtual Dimension	Dim() const		{ return mDim;}

	virtual void	IOGetName( MGString& name) const	{ name = mName;}
	virtual void	IOSetName( const MGString& name)	{ mName = name;}

	virtual void	DimToUndim( vector<MGFloat>& tab ) const	{  }
	virtual void	UndimToDim( vector<MGFloat>& tab ) const	{  }
 
	virtual MGSize	IOSize() const		{ return Size();}			
	virtual MGSize	IOBlockSize() const	{ return mNBlock;}

	virtual void	IOResize( const MGSize& n)			{ Resize( n); }

	virtual void	IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id );

 
private:
	Dimension		mDim;
	MGString		mName; 

	MGSize			mNBlock;
	vector<MGFloat>	mtabQ; 
};
//////////////////////////////////////////////////////////////////////


inline void PartSol::IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const
{
	if ( tabx.size() < mNBlock)
		THROW_INTERNAL( "AuxData<SIZE>::IOGetBlockVector - tabx.size() != SIZE");

	for ( MGSize k=0; k<mNBlock; ++k)
		tabx[k] = mtabQ[ id*mNBlock + k ];
}


inline void PartSol::IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id )
{
	if ( tabx.size() < mNBlock)
		THROW_INTERNAL( "AuxData<SIZE>::IOSetBlockVector - tabx.size() != SIZE");

	for ( MGSize k=0; k<mNBlock; ++k)
		mtabQ[ id*mNBlock + k ] = tabx[k];
}


#endif // __PARTSOL_H__
