
#include "partitioner.h"

#include "libcoreio/writetec.h"
#include "libcoreio/writemsh2.h"
#include "libcoreio/writeintf.h"



extern "C"
{
	void METIS_PartMeshNodal(int *, int *, idxtype *, int *, int *, int *, int *, idxtype *, idxtype *);
	void METIS_PartMeshDual(int *, int *, idxtype *, int *, int *, int *, int *, idxtype *, idxtype *);
}



template <Dimension DIM>
void Partitioner<DIM>::CreatePartions( const MGSize& nproc)
{
	int ne = mGrid.SizeCellTab();
	int nn = mGrid.SizeNodeTab();
	int	etype;
	int	numflag = 0;
	int npart = nproc;
	int edgecut;

	if ( DIM == DIM_2D )
		etype = 1;
	else if ( DIM == DIM_3D )
		etype = 2;
	else
		THROW_INTERNAL( "bad dimesion inside Partitioner<DIM>::CreatePartions");
	
	vector<idxtype>	tabElmnts( (DIM+1) * ne );
	vector<idxtype>	tabEPart( ne );
	mtabNPart.resize( nn );

	for ( MGSize i=0; i<(MGSize)ne; ++i)
	{
		RED::Cell<DIM,DIM+1> cell = mGrid.cCell( i);

		for ( MGSize k=0; k<(DIM+1); ++k)
			tabElmnts[ i*(DIM+1) + k ] = cell.cId( k);
	}

	cout << "before calling METIS\n";

	METIS_PartMeshNodal( &ne, &nn, &tabElmnts[0], &etype, &numflag, &npart, &edgecut, &tabEPart[0], &mtabNPart[0] );
	//METIS_PartMeshDual( &ne, &nn, &tabElmnts[0], &etype, &numflag, &npart, &edgecut, &tabEPart[0], &mtabNPart[0] );
	cout << "after calling METIS\n";


	// creating new grids
	mtabGrid.resize( nproc, PartGrid<DIM>( mGrid.Name()) );

	// redistributing cells
	for ( MGSize i=0; i<(MGSize)ne; ++i)
	{
		const RED::Cell<DIM,DIM+1>& cell = mGrid.cCell( i);

		vector<MGSize> tab;
		for ( MGSize k=0; k<(DIM+1); ++k)
			tab.push_back( mtabNPart[cell.cId( k)] );

		sort( tab.begin(), tab.end() );
		vector<MGSize>::iterator new_end = unique( tab.begin(), tab.end() );
		vector<MGSize>::iterator last = new_end;
		--last;

		for ( vector<MGSize>::iterator itr=tab.begin(); itr!=new_end; ++itr)
		{
			if ( last == tab.begin() )
				mtabGrid[*itr].PushBackInCell( cell);
			else
				mtabGrid[*itr].PushBackExCell( cell);
		}

		if ( last != tab.begin() )
			mtabInterf.push_back( cell);
	}


	cout << "interf size = " << mtabInterf.size() << endl;
	cout << "CELLS" << endl;
	for ( MGSize i=0; i<mtabGrid.size(); ++i)
	{
		cout << "proc = " << i+1  << endl;
		cout << " " << mtabGrid[i].SizeCellTab() << endl;
		cout << " " << mtabGrid[i].SizeInCellTab() << endl;
		cout << " " << mtabGrid[i].SizeExCellTab() << endl;
	}
	
	// redistributing boundary faces
	for ( MGSize i=0; i<mGrid.SizeBFaceTab(); ++i)
	{
		RED::BFace<DIM,DIM> face = mGrid.cBFace( i);

		vector<MGSize> tab;
		for ( MGSize k=0; k<DIM; ++k)
			tab.push_back( mtabNPart[face.cId( k)] );

		sort( tab.begin(), tab.end() );
		vector<MGSize>::iterator new_end = unique( tab.begin(), tab.end() );

		for ( vector<MGSize>::iterator itr=tab.begin(); itr!=new_end; ++itr)
			mtabGrid[*itr].PushBackBFace( face);
	}

	cout << "BFACES" << endl;
	for ( MGSize i=0; i<mtabGrid.size(); ++i)
		cout << i+1 << " " << mtabGrid[i].SizeBFaceTab() << endl;

	CreateInterface();


	for ( MGSize i=0; i<mtabGrid.size(); ++i)
	{
		mtabGrid[i].SetupNodes( mGrid);
		//mtabGrid[i].CreateNodeMap();
		mtabGrid[i].ResetCellIds();
		mtabGrid[i].ResetBFaceIds();
	}

	// node map must be ready for every grid before calling UpdateInterface
	for ( MGSize i=0; i<mtabGrid.size(); ++i)
	{
		mtabGrid[i].rNProc() = nproc;
		mtabGrid[i].UpdateInterface( mtabGrid );
	}
}


template <Dimension DIM>
void Partitioner<DIM>::CreateInterface()
{
	for ( MGSize i=0; i<mtabInterf.size(); ++i)
	{
		const RED::Cell<DIM,DIM+1>& cell = mtabInterf[i];

		for ( MGSize j=0; j<cell.Size(); ++j)
		{
			MGSize idproc = mtabNPart[ cell.cId(j) ];
			pair<MGSize,MGSize>	par;

			for ( MGSize k=0; k<cell.Size(); ++k)
			{
				MGSize idpcur = mtabNPart[ cell.cId(k) ];
				if ( k != j && idproc != idpcur )
				{
					par.first = cell.cId(k);
					par.second = idpcur;

					mtabGrid[idproc].PushBackIntfNode( par);
				}
			}
			

		}
	}
}



template <Dimension DIM>
void Partitioner<DIM>::ExportGrids( const MGString& name)
{
	// msh2 files
	bool bbin = false;
	MGString sbuf = name;
	MGSize ind = sbuf.find_last_of('.');
	sbuf.erase( ind);

	for ( MGSize i=0; i<mtabGrid.size(); ++i)
	{
		ostringstream of;
		of << sbuf << "." << setfill('0') << setw(3) << i+1 << ".msh2";
		cout << of.str() << endl;


		ofstream ofile( of.str().c_str(), ios::out );;

		IO::WriteMSH2 writerMSH2( mtabGrid[i] );
		writerMSH2.DoWrite( ofile, bbin );

		IO::WriteINTF writerINTF( mtabGrid[i] );
		writerINTF.DoWrite( ofile, bbin);

		// solution
		const vector<PartSol>&	tabsol = mtabGrid[i].cTabSolution();
		for ( MGSize isol=0; isol<tabsol.size(); ++isol)
		{
			MGString sname;
			tabsol[isol].IOGetName( sname);
			MGString sext = sname.substr( sname.find_last_of("."));
			MGString score = sname.substr( 0, sname.find_last_of("."));

			ostringstream of;
			of << score << "." << setfill('0') << setw(3) << i+1 << sext;
			cout << of.str() << endl;

			IO::WriteSOL	solwriter( tabsol[isol] );
			solwriter.DoWrite( of.str().c_str(), bbin );
		}

		//if ( mtabGrid[i].cSolution().BlockSize() > 0 )
		//{
		//	ostringstream of;
		//	of << sbuf << "." << setfill('0') << setw(3) << i+1 << ".daux";
		//	cout << of.str() << endl;

		//	IO::WriteSOL	solwriter( mtabGrid[i].rSolution() );
		//	solwriter.DoWrite( of.str().c_str(), bbin );
		//}
	}

	// tecplot files
	for ( MGSize i=0; i<mtabGrid.size(); ++i)
	{
		ostringstream of;
		of << "out" << "." << setfill('0') << setw(3) << i+1 << ".dat";
		cout << of.str() << endl;

		//if ( mtabGrid[i].cSolution().BlockSize() > 0 )
		//{
		//	IO::WriteTEC writer( mtabGrid[i], &mtabGrid[i].cSolution() );
		//	writer.DoWrite( of.str() );
		//}
		//else
		{
			IO::WriteTEC writer( mtabGrid[i] );
			writer.DoWrite( of.str() );
		}
	}
}



template class Partitioner<DIM_2D>;
template class Partitioner<DIM_3D>;

