#ifndef __DEPARTITIONER_H__
#define __DEPARTITIONER_H__

#include "partgrid.h"

#include "libredcore/interface.h"



inline MGString ModifyFileName( const MGString& name, const MGSize& i )
{
    ostringstream of;
    MGString sbuf = name;
    MGString sext = name;
    sbuf.erase( sbuf.find_last_of('.'));
    sext.erase( 0, sext.find_last_of('.') );
    of << sbuf << "." << setfill('0') << setw(3) << i+1 << sext;
    return of.str();
}


//////////////////////////////////////////////////////////////////////
// class PartGrid
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class DePartitioner
{
public:
	DePartitioner( const MGSize& nproc) : mNProc(nproc), mbSol(false)	{}

	void InitGrids( const MGString& name );
	void InitSols( const vector<MGString>& tabsolname );

	void SetWithSol()	{ mbSol = true; }

	void Merge();

	void ExportGrid( const MGString& name );
	void ExportSol( const vector<MGString>& tabsolname );
	void ExportTec( const MGString& name );

private:
	void BuildGlobalIdMaps();

private:
	bool						mbSol;
	MGSize						mNProc;
	vector< PartGrid<DIM> >		mtabGrid;
	vector< RED::Interface >	mtabInterf;

	PartGrid<DIM> 					mGrid;
};
//////////////////////////////////////////////////////////////////////


#endif // __DEPARTITIONER_H__

