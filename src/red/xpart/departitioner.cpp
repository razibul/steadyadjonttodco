#include "departitioner.h"

#include "libcorecommon/key.h"

#include "libcoreio/readmsh2.h"
#include "libcoreio/readintf.h"
#include "libcoreio/writetec.h"
#include "libcoreio/writetecsurf.h"
#include "libcoreio/writesol.h"
#include "libcoreio/writemsh2.h"


template <Dimension DIM>
void DePartitioner<DIM>::InitGrids( const MGString& name )
{
	mtabGrid.resize( mNProc );
	mtabInterf.resize( mNProc );

	for ( MGSize i=0; i<mNProc; ++i)
	{
		mtabInterf[i].SetDim( DIM);

		MGString fname = ModifyFileName( name, i);

		IO::ReadMSH2    reader( mtabGrid[i] );
        IO::ReadINTF    readerINTF( mtabInterf[i] );

		bool bBin = IO::ReadMSH2::IsBin( fname);

        ifstream ifile;
        if ( bBin)
            ifile.open( fname.c_str(), ios::in | ios::binary);
        else
            ifile.open( fname.c_str(), ios::in );
        if ( ! ifile)
            THROW_FILE( "can not open the file", fname);
       
        reader.DoRead( ifile, bBin);
        readerINTF.DoRead( ifile, bBin);
	}
}


template <Dimension DIM>
void DePartitioner<DIM>::InitSols( const vector<MGString>& tabsolname )
{
	for ( MGSize i=0; i<mNProc; ++i)
	{
		mtabGrid[i].rTabSolution().resize( tabsolname.size() );

		for ( MGSize k=0; k<tabsolname.size(); ++k)
		{
			MGString fname = ModifyFileName( tabsolname[k], i);

			PartSol& sol = mtabGrid[i].rTabSolution()[k];

			IO::ReadSOL    reader( sol );
		
			bool bBin = IO::ReadSOL::IsBin( fname);

			reader.ReadHEADER( fname, bBin );

			sol.SetDim( reader.cDim() );
			sol.SetBlockSize( reader.cBlockSize() );
			sol.Resize( reader.cSize() );

			reader.DoRead( fname, bBin );
		}
	}
}


template <Dimension DIM>
void DePartitioner<DIM>::BuildGlobalIdMaps()
{
	map< pair<MGSize,MGSize>, MGSize >	mapGlobId;

	vector<MGFloat> tabx;

	MGSize idglob = 0;

    for ( MGSize igrid=0; igrid<mtabGrid.size(); ++igrid)
    {
		vector<MGSize> tabintf;
        for ( MGSize inf=0; inf<mtabInterf[igrid].Size(); ++inf)
			tabintf.push_back( mtabInterf[igrid].cInfo( inf).mIdLocNode );

        sort( tabintf.begin(), tabintf.end() );

		for( MGSize inode=0; inode<mtabGrid[igrid].SizeNodeTab(); ++inode)
			if( ! binary_search( tabintf.begin(), tabintf.end(), inode ) )
			{
				mapGlobId.insert( map< pair<MGSize,MGSize>, MGSize >::value_type( pair<MGSize,MGSize>( igrid, inode), idglob));
				++idglob;
			}
	}


	mGrid.mtabNode.resize( idglob);

	mGrid.rTabSolution().resize( mtabGrid[0].cTabSolution().size() );
	for ( MGSize k=0; k<mGrid.cTabSolution().size(); ++k)
	{
		mGrid.rTabSolution()[k].SetDim( mtabGrid[0].cTabSolution()[k].Dim() );
		mGrid.rTabSolution()[k].SetBlockSize( mtabGrid[0].cTabSolution()[k].BlockSize() );
		mGrid.rTabSolution()[k].Resize( idglob);
	}
	//if ( mbSol)
	//{
	//	mGrid.rSolution().SetDim( mtabGrid[0].cSolution().Dim() );
	//	mGrid.rSolution().SetBlockSize( mtabGrid[0].cSolution().BlockSize() );
	//	mGrid.rSolution().Resize( idglob);
	//}

    for ( MGSize igrid=0; igrid<mtabGrid.size(); ++igrid)
    {
		map<MGSize,MGSize> mapintf;
        for ( MGSize inf=0; inf<mtabInterf[igrid].Size(); ++inf)
			mapintf.insert( map<MGSize,MGSize>::value_type( mtabInterf[igrid].cInfo( inf).mIdLocNode, inf) );

		for( MGSize inode=0; inode<mtabGrid[igrid].SizeNodeTab(); ++inode)
			if ( mapintf.find( inode) == mapintf.end() )
			{
				map< pair<MGSize,MGSize>, MGSize >::iterator itr;
				if ( (itr = mapGlobId.find( pair<MGSize,MGSize>( igrid, inode) )) == mapGlobId.end() )
					THROW_INTERNAL("ERROR");

				mtabGrid[igrid].mmapNode.insert( map<MGSize,MGSize>::value_type( inode, itr->second ) );

				mGrid.mtabNode[ itr->second ] = mtabGrid[igrid].cNode( inode );

				for ( MGSize isol=0; isol< mtabGrid[igrid].cTabSolution().size(); ++isol)
				{
					tabx.resize( mtabGrid[igrid].cTabSolution()[isol].BlockSize(), 0.);
					mtabGrid[igrid].cTabSolution()[isol].IOGetBlockVector( tabx, inode);
					mGrid.rTabSolution()[isol].IOSetBlockVector( tabx, itr->second );
				}
			}
			else
			{
				RED::InterfInfo info = mtabInterf[igrid].cInfo( mapintf[inode] );

				map< pair<MGSize,MGSize>, MGSize >::iterator itr;
				if ( (itr = mapGlobId.find( pair<MGSize,MGSize>( info.mIdRemProc, info.mIdRemNode) )) == mapGlobId.end() )
					THROW_INTERNAL("ERROR");

				mtabGrid[igrid].mmapNode.insert( map<MGSize,MGSize>::value_type( inode, itr->second ) );
			}
	}
}


template <Dimension DIM>
void DePartitioner<DIM>::Merge()
{
	BuildGlobalIdMaps();

	set< Key<DIM+1> >	setcell;
	set< Key<DIM> >		setface;

    for ( MGSize igrid=0; igrid<mtabGrid.size(); ++igrid)
	    for ( MGSize ic=0; ic<mtabGrid[igrid].SizeCellTab(); ++ic)
		{
			RED::Cell<DIM,DIM+1> cell = mtabGrid[igrid].cCell(ic);
			Key<DIM+1> key;

			for ( MGSize k=0; k<DIM+1; ++k)
			{
				cell.rId(k) = mtabGrid[igrid].mmapNode[ cell.cId(k) ];
				key.rElem(k) = cell.cId(k);
			}
			key.Sort();

			if ( setcell.find( key) == setcell.end() )
			{
				mGrid.mtabCell.push_back( cell);
				setcell.insert( key);
			}
		}


    for ( MGSize igrid=0; igrid<mtabGrid.size(); ++igrid)
	    for ( MGSize ic=0; ic<mtabGrid[igrid].SizeBFaceTab(); ++ic)
		{
			RED::BFace<DIM,DIM> face = mtabGrid[igrid].cBFace(ic);
			Key<DIM> key;

			for ( MGSize k=0; k<DIM; ++k)
			{
				face.rId(k) = mtabGrid[igrid].mmapNode[ face.cId(k) ];
				key.rElem(k) = face.cId(k);
			}
			key.Sort();

			if ( setface.find( key) == setface.end() )
			{
				mGrid.mtabBFace.push_back( face);
				setface.insert( key);
			}
		}

}


template <Dimension DIM>
void DePartitioner<DIM>::ExportGrid( const MGString& name )
{
	IO::WriteMSH2 writerMSH2( mGrid );
	writerMSH2.DoWrite( name, false );
}

template <Dimension DIM>
void DePartitioner<DIM>::ExportSol( const vector<MGString>& tabsolname )
{
	for ( MGSize k=0; k<mGrid.cTabSolution().size(); ++k)
	{
		IO::WriteSOL	solwriter( mGrid.cTabSolution()[k] );
		solwriter.DoWrite( tabsolname[k], false );
	}
}

template <Dimension DIM>
void DePartitioner<DIM>::ExportTec( const MGString& name )
{
	//IO::SolFacade* psol = NULL;
	//if ( mbSol)
	//	psol = &mGrid.rSolution();

	vector<const IO::SolFacade*> tabp( mGrid.cTabSolution().size(), NULL );
	for ( MGSize i=0; i<mGrid.cTabSolution().size(); ++i)
		tabp[i] = & mGrid.cTabSolution()[i];

	IO::WriteTEC writer( mGrid, tabp );
	writer.DoWrite( "_out.dat" );

	IO::WriteTECSurf writeSurf( mGrid );
	writeSurf.DoWrite( "_out_surf.dat");
}


template class DePartitioner<DIM_2D>;
template class DePartitioner<DIM_3D>;
