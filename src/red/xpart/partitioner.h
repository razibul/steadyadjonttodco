#ifndef __PARTITIONER_H__
#define __PARTITIONER_H__

#include "partgrid.h"

typedef int idxtype;


//////////////////////////////////////////////////////////////////////
// class PartGrid
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Partitioner
{
public:
	Partitioner( const PartGrid<DIM>& grid) : mGrid( grid)	{}

	void CreatePartions( const MGSize& nproc);
	
	void CreateInterface();

	void ExportGrids( const MGString&name);

private:
	const PartGrid<DIM>& 		mGrid;
	
	vector< PartGrid<DIM> >			mtabGrid;
	
	vector< RED::Cell<DIM,DIM+1> >	mtabInterf;
	vector<idxtype>					mtabNPart;
};
//////////////////////////////////////////////////////////////////////


#endif // __PARTITIONER_H__

