#include "libcoresystem/mgdecl.h"

#include "partgrid.h"
#include "partitioner.h"

#include "libcoreio/readmsh2.h"
#include "libcoreio/writemsh2.h"
#include "libcoreio/readsol.h"
#include "libcoreio/writesol.h"
#include "libcoreio/writetec.h"



INIT_VERSION("0.1","''");

template <Dimension DIM>
void CreatePartitions( const MGSize& nproc, const bool& bBin, const MGString& name, const vector<MGString>& tabsolname )
{
	PartGrid<DIM>		grid;
	vector<PartSol>&	tabsol = grid.rTabSolution();
	
	IO::ReadMSH2	reader( grid, "test");
	reader.DoRead( name, bBin);

	tabsol.resize( tabsolname.size() );
	for ( MGSize i=0; i<tabsolname.size(); ++i)
	{
		IO::ReadSOL	solreader( tabsol[i] );
		solreader.ReadHEADER( tabsolname[i], bBin );

		tabsol[i].SetDim( solreader.cDim() );
		tabsol[i].SetBlockSize( solreader.cBlockSize() );
		tabsol[i].Resize( solreader.cSize() );

		solreader.DoRead( tabsolname[i], bBin );

		tabsol[i].IOSetName( tabsolname[i]);
		//IO::WriteTEC	tecwriter( grid, &sol);
		//tecwriter.DoWrite( "part_out.dat");
	}

	//IO::WriteMSH2	writer( grid);
	//writer.DoWrite( "part_out.msh2", false);
	
	Partitioner<DIM>	part( grid);
	
	part.CreatePartions( nproc);
	part.ExportGrids( name);
}



int main( int argc, char* argv[])
{
	try
	{
		if ( argc < 3 )
		{
			cout << "part [nproc] [msh2_name] [[sol_name] ...]" << endl;
			return 1;
		}
		
		MGSize nproc;
		MGString name;
		vector<MGString> tabsolname;

		istringstream is( argv[1]);
		is >> nproc;

		name = MGString( argv[2] );
		
		for (MGSize i=3; i<(MGSize)argc; ++i)
			tabsolname.push_back( MGString( argv[i] ) );
		
		cout << "partitioner started for " << name << endl;
		cout << "nproc = " << nproc << endl;

		bool bBin = IO::ReadMSH2::IsBin( name);
		Dimension dim = IO::ReadMSH2::ReadDim( name, bBin);

		if ( dim == DIM_2D)
		{
			CreatePartitions<DIM_2D>( nproc, bBin, name, tabsolname);
		}
		else if ( dim == DIM_3D)
		{
			CreatePartitions<DIM_3D>( nproc, bBin, name, tabsolname);
		}
		else
			THROW_INTERNAL( "unknown dimesionality");
	
		
	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		fprintf( stderr, "%s\n", e.what() );
	}

	return 0;
}
