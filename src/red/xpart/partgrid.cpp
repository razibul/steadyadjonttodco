#include "partgrid.h"



template <Dimension DIM>
MGSize PartGrid<DIM>::LocalNodeId( const MGSize& id)
{
	map<MGSize,MGSize>::iterator itr;

	if ( (itr = mmapNode.find(id)) == mmapNode.end() )
		THROW_INTERNAL( "node id not found in the map");

	return (*itr).second;
}

template <Dimension DIM>
void PartGrid<DIM>::SetupNodes( const PartGrid<DIM>& grd)
{
	vector<MGSize>	tabInMap;
	vector<MGSize>	tabExMap;

	// create inner node tab
	for ( MGSize i=0; i<mtabInCell.size(); ++i)
	{
		for ( MGSize k=0; k<(DIM+1); ++k)
			tabInMap.push_back( mtabInCell[i].cId( k) );
	}

	sort( tabInMap.begin(), tabInMap.end() );

	vector<MGSize>::iterator itr = unique( tabInMap.begin(), tabInMap.end() );

	tabInMap.erase( itr, tabInMap.end() );
	cout << "tabInMap.size() = " << tabInMap.size() << endl;

	// insert inner nodes into the map
	for ( MGSize i=0; i<tabInMap.size(); ++i)
		mmapNode.insert( map<MGSize,MGSize>::value_type( tabInMap[i], i) );

	// create extern node tab
	for ( MGSize i=0; i<mtabExCell.size(); ++i)
	{
		for ( MGSize k=0; k<(DIM+1); ++k)
			if ( mmapNode.find( mtabExCell[i].cId( k) ) == mmapNode.end() )
				tabExMap.push_back( mtabExCell[i].cId( k) );
	}
	
	sort( tabExMap.begin(), tabExMap.end() );
	itr = unique( tabExMap.begin(), tabExMap.end() );
	tabExMap.erase( itr, tabExMap.end() );
	cout << "tabExMap.size() = " << tabExMap.size() << endl;


	// update the node map
	for ( MGSize i=0; i<tabExMap.size(); ++i)
		mmapNode.insert( map<MGSize,MGSize>::value_type( tabExMap[i], i+tabInMap.size() ) );

	cout << "mmapNode.size() = " << mmapNode.size() << endl;

	if ( ( tabInMap.size() + tabExMap.size() ) != mmapNode.size() )
		THROW_INTERNAL( "Inconsistent mmapNode inside PartGrid<DIM>::SetupNodes");


	// creating final array of nodes
	mtabNode.resize( tabExMap.size() + tabInMap.size() );

	map<MGSize,MGSize>::iterator itrmap;
	for ( itrmap=mmapNode.begin(); itrmap!=mmapNode.end(); ++itrmap)
	{
		mtabNode[ (*itrmap).second ] = grd.cNode( (*itrmap).first );
	}

	//// create solution
	//if ( grd.cSolution().BlockSize() > 0 )
	//{
	//	mSolution.SetDim( DIM);
	//	mSolution.SetBlockSize( grd.cSolution().BlockSize() );
	//	mSolution.Resize( tabExMap.size() + tabInMap.size() );

	//	vector<MGFloat> tabx( grd.cSolution().BlockSize() );
	//	for ( map<MGSize,MGSize>::iterator itrmap=mmapNode.begin(); itrmap!=mmapNode.end(); ++itrmap)
	//	{
	//		grd.cSolution().IOGetBlockVector( tabx, (*itrmap).first );
	//		mSolution.IOSetBlockVector( tabx, (*itrmap).second );
	//	}
	//}

	mtabSolution.resize( grd.cTabSolution().size() );
	for ( MGSize isol=0; isol<grd.cTabSolution().size(); ++isol)
	{
		mtabSolution[isol].SetDim( DIM);
		mtabSolution[isol].SetBlockSize( grd.cTabSolution()[isol].BlockSize() );
		mtabSolution[isol].Resize( tabExMap.size() + tabInMap.size() );

		MGString sname;
		grd.cTabSolution()[isol].IOGetName( sname);
		mtabSolution[isol].IOSetName( sname);

		vector<MGFloat> tabx( grd.cTabSolution()[isol].BlockSize() );
		for ( map<MGSize,MGSize>::iterator itrmap=mmapNode.begin(); itrmap!=mmapNode.end(); ++itrmap)
		{
			grd.cTabSolution()[isol].IOGetBlockVector( tabx, (*itrmap).first );
			mtabSolution[isol].IOSetBlockVector( tabx, (*itrmap).second );
		}
	}



	// create final array of cells (with old node ids)
	mtabCell.reserve( mtabInCell.size() + mtabExCell.size() );

	for ( MGSize i=0; i<mtabInCell.size(); ++i)
		mtabCell.push_back( mtabInCell[i] );	

	for ( MGSize i=0; i<mtabExCell.size(); ++i)
		mtabCell.push_back( mtabExCell[i] );	

	mtabInCell.clear();
	mtabExCell.clear();
}


template <Dimension DIM>
void PartGrid<DIM>::CreateNodeMap()
{
	vector<MGSize>	tabMap;

	for ( MGSize i=0; i<mtabCell.size(); ++i)
	{
		for ( MGSize k=0; k<(DIM+1); ++k)
			tabMap.push_back( cCell( i).cId( k) );
	}

	sort( tabMap.begin(), tabMap.end() );

	vector<MGSize>::iterator itr = unique( tabMap.begin(), tabMap.end() );

	cout << "tabMap.size() = " << tabMap.size() << endl;
	tabMap.erase( itr, tabMap.end() );
	cout << "tabMap.size() = " << tabMap.size() << endl;

	for ( MGSize i=0; i<tabMap.size(); ++i)
		mmapNode.insert( map<MGSize,MGSize>::value_type( tabMap[i], i) );
	
	cout << "mmapNode.size() = " << mmapNode.size() << endl;
}


template <Dimension DIM>
void PartGrid<DIM>::ResetCellIds()
{
	map<MGSize,MGSize>::iterator itr;
	
	for ( MGSize i=0; i<mtabCell.size(); ++i)
	{
		for ( MGSize k=0; k<(DIM+1); ++k)
		{
			if ( (itr = mmapNode.find( cCell( i).cId( k) )) != mmapNode.end() )
				rCell( i).rId( k) = (*itr).second;
			else
				THROW_INTERNAL( "inconsistent node ids inside PartGrid<DIM>::ResetCellIds()");
		}
	}
}


template <Dimension DIM>
void PartGrid<DIM>::ResetBFaceIds()
{
	map<MGSize,MGSize>::iterator itr;
	
	for ( MGSize i=0; i<mtabBFace.size(); ++i)
	{
		for ( MGSize k=0; k<DIM; ++k)
		{
			if ( (itr = mmapNode.find( cBFace( i).cId( k) )) != mmapNode.end() )
				rBFace( i).rId( k) = (*itr).second;
			else
			{
				cout << "i = " << i << " k = " << k << endl;
				cout << "cBFace( i).cId( k) = " << cBFace( i).cId( k) << endl; 
				THROW_INTERNAL( "inconsistent node ids inside PartGrid<DIM>::ResetBFaceIds()");
			}
		}
	}
}



template <Dimension DIM>
void PartGrid<DIM>::UpdateInterface( vector< PartGrid<DIM> >& tabgrd)
{
	InterfInfo info;

	sort( mtabInfo.begin(), mtabInfo.end() );
	vector< pair<MGSize,MGSize> >::iterator itr = unique( mtabInfo.begin(), mtabInfo.end() );
	mtabInfo.erase( itr, mtabInfo.end() );

	mtabInterf.reserve( mtabInfo.size() );

	for ( MGSize i=0; i<mtabInfo.size(); ++i)
	{
		info.mIdRemProc = mtabInfo[i].second;
		info.mIdRemNode = tabgrd[ info.mIdRemProc ].LocalNodeId( mtabInfo[i].first );
		info.mIdLocNode = LocalNodeId( mtabInfo[i].first );

		mtabInterf.push_back( info);
	}
}



template class PartGrid<DIM_2D>;
template class PartGrid<DIM_3D>;
