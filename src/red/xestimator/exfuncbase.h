#ifndef __EXFUNCBASE_H__
#define __EXFUNCBASE_H__

#include "libcoreconfig/configbase.h"
#include "libredcore/equation.h"

using namespace Geom;

//////////////////////////////////////////////////////////////////////
// ExFuncBase
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, RED::EQN_TYPE ET>
class ExFuncBase : public ConfigBase
{
public:
	static MGString	Info()	{ return "ExFuncBase"; }

	ExFuncBase()	{}
	ExFuncBase( const CfgSection& cfgsec) : ConfigBase( cfgsec )	{}
	virtual ~ExFuncBase()	{}

	virtual void	Create( const CfgSection* pcfgsec)	{ ConfigBase::Create( pcfgsec); }
	virtual void	PostCreateCheck() const				{ ConfigBase::PostCreateCheck(); }
	virtual void	Init()		= 0;

	virtual MGFloat	Calculate( const MGFloat tab[])		= 0;
};


#endif // __EXFUNCBASE_H__
