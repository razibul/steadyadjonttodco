#include "metricoperators.h"

#include "Eigen/Eigen"

//#include "Eigen"


template <Dimension DIM>
typename MetricIntersection<DIM>::EMatrix MetricIntersection<DIM>::operator() ( const EMatrix& mtx1, const EMatrix& mtx2 )
{
	using namespace Eigen;

	typedef Matrix<MGFloat,DIM,DIM> EigenMatrix;

	EMatrix mtxred;


	EigenMatrix emtxM1, emtxM2;

	for ( MGSize i=0; i<DIM; ++i)
		for ( MGSize j=0; j<DIM; ++j)
		{
			emtxM1(i,j) = mtx1(i,j);
			emtxM2(i,j) = mtx2(i,j);
		}

	EigenMatrix emtxA = emtxM1.inverse() * emtxM2;

	for ( MGSize k=0; k<DIM; ++k)
		for ( MGSize j=0; j<DIM; ++j)
			IS_INFNAN_THROW( emtxA(k,j), "MetricIntersection -- emtxA");

	EigenSolver<MatrixXd> es(emtxA);

	EigenMatrix P = es.eigenvectors().real();
	EigenMatrix D = es.eigenvalues().real().asDiagonal();

	EigenMatrix D1 = P.transpose() * emtxM1 * P;
	EigenMatrix D2 = P.transpose() * emtxM2 * P;

	for ( int i=0; i<D.rows(); ++i)
		D(i,i) = max( D1(i,i), D2(i,i) ); 

	EigenMatrix Pi = P.inverse();
	emtxA = Pi.transpose() * D * Pi;	

	//cout << P << endl;
	for ( MGSize k=0; k<DIM; ++k)
		for ( MGSize j=0; j<DIM; ++j)
			IS_INFNAN_THROW( P(k,j), "MetricIntersection -- P");

	for ( MGSize k=0; k<DIM; ++k)
		for ( MGSize j=0; j<DIM; ++j)
		{
			if ( ISNAN(Pi(k,j)) || ISINF(Pi(k,j)) )
			{
				cout << endl;
				cout << "emtxM1 = "<< endl << emtxM1  << endl << endl;
				cout << "emtxM1.inverse() = "<< endl << emtxM1.inverse()  << endl << endl;
				cout << "emtxM2 = "<< endl << emtxM2 << endl << endl;
				cout << emtxA  << endl << endl;
				cout << P  << endl << endl;
				cout << Pi << endl << endl;
			}

			IS_INFNAN_THROW( Pi(k,j), "MetricIntersection -- Pi");
		}

	for ( MGSize i=0; i<DIM; ++i)
		for ( MGSize j=0; j<DIM; ++j)
			mtxred(i,j) = emtxA(i,j);

	for ( MGSize k=0; k<DIM; ++k)
		for ( MGSize j=0; j<DIM; ++j)
			IS_INFNAN_THROW( mtxred(k,j), "MetricIntersection -- mtxred");

	return mtxred;
}


template class MetricIntersection<DIM_2D>;
template class MetricIntersection<DIM_3D>;




