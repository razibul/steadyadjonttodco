#ifndef __ESTIMATORBASE_H__
#define __ESTIMATORBASE_H__

#include "libcoreconfig/configbase.h"


//////////////////////////////////////////////////////////////////////
// EstimatorBase
//////////////////////////////////////////////////////////////////////
class EstimatorBase : public ConfigBase
{
public:
	static MGString	Info()	{ return "EstimatorBase"; }

	EstimatorBase()	{}
	EstimatorBase( const CfgSection& cfgsec) : ConfigBase( cfgsec )	{}
	virtual ~EstimatorBase()	{}

	virtual void	Create( const CfgSection* pcfgsec)	{ ConfigBase::Create( pcfgsec); }
	virtual void	PostCreateCheck() const				{ ConfigBase::PostCreateCheck(); }
	virtual void	Init()		= 0;

	virtual void	Execute()		= 0;
	virtual void	ExecuteBnd()	= 0;
};
//////////////////////////////////////////////////////////////////////



#endif // __ESTIMATORBASE_H__
