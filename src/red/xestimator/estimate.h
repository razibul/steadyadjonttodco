#ifndef __ESTIMATE_H__
#define __ESTIMATE_H__

#include "solution.h"
#include "funcspace.h"
#include "estgrid.h"
#include "exfuncbase.h"
#include "gradient.h"
#include "smoother.h"
#include "configconst.h"
#include "estimatebase.h"

#include "libcoreconfig/configbase.h"
#include "libredcore/equation.h"
#include "libcorecommon/factory.h"
#include "libcoreio/writetec.h"




//////////////////////////////////////////////////////////////////////
//	class Estimate
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, RED::EQN_TYPE ET>
class Estimate : public EstimateBase<DIM,ET>
{
public:
	typedef SVector<DIM,MGFloat>	EVector;
	typedef SMatrix<DIM,MGFloat>	EMatrix;


	Estimate() : mbWrite(false)	{}
	virtual ~Estimate()			{}

	virtual void	Create( const CfgSection* pcfgsec, const MGSize& id, const EstGrid<DIM>* pgrid);
	virtual void	PostCreateCheck() const;
	virtual void	Init()						{}

	virtual void	Execute( const Solution< DIM, RED::EquationDef<DIM,ET>::SIZE >& sol );


	const Space<DIM,MGFloat>&	cFuncSpc() const		{ return mFunc; }
	const Space<DIM,EVector>&	cGradSpc() const		{ return mGrad; }
	const Space<DIM,EMatrix>&	cHessSpc() const		{ return mHess; }


private:

	void	InitFuncSpace( const Solution< DIM, RED::EquationDef<DIM,ET>::SIZE >& sol );

	void	InitGradSpace();	
	void	InitHessSpace();	
	void	InitMetricSpace();	


	void	SmoothFuncSpace();
	void	SmoothGradSpace();
	void	SmoothHessSpace();
	void	SmoothMetricSpace();

	void	SmoothFuncSpaceBnd();
	void	SmoothGradSpaceBnd();
	void	SmoothHessSpaceBnd();
	void	SmoothMetricSpaceBnd();

private:
	bool		mbWrite;

	Space<DIM,MGFloat>		mFunc;
	Space<DIM,EVector>		mGrad;
	Space<DIM,EMatrix>		mHess;
};


template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::InitFuncSpace( const Solution< DIM, RED::EquationDef<DIM,ET>::SIZE >& sol )
{
	typedef RED::EquationDef<DIM,ET> EqDef;

	MGString sdim = DimToStr( DIM);;
	MGString seqtype = RED::EquationDef<DIM,ET>::NameEqnType();
	MGString sfuntype = this->ConfigSect().ValueString( ConfigStr::Estimator::Estimate::FunType::KEY ); 

	//if ( sfuntype == MGString( "VORTICITY") )		// HACK :: H A C K !!!
	//{
	//	cout << "starting VORTICITY calc" << endl;
	//	Space<DIM,EVector>	spcU;
	//	Space<DIM,EMatrix>	spcDU;

	//	spcU.rTab().resize( sol.Size() );
	//	spcDU.rTab().resize( sol.Size() );

	//	mFunc.rTab().resize( sol.Size(), 0.0 );

	//	for ( MGSize i=0; i<sol.Size(); ++i)
	//	{
	//		for ( MGSize idim=0; idim<DIM; ++idim)
	//			spcU.rTab()[ i ](idim) = sol[i]( EqDef::template U<0>::ID + idim) / sol[i]( EqDef::ID_RHO);
	//	}

	//	//cout << "--- 1" << endl;

	//	Gradient< DIM, SVector<DIM,MGFloat> >	gradU( *this->mpGrid, spcU);
	//	gradU.Calculate();

	//	//cout << "--- 2" << endl;


	//	for ( MGSize i=0; i<gradU.cGSpace().Size(); ++i)
	//	{
	//		for ( MGSize k=0; k<DIM; ++k)
	//			for ( MGSize j=0; j<DIM; ++j)
	//				spcDU.rTab()[i](k,j) = gradU.cGSpace().cTab()[i] [k][j];

	//		// calc T
	//		EMatrix mtx = spcDU.cTab()[i];
	//		mtx.Transp();
	//		EMatrix mtxOm = ( spcDU.cTab()[i] - mtx );

	//		mtx = mtxOm;
	//		mtx.Transp();

	//		mtxOm = mtxOm * mtx;

	//		MGFloat trace = 0.0;
	//		for ( MGSize k=0; k<DIM; ++k)
	//			trace += mtxOm(k,k);

	//		//mFunc.rTab()[ i ] = ::sqrt( 0.5 * trace );
	//		mFunc.rTab()[ i ] = ::log( max( 0.5 * trace, 1.0e-4) );

	//	}


	//}
	//else
	{
		MGString eekey = "EXFUNC_" + sdim + "_" + seqtype + "_" + sfuntype;
		cout << endl;
		cout << "creating '" << eekey << "'" << endl;

		ExFuncBase<DIM,ET>* pfunc = Singleton< Factory< MGString,Creator< ExFuncBase<DIM,ET> > > >::GetInstance()->GetCreator( eekey )->Create( );

		if ( sol.Size() != this->mpGrid->SizeNodeTab() )
			THROW_INTERNAL( "incompatible sizes of the grid and the solution !!!");

		mFunc.rTab().resize( sol.Size(), 0.0 );

		for ( MGSize i=0; i<sol.Size(); ++i)
			mFunc.rTab()[ i ] = pfunc->Calculate( & sol[i](0) );
	}


	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &mFunc);
		writetec.DoWrite( this->mName + MGString("_func.dat") );
	}
}




#endif // __ESTIMATE_H__

