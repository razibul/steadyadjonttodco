#ifndef __SMOOTHER_H__
#define __SMOOTHER_H__


//////////////////////////////////////////////////////////////////////
//	class Smoother
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class T>
class Smoother
{
public:
	Smoother( const EstGrid<DIM>& grid, const Space<DIM,T>& fspace ) : mGrid(grid), mFSpace(fspace)	{}

	void	Calculate();
	void	CalculateBnd();

	const Space<DIM,T>&	cSSpace() const		{ return mSSpace; }

private:
	const EstGrid<DIM>&		mGrid;
	const Space<DIM,T>&		mFSpace;

	Space<DIM,T>			mSSpace;
	vector<MGFloat>			mtabWght;
};



template <Dimension DIM, class T>
void Smoother<DIM,T>::Calculate()
{
	if ( mFSpace.Size() != mGrid.SizeNodeTab() )
		THROW_INTERNAL( "incompatible grid and fspace sizes !!!");

	mSSpace.rTab().resize( mFSpace.Size() );
	mtabWght.resize( mFSpace.Size() );

	for ( MGSize i=0; i<mFSpace.Size(); ++i)
	{
		mtabWght[i] = 0.0;
		mSSpace.rTab()[i] = InitValue<DIM,T>::Val( 0.0 ); 
	}

	for ( typename EstGrid<DIM>::ColCell::const_iterator citr = mGrid.cColCell().begin(); citr != mGrid.cColCell().end(); ++citr)
	{
		const typename EstGrid<DIM>::GCell& cell = *citr;
		Simplex<DIM> smpx = GREEN::GridGeom<DIM>::CreateSimplex( cell, mGrid);

		MGFloat vol = smpx.Volume();

		T uav = InitValue< DIM, T >::Val( 0.0 );
		for ( MGSize k=0; k<EstGrid<DIM>::GCell::SIZE; ++k)
			uav += mFSpace.cTab()[ cell.cNodeId( k) - 1 ];
		uav /= MGFloat( EstGrid<DIM>::GCell::SIZE );

		for ( MGSize i=0; i<EstGrid<DIM>::GCell::SIZE; ++i)
		{
			mtabWght[ cell.cNodeId(i) - 1 ]			+= vol;
			mSSpace.rTab()[ cell.cNodeId(i) - 1 ]	+= uav * vol; 
		}

	}

	for ( MGSize i=0; i<mFSpace.Size(); ++i)
	{
		//cout << mtabWght[i] << endl;
		mSSpace.rTab()[i] /= mtabWght[i];
	}
}


template <Dimension DIM, class T>
void Smoother<DIM,T>::CalculateBnd()
{
	if ( mFSpace.Size() != mGrid.SizeNodeTab() )
		THROW_INTERNAL( "incompatible grid and fspace sizes !!!");

	mSSpace.rTab().resize( mFSpace.Size() );
	mtabWght.resize( mFSpace.Size() );

	for ( MGSize i=0; i<mFSpace.Size(); ++i)
	{
		mtabWght[i] = 0.0;
		mSSpace.rTab()[i] = InitValue<DIM,T>::Val( 0.0 ); 
	}

	for ( MGSize i=0; i<mGrid.cTabBFaces().size(); ++i)
	{
		Key<DIM> key = mGrid.cTabBFaces()[i];
		SimplexFace<DIM> smpxface = GREEN::GridGeom<DIM>::CreateSimplexFace( key, mGrid);

		MGFloat area = smpxface.Area();

		T uav = InitValue< DIM, T >::Val( 0.0 );
		for ( MGSize k=0; k<DIM; ++k)
			uav += mFSpace.cTab()[ key.cElem(k) - 1 ];
		uav /= MGFloat( DIM );

		for ( MGSize i=0; i<DIM; ++i)
		{
			mtabWght[ key.cElem(i) - 1 ]		+= area;
			mSSpace.rTab()[ key.cElem(i) - 1 ]	+= uav * area; 
		}

	}

	for ( MGSize i=0; i<mGrid.cTabBNodes().size(); ++i)
	{
		MGSize id = mGrid.cTabBNodes()[i] - 1;
		//cout << mtabWght[i] << endl;
		mSSpace.rTab()[id] /= mtabWght[id];
	}


}


#endif // __SMOOTHER_H__
