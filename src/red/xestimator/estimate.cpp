#include "estimate.h"
#include "configconst.h"
#include "metrichessbuilder.h"
#include "metricgradbuilder.h"
#include "metricoperators.h"




template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::Create( const CfgSection* pcfgsec, const MGSize& id, const EstGrid<DIM>* pgrid)	
{ 
	EstimateBase<DIM,ET>::Create( pcfgsec, id, pgrid);

	if ( this->ConfigSect().KeyExists( ConfigStr::Estimator::Estimate::BWRITE ) )
		mbWrite = this->ConfigSect().ValueBool( ConfigStr::Estimator::Estimate::BWRITE );
	else
		mbWrite = false;

	ostringstream os;
	os << "_est_" << this->mId << "_" << this->ConfigSect().ValueString( ConfigStr::Estimator::Estimate::FunType::KEY );
	this->mName = os.str();
}

template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::PostCreateCheck() const
{ 
	EstimateBase<DIM,ET>::PostCreateCheck();
}



template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::InitGradSpace()
{
	mGrad.rTab().resize( mFunc.Size() );

	Gradient<DIM,MGFloat>	grad( *this->mpGrid, mFunc);

	grad.Calculate();

	mGrad.rTab().resize( grad.cGSpace().Size() );
	for ( MGSize i=0; i<grad.cGSpace().Size(); ++i)
		mGrad.rTab()[i] = grad.cGSpace().cTab()[i];

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &mGrad);
		writetec.DoWrite( this->mName + MGString("_grad.dat") );
	}
}


template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::InitHessSpace()
{
	//mGrad.rTab().resize( mFunc.Size() );

	Gradient< DIM, SVector<DIM,MGFloat> >	hess( *this->mpGrid, mGrad);

	hess.Calculate();

	mHess.rTab().resize( hess.cGSpace().Size() );
	for ( MGSize i=0; i<hess.cGSpace().Size(); ++i)
	{
		for ( MGSize k=0; k<DIM; ++k)
			for ( MGSize j=0; j<DIM; ++j)
				mHess.rTab()[i](k,j) = hess.cGSpace().cTab()[i] [k][j];

		// force symmetry
		EMatrix mtx = mHess.rTab()[i];
		mtx.Transp();
		mHess.rTab()[i] = 0.5 * ( mHess.rTab()[i] + mtx );
	}

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &mHess);
		writetec.DoWrite( this->mName + MGString("_hess.dat") );
	}
}


template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::InitMetricSpace()
{
	this->mMetric.rTab().resize( mFunc.Size() );

	const CfgSection* metricsect = & this->ConfigSect().GetSection( ConfigStr::Estimator::Estimate::Metric::NAME );

	MGString sdim = DimToStr( DIM);;
	MGString smbtype = metricsect->ValueString( ConfigStr::Estimator::Estimate::Metric::MetricType::KEY ); 

	MGFloat coeff = metricsect->ValueFloat( ConfigStr::Estimator::Estimate::Metric::SCALE );

	MGString mbkey = "METBUILD_" + sdim + "_" + smbtype;
	cout << "creating '" << mbkey << "'" << endl;

	MetricBuilder<DIM>* builder = Singleton< Factory< MGString,Creator< MetricBuilder<DIM> > > >::GetInstance()->GetCreator( mbkey )->Create( );

	builder->Create( metricsect, &mFunc, &mGrad, &mHess, &this->mMetric );
	builder->Calculate();


	//MetricSpcNormalize<DIM> norm( mpGrid);
	//norm.Execute( mMetric);

	for ( MGSize i=0; i<this->mMetric.Size(); ++i)
		this->mMetric.rTab()[ i] *= coeff;

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &this->mMetric);
		writetec.DoWrite( this->mName + MGString("_metric.dat") );
	}
}



//////////////////////////////////////////////////////////////////////

template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::SmoothFuncSpace()
{
	const CfgSection* smoothsect = & this->ConfigSect().GetSection( ConfigStr::Estimator::Estimate::Smoothing::NAME );

	MGSize niter	= smoothsect->ValueSize( ConfigStr::Estimator::Estimate::Smoothing::FUNC_NITER );
	MGFloat wgh		= smoothsect->ValueFloat( ConfigStr::Estimator::Estimate::Smoothing::FUNC_WEIGHT );

	for ( MGSize iter=0; iter<niter; ++iter)
	{
		Smoother<DIM,MGFloat> smooth( *this->mpGrid, mFunc);
		smooth.Calculate();

		for ( MGSize i=0; i<mFunc.Size(); ++i)
			mFunc.rTab()[i] = (1. - wgh) * mFunc.cTab()[i]  +  wgh * smooth.cSSpace().cTab()[i];
	}

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &mFunc);
		writetec.DoWrite( this->mName + MGString("_func_smth.dat") );
	}
}

template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::SmoothFuncSpaceBnd()
{
	const CfgSection* smoothsect = & this->ConfigSect().GetSection( ConfigStr::Estimator::Estimate::Smoothing::NAME );

	MGSize niter	= smoothsect->ValueSize( ConfigStr::Estimator::Estimate::Smoothing::FUNC_NITER );
	MGFloat wgh		= smoothsect->ValueFloat( ConfigStr::Estimator::Estimate::Smoothing::FUNC_WEIGHT );

	for ( MGSize iter=0; iter<niter; ++iter)
	{
		Smoother<DIM,MGFloat> smooth( *this->mpGrid, mFunc);
		smooth.CalculateBnd();

		for ( MGSize i=0; i<this->mpGrid->cTabBNodes().size(); ++i)
		{
			MGSize id = this->mpGrid->cTabBNodes()[i] - 1;
			mFunc.rTab()[id] = (1. - wgh) * mFunc.cTab()[id]  +  wgh * smooth.cSSpace().cTab()[id];
		}
	}

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &mFunc);
		writetec.DoWrite( this->mName + MGString("_func_smthbnd.dat") );
	}
}

/////////////

template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::SmoothGradSpace()
{
	const CfgSection* smoothsect = & this->ConfigSect().GetSection( ConfigStr::Estimator::Estimate::Smoothing::NAME );

	MGSize niter	= smoothsect->ValueSize( ConfigStr::Estimator::Estimate::Smoothing::GRAD_NITER );
	MGFloat wgh		= smoothsect->ValueFloat( ConfigStr::Estimator::Estimate::Smoothing::GRAD_WEIGHT );

	for ( MGSize iter=0; iter<niter; ++iter)
	{
		Smoother< DIM, EVector > smooth( *this->mpGrid, mGrad);
		smooth.Calculate();

		for ( MGSize i=0; i<mGrad.Size(); ++i)
			mGrad.rTab()[i] = (1. - wgh) * mGrad.cTab()[i]  +  wgh * smooth.cSSpace().cTab()[i];
	}

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &mGrad);
		writetec.DoWrite( this->mName + MGString("_grad_smth.dat") );
	}

}

template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::SmoothGradSpaceBnd()
{
	const CfgSection* smoothsect = & this->ConfigSect().GetSection( ConfigStr::Estimator::Estimate::Smoothing::NAME );

	MGSize niter	= smoothsect->ValueSize( ConfigStr::Estimator::Estimate::Smoothing::GRAD_NITER );
	MGFloat wgh		= smoothsect->ValueFloat( ConfigStr::Estimator::Estimate::Smoothing::GRAD_WEIGHT );

	for ( MGSize iter=0; iter<niter; ++iter)
	{
		Smoother< DIM, EVector > smooth( *this->mpGrid, mGrad);
		smooth.Calculate();

		for ( MGSize i=0; i<this->mpGrid->cTabBNodes().size(); ++i)
		{
			MGSize id = this->mpGrid->cTabBNodes()[i] - 1;
			mGrad.rTab()[id] = (1. - wgh) * mGrad.cTab()[id]  +  wgh * smooth.cSSpace().cTab()[id];
		}
	}

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &mGrad);
		writetec.DoWrite( this->mName + MGString("_grad_smthbnd.dat") );
	}

}

/////////////
template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::SmoothHessSpace()
{
	const CfgSection* smoothsect = & this->ConfigSect().GetSection( ConfigStr::Estimator::Estimate::Smoothing::NAME );

	MGSize niter	= smoothsect->ValueSize( ConfigStr::Estimator::Estimate::Smoothing::HESS_NITER );
	MGFloat wgh		= smoothsect->ValueFloat( ConfigStr::Estimator::Estimate::Smoothing::HESS_WEIGHT );

	for ( MGSize iter=0; iter<niter; ++iter)
	{
		Smoother< DIM, EMatrix > smooth( *this->mpGrid, mHess);
		smooth.Calculate();

		for ( MGSize i=0; i<mHess.Size(); ++i)
			mHess.rTab()[i] = (1. - wgh) * mHess.cTab()[i]  +  wgh * smooth.cSSpace().cTab()[i];
	}

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &mHess);
		writetec.DoWrite( this->mName + MGString("_hess_smth.dat") );
	}

}

template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::SmoothHessSpaceBnd()
{
	const CfgSection* smoothsect = & this->ConfigSect().GetSection( ConfigStr::Estimator::Estimate::Smoothing::NAME );

	MGSize niter	= smoothsect->ValueSize( ConfigStr::Estimator::Estimate::Smoothing::HESS_NITER );
	MGFloat wgh		= smoothsect->ValueFloat( ConfigStr::Estimator::Estimate::Smoothing::HESS_WEIGHT );

	for ( MGSize iter=0; iter<niter; ++iter)
	{
		Smoother< DIM, EMatrix > smooth( *this->mpGrid, mHess);
		smooth.Calculate();

		for ( MGSize i=0; i<this->mpGrid->cTabBNodes().size(); ++i)
		{
			MGSize id = this->mpGrid->cTabBNodes()[i] - 1;
			mHess.rTab()[id] = (1. - wgh) * mHess.cTab()[id]  +  wgh * smooth.cSSpace().cTab()[id];
		}
	}

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &mHess);
		writetec.DoWrite( this->mName + MGString("_hess_smthbnd.dat") );
	}

}

/////////////

template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::SmoothMetricSpace()
{
	const CfgSection* smoothsect = & this->ConfigSect().GetSection( ConfigStr::Estimator::Estimate::Smoothing::NAME );

	MGSize niter	= smoothsect->ValueSize( ConfigStr::Estimator::Estimate::Smoothing::METRIC_NITER );
	MGFloat wgh		= smoothsect->ValueFloat( ConfigStr::Estimator::Estimate::Smoothing::METRIC_WEIGHT );

	if ( niter == 0 )
		return;

	for ( MGSize i=0; i<this->mMetric.Size(); ++i)
	{
		EMatrix mtx;
		DecomposeROOT( mtx, this->mMetric.cTab()[i] );
		this->mMetric.rTab()[i] = mtx;
	}

	for ( MGSize iter=0; iter<niter; ++iter)
	{
		Smoother< DIM, EMatrix > smooth( *this->mpGrid, this->mMetric);
		smooth.Calculate();

		for ( MGSize i=0; i<this->mMetric.Size(); ++i)
			this->mMetric.rTab()[i] = (1. - wgh) * this->mMetric.cTab()[i]  +  wgh * smooth.cSSpace().cTab()[i];
	}

	for ( MGSize i=0; i<this->mMetric.Size(); ++i)
	{
		EMatrix mtx;
		DecomposeSQR( mtx, this->mMetric.cTab()[i] );
		this->mMetric.rTab()[i] = mtx;
	}


	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &this->mMetric);
		writetec.DoWrite( this->mName + MGString("_metric_smth.dat") );
	}

}

template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::SmoothMetricSpaceBnd()
{
	const CfgSection* smoothsect = & this->ConfigSect().GetSection( ConfigStr::Estimator::Estimate::Smoothing::NAME );

	MGSize niter	= smoothsect->ValueSize( ConfigStr::Estimator::Estimate::Smoothing::METRIC_NITER );
	MGFloat wgh		= smoothsect->ValueFloat( ConfigStr::Estimator::Estimate::Smoothing::METRIC_WEIGHT );

	for ( MGSize iter=0; iter<niter; ++iter)
	{
		Smoother< DIM, EMatrix > smooth( *this->mpGrid, this->mMetric);
		smooth.Calculate();

		for ( MGSize i=0; i<this->mpGrid->cTabBNodes().size(); ++i)
		{
			MGSize id = this->mpGrid->cTabBNodes()[i] - 1;
			this->mMetric.rTab()[id] = (1. - wgh) * this->mMetric.cTab()[id]  +  wgh * smooth.cSSpace().cTab()[id];
		}
	}

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &this->mMetric);
		writetec.DoWrite( this->mName + MGString("_metric_smthbnd.dat") );
	}

}



template <Dimension DIM, RED::EQN_TYPE ET>
void Estimate<DIM,ET>::Execute( const Solution< DIM, RED::EquationDef<DIM,ET>::SIZE >& sol )
{
	InitFuncSpace( sol );

	SmoothFuncSpace();

	InitGradSpace();
	SmoothGradSpace();

	InitHessSpace();
	SmoothHessSpace();

	InitMetricSpace();
	SmoothMetricSpace();
}



template class Estimate<DIM_2D,RED::EQN_EULER>;
template class Estimate<DIM_2D,RED::EQN_NS>;
template class Estimate<DIM_2D,RED::EQN_RANS_SA>;
//template class Estimate<DIM_2D, RED::EQN_POISSON>;

template class Estimate<DIM_3D,RED::EQN_EULER>;
template class Estimate<DIM_3D,RED::EQN_NS>;
template class Estimate<DIM_3D,RED::EQN_RANS_SA>;
template class Estimate<DIM_3D,RED::EQN_POISSON>;
template class Estimate<DIM_3D,RED::EQN_MHD>;

