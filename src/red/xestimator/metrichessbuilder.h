#ifndef __METRICHESSBUILDER_H__
#define __METRICHESSBUILDER_H__

#include "metricbuilder.h"


//////////////////////////////////////////////////////////////////////
//	class MetricHessBuilder
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class MetricHessBuilder : public MetricBuilder<DIM>
{
public:
	typedef SVector<DIM,MGFloat>	EVector;
	typedef SMatrix<DIM,MGFloat>	EMatrix;

	MetricHessBuilder()				{}
	virtual ~MetricHessBuilder()	{}

	//virtual void	Create( const CfgSection* pcfgsec, const Estimate<DIM>* pest, Space<DIM,EMatrix>* pmspace )	
	virtual void	Create( const CfgSection* pcfgsec, Space<DIM,MGFloat>* pestFunc, Space<DIM,EVector>* pestGrad, Space<DIM,EMatrix>* pestHess, Space<DIM,EMatrix>* pmspace )
		{ 
			MetricBuilder<DIM>::Create( pcfgsec, pestFunc, pestGrad, pestHess, pmspace); 
		}

	virtual void	PostCreateCheck() const;
	virtual void	Init()						{}
	virtual void	Calculate();

private:
};


template <Dimension DIM> 
void MetricHessBuilder<DIM>::PostCreateCheck() const
{ 
	MetricBuilder<DIM>::PostCreateCheck(); 
}


template <Dimension DIM> 
void MetricHessBuilder<DIM>::Calculate()
{
	//if ( this->mpestHess->Size() != this->mpEstimate->cGrid().SizeNodeTab() )
	//	THROW_INTERNAL( "incompatible grid and hspace sizes !!!");


	const MGFloat p_coeff = this->ConfigSect().ValueFloat( ConfigStr::Estimator::Estimate::Metric::HESS_P_COEFF );


	bool bRelVal = false;
	bool bRelGrad = false;

	if ( this->ConfigSect().KeyExists( ConfigStr::Estimator::Estimate::Metric::HESS_B_REL_VAL ) ) 
		bRelVal = this->ConfigSect().ValueBool( ConfigStr::Estimator::Estimate::Metric::HESS_B_REL_VAL );

	if ( this->ConfigSect().KeyExists( ConfigStr::Estimator::Estimate::Metric::HESS_B_REL_GRAD ) ) 
		bRelGrad = this->ConfigSect().ValueBool( ConfigStr::Estimator::Estimate::Metric::HESS_B_REL_GRAD );


	this->mpMSpace->rTab().resize( this->mpestHess->Size() );

	for ( MGSize k=0; k<this->mpestHess->Size(); ++k)
	{
		const EVector&	vec = this->mpestGrad->cTab()[k];
		const MGFloat&	val = this->mpestFunc->cTab()[k];

		Geom::Vect<DIM,MGFloat> grad;
		for ( MGSize i=0; i<DIM; ++i)
			grad.rX(i) = vec(i);

		MGFloat dgrad = sqrt( grad * grad);

		EMatrix	mtxLI, mtxL;
		EVector vecD;

		EMatrix	mtxH = this->mpestHess->cTab()[k];
		EMatrix	mtxHT = mtxH;
		mtxHT.Transp();

		EMatrix	mtx =  0.5 * (mtxH + mtxHT);

		//for ( MGSize i=0; i<DIM; ++i)
		//	for ( MGSize j=i+1; j<DIM; ++j)
		//		mtx(i,j) = 0.5 * ( mtx(i,j) + mtx(j,i) );


		mtx.Decompose( vecD, mtxL );

		mtxLI = mtxL;
		mtxLI.Invert();

		MGFloat p = 1.;
		for ( MGSize i=0; i<DIM; ++i)
		{
			vecD(i) = ::fabs( vecD(i) );

			if ( bRelGrad ) 
				vecD(i) = ::fabs( vecD(i) ) / ::sqrt(1. + dgrad);

			if ( bRelVal ) 
				vecD(i) = ::fabs( vecD(i) ) / ::sqrt(1. + fabs(val) );


			if ( vecD(i) < ZERO)
				vecD(i) = ZERO;

			p *= vecD(i);
		}

		p = pow( fabs(p), -1./p_coeff );

		for ( MGSize i=0; i<DIM; ++i)
			vecD(i) *= p;


		this->mpMSpace->rTab()[k].Assemble( mtxL, vecD, mtxLI );
	}

}

#endif // __METRICHESSBUILDER_H__
