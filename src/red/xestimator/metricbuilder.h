#ifndef __METRICBUILDER_H__
#define __METRICBUILDER_H__


#include "libcoreconfig/configbase.h"
#include "estimate.h"


//////////////////////////////////////////////////////////////////////
//	class MetricHessBuilder
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class MetricBuilder : public ConfigBase
{
public:
	typedef SVector<DIM,MGFloat>	EVector;
	typedef SMatrix<DIM,MGFloat>	EMatrix;

	static MGString	Info()	{ return "MetricBuilder"; }

	MetricBuilder() : mpestFunc(NULL), mpestGrad(NULL), mpestHess(NULL), mpMSpace(NULL)	{}
	virtual ~MetricBuilder()			{}

	//virtual void	Create( const CfgSection* pcfgsec, const Estimate<DIM>* pest, Space<DIM,EMatrix>* pmspace )	
	virtual void	Create( const CfgSection* pcfgsec, Space<DIM,MGFloat>* pestFunc, Space<DIM,EVector>* pestGrad, Space<DIM,EMatrix>* pestHess, Space<DIM,EMatrix>* pmspace )
		{ 
			ConfigBase::Create( pcfgsec); 

			mpestFunc = pestFunc;
			mpestGrad = pestGrad;
			mpestHess = pestHess;

			mpMSpace = pmspace;
		}

	virtual void	PostCreateCheck() const;
	virtual void	Init()						{}

	virtual void	Calculate()		= 0;


protected:
	Space<DIM,EMatrix>*		mpMSpace;

	Space<DIM,MGFloat>*		mpestFunc;
	Space<DIM,EVector>*		mpestGrad;
	Space<DIM,EMatrix>*		mpestHess;

};


template <Dimension DIM> 
void MetricBuilder<DIM>::PostCreateCheck() const
{ 
	ConfigBase::PostCreateCheck(); 

	if ( ! mpestFunc || ! mpestGrad || ! mpestHess )
		THROW_INTERNAL( "MetricBuilder<DIM>::PostCreateCheck() -- failed : 'mpEstimate == NULL'" );

	if ( ! mpMSpace )
		THROW_INTERNAL( "MetricBuilder<DIM>::PostCreateCheck() -- failed : 'mpMSpace == NULL'" );
}



#endif // __METRICBUILDER_H__

