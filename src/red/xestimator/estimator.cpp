#include "estimator.h"

#include "estimate.h"
#include "estimate_visctensor.h"

#include "libcorecommon/factory.h"
#include "libcoreconfig/config.h" 


template <Dimension DIM, RED::EQN_TYPE ET>
Estimator<DIM,ET>::~Estimator()
{
	for ( typename vector< EstimateBase<DIM,ET>* >::iterator itr=mtabEstimate.begin(); itr!=mtabEstimate.end(); ++itr)
		if ( *itr != 0 )
			delete *itr;
}

template <Dimension DIM, RED::EQN_TYPE ET>
void Estimator<DIM,ET>::Create( const CfgSection* pcfgsec)
{
	EstimatorBase::Create( pcfgsec);

	const CfgSection* datasect = & ConfigSect().GetSection( ConfigStr::Estimator::Data::NAME );
	const CfgSection* cspsect = & ConfigSect().GetSection( ConfigStr::Estimator::CSpace::NAME );

	mData.Create( datasect);
	mCSpace.Create( cspsect, &mData.cGrid() );

	//const RED::CfgSection* estimsect = & ConfigSect().GetSection( ConfigStr::Estimator::Estimate::NAME );
	//mEstimate.Create( estimsect, &mData.cGrid() );


	MGSize count = ConfigSect().GetSectionCount( ConfigStr::Estimator::Estimate::NAME );

	MGSize id = 0;
	for ( MGSize i=0; i<count; ++i)
	{
		const CfgSection* cfgsec = &ConfigSect().GetSection( ConfigStr::Estimator::Estimate::NAME, i);

		EstimateBase<DIM,ET>* ptr = new Estimate<DIM,ET>;
		ptr->Create( cfgsec, ++id, &mData.cGrid() );

		mtabEstimate.push_back( ptr);
	}

	MGSize count_vt = ConfigSect().GetSectionCount( ConfigStr::Estimator::Estimate::NAME_VT );

	//if ( count_vt == 1)
	//{
	//	const CfgSection* cfgsec = &ConfigSect().GetSection( ConfigStr::Estimator::Estimate::NAME_VT );

	//	EstimateBase<DIM,ET>* ptr = new EstimateViscTensor<DIM,ET>;
	//	ptr->Create( cfgsec, ++id, &mData.cGrid() );

	//	mtabEstimate.push_back( ptr);
	//}
}


template <Dimension DIM, RED::EQN_TYPE ET>
void Estimator<DIM,ET>::PostCreateCheck() const
{
	EstimatorBase::PostCreateCheck();

	mData.PostCreateCheck();
	mCSpace.PostCreateCheck();

	if ( mtabEstimate.size() == 0 )
		THROW_INTERNAL( "Estimator<DIM,ET>::PostCreateCheck() -- failed : 'mtabEstimate.size() == 0'" );

	for ( MGSize i=0; i<mtabEstimate.size(); ++i)
		if ( ! mtabEstimate[i] )
			THROW_INTERNAL( "Estimator<DIM,ET>::PostCreateCheck() -- failed : 'mtabEstimate[" << i << "] == NULL'" );

	for ( MGSize i=0; i<mtabEstimate.size(); ++i)
		mtabEstimate[i]->PostCreateCheck();
}

template <Dimension DIM, RED::EQN_TYPE ET>
void Estimator<DIM,ET>::Init()
{
	mData.Init();
	mCSpace.Init();

	for ( MGSize i=0; i<mtabEstimate.size(); ++i)
		mtabEstimate[i]->Init();
}

template <Dimension DIM, RED::EQN_TYPE ET>
void Estimator<DIM,ET>::ExecuteFull()
{

	for ( MGSize i=0; i<mtabEstimate.size(); ++i)
	{
		try
		{
			mtabEstimate[i]->Execute( mData.cSol() );
		}
		catch ( EHandler::Except& )
		{
			cout << " ---- CATCHED ! ---- " << endl;
			cout << "i = " << i << endl;
			throw;
		}

	}

	cout << endl;

	cout << "InitMetric" << endl;
	mCSpace.InitMetric( mtabEstimate[0]->cMetricSpc() );

	for ( MGSize i=1; i<mtabEstimate.size(); ++i)
	{
		cout << "AddMetric " << i << endl;
		mCSpace.AddMetric( mtabEstimate[i]->cMetricSpc() );
	}

	cout << "Limit" << endl;
	mCSpace.Limit();

	cout << "ExportCSPACE" << endl;
	mCSpace.ExportCSPACE();
	mCSpace.ExportTEC( MGString("cspace_tec.dat") );
	//mCSpace.ExportMEANDROS( MGString("cspace") );
}


template <Dimension DIM, RED::EQN_TYPE ET>
void Estimator<DIM,ET>::ExecuteBnd()
{
	for ( MGSize i=0; i<mtabEstimate.size(); ++i)
	{
		mtabEstimate[i]->Execute( mData.cSol() );
	}


	cout << endl;
	mCSpace.InitMetric( mtabEstimate[0]->cMetricSpc() );

	for ( MGSize i=1; i<mtabEstimate.size(); ++i)
		mCSpace.AddMetric( mtabEstimate[i]->cMetricSpc() );

	mCSpace.Limit();

	//mCSpace.ExportCSPACE();
	//mCSpace.ExportTEC( MGString("cspace_bnd.dat") );
	mCSpace.ExportBndMEANDROS( MGString("cspace_bnd") );
}

template <Dimension DIM, RED::EQN_TYPE ET>
void Estimator<DIM,ET>::Execute()
{
	MGString stype = ConfigSect().ValueString( ConfigStr::Estimator::Type::KEY );

	if ( stype == MGString( ConfigStr::Estimator::Type::Value::BOUNDARY ) )
		ExecuteBnd();
	else
	if ( stype == MGString( ConfigStr::Estimator::Type::Value::FULL ) )
		ExecuteFull();
	else
		THROW_INTERNAL( "Estimator<DIM,ET>::Execute() -- unrecognized type !!!" );

}

//////////////////////////////////////////////////////////////////////
void init_Estimator()
{
	static ConcCreator< MGString, Estimator<DIM_2D,RED::EQN_EULER>, EstimatorBase>			gCreatorEstimatorEuler2D( "ESTIMATOR_2D_EULER");
	static ConcCreator< MGString, Estimator<DIM_2D,RED::EQN_NS>, EstimatorBase>				gCreatorEstimatorNS2D( "ESTIMATOR_2D_NS");
	static ConcCreator< MGString, Estimator<DIM_2D,RED::EQN_RANS_SA>, EstimatorBase>		gCreatorEstimatorRANSSA2D( "ESTIMATOR_2D_RANS_SA");


	static ConcCreator< MGString, Estimator<DIM_3D,RED::EQN_EULER>, EstimatorBase>			gCreatorEstimatorEuler3D( "ESTIMATOR_3D_EULER");
	static ConcCreator< MGString, Estimator<DIM_3D,RED::EQN_NS>, EstimatorBase>				gCreatorEstimatorNS3D( "ESTIMATOR_3D_NS");
	static ConcCreator< MGString, Estimator<DIM_3D,RED::EQN_RANS_SA>, EstimatorBase>		gCreatorEstimatorRANSSA3D( "ESTIMATOR_3D_RANS_SA");
	static ConcCreator< MGString, Estimator<DIM_3D,RED::EQN_POISSON>, EstimatorBase>		gCreatorEstimatorPoisson3D( "ESTIMATOR_3D_POISSON");

	static ConcCreator< MGString, Estimator<DIM_3D,RED::EQN_MHD>, EstimatorBase>			gCreatorEstimatorMHD3D( "ESTIMATOR_3D_MHD");
}
