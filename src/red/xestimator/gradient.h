#ifndef __GRADIENT_H__
#define __GRADIENT_H__

#include "funcspace.h"
#include "estgrid.h"
#include "libgreen/gridgeom.h" 


//////////////////////////////////////////////////////////////////////
//	class InitValue
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class T>
class InitValue
{
public:
	static T	Val( const MGFloat& d) 		
		{ return T(d); }
};


template <Dimension DIM>
class InitValue< DIM, SVector<DIM,MGFloat> >
{
public:
	static SVector<DIM,MGFloat>	Val( const MGFloat& d) 		
		{ return SVector<DIM,MGFloat>(DIM,d); }
};

template <Dimension DIM>
class InitValue< DIM, SVector< DIM, SVector<DIM,MGFloat> > >
{
public:
	static SVector< DIM, SVector<DIM,MGFloat> >	Val( const MGFloat& d) 		
		{ return SVector< DIM, SVector<DIM,MGFloat> >( DIM, InitValue< DIM, SVector<DIM,MGFloat> >::Val( d) ); }
};

template <Dimension DIM>
class InitValue< DIM, SMatrix<DIM,MGFloat> >
{
public:
	static SMatrix<DIM,MGFloat>	Val( const MGFloat& d) 		
		{ return SMatrix<DIM,MGFloat>(DIM,DIM,d); }
};




//////////////////////////////////////////////////////////////////////
//	class Gradient
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class T>
class Gradient
{
public:
	typedef Vect<DIM,T>		GVect;


	Gradient( const EstGrid<DIM>& grid, const Space<DIM,T>& fspace ) : mGrid(grid), mFSpace(fspace)	{}

	void	Calculate();
	void	CalculateBnd();

	const Space< DIM, SVector<DIM,T> >&	cGSpace() const		{ return mGSpace; }

private:
	const EstGrid<DIM>&		mGrid;
	const Space<DIM,T>&		mFSpace;

	Space< DIM, SVector<DIM,T> >	mGSpace;
	vector<MGFloat>					mtabWght;
};


template <Dimension DIM, class T>
void Gradient<DIM,T>::Calculate()
{
	if ( mFSpace.Size() != mGrid.SizeNodeTab() )
		THROW_INTERNAL( "incompatible grid and fspace sizes !!!");

	mGSpace.rTab().resize( mFSpace.Size() );
	mtabWght.resize( mFSpace.Size() );

	for ( MGSize i=0; i<mFSpace.Size(); ++i)
	{
		mtabWght[i] = 0.0;
		//mGSpace.rTab()[i] = 0.0; 
		mGSpace.rTab()[i] = InitValue< DIM, SVector<DIM,T> >::Val( 0.0 ); 
	}

	for ( typename EstGrid<DIM>::ColCell::const_iterator citr = mGrid.cColCell().begin(); citr != mGrid.cColCell().end(); ++citr)
	{
		const typename EstGrid<DIM>::GCell& cell = *citr;
		Simplex<DIM> smpx = GREEN::GridGeom<DIM>::CreateSimplex( cell, mGrid);

		MGFloat vol = smpx.Volume();
		SVector<DIM,T> grad = InitValue< DIM, SVector<DIM,T> >::Val( 0.0);

		for ( MGSize i=0; i<EstGrid<DIM>::GCell::SIZE; ++i)
		{
			typename EstGrid<DIM>::GFace face;
			cell.GetFaceVNOut( face, i);

			//T uav = 0.0;
			T uav = InitValue< DIM, T >::Val( 0.0 );
			for ( MGSize k=0; k<EstGrid<DIM>::GFace::SIZE; ++k)
				uav += mFSpace.cTab()[ face.cNodeId( k) - 1 ];
			uav /= MGFloat( DIM);

			SimplexFace<DIM> sface = GREEN::GridGeom<DIM>::CreateSimplexFace( face, mGrid);
			Vect<DIM> vn = sface.Vn();

			for ( MGSize idim=0; idim<DIM; ++idim)
				grad( idim ) += uav * vn.cX( idim);
		}

		for ( MGSize i=0; i<EstGrid<DIM>::GCell::SIZE; ++i)
		{
			mtabWght[cell.cNodeId(i) - 1]			+= vol;
			mGSpace.rTab()[ cell.cNodeId(i) - 1 ]	+= grad; 
		}

	}

	for ( MGSize i=0; i<mFSpace.Size(); ++i)
	{
		//cout << mtabWght[i] << endl;
		mGSpace.rTab()[i] /= mtabWght[i];
	}
}


template <Dimension DIM, class T>
void Gradient<DIM,T>::CalculateBnd()
{
	if ( mFSpace.Size() != mGrid.SizeNodeTab() )
		THROW_INTERNAL( "incompatible grid and fspace sizes !!!");

	mGSpace.rTab().resize( mFSpace.Size() );
	mtabWght.resize( mFSpace.Size() );

	for ( MGSize i=0; i<mFSpace.Size(); ++i)
	{
		mtabWght[i] = 0.0;
		mGSpace.rTab()[i] = InitValue< DIM, SVector<DIM,T> >::Val( 0.0 ); 
	}

	//for ( MGSize i=0; i<mGrid.cTabBFaces().size(); ++i)
	//{
	//	Key<DIM> key = mGrid.cTabBFaces()[i];
	//	SimplexFace<DIM> smpxface = GREEN::GridGeom<DIM>::CreateSimplexFace( key, mGrid);

	//	MGFloat area = smpxface.Area();


	for ( typename EstGrid<DIM>::ColCell::const_iterator citr = mGrid.cColCell().begin(); citr != mGrid.cColCell().end(); ++citr)
	{
		const typename EstGrid<DIM>::GCell& cell = *citr;
		Simplex<DIM> smpx = GREEN::GridGeom<DIM>::CreateSimplex( cell, mGrid);

		MGFloat vol = smpx.Volume();
		SVector<DIM,T> grad = InitValue< DIM, SVector<DIM,T> >::Val( 0.0);

		for ( MGSize i=0; i<EstGrid<DIM>::GCell::SIZE; ++i)
		{
			typename EstGrid<DIM>::GFace face;
			cell.GetFaceVNOut( face, i);

			//T uav = 0.0;
			T uav = InitValue< DIM, T >::Val( 0.0 );
			for ( MGSize k=0; k<EstGrid<DIM>::GFace::SIZE; ++k)
				uav += mFSpace.cTab()[ face.cNodeId( k) - 1 ];
			uav /= MGFloat( DIM);

			SimplexFace<DIM> sface = GREEN::GridGeom<DIM>::CreateSimplexFace( face, mGrid);
			Vect<DIM> vn = sface.Vn();

			for ( MGSize idim=0; idim<DIM; ++idim)
				grad( idim ) += uav * vn.cX( idim);
		}

		for ( MGSize i=0; i<EstGrid<DIM>::GCell::SIZE; ++i)
		{
			mtabWght[ cell.cNodeId(i) - 1 ]			+= vol;
			mGSpace.rTab()[ cell.cNodeId(i) - 1 ]	+= grad; 
		}

	}

	for ( MGSize i=0; i<mFSpace.Size(); ++i)
	{
		//cout << mtabWght[i] << endl;
		mGSpace.rTab()[i] /= mtabWght[i];
	}
}


#endif // __GRADIENT_H__
