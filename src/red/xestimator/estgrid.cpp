#include "estgrid.h"

#include "libgreen/localizator.h"
#include "libgreen/gridgeom.h"
#include "libgreen/generatordata.h"
#include "libgreen/kernel.h"

#include "libcoreio/writetec.h"



template <Dimension DIM> 
void EstGrid<DIM>::PostReadInitialization()
{
	//UpdateNodeCellIds();
	TGrid::RebuildConnectivity();

	FindBoundary();

}

//template <Dimension DIM> 
//void EstGrid<DIM>::PostReadInitialization()
//{
//	//UpdateNodeCellIds();
//	TGrid::RebuildConnectivity();
//
//	FindBoundary();
//
//	GREEN::Localizator<DIM> local( this);
//
//	//test localizator
//
//	cout << "TESTING LOCALIZATOR" << endl;
//	typedef GREEN::Grid<DIM>::GVect GVect;
//
//	vector<GVect>	tabprop;
//	tabprop.reserve( SizeCellTab() );
//
//	for ( typename TGrid::ColCell::const_iterator citr = this->cColCell().begin(); citr != this->cColCell().end(); ++citr)
//	{
//		GVect vc = GREEN::GridGeom<DIM>::CreateSimplex( *citr, *this ).Center();
//
//		MGSize id = local.Localize( vc );
//
//		if ( id != citr->cId() )
//			cout << id << " != " << citr->cId() << endl;
//
//		tabprop.push_back( vc);
//	}
//
//	cout << "END OF THE TEST" << endl;
//
//
//
//	GREEN::GeneratorData<DIM>	gendata;
//	GREEN::Kernel<DIM> kernel( *this, gendata);
//
//	Box<DIM> box = this->FindBoundingBox();
//
//	kernel.InitBoundingBox( box.cVMin(), box.cVMax() );
//
//	IO::WriteTEC	writetec( *this);
//
//	for ( MGSize i=0; i<tabprop.size(); ++i)
//	//for ( MGSize i=0; i<2200; ++i)
//	{
//		cout << i << endl;
//		if ( i == 2208 )
//		{
//			tabprop[i].Write();
//			writetec.DoWrite( "_2208_.dat" );
//		}
//
//		kernel.InsertPoint( tabprop[i] );
//	}
//
//	writetec.DoWrite( "_after_.dat" );
//
//	THROW_INTERNAL( "THE END");;
//}

template <Dimension DIM> 
void EstGrid<DIM>::FindBoundary()
{
	vector< Key<DIM> > tabface;
	tabface.reserve( this->SizeCellTab() );

	for ( typename TGrid::ColCell::const_iterator citr = this->cColCell().begin(); citr != this->cColCell().end(); ++citr)
	{
		const typename TGrid::GCell& cell = *citr;
		for ( MGSize k=0; k<TGrid::GCell::SIZE; ++k)
		{
			Key<DIM> key = cell.FaceKey( k);
			key.Sort();
			tabface.push_back( key);
		}
	}

	// extract boundary faces
	sort( tabface.begin(), tabface.end() );

	for ( typename vector< Key<DIM> >::iterator itr=tabface.begin(); itr!=tabface.end(); ++itr)
	{
		typename vector< Key<DIM> >::iterator itrnext = itr+1;

		if ( itrnext!=tabface.end() )
		{
			if ( *itr != *itrnext)
				mtabBFace.push_back( *itr);
			else
				itr = itrnext;
		}
		else
			mtabBFace.push_back( *itr);
	}

	sort( mtabBFace.begin(), mtabBFace.end() );

	cout << " mtabBFace = " << mtabBFace.size() << endl;

	// extract boundary nodes
	mtabBNode.reserve( DIM * mtabBFace.size() );
	for ( MGSize i=0; i<mtabBFace.size(); ++i)
		for ( MGSize k=0; k<DIM; ++k)
			mtabBNode.push_back( mtabBFace[i].cElem(k) );

	sort( mtabBNode.begin(), mtabBNode.end() );
	mtabBNode.erase( unique( mtabBNode.begin(), mtabBNode.end() ), mtabBNode.end() );
}

template class EstGrid<DIM_2D>;
template class EstGrid<DIM_3D>;
