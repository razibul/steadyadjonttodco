#ifndef __ESTIMATEBASE_H__
#define __ESTIMATEBASE_H__

#include "solution.h"
#include "estgrid.h"
#include "configconst.h"
#include "funcspace.h"

#include "libcoreconfig/configbase.h"
#include "libredcore/equation.h"
#include "libcorecommon/factory.h"




//////////////////////////////////////////////////////////////////////
//	class EstimateBase
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, RED::EQN_TYPE ET>
class EstimateBase : public ConfigBase
{
public:
	typedef SVector<DIM,MGFloat>	EVector;
	typedef SMatrix<DIM,MGFloat>	EMatrix;


	EstimateBase() : mId(0), mpGrid(NULL)	{}
	virtual ~EstimateBase()			{}

	virtual void	Create( const CfgSection* pcfgsec, const MGSize& id, const EstGrid<DIM>* pgrid);
	virtual void	PostCreateCheck() const;
	virtual void	Init()						{}

	virtual void	Execute( const Solution< DIM, RED::EquationDef<DIM,ET>::SIZE >& sol )	= 0;

	const EstGrid<DIM>&			cGrid() const			{ return *mpGrid; }
	const Space<DIM,EMatrix>&	cMetricSpc() const		{ return mMetric; }

protected:
	MGSize		mId;
	MGString	mName;

	const EstGrid<DIM>*		mpGrid;

	Space<DIM,EMatrix>		mMetric;
};


template <Dimension DIM, RED::EQN_TYPE ET>
void EstimateBase<DIM,ET>::Create( const CfgSection* pcfgsec, const MGSize& id, const EstGrid<DIM>* pgrid)	
{ 
	ConfigBase::Create( pcfgsec); 
	mId = id; 
	mpGrid = pgrid; 
}

template <Dimension DIM, RED::EQN_TYPE ET>
void EstimateBase<DIM,ET>::PostCreateCheck() const
{ 
	ConfigBase::PostCreateCheck(); 

	if ( ! mpGrid )
		THROW_INTERNAL( "EstimateBase<DIM>::PostCreateCheck() -- failed : 'mpGrid == NULL'" );
}



#endif // __ESTIMATEBASE_H__
