#include "estimate_visctensor.h"
#include "configconst.h"
#include "libredcore/equation.h"
#include "metricoperators.h"


template <Dimension DIM, RED::EQN_TYPE ET>
void EstimateViscTensor<DIM,ET>::Create( const CfgSection* pcfgsec, const MGSize& id, const EstGrid<DIM>* pgrid)	
{ 
	EstimateBase<DIM,ET>::Create( pcfgsec, id, pgrid);

	if ( this->ConfigSect().KeyExists( ConfigStr::Estimator::Estimate::BWRITE ) )
		mbWrite = this->ConfigSect().ValueBool( ConfigStr::Estimator::Estimate::BWRITE );
	else
		mbWrite = false;

	ostringstream os;
	os << "_est_" << this->mId << "_VISCTENSOR";
	this->mName = os.str();
}

template <Dimension DIM, RED::EQN_TYPE ET>
void EstimateViscTensor<DIM,ET>::PostCreateCheck() const
{ 
	EstimateBase<DIM,ET>::PostCreateCheck();
}




template <Dimension DIM, RED::EQN_TYPE ET>
void EstimateViscTensor<DIM,ET>::InitFuncSpace( const Solution< DIM, RED::EquationDef<DIM,ET>::SIZE >& sol )
{
	typedef RED::EquationDef<DIM,ET> EqDef;

	if ( sol.Size() != this->mpGrid->SizeNodeTab() )
		THROW_INTERNAL( "incompatible sizes of the grid and the solution !!!");

	mU.rTab().resize( sol.Size() );
	mU2.rTab().resize( sol.Size() );

	for ( MGSize i=0; i<sol.Size(); ++i)
	{
		for ( MGSize idim=0; idim<DIM; ++idim)
			mU.rTab()[ i ](idim) = sol[i]( EqDef::template U<0>::ID + idim) / sol[i]( EqDef::ID_RHO);

		//mU.rTab()[ i ](0) = sol[i]( EqDef::template U<0>::ID) / sol[i]( EqDef::ID_RHO);
		//mU.rTab()[ i ](1) = sol[i]( EqDef::template U<1>::ID) / sol[i]( EqDef::ID_RHO);

		mU2.rTab()[ i ] = 0.5 * Dot( mU.cTab()[i], mU.cTab()[i] );
	}

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &mU);
		writetec.DoWrite( this->mName + MGString("_func.dat") );
	}
}



template <Dimension DIM, RED::EQN_TYPE ET>
void EstimateViscTensor<DIM,ET>::InitGradSpace()
{
	//Gradient< DIM, MGFloat >	gradU2( *mpGrid, mU2);
	//gradU2.Calculate();

	//mGradU2.rTab().resize( gradU2.cGSpace().Size() );

	//for ( MGSize i=0; i<gradU2.cGSpace().Size(); ++i)
	//	mGradU2.rTab()[i] = gradU2.cGSpace().cTab()[i];

	//if ( mbWrite )
	//{
	//	IO::WriteTEC	writetec( *mpGrid, &mGradU2);
	//	writetec.DoWrite( mName + MGString("_gradu2.dat") );
	//}

	MGString type = this->ConfigSect().ValueString( ConfigStr::Estimator::Estimate::FunType::KEY );

	Gradient< DIM, SVector<DIM,MGFloat> >	gradU( *this->mpGrid, mU);
	gradU.Calculate();

	mT.rTab().resize( gradU.cGSpace().Size() );
	mFf.rTab().resize( gradU.cGSpace().Size() );

	for ( MGSize i=0; i<gradU.cGSpace().Size(); ++i)
	{
		for ( MGSize k=0; k<DIM; ++k)
			for ( MGSize j=0; j<DIM; ++j)
				mT.rTab()[i](k,j) = gradU.cGSpace().cTab()[i] [k][j];

		// calc T
		EMatrix mtx = mT.rTab()[i];
		mtx.Transp();
		
		if ( type == ConfigStr::Estimator::Estimate::FunType::Value::VORT_TENSOR ) // rot
		{
			mT.rTab()[i] = ( mT.rTab()[i] - mtx );
		}
		else
		if ( type == ConfigStr::Estimator::Estimate::FunType::Value::DEFORM_TENSOR ) // visc tensor
		{
			mT.rTab()[i] = ( mT.rTab()[i] + mtx );
			MGFloat trace = 0.0;
			for ( MGSize idim=0; idim<DIM; ++idim)
				trace += mT.rTab()[i](idim,idim);

			for ( MGSize idim=0; idim<DIM; ++idim)
				mT.rTab()[i](idim,idim) -=  trace / 2.0;
		}
		else
			THROW_INTERNAL( "unknown type");


		EVector vu = mU.cTab()[ i ];
		vu = vu / sqrt( Dot(vu,vu) );

		EVector vf = mT.rTab()[i] * vu;

		
		mFf.rTab()[i] = vf;
		//mFf.rTab()[i] = vf - Dot(vf,vu)*vu;

		//mFf.rTab()[i] = mT.rTab()[i] * vu;

		//mtx = mT.rTab()[i];
		//mtx.Transp();
		//mT.rTab()[i] = mtx * mT.rTab()[i];

	}

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &mT);
		writetec.DoWrite( this->mName + MGString("_vsctensor.dat") );

		IO::WriteTEC	writetec2( *this->mpGrid, &mFf);
		writetec2.DoWrite( this->mName + MGString("_vscforce.dat") );
	}
}

template <Dimension DIM, RED::EQN_TYPE ET>
void EstimateViscTensor<DIM,ET>::InitMetricSpace()
{
	const CfgSection* metricsect = & this->ConfigSect().GetSection( ConfigStr::Estimator::Estimate::Metric::NAME );

	const MGFloat iso_coeff = metricsect->ValueFloat( ConfigStr::Estimator::Estimate::Metric::GRAD_ISO_COEFF );
	const MGFloat coeff = metricsect->ValueFloat( ConfigStr::Estimator::Estimate::Metric::SCALE );

	MGString sdim = DimToStr( DIM);;
	MGString smbtype = "VTENSOR";//metricsect->ValueString( ConfigStr::Estimator::Estimate::Metric::MetricType::KEY ); 

	MGString mbkey = "METBUILD_" + sdim + "_" + smbtype;
	cout << "creating '" << mbkey << "'" << endl;

	//////////////////////
	//MetricBuilder<DIM>* builder = Singleton< Factory< MGString,Creator< MetricBuilder<DIM> > > >::GetInstance()->GetCreator( mbkey )->Create( );

	//builder->Create( metricsect, &mFunc, &mGrad, &mHess, &mMetric );
	//builder->Calculate();

	this->mMetric.rTab().resize( mFf.Size() );

	for ( MGSize k=0; k<mFf.Size(); ++k)
	{
		const EVector&	vec = mFf.cTab()[k];

		Geom::Vect<DIM,MGFloat> grad;
		for ( MGSize i=0; i<DIM; ++i)
			grad.rX(i) = vec(i);

		MGFloat dgrad = sqrt( grad * grad);

		EMatrix	mtxLI, mtxL;
		EVector vecD;


		// gradient metric
		EMatrix			mtxG;

		for ( MGSize ir=0; ir<DIM; ++ir)
		{
			mtxG(ir,ir) = vec[ir] * vec[ir];

			for ( MGSize ic=ir+1; ic<DIM; ++ic)
				mtxG(ic,ir) = mtxG(ir,ic) = vec[ir] * vec[ic];
		}



		EMatrix	 mtx = mtxG;
		
		mtx.Decompose( vecD, mtxL );

		mtxLI = mtxL;
		mtxLI.Invert();

		MGFloat vmax = 0.0;
		for ( MGSize i=0; i<DIM; ++i)
			if ( ::fabs(vecD(i)) > vmax )
				vmax = ::fabs(vecD(i));

		MGFloat p = 1.;
		for ( MGSize i=0; i<DIM; ++i)
		{
			vecD(i) = max( ::fabs( vecD(i) ), iso_coeff);

			//vecD(i) += ZERO;

			//vecD(i) = max( ::fabs( vecD(i) ), 1.0e-6);

			//vecD(i) = ::fabs( vecD(i) ) + iso_coeff ;
			//vecD(i) = sqrt( ::fabs( vecD(i) )) + iso_coeff ;

			//if ( vecD(i) < iso_coeff)
			//	vecD(i) = iso_coeff;

			//if ( vecD(i) < ZERO)
			//	vecD(i) = ZERO;

			p *= vecD(i);
		}

		//for ( MGSize i=0; i<DIM; ++i)
		//	vecD(i) *= p;


		for ( MGSize i=0; i<DIM; ++i)
			IS_INFNAN_THROW( vecD(i), "");


		this->mMetric.rTab()[k].Assemble( mtxL, vecD, mtxLI );

		//for ( MGSize ir=0; ir<DIM; ++ir)
		//	this->mMetric.rTab()[k](ir,ir) += iso_coeff;

	}

	//////////////////////



	//MetricSpcNormalize<DIM> norm( mpGrid);
	//norm.Execute( mMetric);

	for ( MGSize i=0; i<this->mMetric.Size(); ++i)
		this->mMetric.rTab()[ i] *= coeff;

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &this->mMetric);
		writetec.DoWrite( this->mName + MGString("_metric.dat") );
	}

	///////


}




template <Dimension DIM, RED::EQN_TYPE ET>
void EstimateViscTensor<DIM,ET>::SmoothFuncSpace()
{
	const CfgSection* smoothsect = & this->ConfigSect().GetSection( ConfigStr::Estimator::Estimate::Smoothing::NAME );

	MGSize niter	= smoothsect->ValueSize( ConfigStr::Estimator::Estimate::Smoothing::FUNC_NITER );
	MGFloat wgh		= smoothsect->ValueFloat( ConfigStr::Estimator::Estimate::Smoothing::FUNC_WEIGHT );

	for ( MGSize iter=0; iter<niter; ++iter)
	{
		Smoother< DIM, EVector > smooth( *this->mpGrid, mU);
		smooth.Calculate();

		for ( MGSize i=0; i<mU.Size(); ++i)
			mU.rTab()[i] = (1. - wgh) * mU.cTab()[i]  +  wgh * smooth.cSSpace().cTab()[i];
	}

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &mU);
		writetec.DoWrite( this->mName + MGString("_func_smth.dat") );
	}
}

template <Dimension DIM, RED::EQN_TYPE ET>
void EstimateViscTensor<DIM,ET>::SmoothGradSpace()
{
	const CfgSection* smoothsect = & this->ConfigSect().GetSection( ConfigStr::Estimator::Estimate::Smoothing::NAME );

	MGSize niter	= smoothsect->ValueSize( ConfigStr::Estimator::Estimate::Smoothing::GRAD_NITER );
	MGFloat wgh		= smoothsect->ValueFloat( ConfigStr::Estimator::Estimate::Smoothing::GRAD_WEIGHT );

	for ( MGSize iter=0; iter<niter; ++iter)
	{
		Smoother< DIM, EVector > smooth( *this->mpGrid, mFf);
		smooth.Calculate();

		for ( MGSize i=0; i<this->mpGrid->cTabBNodes().size(); ++i)
		{
			MGSize id = this->mpGrid->cTabBNodes()[i] - 1;
			mFf.rTab()[id] = (1. - wgh) * mFf.cTab()[id]  +  wgh * smooth.cSSpace().cTab()[id];
		}
	}

	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &mFf);
		writetec.DoWrite( this->mName + MGString("_grad_smthbnd.dat") );
	}
}

template <Dimension DIM, RED::EQN_TYPE ET>
void EstimateViscTensor<DIM,ET>::SmoothMetricSpace()
{
	const CfgSection* smoothsect = & this->ConfigSect().GetSection( ConfigStr::Estimator::Estimate::Smoothing::NAME );

	MGSize niter	= smoothsect->ValueSize( ConfigStr::Estimator::Estimate::Smoothing::METRIC_NITER );
	MGFloat wgh		= smoothsect->ValueFloat( ConfigStr::Estimator::Estimate::Smoothing::METRIC_WEIGHT );

	if ( niter == 0 )
		return;

	for ( MGSize i=0; i<this->mMetric.Size(); ++i)
	{
		EMatrix mtx;
		DecomposeROOT( mtx, this->mMetric.cTab()[i] );
		this->mMetric.rTab()[i] = mtx;
	}

	for ( MGSize iter=0; iter<niter; ++iter)
	{
		Smoother< DIM, EMatrix > smooth( *this->mpGrid, this->mMetric);
		smooth.Calculate();

		for ( MGSize i=0; i<this->mMetric.Size(); ++i)
			this->mMetric.rTab()[i] = (1. - wgh) * this->mMetric.cTab()[i]  +  wgh * smooth.cSSpace().cTab()[i];
	}

	for ( MGSize i=0; i<this->mMetric.Size(); ++i)
	{
		EMatrix mtx;
		DecomposeSQR( mtx, this->mMetric.cTab()[i] );
		this->mMetric.rTab()[i] = mtx;
	}


	if ( mbWrite )
	{
		IO::WriteTEC	writetec( *this->mpGrid, &this->mMetric);
		writetec.DoWrite( this->mName + MGString("_metric_smth.dat") );
	}
}


template <Dimension DIM, RED::EQN_TYPE ET>
void EstimateViscTensor<DIM,ET>::SmoothFuncSpaceBnd()
{
}

template <Dimension DIM, RED::EQN_TYPE ET>
void EstimateViscTensor<DIM,ET>::SmoothGradSpaceBnd()
{
}

template <Dimension DIM, RED::EQN_TYPE ET>
void EstimateViscTensor<DIM,ET>::SmoothMetricSpaceBnd()
{
}



template <Dimension DIM, RED::EQN_TYPE ET>
void EstimateViscTensor<DIM,ET>::Execute( const Solution< DIM, RED::EquationDef<DIM,ET>::SIZE >& sol )
{
	InitFuncSpace( sol );

	SmoothFuncSpace();

	InitGradSpace();
	SmoothGradSpace();

	//cout << "-1" <<endl;
	InitMetricSpace();
	//cout << "-2" <<endl;
	SmoothMetricSpace();
	//cout << "-3" <<endl;
}



template class EstimateViscTensor<DIM_2D,RED::EQN_EULER>;
template class EstimateViscTensor<DIM_2D,RED::EQN_NS>;
template class EstimateViscTensor<DIM_2D,RED::EQN_RANS_SA>;


template class EstimateViscTensor<DIM_3D,RED::EQN_EULER>;
template class EstimateViscTensor<DIM_3D,RED::EQN_NS>;
template class EstimateViscTensor<DIM_3D,RED::EQN_RANS_SA>;
template class EstimateViscTensor<DIM_3D,RED::EQN_MHD>;

