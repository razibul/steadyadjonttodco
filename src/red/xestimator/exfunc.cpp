#include "exfunc.h"
#include "libcorecommon/factory.h"

//////////////////////////////////////////////////////////////////////
void init_ExFunc()
{
	// Euler
	static ConcCreator< MGString, ExFuncDensity<DIM_2D,RED::EQN_EULER>, ExFuncBase<DIM_2D,RED::EQN_EULER> >		gCreatorExFuncEuler2DDensity( "EXFUNC_2D_EULER_DENSITY");
	static ConcCreator< MGString, ExFuncVelocity<DIM_2D,RED::EQN_EULER>, ExFuncBase<DIM_2D,RED::EQN_EULER> >	gCreatorExFuncEuler2DVelocity( "EXFUNC_2D_EULER_VELOCITY");
	static ConcCreator< MGString, ExFuncPressure<DIM_2D,RED::EQN_EULER>, ExFuncBase<DIM_2D,RED::EQN_EULER> >	gCreatorExFuncEuler2DPressure( "EXFUNC_2D_EULER_PRESSURE");
	static ConcCreator< MGString, ExFuncMach<DIM_2D,RED::EQN_EULER>, ExFuncBase<DIM_2D,RED::EQN_EULER> >		gCreatorExFuncEuler2DMach( "EXFUNC_2D_EULER_MACH");
	static ConcCreator< MGString, ExFuncEntropy<DIM_2D, RED::EQN_EULER>, ExFuncBase<DIM_2D, RED::EQN_EULER> >	gCreatorExFuncEuler2DEntropy("EXFUNC_2D_EULER_ENTROPY");

	static ConcCreator< MGString, ExFuncDensity<DIM_3D,RED::EQN_EULER>, ExFuncBase<DIM_3D,RED::EQN_EULER> >		gCreatorExFuncEuler3DDensity( "EXFUNC_3D_EULER_DENSITY");
	static ConcCreator< MGString, ExFuncVelocity<DIM_3D,RED::EQN_EULER>, ExFuncBase<DIM_3D,RED::EQN_EULER> >	gCreatorExFuncEuler3DVelocity( "EXFUNC_3D_EULER_VELOCITY");
	static ConcCreator< MGString, ExFuncPressure<DIM_3D,RED::EQN_EULER>, ExFuncBase<DIM_3D,RED::EQN_EULER> >	gCreatorExFuncEuler3DPressure( "EXFUNC_3D_EULER_PRESSURE");


	// Poisson
	static ConcCreator< MGString, ExFuncPoisson<DIM_2D,RED::EQN_POISSON>, ExFuncBase<DIM_2D,RED::EQN_POISSON> >	gCreatorExFuncPoisson2DDensity( "EXFUNC_2D_POISSON_DENSITY");
	static ConcCreator< MGString, ExFuncPoisson<DIM_3D,RED::EQN_POISSON>, ExFuncBase<DIM_3D,RED::EQN_POISSON> >	gCreatorExFuncPoisson3DDensity( "EXFUNC_3D_POISSON_DENSITY");


	// NS
	static ConcCreator< MGString, ExFuncDensity<DIM_2D,RED::EQN_NS>, ExFuncBase<DIM_2D,RED::EQN_NS> >			gCreatorExFuncNS2DDensity( "EXFUNC_2D_NS_DENSITY");
	static ConcCreator< MGString, ExFuncVelocity<DIM_2D,RED::EQN_NS>, ExFuncBase<DIM_2D,RED::EQN_NS> >			gCreatorExFuncNS2DVelocity( "EXFUNC_2D_NS_VELOCITY");
	static ConcCreator< MGString, ExFuncMach<DIM_2D,RED::EQN_NS>, ExFuncBase<DIM_2D,RED::EQN_NS> >				gCreatorExFuncNS2DMach( "EXFUNC_2D_NS_MACH");

	static ConcCreator< MGString, ExFuncDensity<DIM_3D,RED::EQN_NS>, ExFuncBase<DIM_3D,RED::EQN_NS> >			gCreatorExFuncNS3DDensity( "EXFUNC_3D_NS_DENSITY");
	static ConcCreator< MGString, ExFuncVelocity<DIM_3D,RED::EQN_NS>, ExFuncBase<DIM_3D,RED::EQN_NS> >			gCreatorExFuncNS3DVelocity( "EXFUNC_3D_NS_VELOCITY");
	static ConcCreator< MGString, ExFuncMach<DIM_3D,RED::EQN_NS>, ExFuncBase<DIM_3D,RED::EQN_NS> >				gCreatorExFuncNS3DMach( "EXFUNC_3D_NS_MACH");


	// RANS SA
	static ConcCreator< MGString, ExFuncDensity<DIM_2D,RED::EQN_RANS_SA>, ExFuncBase<DIM_2D,RED::EQN_RANS_SA> >		gCreatorExFuncRANSSA2DDensity( "EXFUNC_2D_RANS_SA_DENSITY");
	static ConcCreator< MGString, ExFuncVelocity<DIM_2D,RED::EQN_RANS_SA>, ExFuncBase<DIM_2D,RED::EQN_RANS_SA> >	gCreatorExFuncRANSSA2DVelocity( "EXFUNC_2D_RANS_SA_VELOCITY");
	static ConcCreator< MGString, ExFuncVelocitySqr<DIM_2D,RED::EQN_RANS_SA>, ExFuncBase<DIM_2D,RED::EQN_RANS_SA> >	gCreatorExFuncRANSSA2DVelocitySqr( "EXFUNC_2D_RANS_SA_VELOCITY_SQR");
	static ConcCreator< MGString, ExFuncMach<DIM_2D,RED::EQN_RANS_SA>, ExFuncBase<DIM_2D,RED::EQN_RANS_SA> >		gCreatorExFuncRANSSA2DMach( "EXFUNC_2D_RANS_SA_MACH");
	static ConcCreator< MGString, ExFuncPressure<DIM_2D,RED::EQN_RANS_SA>, ExFuncBase<DIM_2D,RED::EQN_RANS_SA> >	gCreatorExFuncRANSSA2DPressure( "EXFUNC_2D_RANS_SA_PRESSURE");

	static ConcCreator< MGString, ExFuncTurbViscosity<DIM_2D,RED::EQN_RANS_SA>, ExFuncBase<DIM_2D,RED::EQN_RANS_SA> >	gCreatorExFuncRANSSA2DTurbViscosity( "EXFUNC_2D_RANS_SA_TURB_VISCOSITY");



	static ConcCreator< MGString, ExFuncDensity		<DIM_3D,RED::EQN_RANS_SA>, ExFuncBase<DIM_3D,RED::EQN_RANS_SA> >	gCreatorExFuncRANSSA3DDensity( "EXFUNC_3D_RANS_SA_DENSITY");
	static ConcCreator< MGString, ExFuncVelocity		<DIM_3D,RED::EQN_RANS_SA>, ExFuncBase<DIM_3D,RED::EQN_RANS_SA> >	gCreatorExFuncRANSSA3DVelocity( "EXFUNC_3D_RANS_SA_VELOCITY");
	static ConcCreator< MGString, ExFuncVelocitySqr	<DIM_3D,RED::EQN_RANS_SA>, ExFuncBase<DIM_3D,RED::EQN_RANS_SA> >	gCreatorExFuncRANSSA3DVelocitySqr( "EXFUNC_3D_RANS_SA_VELOCITY_SQR");
	static ConcCreator< MGString, ExFuncMach			<DIM_3D,RED::EQN_RANS_SA>, ExFuncBase<DIM_3D,RED::EQN_RANS_SA> >	gCreatorExFuncRANSSA3DMach( "EXFUNC_3D_RANS_SA_MACH");
	static ConcCreator< MGString, ExFuncPressure		<DIM_3D,RED::EQN_RANS_SA>, ExFuncBase<DIM_3D,RED::EQN_RANS_SA> >	gCreatorExFuncRANSSA3DPressure( "EXFUNC_3D_RANS_SA_PRESSURE");

	//MHD
	static ConcCreator< MGString, ExFuncDensity<DIM_3D,RED::EQN_MHD>, ExFuncBase<DIM_3D,RED::EQN_MHD> >		gCreatorExFuncMHD3DDensity( "EXFUNC_3D_MHD_DENSITY");
	static ConcCreator< MGString, ExFuncEnergy<DIM_3D,RED::EQN_MHD>, ExFuncBase<DIM_3D,RED::EQN_MHD> >		gCreatorExFuncMHD3DEnergy( "EXFUNC_3D_MHD_ENERGY");
}


