#ifndef __ESTIMATE_VISCTENSOR_H__
#define __ESTIMATE_VISCTENSOR_H__

#include "solution.h"
#include "funcspace.h"
#include "estgrid.h"
#include "exfuncbase.h"
#include "gradient.h"
#include "smoother.h"
#include "configconst.h"
#include "estimatebase.h"

#include "libcoreconfig/configbase.h"
#include "libredcore/equation.h"
#include "libcorecommon/factory.h"
#include "libcoreio/writetec.h"



//////////////////////////////////////////////////////////////////////
//	class EstimateViscTensor
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, RED::EQN_TYPE ET>
class EstimateViscTensor : public EstimateBase<DIM,ET>
{
public:
	typedef SVector<DIM,MGFloat>	EVector;
	typedef SMatrix<DIM,MGFloat>	EMatrix;


	EstimateViscTensor() : mbWrite(false)	{}
	virtual ~EstimateViscTensor()			{}

	virtual void	Create( const CfgSection* pcfgsec, const MGSize& id, const EstGrid<DIM>* pgrid);
	virtual void	PostCreateCheck() const;
	virtual void	Init()						{}

	virtual void	Execute( const Solution< DIM, RED::EquationDef<DIM,ET>::SIZE >& sol );


private:

	void	InitFuncSpace( const Solution< DIM, RED::EquationDef<DIM,ET>::SIZE >& sol );

	void	InitGradSpace();	
	void	InitMetricSpace();	


	void	SmoothFuncSpace();
	void	SmoothGradSpace();
	void	SmoothMetricSpace();

	void	SmoothFuncSpaceBnd();
	void	SmoothGradSpaceBnd();
	void	SmoothMetricSpaceBnd();

private:
	bool		mbWrite;

	Space<DIM,EVector>		mU;
	Space<DIM,EMatrix>		mT;
	//Space<DIM,EMatrix>		mGradU;

	Space<DIM,MGFloat>		mU2;
	Space<DIM,EVector>		mGradU2;


	Space<DIM,EVector>		mFf;

};



#endif // __ESTIMATE_VISCTENSOR_H__
