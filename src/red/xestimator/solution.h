#ifndef __SOLUTION_H__
#define __SOLUTION_H__

//#include "libcoreconfig/configbase.h"
#include "libextsparse/sparsevector.h"
#include "libcoreio/store.h"
#include "libcoreio/readsol.h"
#include "libcoreio/writesol.h"


//////////////////////////////////////////////////////////////////////
// class Solution
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, MGSize SIZE>
class Solution : public IO::SolFacade
{
public:
	Solution()		{}


	const Sparse::BlockVector<SIZE>&	operator [] ( const  MGSize& i) const	{ return mvecQ[i]; }
	Sparse::BlockVector<SIZE>&			operator [] ( const  MGSize& i)			{ return mvecQ[i]; }

	MGSize		Size() const	{ return mvecQ.Size(); }
	void		Resize( const MGSize& n)	{ return mvecQ.Resize( n); }


// Solution Facade
protected:
	virtual Dimension	Dim() const								{ return DIM;}

	virtual void	IOGetName( MGString& name) const			{ name = mSolName;}
	virtual void	IOSetName( const MGString& name)			{ mSolName = name;}

	virtual void	DimToUndim( vector<MGFloat>& tab ) const	{}
	virtual void	UndimToDim( vector<MGFloat>& tab ) const	{}
 
	virtual MGSize	IOSize() const								{ return Size();}			
	virtual MGSize	IOBlockSize() const							{ return SIZE;}

	virtual void	IOResize( const MGSize& n)					{ mvecQ.Resize( n); }

	virtual void	IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id );

 
private:
	MGString				mSolName; 

	Sparse::Vector<SIZE>	mvecQ; 
};
//////////////////////////////////////////////////////////////////////


 
template <Dimension DIM, MGSize SIZE>
inline void Solution<DIM,SIZE>::IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const
{
	if ( tabx.size() != SIZE)
		THROW_INTERNAL( "Solution<SIZE>::IOGetBlockVector - tabx.size() != SIZE");

	for ( MGSize k=0; k<SIZE; ++k)
		tabx[k] = mvecQ[id](k);
}


template <Dimension DIM, MGSize SIZE>
inline void Solution<DIM,SIZE>::IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id )
{
	if ( tabx.size() != SIZE)
		THROW_INTERNAL( "Solution<SIZE>::IOSetBlockVector - tabx.size() != SIZE");

	for ( MGSize k=0; k<SIZE; ++k)
		mvecQ[id](k) = tabx[k];
}



#endif // __SOLUTION_H__
