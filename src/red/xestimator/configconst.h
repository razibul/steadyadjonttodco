#ifndef __EST_CONFIGCONST_H__
#define __EST_CONFIGCONST_H__

namespace ConfigStr
{

//////////////////////////////////////////////////////////////////////
// section MASTER
	namespace Master
	{
		const char NAME[]		= "MASTER";
		const char CASE[]		= "MST_NAME";
		const char DESRIPT[]	= "MST_DESCRIPT";
	}

//////////////////////////////////////////////////////////////////////
// section ESTIMATOR
	namespace Estimator
	{
		const char NAME[]		= "ESTIMATOR";

		namespace Dim
		{
			const char KEY[]		= "EE_DIM";
			namespace Value
			{
				const char	DIM_NONE[]	= "NONE";
				const char	DIM_0D[]	= "0D";
				const char	DIM_1D[]	= "1D";
				const char	DIM_2D[]	= "2D";
				const char	DIM_3D[]	= "3D";
				const char	DIM_4D[]	= "4D";
			}

		}
	
		namespace EqType
		{
			const char KEY[]		= "EE_EQNTYPE";
			namespace Value
			{
				const char	NONE[]			= "NONE";
				const char	ADVECT[]		= "ADVECT";
				const char	POISSON[]		= "POISSON";
				const char	EULER[]			= "EULER";
				const char	NS[]			= "NS";
				const char	RANS_SA[]		= "RANS_SA";
				const char	RANS_KOMEGA[]	= "RANS_KOMEGA";
				const char	RANS_2E[]		= "RANS_2E";
			}
		}

		namespace Type
		{
			const char KEY[]		= "EE_TYPE";
			namespace Value
			{
				const char	BOUNDARY[]	= "BOUNDARY";
				const char	FULL[]		= "FULL";
			}
		}


//////////////////////////////////////////////////////////////////////
// section ESTIMATOR :: subsection DATA
		namespace Data
		{
			const char NAME[]		= "DATA";
			const char FNAME[]		= "DAT_FNAME";

			namespace FType
			{
				const char KEY[]		= "DAT_FTYPE";
				namespace Value
				{
					const char	TECPLOT[]		= "TECPLOT";
				}
			}
		}

//////////////////////////////////////////////////////////////////////
// section ESTIMATOR :: subsection CSPACE
		namespace CSpace
		{
			const char NAME[]		= "CSPACE";
			const char FNAME[]		= "CSPC_FNAME";

			namespace FType
			{
				const char KEY[]		= "CSPC_FTYPE";
				namespace Value
				{
					const char	TECPLOT[]		= "TECPLOT";
					const char	MEANDROS[]		= "MEANDROS";
					const char	CSPACE[]		= "CSPACE";
				}
			}

			const char H_MIN[]		= "CSPC_H_MIN";
			const char H_MAX[]		= "CSPC_H_MAX";
			const char AR_MAX[]		= "CSPC_AR_MAX";
			const char SCALE[]		= "CSPC_SCALE";

			const char SMTH_NITER[]		= "CSPC_SMTH_NITER";
			const char SMTH_WEIGHT[]	= "CSPC_SMTH_WEIGHT";

		}

//////////////////////////////////////////////////////////////////////
// section ESTIMATOR :: subsection ESTIMATE
		namespace Estimate
		{
			const char NAME[]		= "ESTIMATE";
			const char NAME_VT[]	= "ESTIMATE_VTENSOR";


			const char BWRITE[]		= "EST_BWRITE";

			namespace FunType
			{
				const char KEY[]		= "EST_FUNCTYPE";
				namespace Value
				{
					const char	DENSITY[]		= "DENSITY";
					const char	VELOCITY[]		= "VELOCITY";
					const char	MACH[]			= "MACH";

					const char	VORT_TENSOR[]		= "VORT_TENSOR";
					const char	DEFORM_TENSOR[]		= "DEFORM_TENSOR";
				}
			}

			namespace Smoothing
			{
				const char NAME[]			= "SMOOTHING";

				const char FUNC_NITER[]		= "SMTH_FUNC_NITER";
				const char FUNC_WEIGHT[]	= "SMTH_FUNC_WEIGHT";

				const char GRAD_NITER[]		= "SMTH_GRAD_NITER";
				const char GRAD_WEIGHT[]	= "SMTH_GRAD_WEIGHT";

				const char HESS_NITER[]		= "SMTH_HESS_NITER";
				const char HESS_WEIGHT[]	= "SMTH_HESS_WEIGHT";

				const char METRIC_NITER[]	= "SMTH_METRIC_NITER";
				const char METRIC_WEIGHT[]	= "SMTH_METRIC_WEIGHT";
			}

			namespace Metric
			{
				const char NAME[]			= "METRIC";

				const char SCALE[]				= "MET_SCALE";
				const char HESS_P_COEFF[]		= "MET_HESS_P_COEFF";
				const char HESS_B_REL_VAL[]		= "MET_HESS_B_REL_VAL";
				const char HESS_B_REL_GRAD[]	= "MET_HESS_B_REL_GRAD";

				const char GRAD_PSQRT_COEFF[]	= "MET_GRAD_PSQRT_COEFF";
				const char GRAD_ISO_COEFF[]		= "MET_GRAD_ISO_COEFF";

				namespace MetricType
				{
					const char KEY[]		= "MET_TYPE";
					namespace Value
					{
						const char	HESSIAN[]		= "HESSIAN";
						const char	GRADIENT[]		= "GRADIENT";
					}
				}

			}

		}

	}



//////////////////////////////////////////////////////////////////////
// section SOLVER
	namespace Solver
	{
		const char NAME[] = "SOLVER";

		namespace Dim
		{
			const char KEY[]		= "SOL_DIM";
			namespace Value
			{
				const char	DIM_NONE[]	= "NONE";
				const char	DIM_0D[]	= "0D";
				const char	DIM_1D[]	= "1D";
				const char	DIM_2D[]	= "2D";
				const char	DIM_3D[]	= "3D";
				const char	DIM_4D[]	= "4D";
			}
		}

		namespace EqType
		{
			const char KEY[]		= "SOL_EQNTYPE";
			namespace Value
			{
				const char	NONE[]			= "NONE";
				const char	ADVECT[]		= "ADVECT";
				const char	POISSON[]		= "POISSON";
				const char	EULER[]			= "EULER";
				const char	NS[]			= "NS";
				const char	RANS_SA[]		= "RANS_SA";
				const char	RANS_KOMEGA[]	= "RANS_KOMEGA";
				const char	RANS_2E[]		= "RANS_2E";
			}
		}

		namespace SolType
		{
			const char KEY[]	= "SOL_SOLTYPE";
			namespace Value
			{
				const char DEFAULT[]	= "DEFAULT";
			}
		}

		const char MAXNITER[]	= "SOL_MAXNITER";			// max. number of iter.
		const char NSTORE[]		= "SOL_NSTORE";				// number of iter. for storing	
		const char CONVEPS[]	= "SOL_CONVEPS";			// convergence criterion


//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection GRID
		namespace Grid
		{
			const char NAME[]		= "GRID";
			const char FNAME[]		= "GRD_FNAME";
			namespace FType
			{
				const char KEY[]	= "GRD_FTYPE";
				namespace Value
				{
					const char ASCII[]	= "ASCII";
					const char BIN[]	= "BIN";
				}
			}
			const char MAXANGLE[]	= "GRD_MAXANGLE";
		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection AUXDATA
		namespace AuxData
		{
			const char NAME[]		= "AUXDATA";
			const char FNAME[]		= "AUX_FNAME";
			const char FTYPE[]		= "AUX_FTYPE";
		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection SOLUTION
		namespace Solution
		{
			const char NAME[]		= "SOLUTION";
			const char RESTART[]	= "TIM_RESTART";
			const char FNAME[]		= "TIM_FNAME";
			const char FTYPE[]		= "TIM_FTYPE";
		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection PHYSICS
		namespace Physics
		{
			const char NAME[]				= "PHYSICS";
			const char REF_LENGTH[]			= "VAR_REF_LENGTH";
			const char REF_REYNOLDS[]		= "VAR_REF_REYNOLDS";
			const char REF_PRANDTL[]		= "VAR_REF_PRANDTL";
			const char REF_PRANDTL_TURB[]	= "VAR_REF_PRANDTL_TURB";
			const char REF_MACH[]			= "VAR_REF_MACH";

			namespace Var
			{
				const char NAME[]			= "VAR";
				const char REFERENCE[]		= "reference";

				namespace Type
				{
					const char KEY[]		= "VAR_TYPE";
					namespace Value
					{
						const char RPUV[]		= "VAR_RPUV";
						const char RRERURV[]	= "VAR_RRERURV";
						const char MRPA[]		= "VAR_MRPA";
					}
				}

				const char RO[]			= "VAR_RO";
				const char P[]			= "VAR_P";
				const char UX[]			= "VAR_UX";
				const char UY[]			= "VAR_UY";
				const char UZ[]			= "VAR_UZ";
				const char ROE[]		= "VAR_ROE";
				const char ROUX[]		= "VAR_ROUX";
				const char ROUY[]		= "VAR_ROUY";
				const char ROUZ[]		= "VAR_ROUZ";
				const char ALPHA[]		= "VAR_ALPHA";
				const char MACH[]		= "VAR_MACH";
				const char NUT[]		= "VAR_NUT";
				const char K[]			= "VAR_K";
				const char OMEGA[]		= "VAR_OMEGA";
			}

			namespace BC
			{
				const char KEY[]			= "BC";
				namespace Value
				{
					const char	FARFIELD[]		= "FARFIELD";
					const char	INVISCWALL[]	= "INVISCWALL";
					const char	SYMETRY[]		= "SYMETRY";
					const char	VISCWALL[]		= "VISCWALL";
					const char	EXTRAPOL[]		= "EXTRAPOL";
					const char	DEF[]			= "DEF";
				}
			}
		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection EXECUTOR
		namespace Executor
		{
			const char NAME[]		= "EXECUTOR";

			namespace BlockDef
			{
				const char KEY[]		= "BLOCKDEF";
				namespace Value
				{
					const char FULL[] = "FULL";
				}
			}

			//-------------------------------------------------------
			// SPACESOL section

			namespace SpaceSol
			{	
				const char NAME[]	= "SPACESOL";

				namespace Type
				{
					const char KEY[]	= "SPC_TYPE";
					namespace Value
					{
						const char	NONE[]			= "NONE";
						const char	RDS[]			= "RDS";
					}
				}

				const char NUMDIFF_EPS[]	= "SPC_NUMDIFF_EPS";


				namespace FuncType
				{
					const char KEY[]	= "SPC_FUNCTYPE";
					namespace Value
					{
						const char	NONE[]			= "NONE";
						const char	MTX_LDAN[]		= "MTX_LDAN";
						const char	MTX_LDA[]		= "MTX_LDA";
						const char	MTX_N[]			= "MTX_N";
						const char	HE_LDA_PSI[]	= "HE_LDA_PSI";
						const char	HE_LDA_LDA[]	= "HE_LDA_LDA";
						const char	HE_LDA_N[]		= "HE_LDA_N";
						const char	HE_LDAN_PSI[]	= "HE_LDAN_PSI";
						const char	HE_N_PSI[]		= "HE_N_PSI";
						const char	HE_N_N[]		= "HE_N_N";
						const char	SM_LDAN_PSI[]	= "SM_LDAN_PSI";
						const char	SM_LDA_PSI[]	= "SM_LDA_PSI";
						const char	SM_LDA_LDA[]	= "SM_LDA_LDA";
					}
				}
			}


			//-------------------------------------------------------
			// TIMESOL section

			namespace TimeSol
			{	
				const char NAME[]	= "TIMESOL";
				namespace Type
				{
					const char KEY[]		= "TIM_TYPE";
					namespace Value
					{
						const char	EXPLICIT[]	= "EXPLICIT";
						const char	IMPLICIT[]	= "IMPLICIT";
					}
				}

				namespace JacobType
				{
					const char KEY[]		= "TIM_JACOBTYPE";
					namespace Value
					{
						const char	NONE[]		= "NONE";
						const char	NUMERICAL[]	= "NUMERICAL";
						const char	PICARD[]	= "PICARD";
					}
				}

				const char LINMAXITER[]	= "TIM_LINMAXITER";
				const char LINDKSPACE[]	= "TIM_LINDKSPACE";
				const char LINMINEPS[]	= "TIM_LINMINEPS";

				const char MAXNITER[]	= "TIM_MAXNITER";
				const char NSTORE[]		= "TIM_NSTORE";
				const char CONVEPS[]	= "TIM_CONVEPS";
				const char CFL[]		= "TIM_CFL";
				const char MINCFL[]		= "TIM_MINCFL";
				const char MAXCFL[]		= "TIM_MAXCFL";
				const char NCFLGROWTH[]	= "TIM_NCFLGROWTH";
				const char NCFLSTART[]	= "TIM_NCFLSTART";
			}

			//-------------------------------------------------------
			// INTEGRATOR section

			namespace Integrator
			{	
				const char NAME[]	= "INTEGRATOR";

				namespace Type
				{
					const char KEY[]	= "INTEG_TYPE";
					namespace Value
					{
						const char	NONE[]			= "NONE";
					}
				}
			}

		}
	
	}
}
  

#endif // __EST_CONFIGCONST_H__
