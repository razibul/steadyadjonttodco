#include "gridsmoother.h"

#include "libgreen/bndgrid.h"
#include "libcorecommon/progressbar.h"
#include "libgreen/writegrdtec.h"
#include "libgreen/reconstructor.h"
#include "libgreen/kernel.h"
#include "libgreen/readmeandr.h"
#include "libgreen/writemeandr.h"
#include "libgreen/writegrdtec.h"

#include "libgreen/grid_ioproxy.h"
#include "libgreen/gridcontext_ioproxy.h"
#include "libcoreio/readtec.h"

#include "libcoreio/readmsh2.h"

#include "functionalgrid.h"
#include "optimizer.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <Dimension DIM>
void GridSmoother<DIM>::WriteGridTEC( const MGString& fname)
{
	WriteGrdTEC<DIM>	writer( &mGrid);
	writer.DoWrite( fname);
}


template <Dimension DIM>
void GridSmoother<DIM>::InitFileMSH2( const MGString& fname)
{
	//MGString fnameTEC = fname + MGString(".dat");

	//ReadTEC<DIM>	reader( IOReadGreenProxy<DIM>( &mGrid, NULL), &mMetricCx );
	//reader.DoRead( fnameTEC);

	cout << "reading MSH2 : " << fname << endl;

	IOReadGreenProxy<DIM>	grdproxy( &mGrid, &mpBndGrid);
	IO::ReadMSH2	reader( grdproxy );
	reader.DoRead( fname);

	cout << "reading MSH2 : " << fname << " -- FINISHED" <<  endl;


	//IO::ReadTEC	reader( grdproxy );
	//reader.DoRead( fnameTEC);
}


template <Dimension DIM>
void GridSmoother<DIM>::PostInit()
{
	vector< Key<DIM> > tabbnd;
	mGrid.RebuildConnectivity( &tabbnd );
	mGrid.UpdateNodeCellIds();

	mGrid.CheckQuality();

	ExtractBlob();

	GVect	vmin, vmax;


	mtabBNode.clear();
	mtabBNode.reserve( DIM*tabbnd.size() );
	for ( MGSize i=0; i<tabbnd.size(); ++i)
	{
		for ( MGSize k=0; k<DIM; ++k)
			mtabBNode.push_back( tabbnd[i].cElem(k) );
	}

	sort( mtabBNode.begin(), mtabBNode.end() );
	mtabBNode.erase( unique( mtabBNode.begin(), mtabBNode.end() ), mtabBNode.end() );;

	/////////////////////////////////////////////////////////////
	//ofstream f( "_bnd_.dat" );

	//cout << "\nWriting TEC from PostInitTEC" << std::flush;

	//f << "TITLE = \"boundary grid\"\n";
	//f << "VARIABLES = ";
	//f << "\"X\", \"Y\"";
	//f << "\n";

	//f << "ZONE T=\"bnd grid\", N=" << mapbnode.size() << ", E=" << tabbnd.size() << ", F=FEPOINT, ET=LINESEG\n";

	//for ( map<MGSize,MGSize>::const_iterator itrn = mapbnode.begin(); itrn != mapbnode.end(); ++itrn)
	//{
	//	const GVect& vpos = mGrid.cNode( itrn->first ).cPos();
	//	for ( MGSize k=0; k<DIM; ++k)
	//		f << setprecision(8) << vpos.cX( k) << " ";
	//	f << endl;
	//}

	//for ( MGSize i=0; i<tabbnd.size(); ++i)
	//{
	//	for ( MGSize k=0; k<DIM; ++k)
	//		f << mapbnode[ tabbnd[i].cElem(k) ] << " ";
	//	f << endl;
	//}

	//cout << " - FINISHED\n";
	///////////////////////////////////////////////////////////

}


template <Dimension DIM>
void GridSmoother<DIM>::ExtractBlob()
{
	GeneratorData<DIM>	gend;
	Kernel<DIM> kernel( mGrid, gend);

	vector<MGSize> tabcell;

	MGSize id = 11824502;

	for ( MGSize in=0; in<Grid<DIM>::GCell::SIZE; ++in)
	{
		MGSize idn = mGrid.cCell(id).cNodeId(in);
		vector<MGSize> tabball;
		kernel.FindBall( tabball, idn);

		for ( MGSize k=0; k<tabball.size(); ++k)
			tabcell.push_back( tabball[k] );
	}

	sort( tabcell.begin(), tabcell.end() );
	tabcell.erase( unique( tabcell.begin(), tabcell.end()), tabcell.end() );


	//vector<MGSize> tabball;
	//kernel.FindBall( tabball, 121);

	WriteGrdTEC<DIM> writeTEC( &mGrid);
	writeTEC.WriteCells( "blob.dat", tabcell);

}


template <Dimension DIM>
void GridSmoother<DIM>::Execute()
{
	FunctionalGrid<DIM>	func( mGrid);

	func.Init( mtabBNode );

	MGSize n;
	//MGFloat val;
	vector<MGFloat>	tabx;
	vector<MGFloat>	tabgrad;

	n = func.cSize() * DIM;
	func.SetupParams( tabx, tabgrad);

	//func.Evaluate( &val, &tabgrad[0], &tabx[0], n);


	Optimizer opt( &func);
	
	opt.DoOptimize( tabx);

	cout << endl;
	mGrid.CheckQuality();

	for ( typename Grid<DIM>::ColNode::iterator itr = mGrid.rColNode().begin(); itr != mGrid.rColNode().end(); ++itr)
	{
		MGSize idnod = itr->cId();
		MGSize idloc = func.cLocId( itr->cId() - 1 );

		if ( idloc != 0 )
		{
			--idloc;
			for ( MGSize idim=0; idim<DIM; ++idim)
				 itr->rPos().rX(idim) = tabx[ idloc*DIM + idim];

		}
	}

	cout << endl;
	mGrid.CheckQuality();

}



template class GridSmoother<DIM_2D>;
template class GridSmoother<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

