#ifndef __FUNCTIONALGRID_H__
#define __FUNCTIONALGRID_H__


#include "libcoresystem/mgdecl.h"
#include "libgreen/grid.h"

#include "functionalbase.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



class SubFuncInvP1
{
public:
	static MGFloat Eval( const MGFloat& x)		{ return 1.0 / ( x ); }
	static MGFloat EvalDiff( const MGFloat& x)	{ return -1.0 / ( x*x ); }
};

class SubFuncInvP2
{
public:
	static MGFloat Eval( const MGFloat& x)		{ return 1.0 / ( x*x ); }
	static MGFloat EvalDiff( const MGFloat& x)	{ return -2.0 / ( x*x*x ); }
};

class SubFuncInvP3
{
public:
	static MGFloat Eval( const MGFloat& x)		{ return 1.0 / ( x*x*x ); }
	static MGFloat EvalDiff( const MGFloat& x)	{ return -3.0 / ( x*x*x*x ); }
};




template <Dimension DIM>
class FunctionalGrid : public FunctionalBase
{
public:
	typedef typename GREEN::TDefs<DIM>::GVect		GVect;


	FunctionalGrid( GREEN::Grid<DIM>& grid) : mGrid(grid), mN(0)	{}

	void	Init( const vector<MGSize>& tabbnd);
	void	SetupParams( vector<MGFloat>& tabx, vector<MGFloat>& tabgrad);

	const MGSize&	cSize() const		{ return mN; }

	const MGSize&	cLocId( const MGSize idglob) const		{ return mtabMap[idglob]; }

	virtual void	Evaluate( MGFloat* val,  MGFloat* tabgrad, const MGFloat* tabx, const MGSize& n);


protected:
	void	Reset(  MGFloat* val,  MGFloat* tabgrad, const MGSize& n);

	template <class TSubFunc, class TFunc>
	void	EvaluateSUM( MGFloat* val,  MGFloat* tabgrad, const MGFloat* tabx, const MGSize& n);

	template <class TSubFunc, class TFunc>
	void	EvaluateEXP( MGFloat* val,  MGFloat* tabgrad, const MGFloat* tabx, const MGSize& n);

private:
	Grid<DIM>&	mGrid;

	MGSize	mN;
	vector<MGSize>	mtabMap;
	//vector<
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __FUNCTIONALGRID_H__

