#ifndef __GRIDSMOOTHER_H__
#define __GRIDSMOOTHER_H__


#include "libcoresystem/mgdecl.h"
#include "libextget/geomentity.h"

#include "libgreen/grid.h"
#include "libgreen/bndgrid.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class GridSmoother
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class GridSmoother
{
	typedef typename TDefs<DIM>::GVect		GVect;
	//typedef typename TDefs<DIM>::GMetric	GMetric;

	//typedef typename GMetric::SMtx			SMtx;


public:
	GridSmoother()
	{ 
		//mCountLost = mCountHit = mCount = 0;
		//mFile.open( "_lost_points.dat");
	}

	virtual ~GridSmoother()		
	{ 
		//cout << "mCountLost = " << mCountLost << endl; 
		//cout << "mCountHit = " << mCountHit << endl; 
		//cout << "mCount = " << mCount << endl; 
		//mFile.close();
	}

	void	InitFileMSH2( const MGString& fname);
	void	PostInit();

	void	Execute();

	void	WriteGridTEC( const MGString& fname);

private:
	void	ExtractBlob();

private:
	Grid<DIM>		mGrid;
	BndGrid<DIM>	mpBndGrid;

	vector<MGSize>	mtabBNode;		
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __GRIDSMOOTHER_H__
