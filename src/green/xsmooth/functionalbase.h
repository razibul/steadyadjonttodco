#ifndef __FUNCTIONALBASE_H__
#define __FUNCTIONALBASE_H__


class FunctionalBase
{
public:
	//void	Value( MGFloat& val, const vector<MGFloat>& tabx);
	//void	Gradient( vector<MGFloat>& tabgrad, const vector<MGFloat>& tabx);

	//void	Evaluate( MGFloat& val, vector<MGFloat>& tabgrad, const vector<MGFloat>& tabx);

	virtual void	Evaluate( MGFloat* val,  MGFloat* tabgrad, const MGFloat* tabx, const MGSize& n) = 0;
};




class FunctionalTest : public FunctionalBase
{
public:

	virtual void	Evaluate( MGFloat* val,  MGFloat* tabgrad, const MGFloat* tabx, const MGSize& n)
	{
		*val = 2.0;
		for ( MGSize i=0 ; i<n; ++i)
		{
			*val += (tabx[i]-1.0)*(tabx[i]-1.0);
			tabgrad[i] = 2.0 * (tabx[i]-1.0);
		}
	}
};


#endif // __FUNCTIONALBASE_H__
