#ifndef __MANAGER_H__
#define __MANAGER_H__


#include "master.h"
#include "libcoreconfig/config.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
//	class Manager
//////////////////////////////////////////////////////////////////////
class Manager
{
public:
	Manager()	{}

	void	ReadConfig( const MGString& fname);

	void Execute();

	template <Dimension DIM>
	void ExecuteMaster();


private:
	Config			mConfig;
};
//////////////////////////////////////////////////////////////////////

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __MANAGER_H__
 