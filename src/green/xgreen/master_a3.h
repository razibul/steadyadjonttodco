
template <Dimension DIM>
void Master<DIM>::CreateBndGrid_A3_6( BndGrid<DIM_2D>& bgrd)
{
	const MGSize id = 6;
	const GET::TopoEntity<DIM_2D>& tent = mGeTMaster.cTopo().template cTEnt<DIM_2D>( id);

	cout << "Creating boundary grid - A3 6" << endl;

	GET::NullGeomEntity<DIM_2D,DIM> nullsrf;

	const GET::GeomEntity<DIM_2D,DIM> *psrf = &nullsrf;

	if ( tent.cGeomId() > 0)
		psrf = mGeTMaster.cGeom().template cGEnt<DIM_2D>( tent.cGeomId() );


	MGSize	offset=0;
	MGSize	idnode = 0;
	MGSize	idcell = 0;

	for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
	{
		const GET::TopoLoop	&loop = tent.cTab()[iloop].first;
		const bool &bloop = tent.cTab()[iloop].second;

		for ( MGSize iedg=0; iedg<loop.size(); ++iedg)
		{
			const MGSize &ide = loop[iedg].first;
			const bool &be = loop[iedg].second;

			//if ( ide != 1 && ide != 6 && ide != 21 )
			if ( ide != 54 && ide != 55 )
				continue;


			bool borient = (bloop && be) || ( !bloop && !be);

			const GET::TopoEntity<DIM_1D>& tedg = mGeTMaster.cTopo().template cTEnt<DIM_1D>( ide);
			const GET::GeomEntity<DIM_1D,DIM> *pgent = mGeTMaster.cGeom().template cGEnt<DIM_1D>( tedg.cGeomId() );

			const GridWBnd<DIM_1D> &grid = mGrdMaster.template cGrid<DIM_1D>( ide);


			for ( Grid<DIM_1D>::ColNode::const_iterator itrn = grid.cColNode().begin(); itrn != grid.cColNode().end(); ++itrn)
			{
				BndGrid<DIM_2D>::GBndNode	bnod;

				GVect v;
				if ( itrn->cMasterId() > 0 )
				{
					v = mGrdMaster.cMasterPosCx().cData( itrn->cMasterId() );	// use global node positions
					bnod.rMasterId() = itrn->cMasterId();
				}
				else
				{
					THROW_INTERNAL( "CreateBndGrid 2D : master representation of boundary node not found !");
					//pgent->GetCoord( v, (*itrn).cPos() );
					//MGSize idpos = mGrdMaster.rAuxData().Insert( v);
					//bnod.rMasterId() = idpos;
				}

				//v.Write();
				bnod.rPos() = psrf->FindParam( v );

				//cout << bnod.rMasterId() << endl;

				//bnod.cPos().Write();
				//cout << endl;
				//bnod.rGrdId() = tent.cId();	// TODO :: ??????????

				bnod.rId() = ++idnode;		// global node id

				bgrd.rColNode().push_back( bnod);
			}

			for ( Grid<DIM_1D>::ColCell::const_iterator itrc = grid.cColCell().begin(); itrc != grid.cColCell().end(); ++itrc)
			{
				BndGrid<DIM_2D>::GBndCell	bcell;

				bcell.rTopoId() = tedg.cId();
				bcell.rId() = ++idcell;		// global cell id

				if ( borient)
				{
					for ( MGSize k=0; k<BndGrid<DIM_2D>::GBndCell::SIZE; ++k)
						bcell.rNodeId(k) = (*itrc).cNodeId(k) + offset;
				}
				else
				{
					for ( MGSize k=0; k<BndGrid<DIM_2D>::GBndCell::SIZE; ++k)
						bcell.rNodeId(k) = (*itrc).cNodeId( BndGrid<DIM_2D>::GBndCell::SIZE-1 - k) + offset;
				}

				bgrd.rColCell().push_back( bcell );
			}
	
			offset += grid.SizeNodeTab();


		}
	}


	AddExternEdgeGrid( bgrd, "Bounding_curve.smesh", 6 );

	WriteBndTEC<DIM_2D>		bndwriter( &bgrd);
	bndwriter.DoWrite( "boundary_befdupl.plt");

	//bgrd.RemoveDuplicatePoints();
	bgrd.RemoveDuplicatePointsPosBased( 1.0e-5 );

	bndwriter.DoWrite( "boundary.plt");

}




template <>
void Master<DIM_3D>::GenerateA3()
{
	const ControlSpace<DIM_3D>* pcsp = mCSpMaster.GetGlobalCSpace();

	if ( pcsp == NULL)
		THROW_INTERNAL( "no global cspace defined");

	cout << "\nA3 -- Generating grids for 0D entities\n";
	GenerateLocal0D( pcsp);

	cout << "\nA3 -- Generating grids for 1D entities\n";
	GenerateLocal<DIM_1D>( pcsp);
	ExportTEC<DIM_1D>( "a3_grid_1d.dat");



////////////////////

	for ( MGSize i=1; i <= mGeTMaster.cTopo().Size<DIM_2D>(); ++i)
	{
		//if ( i != 5 && i != 7 && i != 8 )
		//	continue;

		cout << "  Entity id = " << i << endl;



		const GET::TopoEntity<DIM_2D>& tent = mGeTMaster.cTopo().cTEnt<DIM_2D>( i);


		MGSize gid = mGrdMaster.CreateGrid<DIM_2D>();

		//if ( gid != i)
		//	THROW_INTERNAL( "incompatible ids inside Master<DIM>::Generate()");


		cout << "DIM_2D = " << DIM_2D << endl;

		GridWBnd<DIM_2D>	&grid	= mGrdMaster.rGrid<DIM_2D>( gid);
		BndGrid<DIM_2D>		&bgrd	= grid.rBndGrid();


		const GET::GeomEntity<DIM_2D,DIM_3D> *pgent = mGeTMaster.cGeom().cGEnt<DIM_2D>( tent.cGeomId() );

		ostringstream ostr;
		ostr << "gent_D" << DIM_2D << "_id" << gid << ".dat";
		ofstream of( ostr.str().c_str() );
		pgent->ExportTEC( of);
		of.close();

		if ( i == 6 )
			CreateBndGrid_A3_6( bgrd );
		else
			CreateBndGrid( bgrd, tent);
	

		SliceControlSpace<DIM_2D,DIM_3D>	slicecspace( pcsp, pgent);

		GeomControlSpace<DIM_2D,DIM_3D>	gcspace( *pgent);
		CombineControlSpace<DIM_2D>		combcspace( slicecspace, gcspace);


		MGString secname = MGString( ConfigStr::Master::Generator::NAME) + MGString("_") + DimToStr(DIM_2D);
		const CfgSection* cfgsec = &ConfigSect().GetSection( secname);

		Generator<DIM_2D>	gen( grid, &combcspace );

		gen.Create( cfgsec);
		gen.PostCreateCheck();


		if ( i == 6 || i == 22 || i == 23  )
			gen.Process();

			//ofstream file( "_grid.dat", ios::out);
			//WriteGrdTEC<DIM_2D> wtec(&grid);
			//wtec.DoWrite( file, pgent );
			//file.close();


		for ( Grid<DIM_2D>::ColNode::iterator itrn = grid.rColNode().begin(); itrn != grid.rColNode().end(); ++itrn)
		{
			if ( itrn->cMasterId() == 0 )
			{
				// updating master representation of nodes
				GVect v;
				pgent->GetCoord( v, itrn->cPos() );
				MGSize idpos = mGrdMaster.rMasterPosCx().Insert( v);


				itrn->rMasterId() = idpos;
			}
		}

	}

/////////////////////
	ExportTEC<DIM_2D>( "grid_2d.dat");


	const GET::TopoEntity<DIM_3D>& tent = mGeTMaster.cTopo().cTEnt<DIM_3D>( 1);
	ExportSMESH( "out.smesh", tent);


}

