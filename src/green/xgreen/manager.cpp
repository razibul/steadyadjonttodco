#include "manager.h"
#include "master.h"

#include "libgreen/configconst.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

void Manager::ReadConfig( const MGString& fname)
{
	mConfig.ReadCFG( fname);
}

void Manager::Execute()
{
	Dimension dim;

	const CfgSection* cfgsec = &mConfig.GetSection( ConfigStr::Master::NAME);

	MGString sdim	= cfgsec->ValueString( ConfigStr::Master::Dim::KEY );
	dim = StrToDim( sdim);

	if ( dim == DIM_2D)
	{
		ExecuteMaster<DIM_2D>();
	}
	else
	if ( dim == DIM_3D)
	{
		ExecuteMaster<DIM_3D>();
	}
	else
	{
		THROW_INTERNAL( "Manager::Execute() -- unknown dim" );
	}
}


template <Dimension DIM>
void Manager::ExecuteMaster()
{
	const CfgSection* cfgsec = &mConfig.GetSection( ConfigStr::Master::NAME);

	Master<DIM>	master;

	master.Create( cfgsec);
	master.PostCreateCheck();
	master.Init();

	master.Generate();
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


