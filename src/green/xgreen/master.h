#ifndef __MASTER_H__
#define __MASTER_H__


#include "libgreen/tdefs.h"
#include "libextget/geotopomaster.h"
#include "libgreen/gridmaster.h"
#include "libgreen/controlspace.h"
#include "libgreen/cspacemaster.h"

#include "libcoreconfig/configbase.h"
#include "libcoreconfig/config.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//template <Dimension DIM>
//class BndGrid;

//////////////////////////////////////////////////////////////////////
//	class Master
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Master : public ConfigBase
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

public:
	Master()			{}
	virtual ~Master()	{}

	virtual void	Create( const CfgSection* pcfgsec);
	virtual void	PostCreateCheck() const;


	void	Init();
	void	Init( const MGString& fname);

	void	InitGET( const MGString& fname);
	void	InitGETExtract( const MGString& fname, const vector<MGSize>& tabfid);
	void	InitSTEP( const MGString& fname);
	void	InitCS( const MGString& fname);

	void	Generate();

	// HACK ::
	const ControlSpace<DIM>*	GetGlobalCSpace() const		{ return mCSpMaster.GetGlobalCSpace();}

	void	AddExternEdgeGrid( BndGrid<DIM_2D>& bgrd, const MGString& name, const MGSize& tid);
	void	CreateBndGrid_Onera_5( BndGrid<DIM_2D>& bgrd);
	void	GenerateOnera();

	void	CreateBndGrid_LANN_2( BndGrid<DIM_2D>& bgrd);
	void	GenerateLANN();

	void	CreateBndGrid_A3_6( BndGrid<DIM_2D>& bgrd);
	void	GenerateA3();

protected:

	void	CreateSrcGrid( BndGrid<DIM_1D>& bgrd, const GET::TopoEntity<DIM_1D>& tent);
	void	CreateSrcGrid( BndGrid<DIM_2D>& bgrd, const GET::TopoEntity<DIM_2D>& tent);
	void	CreateSrcGrid( BndGrid<DIM_3D>& bgrd, const GET::TopoEntity<DIM_3D>& tent);


	void	CreateBndGrid( BndGrid<DIM_1D>& bgrd, const GET::TopoEntity<DIM_1D>& tent);
	void	CreateBndGrid( BndGrid<DIM_2D>& bgrd, const GET::TopoEntity<DIM_2D>& tent);
	void	CreateBndGrid( BndGrid<DIM_3D>& bgrd, const GET::TopoEntity<DIM_3D>& tent);


	void	GenerateLocal0D();

	template <Dimension LDIM>
	void	GenerateLocal();

	void	GenerateLocal();	// specialization for LDIM = DIM


	void	GenerateLocal0D( const ControlSpace<DIM> *pcsp);

	template <Dimension LDIM>
	void	GenerateLocal( const ControlSpace<DIM> *pcsp);

	void	GenerateLocal( const ControlSpace<DIM> *pcsp);	// specialization for LDIM = DIM

	template <Dimension LDIM>
	void	OptimiseSurf( const GET::GeomEntity<LDIM,DIM>* pgent, Grid<LDIM>* pgrid)	{}

	//template <Dimension LDIM>
	//void	GenerateLocal();
	//void	GenerateLocal0D();


	void	ExportBMSH( const MGString& fname, const GET::TopoEntity<DIM>& tent);

	void	ExportSMESH( const MGString& fname, const GET::TopoEntity<DIM>& tent);
	//void	ExportSMESH( const MGString& fname, const GET::TopoEntity<DIM_3D>& tent);

	template <Dimension LDIM>
	void	ExportTEC( const MGString& fname);

	void	ExportMSH2( const MGString& fname);

private:
	//ControlSpace<DIM>*	mpCSpace;

	GET::GeoTopoMaster<DIM>	mGeTMaster;
	GridMaster<DIM>			mGrdMaster;
	CSpaceMaster<DIM>		mCSpMaster;

};
//////////////////////////////////////////////////////////////////////

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __MASTER_H__
