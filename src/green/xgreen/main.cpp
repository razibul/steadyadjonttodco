#include "libcoresystem/mgdecl.h"
#include "libgreen/grid.h"
#include "libgreen/bndgrid.h"
#include "libgreen/generator.h"
#include "libgreen/writebndtec.h"
#include "libgreen/writegrdtec.h"
#include "libgreen/readbndsmesh.h"
#include "libgreen/writegrdmsh2.h"

#include "libgreen/gridgeom.h"
#include "libgreen/cspacegrid.h"

#include "libcorecommon/progressbar.h"


#include "libcoreconfig/config.h"
#include "libgreen/configconst.h"
#include "master.h"
#include "manager.h"

#include "libextnurbs/nurbscurve.h"

#include "libcorecommon/fgvector.h"

#include "libgreen/readtec.h"
#include "libgreen/readmeandr.h"




#include "libcorecommon/smatrix.h"
#include "libcoregeom/simplex.h"



INIT_VERSION("0.1","'surface grid generation with GeT and STEP'");

using namespace GREEN;


struct Test
{
	MGSize	mtabSize[4];
	MGChar	mtabChar[4];
};

class A
{
public:
	MGSize ms;
	MGChar ma;
};

class B// : public A
{
public:
	A ma;
	MGChar mb;
};


template <class T1, class T2>
class Ne
{
public:
	Ne( T1& t1, T2& t2) : first(t1), second(t2)		{}
	T1& first;
	T2& second;
};

int main( int argc, char* argv[])
{
	try
	{
		cout << "ver.: " << VERSION_NUMBER << "  build: " << VERSION_DATE << " " << VERSION_TIME << endl << endl;
		///////////////////////////////////////////////////////////////////////
	
		exactinit();	// initialization of geom predicates

		///////////////////////////////////////////////////////////////////////

		typedef  TDefs<DIM_3D>::GNode		GNode;
		typedef  TDefs<DIM_3D>::GCell		GCell;
		typedef  TDefs<DIM_3D>::GFace		GFace;
		typedef  TDefs<DIM_3D>::GVect		GVect;


		cout << "sizeof( GVect) = " << sizeof( GVect) << endl;
		cout << "sizeof( GNode) = " << sizeof( GNode) << endl;
		cout << "sizeof( GCell) = " << sizeof( GCell) << endl;
		cout << "sizeof( GFace) = " << sizeof( GFace) << endl;

		cout << "sizeof( MGString) = " << sizeof( MGString) << endl;
		cout << "sizeof( MGSize) = " << sizeof( MGSize) << endl;
		cout << "sizeof( MGChar) = " << sizeof( MGChar) << endl;
		cout << "sizeof( bool) = " << sizeof( bool) << endl;

		cout << "sizeof( pair<MGSize,bool>) = " << sizeof( pair<MGSize,bool> ) << endl;
		cout << "sizeof( pair<GCell,bool>) = " << sizeof( pair<GCell,bool> ) << endl;
		cout << "sizeof( pair<GNode,bool>) = " << sizeof( pair<GNode,bool> ) << endl;
		cout << "sizeof( GNeDef) = " << sizeof( GNeDef ) << endl;
		cout << "sizeof( Test) = " << sizeof( Test ) << endl;
		cout << "sizeof( bitset<8>) = " << sizeof( bitset<8> ) << endl;

		cout << endl;

		cout << "sizeof( A) = " << sizeof( A ) << endl;
		cout << "sizeof( B) = " << sizeof( B ) << endl;
		cout << "sizeof( pair<A,MGChar>) = " << sizeof( pair<A,MGChar> ) << endl;
		cout << "sizeof( A[3]) = " << sizeof( A[3]) << endl;



		Vect3D  v3( 167.5, 265, -15);
		Vect3D v12( 0, 150, 50 );
		Vect3D v13( 0, 116.66666650000001, 66.666666500000005 );
		Vect3D v16( 5, 148.08302800000001, 94.834008100000005 );
		Vect3D v21( 5, 145.21541999999999, 95.278195499999995 );
		Vect3D v29( 5, 149.10866899999999, 91.724696399999999 );
		Vect3D v44( 5, 146.39455799999999, 92.390977399999997 );

		Simplex<DIM_3D> tet1(  v3, v21, v16, v29 );
		Simplex<DIM_3D> tet2( v16, v21, v12, v29 );

		Simplex<DIM_3D> tet3( v13, v29, v21, v44 );
		Simplex<DIM_3D> tet4(  v3, v21, v29, v44 );

		cout << tet1.IsInsideSphere( v44) << endl;
		cout << tet2.IsInsideSphere( v44) << endl;

		cout << tet3.IsInsideSphere( v16) << endl;
		cout << tet4.IsInsideSphere( v16) << endl;

		//Vect3D v1( 11866.195, 6319.2422, -1042.262 );
		//Vect3D v2( 11803.367, 6319.2109, -1047.3501 );
		//Vect3D v3( 11735.703, 5909.6738, -1045.6149 );
		//Vect3D v4( 11753.828, 6114.4346, -1047.7545 );

		//Simplex<DIM_3D> tet( v1, v2, v3, v4 );

		//Vect3D vc = tet.Center();

		//if ( tet.IsInside( vc) )
		//	cout << "inside" << endl;
		//else
		//	cout << "NOT inside" << endl;

		//if ( tet.IsInsideSphere( vc) )
		//	cout << "inside sphere" << endl;
		//else
		//	cout << "NOT inside sphere" << endl;
		//
		//return 0;

/*		SMatrix<MGFloat,5>	mtxA, mtxAA, mtxL, mtxLI, mtxD;
		SVector<MGFloat,5>	vecD;

		mtxA.Resize(3,3);
		mtxA(0,0) = 1;
		mtxA(1,1) = 2;
		mtxA(2,2) = 3;
		mtxA(0,1) = mtxA(1,0) = 2;
		mtxA(0,2) = mtxA(2,0) = 1;
		mtxA(1,2) = mtxA(2,1) = 1;

		//mtxA.InitDelta(3);
		mtxA.Init( 1.0);

		mtxA.Write();

		mtxA.DecomposeNew(vecD, mtxL);

		vecD.Write();
		mtxL.Write();

		mtxLI = mtxL;
		mtxLI.Invert();

		mtxAA.Assemble(mtxL,vecD,mtxLI);

		mtxAA.Write();


		return 0;
*/
		//vector<MGFloat>	taba;

		//Vect2D	v0(0,0), v1(1,0), v2(1,1), vv(0.5,0.25);
		////Vect2D	v0(0,0), v1(1,0), v2(0.5,::sqrt(0.75)), vv(0.5,0.25);
		//Simplex<DIM_2D>	simpTri(v0,v1,v2);

		//cout << "r     = " << simpTri.InscribedSphereRadius() << endl;
		//cout << "R     = " << simpTri.CircumSphereRadius() << endl;
		//cout << "ALPHA = " << simpTri.QMeasureALPHA() << endl;
		//cout << "BETA  = " << simpTri.QMeasureBETA() << endl;

		//cout << " vol = " << simpTri.Volume() << endl;

		//for ( int i=0; i<Simplex<DIM_2D>::SIZE; ++i)
		//	cout << " vol face = " << simpTri.FaceSmpxVolume( vv, i) << endl;

		//simpTri.FaceAngles( taba);

		//for ( MGSize i=0; i<Simplex<DIM_2D>::SIZE; ++i)
		//{	
		//	cout << "  fac Vn " << i << " = ";
		//	simpTri.GetFace(i).Vn().Write();
		//}

		//for ( MGSize i=0; i<taba.size(); ++i)
		//	cout << "  angle " << i << " = " << 180/M_PI*taba[i] << endl;


		//cout << endl;

		//Vect3D	w0(0,0,0), w1(1,0,0), w2(0.5,::sqrt(0.75),0), w3(0.5, ::sqrt(0.75)/3.0, ::sqrt(6.0)/3.0), ww(0.5,0.25,0.5);
		////Vect3D	w0(0,0,0), w1(1,0,0), w2(1,1,0), w3(0.5, 0.2, 1), ww(0.5,0.25,0.5);
		//Simplex<DIM_3D>	simpTet(w0,w1,w2,w3);

		//cout << "r     = " << simpTet.InscribedSphereRadius() << endl;
		//cout << "R     = " << simpTet.CircumSphereRadius() << endl;
		//cout << "ALPHA = " << simpTet.QMeasureALPHA() << endl;
		//cout << "BETA  = " << simpTet.QMeasureBETA() << endl;

		//cout << " vol = " << simpTet.Volume() << endl;

		//for ( int i=0; i<Simplex<DIM_3D>::SIZE; ++i)
		//	cout << " vol face = " << simpTet.FaceSmpxVolume( w3, i) << endl;

		//simpTet.FaceAngles( taba);

		//for ( MGSize i=0; i<Simplex<DIM_3D>::SIZE; ++i)
		//{	
		//	cout << "  fac Vn " << i << " = ";
		//	simpTri.GetFace(i).Vn().Write();
		//}

		//for ( MGSize i=0; i<taba.size(); ++i)
		//	cout << "  angle " << i << " = " << 180/M_PI*taba[i] << endl;

		//return 0;

		////////////////////////////////////////////////////////////////////////
		//// crossing tests
		//Vect3D v1(0,1,0), v2(4,5,4), v3(1,1,3), v4(3,5,1);
		//Vect3D vc;

		//bool b = FindEdgeEdgeCross( vc, v4, v3, v2, v1);

		//vc.Write();

		//Vect3D vf1(0,1,-2), vf2(2,1,2), vf3(2,2,2);
		//Vect3D ve1(2.5, 2.5, 0), ve2(0.5, 0.5, 2);

		//bool b2 = FindEdgeFaceCross( vc, ve1, ve2, vf1, vf2, vf3);

		//vc.Write();

		//return 0;


/*		
		Grid<DIM_3D>	grid;

		ReadMEANDR<DIM_3D>	readme( grid);
		readme.DoRead( "expe");

		//ReadTEC<DIM_3D>	readtec( grid);
		//readtec.DoRead( "_sphere.dat");

		WriteGrdTEC<DIM_3D> wtec( &grid);
		wtec.DoWrite( "_out_.dat");

		return 0;

		///////////////////////////////////////////////////////////////////////


		fgvector<double>	tab1;

		MGSize nn = 7;

		tab1.resize( nn, 2);

		//for ( MGSize i=1; i<=nn; ++i)
		//	tab1[i] = i+100;

		for ( fgvector<double>::iterator itr=tab1.begin(); itr!= tab1.end(); ++itr)
			cout << *itr << endl;
		cout << endl;

		return 0;

		tab1.insert( 1.2);
		tab1.insert( 2.2);
		tab1.insert( 3.2);
		tab1.insert( 4.2);
		tab1.insert( 5.2);

		tab1.erase( 3);

		//tab2 = tab1;
		fgvector<double>	tab2( tab1);

		cout << &tab1[0] << endl;
		cout << &tab2[0] << endl;
		cout << endl;

		tab1.insert( 30);


		for ( fgvector<double>::iterator itr=tab1.begin(); itr!= tab1.end(); ++itr)
			cout << *itr << endl;
		cout << endl;

		for ( fgvector<double>::iterator itr=tab2.begin(); itr!= tab2.end(); ++itr)
			cout << *itr << endl;
		cout << endl;

		return 0;
*/

		///////////////////////////////////////////////////////////////////////
		//NURBS::NurbsCurve<MGFloat,DIM_2D>	crv;
		//vector<Vect2D>	tabPnt;

		//tabPnt.push_back( Vect2D( 0. ,0.) );
		//tabPnt.push_back( Vect2D( 1. ,1.2) );
		//tabPnt.push_back( Vect2D( 1.2 ,0.7) );
		//tabPnt.push_back( Vect2D( 2. ,0.1) );
		//crv.GlobalInterpol( tabPnt, 3);
		//crv.ExportCrvTEC( "curve.dat", 100);
		//MGFloat t = crv.PointProject( Vect2D( 1.3 ,0.9), 10 );

		//cout << "t = " << t << endl;
		//Vect2D vct;
		//crv.CalcPoint( vct, t);
		//vct.Write();
	
		//return 0;

///////////////////////////////////////////////////////////////////////
/*
		Master<DIM_2D>	master;

		master.InitGET( "nc0012");
		master.InitCS( "nc0012");


		//Master<DIM_3D>	master;
		//Master<DIM_2D>	master;

		//MGString name ="onera_m6_ws.stp";

		//master.InitGET( "onera_m6_nx");
		//master.InitGET( "nc0012");
		//master.InitGET( "example");
		//master.InitGET( "converted");

		//master.InitSTEP( "test_box.stp");
		//master.InitSTEP( "m6-nx.stp");
		//master.InitSTEP( "onera_m6_ws.stp");
		
		//master.InitCS( "nc0012");
		//master.InitCS( "box");

		master.Generate();

		return 0;
*/


		//Master<DIM_2D>	master;
		//master.Init( "nc0012");

		//Master<DIM_3D>	master;
		//master.Init( "onera_m6");

		//Master<DIM_3D>	master;
		//master.Init( "VFE2");


		//Master<DIM_3D>	master;
		//master.Init( "btc0_1");

		//master.Init( "btc0_sphere");
		//master.Init( "btc0_sphere_surf1");
		//master.Init( "btc0_body_surf3");
		//master.Init( "btc0_body_surf2");
		//master.Init( "btc0_body");
		//master.Init( "btc0_1");

		//master.Init( "test_onera_m6");
		
		//master.InitSTEP( "test_box.stp");
		//master.InitCS( "box");


		//master.Generate();

		//return 0;

		///////////////////////////////////////////////////////////////////////

		MGString cfgname = "green.cfg";
		Config config;
		config.ReadCFG( cfgname);


		if ( argc < 3)
		{
			printf( "call:\txgreen [2D|3D] [get name]\n");
			return 1;
		}

		//if ( MGString(argv[1]) == MGString("config") )
		//{
		//	Manager	manager;
		//	manager.ReadConfig( argv[2]);
		//	manager.Execute();
		//}
		//else
		if ( MGString(argv[1]) == MGString("get") )
		{
			vector<MGSize> tabflag;

			for ( MGSize i=3; i<(MGSize)argc; ++i)
			{
				MGSize flag;
				istringstream is;
				is.str( argv[i] );
				is >> flag;
				tabflag.push_back( flag);
			}

			for ( MGSize i=0;i<tabflag.size(); ++i)
				cout << tabflag[i] << " ";
			cout << endl;


			Master<DIM_3D>	master;
			//master.InitSTEP( "sbend.stp");
			//master.InitSTEP( "ADIGMA_BTC0_f_full_cf.stp");
			//master.InitSTEP( "idihom_Iryda_acn.stp");
			//master.InitSTEP( "IRYDA_ready_solid.stp");
			master.InitGETExtract( argv[2], tabflag);
		}
		else
		if ( MGString(argv[1]) == MGString("step") )
		{
			Master<DIM_3D>	master;
			//master.InitSTEP( "sbend.stp");
			//master.InitSTEP( "ADIGMA_BTC0_f_full_cf.stp");
			//master.InitSTEP( "idihom_Iryda_acn.stp");
			//master.InitSTEP( "IRYDA_ready_solid.stp");
			master.InitSTEP( argv[2]);
		}
		else
		if ( MGString(argv[1]) == MGString("2D") )
		{
			Master<DIM_2D>	master;

			master.Create( &config.GetSection( ConfigStr::Master::NAME) );
			master.PostCreateCheck();
			master.Init( argv[2]);

			master.Generate();
		}
		else
		if ( MGString(argv[1]) == MGString("3D") )
		{
			Master<DIM_3D>	master;

			master.Create( &config.GetSection( ConfigStr::Master::NAME) );	
			master.PostCreateCheck();
			master.Init( argv[2]);

			master.Generate();
		}
		else
		if ( argc == 3)
		{
			if ( ::strcmp( argv[1], "-exp") == 0 )
			{
				GridWBnd<DIM_2D>		grid;
				Generator<DIM_2D>	gen(grid);

				MGSize n;
				sscanf( argv[2], "%d", &n);

				gen.GenerateExp( n);
				
				WriteGrdTEC<DIM_2D> wtec( &grid);
				wtec.DoWrite( "_final_.dat");
			}
			else
			if ( ::strcmp( argv[1], "-b2d") == 0 )
			{
				GridWBnd<DIM_2D>	grid;
				BndGrid<DIM_2D>		&bnd = grid.rBndGrid();

				ReadBndSMESH<DIM_2D>	bndreader( &bnd);
				bndreader.DoRead( argv[2]);
				
				ProgressBar	bar(40);
				bar.Init( 10 );
				bar.Start();

				bnd.RemoveDuplicatePointsPosBased();

				WriteBndTEC<DIM_2D>		bndwriter( &bnd);
				bndwriter.DoWrite( "boundary.plt");


				GridContext<TDefs<DIM_2D>::GMetric> cxmet;
				bnd.FindSpacing( cxmet);


				GridControlSpace<DIM_2D,DIM_2D>	cspace;
				cspace.Init( cxmet, bnd);

				const CfgSection* cfgsec = &config.GetSection( ConfigStr::Master::NAME);
				MGString secname = MGString( ConfigStr::Master::Generator::NAME) + MGString("_") + DimToStr(DIM_2D);
				const CfgSection* cfggensec = &cfgsec->GetSection( secname);

				const MGFloat scale = cfggensec->ValueFloat( ConfigStr::Master::Generator::GEN_SCALE );

				//GridControlSpace<DIM_2D,DIM_2D> cspace;
				//cspace.InitFileMEANDR( "cspacegrid_2D");
				//cspace.ApplyScale( scale);



				Generator<DIM_2D>	gen( grid, &cspace);

				gen.Create( cfggensec);
				gen.PostCreateCheck();

				gen.Process();

				bar.Finish();
				
				WriteGrdTEC<DIM_2D> wtec( &grid);
				wtec.DoWrite( "_final_.dat");

				cout << "Write msh2" << endl;
				WriteGrdMSH2<DIM_2D> wmsh2( &grid);
				wmsh2.DoWrite( "_final_.msh2" );
			}
			else
			if ( ::strcmp( argv[1], "-t2d") == 0 )
			{
				GridWBnd<DIM_2D>		grid;
				Generator<DIM_2D>	gen(grid);

				MGSize n;
				sscanf( argv[2], "%d", &n);

				gen.GenerateTestCube( n);
				
				WriteGrdTEC<DIM_2D> wtec( &grid);
				wtec.DoWrite( "_final_.dat");

				WriteGrdMSH2<DIM_2D> wmsh2( &grid);
				wmsh2.DoWrite( "_final_.msh2" );
			}
			else
			if ( ::strcmp( argv[1], "-b3d") == 0 )
			{
				GridWBnd<DIM_3D>	grid;
				BndGrid<DIM_3D>		&bnd = grid.rBndGrid();

				ReadBndSMESH<DIM_3D>	bndreader( &bnd);
				bndreader.DoRead( argv[2]);
				
				ProgressBar	bar(40);
				bar.Init( 1, false );
				bar.Start();

				bnd.RemoveDuplicatePointsPosBased();

				WriteBndTEC<DIM_3D>		bndwriter( &bnd);
				bndwriter.DoWrite( "boundary.plt");


				GridContext<TDefs<DIM_3D>::GMetric> cxmet;
				bnd.FindSpacing( cxmet);


				GridControlSpace<DIM_3D,DIM_3D>	cspace;
				cspace.Init( cxmet, bnd);

				const CfgSection* cfgsec = &config.GetSection( ConfigStr::Master::NAME);
				MGString secname = MGString( ConfigStr::Master::Generator::NAME) + MGString("_") + DimToStr(DIM_3D);
				const CfgSection* cfggensec = &cfgsec->GetSection( secname);


				Generator<DIM_3D>	gen( grid, &cspace);

				gen.Create( cfggensec);
				gen.PostCreateCheck();

				gen.Process();

				bar.Finish();
				
				WriteGrdTEC<DIM_3D> wtec( &grid);
				wtec.DoWrite( "_final_.dat");

				WriteGrdMSH2<DIM_3D> wmsh2( &grid);
				wmsh2.DoWrite( "_final_.msh2" );

			}
			else
			if ( ::strcmp( argv[1], "-t3d") == 0 )
			{
				GridWBnd<DIM_3D>		grid;
				Generator<DIM_3D>	gen(grid);

				MGSize n;
				sscanf( argv[2], "%d", &n);

				gen.GenerateTestCube( n);
				
				WriteGrdTEC<DIM_3D> wtec( &grid);
				wtec.DoWrite( "_final_.dat");
			}
			else
			if ( ::strcmp( argv[1], "-onera") == 0 )
			{
				Master<DIM_3D>	master;
				master.Create( &config.GetSection( ConfigStr::Master::NAME) );
				master.PostCreateCheck();

				master.Init( "onera_m6" );
				master.GenerateOnera();
			}
			else
			if ( ::strcmp( argv[1], "-lann") == 0 )
			{
				Master<DIM_3D>	master;
				master.Create( &config.GetSection( ConfigStr::Master::NAME) );
				master.PostCreateCheck();

				master.Init( "lann" );
				master.GenerateLANN();
			}
			else
			if ( ::strcmp( argv[1], "-a3") == 0 )
			{
				Master<DIM_3D>	master;
				master.Create( &config.GetSection( ConfigStr::Master::NAME) );
				master.PostCreateCheck();

				master.Init( "a3_iges_fixed" );
				master.GenerateA3();
			}
			else
			if ( ::strcmp( argv[1], "-b3donera") == 0 )
			{
				Master<DIM_3D>	master;
				master.Create( &config.GetSection( ConfigStr::Master::NAME) );
				master.PostCreateCheck();

				master.Init( "onera_m6" );

				GridWBnd<DIM_3D>	grid;
				BndGrid<DIM_3D>		&bnd = grid.rBndGrid();

				ReadBndSMESH<DIM_3D>	bndreader( &bnd);
				bndreader.DoRead( argv[2]);
				
				ProgressBar	bar(40);
				bar.Init( 1, false );
				bar.Start();

				bnd.RemoveDuplicatePointsPosBased( 1.0e-12 );

				WriteBndTEC<DIM_3D>		bndwriter( &bnd);
				bndwriter.DoWrite( "boundary.plt");

				//
				GridContext<TDefs<DIM_3D>::GMetric> cxmet;
				bnd.FindSpacing( cxmet);

				GridControlSpace<DIM_3D,DIM_3D>	cspace;
				cspace.Init( cxmet, bnd);

				//
				//Master<DIM_3D>	master;
				//master.Init( "onera_m6" );

				const ControlSpace<DIM_3D> *pcspace;

				pcspace = master.GetGlobalCSpace();
				//pcspace = &cspace;

				const CfgSection* cfgsec = &config.GetSection( ConfigStr::Master::NAME);
				MGString secname = MGString( ConfigStr::Master::Generator::NAME) + MGString("_") + DimToStr(DIM_3D);
				const CfgSection* cfggensec = &cfgsec->GetSection( secname);


				Generator<DIM_3D>	gen( grid, pcspace);

				gen.Create( cfggensec);

				gen.Process();

				bar.Finish();
				
				WriteGrdTEC<DIM_3D> wtec( &grid);
				wtec.DoWrite( "_final_.dat");

				WriteGrdMSH2<DIM_3D> wmsh2( &grid);
				wmsh2.DoWrite( "_final_.msh2" );
			}
			else
			if ( ::strcmp( argv[1], "-b3dboxv1") == 0 )
			{
				Master<DIM_3D>	master;
				master.Create( &config.GetSection( ConfigStr::Master::NAME) );
				master.PostCreateCheck();

				master.Init( "test_box_v1" );

				GridWBnd<DIM_3D>	grid;
				BndGrid<DIM_3D>		&bnd = grid.rBndGrid();

				ReadBndSMESH<DIM_3D>	bndreader( &bnd);
				bndreader.DoRead( argv[2]);
				
				ProgressBar	bar(40);
				bar.Init( 1, false );
				bar.Start();

				bnd.RemoveDuplicatePointsPosBased( 1.0e-12 );

				WriteBndTEC<DIM_3D>		bndwriter( &bnd);
				bndwriter.DoWrite( "boundary.plt");

				//
				GridContext<TDefs<DIM_3D>::GMetric> cxmet;
				bnd.FindSpacing( cxmet);

				GridControlSpace<DIM_3D,DIM_3D>	cspace;
				cspace.Init( cxmet, bnd);

				//
				//Master<DIM_3D>	master;
				//master.Init( "onera_m6" );

				const ControlSpace<DIM_3D> *pcspace;

				pcspace = master.GetGlobalCSpace();
				//pcspace = &cspace;

				const CfgSection* cfgsec = &config.GetSection( ConfigStr::Master::NAME);
				MGString secname = MGString( ConfigStr::Master::Generator::NAME) + MGString("_") + DimToStr(DIM_3D);
				const CfgSection* cfggensec = &cfgsec->GetSection( secname);


				Generator<DIM_3D>	gen( grid, pcspace);

				gen.Create( cfggensec);

				gen.Process();

				bar.Finish();
				
				WriteGrdTEC<DIM_3D> wtec( &grid);
				wtec.DoWrite( "_final_.dat");

				WriteGrdMSH2<DIM_3D> wmsh2( &grid);
				wmsh2.DoWrite( "_final_.msh2" );
			}
			else
			if ( ::strcmp( argv[1], "-b3dlann") == 0 )
			{
				Master<DIM_3D>	master;
				master.Create( &config.GetSection( ConfigStr::Master::NAME) );
				master.PostCreateCheck();

				master.Init( "lann" );

				GridWBnd<DIM_3D>	grid;
				BndGrid<DIM_3D>		&bnd = grid.rBndGrid();

				ReadBndSMESH<DIM_3D>	bndreader( &bnd);
				bndreader.DoRead( argv[2]);
				
				ProgressBar	bar(40);
				bar.Init( 1, false );
				bar.Start();

				bnd.RemoveDuplicatePointsPosBased( 1.0e-12 );

				WriteBndTEC<DIM_3D>		bndwriter( &bnd);
				bndwriter.DoWrite( "boundary.plt");

				//
				GridContext<TDefs<DIM_3D>::GMetric> cxmet;
				bnd.FindSpacing( cxmet);

				GridControlSpace<DIM_3D,DIM_3D>	cspace;
				cspace.Init( cxmet, bnd);

				//
				//Master<DIM_3D>	master;
				//master.Init( "onera_m6" );

				const ControlSpace<DIM_3D> *pcspace;

				pcspace = master.GetGlobalCSpace();
				//pcspace = &cspace;

				const CfgSection* cfgsec = &config.GetSection( ConfigStr::Master::NAME);
				MGString secname = MGString( ConfigStr::Master::Generator::NAME) + MGString("_") + DimToStr(DIM_3D);
				const CfgSection* cfggensec = &cfgsec->GetSection( secname);

				Generator<DIM_3D>	gen( grid, pcspace);

				gen.Create( cfggensec);

				gen.Process();

				bar.Finish();
				
				WriteGrdTEC<DIM_3D> wtec( &grid);
				wtec.DoWrite( "_final_.dat");

				WriteGrdMSH2<DIM_3D> wmsh2( &grid);
				wmsh2.DoWrite( "_final_.msh2" );
			}
			else
			if ( ::strcmp( argv[1], "-b3da3") == 0 )
			{
				Master<DIM_3D>	master;
				master.Create( &config.GetSection( ConfigStr::Master::NAME) );
				master.PostCreateCheck();

				master.Init( "a3_iges_fixed" );

				GridWBnd<DIM_3D>	grid;
				BndGrid<DIM_3D>		&bnd = grid.rBndGrid();

				ReadBndSMESH<DIM_3D>	bndreader( &bnd);
				bndreader.DoRead( argv[2]);
				
				ProgressBar	bar(40);
				bar.Init( 1, false );
				bar.Start();

				bnd.RemoveDuplicatePointsPosBased( 1.0e-12 );

				WriteBndTEC<DIM_3D>		bndwriter( &bnd);
				bndwriter.DoWrite( "boundary.plt");

				//
				GridContext<TDefs<DIM_3D>::GMetric> cxmet;
				bnd.FindSpacing( cxmet);

				GridControlSpace<DIM_3D,DIM_3D>	cspace;
				cspace.Init( cxmet, bnd);

				//
				//Master<DIM_3D>	master;
				//master.Init( "onera_m6" );

				const ControlSpace<DIM_3D> *pcspace;

				pcspace = master.GetGlobalCSpace();
				//pcspace = &cspace;

				const CfgSection* cfgsec = &config.GetSection( ConfigStr::Master::NAME);
				MGString secname = MGString( ConfigStr::Master::Generator::NAME) + MGString("_") + DimToStr(DIM_3D);
				const CfgSection* cfggensec = &cfgsec->GetSection( secname);


				Generator<DIM_3D>	gen( grid, pcspace);

				gen.Create( cfggensec);
				gen.PostCreateCheck();


				gen.Process();

				bar.Finish();
				
				WriteGrdTEC<DIM_3D> wtec( &grid);
				wtec.DoWrite( "_final_.dat");

				WriteGrdMSH2<DIM_3D> wmsh2( &grid);
				wmsh2.DoWrite( "_final_.msh2" );

			}

		}


		//vector<MGSize>	tabc;
		//tabc.push_back( 23);
		//tabc.push_back( 21);
		//tabc.push_back( 22);

		//wtec.WriteExploded( "_exp.dat", tabc, 3);

		return 0;


	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		cout << endl << "System exception: " << e.what() << endl;
	}
	catch( ...) 
	{
		TRACE( "Unknown exception thrown" );
		cout << endl << "Unknown exception thrown" << endl;
	}

	return 0;
} 

