#include "master.h"
#include "libextget/geotopoio.h"
#include "libextget/topoanalyzer.h"

#include "libgreen/bndgrid.h"
#include "libgreen/cspacegeom.h"
#include "libgreen/cspacegeomcurv.h"

#include "libgreen/generator.h"
#include "libgreen/writegrdtec.h"
#include "libgreen/writebndtec.h"
#include "libgreen/writegrdmsh2.h"


#include "libextget/convertstep.h"

#include "libgreen/cspaceio.h"
#include "libgreen/cspacegrid.h"
#include "libgreen/cspaceuniform.h"
#include "libgreen/cspacecombine.h"
#include "libgreen/cspaceslice.h"
#include "libgreen/cspaceblend.h"

#include "libgreen/bmsh.h"

#include "libcorecommon/progressbar.h"

#include "libgreen/configconst.h"

#include "libgreen/optimiser_surf.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
void Master<DIM>::Create( const CfgSection* pcfgsec)
{
	ConfigBase::Create( pcfgsec);
}


template <Dimension DIM>
void Master<DIM>::PostCreateCheck() const
{
	ConfigBase::PostCreateCheck();
}


template <Dimension DIM>
void Master<DIM>::Init()
{
	MGString fname = ConfigSect().ValueString( ConfigStr::Master::CASE );

	InitGET( fname);
	InitCS( fname);
}



template <Dimension DIM>
void Master<DIM>::Init( const MGString& fname)
{
	InitGET( fname);
	InitCS( fname);
}





template <Dimension DIM>
void Master<DIM>::InitSTEP( const MGString& fname)
{
	THROW_INTERNAL( "Only 3D STEP is supported");
}


template <>
void Master<DIM_3D>::InitSTEP( const MGString& fname)
{
	Step::StepMaster	step;

	step.Read( fname);
	step.CreateStepObjects();
	//step.RemoveUnreferencedObj();

	//step.WriteDotGraph( "step.dot", fname, "REPRESENTATION_ITEM");
	step.WriteDotGraph( "step.dot", fname);

	//cout << "1" << endl;
	GET::ConvertStep	convert( &step, &mGeTMaster);

	//cout << "2" << endl;
	convert.DoConvert();

	//cout << "3" << endl;
	GET::GeoTopoIO<DIM_3D>	getIO( mGeTMaster);
	getIO.DoWrite( "converted.get");

	//cout << "4" << endl;

	//THROW_INTERNAL( "END");
}


template <Dimension DIM>
void Master<DIM>::InitGET( const MGString& fname)
{
	GET::GeoTopoIO<DIM>	getIO( mGeTMaster);

	ostringstream os;
	os << fname << ".get";

	getIO.DoRead( os.str() );
	getIO.DoWrite( "converted.get");

	cout << endl;

	GET::TopoAnalyzer<DIM> tanal( mGeTMaster.cTopo() );
	tanal.CheckEdges();
}


template <Dimension DIM>
void Master<DIM>::InitGETExtract( const MGString& fname, const vector<MGSize>& tabfid)
{
	GET::GeoTopoIO<DIM>	getIO( mGeTMaster);

	ostringstream os;
	os << fname << ".get";

	getIO.DoRead( os.str() );

	getIO.ExtractFaces( tabfid );
	
	getIO.DoWrite( "converted__.get");

	getIO.ExportTEC( "conv.dat" );
	cout << endl;

	GET::TopoAnalyzer<DIM> tanal( mGeTMaster.cTopo() );
	tanal.CheckEdges();
}






template <Dimension DIM>
void Master<DIM>::InitCS( const MGString& fname)
{
	CSpaceIO<DIM>	csIO( mCSpMaster);

	ostringstream os;
	os << fname << ".csp";

	csIO.DoRead( os.str() );
	csIO.DoWrite( "converted.csp");

	cout << endl;
}







template <Dimension DIM>
template <Dimension LDIM>
void Master<DIM>::ExportTEC( const MGString& fname)
{
	ofstream file( fname.c_str(), ios::out);

	GET::NullGeomEntity<LDIM,DIM> nullgent;

	try
	{
		if ( ! file)
			THROW_FILE( "can not open the file", fname);

		cout << "\nWriting TEC " << fname << endl;
		cout.flush();

		if ( mGeTMaster.cTopo().template Size<LDIM>() != mGrdMaster.template Size<LDIM>() )
			THROW_INTERNAL( "some topo entities are not meshed !");

		for ( MGSize i=1; i <= mGeTMaster.cTopo().template Size<LDIM>(); ++i)
		{
			cout << "  Entity id = " << i << endl;

			const GET::TopoEntity<LDIM>	&tent = mGeTMaster.cTopo().template cTEnt<LDIM>( i);
			Grid<LDIM>					&grid = mGrdMaster.template rGrid<LDIM>( i);

			const GET::GeomEntity<LDIM,DIM>	*pgent = mGeTMaster.cGeom().template cGEnt<LDIM>( tent.cGeomId() );

			MGSize gid = 0;
			if ( pgent )
				gid = pgent->cId();
			else
				pgent = &nullgent;

			cout << "  topo id = " << tent.cId() << " geom id = " << gid << endl;

			if ( grid.SizeCellTab() > 0 )
			{
				WriteGrdTEC<LDIM> wtec(&grid);
				wtec.DoWrite( file, pgent );
			}

		}

		cout << "Writing TEC " << fname <<  " - FINISHED" << endl;
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: WriteGrdTEC::DoWrite - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}


template <Dimension DIM>
void Master<DIM>::ExportMSH2( const MGString& fname)
{
	try
	{
		cout << "\nWriting MSH2 " << fname << endl;
		cout.flush();

		if ( mGeTMaster.cTopo().template Size<DIM>() != mGrdMaster.template Size<DIM>() )
			THROW_INTERNAL( "some topo entities are not meshed !");

		for ( MGSize i=1; i <= mGeTMaster.cTopo().template Size<DIM>(); ++i)
		{
			cout << "  Entity id = " << i << endl;

			GridWBnd<DIM> &grid = mGrdMaster.template rGrid<DIM>( i);

			WriteGrdMSH2<DIM> wmsh2(&grid);
			wmsh2.DoWrite( fname );
		}

		cout << "Writing MSH2 " << fname <<  " - FINISHED" << endl;
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: WriteGrdMSH2::DoWrite - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}

//////////////////////////////////////////////////////////////////////
// N6 global ControlSpace - build one incrementally

template <Dimension DIM>
void Master<DIM>::GenerateLocal0D()
{
	for ( MGSize i=1; i <= mGeTMaster.cTopo().template Size<DIM_0D>(); ++i)
	{
		cout << "  Entity id = " << i << endl;

		const GET::TopoEntity<DIM_0D>& tent = mGeTMaster.cTopo().template cTEnt<DIM_0D>( i);
		const GET::GeomEntity<DIM_0D,DIM> *pgent = mGeTMaster.cGeom().template cGEnt<DIM_0D>( tent.cGeomId() );

		GVect	vct;
		pgent->GetCoord( vct);

		MGSize gid = mGrdMaster.template CreateGrid<DIM_0D>();

		if ( gid != i)
			THROW_INTERNAL( "incompatible ids inside Master<DIM>::GenerateLocal0D()")

		GridWBnd<DIM_0D>	&grid	= mGrdMaster.template rGrid<DIM_0D>( gid);
		Source<DIM_0D,DIM>	&src	= mCSpMaster.template rSource<DIM_0D>( gid);

		// set source pointers
		src.InitPointers( &grid, &(mGrdMaster.rMasterPosCx()), &(mGrdMaster.rMasterMetricCx()) );


		// updating master representation of node
		MGSize posid = mGrdMaster.rMasterPosCx().Insert( vct);

		// updating global metric of node
		GMetric met = src.cMetric();

		//////////////
		//for ( MGSize ie=1; ie <= mGeTMaster.cTopo().template Size<DIM_1D>(); ++ie)
		//{
		//	const GET::TopoEntity<DIM_1D>& tedge = mGeTMaster.cTopo().template cTEnt<DIM_1D>( ie);

		//	ASSERT( tedge.cTab().size() == 1);
		//	ASSERT( tedge.cTab()[0].first.size() == 2);

		//	MGSize idv1 = tedge.cTab()[0].first[0].first;
		//	MGSize idv2 = tedge.cTab()[0].first[1].first;

		//	ASSERT( idv1 > 0 && idv2 > 0 );

		//	if ( idv1 == gid || idv2 == gid)
		//	{
		//		const GET::GeomEntity<DIM_1D,DIM> *pgcrv = mGeTMaster.cGeom().cGEnt<DIM_1D>( tedge.cGeomId() );
		//		MGFloat ccoeff = 0.05;

		//		GeomCurvControlSpace<DIM_1D,DIM>	gcurvcspace( *pgcrv, ccoeff);

		//		GMetric	metric2 = gcurvcspace.GetGlobSpacing( pgcrv->FindParam( vct) );
		//		GMetric	metric;
		//		IntersectMetric( metric, metric2, met);

		//		//met = metric;
		//	}

		//}


		MGSize metid = mGrdMaster.rMasterMetricCx().Insert( met );

		if ( posid != metid)
			THROW_INTERNAL( "Master<DIM>::GenerateLocal0D()  : posid != metid !!!!!!");

		grid.rMasterId() = posid;
	}
}


template<>
template<>
void Master<DIM_3D>::OptimiseSurf<DIM_2D>( const GET::GeomEntity<DIM_2D,DIM_3D>* pgent, Grid<DIM_2D>* pgrid)	
{
	OptimiserSurf	opt( *pgent, *pgrid);
	opt.DoOptimise( 10);
}


template <Dimension DIM>
template <Dimension LDIM>
void Master<DIM>::GenerateLocal()
{
	if ( DIM == LDIM)
		THROW_INTERNAL( "Wrong GenerateLocal() called !!!");


	for ( MGSize i=1; i <= mGeTMaster.cTopo().template Size<LDIM>(); ++i)
	{
		cout << "  Entity id = " << i << endl;



		const GET::TopoEntity<LDIM>& tent = mGeTMaster.cTopo().template cTEnt<LDIM>( i);


		MGSize gid = mGrdMaster.template CreateGrid<LDIM>();

		if ( gid != i)
			THROW_INTERNAL( "incompatible ids inside Master<DIM>::Generate()");


		cout << "LDIM NoCSpace= " << LDIM << endl;


		// A3 def
		//MGFloat ccoeff = 0.15;
		//if ( (LDIM == DIM_1D && gid == 26) || (LDIM == DIM_1D && gid == 49) 
		//	|| (LDIM == DIM_2D && gid == 21) || (LDIM == DIM_2D && gid == 22) || (LDIM == DIM_2D && gid == 23) )
		//{
		//	bcurv = false;
		//}

		GridWBnd<LDIM>		&grid	= mGrdMaster.template rGrid<LDIM>( gid);
		BndGrid<LDIM>		&bgrd	= grid.rBndGrid();

		CreateBndGrid( bgrd, tent);

		const GET::GeomEntity<LDIM,DIM> *pgent = mGeTMaster.cGeom().template cGEnt<LDIM>( tent.cGeomId() );

		GridControlSpace<LDIM,DIM>	gridcspace( pgent);

		// Initialization of the GridCSontrolpace based on BndGrid
		gridcspace.Init( mGrdMaster.cMasterMetricCx(), bgrd);

		// Add internal sources
		// TODO ::

		// Update local metric inside the GridCSontrolpace (use geom rep)
		//gridcspace.UpdateLocMetric( *pgent);

		bool bcurv = ConfigSect().ValueBool( ConfigStr::Master::WITH_CURVATURE );
		MGFloat ccoeff = ConfigSect().ValueFloat( ConfigStr::Master::CURVCOEFF );


		vector<MGSize> tabid;
		MGString listname = MGString( ConfigStr::Master::CURVEXCEPT) + MGString("_") + DimToStr(LDIM);
		if ( ConfigSect().KeyExists( listname) )
		{
			tabid = ConfigSect().ValueVectorID( listname );
			sort( tabid.begin(), tabid.end() );
		}


		GeomControlSpace<LDIM,DIM>		gcspace( *pgent);
		CombineControlSpace<LDIM>		combcspace( gridcspace, gcspace);

		GeomCurvControlSpace<LDIM,DIM>	gcurvcspace( *pgent, ccoeff);
		BlendControlSpace<LDIM>			blendcspace( &gcurvcspace, &combcspace );

		ControlSpace<LDIM>* pcspace;

		if ( ! bcurv || binary_search( tabid.begin(), tabid.end(), gid) )
			pcspace = &combcspace;
		else
			pcspace = &blendcspace;
		

		MGString secname = MGString( ConfigStr::Master::Generator::NAME) + MGString("_") + DimToStr(LDIM);
		const CfgSection* cfgsec = &ConfigSect().GetSection( secname);

		Generator<LDIM>	gen( grid, pcspace );

		gen.Create( cfgsec);
		gen.PostCreateCheck();

		gen.Process();

		//OptimiseSurf( pgent, &grid);

			ofstream file( "_grid.dat", ios::out);
			WriteGrdTEC<LDIM> wtec(&grid);
			wtec.DoWrite( file, pgent );
			file.close();




		for ( typename Grid<LDIM>::ColNode::iterator itrn = grid.rColNode().begin(); itrn != grid.rColNode().end(); ++itrn)
		{
			// updating master representation of nodes
			GVect v;
			pgent->GetCoord( v, itrn->cPos() );

			// updating global metric of node
				
			// get metric from ControlSpace
			GMetric	metric;
			GMetric	metric1 = gridcspace.GetGlobSpacing( itrn->cPos() );
			GMetric	metric2 = gcurvcspace.GetGlobSpacing( itrn->cPos() );
			IntersectMetric( metric, metric1, metric2);

			//if ( (LDIM == DIM_2D && gid == 21) || (LDIM == DIM_1D && gid == 26) || (LDIM == DIM_1D && gid == 49) )
				metric = metric1;

			// change metric according to the current spacing
			// TODO ::

			if ( itrn->cMasterId() == 0 )
			{

				MGSize idpos = mGrdMaster.rMasterPosCx().Insert( v);
				MGSize idmet = mGrdMaster.rMasterMetricCx().Insert( metric );

				if ( idpos != idmet)
					THROW_INTERNAL( "Master<DIM>::GenerateLocal()  : idpos != idmet !!!!!!");

				itrn->rMasterId() = idpos;
			}
			//else
			//{
			//	GMetric	met;
			//	IntersectMetric( met, metric, mGrdMaster.rMasterMetricCx().cData( itrn->rMasterId() ) );
			//	mGrdMaster.rMasterMetricCx().rData( itrn->rMasterId() ) = met;
			//}
		}

	}
}



template <Dimension DIM>
void Master<DIM>::GenerateLocal()
{
	//UniformControlSpace<DIM> unicspace( 0.05);

	for ( MGSize i=1; i <= mGeTMaster.cTopo().template Size<DIM>(); ++i)
	{
		cout << "  Entity id = " << i << endl;

		const GET::TopoEntity<DIM>& tent = mGeTMaster.cTopo().template cTEnt<DIM>( i);

		ExportSMESH( "out.smesh", tent);
		ExportBMSH( "out.bmsh", tent);

		MGSize gid = mGrdMaster.template CreateGrid<DIM>();

		if ( gid != i)
			THROW_INTERNAL( "incompatible ids inside Master<DIM>::Generate()");


		cout << "LDIM = " << DIM << endl;

		GridWBnd<DIM>		&grid	= mGrdMaster.template rGrid<DIM>( gid);
		BndGrid<DIM>		&bgrd	= grid.rBndGrid();

		CreateBndGrid( bgrd, tent);

		GridControlSpace<DIM,DIM>	gridcspace;

		// Initialization of the GridCSontrolpace based on BndGrid
		gridcspace.Init( mGrdMaster.cMasterMetricCx(), bgrd);

		ostringstream str;
		str << "cspacegrid_" << DIM << "D";

		gridcspace.ExportFileMEANDR( str.str() );

		// Add internal sources
		// TODO ::

		// Update local metric inside the GridCSontrolpace (use geom rep)
		// gridcspace.UpdateLocMetric( NULL);


		MGString secname = MGString( ConfigStr::Master::Generator::NAME) + MGString("_") + DimToStr(DIM);
		const CfgSection* cfgsec = &ConfigSect().GetSection( secname);

		Generator<DIM>	gen( grid, &gridcspace );

		gen.Create( cfgsec);
		gen.PostCreateCheck();

		gen.Process();


		//Generator<DIM>	gen( grid, &unicspace );
		//gen.Process();
	}
}



//////////////////////////////////////////////////////////////////////
// Global ControlSpace exists


template <Dimension DIM>
void Master<DIM>::GenerateLocal0D( const ControlSpace<DIM> *pcsp)
{
	for ( MGSize i=1; i <= mGeTMaster.cTopo().template Size<DIM_0D>(); ++i)
	{
		cout << "  Entity id = " << i << endl;

		const GET::TopoEntity<DIM_0D>& tent = mGeTMaster.cTopo().template cTEnt<DIM_0D>( i);
		const GET::GeomEntity<DIM_0D,DIM> *pgent = mGeTMaster.cGeom().template cGEnt<DIM_0D>( tent.cGeomId() );

		GVect	vct;
		pgent->GetCoord( vct);

		MGSize gid = mGrdMaster.template CreateGrid<DIM_0D>();

		if ( gid != i)
			THROW_INTERNAL( "incompatible ids inside Master<DIM>::GenerateLocal0D()")

		GridWBnd<DIM_0D>	&grid	= mGrdMaster.template rGrid<DIM_0D>( gid);

		// updating master representation of node
		MGSize posid = mGrdMaster.rMasterPosCx().Insert( vct);

		//// updating global metric of node
		//GMetric met = GMetric();//src.cMetric();
		//MGSize metid = mGrdMaster.rMasterMetricCx().Insert( met );

		//if ( posid != metid)
		//	THROW_INTERNAL( "Master<DIM>::GenerateLocal0D()  : posid != metid !!!!!!");

		grid.rMasterId() = posid;
	}
}


template <Dimension DIM>
template <Dimension LDIM>
void Master<DIM>::GenerateLocal( const ControlSpace<DIM> *pcsp)
{
	if ( DIM == LDIM)
		THROW_INTERNAL( "Wrong GenerateLocal() called !!!");


	for ( MGSize i=1; i <= mGeTMaster.cTopo().template Size<LDIM>(); ++i)
	{
		cout << "  Entity id = " << i << endl;

		const GET::TopoEntity<LDIM>& tent = mGeTMaster.cTopo().template cTEnt<LDIM>( i);


		MGSize gid = mGrdMaster.template CreateGrid<LDIM>();

		if ( gid != i)
			THROW_INTERNAL( "incompatible ids inside Master<DIM>::Generate() -- gid = " << gid << " i = " << i);


		cout << "LDIM CSpace= " << LDIM << endl;

		//gearbox
		//if ( LDIM == DIM_2D && ( i == 185 || i == 221 || i == 223 || i == 314  || i == 347 || i == 444 || i == 628 || i == 632 || 
		//	i == 662 || i == 836 || i == 1117 || i == 1118 || i == 1199 || i == 1238 || i == 1243 || i== 1295 || i== 1303 || i== 1378 || i==1288 || i>1300   ) )
		//	continue;


		GridWBnd<LDIM>		&grid	= mGrdMaster.template rGrid<LDIM>( gid);
		BndGrid<LDIM>		&bgrd	= grid.rBndGrid();


		const GET::GeomEntity<LDIM,DIM> *pgent = mGeTMaster.cGeom().template cGEnt<LDIM>( tent.cGeomId() );

		//ostringstream ostr;
		//ostr << "gent_D" << LDIM << "_id" << gid << ".dat";
		//ofstream of( ostr.str().c_str() );
		//pgent->ExportTEC( of);
		//of.close();

		CreateBndGrid( bgrd, tent);


		// A3 def
		//bool bcurv = false;
		//MGFloat ccoeff = 0.15;

		bool bcurv = ConfigSect().ValueBool( ConfigStr::Master::WITH_CURVATURE );
		MGFloat ccoeff = ConfigSect().ValueFloat( ConfigStr::Master::CURVCOEFF );

		vector<MGSize> tabid;
		MGString listname = MGString( ConfigStr::Master::CURVEXCEPT) + MGString("_") + DimToStr(LDIM);
		if ( ConfigSect().KeyExists( listname) )
		{
			tabid = ConfigSect().ValueVectorID( listname );
			sort( tabid.begin(), tabid.end() );
		}


		SliceControlSpace<LDIM,DIM>	slicecspace( pcsp, pgent);

		GeomControlSpace<LDIM,DIM>	gcspace( *pgent);
		CombineControlSpace<LDIM>	combcspace( slicecspace, gcspace);

		GeomCurvControlSpace<LDIM,DIM>	gcurvcspace( *pgent, ccoeff);
		BlendControlSpace<LDIM>			blendcspace( &gcurvcspace, &combcspace );

		ControlSpace<LDIM>* pcspace;


		if ( ! bcurv || binary_search( tabid.begin(), tabid.end(), gid) )
			pcspace = &combcspace;
		else
			pcspace = &blendcspace;

		Generator<LDIM>	gen( grid, pcspace );

		MGString secname = MGString( ConfigStr::Master::Generator::NAME) + MGString("_") + DimToStr(LDIM);
		const CfgSection* cfgsec = &ConfigSect().GetSection( secname);

		gen.Create( cfgsec);
		gen.PostCreateCheck();

		gen.Process();

			ofstream file( "_grid.dat", ios::out);
			WriteGrdTEC<LDIM> wtec(&grid);
			wtec.DoWrite( file, pgent );
			file.close();


		//if ( i == 15 && LDIM == DIM_2D)
		//	THROW_INTERNAL("USER BREAK");


		for ( typename Grid<LDIM>::ColNode::iterator itrn = grid.rColNode().begin(); itrn != grid.rColNode().end(); ++itrn)
		{
			if ( itrn->cMasterId() == 0 )
			{
				// updating master representation of nodes
				GVect v;
				pgent->GetCoord( v, itrn->cPos() );
				MGSize idpos = mGrdMaster.rMasterPosCx().Insert( v);

				//// updating global metric of node
				//GMetric	metric;
				//// TODO :: get metric from ControlSpace
				//MGSize idmet = mGrdMaster.rMasterMetricCx().Insert( metric );
				//if ( idpos != idmet)
				//	THROW_INTERNAL( "Master<DIM>::GenerateLocal()  : idpos != idmet !!!!!!");

				itrn->rMasterId() = idpos;
			}
		}

	}
}



template <Dimension DIM>
void Master<DIM>::GenerateLocal( const ControlSpace<DIM> *pcsp)
{
	for ( MGSize i=1; i <= mGeTMaster.cTopo().template Size<DIM>(); ++i)
	{
		cout << "  Entity id = " << i << endl;

		const GET::TopoEntity<DIM>& tent = mGeTMaster.cTopo().template cTEnt<DIM>( i);

		ExportSMESH( "out.smesh", tent);
		ExportBMSH( "out.bmsh", tent);


		MGSize gid = mGrdMaster.template CreateGrid<DIM>();

		if ( gid != i)
			THROW_INTERNAL( "incompatible ids inside Master<DIM>::Generate()");


		cout << "LDIM = " << DIM << endl;

		GridWBnd<DIM>		&grid	= mGrdMaster.template rGrid<DIM>( gid);
		BndGrid<DIM>		&bgrd	= grid.rBndGrid();

		CreateBndGrid( bgrd, tent);


		MGString secname = MGString( ConfigStr::Master::Generator::NAME) + MGString("_") + DimToStr(DIM);
		const CfgSection* cfgsec = &ConfigSect().GetSection( secname);

		Generator<DIM>	gen( grid, pcsp );

		gen.Create( cfgsec);
		gen.PostCreateCheck();

		gen.Process();
	}
}



//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void Master<DIM>::Generate()
{
	THROW_INTERNAL( "Not implemented");
}


template <>
void Master<DIM_2D>::Generate()
{
	const ControlSpace<DIM_2D>* pcsp = mCSpMaster.GetGlobalCSpace();

	if ( pcsp)	// global space exists
	{
		cout << "\nGenerating grids for 0D entities\n";
		GenerateLocal0D( pcsp);

		cout << "\nGenerating grids for 1D entities\n";
		GenerateLocal<DIM_1D>( pcsp);
		ExportTEC<DIM_1D>( "grid_1d.dat");

		cout << "\nGenerating grids for 2D entities\n";
		GenerateLocal( pcsp);
		ExportTEC<DIM_2D>( "grid_2d.dat");
	}
	else // use spacing based on user defined sources
	{
		cout << "\nGenerating grids for 0D entities\n";
		GenerateLocal0D();

		cout << "\nGenerating grids for 1D entities\n";
		GenerateLocal<DIM_1D>();
		ExportTEC<DIM_1D>( "grid_1d.dat");

		cout << "\nGenerating grids for 2D entities\n";
		GenerateLocal();
		ExportTEC<DIM_2D>( "grid_2d.dat");

	}

	ExportMSH2( "grid.msh2");


	return;
}


template <>
void Master<DIM_3D>::Generate()
{
	const ControlSpace<DIM_3D>* pcsp = mCSpMaster.GetGlobalCSpace();

	if ( pcsp)	// global space exists
	{
		cout << "\nGenerating grids for 0D entities\n";
		GenerateLocal0D( pcsp);

		cout << "\nGenerating grids for 1D entities\n";
		GenerateLocal<DIM_1D>( pcsp);
		ExportTEC<DIM_1D>( "grid_1d.dat");

		cout << "\nGenerating grids for 2D entities\n";
		GenerateLocal<DIM_2D>( pcsp);
		ExportTEC<DIM_2D>( "grid_2d.dat");

		cout << "\nGenerating grids for 3D entities\n";
		GenerateLocal( pcsp);
		ExportTEC<DIM_3D>( "grid_3d.dat");

	}
	else // use spacing based on user defined sources
	{
		cout << "\nGenerating grids for 0D entities\n";
		GenerateLocal0D();

		cout << "\nGenerating grids for 1D entities\n";
		GenerateLocal<DIM_1D>();
		ExportTEC<DIM_1D>( "grid_1d.dat");

		cout << "\nGenerating grids for 2D entities\n";
		GenerateLocal<DIM_2D>();
		ExportTEC<DIM_2D>( "grid_2d.dat");

		cout << "\nGenerating grids for 3D entities\n";
		GenerateLocal();
		ExportTEC<DIM_3D>( "grid_3d.dat");
	}

	ExportMSH2( "grid.msh2");

	return;
}

//template <Dimension DIM>
//void Master<DIM>::Generate()
//{
//	//cout << "MGSize "  << sizeof( MGSize) << endl;
//	//cout << "bool "  << sizeof( bool) << endl;
//	//cout << "bitset<8> "  << sizeof( bitset<8>) << endl;
//	//cout << "bitset<9> "  << sizeof( bitset<9>) << endl;
//	//cout << "bitset<32> "  << sizeof( bitset<32>) << endl;
//	//cout << "bitset<33> "  << sizeof( bitset<33>) << endl;
//	//cout << "bitset<4> "  << sizeof( bitset<4>) << endl;
//
//	//cout << "\n 1D\n";
//	//cout << "GVect "  << sizeof( Grid<DIM_1D>::GVect) << endl;
//	//cout << "GNode "  << sizeof( Grid<DIM_1D>::GNode) << endl;
//	//cout << "GCell "  << sizeof( Grid<DIM_1D>::GCell) << endl;
//
//	//cout << "\n 2D\n";
//	//cout << "GVect "  << sizeof( Grid<DIM_2D>::GVect) << endl;
//	//cout << "GNode "  << sizeof( Grid<DIM_2D>::GNode) << endl;
//	//cout << "GCell "  << sizeof( Grid<DIM_2D>::GCell) << endl;
//
//	//cout << "\n 3D\n";
//	//cout << "GVect "  << sizeof( Grid<DIM_3D>::GVect) << endl;
//	//cout << "GNode "  << sizeof( Grid<DIM_3D>::GNode) << endl;
//	//cout << "GCell "  << sizeof( Grid<DIM_3D>::GCell) << endl;
//
//	//THROW_INTERNAL("");
//
//
//	cout << "\nGenerating grids for 0D entities\n";
//	GenerateLocal0D();
//
//	cout << "\nGenerating grids for 1D entities\n";
//	GenerateLocal<DIM_1D>();
//	ExportTEC<DIM_1D>( "grid_1d.dat");
//
//	cout << "\nGenerating grids for 2D entities\n";
//	GenerateLocal<DIM_2D>();
//	ExportTEC<DIM_2D>( "grid_2d.dat");
//
//	return;
//}





template <Dimension DIM>
void Master<DIM>::GenerateOnera()
{
	THROW_INTERNAL( "Not implemented");
}

template <Dimension DIM>
void Master<DIM>::GenerateLANN()
{
	THROW_INTERNAL( "Not implemented");
}

template <Dimension DIM>
void Master<DIM>::GenerateA3()
{
	THROW_INTERNAL( "Not implemented");
}


//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void Master<DIM>::CreateBndGrid( BndGrid<DIM_1D>& bgrd, const GET::TopoEntity<DIM_1D>& tent)
{
	//const TopoEntity<DIM_1D>& tent = mGeTMaster.cTopo().cTEnt<DIM_1D>( id);
	
	ASSERT( tent.cTab().size() == 1);
	ASSERT( tent.cTab()[0].first.size() == 2);

	MGSize idv1 = tent.cTab()[0].first[0].first;
	MGSize idv2 = tent.cTab()[0].first[1].first;

	ASSERT( idv1 > 0 && idv2 > 0 );

	const GET::TopoEntity<DIM_0D>& vrt1 = mGeTMaster.cTopo().template cTEnt<DIM_0D>( idv1);
	const GET::TopoEntity<DIM_0D>& vrt2 = mGeTMaster.cTopo().template cTEnt<DIM_0D>( idv2);

	const GET::GeomEntity<DIM_1D,DIM> *pcrv = mGeTMaster.cGeom().template cGEnt<DIM_1D>( tent.cGeomId() );


	const GridWBnd<DIM_0D> &grid1 = mGrdMaster.template cGrid<DIM_0D>( idv1);
	const GridWBnd<DIM_0D> &grid2 = mGrdMaster.template cGrid<DIM_0D>( idv2);

	GVect gv1 = mGrdMaster.cMasterPosCx().cData( grid1.cMasterId() );
	GVect gv2 = mGrdMaster.cMasterPosCx().cData( grid2.cMasterId() );



	TDefs<DIM_1D>::GBndNode	bnod;

	Vect1D	vp1 = pcrv->FindParam( gv1);
	Vect1D	vp2 = pcrv->FindParam( gv2);

	bool borient = tent.cTab()[0].second;

	if ( pcrv->IsPeriodic(0) )
	{
		if ( borient)
		{
			if ( vp2.cX() < vp1.cX() || vp2.cX() == vp1.cX() )
				vp2.rX() += pcrv->GetPeriod(0);
		}
		else
		{
			if ( vp2.cX() > vp1.cX() || vp2.cX() == vp1.cX() )
				vp1.rX() += pcrv->GetPeriod(0);
		}
	}



	bnod.rId() = 1;
	bnod.rMasterId() = grid1.cMasterId();
	bnod.rPos() = vp1;
	bgrd.rNode1() = bnod;


	bnod.rId() = 2;
	bnod.rMasterId() = grid2.cMasterId();
	bnod.rPos() = vp2;
	bgrd.rNode2() = bnod;

	//// tmp dump
	//pcrv->GetCoord( vtmp, bnod.rPos() );
	//bnod.rPos().Write();
	//gv2.Write();
	//vtmp.Write();
}


template <Dimension DIM>
void Master<DIM>::CreateBndGrid( BndGrid<DIM_2D>& bgrd, const GET::TopoEntity<DIM_2D>& tent)
{
	cout << "Creating boundary grid" << endl;

	GET::NullGeomEntity<DIM_2D,DIM> nullsrf;

	const GET::GeomEntity<DIM_2D,DIM> *psrf = &nullsrf;

	if ( tent.cGeomId() > 0)
		psrf = mGeTMaster.cGeom().template cGEnt<DIM_2D>( tent.cGeomId() );

	// finding number of boundary nodes
	MGSize ntot = 0;
	for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
	{
		const GET::TopoLoop	&loop = tent.cTab()[iloop].first;
		const bool &bloop = tent.cTab()[iloop].second;

		for ( MGSize iedg=0; iedg<loop.size(); ++iedg)
		{
			const MGSize &ide = loop[iedg].first;
			const GridWBnd<DIM_1D> &grid = mGrdMaster.template cGrid<DIM_1D>( ide);

			ntot += grid.cColNode().size_valid();
		}
	}


	MGSize	offset=0;
	MGSize	idnode = 0;
	MGSize	idcell = 0;

	ProgressBar	bar(40);
	bar.Init( ntot );
	bar.Start();

	bool bfirst = true;
	Vect2D uprev;
	Vect2D ucenter(0.,0.);;

	ofstream of( "_bndgrid.dat");

	for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
	{
		const GET::TopoLoop	&loop = tent.cTab()[iloop].first;
		const bool &bloop = tent.cTab()[iloop].second;

		for ( MGSize iedg=0; iedg<loop.size(); ++iedg)
		{
			const MGSize &ide = loop[iedg].first;
			const bool &be = loop[iedg].second;

			bool borient = (bloop && be) || ( !bloop && !be);

			const GET::TopoEntity<DIM_1D>& tedg = mGeTMaster.cTopo().template cTEnt<DIM_1D>( ide);
			const GET::GeomEntity<DIM_1D,DIM> *pgent = mGeTMaster.cGeom().template cGEnt<DIM_1D>( tedg.cGeomId() );

			const GridWBnd<DIM_1D> &grid = mGrdMaster.template cGrid<DIM_1D>( ide);



			//if ( grid.cColNode().size_valid() < 2)
			//	THROW_INTERNAL( "CreateBndGrid 2D : at least 2 points should be inside the boundary grid");

			//if ( grid.cNode(1).cMasterId() == 0 || grid.cNode(2).cMasterId() == 0 )
			//		THROW_INTERNAL( "CreateBndGrid 2D : master representation of boundary node not found !");

			//Vect2D w1 = psrf->FindParam( mGrdMaster.cMasterPosCx().cData( grid.cNode(1).cMasterId() ) );
			//Vect2D w2 = psrf->FindParam( mGrdMaster.cMasterPosCx().cData( grid.cNode(2).cMasterId() ) );

			vector< pair< MGFloat, Grid<DIM_1D>::ColNode::const_iterator > > tabgid;
			tabgid.reserve( grid.cColNode().size_valid() );

			for ( Grid<DIM_1D>::ColNode::const_iterator itrn = grid.cColNode().begin(); itrn != grid.cColNode().end(); ++itrn)
				tabgid.push_back( pair< MGFloat, Grid<DIM_1D>::ColNode::const_iterator >( itrn->cPos().cX(), itrn)  );

			if ( borient )
				sort( tabgid.begin(), tabgid.end(), less< pair< MGFloat, Grid<DIM_1D>::ColNode::const_iterator > >() );
			else
				sort( tabgid.begin(), tabgid.end(), greater< pair< MGFloat, Grid<DIM_1D>::ColNode::const_iterator > >() );


			map<MGSize, MGSize> mapgid;
			for ( MGSize igid=0; igid<tabgid.size(); ++igid)
				mapgid.insert( map<MGSize, MGSize>::value_type( tabgid[igid].second->cId(), igid+1) ); 


			//for ( Grid<DIM_1D>::ColNode::const_iterator itrn = grid.cColNode().begin(); itrn != grid.cColNode().end(); ++itrn, ++bar)
			for ( MGSize igid=0; igid<tabgid.size(); ++igid, ++bar)
			{
				Grid<DIM_1D>::ColNode::const_iterator itrn = tabgid[igid].second;

				BndGrid<DIM_2D>::GBndNode	bnod;

				GVect v;
				if ( itrn->cMasterId() > 0 )
				{
					v = mGrdMaster.cMasterPosCx().cData( itrn->cMasterId() );	// use global node positions
					bnod.rMasterId() = itrn->cMasterId();
				}
				else
				{
					THROW_INTERNAL( "CreateBndGrid 2D : master representation of boundary node not found !");
					//pgent->GetCoord( v, (*itrn).cPos() );
					//MGSize idpos = mGrdMaster.rAuxData().Insert( v);
					//bnod.rMasterId() = idpos;
				}

				//v.Write();
				Vect2D ucur = psrf->FindParam( v );
				vector<Vect2D> tabprop;

				tabprop.push_back( ucur);

				MGSize icur = 0;
				MGFloat dist = (tabprop[0] - uprev).module();

				if ( !bfirst)
				{
					if ( psrf->IsPeriodic(0) )
					{
						tabprop.push_back( ucur + Vect2D( psrf->GetPeriod(0), 0. ) );
						tabprop.push_back( ucur - Vect2D( psrf->GetPeriod(0), 0. ) );

						tabprop.push_back( ucur + Vect2D( 2.*psrf->GetPeriod(0), 0. ) );
						tabprop.push_back( ucur - Vect2D( 2.*psrf->GetPeriod(0), 0. ) );
					}

					if ( psrf->IsPeriodic(1) )
					{
						tabprop.push_back( ucur + Vect2D( 0., psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur - Vect2D( 0., psrf->GetPeriod(1) ) );

						tabprop.push_back( ucur + Vect2D( 0., 2.*psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur - Vect2D( 0., 2.*psrf->GetPeriod(1) ) );
					}

					if ( psrf->IsPeriodic(0) && psrf->IsPeriodic(1) )
					{
						tabprop.push_back( ucur + Vect2D( psrf->GetPeriod(0),      psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( psrf->GetPeriod(0),   2.*psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( psrf->GetPeriod(0), -    psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( psrf->GetPeriod(0), - 2.*psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( 2.*psrf->GetPeriod(0),      psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( 2.*psrf->GetPeriod(0),   2.*psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( 2.*psrf->GetPeriod(0), -    psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( 2.*psrf->GetPeriod(0), - 2.*psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( -psrf->GetPeriod(0),      psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( -psrf->GetPeriod(0),   2.*psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( -psrf->GetPeriod(0), -    psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( -psrf->GetPeriod(0), - 2.*psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( -2.*psrf->GetPeriod(0),      psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( -2.*psrf->GetPeriod(0),   2.*psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( -2.*psrf->GetPeriod(0), -    psrf->GetPeriod(1) ) );
						tabprop.push_back( ucur + Vect2D( -2.*psrf->GetPeriod(0), - 2.*psrf->GetPeriod(1) ) );
					}

					for ( MGSize i=1; i<tabprop.size(); ++i )
						if ( (tabprop[i] - uprev).module() < dist )
						{
							dist = (tabprop[i] - uprev).module();
							icur = i;
						}
				}

				bnod.rPos() = tabprop[icur];


				of << igid  << " " << itrn->cPos().cX() << " " << bnod.cPos().cX() << " " << bnod.cPos().cY() << endl;

				//bnod.rPos() = psrf->FindParam( v );

				

				//if ( psrf->IsPeriodic(0) || psrf->IsPeriodic(1) )

				//{
				//	Vect2D w1 = bnod.rPos() + Vect2D( 2.*M_PI,0.);
				//	Vect2D w2 = bnod.rPos() + Vect2D( -2.*M_PI,0.);

				//	GVect v1, v2, vv;
				//	psrf->GetCoord( v1, w1);
				//	psrf->GetCoord( v2, w2);
				//	psrf->GetCoord( vv, bnod.cPos());

				//	MGFloat e1 = (v1-v).module();
				//	MGFloat e2 = (v2-v).module();
				//	MGFloat e3 = (vv-v).module();
				//	MGFloat e =3;

				//}

				//cout << bnod.rMasterId() << endl;

				//bnod.cPos().Write();
				//cout << endl;
				//bnod.rGrdId() = tent.cId();	// TODO :: ??????????

				bnod.rId() = ++idnode;		// global node id

				bgrd.rColNode().push_back( bnod);
				uprev = bnod.cPos();
				bfirst = false;
				ucenter += bnod.cPos();
			}

			for ( Grid<DIM_1D>::ColCell::const_iterator itrc = grid.cColCell().begin(); itrc != grid.cColCell().end(); ++itrc)
			{
				BndGrid<DIM_2D>::GBndCell	bcell;

				bcell.rTopoId() = tedg.cId();
				bcell.rId() = ++idcell;		// global cell id

				if ( borient)
				{
					for ( MGSize k=0; k<BndGrid<DIM_2D>::GBndCell::SIZE; ++k)
						bcell.rNodeId(k) = mapgid[ (*itrc).cNodeId(k) ] + offset;
						//bcell.rNodeId(k) = (*itrc).cNodeId(k) + offset;
				}
				else
				{
					for ( MGSize k=0; k<BndGrid<DIM_2D>::GBndCell::SIZE; ++k)
						bcell.rNodeId(k) = mapgid[ (*itrc).cNodeId( BndGrid<DIM_2D>::GBndCell::SIZE-1 - k) ] + offset;
						//bcell.rNodeId(k) = (*itrc).cNodeId( BndGrid<DIM_2D>::GBndCell::SIZE-1 - k) + offset;
				}

				bgrd.rColCell().push_back( bcell );
			}
	
			offset += grid.SizeNodeTab();


		}
	}
	bar.Finish();

	ucenter /= static_cast<MGFloat>(bgrd.cColNode().size_valid());


	WriteBndTEC<DIM_2D>		bndwriter( &bgrd);
	bndwriter.DoWrite( "boundary_befcorr.plt");


	// U period correction
	if ( psrf->IsPeriodic(0) && ucenter.cY() > psrf->GetPeriod(0) )
		for ( BndGrid<DIM_2D>::ColBNode::iterator itrn = bgrd.rColNode().begin(); itrn != bgrd.rColNode().end(); ++itrn)
		{
			itrn->rPos().rX() -= psrf->GetPeriod(0);
		}


	if ( psrf->IsPeriodic(0) && ucenter.cY() < 0. )
		for ( BndGrid<DIM_2D>::ColBNode::iterator itrn = bgrd.rColNode().begin(); itrn != bgrd.rColNode().end(); ++itrn)
		{
			itrn->rPos().rX() += psrf->GetPeriod(0);
		}


	// V period correction
	if ( psrf->IsPeriodic(1) && ucenter.cY() > psrf->GetPeriod(1) )
		for ( BndGrid<DIM_2D>::ColBNode::iterator itrn = bgrd.rColNode().begin(); itrn != bgrd.rColNode().end(); ++itrn)
		{
			itrn->rPos().rY() -= psrf->GetPeriod(1);
		}


	if ( psrf->IsPeriodic(1) && ucenter.cY() < 0. )
		for ( BndGrid<DIM_2D>::ColBNode::iterator itrn = bgrd.rColNode().begin(); itrn != bgrd.rColNode().end(); ++itrn)
		{
			itrn->rPos().rY() += psrf->GetPeriod(1);
		}



	bndwriter.DoWrite( "boundary_befdupl.plt");

	//bgrd.RemoveDuplicatePoints();

	if ( psrf->IsPeriodic(0) || psrf->IsPeriodic(1) )
		bgrd.RemoveDuplicatePointsPosBased( 1.0e-8 );
	else
		bgrd.RemoveDuplicatePoints();

	bndwriter.DoWrite( "boundary.plt");

	//// correction for periodic surfaces
	//if ( psrf->IsPeriodic(0) || psrf->IsPeriodic(1) )
	//{
	//	vector<MGSize>	tabc;
	//	tabc.resize( bgrd.SizeCellTab()+1, 0);

	//	for ( MGSize ibc=1; ibc<=bgrd.SizeCellTab(); ++ibc)
	//	{
	//		tabc[ bgrd.cCell(ibc).cNodeId(0) ] = bgrd.cCell(ibc).cNodeId(1);

	//		//cout << ibc << " : ";
	//		//cout << bgrd.cCell(ibc).cNodeId(0) << " ";
	//		//cout << bgrd.cCell(ibc).cNodeId(1) << endl;
	//	}

	//	for ( MGSize i=1; i<tabc.size(); ++i)
	//		if ( tabc[i] == 0)
	//			THROW_INTERNAL( "periodic check: boundary edge orientation problem");

	//			
	//	BndGrid<DIM_2D>::GBndCell	bcell;

	//	Vect2D vct1 = bgrd.cNode(1).cPos();
	//	MGSize id1 = 1;
	//	MGSize id = id1;

	//	for ( MGSize in=2; in<=bgrd.SizeNodeTab(); ++in)
	//	{
	//		//cout << "[" << id;
	//		Vect2D vct = bgrd.cNode( id = tabc[id] ).cPos();
	//		Vect2D dvct = vct - vct1;

	//		//cout << "," << id << "] : " << vct.cX() << "," << vct.cY();
	//		//cout << " : " << dvct.cX() << "," << dvct.cY() << endl;

	//		if ( psrf->IsPeriodic(0) )
	//		{
	//			if ( dvct.cX() < -0.5*psrf->GetPeriod(0) )
	//				bgrd.rNode(id).rPos().rX() += psrf->GetPeriod(0);

	//			if ( dvct.cX() > 0.5*psrf->GetPeriod(0) )
	//				bgrd.rNode(id).rPos().rX() -= psrf->GetPeriod(0);
	//		}

	//		vct1 = bgrd.cNode(id).cPos();
	//	}

	//}

	bndwriter.DoWrite( "boundary_aft_periodic.plt");
}


template <Dimension DIM>
void Master<DIM>::CreateBndGrid( BndGrid<DIM_3D>& bgrd, const GET::TopoEntity<DIM_3D>& tent)
{
	THROW_INTERNAL( "should never be called !!!");
}

template <>
void Master<DIM_3D>::CreateBndGrid( BndGrid<DIM_3D>& bgrd, const GET::TopoEntity<DIM_3D>& tent)
{
	cout << "Creating boundary grid" << endl;

	GET::NullGeomEntity<DIM_3D,DIM_3D> nullsrf;

	const GET::GeomEntity<DIM_3D,DIM_3D> *psrf = &nullsrf;

	if ( tent.cGeomId() > 0)
		psrf = mGeTMaster.cGeom().cGEnt<DIM_3D>( tent.cGeomId() );
		//psrf = mGeTMaster.cGeom().template cGEnt<DIM_3D>( tent.cGeomId() );


	MGSize	offset=0;
	MGSize	idnode = 0;
	MGSize	idcell = 0;

	for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
	{
		const GET::TopoLoop	&loop = tent.cTab()[iloop].first;
		const bool &bloop = tent.cTab()[iloop].second;

		for ( MGSize iedg=0; iedg<loop.size(); ++iedg)
		{
			const MGSize &ide = loop[iedg].first;
			const bool &be = loop[iedg].second;

			bool borient = (bloop && be) || ( !bloop && !be);

			const GET::TopoEntity<DIM_2D>& tedg = mGeTMaster.cTopo().cTEnt<DIM_2D>( ide);
			const GET::GeomEntity<DIM_2D,DIM_3D> *pgent = mGeTMaster.cGeom().cGEnt<DIM_2D>( tedg.cGeomId() );

			const GridWBnd<DIM_2D> &grid = mGrdMaster.cGrid<DIM_2D>( ide);


			MGSize wid = 1;
			vector<MGSize>	tabid( grid.cColNode().size() );

			for ( Grid<DIM_2D>::ColNode::const_iterator itrn = grid.cColNode().begin(); itrn != grid.cColNode().end(); ++itrn)
			{
				tabid[ (*itrn).cId() ] = wid++;

				BndGrid<DIM_3D>::GBndNode	bnod;

				GVect v;
				if ( itrn->cMasterId() > 0 )
				{
					v = mGrdMaster.cMasterPosCx().cData( itrn->cMasterId() );	// use global node positions
					bnod.rMasterId() = itrn->cMasterId();
				}
				else
				{
					THROW_INTERNAL( "CreateBndGrid 3D : master representation of boundary node not found !");
					//pgent->GetCoord( v, (*itrn).cPos() );
					//MGSize idpos = mGrdMaster.rAuxData().Insert( v);
					//bnod.rMasterId() = idpos;
				}

				//v.Write();
				bnod.rPos() = psrf->FindParam( v );

				//bnod.cPos().Write();
				//cout << endl;
				//bnod.rGrdId() = tent.cId();	// TODO :: ??????????

				bnod.rId() = ++idnode;		// global node id

				bgrd.rColNode().push_back( bnod);
			}

			for ( Grid<DIM_2D>::ColCell::const_iterator itrc = grid.cColCell().begin(); itrc != grid.cColCell().end(); ++itrc)
			{
				BndGrid<DIM_3D>::GBndCell	bcell;

				bcell.rTopoId() = tedg.cId();
				bcell.rId() = ++idcell;		// global cell id

				if ( borient)
				{
					for ( MGSize k=0; k<BndGrid<DIM_3D>::GBndCell::SIZE; ++k)
						bcell.rNodeId(k) = tabid[ (*itrc).cNodeId(k) ] + offset;
				}
				else
				{
					for ( MGSize k=0; k<BndGrid<DIM_3D>::GBndCell::SIZE; ++k)
						bcell.rNodeId(k) = tabid[ (*itrc).cNodeId( BndGrid<DIM_3D>::GBndCell::SIZE-1 - k) ] + offset;
				}

				bgrd.rColCell().push_back( bcell );
			}
	
			offset += grid.SizeNodeTab();

		}
	}

	WriteBndTEC<DIM_3D>		bndwriter( &bgrd);
	bndwriter.DoWrite( "boundary_befdupl.plt");

	bgrd.RemoveDuplicatePoints();

	bndwriter.DoWrite( "boundary.plt");
}


//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void Master<DIM>::ExportSMESH( const MGString& fname, const GET::TopoEntity<DIM>& tent)
{
	THROW_INTERNAL( "should never be called !!!");
}


template <>
void Master<DIM_3D>::ExportSMESH( const MGString& fname, const GET::TopoEntity<DIM_3D>& tent)
{

	ofstream file( fname.c_str(), ios::out);

	try
	{
		if ( ! file)
			THROW_FILE( "can not open the file", fname);

		cout << "\nWriting SMESH " << fname << endl;
		cout.flush();


		// count number of faces
		MGSize nfac = 0;
		for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
		{
			nfac += tent.cTab()[iloop].first.size();
		}

		cout << "nfac = " << nfac << endl;

		file << nfac << " # faces" << endl;



		GET::NullGeomEntity<DIM_3D,DIM_3D> nullsrf;

		const GET::GeomEntity<DIM_3D,DIM_3D> *psrf = &nullsrf;

		if ( tent.cGeomId() > 0)
		{
			// TODO :: check for 3D there should be no geom entity
			psrf = mGeTMaster.cGeom().cGEnt<DIM_3D>( tent.cGeomId() );
		}



		for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
		{
			const GET::TopoLoop	&loop = tent.cTab()[iloop].first;
			const bool &bloop = tent.cTab()[iloop].second;

			for ( MGSize ifac=0; ifac<loop.size(); ++ifac)
			{
				const MGSize &idf = loop[ifac].first;
				const bool &bf = loop[ifac].second;

				bool borient = (bloop && bf) || ( !bloop && !bf);

				const GET::TopoEntity<DIM_2D>& tfac = mGeTMaster.cTopo().cTEnt<DIM_2D>( idf);
				const GET::GeomEntity<DIM_2D,DIM_3D> *pgent = mGeTMaster.cGeom().cGEnt<DIM_2D>( tfac.cGeomId() );

				file << "# face GeT id " << idf << endl;

				const GridWBnd<DIM_2D> &grid = mGrdMaster.cGrid<DIM_2D>( idf);


				if ( grid.cColCell().size_valid() == 0 )
					continue;

				// write nodes
				file << grid.cColNode().size_valid() << " # nodes" << endl;


				MGSize wid = 1;
				vector<MGSize>	tabid( grid.cColNode().size() );

				for ( Grid<DIM_2D>::ColNode::const_iterator itrn = grid.cColNode().begin(); itrn != grid.cColNode().end(); ++itrn)
				{
					tabid[ (*itrn).cId() ] = wid++;

					BndGrid<DIM_3D>::GBndNode	bnod;

					GVect v;
					if ( itrn->cMasterId() > 0 )
					{
						v = mGrdMaster.cMasterPosCx().cData( itrn->cMasterId() );	// use global node positions
						bnod.rMasterId() = itrn->cMasterId();
					}
					else
					{
						THROW_INTERNAL( "CreateBndGrid 3D : master representation of boundary node not found !");
					}

					GVect vloc = psrf->FindParam( v );

					file << setw(5) << wid-1 << " " 
						 << setw(19) << setprecision(18) << vloc.cX() << " " 
						 << setw(19) << setprecision(18) << vloc.cY() << " " 
						 << setw(19) << setprecision(18) << vloc.cZ() << endl;
				}

				// write cells
				file << grid.cColCell().size_valid() << " # cells" << endl;

				wid = 1;
				for ( Grid<DIM_2D>::ColCell::const_iterator itrc = grid.cColCell().begin(); itrc != grid.cColCell().end(); ++itrc)
				{

					file << setw(5) << wid++ << " " << BndGrid<DIM_3D>::GBndCell::SIZE;

					if ( borient)
					{
						for ( MGSize k=0; k<BndGrid<DIM_3D>::GBndCell::SIZE; ++k)
							file << " " << setw(8) << tabid[ (*itrc).cNodeId(k) ];
					}
					else
					{
						for ( MGSize k=0; k<BndGrid<DIM_3D>::GBndCell::SIZE; ++k)
							file << " " << setw(8) << tabid[ (*itrc).cNodeId( BndGrid<DIM_3D>::GBndCell::SIZE-1 - k) ];
					}

					file << endl;


				}


			}
		}


		cout << "Writing SMESH " << fname <<  " - FINISHED" << endl;
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: Master::ExportSMESH - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}

}



template <>
void Master<DIM_2D>::ExportSMESH( const MGString& fname, const GET::TopoEntity<DIM_2D>& tent)
{

	ofstream file( fname.c_str(), ios::out);

	try
	{
		if ( ! file)
			THROW_FILE( "can not open the file", fname);

		cout << "\nWriting SMESH " << fname << endl;
		cout.flush();


		// count number of faces
		MGSize nfac = 0;
		for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
		{
			nfac += tent.cTab()[iloop].first.size();
		}

		cout << "nfac = " << nfac << endl;

		file << nfac << " # faces" << endl;



		GET::NullGeomEntity<DIM_2D,DIM_2D> nullsrf;

		const GET::GeomEntity<DIM_2D,DIM_2D> *psrf = &nullsrf;

		if ( tent.cGeomId() > 0)
		{
			// TODO :: check for 2D there should be no geom entity
			psrf = mGeTMaster.cGeom().cGEnt<DIM_2D>( tent.cGeomId() );
		}



		for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
		{
			const GET::TopoLoop	&loop = tent.cTab()[iloop].first;
			const bool &bloop = tent.cTab()[iloop].second;

			for ( MGSize ifac=0; ifac<loop.size(); ++ifac)
			{
				const MGSize &idf = loop[ifac].first;
				const bool &bf = loop[ifac].second;

				bool borient = (bloop && bf) || ( !bloop && !bf);

				const GET::TopoEntity<DIM_1D>& tfac = mGeTMaster.cTopo().cTEnt<DIM_1D>( idf);
				const GET::GeomEntity<DIM_1D,DIM_2D> *pgent = mGeTMaster.cGeom().cGEnt<DIM_1D>( tfac.cGeomId() );

				file << "# face GeT id " << idf << endl;

				const GridWBnd<DIM_1D> &grid = mGrdMaster.cGrid<DIM_1D>( idf);

				// write nodes
				file << grid.cColNode().size_valid() << " # nodes" << endl;


				MGSize wid = 1;
				vector<MGSize>	tabid( grid.cColNode().size() );

				for ( Grid<DIM_1D>::ColNode::const_iterator itrn = (grid.cColNode().begin()); itrn != grid.cColNode().end(); ++itrn)
				{
					tabid[ (*itrn).cId() ] = wid++;

					BndGrid<DIM_2D>::GBndNode	bnod;

					GVect v;
					if ( itrn->cMasterId() > 0 )
					{
						v = mGrdMaster.cMasterPosCx().cData( itrn->cMasterId() );	// use global node positions
						bnod.rMasterId() = itrn->cMasterId();
					}
					else
					{
						THROW_INTERNAL( "CreateBndGrid 2D : master representation of boundary node not found !");
					}

					GVect vloc = psrf->FindParam( v );

					file << setw(5) << wid-1 << " " 
						 << setw(17) << setprecision(9) << vloc.cX() << " " 
						 << setw(17) << setprecision(9) << vloc.cY() << endl;
				}

				// write cells
				file << grid.cColCell().size_valid() << " # cells" << endl;

				wid = 1;
				for ( Grid<DIM_1D>::ColCell::const_iterator itrc = grid.cColCell().begin(); itrc != grid.cColCell().end(); ++itrc)
				{

					file << setw(5) << wid++ << " " << BndGrid<DIM_2D>::GBndCell::SIZE;

					if ( borient)
					{
						for ( MGSize k=0; k<BndGrid<DIM_2D>::GBndCell::SIZE; ++k)
							file << " " << setw(24) << setprecision(16) << tabid[ (*itrc).cNodeId(k) ];
					}
					else
					{
						for ( MGSize k=0; k<BndGrid<DIM_2D>::GBndCell::SIZE; ++k)
							file << " " << setw(24) << setprecision(16) << tabid[ (*itrc).cNodeId( BndGrid<DIM_2D>::GBndCell::SIZE-1 - k) ];
					}

					file << endl;


				}


			}
		}


		cout << "Writing SMESH " << fname <<  " - FINISHED" << endl;
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: Master::ExportSMESH - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}

}






template <Dimension DIM>
void Master<DIM>::ExportBMSH( const MGString& fname, const GET::TopoEntity<DIM>& tent)
{
	BMesh<DIM> bmsh( mGeTMaster, mGrdMaster );

	bmsh.Init( tent.cId() );
	bmsh.DoWrite( "out.bmsh");

	//THROW_INTERNAL( "should never be called !!!");
}


//template <>
//void Master<DIM_3D>::ExportBMSH( const MGString& fname, const GET::TopoEntity<DIM_3D>& tent)
//{
//
//	ofstream file( fname.c_str(), ios::out);
//
//	try
//	{
//		if ( ! file)
//			THROW_FILE( "can not open the file", fname);
//
//		cout << "\nWriting BMSH " << fname << endl;
//		cout.flush();
//
//		// header
//		file << "BMSH_VER 1.0" << endl;
//		file << "3D" << endl;
//		file << "something" << endl;
//
//
//		// count number of faces
//		MGSize nfac = 0;
//		for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
//		{
//			nfac += tent.cTab()[iloop].first.size();
//		}
//
//		cout << "nfac = " << nfac << endl;
//
//		file << nfac << " # faces" << endl;
//
//
//
//		GET::NullGeomEntity<DIM_3D,DIM_3D> nullsrf;
//
//		const GET::GeomEntity<DIM_3D,DIM_3D> *psrf = &nullsrf;
//
//		if ( tent.cGeomId() > 0)
//		{
//			// TODO :: check for 3D there should be no geom entity
//			psrf = mGeTMaster.cGeom().cGEnt<DIM_3D>( tent.cGeomId() );
//		}
//
//
//
//		for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
//		{
//			const GET::TopoLoop	&loop = tent.cTab()[iloop].first;
//			const bool &bloop = tent.cTab()[iloop].second;
//
//			for ( MGSize ifac=0; ifac<loop.size(); ++ifac)
//			{
//				const MGSize &idf = loop[ifac].first;
//				const bool &bf = loop[ifac].second;
//
//				bool borient = (bloop && bf) || ( !bloop && !bf);
//
//				const GET::TopoEntity<DIM_2D>& tfac = mGeTMaster.cTopo().cTEnt<DIM_2D>( idf);
//				const GET::GeomEntity<DIM_2D,DIM_3D> *pgent = mGeTMaster.cGeom().cGEnt<DIM_2D>( tfac.cGeomId() );
//
//				file << "# face GeT id " << idf << endl;
//
//				const GridWBnd<DIM_2D> &grid = mGrdMaster.cGrid<DIM_2D>( idf);
//
//				// write nodes
//				file << grid.cColNode().size_valid() << " # nodes" << endl;
//
//
//				MGSize wid = 1;
//				vector<MGSize>	tabid( grid.cColNode().size() );
//
//				for ( Grid<DIM_2D>::ColNode::const_iterator itrn = grid.cColNode().begin(); itrn != grid.cColNode().end(); ++itrn)
//				{
//					tabid[ (*itrn).cId() ] = wid++;
//
//					BndGrid<DIM_3D>::GBndNode	bnod;
//
//					GVect v;
//					if ( itrn->cMasterId() > 0 )
//					{
//						v = mGrdMaster.cMasterPosCx().cData( itrn->cMasterId() );	// use global node positions
//						bnod.rMasterId() = itrn->cMasterId();
//					}
//					else
//					{
//						THROW_INTERNAL( "CreateBndGrid 3D : master representation of boundary node not found !");
//					}
//
//					GVect vloc = psrf->FindParam( v );
//
//					file << setw(5) << wid-1 << " " 
//						 << setw(17) << setprecision(9) << vloc.cX() << " " 
//						 << setw(17) << setprecision(9) << vloc.cY() << " " 
//						 << setw(17) << setprecision(9) << vloc.cZ() << endl;
//				}
//
//				// write cells
//				file << grid.cColCell().size_valid() << " # cells" << endl;
//
//				wid = 1;
//				for ( Grid<DIM_2D>::ColCell::const_iterator itrc = grid.cColCell().begin(); itrc != grid.cColCell().end(); ++itrc)
//				{
//
//					file << setw(5) << wid++ << " " << BndGrid<DIM_3D>::GBndCell::SIZE;
//
//					if ( borient)
//					{
//						for ( MGSize k=0; k<BndGrid<DIM_3D>::GBndCell::SIZE; ++k)
//							file << " " << setw(8) << tabid[ (*itrc).cNodeId(k) ];
//					}
//					else
//					{
//						for ( MGSize k=0; k<BndGrid<DIM_3D>::GBndCell::SIZE; ++k)
//							file << " " << setw(8) << tabid[ (*itrc).cNodeId( BndGrid<DIM_3D>::GBndCell::SIZE-1 - k) ];
//					}
//
//					file << endl;
//
//
//				}
//
//
//			}
//		}
//
//
//		cout << "Writing BMSH " << fname <<  " - FINISHED" << endl;
//	}
//	catch ( EHandler::Except& e)
//	{
//		cout << "ERROR: Master::ExportBMSH - file name: " << fname << endl << endl;
//		TRACE_EXCEPTION( e);
//		TRACE_TO_STDERR( e);
//
//		THROW_INTERNAL("END");
//	}
//
//}



template <Dimension DIM>
void Master<DIM>::CreateSrcGrid( BndGrid<DIM_1D>& bgrd, const GET::TopoEntity<DIM_1D>& tent)
{
}

template <Dimension DIM>
void Master<DIM>::CreateSrcGrid( BndGrid<DIM_2D>& bgrd, const GET::TopoEntity<DIM_2D>& tent)
{
}

template <Dimension DIM>
void Master<DIM>::CreateSrcGrid( BndGrid<DIM_3D>& bgrd, const GET::TopoEntity<DIM_3D>& tent)
{
}


#include "master_onera.h"
#include "master_lann.h"
#include "master_a3.h"


template class Master<DIM_2D>;
template class Master<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

