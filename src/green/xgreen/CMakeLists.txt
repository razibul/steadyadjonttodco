LIST ( APPEND xgreen_files
	main.cpp
	manager.cpp
	manager.h
	master.cpp
	master_onera.h
	master_lann.h
	master_a3.h
	master.h
) 


INCLUDE_DIRECTORIES (
  ${FLOW2_SOURCE_DIR}/${NAME_CORE}
  ${FLOW2_SOURCE_DIR}/${NAME_AUX}
  ${FLOW2_SOURCE_DIR}/${NAME_GREEN}
)


LINK_DIRECTORIES (
  ${FLOW2_BINARY_DIR}/${NAME_CORE}/libcoresystem 
  ${FLOW2_BINARY_DIR}/${NAME_CORE}/libcorecommon 
  ${FLOW2_BINARY_DIR}/${NAME_CORE}/libcoreio
  ${FLOW2_BINARY_DIR}/${NAME_CORE}/libcoregeom
  ${FLOW2_BINARY_DIR}/${NAME_AUX}/libextsparse
  ${FLOW2_BINARY_DIR}/${NAME_AUX}/libextget
  ${FLOW2_BINARY_DIR}/${NAME_AUX}/libextnurbs
  ${FLOW2_BINARY_DIR}/${NAME_AUX}/libextstep
  ${FLOW2_BINARY_DIR}/${NAME_GREEN}/libgreen
)

ADD_EXECUTABLE ( xgreen ${xgreen_files} )


##############################################################
# !!!  the order of the libraries in the list is IMPORTANT !!!
TARGET_LINK_LIBRARIES ( xgreen 
	green
	extget
	extstep
	extnurbs
	extsparse 
	coreio 
	coregeom 
	coreconfig 
	corecommon 
	coresystem 
) 
