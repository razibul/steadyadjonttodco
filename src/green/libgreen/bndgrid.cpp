#include "bndgrid.h"
#include "libcoregeom/tree.h"
#include "libcorecommon/key.h"
#include "libgreen/gridgeom.h"
#include "libcoregeom/triangle3d.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




template <Dimension DIM> 
void BndGrid<DIM>::InitBox()
{
	FindMinMax( mBox.rVMin(), mBox.rVMax() );
	const MGFloat marg = 0.005 * ( mBox.cVMax() - mBox.cVMin() ).module();

	mBox.rVMax() += Vect<DIM>(marg);
	mBox.rVMin() -= Vect<DIM>(marg);
}


template <Dimension DIM> 
void BndGrid<DIM>::CreateCopy( BndGrid<DIM> &grid) const
{
	grid.mBox = mBox;
	grid.mtabNode = mtabNode;
	grid.mtabCell = mtabCell;
}



//template <> 
//void BndGrid<DIM_3D>::InitBox()
//{
//	FindMinMax( mBox.rVMin(), mBox.rVMax() );
//	const MGFloat marg = 0.005 * ( mBox.VMax() - mBox.VMin() ).module();
//
//	mBox.rVMax() += Vect3D(marg,marg,marg);
//	mBox.rVMin() -= Vect3D(marg,marg,marg);
//}


template <Dimension DIM> 
void BndGrid<DIM>::RemoveDuplicatePoints()
{
	// corner points should have the same MasterId

	cout << "removing duplicate nodes" << std::flush;

	MGSize	id;

	map<MGSize,MGSize>			mapnodeid;
	map<MGSize,MGSize>			mapmasterid;

	map<MGSize,MGSize>::iterator	itrmap;


	ColBNode	tabtmp;

	tabtmp.swap( mtabNode);

	mtabNode.reserve( tabtmp.size_valid() );
	mtabNode.push_back( tabtmp[1] );

	mapmasterid[ tabtmp[1].cMasterId() ] = 1;

	for ( MGSize in=2; in<tabtmp.size(); ++in)
	{
		//GVect	vct = tabtmp[in].cPos();
		//cout << in << " " << tabtmp[in].cMasterId();
		//cout << " [ " << vct.cX() << ", " << vct.cY() << " ]\n";

		if ( (itrmap = mapmasterid.find( tabtmp[in].cMasterId() )) != mapmasterid.end() )
		{
			// duplicate point found
			mapnodeid[ tabtmp[in].cId() ] = itrmap->second;

			//cout << "mapnodeid.size() - " << mapnodeid.size() << endl;
		}
		else
		{

			mtabNode.push_back( tabtmp[in] );

			id = mtabNode.size() - 1;
			mtabNode[id].rId() = id;
			tabtmp[in].rId() = id;

			mapmasterid[ tabtmp[in].cMasterId() ] = id;
		}
	}

	
	//cout << "number of duplicate nodes - " << mapnodeid.size() << endl;


	// map node indices for each cell to correct ones
	for ( typename ColBCell::iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		for ( MGSize ifac=0; ifac<GBndCell::SIZE; ++ifac)
		{
			if ( (itrmap = mapnodeid.find( (*itr).cNodeId( ifac) )) != mapnodeid.end() )
			{
				(*itr).rNodeId( ifac) = (*itrmap).second;
			}
			else
			{
				(*itr).rNodeId( ifac) = tabtmp[ (*itr).cNodeId( ifac) ].cId();
			}
		}
	}

	cout << " - FINISHED\n";
}


template <Dimension DIM> 
void BndGrid<DIM>::RemoveDuplicatePointsPosBased( const MGFloat& zero)
{
	cout << "removing duplicate nodes - pos based" << std::flush;

	//cout << endl;

	MGSize	id;

	Tree<MGSize,DIM>			tree;
	typename Tree<MGSize,DIM>::TPair	res;
	map<MGSize,MGSize>			mapnodeid;

	map<MGSize,MGSize>::iterator	itrmap;


	InitBox();	// init bounding box

	// initialize tree
	tree.InitBox( mBox);

	// insert first point
	tree.Insert( mtabNode[1].cPos(), 1);

	ColBNode	tabtmp;

	tabtmp.swap( mtabNode);

	mtabNode.reserve( tabtmp.size_valid() );
	mtabNode.push_back( tabtmp[1] );

	//cout << "mapnodeid.size() - " << mapnodeid.size() << endl;


	for ( MGSize in=2; in<tabtmp.size(); ++in)
	{
		GVect vv = tabtmp[in].cPos();

		if ( tree.GetClosestItem( res, tabtmp[in].cPos() ) )
		{
			GVect	vct = res.first;
			MGFloat dd = (vct - tabtmp[in].cPos() ).module();

			if ( (vct - tabtmp[in].cPos() ).module() < zero)
			{
				// duplicate point found
				mapnodeid[ tabtmp[in].cId() ] = res.second;

				//cout << "duplicate point coords [ " << vct.cX() << ", " << vct.cY() << " ]\n";
				//cout << "mapnodeid.size() - " << mapnodeid.size() << endl;
			}
			else
			{

				mtabNode.push_back( tabtmp[in] );

				id = mtabNode.size() - 1;
				mtabNode[id].rId() = id;
				tabtmp[in].rId() = id;
				tree.Insert( tabtmp[in].cPos(), id);
			}
		}
		else
		{
			THROW_INTERNAL( "tree corrupted !!!");
		}
	}

	
	//cout << "mapnodeid.size() - " << mapnodeid.size() << endl;
	//cout << "number of duplicate nodes - " << mapnodeid.size() << endl;


	// map node indices for each cell to correct ones
	for ( typename ColBCell::iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		for ( MGSize ifac=0; ifac<GBndCell::SIZE; ++ifac)
		{
			if ( (itrmap = mapnodeid.find( (*itr).cNodeId( ifac) )) != mapnodeid.end() )
			{
				(*itr).rNodeId( ifac) = (*itrmap).second;
			}
			else
			{
				(*itr).rNodeId( ifac) = tabtmp[ (*itr).cNodeId( ifac) ].cId();
			}
		}
	}

	cout << " - FINISHED\n";
}


template <Dimension DIM> 
void BndGrid<DIM>::FindEdges( vector< Key<2> >& tabedg) const
{
}

template <> 
void BndGrid<DIM_3D>::FindEdges( vector< Key<2> >& tabedg) const
{
	cout << "searching for boundary edges" << std::flush;
	fflush( stdin);

	typedef vector< Key<2> >	TKeyTab;
	TKeyTab tabetmp;

	tabetmp.reserve( 3*mtabCell.size() );

	for ( ColBCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		for ( MGSize ie=0; ie<GBndCell::SIZE; ++ie)
		{
			Key<2> key = (*itr).EdgeKey( ie);
			key.rFirst() = cNode( key.cFirst() ).cGrdId();
			key.rSecond() = cNode( key.cSecond() ).cGrdId();
			key.Sort();

			tabetmp.push_back( key);
		}
	}

	sort( tabetmp.begin(), tabetmp.end() );



	
	MGSize	ie;

	tabedg.reserve( tabetmp.size() /2);

	for ( MGSize ip=0; ip<tabetmp.size(); ip+=2)
	{
		Key<2> key = tabetmp[ip];

		// check that each edge exist twice
		// if not then surface is corrupted (e.g. it is not closed)
		for ( ie=ip; ie < tabetmp.size(); ++ie)
			if ( ! ( tabetmp[ie] == key) )
				break;	

		if ( (ie - ip) != 2)
			THROW_INTERNAL( "surface grid is corrupted - inside FindEdges() --- " << ie-ip );

		tabedg.push_back( GBndEdge( key) );

	}


	if ( (3*mtabCell.size_valid()) / 2 - tabedg.size() != 0 )
	{
		cout << endl;
		cout << "\ttabedge size = " << tabetmp.size() << endl;

		cout << "\tbcell size = %d" << mtabCell.size()-1 << endl;
		cout << "\tbedge size = %d" << tabedg.size()-1 << endl;

//		printf( "\n");
//		printf( "\ttabedge size = %d\n", tabetmp.size() );
//
//		printf( "\tbcell size = %d\n", mtabCell.size()-1 );
//		printf( "\tbedge size = %d\n", tabedg.size()-1 );

		THROW_INTERNAL( "surface grid is corrupted - inside FindEdges()");
	}

	cout << " - FINISHED\n";
}



template <Dimension DIM> 
void BndGrid<DIM>::FindFaces( vector< Key<DIM> >& tabfac) const
{
	tabfac.reserve( mtabCell.size_valid() );

	for ( typename ColBCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		Key<DIM> key;
		for ( MGSize i=0; i<DIM; ++i)
			key.rElem( i) = cNode( (*itr).cNodeId(i) ).cGrdId();
		key.Sort();

		tabfac.push_back( key);
	}
}

//template <> 
//void BndGrid<DIM_2D>::FindFaces( vector< Key<DIM_2D> >& tabfac) const
//{
//	tabfac.reserve( mtabCell.size_valid() );
//
//	for ( ColBCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
//	{
//		Key<2> key;
//		key.rFirst()	= cNode( (*itr).cNodeId(0) ).cGrdId();
//		key.rSecond()	= cNode( (*itr).cNodeId(1) ).cGrdId();
//		key.Sort();
//
//		tabfac.push_back( key);
//	}
//}
//
//template <> 
//void BndGrid<DIM_3D>::FindFaces( vector< Key<DIM_3D> >& tabfac) const
//{
//	tabfac.reserve( mtabCell.size_valid() );
//
//	for ( ColBCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
//	{
//		Key<3> key;
//		key.rFirst()	= cNode( (*itr).cNodeId(0) ).cGrdId();
//		key.rSecond()	= cNode( (*itr).cNodeId(1) ).cGrdId();
//		key.rThird()	= cNode( (*itr).cNodeId(2) ).cGrdId();
//		key.Sort();
//
//		tabfac.push_back( key);
//	}
//}



template <Dimension DIM> 
void BndGrid<DIM>::FindSpacing( vector<MGFloat>& tabspac) const
{
	//cout << "computing boundary nodes spacing" << std::flush;
	//
	//TRACE( "BndGrid<DIM>::FindSpacing");
	
	typename ColBNode::const_iterator	itrn;
	
	//MGSize	maxid = 0;
	//for ( itrn = mtabNode.begin(); itrn != mtabNode.end(); ++itrn)
	//{
	//	cout << itrn->cId() << endl;
	//	cout << itrn->cGrdId() << endl;
	//	cout << itrn->cMasterId() << endl;
	//	itrn->cPos().Write( cout);
	//}
		//if ( (*itrn).cGrdId() > maxid )
		//	maxid = (*itrn).cGrdId();
	
	// TRACE1( "maxid = %d", maxid);
			
	tabspac.clear();
	tabspac.resize( mtabNode.size(), 0.0);
	//tabspac.resize( mtabNode.size(), 1.0);
	
	vector<MGFloat>	tabwgh( mtabNode.size(), 0.0);
	
	for ( typename ColBCell::const_iterator itrc = mtabCell.begin(); itrc != mtabCell.end(); ++itrc)
	{
		MGSize nnn = SimplexFace<DIM>::ESIZE;
		for ( MGSize ie=0; ie<SimplexFace<DIM>::ESIZE; ++ie)
		{
			const MGSize in1 = cNode( itrc->cNodeId( SimplexFace<DIM>::cEdgeConn(ie,0) ) ).cId();
			const MGSize in2 = cNode( itrc->cNodeId( SimplexFace<DIM>::cEdgeConn(ie,1) ) ).cId();

			// TRACE2( "in1 = %d  in2 = %d", in1, in2);
			
			const GVect v1 = cNode( itrc->cNodeId( SimplexFace<DIM>::cEdgeConn(ie,0) ) ).cPos();
			const GVect v2 = cNode( itrc->cNodeId( SimplexFace<DIM>::cEdgeConn(ie,1) ) ).cPos();
			
			const MGFloat dist = (v2 - v1).module();
			
			//tabspac[in1] *= dist;
			//tabspac[in2] *= dist;
			tabspac[in1] += ::sqrt( ::sqrt( dist ));
			tabspac[in2] += ::sqrt( ::sqrt( dist ));
			//tabspac[in1] += dist;
			//tabspac[in2] += dist;
			tabwgh[in1] += 1.0;
			tabwgh[in2] += 1.0;
			
		}
	}
	
	for ( itrn = mtabNode.begin(); itrn != mtabNode.end(); ++itrn)
	{
		const MGSize id = itrn->cId();

		if ( ::fabs( tabwgh[id]) < ZERO )
			THROW_INTERNAL( "BndGrid<DIM>::FindSpacing :  ::fabs( tabwgh[i]) < ZERO");

		//tabspac[id] = ::pow( tabspac[id], 1.0/tabwgh[id] );

		tabspac[id] /= tabwgh[id];
		tabspac[id] *= tabspac[id];
		tabspac[id] *= tabspac[id];
		//tabspac[id] *= tabspac[id];
	}

	//for ( MGSize i=0; i<tabspac.size(); ++i)
	//{
	//	if ( ::fabs( tabwgh[i]) < ZERO )
	//		THROW_INTERNAL( "BndGrid<DIM>::FindSpacing :  ::fabs( tabwgh[i]) < ZERO");
	//		
	//	tabspac[i] /= tabwgh[i];
	//	//TRACE2( "tabspac[ %d ]  = %lg", i, tabspac[i] );
	//}

	//TRACE( "BndGrid<DIM>::FindSpacing - finished");
	//cout << " - FINISHED\n";
}


//template <> 
//void BndGrid<DIM_2D>::FindSpacing( vector<MGFloat>& tabspac) const
//{
//	cout << "computing boundary nodes spacing" << std::flush;
//	
//	TRACE( "BndGrid<DIM_2D>::FindSpacing");
//	
//	ColBNode::const_iterator	itrn;
//	ColBCell::const_iterator	itrc;
//	
//	//MGSize	maxid = 0;
//	//for ( itrn = mtabNode.begin(); itrn != mtabNode.end(); ++itrn)
//	//{
//	//	cout << itrn->cId() << endl;
//	//	cout << itrn->cGrdId() << endl;
//	//	cout << itrn->cMasterId() << endl;
//	//	itrn->cPos().Write( cout);
//	//}
//		//if ( (*itrn).cGrdId() > maxid )
//		//	maxid = (*itrn).cGrdId();
//	
//	// TRACE1( "maxid = %d", maxid);
//			
//	tabspac.clear();
//	tabspac.resize( mtabNode.size()+1, 0.0);
//	
//	vector<MGFloat>	tabwgh( mtabNode.size()+1, 0.0);
//	
//	for ( itrc = mtabCell.begin(); itrc != mtabCell.end(); ++itrc)
//	{
//		for ( MGSize ie=0; ie<GBndCell::SIZE; ++ie)
//		{
//			const MGSize in1 = cNode( (*itrc).cNodeId( GeomTools::Triangle3D::cFaceConn(ie,0) ) ).cGrdId();
//			const MGSize in2 = cNode( (*itrc).cNodeId( GeomTools::Triangle3D::cFaceConn(ie,1) ) ).cGrdId();
//
//			// TRACE2( "in1 = %d  in2 = %d", in1, in2);
//			
//			const GVect v1 = cNode( (*itrc).cNodeId( GeomTools::Triangle3D::cFaceConn(ie,0) ) ).cPos();
//			const GVect v2 = cNode( (*itrc).cNodeId( GeomTools::Triangle3D::cFaceConn(ie,1) ) ).cPos();
//			
//			const MGFloat dist = (v2 - v1).module();
//			
//			tabspac[in1] += dist;
//			tabspac[in2] += dist;
//			tabwgh[in1] += 1.0;
//			tabwgh[in2] += 1.0;
//			
//		}
//	}
//	
//	for ( MGSize i=0; i<tabspac.size(); ++i)
//	{
//		if ( ::fabs( tabwgh[i]) > ZERO )
//			tabspac[i] /= tabwgh[i];
//		//TRACE2( "tabspac[ %d ]  = %lg", i, tabspac[i] );
//	}
//
//	TRACE( "BndGrid<DIM_3D>::FindSpacing - finished");
//	cout << " - FINISHED\n";
//}

//template <> 
//void BndGrid<DIM_3D>::FindSpacing( vector<MGFloat>& tabspac) const
//{
//	cout << "computing boundary nodes spacing" << std::flush;
//	
//	TRACE( "BndGrid<DIM_3D>::FindSpacing");
//	
//	ColBNode::const_iterator	itrn;
//	ColBCell::const_iterator	itrc;
//	
//	MGSize	maxid = 0;
//	for ( itrn = mtabNode.begin(); itrn != mtabNode.end(); ++itrn)
//		if ( (*itrn).cGrdId() > maxid )
//			maxid = (*itrn).cGrdId();
//	
//	// TRACE1( "maxid = %d", maxid);
//			
//	tabspac.clear();
//	tabspac.resize( maxid+1, 0.0);
//	
//	vector<MGFloat>	tabwgh( maxid+1, 0.0);
//	
//	for ( itrc = mtabCell.begin(); itrc != mtabCell.end(); ++itrc)
//	{
//		for ( MGSize ie=0; ie<GBndCell::SIZE; ++ie)
//		{
//			const MGSize in1 = cNode( (*itrc).cNodeId( GeomTools::Triangle3D::cFaceConn(ie,0) ) ).cGrdId();
//			const MGSize in2 = cNode( (*itrc).cNodeId( GeomTools::Triangle3D::cFaceConn(ie,1) ) ).cGrdId();
//
//			// TRACE2( "in1 = %d  in2 = %d", in1, in2);
//			
//			const GVect v1 = cNode( (*itrc).cNodeId( GeomTools::Triangle3D::cFaceConn(ie,0) ) ).cPos();
//			const GVect v2 = cNode( (*itrc).cNodeId( GeomTools::Triangle3D::cFaceConn(ie,1) ) ).cPos();
//			
//			const MGFloat dist = (v2 - v1).module();
//			
//			tabspac[in1] += dist;
//			tabspac[in2] += dist;
//			tabwgh[in1] += 1.0;
//			tabwgh[in2] += 1.0;
//			
//		}
//	}
//	
//	for ( MGSize i=0; i<tabspac.size(); ++i)
//	{
//		if ( ::fabs( tabwgh[i]) > ZERO )
//			tabspac[i] /= tabwgh[i];
//		//TRACE2( "tabspac[ %d ]  = %lg", i, tabspac[i] );
//	}
//
//	TRACE( "BndGrid<DIM_3D>::FindSpacing - finished");
//	cout << " - FINISHED\n";
//}



template <Dimension DIM> 
void BndGrid<DIM>::FindSpacing( GridContext<GMetric>& cxmet)
{
	cout << "computing boundary node spacing" << std::flush;
	
	TRACE( "BndGrid<DIM>::FindSpacing");
	
	
	//for ( ColBNode::const_iterator itrn = mtabNode.begin(); itrn != mtabNode.end(); ++itrn)
	//{
	//	cout << endl;
	//	cout << itrn->cId() << endl;
	//	cout << itrn->cGrdId() << endl;
	//	cout << itrn->cMasterId() << endl;
	//	itrn->cPos().Write( cout);
	//	cout << endl;
	//}

	if ( cxmet.Size() != 0)
		THROW_INTERNAL( "GridContext is NOT empty !!!");

	vector<MGFloat>	tabspac;

	FindSpacing( tabspac);

	
	for ( typename ColBNode::iterator itrn = mtabNode.begin(); itrn != mtabNode.end(); ++itrn)
	{
		if ( itrn->cMasterId() != 0)
			THROW_INTERNAL( "BndGrid<DIM_2D>::FindSpacing : MasterId already initialized !");

		MGSize id = itrn->cId();

		GMetric met;
		met.InitIso( tabspac[id] );

		itrn->rMasterId() = cxmet.Insert( met);
	}

	TRACE( "BndGrid<DIM>::FindSpacing - finished");
	cout << " - FINISHED\n";
}

//template <> 
//void BndGrid<DIM_2D>::FindSpacing( GridContext<GMetric>& cxmet)
//{
//}
//
//
//
//
//template <> 
//void BndGrid<DIM_3D>::FindSpacing( GridContext<GMetric>& cxmet)
//{
//	cout << "computing boundary node spacing" << std::flush;
//	
//	TRACE( "BndGrid<DIM_3D>::FindSpacing");
//	
//	
//	//for ( ColBNode::const_iterator itrn = mtabNode.begin(); itrn != mtabNode.end(); ++itrn)
//	//{
//	//	cout << endl;
//	//	cout << itrn->cId() << endl;
//	//	cout << itrn->cGrdId() << endl;
//	//	cout << itrn->cMasterId() << endl;
//	//	itrn->cPos().Write( cout);
//	//	cout << endl;
//	//}
//
//	if ( cxmet.Size() != 0)
//		THROW_INTERNAL( "GridContext is NOT empty !!!");
//
//	vector<MGFloat>	tabspac;
//
//	FindSpacing( tabspac);
//	
//
//	for ( ColBNode::iterator itrn = mtabNode.begin(); itrn != mtabNode.end(); ++itrn)
//	{
//		if ( itrn->cMasterId() != 0)
//			THROW_INTERNAL( "BndGrid<DIM_3D>::FindSpacing : MasterId already initialized !");
//
//		MGSize id = itrn->cId();
//
//		MGFloat d = tabspac[id];
//		GMetric met;
//		met.InitIso( d);
//
//		itrn->rMasterId() = cxmet.Insert( met);
//	}
//
//
//	TRACE( "BndGrid<DIM_3D>::FindSpacing - finished");
//	cout << " - FINISHED\n";
//
//}






template <Dimension DIM> 
void BndGrid<DIM>::FindFrontPnts( vector<GVect>& tabspac) const
{
}

template <> 
void BndGrid<DIM_3D>::FindFrontPnts( vector<GVect>& tabspac) const
{
	cout << "computing boundary nodes spacing" << std::flush;
	
	TRACE( "BndGrid<DIM_3D>::FindSpacing");
	
	ColBNode::const_iterator	itrn;
	ColBCell::const_iterator	itrc;

	tabspac.clear();	

	

	Box<DIM_3D>	box = mBox;

	const MGFloat	expcoeff = 1.0;
	GVect	dv = box.cVMax() - box.cVMin();

	box.rVMax() += expcoeff*dv;
	box.rVMin() -= expcoeff*dv;

	box.Equalize();
	
	Tree<MGSize,DIM_3D>			tree;
	Tree<MGSize,DIM_3D>::TPair	res;

	tree.InitBox( box);

	for ( itrn = mtabNode.begin(); itrn != mtabNode.end(); ++itrn)
		tree.Insert( (*itrn).cPos(), 1);



	for ( itrc = mtabCell.begin(); itrc != mtabCell.end(); ++itrc)
	{
		const GBndCell& cell = (*itrc);
		MGFloat dist=0.0, distmax=0.0;

		for ( MGSize ie=0; ie<GBndCell::SIZE; ++ie)
		{
			const GVect v1 = cNode( (*itrc).cNodeId( Triangle3D::cFaceConn(ie,0) ) ).cPos();
			const GVect v2 = cNode( (*itrc).cNodeId( Triangle3D::cFaceConn(ie,1) ) ).cPos();
			
			const MGFloat d = (v2 - v1).module();
			
			dist += d;
			distmax = max( d, distmax);
		}
		
		dist /= (MGFloat)GBndCell::SIZE;
		
		SimplexFace<DIM_3D> tri = GridGeom<DIM_3D>::CreateSimplexFace( cell, *this);
		
		const GVect vn = tri.Vn();
		const GVect vc = tri.Center();
		
		const GVect vup = vc + vn.versor() * distmax;
		const GVect vdown = vc - vn.versor() * 2.0 * distmax; // inside domain


		if ( tree.GetClosestItem( res, vup ) )
		{
			Vect3D	vct = res.first;

			if ( (vct - vup).module() >=  dist)
			{
				tabspac.push_back( vup);
				tree.Insert( vup, 1);
			}
		}
		else
		{
			THROW_INTERNAL( "tree corrupted !!!");
		}
		

		if ( tree.GetClosestItem( res, vdown ) )
		{
			Vect3D	vct = res.first;

			if ( (vct - vdown).module() >= dist)
			{
				tabspac.push_back( vdown);
				tree.Insert( vdown, 1);
			}
		}
		else
		{
			THROW_INTERNAL( "tree corrupted !!!");
		}
	
	}

}



template <Dimension DIM> 
void BndGrid<DIM>::FindFaces( vector< Key<DIM> >& tabfac, const MGSize& topoid) const
{
//	tabfac.reserve( mtabCell.size_valid() );

	for ( typename ColBCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		if ( itr->cTopoId() == topoid)
		{
			Key<DIM> key;
			for ( MGSize i=0; i<DIM; ++i)
				key.rElem( i) = cNode( (*itr).cNodeId(i) ).cGrdId();
			//key.Sort();

			tabfac.push_back( key);
		}
	}
}


template class BndGrid<DIM_2D>;
template class BndGrid<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

