#include "kernel.h"
#include "libgreen/localizator.h"
#include "libgreen/gridgeom.h"
#include "libgreen/grid.h"

#include "libcoreio/store.h"
#include "libgreen/writegrdtec.h"

#include "libgreen/operator3d_flip44.h"
#include "libgreen/operator3d_flip32.h"
#include "libgreen/operator3d_flip23.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 





//template <>
bool Kernel<DIM_3D>::Flip23( vector<MGSize>& tabcell)
{	
	TRACE( "inside flip23");
	ASSERT( tabcell.size() == 2);

	Operator3D_Flip23 flip( *this);

	flip.Init( tabcell);
	flip.Select();
	if ( ! flip.Error() )
	{
		flip.DoFlip( tabcell);
		//mpGrd->CheckConnectivity();
		return true;
	}

	return false;
}





//template <>
bool Kernel<DIM_3D>::Flip32( vector<MGSize>& tabcell)
{
	TRACE( "inside flip32");
	ASSERT( tabcell.size() == 3);

	Operator3D_Flip32 flip( *this);

	flip.Init( tabcell);
	flip.Select();
	if ( ! flip.Error() )
	{
		flip.DoFlip( tabcell);
		//mpGrd->CheckConnectivity();
		return true;
	}

	return false;
}





//template <>
bool Kernel<DIM_3D>::Flip44( vector<MGSize>& tabcell, const Key<2>& edge)
{
	TRACE( "inside flip44");
	ASSERT( tabcell.size() == 4);

	Operator3D_Flip44 flip( *this);

	flip.Init( tabcell);
	flip.SelectForceEdge( edge);
	if ( ! flip.Error() )
	{
		flip.DoFlip( tabcell);
		//mpGrd->CheckConnectivity();
		return true;
	}

	return false;
}





//template <>
bool Kernel<DIM_3D>::CollapseEdge( const Key<2>& edge, const MGSize& inrm)
{
	//THROW_INTERNAL( "not working !!!!!!!!");
	
	mpGrd->CheckConnectivity();
//	ASSERT(0);



	MGSize	ina;
		
	if ( edge.cFirst() == inrm )
	{
		ina = edge.cSecond();
	}
	else
	if ( edge.cSecond() == inrm )
	{
		ina = edge.cFirst();
	}
	else
		THROW_INTERNAL( "ColapseEdge - node inrm is not one of the edge nodes");



	vector<MGSize>	tabshell;
	
	FindEdgeShell( tabshell, edge);
	
 	WriteGrdTEC<DIM_3D>	write( mpGrd);

	write.WriteCells( "_shell_.plt", tabshell);
	
	
	if ( tabshell.size() == 0 )
	{
		TRACE( "ColapseEdge - edge is not existing in the grid");
		return false;
	}
	
	bool	bFound, bFa, bFr;
	
	for ( vector<MGSize>::iterator itr = tabshell.begin(); itr != tabshell.end(); ++itr)
	{
		MGSize	icna=0, icnr=0;
		GCell&	cell = mpGrd->rCell( *itr);
		
		bFa = bFr = false;
		for ( MGSize i=0; i<GCell::SIZE; ++i)
		{
			if ( cell.cNodeId(i) == ina )
			{
				bFa = true;
				icna = i;
			}	

			if ( cell.cNodeId(i) == inrm )
			{
				bFr = true;
				icnr = i;
			}
		}
		
		ASSERT( bFa && bFr);
		


		// change node id in neibourghing cell
		GCell&	cella = mpGrd->rCell( cell.cCellId( icna).first );
		GCell&	cellr = mpGrd->rCell( cell.cCellId( icnr).first );
		
		//cellr.Dump( *mpGrd);
		//cella.Dump( *mpGrd);
		
		bFound = false;
		for ( MGSize i=0; i<GCell::SIZE; ++i)
		{
			if ( cella.cNodeId( i) == inrm )
			{
				cella.rNodeId( i) = ina;
				bFound = true;
				break;
			}
		}
// 		if ( ! bFound)
// 		{
// 			cell.Dump( *mpGrd);
// 			cellr.Dump( *mpGrd);
// 			cella.Dump( *mpGrd);
// 			cout << "++++++++++++++++++++++++++++++\n";
// 		}
		
		

		TRACE( "ina = " << ina << "; inr = " << inrm);
		TRACE( "icna = " << icna << "; icnr = " << icnr);
					
		// update connectivity
		cella.rCellId( cell.cCellId( icna).second ) = cell.cCellId( icnr);
		cellr.rCellId( cell.cCellId( icnr).second ) = cell.cCellId( icna);

// 		if ( ! bFound)
// 		{
// 			cell.Dump( *mpGrd);
// 			cellr.Dump( *mpGrd);
// 			cella.Dump( *mpGrd);
// 		}
// 		ASSERT( bFound);

// 		cellr.Dump( *mpGrd);
// 		cella.Dump( *mpGrd);

	}
	
	ASSERT(0);

	// removing old cells from the tree
	// ereasing old cells
	GVect	vc;
	for ( MGSize ic=0; ic<tabshell.size(); ++ic)
		if ( mpGrd->cCell(tabshell[ic]).IsTree() )
		{
			vc = GridGeom<DIM_3D>::CreateSimplex( tabshell[ic], *mpGrd).Center();
			mLoc.TreeErase( vc, tabshell[ic]);
			mpGrd->EraseCell( tabshell[ic]);
		}

	mpGrd->EraseNode( inrm);


	mpGrd->CheckConnectivity();
	ASSERT(0);

	TRACE( "edge collapse - successful");
	
	return true;
}


//template <>
MGSize Kernel<DIM_3D>::InsertFacePoint( vector<MGSize>& tabcell, const GVect& vct)
{
	vector<GFace>	tabface;

	// TODO :: check if vct is inside tabccell; check if tabccell is single-connected

	/////////////////////////////////////////////////////////////
	// paint cells
	for ( vector<MGSize>::iterator itrc=tabcell.begin(); itrc!=tabcell.end(); ++itrc)
		mpGrd->rCell( *itrc).Paint();


	// TODO :: that part of code is the sam as in the FindCavity
	bool	bOk;
	GFace	face;
	MGSize	tabfnei[GCell::SIZE];

	static MGSize	tabfacconn[4][4] = { {0,0,2,1}, {0,0,1,2}, {0,2,0,1}, {0,1,2,0} };

	/////////////////////////////////////////////////////////////
	// creating cavity boundary faces 
	for ( vector<MGSize>::iterator itrc=tabcell.begin(); itrc!=tabcell.end(); ++itrc)
	{
		GCell	&cell = mpGrd->rCell( *itrc);

		if ( ! cell.IsPainted() )
			continue;
			

		::memset( tabfnei, 0, GCell::SIZE*sizeof(MGSize) );

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize inei = cell.cCellId( ifc).first;


			bOk = false;

			if ( ! inei)
				bOk = true;
			else if ( ! mpGrd->cCell( inei).IsPainted() )
				bOk = true;

			if ( bOk )
			{
				cell.GetFaceVNIn( face, ifc);
				tabface.push_back( face);
				tabfnei[ifc] = tabface.size();		// <--- !!!!!!  ind + 1
			}

		}

		/////////////////////////////////////////////////////////////
		// set connectivity for the cell faces
		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			if ( tabfnei[ifc] )
			{
				GFace &face = tabface[tabfnei[ifc]-1];

				for ( MGSize ifnei=0; ifnei<GCell::SIZE; ++ifnei)
				{
					if ( ifnei != ifc && tabfnei[ifnei] != 0)
					{
						face.rCellId( tabfacconn[ifc][ifnei]).first = tabfnei[ifnei];
						face.rCellId( tabfacconn[ifc][ifnei]).second = tabfacconn[ifnei][ifc];
					}
				}
			}
		}
	}


	return InsertPoint( tabcell, tabface, vct);
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

