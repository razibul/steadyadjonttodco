#include "optimiser_sliver.h"

#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/bndgrid.h"
#include "libgreen/gridgeom.h"
#include "libgreen/writegrdtec.h"

#include "libgreen/operator3d_pointedge.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


void OptimiserSliver::DoOptimise()
{
	const MGFloat cAlpha = 165.0;

	cout << endl;
	cout << "OptimiserSliver::DoOptimise()" << endl;

	Grid<DIM_3D>&	grid = mpKernel->rGrid();

	// find boundary faces
	vector< Key<3> >	tabbface;

	for ( Grid<DIM_3D>::ColCell::const_iterator citr = grid.cColCell().begin(); citr != grid.cColCell().end(); ++citr)
	{
		for ( MGSize k=0; k<GCell::SIZE; ++k)
			if ( citr->cCellId(k).first == 0 )
			{
				Key<3> key = (*citr).FaceKey( k);
				key.Sort();
				tabbface.push_back( key);
			}
	}

	WriteGrdTEC<DIM_3D>	wtec( &grid);
	wtec.WriteFaces( "_optsliv_bface.dat", tabbface);

	sort( tabbface.begin(), tabbface.end() );

	// find multi-boundary-faces cells

	vector<MGSize>	tabcell2;
	vector<MGSize>	tabcell3;

	for ( Grid<DIM_3D>::ColCell::const_iterator citr = grid.cColCell().begin(); citr != grid.cColCell().end(); ++citr)
	{
		const GCell &cell = *citr;

		vector<MGSize> tabf;

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			Key<3> key = (*citr).FaceKey( k);
			key.Sort();

			if ( binary_search( tabbface.begin(), tabbface.end(), key ) )
				tabf.push_back(k);
		}

		if ( tabf.size() == 2)
		{
			Simplex<DIM_3D> simp = GridGeom<DIM_3D>::CreateSimplex( cell, grid);

			MGFloat d = -  (simp.GetFace( tabf[0] ).Vn().versor() *	simp.GetFace( tabf[1] ).Vn().versor() );
			MGFloat fi = ::atan2( ::sqrt( 1.0 - d*d), d );

			if ( fi > cAlpha * M_PI / 180.0 ) 
				tabcell2.push_back( citr->cId() );
		}
		else if ( tabf.size() == 3 )
			tabcell3.push_back( citr->cId() );
		else if ( tabf.size() == 4 )
			THROW_INTERNAL( "'count == 4' case found");
	}

	wtec.WriteCells( "_optsliv_cells2.dat", tabcell2);
	wtec.WriteCells( "_optsliv_cells3.dat", tabcell3);

	
	// find edges to split
	vector< Key<2> > tabedges;

	for ( MGSize i=0; i<tabcell2.size(); ++i)
	{
		const GCell& cell = grid.cCell( tabcell2[i] );

		vector<MGSize> tabf;

		for ( MGSize k=0; k<GCell::SIZE; ++k)
			if ( cell.cCellId(k).first == 0 )
				tabf.push_back(k);

		if ( tabf.size() != 2 )
			THROW_INTERNAL( "tabf.size() != 2");

		vector< Key<2> > tabe;
		for ( MGSize k=0; k<tabf.size(); ++k)
		{
			GFace face;
			cell.GetFaceVNOut( face, tabf[k] );

			for ( MGSize ie=0; ie<GFace::ESIZE; ++ie)
			{
				Key<2> key = Key<2>( face.cNodeId( SimplexFace<DIM_3D>::cEdgeConn(ie,0)), face.cNodeId( SimplexFace<DIM_3D>::cEdgeConn(ie,1)) );
				key.Sort();
				tabe.push_back( key);
			}
		}

		sort( tabe.begin(), tabe.end() );

		bool bfound = false;
		Key<2> edgekey;
		for ( MGSize ie=0; ie<GCell::ESIZE; ++ie)
		{
			Key<2> key = cell.EdgeKey( ie);
			key.Sort();

			if ( ! binary_search( tabe.begin(), tabe.end(), key ) )
			{
				bfound = true;
				edgekey = key;
			}
		}

		if ( ! bfound)
			THROW_INTERNAL( "edge not found");

		tabedges.push_back( edgekey);
	}

	wtec.WriteEdges( "_optsliv_edges2.dat", tabedges);


	for ( MGSize i=0; i<tabedges.size(); ++i)
	{
		vector<MGSize>	tabcell;
		mpKernel->FindEdgeShell( tabcell, tabedges[i] );

		GVect vct = 0.5 * ( grid.cNode( tabedges[i].cFirst() ).cPos() + grid.cNode( tabedges[i].cSecond() ).cPos() );

		Operator3D_PointEdge operPE( *mpKernel);

		operPE.Init( tabcell, vct);
		if ( ! operPE.Error() )
			operPE.Execute( tabcell);

	}
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

