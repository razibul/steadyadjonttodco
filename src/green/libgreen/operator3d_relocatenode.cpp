#include "operator3d_relocatenode.h"

#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/gridgeom.h"

#include "libcorecommon/progressbar.h"
#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



void Operator3D_RelocateNode::Reset()
{
	mErrorCode = 0;
	mQBefore = mQAfter = 0.0;
	mVolMinBefore = mVolMinAfter = 0.0;

	mIdNode = 0;

	mtabBall.clear();
}



void Operator3D_RelocateNode::Init( const MGSize& idnod, const GVect& pos)
{
	mIdNode = idnod;
	mPos = pos;

	// find ball for inod
	MGSize icell = mKernel.cGrid().cNode( mIdNode ).cCellId();

	if ( icell == 0)
		THROW_INTERNAL( "Operator3D_RelocateNode::Init :: icell==0 for inod = " << mIdNode );

	mKernel.FindBall( mtabBall, mIdNode, icell );

	// is inside
	MGSize incell = 0;
	for ( MGSize i=0; i<mtabBall.size(); ++i)
	{
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( mtabBall[i], mKernel.cGrid());
		if ( tet.IsInside( mPos) )
		{
			incell = mtabBall[i];
			break;
		}
	}

	if ( incell == 0)
	{
		mErrorCode = 1;
		return;
	}

	// find min volume & q
	for ( MGSize i=0; i<mtabBall.size(); ++i)
	{
		const GCell &cell = mKernel.cGrid().cCell( mtabBall[i] );
		Simplex<DIM_3D> tetb = GridGeom<DIM_3D>::CreateSimplex( mtabBall[i], mKernel.cGrid());

		vector<GVect> tabpnt(4);
		for ( MGSize in=0; in<GCell::SIZE; ++in)
		{
			if ( cell.cNodeId(in) != mIdNode )
				tabpnt[in] = mKernel.cGrid().cNode( cell.cNodeId(in) ).cPos();
			else
				tabpnt[in] = mPos;
		}

		Simplex<DIM_3D> teta( tabpnt);

		if ( i==0 )
		{
			mVolMinBefore = tetb.Volume();
			mVolMinAfter  = teta.Volume();
			mQBefore = tetb.QMeasureBETAmax();
			mQAfter  = teta.QMeasureBETAmax();
		}
		else
		{
			MGFloat volb = tetb.Volume();
			MGFloat vola = teta.Volume();
			MGFloat qb  = tetb.QMeasureBETAmax();
			MGFloat qa  = teta.QMeasureBETAmax();

			if ( volb < mVolMinBefore )
				mVolMinBefore = volb;

			if ( vola < mVolMinAfter )
				mVolMinAfter = vola;

			if ( qb < mQBefore )
				mQBefore = qb;

			if ( qa < mQAfter )
				mQAfter = qa;
		}
	}

	if ( mVolMinAfter <= 0.0 )
		mErrorCode = 2;
}


void Operator3D_RelocateNode::Execute()
{
	if ( mErrorCode != 0)
		THROW_INTERNAL( "Operator3D_RelocateNode::Execute :: ErrorCode = " << mErrorCode );

	for ( MGSize i=0; i<mtabBall.size(); ++i)
		mKernel.RemoveCellFromTree( mtabBall[i] );

	mKernel.rGrid().rNode( mIdNode).rPos() = mPos;

	for ( MGSize i=0; i<mtabBall.size(); ++i)
		mKernel.InsertCellIntoTree( mtabBall[i]);
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

