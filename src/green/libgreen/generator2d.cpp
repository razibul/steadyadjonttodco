#include "generator.h"
#include "libgreen/configconst.h"
#include "libcoregeom/tree.h"
#include "libcoreio/store.h"
#include "libgreen/writegrdtec.h"
#include "libgreen/gridwbnd.h"
#include "libgreen/bndgrid.h"
#include "libgreen/gridgeom.h"
#include "libcorecommon/progressbar.h"
#include "libcorecommon/triple.h"

#include "libgreen/kernel.h"
#include "libgreen/reconstructor.h"
#include "libgreen/operator2d_flip22.h"

#include "libgreen/controlspace.h"
#include "libgreen/cspaceslice.h"

#include "libextget/getcurve.h"
#include "libgreen/cspacegeom.h"

#include "libgreen/metricinterpol.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




template<>
void Generator<DIM_2D>::FlagExternalCells( BndGrid<DIM_2D>& bnd)
{
	// create boundary faces search struct
	vector< Key<2> >	tabbndfac;

	tabbndfac.reserve( bnd.SizeCellTab() );

	BndGrid<DIM_2D>::ColBCell::const_iterator	citr;

	for ( citr=bnd.cColCell().begin(); citr!=bnd.cColCell().end(); ++citr)
	{
		const BndGrid<DIM_2D>::GBndCell	&bcell = *citr;
		Key<2>	key;
		key.rFirst() = bnd.cNode( bcell.cNodeId( 0) ).cGrdId();
		key.rSecond() = bnd.cNode( bcell.cNodeId( 1) ).cGrdId();

		// check
		Vect2D vg1 = mpGrd->cNode( bnd.cNode( bcell.cNodeId( 0) ).cGrdId() ).cPos();
		Vect2D vg2 = mpGrd->cNode( bnd.cNode( bcell.cNodeId( 1) ).cGrdId() ).cPos();
		Vect2D vb1 = bnd.cNode( bcell.cNodeId( 0) ).cPos();
		Vect2D vb2 = bnd.cNode( bcell.cNodeId( 1) ).cPos();

		if ( (vg1 - vb1).module() > ZERO || (vg2 - vb2).module() > ZERO )
			cout << "Problem with grid id for boundary nodes\n";

		ASSERT( key.cFirst() > 0 && key.cSecond() > 0 );

		key.Sort();

		tabbndfac.push_back( key);
	}

	sort( tabbndfac.begin(), tabbndfac.end() );

	//// remove duplicate faces from the boundary grid
	//vector< Key<2> >::iterator itre = unique( tabbndfac.begin(), tabbndfac.end() );
	//tabbndfac.erase( itre, tabbndfac.end() );


	// remove duplicate faces from the boundary grid
	vector< Key<2> > tabwodup;
	tabwodup.reserve( tabbndfac.size() );

	Key<2> key = tabbndfac[0];
	MGSize count = 1;
	for ( MGSize id=1; id<tabbndfac.size(); ++id)
	{
		if ( key == tabbndfac[id] )
			++count;
		else
		{
			if ( count % 2 > 0)
				tabwodup.push_back( key);

			count = 1;
			key = tabbndfac[id];
		}
	}

	if ( count % 2 > 0)
		tabwodup.push_back( key);

	//cout << tabbndfac.size() << "  " << tabwodup.size() << endl;

	tabbndfac.swap( tabwodup);
	tabwodup.clear();

//FILE *ff = fopen( "_dump.txt", "wt");
//for ( vector< Key<2> >::iterator itrr=tabbndfac.begin(); itrr!=tabbndfac.end(); ++itrr)
//{
//	Key<2> key = *itrr;
//	key.Sort();
//	Vect2D vb1 = mpGrd->cNode( key.cFirst() ).cPos();
//	Vect2D vb2 = mpGrd->cNode( key.cSecond() ).cPos();
//	fprintf( ff, "key %8d %8d : %8.0lf %8.0lf : %8.0lf %8.0lf\n", key.cFirst(), key.cSecond(),
//		vb1.cX(), vb1.cY(), vb2.cX(), vb2.cY());
//}
//fclose(ff);


	//////////////// check the boundary - every node should be touched by edges even times (only 2D)
	//map<MGSize, MGSize>	mapa;
	//map<MGSize, MGSize>::iterator	itrmap;

	//for ( MGSize k=0; k<tabbndfac.size(); ++k)
	//{
	//	MGSize id = tabbndfac[k].cFirst();

	//	if ( (itrmap = mapa.find( id)) == mapa.end() )
	//		mapa.insert( map<MGSize, MGSize>::value_type( id, 1) );
	//	else
	//		++(*itrmap).second;

	//	id = tabbndfac[k].cSecond();

	//	if ( (itrmap = mapa.find( id)) == mapa.end() )
	//		mapa.insert( map<MGSize, MGSize>::value_type( id, 1) );
	//	else
	//		++(*itrmap).second;
	//}

	//for ( itrmap=mapa.begin(); itrmap!= mapa.end(); ++itrmap)
	//	if ( (*itrmap).second % 2 > 0)
	//	{
	//		cout << (*itrmap).first << " " << (*itrmap).second << endl;

	//		Vect2D vv = mpGrd->cNode( (*itrmap).first ).cPos();
	//		cout << vv.cX() << " " << vv.cY() << endl << endl;
	//	}
	////////////////////


	// check if any cell is painted - should not be neccessary
	Grid<DIM_2D>::ColCell::iterator	itr;
	for ( itr=mpGrd->rColCell().begin(); itr!=mpGrd->rColCell().end(); ++itr)
	{
		GCell& cell = *itr;
		if ( cell.IsPainted() )
			cout << "cell with id = " << cell.cId() << " is painted\n";
	}

	// find a starting cell 
	MGSize icell = 0;
	for ( MGSize i=0; i<mpGrd->SizeCellTab(); ++i)
	{
		GCell& cell = mpGrd->rCell( i);
		for ( MGSize in=0; in<GCell::SIZE; ++in)
			if ( cell.cNodeId( in) >= 1 && cell.cNodeId( in) <= 4 /* <- !!!*/ ) // bounding box nodes
			{
				cell.SetExternal( true);
				cell.Paint();
				icell = i;
				break;
			}

		if ( icell)
			break;
	}

	// start painting algo
	static vector<GCell*>	tabstack;

	tabstack.push_back( &mpGrd->rCell( icell) );

	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();
		bool bcolor = cell.IsExternal();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;

			if ( ic)
			{
				GCell &cellnei = mpGrd->rCell( ic);

				if ( ! cellnei.IsPainted() )
				{
					tabstack.push_back( &cellnei );
					cellnei.Paint();

					Key<2> key = cell.FaceKey( ifc);
					key.Sort();

					if ( binary_search( tabbndfac.begin(), tabbndfac.end(), key ) )
						cellnei.SetExternal( ! bcolor );
					else
						cellnei.SetExternal( bcolor);
				}
			}
		}
	}

	// wash paint and make statistics
	MGSize	nextern=0, npainted=0;
	for ( itr=mpGrd->rColCell().begin(); itr!=mpGrd->rColCell().end(); ++itr)
	{
		GCell& cell = *itr;

		if ( cell.IsExternal() )
			++nextern;

		if ( cell.IsPainted() )
		{
			++npainted;
			cell.WashPaint();
		}
	}
	
	cout << "found external/painted/total = " << nextern << " / " << npainted << " / " << mpGrd->SizeCellTab() << endl;

	return;
}


template<>
void Generator<DIM_2D>::FlagExternalCells()
{
	// flag cells conected to box nodes

	Grid<DIM_2D>::ColCell::iterator	itr;

	for ( itr=mpGrd->rColCell().begin(); itr!=mpGrd->rColCell().end(); ++itr)
	{
		GCell& cell = *itr;
		for ( MGSize in=0; in<GCell::SIZE; ++in)
			if ( cell.cNodeId( in) >= 1 && cell.cNodeId( in) <= 4 /* <- !!!*/ ) // bounding box nodes
			{
				cell.SetExternal( true);
				break;
			}
	}

}

template<>
void Generator<DIM_2D>::RemoveExternalCells()
{
	for ( Grid<DIM_2D>::ColCell::iterator itr = mpGrd->rColCell().begin(); itr != mpGrd->rColCell().end(); ++itr)
	{
		if ( (*itr).IsExternal() )
			mpGrd->EraseCellSafe( itr->cId() );
			//mKernel.EraseCellSafe( itr->cId() );
			//mpGrd->rColCell().erase( itr);
	}

	// removing nodes of the external cube
	mpGrd->EraseNode( 1);
	mpGrd->EraseNode( 2);
	mpGrd->EraseNode( 3);
	mpGrd->EraseNode( 4);
 
}






template <>
void Generator<DIM_2D>::DelaunayCorrection( const MGSize& maxiter)
{
	Grid<DIM_2D>::ColCell::iterator	itr;
	
	GMetric metprop, met;
	
	ProgressBar	bar(40);

	MGSize iter = 1;
	MGSize countswap = 1;

	while ( countswap && iter <= maxiter)
	{
		for ( itr = mpGrd->rColCell().begin(); itr != mpGrd->rColCell().end(); ++itr)
			itr->SetModified( false);

		bar.Init( mpGrd->SizeCellTab() );
		bar.Start();

		MGSize tabidn[4];
		MGSize count = 0;

		countswap = 0;

		for ( itr = mpGrd->rColCell().begin(); itr != mpGrd->rColCell().end(); ++itr, ++bar)
		{
			GCell &cell = *itr;

			Simplex<DIM_2D>	tri = GridGeom<DIM_2D>::CreateSimplex( cell, *mpGrd);

			if ( cell.IsExternal() || cell.IsModified() )
				continue;

			for ( MGSize ifac=0; ifac<GCell::SIZE; ++ifac)
			{
				GCell &cellnei = mpGrd->rCell( cell.cCellId( ifac).first );

				if ( cellnei.IsExternal() || cellnei.IsModified() || cellnei.cId() == 0 )
					continue;

				tabidn[0] = cell.cNodeId( Simplex<DIM_2D>::cFaceConn( ifac, 0 ) );
				tabidn[1] = cell.cNodeId( Simplex<DIM_2D>::cFaceConn( ifac, 1 ) );
				tabidn[2] = cell.cNodeId( ifac );
				tabidn[3] = cellnei.cNodeId( cell.cCellId( ifac).second );

				GVect vc(0.0);
				met.Reset();
				for ( MGSize k=0; k<4; ++k)
				{
					vc += mpGrd->cNode( tabidn[k]).cPos();
					met += mData.cMetric( tabidn[k] );
				}
				met = 0.25 * met;
				vc *= 0.25;

				//metprop = 0.5 * (met + mData.cMetric( tabidn[0] ) );
				//MGFloat lc0 = ::sqrt( met.Distance( mpGrd->cNode( tabidn[0]).cPos() - vc ) );

				//metprop = 0.5 * (met + mData.cMetric( tabidn[1] ) );
				//MGFloat lc1 = ::sqrt( met.Distance( mpGrd->cNode( tabidn[1]).cPos() - vc ) );

				//metprop = 0.5 * (met + mData.cMetric( tabidn[2] ) );
				//MGFloat lc2 = ::sqrt( met.Distance( mpGrd->cNode( tabidn[2]).cPos() - vc ) );

				//metprop = 0.5 * (met + mData.cMetric( tabidn[3] ) );
				//MGFloat lc3 = ::sqrt( met.Distance( mpGrd->cNode( tabidn[3]).cPos() - vc ) );

				//MGFloat lcur  = lc0 + lc1;
				//MGFloat lprop = lc2 + lc3;


				//MGFloat lcur  = ::sqrt( met.Distance( mpGrd->cNode( tabidn[1]).cPos() - mpGrd->cNode( tabidn[0]).cPos() ) );
				//MGFloat lprop = ::sqrt( met.Distance( mpGrd->cNode( tabidn[3]).cPos() - mpGrd->cNode( tabidn[2]).cPos() ) );




				//met = 0.5 * ( mData.cMetric( tabidn[0] ) + mData.cMetric( tabidn[1] ) );
				//metprop = 0.5 * ( mData.cMetric( tabidn[2] ) + mData.cMetric( tabidn[3] ) );

				//MGFloat lcur  = ::sqrt( met.Distance( mpGrd->cNode( tabidn[1]).cPos() - mpGrd->cNode( tabidn[0]).cPos() ) );
				//MGFloat lprop = ::sqrt( metprop.Distance( mpGrd->cNode( tabidn[3]).cPos() - mpGrd->cNode( tabidn[2]).cPos() ) );


				GVect tabv[4];


				SMatrix<2> mtx;

				met.EigenDecompose( mtx);
				met.EigenMultDecomp( tabv[0], mpGrd->cNode( tabidn[0]).cPos() - vc, mtx );
				met.EigenMultDecomp( tabv[1], mpGrd->cNode( tabidn[1]).cPos() - vc, mtx );
				met.EigenMultDecomp( tabv[2], mpGrd->cNode( tabidn[2]).cPos() - vc, mtx );
				met.EigenMultDecomp( tabv[3], mpGrd->cNode( tabidn[3]).cPos() - vc, mtx );

				//met.Decompose();
				//met.MultDecomp( tabv[0], mpGrd->cNode( tabidn[0]).cPos() - vc );
				//met.MultDecomp( tabv[1], mpGrd->cNode( tabidn[1]).cPos() - vc );
				//met.MultDecomp( tabv[2], mpGrd->cNode( tabidn[2]).cPos() - vc );
				//met.MultDecomp( tabv[3], mpGrd->cNode( tabidn[3]).cPos() - vc );

				MGFloat aret = incircle( tabv[0].cTab(), tabv[2].cTab(), tabv[1].cTab(), tabv[3].cTab() );


				if ( aret > 0.0 )
				//if ( lprop < lcur)
				{
					++count;

					Operator2D_Flip22	flip( *mpGrd );

					if ( flip.Check( cell.cId(), cellnei.cId() ) )
					{
						flip.Execute();
						cell.SetModified( true);
						cellnei.SetModified( true);
						++countswap;
					}

				}

			}
		}

		bar.Finish();
		cout << "found edges to swap; " << countswap << endl;


		++iter;
		//cout << "found " << count << " edges to swap; " << countswap << endl;
	}
				
				
}


template<>
void Generator<DIM_2D>::SmoothVariation()
{
	MGSize iter = 1;
	MGSize count;

	do
	{

	vector< Key<2> > tabedge;
	vector< Key<2> >::iterator itr;

	vector<GVect>	tabprop;


	mpGrd->FindInternalEdges( tabedge);

	for ( itr=tabedge.begin(); itr!=tabedge.end(); ++itr)
	{
		GMetric metL = mData.cMetric( itr->cFirst() );
		GMetric metR = mData.cMetric( itr->cSecond() );
		GVect vdist = mpGrd->cNode( itr->cSecond() ).cPos() - mpGrd->cNode( itr->cFirst() ).cPos();

		MGFloat distL = metL.Distance( vdist);
		MGFloat distR = metR.Distance( vdist);
		MGFloat distMax = max( distL, distR);
		MGFloat distMin = min( distL, distR);

		if ( distMax / distMin > 2.0 )
			tabprop.push_back( 0.5 * (mpGrd->cNode( itr->cSecond() ).cPos() + mpGrd->cNode( itr->cFirst() ).cPos()) );
	}

	cout << "SmoothVariation tabprop.size = " << tabprop.size() << endl;




	/////////////////////////////////////////////////////////////
	// randomize the order the nodes will be inserted
	random_shuffle( tabprop.begin(), tabprop.end() );

	const MGFloat	constBETA = 1.0;


	count = 0;
	ProgressBar	bar(40);


	static vector<MGSize>	tabccell;
	static vector<GFace>	tabcface;

	static vector<GCell*>	tabcavcell;

	tabcface.reserve(20);
	tabccell.reserve(20);
	tabcavcell.reserve(20);


	bar.Init( tabprop.size() );
	bar.Start();
	
	
	for ( vector<GVect>::iterator itr = tabprop.begin(); itr != tabprop.end(); ++itr, ++bar)
	{
		
		MGSize inod;
		GVect vct = *itr;
		
		/////////////////////////////////////////////////////////////
		// find cell containing vct
		MGSize	icell = mKernel.mLoc.Localize( vct);


		/////////////////////////////////////////////////////////////
		// search for the delaunay cavity of vct

		tabcface.resize(0);
		tabccell.resize(0);
		tabcavcell.resize(0);

		GMetric	met = mpCSpace->GetSpacing( vct);


		// tabccel  - cells creating a cavity for the vct
		// tabcface - faces bounding the cavity
		//if ( ! mKernel.FindCavity( tabccell, tabcface, icell, vct, GMetric(), false) )
		//if ( ! mKernel.FindCavity( tabccell, tabcface, icell, vct, met, true) )

		if ( ! mKernel.FindCavityMetric( tabccell, icell, vct, met) )
			continue;


		/////////////////////////////////////////////////////////////
		// find all nodes of the cavity
		vector<MGSize>	tabnod;
		//for ( MGSize ic=0; ic<tabcavcell.size(); ++ic)
		//	for ( MGSize in=0; in<GCell::SIZE; ++in)
		//		tabnod.push_back( tabcavcell[ic]->cNodeId( in) );

		for ( MGSize ic=0; ic<tabccell.size(); ++ic)
			for ( MGSize in=0; in<GCell::SIZE; ++in)
				tabnod.push_back( mpGrd->cCell( tabccell[ic] ).cNodeId( in) );

		sort( tabnod.begin(), tabnod.end() );
		vector<MGSize>::iterator itrend = unique( tabnod.begin(), tabnod.end() );
		tabnod.erase( itrend, tabnod.end() );

		/////////////////////////////////////////////////////////////
		// insert new point into mesh
		mKernel.CavityCorrection( tabccell, vct);
		mKernel.CavityBoundary( tabcface, tabccell);

		inod = mKernel.InsertPoint( tabccell, tabcface, vct);

		if ( inod)
		{
			mData.InsertMetric( met, inod);
			
			++count;
		}
	}

	bar.Finish();
	cout << "Number of inserted nodes = " << count << endl;

	}
	while ( count > 0 && iter++ <10 );
}


//template<>
//void Generator<DIM_2D>::Triangulate_CircumCenter() 
//{
//	ProgressBar	bar(40);
//
//
//	Grid<DIM_2D>&	grid = mKernel.rGrid();
//	Grid<DIM_2D>::ColCell::const_iterator	citr;
//	
//	typedef triple< GVect, MGSize, GMetric> TElem;
//
//
//	
//	GVect	vc;
//	GMetric dpmet, met;
//	MGFloat	tabdist[GCell::SIZE];
//	
//	MGSize count, iter = 1;
//	
//	do
//	{
//		vector<TElem>	tabprop;
//		
//		for ( citr = grid.cColCell().begin(); citr != grid.cColCell().end(); ++citr)
//		{
//			if ( (*citr).IsGood() || (*citr).IsExternal() )
//				continue;
//				
//				
//			grid.rCell( (*citr).cId() ).SetModified( false);
//			
//
//			MetricInterpol< MGFloat, DIM_2D > interpol;
//
//			interpol.Reset();
//			for ( MGSize i=0; i<GCell::SIZE; ++i)
//				interpol.Add( mData.cMetric( (*citr).cNodeId( i ) ) );
//
//			GMetric::SMtx mtxcell;
//			interpol.GetResult( mtxcell);
//
//			GMetric metcell( mtxcell);
//
//			GridGeom<DIM_2D>::CreateSimplex( *citr, grid).SphereCenterMetric( vc, metcell);
//
//			if ( ! mKernel.mBox.IsInside( vc) )
//				continue;
//
//			met = mpCSpace->GetSpacing( vc);
//
//			dpmet.Reset();
//			
//			for ( MGSize i=0; i<GCell::SIZE; ++i)
//			{
//				const GVect vtmp = vc - grid.cNode( (*citr).cNodeId( i ) ).cPos();
//
//				const GMetric& metn = mData.cMetric( (*citr).cNodeId( i ) );
//
//				MGFloat l1 = ::sqrt( metn.Distance( vtmp) );
//				MGFloat l2 = l1;//::sqrt( met.Distance( vtmp) );
//				tabdist[i] =  2.0 / 3.0 * (l1*l1 + l1*l2 + l2*l2) / (l1 + l2);
//			}
//			
//			for ( MGSize i=0; i<GCell::SIZE; ++i)
//			{
//				if ( 1.0 > 1.3 * tabdist[i] )
//				{
//					grid.rCell( (*citr).cId() ).SetGood( true);
//					break;
//				}
//			}
//			
//			if ( ! (*citr).IsGood() )
//				tabprop.push_back( TElem( vc, (*citr).cId(), met )  );
//			
//		}
//		
//		TRACE( "number of new points = " << tabprop.size() );
//
//
//
//		count = 0;
//		
//		if ( ! tabprop.size() )
//			continue;
//
//		random_shuffle( tabprop.begin(), tabprop.end() );
//
//
//		bar.Init( tabprop.size() );
//		bar.Start();
//		
//		
//		for ( vector<TElem>::iterator itr = tabprop.begin(); itr != tabprop.end(); ++itr, ++bar)
//		{
//			
//			if ( grid.cCell( (*itr).second ).IsModified() )
//				continue;
//			
//			vc = (*itr).first;
//			
//			MGSize inod = mKernel.InsertPointMetric( GNode( vc), met);
//			if ( inod)
//			{
//				mData.InsertMetric( (*itr).third, inod);
//				//if ( inod > mtabMetric.size()-1 )
//				//	mtabMetric.resize( inod+1);
//				//
//				//mtabMetric[inod] = (*itr).third;
//				
//				
//				++count;
//			}
//		}
//
//		bar.Finish();
//
//		DelaunayCorrection( 1);
//
//	
//	}
//	while ( count && (iter++) < 100);
//}

template<>
void Generator<DIM_2D>::Triangulate_CircumCenter() 
{
	const MGFloat	constALPHA = 1.6;
	const MGFloat	constBETA = 1.3;

	ProgressBar	bar(40);


	Grid<DIM_2D>&	grid = mKernel.rGrid();
	Grid<DIM_2D>::ColCell::const_iterator	citr;
	
	typedef triple< GVect, MGSize, GMetric> TElem;


	
	GVect	vc;
	GMetric dpmet, met;
	MGFloat	tabdist[GCell::SIZE];
	
	MGSize count, iter = 1;
	
	do
	{
		vector<TElem>	tabprop;
		
		for ( citr = grid.cColCell().begin(); citr != grid.cColCell().end(); ++citr)
		{
			if ( (*citr).IsGood() || (*citr).IsExternal() )
				continue;
				
				
			//grid.rCell( (*citr).cId() ).SetModified( false);
			
			//vc = GridGeom<DIM_2D>::CreateSimplex( *citr, grid).Center();
			//GMetric metcell = mpCSpace->GetSpacing( vc);	// TODO :: VERY expensive !!!


			MetricInterpol< MGFloat, DIM_2D > interpol;

			interpol.Reset();
			for ( MGSize i=0; i<GCell::SIZE; ++i)
				interpol.Add( mData.cMetric( (*citr).cNodeId( i ) ) );

			GMetric::SMtx mtxcell;
			interpol.GetResult( mtxcell);

			GMetric metcell( mtxcell);


			GMetric metiso;
			metiso.InitIso( 1.0);
			//GridGeom<DIM_2D>::CreateSimplex( *citr, grid).SphereCenterMetric( vc, metiso);
			GridGeom<DIM_2D>::CreateSimplex( *citr, grid).SphereCenterMetric( vc, metcell);

			if ( ! mKernel.mBox.IsInside( vc) )
				continue;

			MGSize	icell = mKernel.mLoc.Localize( vc);
			GCell& cell = mpGrd->rCell(icell);

			if ( cell.IsExternal() )
				continue;

			cell.SetModified( false);


			met = mpCSpace->GetSpacing( vc);

			dpmet.Reset();
			
			for ( MGSize i=0; i<GCell::SIZE; ++i)
			{
				const GVect vtmp = vc - grid.cNode( cell.cNodeId( i ) ).cPos();

				const GMetric& metn = mData.cMetric( cell.cNodeId( i ) );

				MGFloat l1 = ::sqrt( metn.Distance( vtmp) );
				MGFloat l2 = ::sqrt( met.Distance( vtmp) );
				tabdist[i] =  2.0 / 3.0 * (l1*l1 + l1*l2 + l2*l2) / (l1 + l2);
				tabdist[i] = l1;
			}
			
			for ( MGSize i=0; i<GCell::SIZE; ++i)
			{
				if ( 1.0 > 1.2 * tabdist[i] )
				{
					cell.SetGood( true);
					break;
				}
			}
			
			if ( ! cell.IsGood() )
				tabprop.push_back( TElem( vc, cell.cId(), met )  );
			
		}
		
		TRACE( "number of new points = " << tabprop.size() );



		count = 0;
		
		if ( ! tabprop.size() )
			continue;

		random_shuffle( tabprop.begin(), tabprop.end() );
		
		bar.Init( tabprop.size() );
		bar.Start();
		
		
		for ( vector<TElem>::iterator itr = tabprop.begin(); itr != tabprop.end(); ++itr, ++bar)
		{
			
			if ( grid.cCell( (*itr).second ).IsModified() )
				continue;
			
			vc = (*itr).first;
		
			met = mpCSpace->GetSpacing( vc);

/////////////
//			/////////////////////////////////////////////////////////////
//			// search for the delaunay cavity of vct
//
//			vector<MGSize>	tabccell;
//			vector<GFace>	tabcface;
//
//			vector<GCell*>	tabcavcell;
//
//			tabcface.resize(0);
//			tabccell.resize(0);
//			tabcavcell.resize(0);
//
//			GMetric	met = mpCSpace->GetSpacing( vc);
//			MGSize	icell = mKernel.mLoc.Localize( vc);
//
//
//			// tabccel  - cells creating a cavity for the vct
//			// tabcface - faces bounding the cavity
//			//if ( ! mKernel.FindCavity( tabccell, tabcface, icell, vct, GMetric(), false) )
//			//if ( ! mKernel.FindCavity( tabccell, tabcface, icell, vct, met, true) )
//
//			if ( ! mKernel.FindCavityMetric( tabccell, icell, vc, met) )
//				continue;
//
//
//			/////////////////////////////////////////////////////////////
//			// find all nodes of the cavity
//			vector<MGSize>	tabnod;
//			//for ( MGSize ic=0; ic<tabcavcell.size(); ++ic)
//			//	for ( MGSize in=0; in<GCell::SIZE; ++in)
//			//		tabnod.push_back( tabcavcell[ic]->cNodeId( in) );
//
//			for ( MGSize ic=0; ic<tabccell.size(); ++ic)
//				for ( MGSize in=0; in<GCell::SIZE; ++in)
//					tabnod.push_back( mpGrd->cCell( tabccell[ic] ).cNodeId( in) );
//
//			sort( tabnod.begin(), tabnod.end() );
//			tabnod.erase( unique( tabnod.begin(), tabnod.end() ), tabnod.end() );
//
//			/////////////////////////////////////////////////////////////
//			// check the distance from new point to cavity nodes
//			// if one of them is too close then reject the new point
//			bool bOk = true;
//
//			for ( MGSize in=0; in<tabnod.size(); ++in)
//			{
//				const GVect& vn = mpGrd->cNode( tabnod[in]).cPos();
//				const GMetric& metn = mData.cMetric( tabnod[in] );
//
//				const GVect vtmp = vc - vn;
//
//				MGFloat l1 = ::sqrt( metn.Distance( vtmp) );
//				MGFloat l2 = ::sqrt( met.Distance( vtmp) );
//
//				MGFloat dist =  2.0 / 3.0 * (l1*l1 + l1*l2 + l2*l2) / (l1 + l2);
//
//				//MGFloat dist = (vn - vct).module();
//				//if ( dist*constBETA < hspac)
//
//				if ( dist*constBETA < 1)
//				{
//					bOk = false;
//					break;
//				}
//			}
//
//
//			/////////////////////////////////////////////////////////////
//			// wash paint and start again
//			if ( ! bOk)
//			{
//				//for ( MGSize ic=0; ic<tabcavcell.size(); ++ic)
//				//	tabcavcell[ic]->WashPaint();
//
//				for ( MGSize k=0; k<tabccell.size(); ++k)
//					mpGrd->rCell( tabccell[k]).WashPaint();
//
//				continue;
//			}
//
/////////////

			MGSize inod = mKernel.InsertPointMetric( GNode( vc), met);
			if ( inod)
			{
				mData.InsertMetric( (*itr).third, inod);
				//if ( inod > mtabMetric.size()-1 )
				//	mtabMetric.resize( inod+1);
				//
				//mtabMetric[inod] = (*itr).third;
				
				
				++count;
			}
		}

		bar.Finish();

		//DelaunayCorrection( 1);

		///////////////////////////////////////////////////////////////
		//// randomize the order the nodes will be inserted
		//random_shuffle( tabprop.begin(), tabprop.end() );


		//static vector<MGSize>	tabccell;
		//static vector<GFace>	tabcface;

		//static vector<GCell*>	tabcavcell;

		//tabcface.reserve(20);
		//tabccell.reserve(20);
		//tabcavcell.reserve(20);


		//bar.Init( tabprop.size() );
		//bar.Start();
		//
		//
		//for ( vector<TElem>::iterator itr = tabprop.begin(); itr != tabprop.end(); ++itr, ++bar)
		//{
		//	
		//	MGSize inod;
		//	GVect vct = itr->first;
		//	
		//	/////////////////////////////////////////////////////////////
		//	// find cell containing vct
		//	MGSize	icell = mKernel.mLoc.Localize( vct);

		//	if ( mpGrd->cCell(icell).IsExternal() )
		//		continue;


		//	/////////////////////////////////////////////////////////////
		//	// search for the delaunay cavity of vct

		//	tabcface.resize(0);
		//	tabccell.resize(0);
		//	tabcavcell.resize(0);

		//	GMetric	met = mpCSpace->GetSpacing( vct);


		//	// tabccel  - cells creating a cavity for the vct
		//	// tabcface - faces bounding the cavity
		//	//if ( ! mKernel.FindCavity( tabccell, tabcface, icell, vct, GMetric(), false) )
		//	//if ( ! mKernel.FindCavity( tabccell, tabcface, icell, vct, met, true) )

		//	if ( ! mKernel.FindCavityMetric( tabccell, icell, vct, met) )
		//		continue;


		//	/////////////////////////////////////////////////////////////
		//	// find all nodes of the cavity
		//	vector<MGSize>	tabnod;
		//	//for ( MGSize ic=0; ic<tabcavcell.size(); ++ic)
		//	//	for ( MGSize in=0; in<GCell::SIZE; ++in)
		//	//		tabnod.push_back( tabcavcell[ic]->cNodeId( in) );

		//	for ( MGSize ic=0; ic<tabccell.size(); ++ic)
		//		for ( MGSize in=0; in<GCell::SIZE; ++in)
		//			tabnod.push_back( mpGrd->cCell( tabccell[ic] ).cNodeId( in) );

		//	sort( tabnod.begin(), tabnod.end() );
		//	vector<MGSize>::iterator itrend = unique( tabnod.begin(), tabnod.end() );
		//	tabnod.erase( itrend, tabnod.end() );

		//	/////////////////////////////////////////////////////////////
		//	// check the distance from new point to cavity nodes
		//	// if one of them is too close then reject the new point
		//	bool bOk = true;

		//	for ( MGSize in=0; in<tabnod.size(); ++in)
		//	{
		//		const GVect& vn = mpGrd->cNode( tabnod[in]).cPos();
		//		const GMetric& metn = mData.cMetric( tabnod[in] );

		//		const GVect vtmp = vct - vn;

		//		MGFloat l1 = ::sqrt( metn.Distance( vtmp) );
		//		MGFloat l2 = ::sqrt( met.Distance( vtmp) );

		//		MGFloat dist =  2.0 / 3.0 * (l1*l1 + l1*l2 + l2*l2) / (l1 + l2);

		//		//MGFloat dist = (vn - vct).module();
		//		//if ( dist*constBETA < hspac)

		//		if ( dist*constBETA < 1)
		//		{
		//			bOk = false;
		//			break;
		//		}
		//	}


		//	/////////////////////////////////////////////////////////////
		//	// wash paint and start again
		//	if ( ! bOk)
		//	{
		//		//for ( MGSize ic=0; ic<tabcavcell.size(); ++ic)
		//		//	tabcavcell[ic]->WashPaint();

		//		for ( MGSize k=0; k<tabccell.size(); ++k)
		//			mpGrd->rCell( tabccell[k]).WashPaint();

		//		continue;
		//	}



		//	/////////////////////////////////////////////////////////////
		//	// insert new point into mesh
		//	mKernel.CavityCorrection( tabccell, vct);
		//	mKernel.CavityBoundary( tabcface, tabccell);

		//	//if ( !tabccell.size() || !tabcface.size() )
		//	//{
		//	//	//cout << " !!!!!\n";
		//	//	continue;
		//	//}

		//	inod = mKernel.InsertPoint( tabccell, tabcface, vct);

		//	if ( inod)
		//	{
		//		mData.InsertMetric( met, inod);
		//		
		//		++count;
		//	}
		//}

		//bar.Finish();
		//cout << "Number of inserted nodes = " << count << endl;

		////WriteGrdTEC<DIM_2D> wtec(mpGrd);
		////wtec.DoWrite( "iter.plt");


	
	}
	while ( count && (iter++) < 50);
}


template<>
void Generator<DIM_2D>::Triangulate_Weatherill()
{
	// should be improved (see 3D) !!!

	TRACE( "Triangulation started");


	const MGFloat	constALPHA = 1.3;

	ProgressBar	bar(40);


	Grid<DIM_2D>&	grid = mKernel.rGrid();
	Grid<DIM_2D>::ColCell::const_iterator	citr;
	
	typedef triple< GVect, MGSize, GMetric> TElem;


	
	GVect	vc;
	GMetric dpmet, met;
	MGFloat	tabdist[GCell::SIZE];
	
	MGSize count, iter = 1;
	
	do
	{
		vector<TElem>	tabprop;
		
		for ( citr = grid.cColCell().begin(); citr != grid.cColCell().end(); ++citr)
		{
			if ( (*citr).IsGood() || (*citr).IsExternal() )
				continue;
				
				
			grid.rCell( (*citr).cId() ).SetModified( false);
			
			vc = GridGeom<DIM_2D>::CreateSimplex( *citr, grid).Center();
			met = mpCSpace->GetSpacing( vc);

			dpmet.Reset();
			
			for ( MGSize i=0; i<GCell::SIZE; ++i)
			{
				const GVect vtmp = vc - grid.cNode( (*citr).cNodeId( i ) ).cPos();

				const GMetric& metn = mData.cMetric( (*citr).cNodeId( i ) );

				MGFloat l1 = ::sqrt( metn.Distance( vtmp) );
				MGFloat l2 = ::sqrt( met.Distance( vtmp) );
				tabdist[i] =  2.0 / 3.0 * (l1*l1 + l1*l2 + l2*l2) / (l1 + l2);
			}
			
			for ( MGSize i=0; i<GCell::SIZE; ++i)
			{
				if ( 1.0 > constALPHA * tabdist[i] )
				{
					grid.rCell( (*citr).cId() ).SetGood( true);
					break;
				}
			}
			
			if ( ! (*citr).IsGood() )
				tabprop.push_back( TElem( vc, (*citr).cId(), met )  );
			
		}
		
		TRACE( "number of new points = " << tabprop.size() );



		count = 0;
		
		if ( ! tabprop.size() )
			continue;

		//random_shuffle( tabprop.begin(), tabprop.end() );


		bar.Init( tabprop.size() );
		bar.Start();
		
		
		for ( vector<TElem>::iterator itr = tabprop.begin(); itr != tabprop.end(); ++itr, ++bar)
		{
			
			if ( grid.cCell( (*itr).second ).IsModified() )
				continue;
			
			vc = (*itr).first;
			
			MGSize inod = mKernel.InsertPointMetric( GNode( vc), met);
			if ( inod)
			{
				mData.InsertMetric( (*itr).third, inod);
				//if ( inod > mtabMetric.size()-1 )
				//	mtabMetric.resize( inod+1);
				//
				//mtabMetric[inod] = (*itr).third;
				
				
				++count;
			}
		}

		bar.Finish();

		DelaunayCorrection( 1);

	
	}
	while ( count && (iter++) < 100);
}




template<>
void Generator<DIM_2D>::Triangulate_AlongEdges( vector< Key<2> >* ptabedge )
{
	//const MGFloat	constALPHA = 1.6;
	//const MGFloat	constBETA = 1.3;

	const MGSize	nMaxIter		= this->ConfigSect().ValueSize( ConfigStr::Master::Generator::GEN_MAXITER );
	const MGFloat	constALPHA		= this->ConfigSect().ValueFloat( ConfigStr::Master::Generator::GEN_CONSTALPHA );
	const MGFloat	constBETA		= this->ConfigSect().ValueFloat( ConfigStr::Master::Generator::GEN_CONSTBETA );

	const bool		constBDistCheck	= this->ConfigSect().ValueBool( ConfigStr::Master::Generator::GEN_WITH_BDIST_CHECK );
	const MGFloat	constBndALPHA	= this->ConfigSect().ValueFloat( ConfigStr::Master::Generator::GEN_BND_CONSTALPHA );


	


	MGSize count, iter = 1;
	ProgressBar	bar(40);


	do
	{

		/////////////////////////////////////////////////////////////
		// find all internal edges
		vector< Key<2> > tabedg;
		mpGrd->FindInternalEdges( tabedg );


		bar.Init( tabedg.size() );
		bar.Start();

		vector<GVect>	tabprop;

		for ( MGSize ie=0; ie<tabedg.size(); ++ie, ++bar)
		{
			if ( binary_search( ptabedge->begin(), ptabedge->end(), tabedg[ie] ) )
				continue;

			GVect v0 = mpGrd->cNode( tabedg[ie].cFirst() ).cPos();
			GVect v1 = mpGrd->cNode( tabedg[ie].cSecond() ).cPos();

			GMetric met = 0.5 * ( mData.cMetric( tabedg[ie].cFirst() ) + mData.cMetric( tabedg[ie].cSecond() ) );
			MGFloat dist = ::sqrt( met.Distance( v1 - v0 ) );

			/////////////////////////////////////////////////////////////
			// jump to another edge - current one is short enough
			if ( dist < constALPHA )
				continue;


			/////////////////////////////////////////////////////////////
			// run 1D generator for line created for the edge

			GET::Line<DIM_2D> gline( v0, (v1-v0).versor() );

			SliceControlSpace<DIM_1D,DIM_2D>	scspace( mpCSpace, &gline);

			GridWBnd<DIM_1D>	grid;
			BndGrid<DIM_1D>		&bgrd = grid.rBndGrid();

			Generator<DIM_1D>	gen( grid, &scspace);

			TDefs<DIM_1D>::GBndNode	bnod;

			bnod.rPos() = gline.FindParam( v0);
			bnod.rGrdId() = 0;
			bgrd.rNode1() = bnod;

			bnod.rPos() = gline.FindParam( v1);
			bnod.rGrdId() = 0;
			bgrd.rNode2() = bnod;

			gen.Process();


			/////////////////////////////////////////////////////////////
			// get all internal 1D grid nods and put them in the proposition node array

			//for ( MGSize k=2; k<grid.SizeNodeTab(); ++k)
			for ( MGSize k=3; k<=grid.SizeNodeTab(); ++k)
			{
				GVect vp;
				gline.GetCoord( vp, grid.cNode(k).cPos() );
				tabprop.push_back( vp);
			}
		}

		bar.Finish();


		count = 0;
		
		if ( ! tabprop.size() )
			continue;


		/////////////////////////////////////////////////////////////
		// randomize the order the nodes will be inserted
		random_shuffle( tabprop.begin(), tabprop.end() );


		static vector<MGSize>	tabccell;
		static vector<GFace>	tabcface;

		static vector<GCell*>	tabcavcell;

		tabcface.reserve(20);
		tabccell.reserve(20);
		tabcavcell.reserve(20);


		bar.Init( tabprop.size() );
		bar.Start();
		
		
		for ( vector<GVect>::iterator itr = tabprop.begin(); itr != tabprop.end(); ++itr, ++bar)
		{
			bool bOk = true;

			
			MGSize inod;
			GVect vct = *itr;
			
			/////////////////////////////////////////////////////////////
			// find cell containing vct
			MGSize	icell = mKernel.mLoc.Localize( vct);


			GMetric	met = mpCSpace->GetSpacing( vct);


			///////////////////////////////////////////////////////////////
			//// check dist from boundary
			//Grid<DIM_2D>&	grid = mKernel.rGrid();

			//const GCell& cell = grid.cCell( icell);

			//Simplex<DIM_2D> tri = GridGeom<DIM_2D>::CreateSimplex( icell, grid);

			////if ( ! (*citr).IsGood() )
			//{
			//	for ( MGSize i=0; i<GCell::SIZE; ++i)
			//	{
			//		const GCell& celln = grid.cCell( cell.cCellId(i).first );

			//		if ( celln.IsExternal() )  // check dist
			//		{
			//			GFace face;
			//			cell.GetFaceVNIn( face, i);
			//			SimplexFace<DIM_2D> sface = GridGeom<DIM_2D>::CreateSimplexFace( face, grid);

			//			GVect vnface = sface.Vn();
			//			GVect dv = vct - sface.Center();
			//			GVect vv = ( (dv * vnface) / (vnface*vnface) ) * vnface;

			//			MGFloat h = tri.Volume() / sface.Area();

			//			//vv = h * vnface.versor();

			//			MGFloat facedist;
			//			//GMetric met = mpCSpace->GetSpacing( vc);	// TODO :: VERY expensive !!!
			//			facedist = ::sqrt( met.Distance( vv) );
			//			//facedist =  vv * vv;

			//			//facedist = max( mData.cMetric( face.cNodeId( 0 ) ).Distance( vv ), mData.cMetric( face.cNodeId( 1 ) ).Distance( vv ) );
			//			//facedist = ::sqrt( facedist );


			//			//if ( 1.0 > facedist )
			//			if ( 1.0 > 0.5*constBndALPHA * facedist )
			//			{
			//				bOk = false;
			//				//cout << "good\n";
			//				break;
			//			}
			//		}
			//	}	
			//}

			//if ( ! bOk)
			//	continue;

			/////////////////////////////////////////////////////////////
			// search for the delaunay cavity of vct

			tabcface.resize(0);
			tabccell.resize(0);
			tabcavcell.resize(0);



			// tabccel  - cells creating a cavity for the vct
			// tabcface - faces bounding the cavity
			//if ( ! mKernel.FindCavity( tabccell, tabcface, icell, vct, GMetric(), false) )
			//if ( ! mKernel.FindCavity( tabccell, tabcface, icell, vct, met, true) )

			if ( ! mKernel.FindCavityMetric( tabccell, icell, vct, met) )
				continue;


			/////////////////////////////////////////////////////////////
			// find all nodes of the cavity
			vector<MGSize>	tabnod;
			//for ( MGSize ic=0; ic<tabcavcell.size(); ++ic)
			//	for ( MGSize in=0; in<GCell::SIZE; ++in)
			//		tabnod.push_back( tabcavcell[ic]->cNodeId( in) );

			for ( MGSize ic=0; ic<tabccell.size(); ++ic)
				for ( MGSize in=0; in<GCell::SIZE; ++in)
					tabnod.push_back( mpGrd->cCell( tabccell[ic] ).cNodeId( in) );

			sort( tabnod.begin(), tabnod.end() );
			vector<MGSize>::iterator itrend = unique( tabnod.begin(), tabnod.end() );
			tabnod.erase( itrend, tabnod.end() );

			/////////////////////////////////////////////////////////////
			// check the distance from new point to cavity nodes
			// if one of them is to close then reject the new point

			for ( MGSize in=0; in<tabnod.size(); ++in)
			{
				const GVect& vn = mpGrd->cNode( tabnod[in]).cPos();
				const GMetric& metn = mData.cMetric( tabnod[in] );

				const GVect vtmp = vct - vn;

				MGFloat l1 = ::sqrt( metn.Distance( vtmp) );
				MGFloat l2 = ::sqrt( met.Distance( vtmp) );

				MGFloat dist =  2.0 / 3.0 * (l1*l1 + l1*l2 + l2*l2) / (l1 + l2);

				//MGFloat dist = (vn - vct).module();
				//if ( dist*constBETA < hspac)

				if ( dist*constBETA < 1)
				{
					bOk = false;
					break;
				}
			}


			/////////////////////////////////////////////////////////////
			// check dist from boundary

			if ( bOk && constBDistCheck )
			{
				for ( MGSize ic=0; ic<tabccell.size(); ++ic)
				{
					const GCell& cell = mpGrd->cCell( tabccell[ic] );
	
					Simplex<DIM_2D> tri = GridGeom<DIM_2D>::CreateSimplex( icell, *mpGrd);

					for ( MGSize i=0; i<GCell::SIZE; ++i)
					{
						const GCell& celln = mpGrd->cCell( cell.cCellId(i).first );

						if ( celln.IsExternal() )  // check dist
						{
							GFace face;
							cell.GetFaceVNIn( face, i);
							SimplexFace<DIM_2D> sface = GridGeom<DIM_2D>::CreateSimplexFace( face, *mpGrd);

							GVect vnface = sface.Vn();
							GVect dv = vct - sface.Center();
							GVect vv = ( (dv * vnface) / (vnface*vnface) ) * vnface;

							MGFloat h = tri.Volume() / sface.Area();

							//vv = h * vnface.versor();

							MGFloat facedist;
							//GMetric met = mpCSpace->GetSpacing( vc);	// TODO :: VERY expensive !!!
							//facedist =  vv * vv;

							//facedist = met.Distance( vv);
							facedist = max( mData.cMetric( face.cNodeId( 0 ) ).Distance( vv ), mData.cMetric( face.cNodeId( 1 ) ).Distance( vv ) );
							facedist = ::sqrt( facedist );


							//if ( 1.0 > facedist )
							if ( 1.0 > constBndALPHA * facedist )
							{
								bOk = false;
								//cout << "good\n";
								break;
							}
						}
					}	
				}

			}



			/////////////////////////////////////////////////////////////
			// wash paint and start again
			if ( ! bOk)
			{
				//for ( MGSize ic=0; ic<tabcavcell.size(); ++ic)
				//	tabcavcell[ic]->WashPaint();

				for ( MGSize k=0; k<tabccell.size(); ++k)
					mpGrd->rCell( tabccell[k]).WashPaint();

				continue;
			}



			/////////////////////////////////////////////////////////////
			// insert new point into mesh
			mKernel.CavityCorrection( tabccell, vct);
			mKernel.CavityBoundary( tabcface, tabccell);

			//if ( !tabccell.size() || !tabcface.size() )
			//{
			//	//cout << " !!!!!\n";
			//	continue;
			//}

			inod = mKernel.InsertPoint( tabccell, tabcface, vct);

			if ( inod)
			{
				mData.InsertMetric( met, inod);
				
				++count;
			}
		}

		bar.Finish();
		cout << "Number of inserted nodes = " << count << endl;

		//WriteGrdTEC<DIM_2D> wtec(mpGrd);
		//wtec.DoWrite( "iter.plt");


	}
	while ( count && (iter++) < nMaxIter );

}




template<>
void Generator<DIM_2D>::Triangulate( vector< Key<2> >* ptabedge )
{
	if ( ! this->ConfigSectIsValid() )
		THROW_INTERNAL( "Generator<DIM_2D>::Triangulate -- config is invalid");

	MGString stype = this->ConfigSect().ValueString( ConfigStr::Master::Generator::TriangType::KEY );


	if ( stype == MGString( ConfigStr::Master::Generator::TriangType::Value::GENTT_EDGES ) )
		Triangulate_AlongEdges( ptabedge );
	else
	if ( stype == MGString( ConfigStr::Master::Generator::TriangType::Value::GENTT_CELLC ) )
		Triangulate_Weatherill();
	else
	if ( stype == MGString( ConfigStr::Master::Generator::TriangType::Value::GENTT_CIRCC ) )
		Triangulate_CircumCenter();
	else
		THROW_INTERNAL( "Generator<DIM_2D>::Triangulate -- unknown option");


}




template<>
void Generator<DIM_2D>::Smooth()
{
	// TODO :: use ratio of eigenvalues for scaling the coefficient
	const MGSize	maxiter = 5;
	const MGFloat	coeff = 0.1;

	vector<GVect>	tabpos;
	vector<MGFloat>	tabweight;

	tabpos.resize( mpGrd->SizeNodeTab() + 1 );
	tabweight.resize( mpGrd->SizeNodeTab() + 1);

	vector< Key<2> > tabedg;
	//mpGrd->FindInternalFaces( tabedg );

// TODO :: fix !!!!!!!!!!!!!!!!!!
//	const MGSize nbnod = mpGrd->cBndGrid().SizeNodeTab();
	const MGSize nbnod = 0;

	for ( MGSize iter=0; iter<maxiter; ++iter)
	{
		cout << "Smoothing iter = " << iter << endl;

	//	for ( MGSize in=nbnod+5; in<=mpGrd->SizeNodeTab(); ++in)
	//	{
	//		const GVect& vc = mpGrd->cNode( in ).cPos();

	//		vector<MGSize>	tabball;

	//		mKernel.FindBall( tabball, in);

	//		GMetric met = mData.cMetric( in );
	//		met.Decompose();

	//		GVect	pos(0.0);
	//		MGFloat	wgh = 0;

	//		for ( MGSize ic=0; ic<tabball.size(); ++ic)
	//		{
	//			const GCell& cell = mpGrd->cCell( tabball[ic] );


	//			GVect	tabv[GCell::SIZE];
	//			GVect	tabxv[GCell::SIZE];

	//			for ( MGSize i=0; i<GCell::SIZE; ++i)
	//				tabv[i] = mpGrd->cNode( cell.cNodeId(i) ).cPos();

	//			for ( MGSize i=0; i<GCell::SIZE; ++i)
	//				met.MultDecomp( tabxv[i], tabv[i] - vc);

	//			Simplex<DIM_2D> simp( tabxv[0], tabxv[1], tabxv[2]);

	//			MGFloat area = simp.Volume();
	//			GVect vct = simp.Center();

	//			pos	+= area * vct;
	//			wgh += area;
	//		}


	//		met = mData.cMetric( in );
	//		met.Invert();
	//		met.Decompose();

	//		pos *= coeff / wgh;

	//		GVect dvmet;
	//		met.MultDecomp( dvmet, pos);

	//		mpGrd->rNode( in).rPos() += dvmet;

	//	}
	//
	//	DelaunayCorrection( 5);

	//	continue;


		for ( MGSize in=1; in<=mpGrd->SizeNodeTab(); ++in)
		{
			tabpos[in] = GVect( 0.0);
			tabweight[in] = 0.0;
		}

		Grid<DIM_2D>::ColCell::iterator	itr;
	
		for ( itr = mpGrd->rColCell().begin(); itr != mpGrd->rColCell().end(); ++itr)
		{
			GVect	tabv[GCell::SIZE];
			GVect	tabxv[GCell::SIZE];

			for ( MGSize i=0; i<GCell::SIZE; ++i)
			{
				tabv[i] = mpGrd->cNode( itr->cNodeId(i) ).cPos();
			}


			for ( MGSize inod=0; inod<GCell::SIZE; ++inod)
			{
				MGSize	id = itr->cNodeId(inod);
				const GVect& vc = tabv[inod];

				GMetric met = mData.cMetric( id );

				SMatrix<2> mtx;
				met.EigenDecompose( mtx);

				for ( MGSize i=0; i<GCell::SIZE; ++i)
					met.EigenMultDecomp( tabxv[i], tabv[i] - vc, mtx);

				Simplex<DIM_2D> simp( tabxv[0], tabxv[1], tabxv[2]);

				MGFloat area = simp.Volume();
				GVect vct = simp.Center();

				tabpos[ id ]	+= area * vct;
				tabweight[ id ] += area;

			}

		}

		//for ( MGSize ie=0; ie<tabedg.size(); ++ie)
		//{
		//	GVect v0 = mpGrd->cNode( tabedg[ie].cFirst() ).cPos();
		//	GVect v1 = mpGrd->cNode( tabedg[ie].cSecond() ).cPos();

		//	GVect dv = v1 - v0;



		//	GMetric met0 = mData.cMetric( tabedg[ie].cFirst() );
		//	GMetric met1 = mData.cMetric(  tabedg[ie].cSecond() );

		//	//GMetric met = 0.5 * ( met0 + met1 );
		//	//met.Decompose();

		//	//GVect dvmet;
		//	//met.MultDecomp( dvmet, dv);


		//	//tabpos[ tabedg[ie].cFirst() ]	+= dvmet;
		//	//tabpos[ tabedg[ie].cSecond() ]	-= dvmet;

		//	//tabweight[ tabedg[ie].cFirst() ] += 1.0;
		//	//tabweight[ tabedg[ie].cSecond() ] += 1.0;


		//	met0.Decompose();
		//	met1.Decompose();

		//	GVect dvmet;

		//	met0.MultDecomp( dvmet, dv);
		//	tabpos[ tabedg[ie].cFirst() ]	+= dvmet;

		//	met1.MultDecomp( dvmet, dv);
		//	tabpos[ tabedg[ie].cSecond() ]	-= dvmet;

		//	tabweight[ tabedg[ie].cFirst() ] += 1.0;
		//	tabweight[ tabedg[ie].cSecond() ] += 1.0;

		//}


		for ( MGSize in=nbnod+5; in<=mpGrd->SizeNodeTab(); ++in)
		{
			if ( tabweight[in] > 0.0)
			{
				GMetric met = mData.cMetric( in );
				met.Invert();

				SMatrix<2> mtx;
				met.EigenDecompose( mtx);

				tabpos[in] *= coeff / tabweight[in];

				GVect dvmet;
				met.EigenMultDecomp( dvmet, tabpos[in], mtx);

				tabpos[in] = mpGrd->rNode( in).cPos() + dvmet;
				//mpGrd->rNode( in).rPos() += (coeff / tabweight[in]) * dvmet;
			}
		}

		for ( MGSize in=1; in<=nbnod+4; ++in)
			tabweight[in] = -1.0;

		for ( itr = mpGrd->rColCell().begin(); itr != mpGrd->rColCell().end(); ++itr)
		{
			Simplex<DIM_2D> simp = GridGeom<DIM_2D>::CreateSimplex( *itr, *mpGrd);

			for ( MGSize inod=0; inod<GCell::SIZE; ++inod)
			{
				MGSize id = itr->cNodeId( inod);

				if ( tabweight[id] > 0.0)
				{
					MGFloat a = simp.FaceSmpxVolume( tabpos[ id], inod );

					if ( fabs(a) > 0.8 * simp.Volume() && fabs(a) < 1.2 * simp.Volume()  )
						tabweight[id] = 1.0;
					else
						tabweight[id] = -1.0;
				}
			}
		}


		for ( MGSize in=nbnod+5; in<=mpGrd->SizeNodeTab(); ++in)
		{
			if ( tabweight[in] > 0.0)
				mpGrd->rNode( in).rPos()  = tabpos[ in];
		}
		
		DelaunayCorrection( 5);

	}
}



template<>
void Generator<DIM_2D>::GenerateExp( const MGSize& np)
{
	printf( "\ngenerating %d random points in a cube\n", np);

	ProgressBar	bar(40);

	bar.Init( np);
	bar.Start();

	// create starting box
	GVect	vmin(0,0), vmax(1,1);
	GVect	vct;

	mKernel.InitBoundingBox( vmin, vmax);
	mKernel.InitCube();

	Metric<DIM_2D>	met;
	met.InitIso( 1.0);

	MGFloat dist = 0.0;
	mKernel.InsertPoint( GVect(-dist,-dist) );
	mKernel.InsertPoint( GVect(1+dist,1+dist) );
	mKernel.InsertPoint( GVect(1+dist,-dist) );
	mKernel.InsertPoint( GVect(-dist,1+dist) );


	FlagExternalCells();

	
	GVect ve1, ve2;
	
	
	mKernel.InsertPoint( GVect( 0.65211951048310801, 0.29801324503311261 ) );
	mKernel.InsertPoint( GVect( 0.14255195776238289, 0.47709585863826409 ) );
	mKernel.InsertPoint( GVect( 0.94817957090975680, 0.7048554948576311 ) );
	mKernel.InsertPoint( GVect( 0.27030243842890711, 0.62706381420331436 ) );
	mKernel.InsertPoint( GVect( 0.61765700247199928, 0.44371705374309518 ) );
	mKernel.InsertPoint( GVect( 0.43324076052125615, 0.45283364360484635 ) );
	mKernel.InsertPoint( GVect( 0.41859187597277747, 0.57109286782433544 ) );
	mKernel.InsertPoint( GVect( 0.52497940000610366, 0.57029938657795953 ) );
	mKernel.InsertPoint( GVect( 0.83181249427777948, 0.65016632587664414 ) );
	mKernel.InsertPoint( GVect( 0.54829554124576552, 0.16074098941007722 ) );
	mKernel.InsertPoint( GVect( 0.48591570787682731, 0.86883144627216402 ) );
	mKernel.InsertPoint( GVect( 0.25308999908444474, 0.044618060853907897 ) );
	mKernel.InsertPoint( GVect( 0.15341654713583788, 0.80004272591326642 ) );
	mKernel.InsertPoint( GVect( 0.8776207770012513, 0.86788537247840813 ) );
	mKernel.InsertPoint( GVect( 0.98431348612933744, 0.4183172093874935 ) );
	mKernel.InsertPoint( GVect( 0.80141605883968625, 0.31608020264290293 ) );
	mKernel.InsertPoint( GVect( 0.67711416974394967, 0.82250434888760038 ) );
	mKernel.InsertPoint( GVect( 0.3065279091769158, 0.21875667592394787 ) );
	mKernel.InsertPoint( GVect( 0.30393383587145606, 0.88109988708151499 ) );
	mKernel.InsertPoint( GVect( 0.25666066469313636, 0.49394207586901456 ) );
	mKernel.InsertPoint( GVect( 0.1347697378460036, 0.31989501632740258 ) );
	mKernel.InsertPoint( GVect( 0.37339396343882564, 0.95260475478377638 ) );
	mKernel.InsertPoint( GVect( 0.86764122440260016, 0.18970305490279854 ) );
	mKernel.InsertPoint( GVect( 0.44709616382335887, 0.22821741386150701 ) );

	vector< Key<2> >	tabe;
	MGSize ie1 = 20+4;
	MGSize ie2 = 26+4;
	tabe.push_back( Key<2>( ie1, ie2) );

	ve1 = mpGrd->cNode( ie1).cPos();
	ve2 = mpGrd->cNode( ie2).cPos();

	WriteGrdTEC<DIM_2D> wtec( mpGrd );
	wtec.WriteEdges( "recon_edge_01.dat", tabe);

	GVect vm1 = 0.5 * (ve1 + ve2);
	MGSize im1 = mKernel.InsertPoint( vm1 );

	tabe.clear();
	tabe.push_back( Key<2>( ie1,im1) );
	tabe.push_back( Key<2>( im1,ie2) );

	wtec.WriteEdges( "recon_edge_02.dat", tabe);

	GVect vm2 = 0.5 * (vm1 + ve2);
	MGSize im2 = mKernel.InsertPoint( vm2 );

	GVect vm3 = 0.5 * (vm1 + ve1);
	MGSize im3 = mKernel.InsertPoint( vm3 );


	tabe.clear();
	tabe.push_back( Key<2>( ie2,im2) );
	tabe.push_back( Key<2>( im2,im1) );
	tabe.push_back( Key<2>( im1,im3) );
	tabe.push_back( Key<2>( im3,ie1) );

	wtec.WriteEdges( "recon_edge_03.dat", tabe, "3-EDGES");

	GVect vm4 = 0.5 * (vm2 + ve2);
	MGSize im4 = mKernel.InsertPoint( vm4 );

	tabe.clear();
	//tabe.push_back( Key<2>( ie2,im2) );
	tabe.push_back( Key<2>( im2,im1) );
	tabe.push_back( Key<2>( im1,im3) );
	tabe.push_back( Key<2>( im3,ie1) );

	tabe.push_back( Key<2>( im2,im4) );
	tabe.push_back( Key<2>( im4,ie2) );

	wtec.WriteEdges( "recon_edge_04.dat", tabe, "4-EDGES");

	//mKernel.InsertPoint( GVect( 0.55122531815546127, 0.29407635731070897 ) );
	//mKernel.InsertPoint( GVect( 0.72069460127567364, 0.33268227179784537 ) );
	//mKernel.InsertPoint( GVect( 0.77675710318308056, 0.24570451979125341 ) );
	//mKernel.InsertPoint( GVect( 0.21155430768761255, 0.22409741508224737 ) );
	//mKernel.InsertPoint( GVect( 0.42387157811212500, 0.97262489700003052 ) );
	//mKernel.InsertPoint( GVect( 0.12256233405560472, 0.82753990295113988 ) );
	//mKernel.InsertPoint( GVect( 0.017914365062410353, 0.84331797235023043 ) );
	//mKernel.InsertPoint( GVect( 0.17633594775231179, 0.80751976073488574 ) );
	//mKernel.InsertPoint( GVect( 0.49256874294259467, 0.19483016449476609 ) );
	//mKernel.InsertPoint( GVect( 0.96670430616168701, 0.92074343089083532 ) );
	//mKernel.InsertPoint( GVect( 0.19406720175786615, 0.48664815210425122 ) );
	//mKernel.InsertPoint( GVect( 0.72167119357890563, 0.26914273506881925 ) );
	//mKernel.InsertPoint( GVect( 0.98925748466444896, 0.91769157994323558 ) );
	//mKernel.InsertPoint( GVect( 0.48509170812097535, 0.50245674001281782 ) );
	//mKernel.InsertPoint( GVect( 0.0048829615161595508, 0.7831049531540879 ) );
	//mKernel.InsertPoint( GVect( 0.44523453474532304, 0.5169530320139164 ) );



	//srand((unsigned)time(NULL));
	//for ( MGSize i=0; i<np; ++i, ++bar)
	//{
	//	vct.rX() = ((MGFloat)::rand()) / ((MGFloat)RAND_MAX);
	//	vct.rY() = ((MGFloat)::rand()) / ((MGFloat)RAND_MAX);


	//	mKernel.InsertPoint( vct );
	//}

	//bar.Finish();
	

	RemoveExternalCells();
}


template<>
void Generator<DIM_2D>::GenerateTestCube( const MGSize& np)
{
	printf( "\ngenerating %d random points in a cube\n", np);

	ProgressBar	bar(40);

	bar.Init( np);
	bar.Start();

	// create starting box
	GVect	vmin(0,0), vmax(1,1);
	GVect	vct;

	mKernel.InitBoundingBox( vmin, vmax);

	mKernel.InitCube();

	// insert points

	Metric<DIM_2D>	met;
	met.InitIso( 1.0);

	MGFloat dist = 0.0;
	mKernel.InsertPoint( GVect(-dist,-dist) );
	mKernel.InsertPoint( GVect(1+dist,1+dist) );
	mKernel.InsertPoint( GVect(1+dist,-dist) );
	mKernel.InsertPoint( GVect(-dist,1+dist) );

	FlagExternalCells();

	srand((unsigned)time(NULL));
	for ( MGSize i=0; i<np; ++i, ++bar)
	{
		vct.rX() = ((MGFloat)::rand()) / ((MGFloat)RAND_MAX);
		vct.rY() = ((MGFloat)::rand()) / ((MGFloat)RAND_MAX);


		mKernel.InsertPoint( vct );
	}

	bar.Finish();
	

	RemoveExternalCells();
}



template<>
void Generator<DIM_2D>::GenerateTestSphere( const MGSize& np)
{
	printf( "\ngenerating %d random points in a sphere\n", np);

	ProgressBar	bar(40);

	bar.Init( np);
	bar.Start();


	// create starting box
	GVect	vmin(0,0), vmax(1,1);
	GVect	vct;

	Metric<DIM_2D>	met;
	met.InitIso( 1.0);

	mKernel.InitBoundingBox( vmin, vmax);

	mKernel.InitCube();


	// insert points

	for ( MGSize i=0; i<np; ++i, ++bar)
	{
		//const MGFloat fi  = (M_PI * (MGFloat)::rand()) / ((MGFloat)RAND_MAX) - M_PI_2;
		const MGFloat lam = (2*M_PI * (MGFloat)::rand()) / ((MGFloat)RAND_MAX) - M_PI;

		vct.rX() = cos(lam);
		vct.rY() = sin(lam);

		mKernel.InsertPoint( vct );
	}

	bar.Finish();

	FlagExternalCells();
	RemoveExternalCells();

	// export
	//mpGrd->CheckConnectivity();
	//WriteTEC<DIM_2D> wtec(mpGrd);
	//wtec.DoWrite( "_initial.dat");
}






template <>
void Generator<DIM_2D>::Process()
{

	BndGrid<DIM_2D> &bnd = *mpBndGrd;


	ProgressBar	bar(40);


	bar.Init( bnd.SizeNodeTab() );
	//bar.Start();


	GVect	vmin, vmax;
	GVect	vct;


	bnd.FindMinMax( vmin, vmax);
	
	mKernel.InitBoundingBox( vmin, vmax);
	mKernel.InitCube();


	/////////////////////////////////////////////////////////////
	// setting metric for box corners to dummy values

	GMetric metav = mpCSpace->GetSpacing( bnd.cNode(1).cPos());
	metav.Invert();
	
	bar.Start();
	for ( MGSize i=2; i<bnd.SizeNodeTab()+1; ++i, ++bar)
	{
		vct = bnd.cNode(i).cPos();
		GMetric met = mpCSpace->GetSpacing( vct);
		met.Invert();
		metav += met;
	}
	bar.Finish();

	metav /= (MGFloat)( bnd.SizeNodeTab() );
	metav.Invert();


	GMetric metdef;
	metdef.InitIso( 1.0);
	
	for ( MGSize i=1; i<=mpGrd->SizeNodeTab(); ++i)
		mData.InsertMetric( metdef, i);


	//metdef = metav;

	/////////////////////////////////////////////////////////////
	// check the grid (box)
	mpGrd->CheckConnectivity();

	WriteGrdTEC<DIM_2D> wtec(mpGrd);

	wtec.DoWrite( "box.plt");



	/////////////////////////////////////////////////////////////
	// insert bundary points

	vector<MGSize>	tabid(bnd.SizeNodeTab());
	for ( MGSize i=1; i<bnd.SizeNodeTab()+1; ++i)
		tabid[i-1] = i;

	random_shuffle( tabid.begin(), tabid.end() );




	bar.Start();
	for ( MGSize ii=0; ii<tabid.size(); ++ii, ++bar)
	{
		MGSize i = tabid[ii];

		vct = bnd.cNode(i).cPos();


		//MGSize inod = mKernel.InsertPoint( vct );
		MGSize inod = mKernel.InsertPointMetric( vct, metdef);

		if ( ! inod)
		{
			char sbuf[512];
			sprintf( sbuf, "CRASH :: kernel failed to insert new point with coordinates [%lg %lg]", 
							vct.cX(), vct.cY() );

			THROW_INTERNAL( sbuf);
		}

		//if ( mpCSpace)
			mData.InsertMetric( mpCSpace->GetSpacing( vct), inod );

		mpGrd->rNode(inod).rMasterId() = bnd.cNode(i).cMasterId();
		bnd.rNode(i).rGrdId() = inod;


		//wtec.DoWrite( "_tmp_grd.plt");


		//fprintf( f, "node id = %d\n", i);
		//smtx = SMatrix<MGFloat,3>( bnd.cNode(i).cMetric());
		//smtx.Decompose( smtxD, smtxL);
		//smtxD.Write( f);
		//smtxL.Write( f);
	}
	
	bar.Finish();



	///////////////////////////////////////////////////////////////
	//// setting metric for boundary nodes
	//for ( MGSize ii=5; ii<=mpGrd->SizeNodeTab(); ++ii)
	//{
	//	vct = mpGrd->cNode(ii).cPos();
	//	//mtabMetric[ii] = mpCSpace->GetSpacing( vct);
	//	mData.InsertMetric( mpCSpace->GetSpacing( vct), ii );
	//}

	////bnd.FindSpacing( mtabSpac);	// ???



	wtec.DoWrite( "_bef_rec_grd.plt");

	/////////////////////////////////////////////////////////////
	// recover boundary
	Reconstructor<DIM_2D>	recon( bnd, mKernel);

	recon.Init();
	recon.Reconstruct();

	wtec.DoWrite( "_bef_extern_grd.plt");



	/////////////////////////////////////////////////////////////
	// flag external cells
	FlagExternalCells( bnd);

	wtec.DoWrite( "_initial.dat");

	//wtec.DoWrite( "_bef_init_opt.plt");
	//Optimization( 30);
	//wtec.DoWrite( "_aft_init_opt.plt");


	// HACK ::
	//TriangulateFront();
	//wtec.DoWrite( "_front.dat");

	/////////////////////////////////////////////////////////////
	// start triangulation
	Triangulate( &recon.rTabBnSubEdges() );


	//SmoothVariation();
	//wtec.DoWrite( "_initial_smoothvar.dat");
	
	const MGSize nIter = this->ConfigSect().ValueSize( ConfigStr::Master::Generator::GEN_NITERDELAUNAY );
	DelaunayCorrection( nIter);

	//Smooth();

	wtec.DoWrite( "_bef_rm_ext.plt");


	/////////////////////////////////////////////////////////////
	// remove external cells
	RemoveExternalCells();


	wtec.DoWrite( "_aft_rm_ext.dat");

	mpGrd->CheckQuality();
	mpGrd->CheckConnectivity();

}


struct FrontNode
{
	MGSize	mMasterId;
	MGInt	mFaceId;
	Vect2D	mPos;
	Vect2D	mVn;
};




template <>
void Generator<DIM_2D>::TriangulateFront()
{
	const MGSize	maxLay = 15;
	const MGFloat	treshAR = 1.5;
	const MGFloat	maxCos = 0.8;

	ofstream of("front_dump.txt");

	vector< Key<DIM_2D> >	tabFCell;

	multimap< MGSize, FrontNode>	mapFNode;
	multimap< MGSize, FrontNode>::iterator	itrn;

	mpBndGrd->FindFaces( tabFCell, 1);


		cout << "tabFCell.size = " << tabFCell.size() << endl;
		of << "tabFCell.size = " << tabFCell.size() << endl;

		for ( MGSize in=0; in<tabFCell.size(); ++in)
		{
			MGSize i0 = tabFCell[in].cElem( 0);
			MGSize i1 = tabFCell[in].cElem( 1);

			Vect2D vg0 = mpGrd->cNode( i0 ).cPos();
			Vect2D vg1 = mpGrd->cNode( i1 ).cPos();

			of << "fface " << in << " [ " << i0 << " " << i1 << " ]" << endl;
			
			Vect2D vt = vg1 - vg0;
			Vect2D vn = Vect2D( -vt.cY(), vt.cX() ).versor();

			if ( (itrn = mapFNode.find( i0)) == mapFNode.end() )
			{
				FrontNode fnode;
				fnode.mMasterId = i0;
				fnode.mFaceId = in;
				fnode.mPos = vg0;
				fnode.mVn = vn;

				mapFNode.insert( multimap< MGSize, FrontNode>::value_type( i0, fnode) );
			}
			else
			{
				if ( itrn->second.mVn.versor() * vn.versor() > maxCos )
				{
					itrn->second.mVn += vn;
					itrn->second.mFaceId = -1;
				}
				else
				{
					FrontNode fnode;
					fnode.mMasterId = i0;
					fnode.mFaceId = in;
					fnode.mPos = vg0;
					fnode.mVn = vn;

					mapFNode.insert( multimap< MGSize, FrontNode>::value_type( i0, fnode) );
				}
			}


			if ( (itrn = mapFNode.find( i1)) == mapFNode.end() )
			{
				FrontNode fnode;
				fnode.mMasterId = i1;
				fnode.mFaceId = in;
				fnode.mPos = vg1;
				fnode.mVn = vn;

				mapFNode.insert( multimap< MGSize, FrontNode>::value_type( i1, fnode) );
			}
			else
			{
				if ( itrn->second.mVn.versor() * vn.versor() > maxCos )
				{
					itrn->second.mVn += vn;
					itrn->second.mFaceId = -1;
				}
				else
				{
					FrontNode fnode;
					fnode.mMasterId = i1;
					fnode.mFaceId = in;
					fnode.mPos = vg1;
					fnode.mVn = vn;

					mapFNode.insert( multimap< MGSize, FrontNode>::value_type( i1, fnode) );
				}
			}

		}


		//for ( itrn=mapFNode.begin(); itrn!=mapFNode.end(); ++itrn)
		//{
		//	of << itrn->first << " ";
		//	of << " vpos = (" << itrn->second.mPos.cX() << ", " << itrn->second.mPos.cX() << " )";
		//	of << " vn = (" << itrn->second.mVn.cX() << ", " << itrn->second.mVn.cX() << " )";
		//	of << endl;
		//}


		multimap< MGSize, pair<MGSize,MGInt> > mapMtoGrd;

		cout << "INSERTING FRONT" << endl;

	for ( MGSize ilay=0; ilay<maxLay; ++ilay)
	{

		for ( itrn=mapFNode.begin(); itrn!=mapFNode.end(); ++itrn)
		{


			Metric<DIM_2D> met = mData.cMetric( itrn->first );

			Vect2D vn = itrn->second.mVn.versor();
			Vect2D vt = Vect2D( vn.cY(), -vn.cX() );

			MGFloat h = ::sqrt( 1.0 / met.Distance( vn ) );
			MGFloat t = ::sqrt( 1.0 / met.Distance( vt ) );

			Vect2D vct = itrn->second.mPos + itrn->second.mVn.versor() * h;

			Metric<DIM_2D> meth =  mpCSpace->GetSpacing( vct);

			h = 0.5*( h + ::sqrt( 1.0 / meth.Distance( vn ) ) );
			t = 0.5*( t + ::sqrt( 1.0 / meth.Distance( vt ) ) );

			vct = itrn->second.mPos + itrn->second.mVn.versor() * h;

			MGFloat ar = t / h;
			ar = ar > 1.0 ? ar : 1.0/ar ;

			//cout << h << endl;

			if ( ar > treshAR )
			{


				MGSize inod = mKernel.InsertPoint( vct );

				if ( ! inod)
				{
					cout << "masterid = " << itrn->first << endl;
				
					char sbuf[512];
					sprintf( sbuf, "CRASH :: kernel failed to insert new point with coordinates [%lg %lg]", 
									vct.cX(), vct.cY() );

					THROW_INTERNAL( sbuf);
				}

				mData.InsertMetric( mpCSpace->GetSpacing( vct), inod );

				itrn->second.mPos = vct;
				itrn->second.mMasterId = inod;

			}
		}

		cout << "finished INSERTING FRONT" << endl;

		//mapBack = mapFNode;
		//mapFNode.clear();


	}

}

/*
template <>
void Generator<DIM_2D>::TriangulateFront()
{
	const MGSize	maxLay = 25;
	const MGFloat	treshAR = 4;
	const MGFloat	maxCos = 0.8;

	ofstream of("front_dump.txt");

	vector< Key<DIM_2D> >	tabFCell;

	multimap< MGSize, FrontNode>	mapFNode;
	multimap< MGSize, FrontNode>::iterator	itrn;

	mpBndGrd->FindFaces( tabFCell, 1);


	for ( MGSize ilay=0; ilay<maxLay; ++ilay)
	{
		cout << "tabFCell.size = " << tabFCell.size() << endl;
		of << "tabFCell.size = " << tabFCell.size() << endl;

		for ( MGSize in=0; in<tabFCell.size(); ++in)
		{
			MGSize i0 = tabFCell[in].cElem( 0);
			MGSize i1 = tabFCell[in].cElem( 1);

			Vect2D vg0 = mpGrd->cNode( i0 ).cPos();
			Vect2D vg1 = mpGrd->cNode( i1 ).cPos();

			of << "fface " << in << " [ " << i0 << " " << i1 << " ]" << endl;
			
			Vect2D vt = vg1 - vg0;
			Vect2D vn = Vect2D( -vt.cY(), vt.cX() ).versor();

			if ( (itrn = mapFNode.find( i0)) == mapFNode.end() )
			{
				FrontNode fnode;
				fnode.mMasterId = i0;
				fnode.mFaceId = in;
				fnode.mPos = vg0;
				fnode.mVn = vn;

				mapFNode.insert( multimap< MGSize, FrontNode>::value_type( i0, fnode) );
			}
			else
			{
				if ( itrn->second.mVn.versor() * vn.versor() > maxCos )
				{
					itrn->second.mVn += vn;
					itrn->second.mFaceId = -1;
				}
				else
				{
					FrontNode fnode;
					fnode.mMasterId = i0;
					fnode.mFaceId = in;
					fnode.mPos = vg0;
					fnode.mVn = vn;

					mapFNode.insert( multimap< MGSize, FrontNode>::value_type( i0, fnode) );
				}
			}


			if ( (itrn = mapFNode.find( i1)) == mapFNode.end() )
			{
				FrontNode fnode;
				fnode.mMasterId = i1;
				fnode.mFaceId = in;
				fnode.mPos = vg1;
				fnode.mVn = vn;

				mapFNode.insert( multimap< MGSize, FrontNode>::value_type( i1, fnode) );
			}
			else
			{
				if ( itrn->second.mVn.versor() * vn.versor() > maxCos )
				{
					itrn->second.mVn += vn;
					itrn->second.mFaceId = -1;
				}
				else
				{
					FrontNode fnode;
					fnode.mMasterId = i1;
					fnode.mFaceId = in;
					fnode.mPos = vg1;
					fnode.mVn = vn;

					mapFNode.insert( multimap< MGSize, FrontNode>::value_type( i1, fnode) );
				}
			}

		}


		//for ( itrn=mapFNode.begin(); itrn!=mapFNode.end(); ++itrn)
		//{
		//	of << itrn->first << " ";
		//	of << " vpos = (" << itrn->second.mPos.cX() << ", " << itrn->second.mPos.cX() << " )";
		//	of << " vn = (" << itrn->second.mVn.cX() << ", " << itrn->second.mVn.cX() << " )";
		//	of << endl;
		//}


		multimap< MGSize, pair<MGSize,MGInt> > mapMtoGrd;

		cout << "INSERTING FRONT" << endl;

		for ( itrn=mapFNode.begin(); itrn!=mapFNode.end(); ++itrn)
		{


			Metric<MGFloat,DIM_2D> met = mData.cMetric( itrn->first );

			Vect2D vn = itrn->second.mVn.versor();
			Vect2D vt = Vect2D( vn.cY(), -vn.cX() );

			MGFloat h = ::sqrt( 1.0 / met.Distance( vn ) );
			MGFloat t = ::sqrt( 1.0 / met.Distance( vt ) );
			MGFloat ar = t / h;
			ar = ar > 1.0 ? ar : 1.0/ar ;

			//cout << h << endl;

			if ( ar > treshAR )
			{
				Vect2D vct = itrn->second.mPos + itrn->second.mVn.versor() * h;


				MGSize inod = mKernel.InsertPoint( vct );

				if ( ! inod)
				{
					cout << "masterid = " << itrn->first << endl;
				
					char sbuf[512];
					sprintf( sbuf, "CRASH :: kernel failed to insert new point with coordinates [%lg %lg]", 
									vct.cX(), vct.cY() );

					THROW_INTERNAL( sbuf);
				}

				mData.InsertMetric( mpCSpace->GetSpacing( vct), inod );

				mapMtoGrd.insert( multimap< MGSize, pair<MGSize,MGInt> >::value_type( itrn->first, pair<MGSize,MGInt>( inod, itrn->second.mFaceId ) ) );
			}
		}

		cout << "finished INSERTING FRONT" << endl;

		//mapBack = mapFNode;
		//mapFNode.clear();

		vector< Key<DIM_2D> >	tabFCellNew;

		multimap< MGSize, pair<MGSize,MGInt> >::const_iterator itrm;

		for ( MGSize ifc=0; ifc<tabFCell.size(); ++ifc)
		{
			MGInt id0 = -1;
			MGInt id1 = -1;

			pair< multimap< MGSize, pair<MGSize,MGInt> >::const_iterator, multimap< MGSize, pair<MGSize,MGInt> >::const_iterator> range;

			range = mapMtoGrd.equal_range( tabFCell[ifc].cFirst() );
			for ( itrm=range.first; itrm!=range.second; ++itrm)
				if ( itrm->second.second == ifc || itrm->second.second == -1)
				{
					id0 = itrm->second.first;
				}


			range = mapMtoGrd.equal_range( tabFCell[ifc].cSecond() );
			for ( itrm=range.first; itrm!=range.second; ++itrm)
				if ( itrm->second.second == ifc || itrm->second.second == -1)
				{
					id1 = itrm->second.first;
				}

			if ( id0 >= 0 && id1 >= 0 )
				tabFCellNew.push_back( Key<DIM_2D>( static_cast<MGSize>( id0), static_cast<MGSize>( id1) ) );

		}

		tabFCell.swap( tabFCellNew);
		tabFCellNew.clear();
		mapFNode.clear();

	}

}
*/


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

