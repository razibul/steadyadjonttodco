#include "generator.h"
#include "libgreen/controlspace.h"
#include "libgreen/bndgrid.h"
#include "libgreen/grid.h"
#include "libgreen/writegrdtec.h"

#include "libgreen/gridgeom.h"
#include "libcorecommon/interpol.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




//template <>
void Generator<DIM_1D>::Smooth()
{
	// create mapping function (cs length -> t param) and then regenerate points with spacing uniform according to cspace

	vector< pair<MGFloat,MGSize> >	tabT;
	vector<MGFloat>	tabD, tabS;

	for ( MGSize in=1; in<=mpGrd->SizeNodeTab(); ++in)
		tabT.push_back( pair<MGFloat,MGSize>( mpGrd->cNode(in).cPos().cX(), in ) );

	if ( mpGrd->mBndGrid.cNode1().cPos().cX() < mpGrd->mBndGrid.cNode2().cPos().cX() )
		sort( tabT.begin(), tabT.end(), less< pair<MGFloat,MGSize> >() );
	else
		sort( tabT.begin(), tabT.end(), greater< pair<MGFloat,MGSize> >() );

	tabS.resize( tabT.size() );
	tabD.resize( tabT.size() );
	tabS[0] = tabT[0].first;
	tabD[0] = 0.0;

	for ( MGSize i=1; i<tabT.size(); ++i)
	{
		GVect vct = GVect( tabT[i].first - tabT[i-1].first );

		MGFloat	d1 = ::sqrt( mData.cMetric( tabT[i].second ).Distance( vct) );
		MGFloat	d2 = ::sqrt( mData.cMetric( tabT[i-1].second ).Distance( vct) );

		tabD[i] = 0.5 * (d1 + d2) + tabD[i-1];
		tabS[i] = tabT[i].first;
	}

	Spline	splin;
	splin.UsePoints( tabS.size(), &tabD, &tabS);
	splin.CalcCoeff();

	//cout << "tabD[tabD.size()-1] = " << tabD[tabD.size()-1] << endl;


	// find number of points in the grid after redistribution

	MGSize np = static_cast<MGSize>( tabD[tabD.size()-1] + 0.5 ) + 1; // ??? 
	np = np>2 ? np : 2;

	//MGSize np = static_cast<MGSize>( tabD[tabD.size()-1] ) + 2;
	MGFloat h = tabD[tabD.size()-1] / (np - 1);

	//cout << "np = " << np << " h = " << h  << endl;


	// recreating the grid

	mpGrd->rColCell().clear();
	mpGrd->rColNode().clear();

	MGFloat t = 0.0;
	MGSize id, idp;
	GCell	cell;

	id = mpGrd->InsertNode( GNode( tabS[0] ) );
	mpGrd->rNode(id).rMasterId() = mpGrd->mBndGrid.cNode1().cMasterId();
	mpGrd->mBndGrid.rNode1().rGrdId() = id;

	id = mpGrd->InsertNode( GNode( tabS[tabS.size()-1] ) );
	mpGrd->rNode(id).rMasterId() = mpGrd->mBndGrid.cNode2().cMasterId();
	mpGrd->mBndGrid.rNode2().rGrdId() = id;

	//if ( np == 2)
	//{
	//	cell.rNodeId( 0) = 1;
	//	cell.rNodeId( 1) = 2;

	//	mpGrd->InsertCell( cell);
	//	return;
	//}


	idp = 1;
	for ( MGSize i=1; i<np-1; ++i)
	{
		t += h;
		MGFloat s = splin.Fun( t);
		id = mpGrd->InsertNode( GNode( s ) );

		cell.rNodeId( 0) = idp;
		cell.rNodeId( 1) = id;

		mpGrd->InsertCell( cell);
		idp = id;
	}

	cell.rNodeId( 0) = idp;
	cell.rNodeId( 1) = 2;

	mpGrd->InsertCell( cell);

	//WriteGrdTEC<DIM_1D> wtec(mpGrd);
	//wtec.DoWrite( "_initial_after.dat");
}


//template <>
void Generator<DIM_1D>::Triangulate()
{
	WriteGrdTEC<DIM_1D> wtec(mpGrd);

	bool bOk;

	do
	{
		bOk = true;

		for ( MGSize ic=1; ic<=mpGrd->SizeCellTab(); ++ic)
		{
			const GCell &cell = mpGrd->cCell( ic);
			const MGSize in1 = cell.cNodeId(0);
			const MGSize in2 = cell.cNodeId(1);

			Simplex<DIM_1D> simp = GridGeom<DIM_1D>::CreateSimplex( cell, *mpGrd);

			GMetric metav = 0.5 * ( mData.cMetric( in1) + mData.cMetric( in2) );
			GVect vdist = mpGrd->cNode(in2).cPos() - mpGrd->cNode(in1).cPos();

			MGFloat av = ::sqrt( metav.Distance( vdist) );

			if ( av > 1.0 )
			{
				GVect vct =  0.5*(mpGrd->cNode(in1).cPos() + mpGrd->cNode(in2).cPos() );
				GMetric met = mpCSpace->GetSpacing( vct);

				//MGSize inod = mKernel.InsertPoint( vct, 1, met);
				MGSize inod = mKernel.InsertPoint( vct, ic, met);

				mData.InsertMetric( met, inod);
				//mtabMetric.resize( mpGrd->cColNode().size() );
				//mtabMetric[inod] = met;

				bOk = false;
			}
		}

		//MGFloat	length = 0.0;
		//for ( MGSize ic=1; ic<=mpGrd->SizeCellTab(); ++ic)
		//{
		//	const GCell &cell = mpGrd->cCell( ic);
		//	const MGSize in1 = cell.cNodeId(0);
		//	const MGSize in2 = cell.cNodeId(1);

		//	GVect vdist = mpGrd->cNode(in2).cPos() - mpGrd->cNode(in1).cPos();

		//	MGFloat	d1 = ::sqrt( mtabMetric[in1].Distance( vdist) );
		//	MGFloat	d2 = ::sqrt( mtabMetric[in2].Distance( vdist) );

		//	length += 0.5 * (d1 + d2);
		//}

		//cout << "length = " << length << " np = " << mpGrd->SizeNodeTab() << endl;

	}
	while ( ! bOk);

	//mpGrd->UpdateNodeIds();
	//wtec.DoWrite( "_initial.dat");

}


//template <>
void Generator<DIM_1D>::Process()
{
	BndGrid<DIM_1D> &bnd = *mpBndGrd;
	//ProgressBar	bar(40);
	//bar.Init( bnd.SizeNodeTab() );
	//bar.Start();

	GVect	vmin, vmax;

	bnd.FindMinMax( vmin, vmax);
	
	mKernel.InitCube( bnd.cNode1().cPos(), bnd.cNode2().cPos() );

	mpGrd->rNode(1).rMasterId() = bnd.cNode1().cMasterId();
	mpGrd->rNode(2).rMasterId() = bnd.cNode2().cMasterId();


	//mpGrd->rNode(1).rBndId() = bnd.cNode1().cBndId();
	//mpGrd->rNode(2).rBndId() = bnd.cNode2().cBndId();

	//mpGrd->InsertConn( 1, 1);
	//mpGrd->InsertConn( 2, 2);

	mData.InsertMetric( mpCSpace->GetSpacing( bnd.cNode1().cPos() ), 1);
	mData.InsertMetric( mpCSpace->GetSpacing( bnd.cNode2().cPos() ), 2);
	//mtabMetric.resize( mpGrd->cColNode().size() );
	//mtabMetric[1] = mpCSpace->GetSpacing( bnd.cNode1().cPos() );
	//mtabMetric[2] = mpCSpace->GetSpacing( bnd.cNode2().cPos() );

	//mpGrd->CheckConnectivity();


	//mpGrd->UpdateNodeIds();
	//wtec.DoWrite( "box.plt");

	//bar.Finish();

	//bnd.FindSpacing( mtabSpac);

	Triangulate();
	Smooth();

}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

