#ifndef __RECONSTRUCTOR_H__
#define __RECONSTRUCTOR_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libgreen/edgesplit.h"
#include "libgreen/facesplit.h"

#include "libgreen/protectentities.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class ControlSpace;

template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class BndGrid;

template <Dimension DIM> 
class Kernel;

template <Dimension DIM> 
class EdgePipe;


//////////////////////////////////////////////////////////////////////
// class Reconstructor
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Reconstructor
{
};

//////////////////////////////////////////////////////////////////////
// class Reconstructor
//////////////////////////////////////////////////////////////////////
template <>
class Reconstructor<DIM_2D>
{
	typedef TDefs<DIM_2D>::GVect	GVect;
	typedef TDefs<DIM_2D>::GMetric	GMetric;

	typedef TDefs<DIM_2D>::GNode	GNode;
	typedef TDefs<DIM_2D>::GCell	GCell;
	typedef TDefs<DIM_2D>::GFace	GFace;


public:
	Reconstructor( const BndGrid<DIM_2D>& bgrd, Kernel<DIM_2D>& kernel) 
		: mpBndGrd(&bgrd), mpKernel(&kernel)		{}

	void	Init( const bool& bout=true);
	void	Reconstruct( const bool& bout=true);

	vector< Key<2> >&	rTabBnSubEdges()	{ return mtabBndFaces;}

protected:
	void	FindMissingFaces( const bool& bout);
	
private:
	const BndGrid<DIM_2D>		*mpBndGrd;

	Kernel<DIM_2D>		*mpKernel;

	vector< Key<2> >	mtabBndFaces;
	vector< Key<2> >	mtabMissing;
};
//////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////
// class Reconstructor
//////////////////////////////////////////////////////////////////////
template <>
class Reconstructor<DIM_3D>
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;


public:
	Reconstructor( const BndGrid<DIM_3D>& bgrd, const ControlSpace<DIM_3D>& cspace, Kernel<DIM_3D>& kernel) 
		: mpBndGrd(&bgrd), mpCSpace(&cspace), mpKernel(&kernel)		{}

	void	Init();
	void	Reconstruct();
	void	FillBFaceTab( vector< Key<3> >& tabf);

protected:
	void	InitEdges();
	void	InitFaces();

	bool	IsEdgeRecovered( const Key<2>& edge);
	MGSize	ReconstructSplitEdges();
	MGSize	ReconstructSplitFaces();

	void	FindMissingEdges();
	void	FindMissingFaces();

	void	RemoveProtection( EdgeSplit* pedge, const Key<2>& sedge);
	
	//MGSize	RecTrivial();
	//MGSize	RecSimple();
	//MGSize	RecSimpleSteiner();

	//bool	RecSimplePipe( EdgePipe<DIM_2D>& pipe );

private:
	const BndGrid<DIM_3D>		*mpBndGrd;
	const ControlSpace<DIM_3D>	*mpCSpace;
	Kernel<DIM_3D>				*mpKernel;

	ProtectEntities				mPrEntities;

	// edges
	vector< Key<2> >			mtabBndEdges;
	vector<EdgeSplit>			mtabSplitEdges;
	vector<MGSize>				mtabRecEdges;		// id of the face in mtabSplitEdges

	//faces
	vector< Key<3> >			mtabBndFaces;
	vector<FaceSplit>			mtabSplitFaces;
	vector<MGSize>				mtabRecFaces;		// id of the face in mtabSplitFaces

	// ???
	set< Key<2> >				msetProEdges;
	set< Key<3> >				msetProFaces;
	map< Key<2>, MGSize>	mmapSplitEdge;
	map< Key<3>, MGSize>	mmapSplitFace;

	//multimap< Key<2>, Key<2> >

};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RECONSTRUCTOR_H__

