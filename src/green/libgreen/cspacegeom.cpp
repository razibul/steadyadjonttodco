#include "cspacegeom.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 





//////////////////////////////////////////////////////////////////////
// 1D metric

// in 2D

template <>
inline TDefs<DIM_1D>::GMetric GeomControlSpace<DIM_1D,DIM_2D>::GetSpacing( const GVect& vct) const
{
	UVect tab[DIM_1D];

	mpGEnt->GetFstDeriv( tab, vct);
	Vect3D	vfst( tab[0].cX(),tab[0].cY(), 0 );

	MGFloat d = vfst.module();
	MGFloat espac = d * d;

	GMetric	met;

	met[0] = espac;

	return met;
}


// in 3D

template <>
inline TDefs<DIM_1D>::GMetric GeomControlSpace<DIM_1D,DIM_3D>::GetSpacing( const GVect& vct) const
{
	UVect tab[DIM_1D];

	mpGEnt->GetFstDeriv( tab, vct);
	Vect3D	vfst( tab[0].cX(), tab[0].cY(), tab[0].cZ() );

	MGFloat d = vfst.module();
	MGFloat espac = d * d;

	GMetric	met;

	met[0] = espac;

	return met;
}




//////////////////////////////////////////////////////////////////////
// 2D metric

template <>
inline TDefs<DIM_2D>::GMetric GeomControlSpace<DIM_2D,DIM_2D>::GetSpacing( const GVect& vct) const
{
	return GMetric();
}


template <>
inline TDefs<DIM_2D>::GMetric GeomControlSpace<DIM_2D,DIM_3D>::GetSpacing( const GVect& vct) const
{
	UVect tab[DIM_2D];

	/////////////////////////////////////////////////////////////
	// find first fundamental form
	mpGEnt->GetFstDeriv( tab, vct);

	if ( tab[0].module() < ZERO)
		tab[0].rX() = tab[0].rY() = tab[0].rZ() = ZERO;//1.0e-6;

	if ( tab[1].module() < ZERO)
		tab[1].rX() = tab[1].rY() = tab[1].rZ() = ZERO;//1.0e-6;


	GMetric	metspac;

	metspac[0] = metspac[0] = tab[0]*tab[0];
	metspac[1] = 0.0;			// TODO :: check this !!!!!!
	//metspac[1] = tab[0]*tab[1];
	metspac[2] = metspac[2] = tab[1]*tab[1];


	return metspac;
}




template class GeomControlSpace<DIM_1D,DIM_2D>;
template class GeomControlSpace<DIM_2D,DIM_2D>;

template class GeomControlSpace<DIM_1D,DIM_3D>;
template class GeomControlSpace<DIM_2D,DIM_3D>;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

