#include "readmeandr.h"
#include "libgreen/grid.h"
#include "libcoreio/store.h"
#include "libgreen/kernel.h"
#include "libgreen/generatordata.h"
#include "libcorecommon/progressbar.h"
#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Dimension DIM>
void ReadMEANDR<DIM>::DoRead( const MGString& fname)
{
	ifstream file;


	try
	{
		MGString fnameNode = fname + MGString(".node");

		cout << "Reading MEANDR '" << fnameNode << "'" << endl;//std::flush;

		ProgressBar	bar(40);

		/////////////////////////////////////////////////////////////
		// read node file

		file.open( fnameNode.c_str(), ios::in );

		if ( ! file)
			THROW_FILE( "can not open the file", fnameNode);

		istringstream is;

		MGSize	n, id;
		vector< Vect<DIM> > tabnode;
		Vect<DIM>	vmax, vmin;

		IO::ReadLine( file, is);

		is >> n;

		tabnode.resize(n);

		bar.Init( n );
		bar.Start();
		for ( MGSize i=0; i<n; ++i, ++bar)
		{
			IO::ReadLine( file, is);
			is >> id;
			for ( MGSize k=0; k<DIM; ++k)
				is >> tabnode[i].rX(k);

			if ( i == 0)
				vmin = vmax = tabnode[i];
			else
				for ( MGSize j=0; j<DIM; ++j)
				{
					if ( vmin.cX(j) > tabnode[i].cX(j) )
						vmin.rX(j) = tabnode[i].cX(j);

					if ( vmax.cX(j) < tabnode[i].cX(j) )
						vmax.rX(j) = tabnode[i].cX(j);
				}


			if ( id != i+1)
				THROW_FILE( "index mismatched", fnameNode);
	
		}
		bar.Finish();

		file.close();

		/////////////////////////////////////////////////////////////
		// read metric file
		MGString fnameMetric = fname + MGString(".metric");

		file.open( fnameMetric.c_str(), ios::in );

		if ( ! file)
			THROW_FILE( "can not open the file", fnameNode);

		MGSize	m;
		vector< Metric<DIM> > tabmetric;

		IO::ReadLine( file, is);

		is >> m;

		if ( m != n)
			THROW_INTERNAL( "metric and node files are not compatible!");

		tabmetric.resize(m);

		bar.Init( m );
		bar.Start();
		for ( MGSize i=0; i<m; ++i, ++bar)
		{
			IO::ReadLine( file, is);
			is >> id;

			if ( DIM == DIM_2D)
			{
				for ( MGSize k=0; k<Metric<DIM>::SIZE; ++k)
					is >> (tabmetric[i])[k];

				//tabmetric[i] *= 10000000.0;
			}
			else
			if ( DIM == DIM_3D)
			{
				// 0 1 2	0 1 3
				// 1 3 4	1 2 4
				// 2 4 5	3 4 5

				is >> (tabmetric[i])[0];
				is >> (tabmetric[i])[1];
				is >> (tabmetric[i])[3];
				is >> (tabmetric[i])[2];
				is >> (tabmetric[i])[4];
				is >> (tabmetric[i])[5];

				//(tabmetric[i])[0] = (tabmetric[i])[2] = (tabmetric[i])[5] = 40000;
				//(tabmetric[i])[1] = (tabmetric[i])[3] = (tabmetric[i])[4] = 0;

				//for ( MGSize k=0; k<Metric<MGFloat,DIM>::SIZE; ++k)
				//	is >> (tabmetric[i])[k];
			}
			else
				THROW_INTERNAL( "unknown DIM");

			if ( id != i+1)
				THROW_FILE( "index mismatched", fnameNode);
	
		}
		bar.Finish();

		file.close();



		/////////////////////////////////////////////////////////////
		// generating mesh for the set of points
		/////////////////////////////////////////////////////////////

		GeneratorData<DIM>	gdata;
		Kernel<DIM>	kernel(mGrid,gdata);

		kernel.InitBoundingBox( vmin, vmax);

		kernel.InitCube();

		/////////////////////////////////////////////////////////////
		// setting metric for box corners to dummy values

		Metric<DIM> metdef;
		metdef.InitIso( 1.0);

		for ( MGSize i=1; i<=mGrid.SizeNodeTab(); ++i)
			gdata.InsertMetric( metdef, i);

		for ( typename Grid<DIM>::ColNode::iterator itr = mGrid.rColNode().begin(); itr != mGrid.rColNode().end(); ++itr)
		{
			MGSize idm = mMetricCx.Insert( metdef );
			itr->rMasterId() = idm;
		}


		/////////////////////////////////////////////////////////////
		// insert bundary points

		//random_shuffle( tabnode.begin(), tabnode.end() );

		//ProgressBar	bar(40);
		bar.Init( tabnode.size() );
		bar.Start();
		for ( MGSize ii=0; ii<tabnode.size(); ++ii, ++bar)
		{
			MGSize inod = kernel.InsertPoint( tabnode[ii] );

			//cout << inod << " " 
			//	<< tabnode[ii].cX()  << " " << tabnode[ii].cY()  << " " << tabnode[ii].cZ() << endl;


			if ( ! inod)
			{
				char sbuf[512];
				sprintf( sbuf, "CRASH :: kernel failed to insert new point with coordinates [%lg %lg %lg]", 
								tabnode[ii].cX(), tabnode[ii].cY(), tabnode[ii].cZ() );

				THROW_INTERNAL( sbuf);
			}

			gdata.InsertMetric( metdef, inod );

			MGSize idcx = mMetricCx.Insert( tabmetric[ii] );
			mGrid.rNode( inod).rMasterId() = idcx;

			//mGrid->rNode(inod).rMasterId() = bnd.cNode(i).cMasterId();
			//bnd.rNode(i).rGrdId() = inod;
		}
		bar.Finish();

		WriteGrdTEC<DIM> wtec( &mGrid);
		wtec.DoWrite( "_meandros_.dat");


		cout << " - FINISHED\n";
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: ReadMEANDR<DIM>::DoRead - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}

}



template class ReadMEANDR<DIM_2D>;
template class ReadMEANDR<DIM_3D>;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

