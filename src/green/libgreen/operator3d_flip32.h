#ifndef __OPERATOR3D_FLIP32_H__
#define __OPERATOR3D_FLIP32_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class BndGrid;

template <Dimension DIM> 
class Kernel;



//////////////////////////////////////////////////////////////////////
// class Operator3D_Flip32
//////////////////////////////////////////////////////////////////////
class Operator3D_Flip32
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

public:
	Operator3D_Flip32( Kernel<DIM_3D>& kernel) : mKernel(kernel), mErrorCode(0)		{}

	const MGSize&	Error() const		{ return mErrorCode;}


	void	Reset();

	void	Init( const vector<MGSize>& tabcell);

	void	Select();
	void	SelectQBased();
	void	SelectVolBased();

	void	DoFlip( vector<MGSize>& tabcell);

protected:
	void	BuildCells( vector<GCell>& tabcell);


private:
	Kernel<DIM_3D>		&mKernel;

	MGSize			mErrorCode;

	Key<2>			mEdge;
	vector<MGSize>	mtabOCell;
	vector<MGSize>	mtabENode;

	vector<GCell>	mtabNCell;
};
//////////////////////////////////////////////////////////////////////



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __OPERATOR3D_FLIP32_H__
