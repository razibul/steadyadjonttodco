#include "spointpushdestructor.h"

#include "libgreen/destroyer_v2.h"
#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/bndgrid.h"
#include "libgreen/gridgeom.h"
#include "libgreen/controlspace.h"

#include "libgreen/operator3d_relocatenode.h"
#include "libgreen/operator3d_collapseedge.h"

#include "libgreen/bndgrid.h"
#include "libgreen/gridwbnd.h"
#include "libgreen/reconstructor.h"

#include "libgreen/writebndtec.h"
#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



void SPointPushDestructor::Init(  const vector< Key<2> >& tabsedge, const GVect& vn, const MGFloat& dist)
{
	//cout << "SPointPushDestructor::Init" << endl;

	mVn = vn;
	mDist = dist;

	// find 2 edges
	vector<MGSize> tabedge;
	tabedge.reserve(2);

	for ( MGSize ie=0; ie<tabsedge.size(); ++ie)
	{
		if ( tabsedge[ie].cFirst() == mINode )
			tabedge.push_back( ie);

		if ( tabsedge[ie].cSecond() == mINode )
			tabedge.push_back( ie);
	}

	if ( tabedge.size() != 2)
		THROW_INTERNAL( "EdgeDestr::RemoveNode() :: tabedge.size() != 2" );

	mEdgeL = tabsedge[ tabedge[0] ];
	mEdgeR = tabsedge[ tabedge[1] ];

	if ( mEdgeL.cFirst() == mINode )
		mINodeL = mEdgeL.cSecond();
	else
		mINodeL = mEdgeL.cFirst();

	if ( mEdgeR.cFirst() == mINode )
		mINodeR = mEdgeR.cSecond();
	else
		mINodeR = mEdgeR.cFirst();

	// find ball for inod
	MGSize icell = mpParent->mpKernel->cGrid().cNode(mINode).cCellId();

	if ( icell == 0)
		THROW_INTERNAL( "SPointDestructor::Init :: icell==0 for inod = " << mINode );

	mpParent->mpKernel->FindBall( mtabBall, mINode, icell );


	//WriteGrdTEC<DIM_3D> writer( & mpParent->mpKernel->cGrid() );
	//writer.WriteCells( "spd_ball.dat", mtabBall, "BALL");

	//vector< Key<DIM_3D> > tabnull;
	//mpParent->mpKernel->cGrid().FindNullFaces( tabnull );
	//writer.WriteFaces( string("spd_global_nullfaces") + string(".dat"), tabnull);

	// find boundary faces of the ball
	vector< Key<3> > tabface;
	for ( MGSize i=0; i<mtabBall.size(); ++i)
	{
		const GCell& cell = mpParent->mpKernel->cGrid().cCell( mtabBall[i] );

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			if ( cell.cCellId(k).first == 0 )
			{
				Face<DIM_3D> face;
				cell.GetFaceVNIn( face, k);

				for ( MGSize j=0; j<Face<DIM_3D>::SIZE; ++j)
					if ( face.cNodeId(j) == mINode )
					{
						mtabBFace.push_back( face );
						tabface.push_back( face.CellKey() );
						//cout << face.cNodeId(0) << " " << face.cNodeId(1) << " " << face.cNodeId(2) << endl;
						//tabface.push_back( Key<3>( face.cNodeId(0), face.cNodeId(1), face.cNodeId(2) ) );
						break;
					}
			}
		}
	}

	// find boundary of boundary faces
	vector< Key<2> > tabbe;
	mtabBEL.clear();
	mtabBER.clear();

	for ( MGSize i=0; i<mtabBFace.size(); ++i)
	{
		const Face<DIM_3D>& face = mtabBFace[i];
		SimplexFace<DIM_3D> gface = GridGeom<DIM_3D>::CreateSimplexFace( face, mpParent->mpKernel->cGrid() );

		for ( MGSize j=0; j<Face<DIM_3D>::ESIZE; ++j)
		{
			Key<2> edge( face.cNodeId( gface.cEdgeConn( j, 0)), face.cNodeId(gface.cEdgeConn( j, 1)) );
			if ( edge.cFirst() != mINode && edge.cSecond() != mINode ) 
				tabbe.push_back( edge);
		}
	}

	//cout << endl;
	//cout << mINodeL << " " << mINodeR << endl << endl;
	//
	//for ( MGSize i=0; i<tabbe.size(); ++i)
	//	cout << tabbe[i].cFirst() << " - " << tabbe[i].cSecond() << endl;

	bool bL = false;
	bool bR = false;
	for ( MGSize i=0; i<tabbe.size(); ++i)
	{
		if ( tabbe[i].cFirst() == mINodeL )
			bL = true;
		if ( tabbe[i].cFirst() == mINodeR )
			bR = true;
	}

	if ( !bL ||  !bR)
		THROW_INTERNAL( "SPointPushDestructor::Init -- edge not found !!!");

	// split tabbe

	//cout << "start do L" << endl;
	MGSize in;
	in = mINodeL;
	do
	{
		Key<2> ke;
		bool bok = false;
		for ( MGSize i=0; i<tabbe.size(); ++i)
			if ( tabbe[i].cFirst() == in )
			{
				bok = true;
				ke = tabbe[i];
				break;
			}

		if ( bok )
		{
			mtabBEL.push_back( ke);
			in = ke.cSecond();
		}
	}
	while ( in != mINodeR );

	//cout << "start do R" << endl;
	in = mINodeR;
	do
	{
		Key<2> ke;
		bool bok = false;
		for ( MGSize i=0; i<tabbe.size(); ++i)
			if ( tabbe[i].cFirst() == in )
			{
				bok = true;
				ke = tabbe[i];
				break;
			}

		if ( bok )
		{
			mtabBER.push_back( ke);
			in = ke.cSecond();
		}
	}
	while ( in != mINodeL );

	//cout << "done" << endl;

	//cout << "L" << endl;
	//for ( MGSize i=0; i<mtabBEL.size(); ++i)
	//	cout << mtabBEL[i].cFirst() << " - " << mtabBEL[i].cSecond() << endl;
	//cout << "R" << endl;
	//for ( MGSize i=0; i<mtabBER.size(); ++i)
	//	cout << mtabBER[i].cFirst() << " - " << mtabBER[i].cSecond() << endl;

	//
	//WriteGrdTEC<DIM_3D> writer( & mpParent->mpKernel->cGrid() );

	////writer.WriteCells( "spdestr_cavity.dat", tabccell, "CAVITY");
	//writer.WriteCells( "spdestr_ball.dat", mtabBall, "BALL");
	//writer.WriteFaces( "spdestr_bfaces.dat", tabface, "BFACES");


	//THROW_INTERNAL( "STOP");
}



bool SPointPushDestructor::InsertPushNode( const MGFloat& coeff)
{
	/////////////////////////////////////////////////////////////
	GVect vppnt;
	MGFloat cdist = mDist;

	//cdist *= 0.05;
	cdist *= coeff;

	GVect vp = mpParent->mpKernel->cGrid().cNode( mINode).cPos(); 
	vppnt = vp - mVn * cdist; 

	MGFloat d = (vp - vppnt).module();

	//cout << d << " / " << cdist << endl;
	//vp.Write();
	//vppnt.Write();

	Operator3D_RelocateNode opreloc( *(mpParent->mpKernel) );
	opreloc.Init( mINode, vppnt);

	//cout << "dist = " << d << " / " << cdist << "  Error = " << opreloc.Error() << endl;

	if ( opreloc.Error() != 0)
		return false;
		//THROW_INTERNAL( "opreloc.Error() != 0" );

	/////////////////////////////////////////////////////////////
	// search for the delaunay cavity of vct
	static vector<MGSize>	tabccell;
	static vector<GFace>	tabcface;

	tabcface.reserve(60);
	tabccell.reserve(60);


	tabcface.resize(0);
	tabccell.resize(0);

	GMetric met = mpParent->mpCSpace->GetSpacing( vppnt);	// TODO :: Expensive !!!

	///
	MGSize icell = mpParent->mpKernel->cGrid().cNode(mINode).cCellId();
	mpParent->mpKernel->FindCavityMetricBase( tabccell, mtabBall, icell, vppnt, met);
	//mpParent->mpKernel->FindCavityBase( tabccell, mtabBall, icell, vppnt);

	mpParent->mpKernel->CavityCorrection( tabccell, vppnt);

	// if the ball is still painted carry on; if not get closer to the spoint
	for ( MGSize ic=0; ic<mtabBall.size(); ++ic)
		if ( ! mpParent->mpKernel->cGrid().cCell( mtabBall[ic] ).IsPainted() )
		{
			for ( MGSize k=0; k<tabccell.size(); ++k)
				mpParent->mpKernel->rGrid().rCell( mtabBall[ic] ).WashPaint();

			return false;
		}


	if ( ! mpParent->mpKernel->CavityBoundary( tabcface, tabccell ) )
	{
		// return NULL if cavity is _not_ found 
		return false;
	}	

	/////////////////////////////////////////////////////////////
	// insert push point
	MGSize inod =  mpParent->mpKernel->InsertPoint( tabccell, tabcface, vppnt);

	mpParent->mpKernel->rGrid().UpdateNodeCellIds( tabccell);
	mpParent->mpKernel->rGenData().InsertMetric( met, inod);

	mIPNode = inod;
	mPPnt = vppnt;


	//
	MGSize icella = mpParent->mpKernel->cGrid().cNode(mINode).cCellId();

	if ( icella == 0)
		THROW_INTERNAL( "SPointDestructor::Init :: icell==0 for inod = " << mINode );

	// check the new ball
	mtabBall.clear();
	mpParent->mpKernel->FindBall( mtabBall, mINode, icella );

	//WriteGrdTEC<DIM_3D> writer( & mpParent->mpKernel->cGrid() );
	//writer.WriteCells( "spdestr_new_ball.dat", mtabBall, "NEW_BALL");
	
	
	/////////////////////////////////////////////////////////////

	return true;
}


void SPointPushDestructor::Triangulate( Grid<DIM_3D>& fcGrid, vector< Key<3> >& tabbface, const vector< Key<2> >& tabbe, const string& stxt)
{
	GeneratorData<DIM_3D>	fcGData;
	Kernel<DIM_3D>			fcKernel(fcGrid,fcGData);

	vector<MGSize> tabinod;

	for ( MGSize i=0; i<tabbe.size(); ++i)
	{
		tabinod.push_back( tabbe[i].cFirst() );
		tabinod.push_back( tabbe[i].cSecond() );
	}

	sort( tabinod.begin(), tabinod.end() );
	tabinod.erase( unique( tabinod.begin(), tabinod.end() ), tabinod.end() );

	// find box
	Box<DIM_3D> box;
	for ( MGSize i=0; i<tabinod.size(); ++i)
	{
		GVect pos = mpParent->mpKernel->cGrid().cNode( tabinod[i] ).cPos();

		if ( i==0 )
			box.rVMax() = box.rVMin() = pos;

		box.rVMax() = Maximum( box.cVMax(), pos );
		box.rVMin() = Minimum( box.cVMin(), pos );
	}

	box.Equalize();

	// init cube
	GVect dv = box.cVMax() - box.cVMin();
	dv *= 1.5;
	box.rVMax() = box.cVMax() + dv;
	box.rVMin() = box.cVMin() - dv;

	fcKernel.rBox() = box;
	fcKernel.InitCube();

	// set up metric
	GMetric met = mpParent->mpCSpace->GetSpacing( mPPnt);	// TODO :: Expensive !!!
	for ( MGSize i=1; i<=fcGrid.SizeNodeTab(); ++i)
		fcGData.InsertMetric( met, i);

	fcGrid.CheckConnectivity();

	//WriteGrdTEC<DIM_3D> wtec( &fcGrid);
	//wtec.DoWrite( string("spd_box") + stxt + string(".dat") );


	// insert points
	for ( MGSize i=0; i<tabinod.size(); ++i)
	{
		GVect pos = mpParent->mpKernel->cGrid().cNode( tabinod[i] ).cPos();

		MGSize inod = fcKernel.InsertPointMetric( pos, met );

		if ( ! inod)
			THROW_INTERNAL( "CRASH :: SPD :: kernel failed to insert new point with coordinates [" 
							<< pos.cX() << ", " << pos.cY() << ", " << pos.cZ() << "]" );

		fcGrid.rNode(inod).rMasterId() = tabinod[i];

		fcKernel.InsertMetric( met, inod );
	}

	//wtec.DoWrite( string("spd_initial_bf") + stxt + string(".dat") );

	// find bfaces
	set< Key<3> > setface;
	for ( Grid<DIM_3D>::ColCell::const_iterator itc=fcGrid.cColCell().begin(); itc!= fcGrid.cColCell().end(); ++itc)
	{
		const GCell& cell = *itc;

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			Key<3> face = cell.FaceKey( ifc);
			face.Sort();

			bool bok = true;
			for ( MGSize in=0; in<3; ++in)
				if ( face.cElem(in) <= 8)
				{
					bok = false;
					break;
				}

			if ( bok)
			{
				tabbface.push_back( face);
				setface.insert( face);
			}
		}
	}

	sort( tabbface.begin(), tabbface.end() );
	tabbface.erase( unique( tabbface.begin(), tabbface.end() ), tabbface.end() );


	// insert ppnt
	MGSize inod = fcKernel.InsertPointMetricProtect( mPPnt, met, setface );

	if ( ! inod)
		THROW_INTERNAL( "CRASH :: SPD :: kernel failed to insert new point with coordinates [" 
						<< mPPnt.cX() << ", " << mPPnt.cY() << ", " << mPPnt.cZ() << "]" );

	fcGrid.rNode(inod).rMasterId() = mIPNode;
	fcKernel.InsertMetric( met, inod );


	//wtec.DoWrite( string("spd_initial") + stxt + string(".dat") );

	// flag external cells (conected to cube nodes)
	for ( Grid<DIM_3D>::ColCell::iterator itr=fcGrid.rColCell().begin(); itr!=fcGrid.rColCell().end(); ++itr)
	{
		GCell& cell = *itr;
		for ( MGSize in=0; in<GCell::SIZE; ++in)
			if ( cell.cNodeId( in) >= 1 && cell.cNodeId( in) <= 8 ) // bounding box nodes
			{
				cell.SetExternal( true);
				break;
			}
	}

	// remove external cells
	for ( Grid<DIM_3D>::ColCell::iterator itr = fcGrid.rColCell().begin(); itr != fcGrid.rColCell().end(); ++itr)
	{
		if ( (*itr).IsExternal() )
			fcKernel.EraseCellSafe( itr->cId() );
	}

	// removing nodes of the external cube
	for ( MGSize i=1; i<=8; ++i)
		fcGrid.EraseNode( i);

	fcKernel.DisableTree();

	//wtec.DoWrite( string("spd_final") + stxt + string(".dat"));

}



void SPointPushDestructor::Triangulate2( Grid<DIM_3D>& fcGrid, vector< Key<3> >& tabbface, const vector< Key<2> >& tabbe, const string& stxt)
{
	TriangulateFaces( tabbface, tabbe, stxt);

	//WriteGrdTEC<DIM_3D> wtec( &mpParent->mpKernel->cGrid() );
	//wtec.WriteFaces( string("spd_final_in_3d") + stxt + string(".dat"), tabbface, "FACES_2D");

	// create tabnode
	vector<MGSize> tabnode;
	for ( MGSize i=0; i<tabbface.size(); ++i)
		for ( MGSize in=0; in<3; ++in)
			tabnode.push_back( tabbface[i].cElem(in) );

	sort( tabnode.begin(), tabnode.end());
	tabnode.erase( unique( tabnode.begin(), tabnode.end()), tabnode.end() );

	// insert nodes
	map<MGSize,MGSize> mapgl;
	for ( MGSize i=0; i<tabnode.size(); ++i)
	{
		MGSize inod = fcGrid.InsertNode( GNode( mpParent->mpKernel->cGrid().cNode( tabnode[i] ).cPos() ) );
		mapgl[ tabnode[i] ] = inod;
		fcGrid.rNode( inod).rMasterId() = tabnode[i];
	}
	MGSize inod = fcGrid.InsertNode( GNode( mpParent->mpKernel->cGrid().cNode( mIPNode ).cPos() ) );
	mapgl[ mIPNode ] = inod;
	fcGrid.rNode( inod).rMasterId() = mIPNode;

	// insert cells
	vector<MGSize> tabcell;
	tabcell.reserve( tabbface.size() );

	for ( MGSize i=0; i<tabbface.size(); ++i)
	{
		GCell cell;

		for ( MGSize in=0; in<3; ++in)
			cell.rNodeId(in) = mapgl[ tabbface[i].cElem(in) ];
		cell.rNodeId(3) = mapgl[ mIPNode ];

		MGSize icell = fcGrid.InsertCell( cell );
		if ( GridGeom<DIM_3D>::CreateSimplex( icell, fcGrid).Volume() < 0.0 )
			THROW_INTERNAL( "negative cell");
	}

	//WriteGrdTEC<DIM_3D> wtecloc( &fcGrid );
	//wtecloc.DoWrite( string("spd_local_final") + stxt + string(".dat"));

	// reconnect
	vector< KeyData< Key<3>, GNeDef> > tabconn;

	for ( Grid<DIM_3D>::ColCell::iterator itr = fcGrid.rColCell().begin(); itr != fcGrid.rColCell().end(); ++itr)
	{
		GCell& cell = *itr;

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			Key<3> key = cell.FaceKey( k);
			key.Sort();
			tabconn.push_back( KeyData< Key<3>, GNeDef>( key, GNeDef( cell.cId(), MGChar(k))) );
		}
	}

	sort( tabconn.begin(), tabconn.end() );

	//for ( MGSize i=0; i<tabconn.size(); ++i)
	//	cout << tabconn[i].first.cFirst() << " "  << tabconn[i].first.cSecond() << " "  << tabconn[i].first.cThird() << " || " 
	//	<< tabconn[i].second.first << " "  << MGSize(tabconn[i].second.second) << endl;
	//cout  << endl;

	for ( Grid<DIM_3D>::ColCell::iterator itr = fcGrid.rColCell().begin(); itr != fcGrid.rColCell().end(); ++itr)
	{
		GCell& cell = *itr;

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			Key<3> key = cell.FaceKey( k);
			key.Sort();

			pair< vector< KeyData< Key<3>, GNeDef> >::iterator, vector< KeyData< Key<3>, GNeDef> >::iterator > result;

			result = equal_range( tabconn.begin(), tabconn.end(), KeyData< Key<3>, GNeDef>( key) );

			if ( result.second - result.first == 2 )
			{
				//cout << result.first->first.cFirst() << " "  << result.first->first.cSecond() << " "  << result.first->first.cThird() << " || " ;
				//cout << result.first->second.first << " "  << MGSize(result.first->second.second) << endl;
				//cout << result.second->first.cFirst() << " "  << result.second->first.cSecond() << " "  << result.second->first.cThird() << " || " ;
				//cout << result.second->second.first << " "  << MGSize(result.second->second.second) << endl;
				//cout  << endl;

				vector< KeyData< Key<3>, GNeDef> >::iterator ri = result.first;

				if ( ri->second.first == itr->cId() )
				{
					++ri;
					if ( ri->second.first == itr->cId() )
						THROW_INTERNAL( "conn map inconsistent !!!");
				}

				cell.rCellId( k ) = ri->second;
			}
		}
	}

	//fcGrid.CheckConnectivity();

	//vector< Key<DIM_3D> > tabnull;
	//fcGrid.FindNullFaces( tabnull );
	//wtecloc.WriteFaces( string("spd_local_nullfaces") + stxt + string(".dat"), tabnull);

}


void SPointPushDestructor::TriangulateFaces( vector< Key<3> >& tabbface, const vector< Key<2> >& tabbe, const string& stxt)
{
	typedef TDefs<DIM_2D>::GVect GVect2D;

	// find vn

	vector<MGSize> tabn;
	tabn.reserve( 2*tabbe.size() );
	tabn.push_back( tabbe[0].cFirst() );

	for ( MGSize ie=0; ie<tabbe.size(); ++ie)
		tabn.push_back( tabbe[ie].cSecond() );


	GVect vn(0,0,0);
	GVect v0 = mpParent->mpKernel->cGrid().cNode( tabn[0] ).cPos();
	GVect dvr = mpParent->mpKernel->cGrid().cNode( tabn[tabn.size()-1] ).cPos() - v0;
	MGFloat scale = 1. / dvr.module();

	IS_INFNAN_THROW( scale, "SPointPushDestructor::TriangulateFaces");

	for ( MGSize i=1; i<tabn.size(); ++i)
	{
		GVect v = mpParent->mpKernel->cGrid().cNode( tabn[i] ).cPos() - v0;
		vn += dvr % v;
	}

	// find base
	//vn = vn.versor();
	//GVect vx = dvr.versor();
	//GVect vy = (vn % vx).versor();

	GVect vy = (vn % dvr).versor();
	GVect vx = dvr.versor();
	vn = vn.versor();

	//vx.Write();
	//vy.Write();
	//(vx%vy).Write();


	GridWBnd<DIM_2D>		fcGrid;
	GeneratorData<DIM_2D>	fcGData;
	Kernel<DIM_2D>			fcKernel(fcGrid,fcGData);
	BndGrid<DIM_2D>&		bnd = fcGrid.rBndGrid();

	// create boundary grid
	map<MGSize,MGSize> map32;
	BndGrid<DIM_2D>::GBndNode	bnode;

	for ( MGSize i=0; i<tabn.size(); ++i)
	{
		GVect v = mpParent->mpKernel->cGrid().cNode( tabn[i] ).cPos();
		GVect2D vpos( vx*(v-v0), vy*(v-v0) );


		bnode.rPos() = vpos * scale;
		bnode.rId() = i+1;

		bnd.rColNode().push_back( bnode);
		map32[ tabn[i] ] = i+1;
	}

	BndGrid<DIM_2D>::GBndCell	bcell;
	map<MGSize,MGSize>::iterator itr;

	for ( MGSize ie=0; ie<tabbe.size(); ++ie)
	{
		bcell.Reset();
		bcell.rTopoId() = 1;
		bcell.rId() = ie+1;

		if ( (itr=map32.find( tabbe[ie].cFirst() ) ) == map32.end() )
			THROW_INTERNAL( "SPointPushDestructor::Triangulate2 crash!!!");

		bcell.rNodeId(0) = itr->second;

		if ( (itr=map32.find( tabbe[ie].cSecond() ) ) == map32.end() )
			THROW_INTERNAL( "SPointPushDestructor::Triangulate2 crash!!!");

		bcell.rNodeId(1) = itr->second;

		bnd.rColCell().push_back( bcell );
	}

	bcell.Reset();
	bcell.rTopoId() = 1;
	bcell.rId() = tabbe.size()+1;

	if ( (itr=map32.find( tabbe[tabbe.size()-1].cSecond() ) ) == map32.end() )
		THROW_INTERNAL( "SPointPushDestructor::Triangulate2 crash!!!");

	bcell.rNodeId(0) = itr->second;

	if ( (itr=map32.find( tabbe[0].cFirst() ) ) == map32.end() )
		THROW_INTERNAL( "SPointPushDestructor::Triangulate2 crash!!!");

	bcell.rNodeId(1) = itr->second;

	bnd.rColCell().push_back( bcell );

	//WriteBndTEC<DIM_2D>	 bndwriter( &bnd);
	//bndwriter.DoWrite( string("spd_boundary") + stxt + string(".dat"));

	// trinagulate 2d
	GVect2D	vmin, vmax;

	bnd.FindMinMax( vmin, vmax);
	
	fcKernel.InitBoundingBox( vmin, vmax);
	fcKernel.InitCube();


	for ( map<MGSize,MGSize>::const_iterator citr=map32.begin(); citr!=map32.end(); ++citr)
	{
		MGSize i = citr->second;
		GVect2D vct = bnd.cNode(i).cPos();
		MGSize inod = fcKernel.InsertPoint( vct );

		if ( ! inod)
			THROW_INTERNAL( "SPD::CRASH :: kernel failed to insert new point with coordinates [" << vct.cX() << " " << vct.cY() << "]" );

		fcGrid.rNode(inod).rMasterId() = citr->first;
		bnd.rNode(i).rGrdId() = inod;
	}

	//WriteGrdTEC<DIM_2D> wtec( &fcGrid);
	//wtec.DoWrite( string("spd_initial_1") + stxt + string(".dat"));

	Reconstructor<DIM_2D>	recon( bnd, fcKernel);

	try
	{
		recon.Init( false);
		recon.Reconstruct( false);
	}
	catch (...)
	{
		cout << "CRASH - writing crash_dump.txt" <<endl;
		ofstream of( "crash_dump.txt");
		vn.Write(of);
		vx.Write(of);
		vy.Write(of);

		of << endl;
		for ( map<MGSize,MGSize>::const_iterator citr=map32.begin(); citr!=map32.end(); ++citr)
		{
			of << citr->first << endl;
			GVect v = mpParent->mpKernel->cGrid().cNode( citr->first ).cPos();
			v.Write( of);
			MGSize i = citr->second;
			GVect2D vct = bnd.cNode(i).cPos();

			vct.Write( of);
			of << endl;
		}

		throw;
	}

	//wtec.DoWrite( string("spd_initial_rec") + stxt + string(".dat"));

	// flag external cells (conected to cube nodes)
	for ( Grid<DIM_2D>::ColCell::iterator itr=fcGrid.rColCell().begin(); itr!=fcGrid.rColCell().end(); ++itr)
	{
		Grid<DIM_2D>::GCell& cell = *itr;
		for ( MGSize in=0; in<Grid<DIM_2D>::GCell::SIZE; ++in)
			if ( cell.cNodeId( in) >= 1 && cell.cNodeId( in) <= 4 ) // bounding box nodes
			{
				cell.SetExternal( true);
				break;
			}
	}

	// remove external cells
	for ( Grid<DIM_2D>::ColCell::iterator itr = fcGrid.rColCell().begin(); itr != fcGrid.rColCell().end(); ++itr)
	{
		if ( (*itr).IsExternal() )
			fcGrid.rColCell().erase( itr);
	}

	// removing nodes of the external cube
	for ( MGSize i=1; i<=4; ++i)
		fcGrid.EraseNode( i);

	//wtec.DoWrite( string("spd_final_") + stxt + string(".dat"));

	tabbface.clear();
	tabbface.reserve( fcGrid.SizeCellTab() );
	for ( Grid<DIM_2D>::ColCell::iterator itr = fcGrid.rColCell().begin(); itr != fcGrid.rColCell().end(); ++itr)
	{
		Key<3> key;

		if ( GridGeom<DIM_2D>::CreateSimplex( *itr, fcGrid).Volume() < 0.0 )
			THROW_INTERNAL( "negative face (2D cell)");

		for ( MGSize in=0; in<Grid<DIM_2D>::GCell::SIZE; ++in)
			key.rElem(in) = fcGrid.cNode( itr->cNodeId(in) ).cMasterId();

		tabbface.push_back( key);
	}

	//cout << "tabbfaces.size() = " << tabbface.size() << endl;
	//cout << "fcGrid.rColCell() = " << fcGrid.SizeCellTab() << endl;

}



void SPointPushDestructor::SyncCavities( Grid<DIM_3D>& fcGridL, Grid<DIM_3D>& fcGridR)
{
	//cout << " SPointPushDestructor::SyncCavities " << endl;

	Kernel<DIM_3D> *pKernel = mpParent->mpKernel;

	// build new cell collection
	vector<MGSize> tabnewcell;

	// left grid
	map<MGSize,MGSize> mapcellid;
	vector<MGSize> tabtmp;
	for ( Grid<DIM_3D>::ColCell::iterator itr = fcGridL.rColCell().begin(); itr != fcGridL.rColCell().end(); ++itr)
	{
		GCell cell;
		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			cell.rNodeId( ifc) = fcGridL.cNode( itr->cNodeId( ifc) ).cMasterId();
			cell.rCellId( ifc) = itr->cCellId( ifc);
		}
		MGSize id = pKernel->rGrid().InsertCell( cell);

		tabnewcell.push_back( id);
		tabtmp.push_back( id);
		mapcellid[ itr->cId() ] = id;
	}

	for ( MGSize i=0; i<tabtmp.size(); ++i)
	{
		GCell& cell = pKernel->rGrid().rCell( tabtmp[i] );

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize inext = cell.cCellId(ifc).first;

			if (inext)
				cell.rCellId(ifc).first = mapcellid[ cell.cCellId(ifc).first ];
		}
	}

	// right grid
	mapcellid.clear();
	tabtmp.clear();
	for ( Grid<DIM_3D>::ColCell::iterator itr = fcGridR.rColCell().begin(); itr != fcGridR.rColCell().end(); ++itr)
	{
		GCell cell;
		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			cell.rNodeId( ifc) = fcGridR.cNode( itr->cNodeId( ifc) ).cMasterId();
			cell.rCellId( ifc) = itr->cCellId( ifc);
		}
		MGSize id = pKernel->rGrid().InsertCell( cell);

		tabnewcell.push_back( id);
		tabtmp.push_back( id);
		mapcellid[ itr->cId() ] = id;
	}

	for ( MGSize i=0; i<tabtmp.size(); ++i)
	{
		GCell& cell = pKernel->rGrid().rCell( tabtmp[i] );

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize inext = cell.cCellId(ifc).first;

			if (inext)
				cell.rCellId(ifc).first = mapcellid[ cell.cCellId(ifc).first ];
		}
	}

	//WriteGrdTEC<DIM_3D> writerNew( & mpParent->mpKernel->cGrid() );
	//writerNew.WriteCells( "spd_new_cells.dat", tabnewcell, "NEW_CELLS");


	// paint ball cells
	for ( MGSize i=0; i<mtabBall.size(); ++i)
	{
		GCell& cell = pKernel->rGrid().rCell( mtabBall[i] );
		cell.Paint();
	}

	// find cav boundary
	vector< Key<3> > tabcavfaces;

	//typedef KeyData< Key<3>, pair<GNeDef,GNeDef> > TFaceInfo;
	//vector<TFaceInfo> tabcavbnd;

	map<Key<3>,GNeDef> mapcellinfo;

	for ( MGSize i=0; i<mtabBall.size(); ++i)
	{
		GCell& cell = pKernel->rGrid().rCell( mtabBall[i] );

		if ( ! cell.IsPainted() )
			THROW_INTERNAL( "SPD::SyncCavities :: the cell is not painted !!!");

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			MGSize icnext = cell.cCellId(k).first;
			if ( icnext == 0 )
				continue;

			GCell& cellnext = pKernel->rGrid().rCell( icnext );

			if ( ! cellnext.IsPainted() )
			{
				Key<3> key = cell.FaceKey(k);
				key.Sort();
				tabcavfaces.push_back( key );

				mapcellinfo.insert( map<Key<3>,GNeDef>::value_type( key, cell.cCellId(k) ) );
			}
		}
	}

	//WriteGrdTEC<DIM_3D> wtec( &pKernel->cGrid());
	//wtec.WriteFaces( "spd_grid_bndfaces.dat", tabcavfaces);

	for ( MGSize i=0; i<mtabBall.size(); ++i)
		pKernel->rGrid().EraseCell( mtabBall[i] );
		//pKernel->rGrid().EraseCellSafe( mtabBall[i] );


	// find local grid faces
	map<Key<3>,GNeDef> mapcellinfoNew;

	for ( MGSize i=0; i<tabnewcell.size(); ++i)
	{
		const GCell& cell = pKernel->cGrid().cCell( tabnewcell[i] );

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			MGSize icnext = cell.cCellId(k).first;
			if ( icnext != 0 )
				continue;

			Key<3> key = cell.FaceKey( k);
			key.Sort();

			if ( mapcellinfoNew.find( key ) == mapcellinfoNew.end() )
				mapcellinfoNew.insert( map< Key<3>, GNeDef >::value_type( key, GNeDef( tabnewcell[i], k ) ) );
		}
	}


	// do sync
	for ( map<Key<3>,GNeDef>::iterator itr=mapcellinfo.begin(); itr!=mapcellinfo.end(); ++itr)
	{
		map<Key<3>,GNeDef>::iterator itrnew;
		if ( (itrnew = mapcellinfoNew.find( itr->first )) == mapcellinfoNew.end() )
		{
			itr->first.Dump();
			WriteGrdTEC<DIM_3D> wtec( &pKernel->cGrid());
			wtec.WriteFaces( "spd_grid_bndfaces.dat", tabcavfaces, "OLD_FACES");
			wtec.WriteCells( "spd_new_cells.dat", tabnewcell, "NEW_CELLS");

			THROW_INTERNAL( "SPD::SyncCavities - sync failed !!!");
		}

		GCell& cell = pKernel->rGrid().rCell( itr->second.first );
		GCell& cellnew = pKernel->rGrid().rCell( itrnew->second.first );

		cell.rCellId( itr->second.second) = itrnew->second;
		cellnew.rCellId( itrnew->second.second) = itr->second;
	}

	// stitch left and right part
	Key<3> keyst(mINodeL,mINodeR,mIPNode);
	keyst.Sort();

	vector<GNeDef> tabstitch;
	tabstitch.reserve(2);

	for ( MGSize i=0; i<tabnewcell.size(); ++i)
	{
		const GCell& cell = pKernel->cGrid().cCell( tabnewcell[i] );

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			Key<3> key = cell.FaceKey( k);
			key.Sort();

			if ( key == keyst)
				tabstitch.push_back( GNeDef( tabnewcell[i], k) );
		}
	}

	if ( tabstitch.size() != 2)
		THROW_INTERNAL( "SPD::SyncCavities - sync failed - tabstitch.size() = " << tabstitch.size());

	pKernel->rGrid().rCell( tabstitch[0].first ).rCellId( tabstitch[0].second ) = tabstitch[1];
	pKernel->rGrid().rCell( tabstitch[1].first ).rCellId( tabstitch[1].second ) = tabstitch[0];


	// update aux data and check volumes
	for ( MGSize ic=0; ic<tabnewcell.size(); ++ic)
	{
		pKernel->InsertCellIntoTree( tabnewcell[ic]);

		const GCell& cell = pKernel->cGrid().cCell( tabnewcell[ic] );

		for ( MGSize k=0; k<GCell::SIZE; ++k)
			pKernel->rGrid().rNode( cell.cNodeId(k) ).rCellId() = cell.cId();
	}

	//pKernel->cGrid().CheckConnectivity();
	//cout << " SPointPushDestructor::SyncCavities -- finished " << endl;

}



void SPointPushDestructor::UpdateSubEdges( vector< Key<2> >& tabsedge)
{
	Key<2> newsedge(mINodeL, mINodeR);
	newsedge.Sort();

	vector< Key<2> >	tabNSEdge;
	for ( MGSize i=0; i<tabsedge.size(); ++i)
	{
		if ( tabsedge[i] != mEdgeL && tabsedge[i] != mEdgeR)
			tabNSEdge.push_back( tabsedge[i] );
	}

	tabNSEdge.push_back( newsedge );

	tabsedge.swap( tabNSEdge );
}


bool SPointPushDestructor::Delete( const MGSize& niter)
{
	WriteGrdTEC<DIM_3D> wtec(&  mpParent->mpKernel->cGrid() );

	bool bok;
	//bool bok = InsertPushNode();

	//if ( ! bok)
	//	return false;

	MGSize n=0;
	MGFloat coeff = 1.0;

	do
	{
		bok = InsertPushNode( coeff);

		coeff /= 2.0;
		++n;
	}
	while ( !bok && n < niter);

	if ( ! bok)
		return false;

	//wtec.DoWrite( "__before.plt");

	Grid<DIM_3D> fcGridL;
	Grid<DIM_3D> fcGridR;
	vector< Key<3> > tabbfaceL;
	vector< Key<3> > tabbfaceR;

	Triangulate2( fcGridL, tabbfaceL, mtabBEL, "_L");
	Triangulate2( fcGridR, tabbfaceR, mtabBER, "_R");
	SyncCavities( fcGridL, fcGridR );

	mpParent->mpKernel->EraseNode( mINode);
	mINode = 0;

	//mpParent->mpKernel->rGrid().CheckConnectivity();

	//wtec.DoWrite( "__after.plt");

	// f
	
	//THROW_INTERNAL( "STOP");

	return true;
}

//bool SPointPushDestructor::Delete()
//{
//	// find push point
//	GVect vppnt;
//	MGFloat cdist = mDist;
//
//	do
//	{
//		cdist *= 0.5;
//
//		GVect vp = mpParent->mpKernel->cGrid().cNode( mINode).cPos(); 
//		vppnt = vp - mVn * cdist; 
//
//		MGFloat d = (vp - vppnt).module();
//
//		//cout << d << " / " << cdist << endl;
//		//vp.Write();
//		//vppnt.Write();
//
//		Operator3D_RelocateNode opreloc( *(mpParent->mpKernel) );
//		opreloc.Init( mINode, vppnt);
//
//		cout << "dist = " << d << " / " << cdist << "  Error = " << opreloc.Error() << endl;
//
//		if ( opreloc.Error() == 0)
//		{
//			cout << endl;
//			break;
//		}
//
//
//		//cout << endl;
//		//THROW_INTERNAL( "STOP");
//	}
//	while ( true );
//
//
//
//	//cout << endl;
//
//	GMetric met = mpParent->mpCSpace->GetSpacing( vppnt);	// TODO :: Expensive !!!
//
//	return false;
//}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

