#include "operator3d_flip32.h"

#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/gridgeom.h"

#include "libcorecommon/progressbar.h"
#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




void Operator3D_Flip32::Init( const vector<MGSize>& tabcell)
{
	if ( tabcell.size() != 3)
	{
		mErrorCode = 1;
		return;
	}

	mtabOCell = tabcell;

	Grid<DIM_3D>&	grid = mKernel.rGrid();


	/////////////////////////////////////////////////////////////
	// trying to find the common edge
	vector<MGSize> tab;

	const GCell &fstcell = grid.cCell( mtabOCell[0] );

	for ( MGSize i=0; i<GCell::SIZE; ++i)
	{
		MGSize in = fstcell.cNodeId( i);
		MGSize count = 1;

		for ( MGSize ic=1; ic<mtabOCell.size(); ++ic)
		{
			const GCell &celln = grid.cCell( mtabOCell[ic] );
		
			for ( MGSize k=0; k<GCell::SIZE; ++k)
				if ( celln.cNodeId(k) == in)
					++count;
		}

		if ( count == 3)
			tab.push_back( in);
	}

	if ( tab.size() != 2)
	{
		mErrorCode = 2;
		return;
	}

	mEdge.rFirst()  = tab[0];
	mEdge.rSecond() = tab[1];

	/////////////////////////////////////////////////////////////
	// trying to find the equator
	vector< Key<2> > tabedge;

	for ( MGSize ic=0; ic<mtabOCell.size(); ++ic)
	{
		const GCell &cell = grid.cCell( mtabOCell[ic] );

		for ( MGSize ie=0; ie<GCell::ESIZE; ++ie)
		{
			Key<2> edge = cell.EdgeKey( ie);
			if ( edge.cFirst() != mEdge.cFirst() && edge.cFirst() != mEdge.cSecond() &&
				 edge.cSecond() != mEdge.cFirst() && edge.cSecond() != mEdge.cSecond() )
				 tabedge.push_back( edge);
		}
	}

	if ( tabedge.size() != 3)
	{
		mErrorCode = 3;
		return;
	}

	/////////////////////////////////////////////////////////////
	// check orientation of the equator
	for ( MGSize ie=0; ie<tabedge.size(); ++ie)
	{
		Key<2>	&edge = tabedge[ie];

		Simplex<DIM_3D> tet( grid.cNode( mEdge.cFirst() ).cPos(),
							 grid.cNode( edge.cFirst() ).cPos(),
							 grid.cNode( edge.cSecond() ).cPos(),
							 grid.cNode( mEdge.cSecond() ).cPos() );

		if ( tet.Volume() < 0.0)
		{
			const MGSize itmp = edge.cFirst();
			edge.rFirst() = edge.cSecond();
			edge.rSecond() = itmp;
		}
	}

	/////////////////////////////////////////////////////////////
	// check the equator is closed
	if ( tabedge[0].cSecond() != tabedge[1].cFirst() ||
		 tabedge[1].cSecond() != tabedge[2].cFirst() ||
		 tabedge[2].cSecond() != tabedge[0].cFirst() )
	{
		reverse( tabedge.begin(), tabedge.end() );

		if ( tabedge[0].cSecond() != tabedge[1].cFirst() ||
			 tabedge[1].cSecond() != tabedge[2].cFirst() ||
			 tabedge[2].cSecond() != tabedge[0].cFirst() )
		{
			mErrorCode = 4;
			return;
		}
	}

	for ( MGSize ie=0; ie<tabedge.size(); ++ie)
		mtabENode.push_back( tabedge[ie].cSecond() );
}


void Operator3D_Flip32::BuildCells( vector<GCell>& tabcell)
{
	GCell cell;

	tabcell.clear();

	cell.rNodeId(0) = mtabENode[0];
	cell.rNodeId(1) = mtabENode[1];
	cell.rNodeId(2) = mtabENode[2];
	cell.rNodeId(3) = mEdge.cSecond();
	tabcell.push_back( cell);

	cell.rNodeId(0) = mtabENode[0];
	cell.rNodeId(1) = mtabENode[2];
	cell.rNodeId(2) = mtabENode[1];
	cell.rNodeId(3) = mEdge.cFirst();
	tabcell.push_back( cell);
}

void Operator3D_Flip32::Select()
{
	if ( mErrorCode != 0)
		return;

	BuildCells( mtabNCell);

	if ( ! mKernel.CheckCellVolume( mtabNCell) )
		mErrorCode = 2000;
}

void Operator3D_Flip32::SelectQBased()
{
	if ( mErrorCode != 0)
		return;

	vector<GCell> tabco;
	for ( MGSize i=0; i<mtabOCell.size(); ++i)
		tabco.push_back( mKernel.rGrid().cCell( mtabOCell[i]) );

	BuildCells( mtabNCell);

	if ( ! mKernel.CheckCellVolume( mtabNCell) )
	{
		mErrorCode = 2000;
		return;
	}

	MGFloat vo = mKernel.MinCellVolume( tabco);
	MGFloat v1 = mKernel.MinCellVolume( mtabNCell);
	if ( vo > v1)
	{
		mErrorCode = 1500;
		return;
	}

	MGFloat qbo = mKernel.CheckCellQuality( tabco);
	MGFloat qb1 = mKernel.CheckCellQuality( mtabNCell);

	if ( qbo < qb1)
	{
		mErrorCode = 1000;
		return;
	}
}

void Operator3D_Flip32::SelectVolBased()
{
	if ( mErrorCode != 0)
		return;

	vector<GCell> tabco;
	for ( MGSize i=0; i<mtabOCell.size(); ++i)
		tabco.push_back( mKernel.rGrid().cCell( mtabOCell[i]) );

	BuildCells( mtabNCell);

	if ( ! mKernel.CheckCellVolume( mtabNCell) )
	{
		mErrorCode = 2000;
		return;
	}

	MGFloat vo = mKernel.MinCellVolume( tabco);
	MGFloat v1 = mKernel.MinCellVolume( mtabNCell);
	if ( vo > v1)
	{
		mErrorCode = 1500;
		return;
	}
}



void Operator3D_Flip32::DoFlip( vector<MGSize>& tabcell)
{
	if ( mErrorCode != 0)
		return;


	ASSERT( mtabNCell.size() == 2);

	tabcell.clear();

	/////////////////////////////////////////////////////////////
	// remove old cell from the tree
	for ( MGSize ic=0; ic<mtabOCell.size(); ++ic)
		mKernel.RemoveCellFromTree( mtabOCell[ic] );

	/////////////////////////////////////////////////////////////
	// insert new cells 
	for ( MGSize i=0; i<mtabNCell.size(); ++i)
	{
		//MGSize id = mKernel.rGrid().InsertCell( mtabNCell[i] );
		MGSize id = mKernel.InsertCell( mtabNCell[i] );
		tabcell.push_back( id);

		mKernel.rGrid().rCell( id).SetModified( true);
	}

	/////////////////////////////////////////////////////////////
	// update internal connectivity

	// cell 0
	mKernel.rGrid().rCell( tabcell[0] ).rCellId( 3 ).first = tabcell[1];
	mKernel.rGrid().rCell( tabcell[0] ).rCellId( 3 ).second = 3;

	// cell 1
	mKernel.rGrid().rCell( tabcell[1] ).rCellId( 3 ).first = tabcell[0];
	mKernel.rGrid().rCell( tabcell[1] ).rCellId( 3 ).second = 3;


	/////////////////////////////////////////////////////////////
	// update external connectivity

	typedef map< Key<3>, GNeDef > FaceMap;	
	FaceMap mapface;
	FaceMap::iterator itrmf;

	for ( MGSize i=0; i<mtabOCell.size(); ++i)
	{
		const GCell &cell = mKernel.rGrid().cCell( mtabOCell[i]);

		for ( MGSize ifac=0; ifac<GCell::SIZE; ++ifac)
		{
			Key<3> key = cell.FaceKey( ifac);
			key.Sort();
			mapface.insert( FaceMap::value_type( key, cell.cCellId( ifac) ) );
		}

	}


	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		GCell &cell = mKernel.rGrid().rCell( tabcell[i]);

		for ( MGSize ifac=0; ifac<3; ++ifac)
		{
			Key<3> key = cell.FaceKey( ifac);
			key.Sort();
			if ( (itrmf = mapface.find( key)) == mapface.end() )
			{
				THROW_INTERNAL( "Flip44 - corrupted data !!!");
			}

			cell.rCellId(ifac) = itrmf->second;

			mKernel.rGrid().rCell( cell.cCellId( ifac ).first ).rCellId( cell.cCellId( ifac ).second ).first = cell.cId();
			mKernel.rGrid().rCell( cell.cCellId( ifac ).first ).rCellId( cell.cCellId( ifac ).second ).second = ifac;
		}
	}


	/////////////////////////////////////////////////////////////
	// remove old cells
	// remove old cell from the tree

	for ( MGSize ic=0; ic<mtabOCell.size(); ++ic)
		mKernel.EraseCell( mtabOCell[ic] );

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

