#ifndef __READBNDSMESH_H__
#define __READBNDSMESH_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

using namespace Geom;


template <Dimension DIM>
class BndGrid;


//////////////////////////////////////////////////////////////////////
// class ReadBndSMESH
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class ReadBndSMESH
{
public:
	ReadBndSMESH( BndGrid<DIM>* pgrd) : mpGrd(pgrd)	{}

	void	DoRead( const MGString& fname);

private:
	BndGrid<DIM>	*mpGrd;

	MGString		mFName;
};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __READBNDSMESH_H__

