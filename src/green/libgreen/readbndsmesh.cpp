#include "readbndsmesh.h"
#include "libgreen/bndgrid.h"
#include "libcoreio/store.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <>
void ReadBndSMESH<DIM_2D>::DoRead( const MGString& fname)
{
	mFName = fname;
	ifstream f( fname.c_str() );

	cout << "\nReading SMESH 2D'" << fname.c_str() << "'" << std::flush;

	istringstream is;

	//char	sbuf[1024];
	MGSize	nsurf, nnode, ncell, id, tabid[2], ncn, offset=0;
	Vect2D	vct;

	//IO::ReadLine( f, sbuf, "#");
	//sscanf( sbuf, "%d", &nsurf);
	IO::ReadLine( f, is, "#");
	is >> nsurf;


	MGSize	idnode = 0;
	MGSize	idcell = 0;

	for ( MGSize isurf=0; isurf<nsurf; ++isurf)
	{
		MGSize idsurf = isurf + 1;

		//IO::ReadLine( f, sbuf, "#");
		//IO::ReadLine( f, sbuf, "#");
		//sscanf( sbuf, "%d", &nnode);
		IO::ReadLine( f, is, "#");
		IO::ReadLine( f, is, "#");
		is >> nnode;


		TRACE( "nnode = " << nnode);
		mpGrd->rColNode().reserve( mpGrd->cColNode().size() + nnode);

		BndGrid<DIM_2D>::GBndNode	bnode;

		for ( MGSize in=0; in<nnode; ++in)
		{
			//IO::ReadLine( f, sbuf, "#");
			//sscanf( sbuf, "%d %lg %lg", &id, &vct.rX(), &vct.rY() );
			IO::ReadLine( f, is, "#");
			is >> id >> vct.rX() >> vct.rY();

			if ( id != in+1)
				THROW_INTERNAL( "SMESH file corrupted - node section");

			bnode.rPos() = vct;
			bnode.rId() = ++idnode;		// global node id

			mpGrd->rColNode().push_back( bnode);
		}

		//IO::ReadLine( f, sbuf, "#");
		//sscanf( sbuf, "%d", &ncell);
		IO::ReadLine( f, is, "#");
		is >> ncell;

		TRACE( "ncell = " << ncell);
		mpGrd->rColCell().reserve( mpGrd->cColCell().size() + ncell );

		BndGrid<DIM_2D>::GBndCell	bcell;

		for ( MGSize ic=0; ic<ncell; ++ic)
		{
			//IO::ReadLine( f, sbuf, "#");
			//// TODO :: check number of nodes per face
			//sscanf( sbuf, "%d %d %d %d", &id, &ncn, &tabid[0], &tabid[1] );
			// TODO :: check number of nodes per face
			IO::ReadLine( f, is, "#");
			is >> id >> ncn >> tabid[0] >> tabid[1];

			if ( id != ic+1)
				THROW_INTERNAL( "SMESH file corrupted - cell section");

			bcell.Reset();
			bcell.rTopoId() = idsurf;
			bcell.rId() = ++idcell;		// global cell id

			for ( MGSize k=0; k<BndGrid<DIM_2D>::GBndCell::SIZE; ++k)
				bcell.rNodeId(k) = tabid[k] + offset;

			mpGrd->rColCell().push_back( bcell );
		}

		offset += nnode;
	}

	cout << " - FINISHED\n";
}



template <>
void ReadBndSMESH<DIM_3D>::DoRead( const MGString& fname)
{
	mFName = fname;
	ifstream f( fname.c_str() );

	cout << "\nReading SMESH 3D'" << fname.c_str() << "'" << std::flush;

	istringstream is;


	//char	sbuf[1024];
	MGSize	nsurf, nnode, ncell, id, tabid[3], ncn, offset=0;
	Vect3D	vct;

	//IO::ReadLine( f, sbuf, "#");
	//sscanf( sbuf, "%d", &nsurf);
	IO::ReadLine( f, is, "#");
	is >> nsurf;

	MGSize	idnode = 0;
	MGSize	idcell = 0;

	for ( MGSize isurf=0; isurf<nsurf; ++isurf)
	{
		MGSize idsurf = isurf + 1;

		//IO::ReadLine( f, sbuf, "#");
		//IO::ReadLine( f, sbuf, "#");
		//sscanf( sbuf, "%d", &nnode);
		IO::ReadLine( f, is, "#");
		IO::ReadLine( f, is, "#");
		is >> nnode;


		TRACE( "nnode = " << nnode);
		mpGrd->rColNode().reserve( mpGrd->cColNode().size() + nnode);

		BndGrid<DIM_3D>::GBndNode	bnode;

		for ( MGSize in=0; in<nnode; ++in)
		{
			//IO::ReadLine( f, sbuf, "#");
			//sscanf( sbuf, "%d %lg %lg %lg", &id, &vct.rX(), &vct.rY(), &vct.rZ() );
			IO::ReadLine( f, is, "#");
			is >> id >> vct.rX() >> vct.rY() >> vct.rZ();

			if ( id != in+1)
				THROW_INTERNAL( "SMESH file corrupted - node section");

			bnode.rPos() = vct;
			bnode.rId() = ++idnode;		// global node id

			mpGrd->rColNode().push_back( bnode);
		}

		//IO::ReadLine( f, sbuf, "#");
		//sscanf( sbuf, "%d", &ncell);
		IO::ReadLine( f, is, "#");
		is >> ncell;

		TRACE( "ncell = " << ncell);
		mpGrd->rColCell().reserve( mpGrd->cColCell().size() + ncell );

		BndGrid<DIM_3D>::GBndCell	bcell;

		for ( MGSize ic=0; ic<ncell; ++ic)
		{
			//IO::ReadLine( f, sbuf, "#");
			//// TODO :: check number of nodes per face
			//sscanf( sbuf, "%d %d %d %d %d", &id, &ncn, &tabid[0], &tabid[1], &tabid[2] );
			// TODO :: check number of nodes per face
			IO::ReadLine( f, is, "#");
			is >> id >> ncn >> tabid[0] >> tabid[1] >> tabid[2];

			if ( id != ic+1)
				THROW_INTERNAL( "SMESH file corrupted - cell section");

			bcell.Reset();
			bcell.rTopoId() = idsurf;
			bcell.rId() = ++idcell;		// global cell id

			for ( MGSize k=0; k<BndGrid<DIM_3D>::GBndCell::SIZE; ++k)
				bcell.rNodeId(k) = tabid[k] + offset;

			mpGrd->rColCell().push_back( bcell );
		}

		offset += nnode;
	}

	cout << " - FINISHED\n";
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

