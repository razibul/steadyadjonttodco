#include "readtec.h"
#include "libcorecommon/regexp.h"
#include "libgreen/grid.h"
#include "libcoreio/store.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



const char STRTEC_TITLE[]		= "TITLE";
const char STRTEC_VARIABLES[]	= "VARIABLES";
const char STRTEC_ZONE[]		= "ZONE";
const char STRTEC_FEPOINT[]		= "FEPOINT";

const char STRTEC_TRIANGLE[]	= "TRIANGLE";
const char STRTEC_TETRAHEDRON[]	= "TETRAHEDRON";
const char STRTEC_BRICK[]		= "BRICK";



template <Dimension DIM>
void ReadTEC<DIM>::SetVariables( const MGString& str)
{
	MGString	strbuf = str;
	MGString	s1, s2;
	RegExp		rex;


	do
	{
		rex.InitPattern( "^(.*)[ \t,](.*)$" );
		rex.SetString( strbuf);

		if ( rex.IsOk())
		{
			strbuf = MGString( rex.GetSubString( 1).c_str() );
			s2 = MGString( rex.GetSubString( 2).c_str() );

			IO::Strip( strbuf, " \t,");
			IO::Strip( s2, " \t\"");

			mtabVars.insert( mtabVars.begin(), s2 );
		}
		else
		{
			IO::Strip( strbuf, " \t\"");
			mtabVars.insert( mtabVars.begin(), strbuf );
		}
	}
	while ( rex.IsOk());
}



template <Dimension DIM>
void ReadTEC<DIM>::ReadHeader( istream& file)
{
	istringstream is;

	MGString	skey, sbuf;
	
	RegExp		rex;
	MGString	params;
	MGString	pattern;
	MGString	strkey;
	MGString	strline;

	MGString	strvars;
	MGString	strtitle;
	MGString	*pline = NULL;





	while ( true )
	{
		IO::ReadLine( file, is, "#");

		sbuf = is.str();

		is >> skey;

		if ( skey == MGString(STRTEC_ZONE) )
			break;

		rex.InitPattern( "^(.*)=(.*)$" );
		rex.SetString( sbuf);

		if ( rex.IsOk())
		{
			strkey = rex.GetSubString( 1).c_str();
			strline = rex.GetSubString( 2).c_str();
		}
		else
		{
			strkey = "";
			strline = sbuf;
			if ( pline)
				*pline += " ";
		}

		IO::StripBlanks( strkey);

		if ( strkey == MGString(STRTEC_TITLE) )
		{
			pline = &strtitle;
		}
		else
		if ( strkey == MGString(STRTEC_VARIABLES) )
		{
			pline = &strvars;
		}

		if ( pline)
			*pline += strline;
	}

	IO::StripBlanks( strtitle);
	IO::StripBlanks( strvars);

	SetTitle( strtitle);
	SetVariables( strvars);


	if ( mTitle.size() > 0)
		cout << "title = '" << mTitle << "'" << endl;

	cout << "variables found = ";
	for ( MGSize i=0; i<mtabVars.size(); ++i)
		cout << "'" << mtabVars[i] << "' ";
	cout << endl;

	// searching for the zone type
	rex.InitPattern( ".*ZONE.*F[ \t]*=[ \t]*([^ \t,]*)" );
	rex.SetString( sbuf);

	if ( rex.IsOk() == false)
		THROW_FILE( "rex failed to find the pattern in ReadTEC()", sbuf);

	mZoneT = rex.GetSubString( 1);

	cout << "zone type = " << mZoneT << endl;
	if ( mZoneT != MGString(STRTEC_FEPOINT) )
	{
		ostringstream os;
		os << "unsupported zone type - " << mZoneT;
		THROW_FILE( mFName, os.str());
	}


	// searching for the cell type
	rex.InitPattern( ".*ZONE.*ET[ \t]*=[ \t]*([^ \t,]*)" );
	rex.SetString( sbuf);

	if ( rex.IsOk() == false)
		THROW_FILE( "rex failed to find the pattern in ReadTEC()", sbuf);

	mElemT = rex.GetSubString( 1);
	cout << "cell type = " << mElemT << endl;


	// searching for the number of nodes
	rex.InitPattern( ".*ZONE.*N[ \t]*=[ \t]*([^ \t,]*)" );
	rex.SetString( sbuf);
	if ( rex.IsOk() == false)
		THROW_FILE( "rex failed to find the pattern in ReadTEC()", sbuf);

	is.clear();
	is.str( rex.GetSubString( 1).c_str() );
	is >> mnNode;
	cout << "n of nodes = " << mnNode << endl;


	// searching for the number of cells
	rex.InitPattern( ".*ZONE.*E[ \t]*=[ \t]*([0-9]*)" );
	rex.SetString( sbuf);
	if ( rex.IsOk() == false)
		THROW_FILE( "rex failed to find the pattern in ReadTEC()", sbuf);

	is.clear();
	is.str( rex.GetSubString( 1).c_str() );
	is >> mnElem;
	cout << "n of elements = " << mnElem << endl;
}


template <Dimension DIM>
void ReadTEC<DIM>::ReadNodes( istream& file)
{
	istringstream is;

	mGrdProxy.IOTabNodeResize( mnNode);

	if ( mpContext)
	{
		mpContext->Resize( mnNode );
		if ( mtabVars.size() < DIM*DIM + DIM )
			THROW_INTERNAL("it is not a metric tecplot data '" << mFName << "'");
	}

	vector<MGFloat> tabx(DIM);

	for ( MGSize i=0; i<mnNode; ++i)
	{
		IO::ReadLine( file, is, "#");

		for ( MGSize k=0; k<DIM; ++k)
		{
			is >> tabx[k];
		}

		mGrdProxy.IOSetNodePos( tabx, i+1);

		if ( mpContext)
		{
			vector<MGFloat> tabv(DIM*DIM);
			for ( MGSize kr=0; kr<DIM; ++kr)
				for ( MGSize kc=0; kc<DIM; ++kc)
					is >> mpContext->rData(i+1)(kr,kc);
		}

		//for ( MGSize k=0; k<neq; ++k)
		//{
		//	sscanf( sbuf, "%lg %[^\n]", &d, sbuf);
		//	vect(k) = d;
		//}
		//mpGrd->PushBackState( vect);
	}


}


template <Dimension DIM>
void ReadTEC<DIM>::ReadCells( istream& file)
{
	istringstream is;

	mGrdProxy.IOTabCellResize( DIM+1, mnElem);

	vector<MGSize> tabid(DIM+1);

	for ( MGSize i=0; i<mnElem; ++i)
	{
		IO::ReadLine( file, is, "#");

		for ( MGSize k=0; k<=DIM; ++k)
		{
			is >> tabid[k];
		}

		mGrdProxy.IOSetCellIds( tabid, DIM+1, i+1);
	}
}



template <Dimension DIM>
void ReadTEC<DIM>::DoRead( const MGString& fname)
{
	ifstream ifile;


	try
	{
		cout << "Reading TEC '" << fname.c_str() << "'" << endl;//std::flush;

		ifile.open( fname.c_str(), ios::in );

		if ( ! ifile)
			THROW_FILE( "can not open the file", fname);

		ReadHeader( ifile);
		ReadNodes( ifile);
		ReadCells( ifile);

		cout << " - FINISHED\n";
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: ReadTEC<DIM>::DoRead - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}

}



template class ReadTEC<DIM_2D>;
template class ReadTEC<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

