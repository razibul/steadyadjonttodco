#ifndef __SPOINTDESTRUCTOR_H__
#define __SPOINTDESTRUCTOR_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

	
template <Dimension DIM>
class DestroyerV2;



//////////////////////////////////////////////////////////////////////
// class SPointDestructor
//////////////////////////////////////////////////////////////////////
class SPointDestructor
{
public:
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

public:
	SPointDestructor( const MGSize& inod) : mINode(inod), mflagL(0), mflagR(0)	{}

	void	Init( const vector< Key<2> >& tabsedge);
	bool	Delete();
	void	UpdateSubEdges( vector< Key<2> >& tabsedge);

private:
	void	FindComplement( vector<MGSize>& tabcomp, vector<MGSize>& tabshell, const MGSize& idtarget);

	void	Analyze( MGSize& flag, const MGSize& idtarget);


public:
	DestroyerV2<DIM_3D>	*mpParent;

	MGSize				mINode;
	MGSize				mINodeL;
	MGSize				mINodeR;

	Key<2>				mEdge;
	Key<2>				mEdgeL;
	Key<2>				mEdgeR;

	vector<MGSize>			mtabBall;
	vector< Face<DIM_3D> >	mtabBFace;

	MGSize	mflagL;
	MGSize	mflagR;

	vector<MGSize>			mtabCompl_L;
	vector<MGSize>			mtabShell_L;
	vector<MGSize>			mtabCompl_R;
	vector<MGSize>			mtabShell_R;

};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __SPOINTDESTRUCTOR_H__
