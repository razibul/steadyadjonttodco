#include "edgesplit.h"

#include "libgreen/kernel.h"
#include "libgreen/gridgeom.h"
#include "libgreen/controlspace.h"

#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 





bool EdgeSplit::IsSubEdgeRecovered( const Key<2>& edge)
{
	vector<MGSize>	tabball;
	vector<MGSize>	tabnode;

	tabnode.push_back( edge.cFirst() );
	tabnode.push_back( edge.cSecond() );
	
	if ( ! mpKernel->FindBall( tabball, edge.cFirst() ) )	
	//if ( ! mpKernel->FindBall( tabball, tabnode ) )	
	{
		cout << "failed to find ball [" << edge.cFirst() << ", " << edge.cSecond() << "]\n";
		TRACE( "failed to find ball [" << edge.cFirst() << ", " << edge.cSecond() << "]\n"  );

		THROW_INTERNAL( "failed to find ball");
		
		return false;
	}
	
	//// dump
	//WriteGrdTEC<DIM_3D>	write( & mpKernel->rGrid() );
	//write.WriteCells( "_ball_rec.dat", tabball, "ball");


	//vector< Key<2> > tabedge;
	//tabedge.reserve( GCell::ESIZE * tabball.size() );

	for ( MGSize i=0; i<tabball.size(); ++i)
	{
		const GCell& cell = mpKernel->rGrid().cCell( tabball[i] );
	
		for ( MGSize k=0; k<GCell::ESIZE; ++k)
		{
			Key<2> key = cell.EdgeKey( k);
			key.Sort();

			if ( key == edge)
				return true;
			//tabedge.push_back( key);
		}
	}

	return false;


	////ofstream of( "-dump.txt");
	////of << edge.cFirst() << " " << edge.cSecond() << endl << endl;
	////for ( MGSize i=0; i<tabedge.size(); ++i)
	////	of << i << "   -  " << tabedge[i].cFirst() << " " << tabedge[i].cSecond() << endl;

	//sort( tabedge.begin(), tabedge.end() );

	//bool bFound = binary_search( tabedge.begin(), tabedge.end(), edge);
	////vector< Key<2> >::iterator itrlast = unique( tabedge.begin(), tabedge.end() );
	////bool bFound = binary_search( tabedge.begin(), itrlast, edge);

	//return bFound;
}



void EdgeSplit::FindCrossings( vector<GVect>& tabcross)
{
}





bool EdgeSplit::IsRecoverd()
{
	for ( MGSize i=0; i<mtabSubEdges.size(); ++i)
		if ( ! IsSubEdgeRecovered( mtabSubEdges[i] ) )
			return false;

	return true;
}

void EdgeSplit::UpdateSubElems()
{
	//sort internal nodes
	vector< pair<MGFloat,MGSize> > tabsort;
	tabsort.reserve( mtabINodes.size() + 2 );

	GVect vct1 = mpKernel->cGrid().cNode( mEdge.cFirst() ).cPos();
	GVect vct2 = mpKernel->cGrid().cNode( mEdge.cSecond() ).cPos();

	tabsort.push_back( pair<MGFloat,MGSize>( 0.0, mEdge.cFirst() ) );

	for ( MGSize i=0; i<mtabINodes.size(); ++i)
	{
		GVect vct = mpKernel->cGrid().cNode( mtabINodes[i] ).cPos();
		MGFloat dist = (vct - vct1).module();

		tabsort.push_back( pair<MGFloat,MGSize>( dist, mtabINodes[i] ) );
	}

	tabsort.push_back( pair<MGFloat,MGSize>( (vct2-vct1).module(), mEdge.cSecond() ) );


	sort( tabsort.begin(), tabsort.end() );

	// fill array of sub-edges
	mtabSubEdges.clear();
	mtabSubEdges.reserve( tabsort.size()-1 );
	for ( MGSize i=1; i<tabsort.size(); ++i)
	{
		Key<2> edge( tabsort[i-1].second, tabsort[i].second );
		edge.Sort();
		mtabSubEdges.push_back( edge);
	}
}



void EdgeSplit::FindBestCrossings( GVect& vcross, Key<3>& face, const Key<2>& edge)
{
	vector<MGSize> tabpipe, tabbase;
	mpKernel->FindEdgePipe( tabpipe, edge);


	//WriteGrdTEC<DIM_3D>	write( & mpKernel->rGrid() );
	//write.WriteEdges( "_spliteedge_subedges.dat", mtabSubEdges, "subedges");
	//write.WriteCells( "_spliteedge_pipe.dat", tabpipe, "pipe");


	vector< pair< GVect,Key<3> > >	tabcross;
	vector< Key<3> > tabface;

	for ( MGSize i=0; i<tabpipe.size(); ++i)
	{
		const GCell& cell = mpKernel->cGrid().cCell( tabpipe[i] );

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			Key<3> iface = cell.FaceKey( k);
			iface.Sort();

			if ( iface.cFirst()  != edge.cFirst() && iface.cFirst()  != edge.cSecond() &&
				 iface.cSecond() != edge.cFirst() && iface.cSecond() != edge.cSecond() &&
				 iface.cThird()  != edge.cFirst() && iface.cThird()  != edge.cSecond() )
			{
				tabface.push_back( iface);
			}
		}
	}

	//cout << endl << "before" << endl;
	//for ( MGSize i=0; i<tabface.size(); ++i)
	//	cout << tabface[i].cFirst() << " " << tabface[i].cSecond() << " " << tabface[i].cThird() << endl;

	sort( tabface.begin(), tabface.end() );
	tabface.erase( unique( tabface.begin(), tabface.end() ), tabface.end() );

	//cout << endl << "after" << endl;
	//for ( MGSize i=0; i<tabface.size(); ++i)
	//	cout << tabface[i].cFirst() << " " << tabface[i].cSecond() << " " << tabface[i].cThird() << endl;
	//THROW_INTERNAL( "STOP");

	GVect ve1 = mpKernel->cGrid().cNode( edge.cFirst() ).cPos();
	GVect ve2 = mpKernel->cGrid().cNode( edge.cSecond() ).cPos();

	for ( MGSize i=0; i<tabface.size(); ++i)
	{
		const Key<3>& iface = tabface[i];
		GVect vf1 = mpKernel->cGrid().cNode( iface.cFirst() ).cPos();
		GVect vf2 = mpKernel->cGrid().cNode( iface.cSecond() ).cPos();
		GVect vf3 = mpKernel->cGrid().cNode( iface.cThird() ).cPos();

		if ( FindEdgeFaceCross( vcross, ve1, ve2, vf1, vf2, vf3 ) )
		{
			tabcross.push_back( pair< GVect,Key<3> >( vcross, iface) );
		}
	}

	//for ( MGSize i=0; i<tabcross.size(); ++i)
	//	cout << tabcross[i].first.cX() << " " << tabcross[i].first.cY() << " " << tabcross[i].first.cZ()<< " - " 
	//	<< tabcross[i].second.cFirst() << " "<< tabcross[i].second.cSecond() << " "<< tabcross[i].second.cThird() << " " 
	//	<<  endl;

	//THROW_INTERNAL( "STOP");


	//cout << tabcross.size() << endl;


	GVect vct1 = mpKernel->cGrid().cNode( edge.cFirst() ).cPos();
	GVect vct2 = mpKernel->cGrid().cNode( edge.cSecond() ).cPos();
	GVect vm = 0.5 * ( vct1 + vct2);

	//vcross = vm;
	//return;

	if ( tabcross.size() == 0 )
		THROW_INTERNAL( "tabcross.size() == 0" );

	vcross = tabcross[0].first;
	face = tabcross[0].second;
	MGFloat dist = (vcross - vm).module();

	for ( MGSize i=1; i<tabcross.size(); ++i)
	{
		MGFloat curdist = (tabcross[i].first - vm).module();
		if ( curdist < dist )
		{
			dist = curdist;
			vcross = tabcross[i].first;
			face = tabcross[i].second;
		}
	}


	//mpKernel->FindBase( tabbase, vcross);
	//write.WriteCells( "_spliteedge_base.dat", tabpipe, "base");

		cout << edge.cFirst() << " "<< edge.cSecond() << " - " << tabcross.size() << " - "
		<< vcross.cX() << " " << vcross.cY() << " " << vcross.cZ() << " - " 
		<< face.cFirst() << " "<< face.cSecond() << " "<< face.cThird() << " " 
		<<  endl;
}



void EdgeSplit::Recover( ProtectEntities& prent)
{
	MGSize maxpassno = 1000;

	for ( MGSize i=0; i<maxpassno; ++i)
	{
		bool bBreak = true;

		for ( MGSize i=0; i<mtabSubEdges.size(); ++i)
		{
			const Key<2>& edge = mtabSubEdges[i];

			if ( prent.IsEdgeProtected( edge ) )
				continue;

			if ( ! IsSubEdgeRecovered( edge ) )
			{
				bBreak = false;

				Key<3> cface;
				GVect vct;
				FindBestCrossings( vct, cface, edge);

				WriteGrdTEC<DIM_3D>	write( & mpKernel->rGrid() );


				MGSize inod = mpKernel->InsertPointProtect( vct, prent.cSetProEdges(), prent.cSetProFaces() );

				if ( ! inod)
				{
					THROW_INTERNAL( "CRASH :: kernel failed to insert new point with coordinates [" 
						<< vct.cX() << " " << vct.cY() << " " << vct.cZ() << "]" );
				}

				mpKernel->InsertMetric( mpCSpace->GetSpacing( vct), inod );


				mtabINodes.push_back( inod);

				//Key<2> kedge;

				//kedge = Key<2>( edge.cFirst(), inod );
				//kedge.Sort();
				//if ( IsSubEdgeRecovered( kedge) )
				//	prent.AddEdge( kedge);

				//kedge = Key<2>( inod, edge.cSecond() );
				//kedge.Sort();
				//if ( IsSubEdgeRecovered( kedge) )
				//	prent.AddEdge( kedge);


				break;
			}
			else
			{
				prent.AddEdge( edge );
			}

		}

		UpdateSubElems();


		if ( bBreak)
			break;
	}
}



//void EdgeSplit::InsertNode( const MGSize& id, const Grid<DIM_3D>& grid)
//{
//	mtabINodes.push_back( id);
//
//	//sort internal nodes
//	vector< pair<MGFloat,MGSize> > tabsort( mtabINodes.size() );
//	GVect vct1 = grid.cNode( mEdge.cFirst() ).cPos();
//
//	for ( MGSize i=0; i<mtabINodes.size(); ++i)
//	{
//		GVect vct = grid.cNode( mtabINodes[i] ).cPos();
//		MGFloat dist = (vct - vct1).module();
//
//		tabsort[i] = pair<MGFloat,MGSize>( dist, mtabINodes[i] );
//	}
//
//	sort( tabsort.begin(), tabsort.end() );
//
//	for ( MGSize i=0; i<mtabINodes.size(); ++i)
//		mtabINodes[i] = tabsort[i].second;
//
//}


void EdgeSplit::ExportTEC( const MGString& name)
{
	//WriteGrdTEC<DIM_3D>	write( & mpKernel->rGrid() );
	//write.WriteCells( "_ball_rec.dat", tabball, "ball");
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

