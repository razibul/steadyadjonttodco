#ifndef __TDEFS_H__
#define __TDEFS_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libcorecommon/key.h"
#include "libcoregeom/vect.h"
#include "libcoregeom/metric.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

using namespace Geom;


template <Dimension DIM>
class Cell;

template <Dimension DIM>
class Node;

template <Dimension DIM>
class Face;

template <Dimension DIM>
class BndCell;

template <Dimension DIM>
class BndNode;


typedef Key<2>	GEdge;
typedef Key<2>	GBndEdge;


//////////////////////////////////////////////////////////////////////
//	class Base
//////////////////////////////////////////////////////////////////////
class Base
{
public:
	Base( const MGSize& id=0) : mId(id)	{}

	MGSize&			rId()		{ return mId;}
	const MGSize&	cId() const	{ return mId;}

private:
	MGSize		mId;
};
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
//  class TDefs
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class TDefs
{
public:
	typedef Vect<DIM>		GVect;
	typedef Metric<DIM>		GMetric;

	typedef Node<DIM>		GNode;
	typedef Cell<DIM>		GCell;
	typedef Face<DIM>		GFace;

	typedef BndNode<DIM>	GBndNode;
	typedef BndCell<DIM>	GBndCell;

};
//////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
////  class GEdge
////////////////////////////////////////////////////////////////////////
//class GEdge : public Key<2>
//{
//public:
//	GEdge() : Key<2>()											{}
//	GEdge( const Key<2>& key) : Key<2>(key)						{}
//	GEdge( const MGSize& i1, const MGSize& i2) : Key<2>(i1,i2)	{}
//
//};
////////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __TDEFS_H__
