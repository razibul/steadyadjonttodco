#ifndef __UNIFORMCSPACE_H__
#define __UNIFORMCSPACE_H__

#include "libgreen/controlspace.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class UniformControlSpace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class UniformControlSpace : public ControlSpace<DIM>
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

public:
	UniformControlSpace( const GMetric& met) : mMetric(met)	{}
	UniformControlSpace( const double& h)					{ mMetric.InitIso( h);}

	virtual ~UniformControlSpace()							{}

	virtual GMetric	GetSpacing( const GVect& vct) const		{ return mMetric;}

private:
	GMetric	mMetric;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __UNIFORMCSPACE_H__
