#ifndef __GEOMCSPACE_H__
#define __GEOMCSPACE_H__

#include "libgreen/tdefs.h"
#include "libextget/geomentity.h"
#include "libgreen/controlspace.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

//////////////////////////////////////////////////////////////////////
//	class GeomControlSpace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, Dimension UNIDIM>
class GeomControlSpace : public ControlSpace<DIM>
{
	typedef typename TDefs<UNIDIM>::GVect	UVect;
	typedef typename TDefs<UNIDIM>::GMetric	UMetric;

	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

public:
	GeomControlSpace( const GET::GeomEntity<DIM,UNIDIM>& gent) : mpGEnt( &gent)	{}
	virtual ~GeomControlSpace()		{}

	virtual GMetric	GetSpacing( const GVect& vct) const;

protected:
	GeomControlSpace() : mpGEnt(NULL)	{}

private:
	const GET::GeomEntity<DIM,UNIDIM>	*mpGEnt;
};
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
//	class NullGeomControlSpace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, Dimension UNIDIM>
class NullGeomControlSpace : public GeomControlSpace<DIM,UNIDIM>
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

public:
	NullGeomControlSpace()		{}
	virtual ~NullGeomControlSpace()		{}

	virtual GMetric	GetSpacing( const GVect& vct) const
	{
		GMetric met;
		met.InitIso(0.04);
		return met;
	}
};
//////////////////////////////////////////////////////////////////////



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GEOMCSPACE_H__
