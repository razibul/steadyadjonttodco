#include "facesplit.h"
#include "libgreen/kernel.h"
#include "libgreen/grid.h"
#include "libgreen/gridgeom.h"
#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




void FaceSplit::UpdateSubFaces( Kernel<DIM_3D>& kernel )
{
	GVect vf1 = kernel.rGrid().cNode( mFace.cFirst() ).cPos();
	GVect vf2 = kernel.rGrid().cNode( mFace.cSecond() ).cPos();
	GVect vf3 = kernel.rGrid().cNode( mFace.cThird() ).cPos();

	GVect ve1 = vf2 - vf1;
	GVect ve2 = vf3 - vf1;

	GVect vn = (ve1 % ve2).versor();
	GVect vox = ve1.versor();
	GVect voy = (vn % vox).versor();;


	// create tab of all face node-ids
	vector<MGSize>	tabidn;

	for ( MGSize i=0; i<3; ++i)
		tabidn.push_back( mFace.cElem(i) );

	for ( MGSize i=0; i<3; ++i)
		for ( MGSize k=0; k<mtabSplitEdge[i]->mtabINodes.size(); ++k)
			tabidn.push_back( mtabSplitEdge[i]->mtabINodes[k] );

	for ( MGSize i=0; i<mtabINodes.size(); ++i)
		tabidn.push_back( mtabINodes[i] );

	sort( tabidn.begin(), tabidn.end() );


	// face boundary edges
	vector< Key<2> > tabbnd;

	for ( MGSize i=0; i<3; ++i)
		for ( MGSize k=0; k<mtabSplitEdge[i]->Size(); ++k)
		{
			Key<2> key = mtabSplitEdge[i]->cSubEdge( k);
			tabbnd.push_back( key);
		}

	sort( tabbnd.begin(), tabbnd.end() );


	// find ball
	vector<MGSize>	tabcell;
	kernel.FindBall( tabcell, tabidn);
	sort( tabcell.begin(), tabcell.end() );

	// find slivers
	vector<MGSize>	tabsliver;
	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		const GCell& cell = kernel.rGrid().cCell( tabcell[i] );

		if (	( binary_search( tabidn.begin(), tabidn.end(), cell.cNodeId(0) ))
			&&	( binary_search( tabidn.begin(), tabidn.end(), cell.cNodeId(1) )) 
			&&	( binary_search( tabidn.begin(), tabidn.end(), cell.cNodeId(2) )) 
			&&	( binary_search( tabidn.begin(), tabidn.end(), cell.cNodeId(3) )) )
		{
			// sliver founed !!!
			tabsliver.push_back( tabcell[i] );

			Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, kernel.rGrid() );
			if ( tet.Volume() != 0.0 )
			{
				cout << "tet.Volume() = " << tet.Volume() << endl;
				//THROW_INTERNAL( "------------ sliver volume corrupted !!!" );
			}
		}
	}

	sort( tabsliver.begin(), tabsliver.end() );

	if ( tabsliver.size() > 0 )
		cout << "--- slivers ; " << tabsliver.size() << endl;


	// find slivers faces
	vector< Key<3> > tabslivfaces;
	//vector< Key<3> > tabslivfacesgood;

	for ( MGSize i=0; i<tabsliver.size(); ++i)
	{
		const GCell& cell = kernel.rGrid().cCell( tabsliver[i] );

		for ( MGSize k=0; k<GCell::SIZE; ++k )
		{
			Key<3> face = cell.FaceKey( k);
			face.Sort();
			tabslivfaces.push_back( face);
		}
	}

	sort( tabslivfaces.begin(), tabslivfaces.end() );


	///////////////////////////////////////////////////////
	// find all existing subfaces

	set< Key<3> > setsubface;
	set< Key<2> > setinedge;


	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		const GCell& cell = kernel.rGrid().cCell( tabcell[i] );

		for ( MGSize k=0; k<GCell::SIZE; ++k )
		{
			Key<3> face = cell.FaceKey( k);

			// check if the face is coplanar
			bool bFound = true;
			for ( MGSize j=0; j<3; ++j)
				if ( ! binary_search( tabidn.begin(), tabidn.end(), face.cElem(j) ) )
					bFound = false;

			// it is coplanar
			if ( bFound)
			{
				face.Sort();

				if ( setsubface.find( face ) != setsubface.end() )
					continue;


				bool bOk = true;

				for ( MGSize k=0; k<face.Size(); ++k)
				{
					Key<2> edge( face.cFirst(), face.cSecond() );
					edge.Sort();

					if ( ! binary_search( tabbnd.begin(), tabbnd.end(), edge) && setinedge.find( edge) == setinedge.end() )
					{
						GVect vt1 = kernel.rGrid().cNode( edge.cFirst() ).cPos();
						GVect vt2 = kernel.rGrid().cNode( edge.cSecond() ).cPos();

						Vect2D wt1( vox*vt1, voy*vt1 );
						Vect2D wt2( vox*vt2, voy*vt2 );

						// check for crossing
						for ( set< Key<2> >::iterator itr=setinedge.begin(); itr!=setinedge.end(); ++itr )
						{
							const Key<2>& oedge = *itr;

							GVect vc1 = kernel.rGrid().cNode( oedge.cFirst() ).cPos();
							GVect vc2 = kernel.rGrid().cNode( oedge.cSecond() ).cPos();

							Vect2D wc1( vox*vc1, voy*vc1 );
							Vect2D wc2( vox*vc2, voy*vc2 );

							SimplexFace<DIM_2D> sf( wc1, wc2);

							if ( sf.CrossedVol( wt1, wt2) > 0.0 )
							{
								bOk = false;
								break;
							}
						}
					}

					face.Rotate(1);
				}

				// face is not crossing any other
				if ( bOk)
				{
					// add face to subfaces
					setsubface.insert( face);

					// add its edges to setinedge
					for ( MGSize k=0; k<face.Size(); ++k)
					{
						Key<2> edge( face.cFirst(), face.cSecond() );
						edge.Sort();

						if ( ! binary_search( tabbnd.begin(), tabbnd.end(), edge) )
							setinedge.insert( edge);

						face.Rotate(1);
					}
				}
			}
		}
	}

	mtabSubFaces.clear();
	for ( set< Key<3> >::iterator itr = setsubface.begin(); itr != setsubface.end(); ++itr )
		mtabSubFaces.push_back( *itr);



//////////////////////////////////
//
//	mtabSubFaces.clear();
//
//	for ( MGSize i=0; i<tabcell.size(); ++i)
//	{
//		const GCell& cell = kernel.rGrid().cCell( tabcell[i] );
//
//		for ( MGSize k=0; k<GCell::SIZE; ++k )
//		{
//			Key<3> face = cell.FaceKey( k);
//
//			bool bFound = true;
//			for ( MGSize j=0; j<3; ++j)
//				if ( ! binary_search( tabidn.begin(), tabidn.end(), face.cElem(j) ) )
//					bFound = false;
//				//if ( find( tabidn.begin(), tabidn.end(), face.cElem(j) ) == tabidn.end() )
//				//	bFound = false;
//
//			if ( bFound)
//			{
//				face.Sort();
//				if ( ! binary_search( tabslivfaces.begin(), tabslivfaces.end(), face ) )
//					mtabSubFaces.push_back( face);
//			}
//		}
//	}
//
//	sort( mtabSubFaces.begin(), mtabSubFaces.end() );
//	vector< Key<3> >::iterator itrfend = unique( mtabSubFaces.begin(), mtabSubFaces.end() );
//	mtabSubFaces.erase( itrfend, mtabSubFaces.end() );
//
//	// find internal edges
//	set< Key<2> > setinedge;
//
//	for ( MGSize i=0; i<mtabSubFaces.size(); ++i)
//	{
//		Key<3> face = mtabSubFaces[i];
//		for ( MGSize k=0; k<face.Size(); ++k)
//		{
//			Key<2> edge( face.cFirst(), face.cSecond() );
//			edge.Sort();
//
//			if ( ! binary_search( tabbnd.begin(), tabbnd.end(), edge) )
//				setinedge.insert( edge);
//
//			face.Rotate(1);
//		}
//	}
//
//	// choose sliver faces
//	for ( MGSize i=0; i<tabslivfaces.size(); ++i)
//	{
//		Key<3> face = tabslivfaces[i]; // allready sorted
//
//		if ( binary_search( mtabSubFaces.begin(), mtabSubFaces.end(), face ) )
//			continue;
//
//
//		bool bOk = true;
//
//		for ( MGSize k=0; k<face.Size(); ++k)
//		{
//			Key<2> edge( face.cFirst(), face.cSecond() );
//			edge.Sort();
//
//			if ( ! binary_search( tabbnd.begin(), tabbnd.end(), edge) )
//			{
//				// check for crossing
//				for ( set< Key<2> >::iterator itr=setinedge.begin(); itr!=setinedge.end(); ++itr )
//				{
//					const Key<2>& oedge = *itr;
//
//					//if ( oedge, edge ARE CROSSED )
//					{
//						bOk = false;
//						break;
//					}
//				}
//			}
//
//			face.Rotate(1);
//		}
//
//		if ( bOk)
//		{
//			// add face to subfaces
//			mtabSubFaces.push_back( face);
//			sort( mtabSubFaces.begin(), mtabSubFaces.end() );
//
//			// add its edges to setinedge
//			for ( MGSize k=0; k<face.Size(); ++k)
//			{
//				Key<2> edge( face.cFirst(), face.cSecond() );
//				edge.Sort();
//
//				if ( ! binary_search( tabbnd.begin(), tabbnd.end(), edge) )
//					setinedge.insert( edge);
//
//				face.Rotate(1);
//			}
//		}
//	}
//

	TRACE( "mtabSubFaces.size = " << mtabSubFaces.size() );

}










void FaceSplit::UpdateSubFaces_new( Kernel<DIM_3D>& kernel )
{
	// create tab of all face node-ids
	vector<MGSize>	tabidn;

	for ( MGSize i=0; i<3; ++i)
		tabidn.push_back( mFace.cElem(i) );

	for ( MGSize i=0; i<3; ++i)
		for ( MGSize k=0; k<mtabSplitEdge[i]->mtabINodes.size(); ++k)
			tabidn.push_back( mtabSplitEdge[i]->mtabINodes[k] );

	for ( MGSize i=0; i<mtabINodes.size(); ++i)
		tabidn.push_back( mtabINodes[i] );

	sort( tabidn.begin(), tabidn.end() );

	//vector<MGSize>	tabcell2;
	//kernel.FindBall( tabcell2, tabidn);

	// find ball
	vector<MGSize>	tabcell;
	kernel.FindBall( tabcell, tabidn);
	sort( tabcell.begin(), tabcell.end() );


	// find slivers
	vector<MGSize>	tabsliver;
	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		const GCell& cell = kernel.rGrid().cCell( tabcell[i] );

		if (	( binary_search( tabidn.begin(), tabidn.end(), cell.cNodeId(0) ))
			&&	( binary_search( tabidn.begin(), tabidn.end(), cell.cNodeId(1) )) 
			&&	( binary_search( tabidn.begin(), tabidn.end(), cell.cNodeId(2) )) 
			&&	( binary_search( tabidn.begin(), tabidn.end(), cell.cNodeId(3) )) )
		{
			// sliver founed !!!
			tabsliver.push_back( tabcell[i] );

			Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, kernel.rGrid() );
			if ( tet.Volume() != 0.0 )
			{
				cout << "tet.Volume() = " << tet.Volume() << endl;
				THROW_INTERNAL( "sliver volume corrupted !!!" );
			}
		}
	}

	sort( tabsliver.begin(), tabsliver.end() );

	if ( tabsliver.size() > 0 )
		cout << "--- slivers ; " << tabsliver.size() << endl;

	// find slivers faces
	vector< Key<3> > tabslivfaces;
	//vector< Key<3> > tabslivfacesgood;

	for ( MGSize i=0; i<tabsliver.size(); ++i)
	{
		const GCell& cell = kernel.rGrid().cCell( tabsliver[i] );
		//Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, kernel.rGrid() );

		//GVect v0 = tet.GetFace(0).Vn();

		//tabslivfacesgood.push_back( cell.FaceKey( 0) );

		//for ( MGSize k=1; k<GCell::SIZE; ++k )
		//{
		//	GVect vcur = tet.GetFace(k).Vn();

		//	if ( vcur * v0 > 0.0 )
		//		tabslivfacesgood.push_back( cell.FaceKey( k) );
		//}

		//if ( tabslivfacesgood.size() != 2 )
		//{
		//	cout << tabslivfacesgood.size() << endl;
		//	THROW_INTERNAL( "tabslivfacesgood.size() != 2" );
		//}

		for ( MGSize k=0; k<GCell::SIZE; ++k )
		{
			Key<3> face = cell.FaceKey( k);
			face.Sort();
			tabslivfaces.push_back( face);
		}
	}

	sort( tabslivfaces.begin(), tabslivfaces.end() );

	//for ( MGSize i=0; i<tabidn.size(); ++i)
	//{
	//	vector<MGSize>	tab;

	//	kernel.FindBall( tab, tabidn[i] );

	//	for ( MGSize k=0; k<tab.size(); ++k)
	//		tabcell.push_back( tab[k] );
	//}

	//sort( tabcell.begin(), tabcell.end() );
	//vector<MGSize>::iterator itr = unique( tabcell.begin(), tabcell.end() );

	//tabcell.erase( itr, tabcell.end() );

	//cout << tabcell.size() << " " << tabcell2.size() << endl;
	////THROW_INTERNAL( "STOP");


	// find all existing subfaces
	mtabSubFaces.clear();

	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		const GCell& cell = kernel.rGrid().cCell( tabcell[i] );

		// slivers found
		if ( binary_search( tabsliver.begin(), tabsliver.end(), tabcell[i] ) )
		{
			Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, kernel.rGrid() );
			GVect v0 = tet.GetFace(0).Vn();

			Key<3> face = cell.FaceKey( 0); 
			face.Sort();

			mtabSubFaces.push_back( face);

			for ( MGSize k=1; k<GCell::SIZE; ++k )
			{
				GVect vcur = tet.GetFace(k).Vn();

				if ( vcur * v0 > 0.0 )
				{
					face = cell.FaceKey( k); 
					face.Sort();

					mtabSubFaces.push_back( face);
					break;
				}
			}

			continue;
		}


		for ( MGSize k=0; k<GCell::SIZE; ++k )
		{
			Key<3> face = cell.FaceKey( k);

			bool bFound = true;
			for ( MGSize j=0; j<3; ++j)
				if ( ! binary_search( tabidn.begin(), tabidn.end(), face.cElem(j) ) )
					bFound = false;
				//if ( find( tabidn.begin(), tabidn.end(), face.cElem(j) ) == tabidn.end() )
				//	bFound = false;

			if ( bFound)
			{
				face.Sort();
				if ( ! binary_search( tabslivfaces.begin(), tabslivfaces.end(), face ) )
					mtabSubFaces.push_back( face);
			}
		}
	}

	sort( mtabSubFaces.begin(), mtabSubFaces.end() );
	vector< Key<3> >::iterator itrfend = unique( mtabSubFaces.begin(), mtabSubFaces.end() );
	mtabSubFaces.erase( itrfend, mtabSubFaces.end() );

	TRACE( "mtabSubFaces.size = " << mtabSubFaces.size() );
}


bool FaceSplit::IsRecoverd()
{
	vector< Key<2> > tabbnd;
	vector< Key<2> > taball;
	vector< Key<2> > tabinn;
	vector< Key<2> > tabdup;

	vector< Key<2> >::iterator itrend;

	// face boundary edges
	for ( MGSize i=0; i<3; ++i)
		for ( MGSize k=0; k<mtabSplitEdge[i]->Size(); ++k)
		{
			Key<2> key = mtabSplitEdge[i]->cSubEdge( k);
			tabbnd.push_back( key);
		}

	sort( tabbnd.begin(), tabbnd.end() );

	// all face edges
	for ( MGSize i=0; i<mtabSubFaces.size(); ++i)
	{
		Key<3> face = mtabSubFaces[i];
		for ( MGSize k=0; k<3; ++k)
		{
			Key<2> edge( face.cFirst(), face.cSecond() );
			edge.Sort();

			taball.push_back( edge);

			face.Rotate(1);
		}
	}

	sort( taball.begin(), taball.end() );

	tabinn.resize( taball.size() );
	itrend = set_difference( taball.begin(), taball.end(), tabbnd.begin(), tabbnd.end(), tabinn.begin() );
	tabinn.erase( itrend, tabinn.end() );


	itrend = unique( taball.begin(), taball.end() );
	taball.erase( itrend, taball.end() );

	tabdup.resize( taball.size() );
	itrend = set_difference( tabinn.begin(), tabinn.end(), taball.begin(), taball.end(), tabdup.begin() );
	tabdup.erase( itrend, tabdup.end() );

	itrend = unique( tabinn.begin(), tabinn.end() );
	tabinn.erase( itrend, tabinn.end() );


	mtabTmpEdges.clear();

	// search for all face boundary edges
	bool bBnd = true;
	for ( MGSize i=0; i<tabbnd.size(); ++i)
	{
		Key<2> edge = tabbnd[i];

		//if ( find( taball.begin(), taball.end(), edge) == taball.end() )
		if ( ! binary_search( taball.begin(), taball.end(), edge) )
		{
			bBnd = false;
			mtabTmpEdges.push_back( edge );
		}
	}

	// test all face inner edges
	bool bInn = true;

	for ( vector< Key<2> >::iterator itr=tabinn.begin(); itr!=tabinn.end(); ++itr)
	{
		Key<2> edge = *itr;

		//if ( find( tabdup.begin(), tabdup.end(), edge) == tabdup.end() )
		if ( ! binary_search( tabdup.begin(), tabdup.end(), edge) )
		{
			bInn = false;
			mtabTmpEdges.push_back( edge );
		}
	}


	if ( bBnd && bInn )
	{
		mControlSum.first = mtabSplitEdge[0]->SizeInNodes() + mtabSplitEdge[1]->SizeInNodes() + mtabSplitEdge[2]->SizeInNodes();
		mControlSum.second = mtabINodes.size();
		return true;
	}

	//cout << "mtabTmpEdges.size = " << mtabTmpEdges.size() << endl;

	//cout << "tabbnd.size = " << tabbnd.size() << endl;
	//cout << "taball.size = " << taball.size() << endl;
	//cout << "tabinn.size = " << tabinn.size() << endl;
	//cout << "tabdup.size = " << tabdup.size() << endl;

	//cout << endl;


	//ofstream of( "_dump.txt");

	//of << "tabbnd.size = " << tabbnd.size() << endl;
	//for ( MGSize i=0;i<tabbnd.size();++i)
	//	of << tabbnd[i].cFirst() << " " << tabbnd[i].cSecond() << endl;
	//of << endl;

	//of << "taball.size = " << taball.size() << endl;
	//for ( MGSize i=0;i<taball.size();++i)
	//	of << taball[i].cFirst() << " " << taball[i].cSecond() << endl;
	//of << endl;

	//of << "tabinn.size = " << tabinn.size() << endl;
	//for ( MGSize i=0;i<tabinn.size();++i)
	//	of << tabinn[i].cFirst() << " " << tabinn[i].cSecond() << endl;
	//of << endl;

	//of << "tabdup.size = " << tabdup.size() << endl;
	//for ( MGSize i=0;i<tabdup.size();++i)
	//	of << tabdup[i].cFirst() << " " << tabdup[i].cSecond() << endl;


	//THROW_INTERNAL( "STOP");

	return false;
}


void FaceSplit::FindRecPoints( EdgeSplit** pparEdge, Key<2>& subEdge, GVect& propvct, Kernel<DIM_3D>& kernel )
{
	ASSERT( mtabTmpEdges.size() > 0 );

	// create tab of all face node-ids
	vector<MGSize>	tabidn;

	for ( MGSize i=0; i<3; ++i)
		tabidn.push_back( mFace.cElem(i) );

	for ( MGSize i=0; i<3; ++i)
		for ( MGSize k=0; k<mtabSplitEdge[i]->mtabINodes.size(); ++k)
			tabidn.push_back( mtabSplitEdge[i]->mtabINodes[k] );

	for ( MGSize i=0; i<mtabINodes.size(); ++i)
		tabidn.push_back( mtabINodes[i] );

	sort( tabidn.begin(), tabidn.end() );



	// find adjacent cells
	vector<MGSize> tabcell;

	for ( MGSize i=0; i<mtabTmpEdges.size(); ++i)
	{
		Key<2> edge = mtabTmpEdges[i];
		vector<MGSize> tabball;

		kernel.FindEdgeShell( tabball, edge );

		tabcell.insert( tabcell.end(), tabball.begin(), tabball.end() );
	}

	sort( tabcell.begin(), tabcell.end() );


	//// dump
	//WriteGrdTEC<DIM_3D>	write( &kernel.rGrid() );
	//write.WriteCells( "_cells_.dat", tabcell);


	// find adjacent edges
	vector<MGSize>::iterator itrend = unique( tabcell.begin(), tabcell.end() );
	tabcell.erase( itrend, tabcell.end() );

	vector< Key<2> > tabedge;

	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		const GCell& cell = kernel.rGrid().cCell( tabcell[i] );

		for ( MGSize k=0; k<GCell::ESIZE; ++k )
		{
			Key<2> edge = cell.EdgeKey( k);
			edge.Sort();
			//if (	find( tabidn.begin(), tabidn.end(), edge.cFirst() ) == tabidn.end()
			//	&&	find( tabidn.begin(), tabidn.end(), edge.cSecond() ) == tabidn.end() )
			if (	(! binary_search( tabidn.begin(), tabidn.end(), edge.cFirst() ))
				&&	(! binary_search( tabidn.begin(), tabidn.end(), edge.cSecond() )) )
			{
				tabedge.push_back( edge);
			}
		}
	}

	vector< Key<2> >::iterator itredge = unique( tabedge.begin(), tabedge.end() );
	tabedge.erase( itredge, tabedge.end() );

	GVect vf1 = kernel.rGrid().cNode( mFace.cFirst() ).cPos();
	GVect vf2 = kernel.rGrid().cNode( mFace.cSecond() ).cPos();
	GVect vf3 = kernel.rGrid().cNode( mFace.cThird() ).cPos();

	//ofstream f( "_cross_edges.dat");


	// find all crossings
	GVect vcross;
	vector<GVect> tabvc;
	vector< Key<2> > tabcross;

	for ( MGSize i=0; i<tabedge.size(); ++i)
	{
		Key<2> edge = tabedge[i];

		GVect ve1 = kernel.rGrid().cNode( edge.cFirst() ).cPos();
		GVect ve2 = kernel.rGrid().cNode( edge.cSecond() ).cPos();

		if ( FindEdgeFaceCross( vcross, ve1, ve2, vf1, vf2, vf3 ) )
		{
			tabvc.push_back( vcross);
			tabcross.push_back( edge);

			//GVect	v1 = kernel.rGrid().cNode( edge.cFirst() ).cPos();
			//GVect	v2 = kernel.rGrid().cNode( edge.cSecond() ).cPos();

			//f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
			//f << "ZONE T=\"edge\"\n";
			//f << v1.cX() << " " << v1.cY() << " " << v1.cZ() << " " << edge.cFirst() << endl;
			//f << v2.cX() << " " << v2.cY() << " " << v2.cZ() << " " << edge.cSecond() << endl;

			//f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
			//f << "ZONE T=\"cross\"\n";
			//f << vcross.cX() << " " << vcross.cY() << " " << vcross.cZ() << " " << 0 << endl;
		}
	}

	//// dump
	//ofstream ff( "_cross_edges.dat");
	//
	//for ( MGSize i=0; i<tabedge.size(); ++i)
	//{
	//	Key<2> edge = tabedge[i];
	//
	//	GVect	v1 = kernel.rGrid().cNode( edge.cFirst() ).cPos();
	//	GVect	v2 = kernel.rGrid().cNode( edge.cSecond() ).cPos();
	//
	//	ff << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	//	ff << "ZONE T=\"edge\"\n";
	//	ff << v1.cX() << " " << v1.cY() << " " << v1.cZ() << " " << edge.cFirst() << endl;
	//	ff << v2.cX() << " " << v2.cY() << " " << v2.cZ() << " " << edge.cSecond() << endl;
	//}

	if ( tabvc.size() == 0 )
	{
		cout << "mtabTmpEdges.size() = " << mtabTmpEdges.size() << endl;
		cout << "tabedge.size() = " << tabedge.size() << endl;
		cout << "mtabSubFaces.size() = " << mtabSubFaces.size() << endl;

		ofstream ff( "_facesplit_subfaces.dat");

		for ( MGSize i=0; i<mtabSubFaces.size(); ++i)
		{
			Key<3> face = mtabSubFaces[i];
	
			GVect	v1 = kernel.rGrid().cNode( face.cFirst() ).cPos();
			GVect	v2 = kernel.rGrid().cNode( face.cSecond() ).cPos();
			GVect	v3 = kernel.rGrid().cNode( face.cThird() ).cPos();
	
			ff << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
			ff << "ZONE T=\"subface\"" << ", N=" << 3 << ", E=" << 1 << ", F=FEPOINT, ET=TRIANGLE\n";
			ff << v1.cX() << " " << v1.cY() << " " << v1.cZ() << " " << face.cFirst() << endl;
			ff << v2.cX() << " " << v2.cY() << " " << v2.cZ() << " " << face.cSecond() << endl;
			ff << v3.cX() << " " << v3.cY() << " " << v3.cZ() << " " << face.cThird() << endl;
			ff << "1 2 3" << endl;
		}

		THROW_INTERNAL( "tabvc.size() == 0" );
	}


	// check edges
	EdgeSplit*	pedge = NULL;
	Key<2> sedge;
	MGFloat distedge = -1;;

	GVect vctface, vctfacetmp, vctedge;
	MGFloat distface = -1;
	MGFloat distmin = -1;

	for ( MGSize j=0; j<tabvc.size(); ++j)
	{
		GVect vct = tabvc[j];

		for ( MGSize i=0; i<3; ++i)
			for ( MGSize k=0; k<mtabSplitEdge[i]->Size(); ++k)
			{
				Key<2> edge = mtabSplitEdge[i]->cSubEdge( k);

				GVect	v1 = kernel.rGrid().cNode( edge.cFirst() ).cPos();
				GVect	v2 = kernel.rGrid().cNode( edge.cSecond() ).cPos();

				MGFloat dist = EdgeDistance( v1, v2, vct );
				//MGFloat radius = 0.5 * (v2 - v1).module();

				MGFloat distrad = (vct - 0.5*(v2+v1) ).module();
				MGFloat radius = 0.5 * (v2 - v1).module();

				//cout << dist << " " << radius << endl;

				//if ( dist < radius)
				if ( distrad < radius)
				{
					if ( dist < distedge || distedge < 0 )
					{
						distedge = dist;

						pedge = mtabSplitEdge[i];
						sedge = edge;

						//GVect w = (v2 - v1).versor();
						//GVect va = vct - v1;
						//vctedge = (va * w) * w + v1;
						vctedge = 0.5 * (v1+v2);

						//vctedge = vct;
						//cout << vctedge.cX() << " " << vctedge.cY() << " " << vctedge.cZ() << " " << endl;
					}
				}
				
				if ( dist < distmin || distmin < 0 )
				{
					distmin = dist;
					vctfacetmp = vct;
				}

			}


		if ( distmin > distface || distface < 0 )
		{
			distface = distmin;
			vctface = vctfacetmp;
		}
	}

	// setting up return values
	if ( pedge != NULL )
	{
		*pparEdge = pedge;
		subEdge = sedge;
		propvct = vctedge;
	}
	else
	{
		*pparEdge = NULL;
		subEdge = Key<2>(0,0);
		propvct = vctface;
	}

	//f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	//f << "ZONE T=\"edge cross\"\n";
	//f << vctedge.cX() << " " << vctedge.cY() << " " << vctedge.cZ() << " " << 0 << endl;
	//
	//f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	//f << "ZONE T=\"face cross\"\n";
	//f << vctface.cX() << " " << vctface.cY() << " " << vctface.cZ() << " " << 0 << endl;
}





void FaceSplit::FindRecPoints( GVect& propvct, Kernel<DIM_3D>& kernel )
{
	ASSERT( mtabTmpEdges.size() > 0 );

	// create tab of all face node-ids
	vector<MGSize>	tabidn;

	for ( MGSize i=0; i<3; ++i)
		tabidn.push_back( mFace.cElem(i) );

	for ( MGSize i=0; i<3; ++i)
		for ( MGSize k=0; k<mtabSplitEdge[i]->mtabINodes.size(); ++k)
			tabidn.push_back( mtabSplitEdge[i]->mtabINodes[k] );

	for ( MGSize i=0; i<mtabINodes.size(); ++i)
		tabidn.push_back( mtabINodes[i] );

	sort( tabidn.begin(), tabidn.end() );



	// find adjacent cells
	vector<MGSize> tabcell;

	for ( MGSize i=0; i<mtabTmpEdges.size(); ++i)
	{
		Key<2> edge = mtabTmpEdges[i];
		vector<MGSize> tabball;

		kernel.FindEdgeShell( tabball, edge );

		tabcell.insert( tabcell.end(), tabball.begin(), tabball.end() );
	}

	sort( tabcell.begin(), tabcell.end() );


	//// dump
	//WriteGrdTEC<DIM_3D>	write( &kernel.rGrid() );
	//write.WriteCells( "_cells_.dat", tabcell);


	// find adjacent edges
	tabcell.erase( unique( tabcell.begin(), tabcell.end() ), tabcell.end() );
	//vector<MGSize>::iterator itrend = unique( tabcell.begin(), tabcell.end() );
	//tabcell.erase( itrend, tabcell.end() );


	vector< Key<2> > tabedge;
	vector<MGSize> tabsliv;

	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		const GCell& cell = kernel.rGrid().cCell( tabcell[i] );

		if (	( binary_search( tabidn.begin(), tabidn.end(), cell.cNodeId(0) ))
			&&	( binary_search( tabidn.begin(), tabidn.end(), cell.cNodeId(1) )) 
			&&	( binary_search( tabidn.begin(), tabidn.end(), cell.cNodeId(2) )) 
			&&	( binary_search( tabidn.begin(), tabidn.end(), cell.cNodeId(3) )) )
		{
			// sliver founed !!!
			tabsliv.push_back( tabcell[i] );
		}

		for ( MGSize k=0; k<GCell::ESIZE; ++k )
		{
			Key<2> edge = cell.EdgeKey( k);
			edge.Sort();
			//if (	find( tabidn.begin(), tabidn.end(), edge.cFirst() ) == tabidn.end()
			//	&&	find( tabidn.begin(), tabidn.end(), edge.cSecond() ) == tabidn.end() )
			if (	(! binary_search( tabidn.begin(), tabidn.end(), edge.cFirst() ))
				&&	(! binary_search( tabidn.begin(), tabidn.end(), edge.cSecond() )) )
			{
				tabedge.push_back( edge);
			}
		}
	}

	if ( tabsliv.size() > 0 )
	{
		cout << endl << "SLIVER !!! " << tabsliv.size() << endl;

		ofstream of( "_slv_facesplit_.dat");
		ExportTEC( of, kernel.rGrid() );

		WriteGrdTEC<DIM_3D>	write( &kernel.rGrid() );
		write.WriteCells( "_slv_slivers_.dat", tabsliv);
		write.WriteCells( "_slv_cells_.dat", tabcell);

		

		//const GCell& cell = kernel.rGrid().cCell( tabsliv[0] );

		//Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, kernel.rGrid() );

		//propvct = tet.Center();
		//return;
	}

	vector< Key<2> >::iterator itredge = unique( tabedge.begin(), tabedge.end() );
	tabedge.erase( itredge, tabedge.end() );

	GVect vf1 = kernel.rGrid().cNode( mFace.cFirst() ).cPos();
	GVect vf2 = kernel.rGrid().cNode( mFace.cSecond() ).cPos();
	GVect vf3 = kernel.rGrid().cNode( mFace.cThird() ).cPos();

	//ofstream f( "_cross_edges.dat");

	GVect vcross;
	vector<GVect> tabvc;
	vector< Key<2> > tabcross;

	for ( MGSize i=0; i<tabedge.size(); ++i)
	{
		Key<2> edge = tabedge[i];

		GVect ve1 = kernel.rGrid().cNode( edge.cFirst() ).cPos();
		GVect ve2 = kernel.rGrid().cNode( edge.cSecond() ).cPos();

		if ( FindEdgeFaceCross( vcross, ve1, ve2, vf1, vf2, vf3 ) )
		{
			tabvc.push_back( vcross);
			tabcross.push_back( edge);

			//GVect	v1 = kernel.rGrid().cNode( edge.cFirst() ).cPos();
			//GVect	v2 = kernel.rGrid().cNode( edge.cSecond() ).cPos();

			//f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
			//f << "ZONE T=\"edge\"\n";
			//f << v1.cX() << " " << v1.cY() << " " << v1.cZ() << " " << edge.cFirst() << endl;
			//f << v2.cX() << " " << v2.cY() << " " << v2.cZ() << " " << edge.cSecond() << endl;

			//f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
			//f << "ZONE T=\"cross\"\n";
			//f << vcross.cX() << " " << vcross.cY() << " " << vcross.cZ() << " " << 0 << endl;
		}
	}

	//// dump
	//ofstream ff( "_cross_edges.dat");
	//
	//for ( MGSize i=0; i<tabedge.size(); ++i)
	//{
	//	Key<2> edge = tabedge[i];
	//
	//	GVect	v1 = kernel.rGrid().cNode( edge.cFirst() ).cPos();
	//	GVect	v2 = kernel.rGrid().cNode( edge.cSecond() ).cPos();
	//
	//	ff << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	//	ff << "ZONE T=\"edge\"\n";
	//	ff << v1.cX() << " " << v1.cY() << " " << v1.cZ() << " " << edge.cFirst() << endl;
	//	ff << v2.cX() << " " << v2.cY() << " " << v2.cZ() << " " << edge.cSecond() << endl;
	//}

	if ( tabvc.size() == 0 )
	{
		cout << "mtabTmpEdges.size() = " << mtabTmpEdges.size() << endl;
		cout << "tabedge.size() = " << tabedge.size() << endl;
		cout << "mtabSubFaces.size() = " << mtabSubFaces.size() << endl;

		ofstream ff( "_facesplit_subfaces.dat");

		for ( MGSize i=0; i<mtabSubFaces.size(); ++i)
		{
			Key<3> face = mtabSubFaces[i];
	
			GVect	v1 = kernel.rGrid().cNode( face.cFirst() ).cPos();
			GVect	v2 = kernel.rGrid().cNode( face.cSecond() ).cPos();
			GVect	v3 = kernel.rGrid().cNode( face.cThird() ).cPos();
	
			ff << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
			ff << "ZONE T=\"subface\"" << ", N=" << 3 << ", E=" << 1 << ", F=FEPOINT, ET=TRIANGLE\n";
			ff << v1.cX() << " " << v1.cY() << " " << v1.cZ() << " " << face.cFirst() << endl;
			ff << v2.cX() << " " << v2.cY() << " " << v2.cZ() << " " << face.cSecond() << endl;
			ff << v3.cX() << " " << v3.cY() << " " << v3.cZ() << " " << face.cThird() << endl;
			ff << "1 2 3" << endl;
		}

		THROW_INTERNAL( "tabvc.size() == 0" );
	}


	// check edges
	MGFloat distedge = -1;;

	GVect vctface, vctfacetmp, vctedge;
	MGFloat distface = -1;
	MGFloat distmin = -1;

	for ( MGSize j=0; j<tabvc.size(); ++j)
	{
		GVect vct = tabvc[j];

		for ( MGSize i=0; i<3; ++i)
			for ( MGSize k=0; k<mtabSplitEdge[i]->Size(); ++k)
			{
				Key<2> edge = mtabSplitEdge[i]->cSubEdge( k);

				GVect	v1 = kernel.rGrid().cNode( edge.cFirst() ).cPos();
				GVect	v2 = kernel.rGrid().cNode( edge.cSecond() ).cPos();

				MGFloat dist = EdgeDistance( v1, v2, vct );

				if ( dist < distmin || distmin < 0 )
				{
					distmin = dist;
					vctfacetmp = vct;
				}

			}


		if ( distmin > distface || distface < 0 )
		{
			distface = distmin;
			vctface = vctfacetmp;
		}
	}

	// setting up return values
	propvct = vctface;

	//f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	//f << "ZONE T=\"edge cross\"\n";
	//f << vctedge.cX() << " " << vctedge.cY() << " " << vctedge.cZ() << " " << 0 << endl;
	//
	//f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	//f << "ZONE T=\"face cross\"\n";
	//f << vctface.cX() << " " << vctface.cY() << " " << vctface.cZ() << " " << 0 << endl;
}


void FaceSplit::ExportTEC( ostream& file, const Grid<DIM_3D>& grid)
{
	GVect	v1 = grid.cNode( mFace.cFirst() ).cPos();
	GVect	v2 = grid.cNode( mFace.cSecond() ).cPos();
	GVect	v3 = grid.cNode( mFace.cThird() ).cPos();

	file << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	file << "ZONE T=\"face\", N=3, E=1, F=FEPOINT, ET=TRIANGLE\n";
	file << v1.cX() << " " << v1.cY() << " " << v1.cZ() << " " << mFace.cFirst() << endl;
	file << v2.cX() << " " << v2.cY() << " " << v2.cZ() << " " << mFace.cSecond() << endl;
	file << v3.cX() << " " << v3.cY() << " " << v3.cZ() << " " << mFace.cThird() << endl;
	file << "1 2 3" << endl;

	if ( mtabSubFaces.size() > 0 )
	{
		map<MGSize,MGSize> mapid;

		for ( MGSize i=0; i<mtabSubFaces.size(); ++i)
		{
			Key<3> face = mtabSubFaces[i];
			for ( MGSize k=0; k<3; ++k)
				mapid[ face.cElem( k) ] = 0;
		}


		file << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
		file << "ZONE T=\"subfaces\", N=" << mapid.size() << ", E=" << mtabSubFaces.size() << ", F=FEPOINT, ET=TRIANGLE\n";

		MGSize id = 1;
		for ( map<MGSize,MGSize>::iterator itr = mapid.begin(); itr != mapid.end(); ++itr, ++id)
		{
			itr->second = id;
			GVect	vct = grid.cNode( itr->first ).cPos();
			file << vct.cX() << " " << vct.cY() << " " << vct.cZ() << " " << itr->first << endl;
		}

		for ( MGSize i=0; i<mtabSubFaces.size(); ++i)
		{
			Key<3> face = mtabSubFaces[i];
			file << mapid[face.cFirst()] << " " << mapid[face.cSecond()] << " " << mapid[face.cThird()] << endl;
		}
	}
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

