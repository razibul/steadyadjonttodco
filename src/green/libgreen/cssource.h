#ifndef __CSSOURCE_H__
#define __CSSOURCE_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libcorecommon/entity.h"
#include "libgreen/gridcontext.h"
#include "libgreen/csdistance.h"
#include "libgreen/cspaceconst.h"
#include "libcorecommon/triple.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <Dimension DIM>
class Grid;



//////////////////////////////////////////////////////////////////////
//	class Source
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, Dimension UNIDIM>
class Source : public Entity
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<UNIDIM>::GVect	UVect;
	typedef typename TDefs<UNIDIM>::GMetric	UMetric;

	typedef triple<Dimension,MGSize,bool>	DimBoolId;

public:
	Source() : mpGrid(NULL), mpMPosCx(NULL), mpMetricCx(NULL)	{}

	static const char*		ClassName();

	void	Init( const MGString& sname, MGString& sparams);
	void	WriteDef( ostream& of) const;

	const MGSize&	cTopoId() const			{ return mTopoId;}

	void	InitPointers( Grid<DIM> *pgrd, GridContext<UVect> *pposcx, GridContext<UMetric> *pmetcx)
	{
		mpGrid = pgrd;
		mpMPosCx = pposcx;
		mpMetricCx = pmetcx;
	}


	const UMetric&	cMetric() const		{ return mMetric;}


private:
	MGSize	mTopoId;
	UMetric	mMetric;

	Grid<DIM>				*mpGrid;
	GridContext<UVect>		*mpMPosCx;
	GridContext<UMetric>	*mpMetricCx;

	vector< DimBoolId >	mtabSrcId;


	// only for 0D ???
	CSDistance	mDistFunc;		// distance function used for the definition of size growth

};



template <>
inline const char* Source<DIM_0D,DIM_2D>::ClassName()
{ 
	return CSP_VERTEXSRC;
}

template <>
inline const char* Source<DIM_1D,DIM_2D>::ClassName()
{ 
	return CSP_EDGESRC;
}

template <>
inline const char* Source<DIM_2D,DIM_2D>::ClassName()
{ 
	return CSP_FACESRC;
}

template <>
inline const char* Source<DIM_0D,DIM_3D>::ClassName()
{ 
	return CSP_VERTEXSRC;
}

template <>
inline const char* Source<DIM_1D,DIM_3D>::ClassName()
{ 
	return CSP_EDGESRC;
}

template <>
inline const char* Source<DIM_2D,DIM_3D>::ClassName()
{ 
	return CSP_FACESRC;
}

template <>
inline const char* Source<DIM_3D,DIM_3D>::ClassName()
{ 
	return CSP_BREPSRC;
}

////////////////////////////////////////////////////////////////////////
////	class Source
////////////////////////////////////////////////////////////////////////
//template <Dimension UNIDIM>
//class Source<DIM_0D,UNIDIM>
//{
//public:
//private:
//};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CSSOURCE_H__
