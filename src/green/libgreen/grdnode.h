#ifndef __GRDNODE_H__
#define __GRDNODE_H__

#include "libgreen/tdefs.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Node
//////////////////////////////////////////////////////////////////////
template <Dimension DIM> 
class Node : public Base
{
	typedef typename TDefs<DIM>::GVect		GVect;

public:

	Node() : mMasterId(0)	{}
	Node( const GVect& vct) : mMasterId(0), mPos(vct)	{}

	MGSize&			rMasterId()			{ return mMasterId;}
	const MGSize&	cMasterId() const	{ return mMasterId;}

	MGSize&			rCellId()			{ return mCellId;}
	const MGSize&	cCellId() const		{ return mCellId;}

	const GVect&	cPos() const		{ return mPos;}
	GVect&			rPos()				{ return mPos;}

	operator	GVect()					{ return mPos;}

private:
	MGSize	mMasterId;
	MGSize	mCellId;
	
	GVect	mPos;
};
//////////////////////////////////////////////////////////////////////



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GRDNODE_H__
