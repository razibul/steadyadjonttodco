#include "operator3d_flip23.h"

#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/gridgeom.h"

#include "libcorecommon/progressbar.h"
#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




void Operator3D_Flip23::Init( const vector<MGSize>& tabcell)
{
	if ( tabcell.size() != 2)
	{
		mErrorCode = 1;
		return;
	}

	mtabOCell = tabcell;

	Grid<DIM_3D>&	grid = mKernel.rGrid();


	const MGSize ic1 = tabcell.front();
	const MGSize ic2 = tabcell.back();
	
	const GCell& cell1 = grid.cCell(ic1);
	const GCell& cell2 = grid.cCell(ic2);

	MGSize	if1, if2;
	bool	bNghb = false;

	// check if they are neighbours
	for ( MGSize i=0; i<GCell::SIZE; ++i)
	{
		if ( cell1.cCellId(i).first == ic2)
		{
			if1 = i;
			if2 = cell1.cCellId(i).second;

			if ( cell2.cCellId( if2 ).first == ic1 && cell2.cCellId( if2 ).second == if1 )
			{
				bNghb = true;
				break;
			}
		}
	}

	if ( ! bNghb)
	{
		TRACE( "Operator3D_Flip23 - cells are not neighbours");
		mErrorCode = 2;
		return;
	}


	mEdge.rFirst() = cell1.cNodeId( if1);	// ina
	mEdge.rSecond() = cell2.cNodeId( if2);	// inb
	mtabENode.clear();
	mtabENode.push_back( cell1.cNodeId( Simplex<DIM_3D>::cFaceConn(if1,0) ) );	// in0
	mtabENode.push_back( cell1.cNodeId( Simplex<DIM_3D>::cFaceConn(if1,2) ) );	// in1
	mtabENode.push_back( cell1.cNodeId( Simplex<DIM_3D>::cFaceConn(if1,1) ) );	// in2

	mif1 = if1;
	mif2 = if2;
}


void Operator3D_Flip23::BuildCells( vector<GCell>& tabcell)
{
	GCell cell;

	tabcell.clear();

	cell.rNodeId(0) = mEdge.cFirst();
	cell.rNodeId(1) = mEdge.cSecond();
	cell.rNodeId(2) = mtabENode[1];
	cell.rNodeId(3) = mtabENode[0];
	tabcell.push_back( cell);

	cell.rNodeId(0) = mEdge.cFirst();
	cell.rNodeId(1) = mEdge.cSecond();
	cell.rNodeId(2) = mtabENode[2];
	cell.rNodeId(3) = mtabENode[1];
	tabcell.push_back( cell);

	cell.rNodeId(0) = mEdge.cFirst();
	cell.rNodeId(1) = mEdge.cSecond();
	cell.rNodeId(2) = mtabENode[0];
	cell.rNodeId(3) = mtabENode[2];
	tabcell.push_back( cell);

}

void Operator3D_Flip23::Select()
{
	if ( mErrorCode != 0)
		return;

	BuildCells( mtabNCell);

	if ( ! mKernel.CheckCellVolume( mtabNCell) )
		mErrorCode = 2000;
}

void Operator3D_Flip23::SelectQBased()
{
	if ( mErrorCode != 0)
		return;

	vector<GCell> tabco;
	for ( MGSize i=0; i<mtabOCell.size(); ++i)
		tabco.push_back( mKernel.rGrid().cCell( mtabOCell[i]) );

	BuildCells( mtabNCell);

	if ( ! mKernel.CheckCellVolume( mtabNCell) )
	{
		mErrorCode = 2000;
		return;
	}

	MGFloat qbo = mKernel.CheckCellQuality( tabco);
	MGFloat qb1 = mKernel.CheckCellQuality( mtabNCell);

	if ( qbo < qb1)
	{
		mErrorCode = 1000;
		return;
	}
}

void Operator3D_Flip23::SelectVolBased()
{
	if ( mErrorCode != 0)
		return;

	vector<GCell> tabco;
	for ( MGSize i=0; i<mtabOCell.size(); ++i)
		tabco.push_back( mKernel.rGrid().cCell( mtabOCell[i]) );

	BuildCells( mtabNCell);

	if ( ! mKernel.CheckCellVolume( mtabNCell) )
	{
		mErrorCode = 2000;
		return;
	}

	MGFloat vo = mKernel.MinCellVolume( tabco);
	MGFloat v1 = mKernel.MinCellVolume( mtabNCell);

	if ( vo > v1)
	{
		mErrorCode = 1000;
		return;
	}
}



void Operator3D_Flip23::DoFlip( vector<MGSize>& tabcell)
{
	if ( mErrorCode != 0)
		return;


	ASSERT( mtabNCell.size() == 3);

	tabcell.clear();

	/////////////////////////////////////////////////////////////
	// insert new cells 
	for ( MGSize i=0; i<mtabNCell.size(); ++i)
	{
		//MGSize id =  mKernel.rGrid().InsertCell( mtabNCell[i]);
		MGSize id = mKernel.InsertCell( mtabNCell[i] );
		tabcell.push_back( id);

		mKernel.rGrid().rCell( id).SetModified( true);
	}

	/////////////////////////////////////////////////////////////
	// update internal connectivity

	// cell 0
	mKernel.rGrid().rCell( tabcell[0] ).rCellId(3).first = tabcell[1];
	mKernel.rGrid().rCell( tabcell[0] ).rCellId(3).second = 2;
	mKernel.rGrid().rCell( tabcell[0] ).rCellId(2).first = tabcell[2];
	mKernel.rGrid().rCell( tabcell[0] ).rCellId(2).second = 3;

	// cell 1
	mKernel.rGrid().rCell( tabcell[1] ).rCellId(3).first = tabcell[2];
	mKernel.rGrid().rCell( tabcell[1] ).rCellId(3).second = 2;
	mKernel.rGrid().rCell( tabcell[1] ).rCellId(2).first = tabcell[0];
	mKernel.rGrid().rCell( tabcell[1] ).rCellId(2).second = 3;

	// cell 2
	mKernel.rGrid().rCell( tabcell[2] ).rCellId(3).first = tabcell[0];
	mKernel.rGrid().rCell( tabcell[2] ).rCellId(3).second = 2;
	mKernel.rGrid().rCell( tabcell[2] ).rCellId(2).first = tabcell[1];
	mKernel.rGrid().rCell( tabcell[2] ).rCellId(2).second = 3;


	/////////////////////////////////////////////////////////////
	// update external connectivity

	const GCell& cell1 = mKernel.rGrid().cCell( mtabOCell[0]);
	const GCell& cell2 = mKernel.rGrid().cCell( mtabOCell[1]);

	GCell& gcell1 = mKernel.rGrid().rCell( tabcell[0] );
	GCell& gcell2 = mKernel.rGrid().rCell( tabcell[1] );
	GCell& gcell3 = mKernel.rGrid().rCell( tabcell[2] );

	const MGSize ie1_0 = Simplex<DIM_3D>::cFaceConn(mif1,0);
	const MGSize ie1_1 = Simplex<DIM_3D>::cFaceConn(mif1,2);
	const MGSize ie1_2 = Simplex<DIM_3D>::cFaceConn(mif1,1);

	MGSize ie2_0, ie2_1, ie2_2;

	for ( MGSize i=0; i<GCell::SIZE; ++i)
	{
		if ( cell2.cNodeId( i) == mtabENode[0] )
			ie2_0 = i;

		if ( cell2.cNodeId( i) == mtabENode[1] )
			ie2_1 = i;

		if ( cell2.cNodeId( i) == mtabENode[2] )
			ie2_2 = i;
	}
	
	ASSERT( cell1.cNodeId( ie1_0) == cell2.cNodeId( ie2_0) );
	ASSERT( cell1.cNodeId( ie1_1) == cell2.cNodeId( ie2_1) );
	ASSERT( cell1.cNodeId( ie1_2) == cell2.cNodeId( ie2_2) );

	gcell1.rCellId(0) = cell2.cCellId( ie2_2);
	gcell1.rCellId(1) = cell1.cCellId( ie1_2);

	gcell2.rCellId(0) = cell2.cCellId( ie2_0);
	gcell2.rCellId(1) = cell1.cCellId( ie1_0);

	gcell3.rCellId(0) = cell2.cCellId( ie2_1);
	gcell3.rCellId(1) = cell1.cCellId( ie1_1);
	
	
	mKernel.rGrid().rCell( gcell1.cCellId(0).first ).rCellId( gcell1.cCellId(0).second ).first = gcell1.cId();
	mKernel.rGrid().rCell( gcell1.cCellId(0).first ).rCellId( gcell1.cCellId(0).second ).second = 0;
	mKernel.rGrid().rCell( gcell1.cCellId(1).first ).rCellId( gcell1.cCellId(1).second ).first = gcell1.cId();
	mKernel.rGrid().rCell( gcell1.cCellId(1).first ).rCellId( gcell1.cCellId(1).second ).second = 1;

	mKernel.rGrid().rCell( gcell2.cCellId(0).first ).rCellId( gcell2.cCellId(0).second ).first = gcell2.cId();
	mKernel.rGrid().rCell( gcell2.cCellId(0).first ).rCellId( gcell2.cCellId(0).second ).second = 0;
	mKernel.rGrid().rCell( gcell2.cCellId(1).first ).rCellId( gcell2.cCellId(1).second ).first = gcell2.cId();
	mKernel.rGrid().rCell( gcell2.cCellId(1).first ).rCellId( gcell2.cCellId(1).second ).second = 1;

	mKernel.rGrid().rCell( gcell3.cCellId(0).first ).rCellId( gcell3.cCellId(0).second ).first = gcell3.cId();
	mKernel.rGrid().rCell( gcell3.cCellId(0).first ).rCellId( gcell3.cCellId(0).second ).second = 0;
	mKernel.rGrid().rCell( gcell3.cCellId(1).first ).rCellId( gcell3.cCellId(1).second ).first = gcell3.cId();
	mKernel.rGrid().rCell( gcell3.cCellId(1).first ).rCellId( gcell3.cCellId(1).second ).second = 1;


	/////////////////////////////////////////////////////////////
	// remove old cells
	// remove old cell from the tree

	for ( MGSize ic=0; ic<mtabOCell.size(); ++ic)
		mKernel.EraseCell( mtabOCell[ic] );

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

