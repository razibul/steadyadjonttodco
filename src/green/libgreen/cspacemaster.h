#ifndef __CSPACEMASTER_H__
#define __CSPACEMASTER_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/cssource.h"




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <Geom::Dimension DIM>
class Master;

template <Dimension DIM>
class CSpaceIO;


//////////////////////////////////////////////////////////////////////
//	class SourceOwner
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, Dimension UNIDIM>
class SourceOwner : public SourceOwner< static_cast<Dimension>( DIM-1 ), UNIDIM >
{
public:

	//template <Dimension D>
	//MGSize	CreateSource()
	//{
	//	SourceOwner<D,UNIDIM>::mtabSource.push_back( Source<D,UNIDIM>() );
	//	MGSize id = SourceOwner<D,UNIDIM>::mtabSource.size();
	//	SourceOwner<D,UNIDIM>::mtabSource[id-1].rId() = id;
	//	return id;
	//}

	template <Dimension D>
	MGSize PushBack( const Source<D,UNIDIM>& ent)		
	{ 
		SourceOwner<D,UNIDIM>::mtabSource.push_back( ent);
		MGSize ind = SourceOwner<D,UNIDIM>::mtabSource.size() - 1;
		SourceOwner<D,UNIDIM>::mtabSource[ind].rId() = SourceOwner<D,UNIDIM>::mtabSource.size();
		return SourceOwner<D,UNIDIM>::mtabSource[ind].cId();
	}


	template <Dimension D>
	const Source<D,UNIDIM>&	cSource( const MGSize& id) const	{ return SourceOwner<D,UNIDIM>::mtabSource[id-1]; }

	template <Dimension D>
	Source<D,UNIDIM>&			rSource( const MGSize& id)			
	{ 
		//cout << "D = " << D << "  " << SourceOwner<D,UNIDIM>::mtabSource.size() << endl;
		return SourceOwner<D,UNIDIM>::mtabSource[id-1]; 
	}

	template <Dimension D>
	MGSize	Size() const { return SourceOwner<D,UNIDIM>::mtabSource.size(); }

protected:
	vector< Source<DIM,UNIDIM> >	mtabSource;
};


//////////////////////////////////////////////////////////////////////
//	class SourceOwner + specialization for 0D
//////////////////////////////////////////////////////////////////////
template <Dimension UNIDIM>
class SourceOwner<DIM_0D, UNIDIM>
{
public:

protected:
	vector< Source<DIM_0D,UNIDIM> >	mtabSource;
};




template <Dimension DIM>
class ControlSpace;


//////////////////////////////////////////////////////////////////////
//	class CSpaceMaster
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class CSpaceMaster
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

	friend class CSpaceIO<DIM>;
	friend class Master<DIM>;		// ????

public:
	CSpaceMaster() : mpGlobCSpace(NULL)		{}
	~CSpaceMaster();

	void	InitGlobalCSpFileTEC( const MGString& fname, const MGFloat& scale=1.0);
	void	InitGlobalCSpFileMEANDROS( const MGString& fname, const MGFloat& scale=1.0);
	void	InitGlobalCSpUniform(  const MGFloat& hsize);

	const ControlSpace<DIM>*	GetGlobalCSpace() const		{ return mpGlobCSpace;}


protected:

	template <Dimension D>
	Source<D,DIM>&			rSource( const MGSize& id)			{ return mSourceOwner.rSource<D>( id); }

	template <Dimension D>
	const Source<D,DIM>&	cSource( const MGSize& id) const	{ return mSourceOwner.rSource<D>( id); }

private:
	MGString	mName;
	MGString	mDescription;

	ControlSpace<DIM>	*mpGlobCSpace;

	SourceOwner<DIM,DIM>	mSourceOwner;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CSPACEMASTER_H__
