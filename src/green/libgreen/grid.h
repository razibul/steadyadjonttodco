#ifndef __GRID_H__
#define __GRID_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libcorecommon/entity.h"

#include "libgreen/grdnode.h"
#include "libgreen/grdcell.h"
#include "libgreen/grdface.h"

#include "libcorecommon/fgarrvector.h"
#include "libcorecommon/fgvector.h"
#include "libcorecommon/fvector.h"

#include "libgreen/bndgrid.h"


template <Dimension DIM, class TYPE>
class BackGrid;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <Dimension DIM>
class Master;


template <Dimension DIM, Dimension UNIDIM>
class GridControlSpace;

template <Dimension DIM>
class GridGlobControlSpace;


template <Dimension DIM>
class Kernel;

template <Dimension DIM>
class Generator;

template <Dimension DIM>
class Localizator;

template <Dimension DIM>
class GridOperators;

template <Dimension DIM>
class ReconstructorCDT;

template <Dimension DIM>
class Destroyer;

template <Dimension DIM>
class GridSmoother;



template <Dimension DIM>
class ReadTEC;

template <Dimension DIM>
class ReadMEANDR;

template <Dimension DIM>
class IOReadGreenProxy;


//////////////////////////////////////////////////////////////////////
//	class Grid
//////////////////////////////////////////////////////////////////////
// for both nodes and cells fortran like indexing is used;
// index 0 is invalid
//////////////////////////////////////////////////////////////////////
template <Dimension DIM> 
class Grid : public Entity
{
	friend class Master<DIM_2D>;
	friend class Master<DIM_3D>;

	friend class GridControlSpace<DIM,DIM_2D>;
	friend class GridControlSpace<DIM,DIM_3D>;

	friend class GridGlobControlSpace<DIM>;
	friend class BackGrid<DIM,MGFloat>;

	friend class Kernel<DIM>;
	friend class Generator<DIM>;
	friend class Localizator<DIM>;

	friend class FaceCDT;
	friend class ReconstructorCDT<DIM>;
	friend class Destroyer<DIM>;
	friend class EdgeDestr;

	friend class OptimiserSurf;

	friend class Operator2D_Flip22;

	friend class Operator3D_Flip44;
	friend class Operator3D_Flip32;
	friend class Operator3D_Flip23;
	friend class Operator3D_PointFace;
	friend class Operator3D_PointEdge;
	friend class Operator3D_CollapseEdge;
	friend class Operator3D_RestoreEdge;
	friend class Operator3D_RelocateNode;
	
	friend class SPointPushDestructor;

	friend class GridSmoother<DIM>;

	friend class ReadTEC<DIM>;
	friend class ReadMEANDR<DIM>;
	friend class IOReadGreenProxy<DIM>;

public:
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

	typedef typename TDefs<DIM>::GNode		GNode;
	typedef typename TDefs<DIM>::GCell		GCell;
	typedef typename TDefs<DIM>::GFace		GFace;

	//typedef fgvector<GNode>	ColNode;
	//typedef fgvector<GCell>	ColCell;
	typedef fgarrvector<GNode,12>	ColNode;
	typedef fgarrvector<GCell,12>	ColCell;

	Grid()		{}
	Grid( const Grid& grd) : mtabNode(grd.mtabNode), mtabCell(grd.mtabCell)		{}

	const GCell&	cCell( const MGSize& i) const	{ return mtabCell[i];}
	const GNode&	cNode( const MGSize& i) const	{ return mtabNode[i];}

	const ColCell&	cColCell() const				{ return mtabCell;}
	const ColNode&	cColNode() const				{ return mtabNode;}

	MGSize			SizeNodeTab() const				{ return mtabNode.size_valid();}
	MGSize			SizeCellTab() const				{ return mtabCell.size_valid();}

	Box<DIM>	FindBoundingBox() const;


	void	FindEdges( vector< Key<DIM-1> >& tabedg) const;
	void	FindFaces( vector< Key<DIM> >& tabfac) const;
	void	FindNullFaces( vector< Key<DIM> >& tabfac) const;
	
	void	FindInternalEdges( vector< Key<2> >& tabfac) const;

	void	UpdateNodeCellIds();
	void	UpdateNodeCellIds( const vector<MGSize>& tabcell);

	void	CheckConnectivity() const;
	void	CheckQuality() const;
	void	Dump( const MGString& fname) const;

	void	RebuildConnectivity( vector< Key<DIM> >* ptabbndkey=NULL);
 
protected:
	GCell&			rCell( const MGSize& i)			{ return mtabCell[i];}
	GNode&			rNode( const MGSize& i)			{ return mtabNode[i];}

	ColCell&		rColCell()						{ return mtabCell;}
	ColNode&		rColNode()						{ return mtabNode;}

	MGSize	InsertCell( const Cell<DIM>& cell);
	MGSize	InsertNode( const Node<DIM>& node);
	//MGSize	InsertNode( const Node<DIM>& node)	{ mtabNode.push_back( node); return mtabNode.size()-1;}

	void	EraseCell( const MGSize& ic)		{ mtabCell.erase( ic); }
	void	EraseCellSafe( const MGSize& ic);

	void	EraseNode( const MGSize& in)		{ mtabNode.erase( in); }



	//// IO 
	//void	IOTabNodeResize( const MGSize& n)		{ mtabNode.resize( n);}
	//void	IOTabCellResize( const MGSize& n)		{ mtabCell.resize( n);}

	//void	IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id );
	//void	IOSetCellIds( const vector<MGSize>& tabid, const MGSize& id );

private:
	ColNode		mtabNode;
	ColCell		mtabCell;
};
//////////////////////////////////////////////////////////////////////


template <Dimension DIM> 
inline Box<DIM> Grid<DIM>::FindBoundingBox() const
{
	GVect vmin, vmax;
	vmin = vmax = cColNode().begin()->cPos();

	for ( typename Grid<DIM>::ColNode::const_iterator itrn = cColNode().begin(); itrn != cColNode().end(); ++itrn)
	{
		vmin = Minimum( vmin, itrn->cPos() );
		vmax = Maximum( vmin, itrn->cPos() );
	}

	return Box<DIM>( vmin, vmax);
}


template <Dimension DIM> 
inline MGSize Grid<DIM>::InsertCell( const Cell<DIM>& cell)	
{ 
	MGSize id = mtabCell.insert( cell);
	mtabCell[id].rId() = id;
	return id;
}


template <Dimension DIM> 
inline void Grid<DIM>::EraseCellSafe( const MGSize& ic)
{ 
	GCell& cell = rCell(ic);

	for ( MGSize i=0; i<GCell::SIZE; ++i)
	{
		MGSize icnext = cell.cCellId(i).first;
		if ( icnext)
		{
			rCell(icnext).rCellId( cell.cCellId(i).second ).first = 0;
		}
	}

	mtabCell.erase( ic); 
}


template <Dimension DIM> 
inline MGSize Grid<DIM>::InsertNode( const Node<DIM>& node)	
{ 
	MGSize id = mtabNode.insert( node);
	mtabNode[id].rId() = id;
	return id;
}


//template <Dimension DIM> 
//inline void Grid<DIM>::IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id )	
//{
//	GVect vpos;
//	for ( MGSize i=0; i<DIM; ++i)
//		vpos.rX(i) = tabx[i];
//
//	GNode &node = rNode(id);
//	node.rId() = id;
//	node.rMasterId() = 0;
//	node.rPos() = vpos;
//}
//
//
//template <Dimension DIM> 
//inline void Grid<DIM>::IOSetCellIds( const vector<MGSize>& tabid, const MGSize& id )	
//{
//	GCell &cell = rCell(id);
//	cell.rId() = id;
//	for ( MGSize i=0; i<=DIM; ++i)
//		cell.rNodeId(i) = tabid[i];
//
//}






//////////////////////////////////////////////////////////////////////
//	class Grid - specialization for DIM_1D
//////////////////////////////////////////////////////////////////////
template <> 
class Grid<DIM_1D> : public Entity
{
	typedef TDefs<DIM_1D>::GVect	GVect;
	typedef TDefs<DIM_1D>::GMetric	GMetric;

	typedef TDefs<DIM_1D>::GNode	GNode;
	typedef TDefs<DIM_1D>::GCell	GCell;

	friend class Master<DIM_2D>;
	friend class Master<DIM_3D>;

	friend class GridControlSpace<DIM_1D,DIM_2D>;
	friend class GridControlSpace<DIM_1D,DIM_3D>;

	friend class GridGlobControlSpace<DIM_1D>;

	friend class Kernel<DIM_1D>;
	friend class Generator<DIM_1D>;

public:
	typedef fgvector<GNode>	ColNode;
	typedef fgvector<GCell>	ColCell;

	Grid()		{}

	const GCell&	cCell( const MGSize& i) const	{ return mtabCell[i];}
	const GNode&	cNode( const MGSize& i) const	{ return mtabNode[i];}

	const ColCell&	cColCell() const				{ return mtabCell;}
	const ColNode&	cColNode() const				{ return mtabNode;}

	MGSize			SizeNodeTab() const				{ return mtabNode.size_valid();}
	MGSize			SizeCellTab() const				{ return mtabCell.size_valid();}

	//void	CheckConnectivity() const;
	//void	Dump( const MGString& fname) const;


protected:
	GCell&			rCell( const MGSize& i)			{ return mtabCell[i];}
	GNode&			rNode( const MGSize& i)			{ return mtabNode[i];}

	ColCell&		rColCell()						{ return mtabCell;}
	ColNode&		rColNode()						{ return mtabNode;}

	MGSize	InsertCell( const Cell<DIM_1D>& cell);
	MGSize	InsertNode( const Node<DIM_1D>& node);

	void	EraseCell( const MGSize& ic)		{ mtabCell.erase( ic); }
	void	EraseNode( const MGSize& in)		{ mtabNode.erase( in); }

private:
	ColNode		mtabNode;
	ColCell		mtabCell;
};


inline MGSize Grid<DIM_1D>::InsertCell( const Cell<DIM_1D>& cell)	
{ 
	MGSize id = mtabCell.insert( cell);
	mtabCell[id].rId() = id;
	return id;
}

inline MGSize Grid<DIM_1D>::InsertNode( const Node<DIM_1D>& node)	
{ 
	MGSize id = mtabNode.insert( node);
	mtabNode[id].rId() = id;
	return id;
}


//////////////////////////////////////////////////////////////////////
//	class Grid - specialization for DIM_0D
//////////////////////////////////////////////////////////////////////
template <> 
class Grid<DIM_0D> : public Entity
{
public:
	Grid() : mMasterId(0)		{}

	MGSize&			rMasterId()			{ return mMasterId;}
	const MGSize&	cMasterId() const	{ return mMasterId;}

private:
	MGSize	mMasterId;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GRID_H__
