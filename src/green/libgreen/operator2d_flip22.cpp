#include "operator2d_flip22.h"

#include "libgreen/grid.h"
#include "libgreen/gridgeom.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class Operator2D_Flip22
//////////////////////////////////////////////////////////////////////
bool Operator2D_Flip22::Check( const MGSize& ic1, const MGSize& ic2)
{
	GCell	&c1 = mpGrd->rCell( ic1 );
	GCell	&c2 = mpGrd->rCell( ic2 );

	MGSize	ifc1 = GCell::SIZE + 10;	// invalid value
	MGSize	ifc2 = GCell::SIZE + 10;

	for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
	{
		if ( c1.cCellId( ifc).first == ic2 )
			ifc1 = ifc;

		if ( c2.cCellId( ifc).first == ic1 )
			ifc2 = ifc;
	}

	if ( ifc1 >= GCell::SIZE || ifc2 >= GCell::SIZE )
		THROW_INTERNAL( "Operator2D_Flip22::Check : not-adjacent cells");

	MGSize	tabid1[GCell::SIZE], tabid2[GCell::SIZE];

	for ( MGSize i=0; i<GCell::SIZE; ++i)
	{
		tabid1[i] = i + ifc1;
		if ( tabid1[i] >= GCell::SIZE )
			tabid1[i] -= GCell::SIZE;

		tabid2[i] = i + ifc2;
		if ( tabid2[i] >= GCell::SIZE )
			tabid2[i] -= GCell::SIZE;
	}

	mCellUp.Reset();
	mCellUp.rId() = c1.cId();
	mCellUp.rNodeId( 0) = c1.cNodeId( tabid1[0] );
	mCellUp.rNodeId( 1) = c2.cNodeId( tabid2[0] );
	mCellUp.rNodeId( 2) = c1.cNodeId( tabid1[2] );
	mCellUp.rCellId( 0) = c2.cCellId( tabid2[2] );
	mCellUp.rCellId( 1) = c1.cCellId( tabid1[1] );
	mCellUp.rCellId( 2) = GNeDef( c2.cId(), 2);

	mCellLo.Reset();
	mCellLo.rId() = c2.cId();
	mCellLo.rNodeId( 0) = c2.cNodeId( tabid2[0] );
	mCellLo.rNodeId( 1) = c1.cNodeId( tabid1[0] );
	mCellLo.rNodeId( 2) = c2.cNodeId( tabid2[2] );
	mCellLo.rCellId( 0) = c1.cCellId( tabid1[2] );
	mCellLo.rCellId( 1) = c2.cCellId( tabid2[1] );
	mCellLo.rCellId( 2) = GNeDef( c1.cId(), 2);

	Simplex<DIM_2D>	tri1 = GridGeom<DIM_2D>::CreateSimplex( mCellUp, *mpGrd );
	Simplex<DIM_2D>	tri2 = GridGeom<DIM_2D>::CreateSimplex( mCellLo, *mpGrd );

	MGFloat	vol1 = tri1.Volume();
	MGFloat	vol2 = tri2.Volume();

	//MGFloat zero = 1.0e-20;
	MGFloat zero = 1.0e-8;
	//if ( vol1 > 0 && vol2 > 0 )		// TODO :: threshold ?!
	if ( vol1 > zero && vol2 > zero )		// TODO :: threshold ?!
		mbReady = true;
	else
		mbReady = false;

	return mbReady;
}


bool Operator2D_Flip22::Execute( const MGSize& ic1, const MGSize& ic2)
{
	bool bok = Check( ic1, ic2);
	Execute();

	return bok;
}

void Operator2D_Flip22::Execute()
{
	if ( mbReady )
	{
		mpGrd->rCell( mCellUp.cId() ) = mCellUp;
		mpGrd->rCell( mCellLo.cId() ) = mCellLo;

		if ( mCellLo.cCellId(0).first )
			mpGrd->rCell( mCellLo.cCellId(0).first ).rCellId( mCellLo.cCellId(0).second) = GNeDef( mCellLo.cId(), 0);

		if ( mCellLo.cCellId(1).first )
			mpGrd->rCell( mCellLo.cCellId(1).first ).rCellId( mCellLo.cCellId(1).second) = GNeDef( mCellLo.cId(), 1);

		if ( mCellUp.cCellId(0).first )
			mpGrd->rCell( mCellUp.cCellId(0).first ).rCellId( mCellUp.cCellId(0).second) = GNeDef( mCellUp.cId(), 0);

		if ( mCellUp.cCellId(1).first )
			mpGrd->rCell( mCellUp.cCellId(1).first ).rCellId( mCellUp.cCellId(1).second) = GNeDef( mCellUp.cId(), 1);

		mbReady = false; // <--------------
	}
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

