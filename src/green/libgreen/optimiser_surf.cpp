#include "optimiser_surf.h"

#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/operator2d_flip22.h"

#include "libcorecommon/progressbar.h"
#include "libgreen/writegrdtec.h"

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_intersection.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


void OptimiserSurf::DoOptimise( const MGSize& maxiter )
{
	Grid<DIM_2D>::ColCell::iterator	itr;
	
	ProgressBar	bar(40);

	MGSize iter = 1;
	MGSize countswap = 1;

	while ( countswap && iter <= maxiter)
	{
		for ( itr = mpGrid->rColCell().begin(); itr != mpGrid->rColCell().end(); ++itr)
			itr->SetModified( false);

		bar.Init( mpGrid->SizeCellTab() );
		bar.Start();

		MGSize tabidn[4];
		MGSize count = 0;

		countswap = 0;

		for ( itr = mpGrid->rColCell().begin(); itr != mpGrid->rColCell().end(); ++itr, ++bar)
		{
			GCell &cell = *itr;

			//Simplex<DIM_2D>	tri = GridGeom<DIM_2D>::CreateSimplex( cell, *mpGrid);

			if ( cell.IsExternal() || cell.IsModified() )
				continue;

			for ( MGSize ifac=0; ifac<GCell::SIZE; ++ifac)
			{
				GCell &cellnei = mpGrid->rCell( cell.cCellId( ifac).first );

				if ( cellnei.IsExternal() || cellnei.IsModified() || cellnei.cId() == 0 )
					continue;

				tabidn[0] = cell.cNodeId( Simplex<DIM_2D>::cFaceConn( ifac, 0 ) );
				tabidn[1] = cell.cNodeId( Simplex<DIM_2D>::cFaceConn( ifac, 1 ) );
				tabidn[2] = cell.cNodeId( ifac );
				tabidn[3] = cellnei.cNodeId( cell.cCellId( ifac).second );

				GVect tabvploc[4];
				Vect3D tabvpglob[4];
				for ( MGSize i=0; i<4; ++i)
				{
					tabvploc[i]= mpGrid->cNode( tabidn[i]).cPos();
					mpSurf->GetCoord( tabvpglob[i], tabvploc[i] );
				}

				typedef HFGeom::GVector<MGFloat,2> Vector;

				HFGeom::SubSimplex<MGFloat,1,2> suba( HFGeom::SubSimplex<MGFloat,1,2>::ARRAY( Vector( tabvploc[0].cX(), tabvploc[0].cY()), Vector( tabvploc[1].cX(), tabvploc[1].cY()) ) );	
				HFGeom::SubSimplex<MGFloat,1,2> subb( HFGeom::SubSimplex<MGFloat,1,2>::ARRAY( Vector( tabvploc[2].cX(), tabvploc[2].cY()), Vector( tabvploc[3].cX(), tabvploc[3].cY()) ) );	

				HFGeom::IntersectionFull<MGFloat,MGFloat,1,1> inter( suba, subb);
				inter.Execute();

				MGFloat	det = inter.cDet();
				if ( ! inter.IsOk() )
				{
					THROW_INTERNAL( "intersection determinant is ZERO" );
				}

				MGFloat	tabA[2], tabB[2];
				inter.GetABaryCoords( tabA);
				inter.GetBBaryCoords( tabB);

				tabA[0] /= det;		tabA[1] /= det;
				tabB[0] /= det;		tabB[1] /= det;

				GVect vc1 = tabA[0]*tabvploc[0] + tabA[1]*tabvploc[1];
				GVect vc2 = tabB[0]*tabvploc[2] + tabB[1]*tabvploc[3];

				Vect3D vcglob1 = tabA[0]*tabvpglob[0] + tabA[1]*tabvpglob[1];
				Vect3D vcglob2 = tabB[0]*tabvpglob[2] + tabB[1]*tabvpglob[3];

				GVect vc = 0.5*(vc1+vc2);


				Vect3D vcglob;

				mpSurf->GetCoord( vcglob, vc );


				if ( (vcglob1 - vcglob).module() > (vcglob2 - vcglob).module() + 1.0e-5 )
					//if ( 2.5*(tabvpglob[1] - tabvpglob[0]).module() > (tabvpglob[3] - tabvpglob[2]).module() )
				{
					++count;

					Operator2D_Flip22	flip( *mpGrid );

					if ( flip.Check( cell.cId(), cellnei.cId() ) )
					{
						flip.Execute();
						cell.SetModified( true);
						cellnei.SetModified( true);
						++countswap;
					}

				}

			}
		}

		bar.Finish();
		cout << "found edges to swap; " << countswap << endl;


		++iter;
		//cout << "found " << count << " edges to swap; " << countswap << endl;
	}
				
				
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

