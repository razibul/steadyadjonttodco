#ifndef __FACESPLIT_H__
#define __FACESPLIT_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libgreen/grid.h"
#include "libgreen/edgesplit.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

class EdgeReconstructor;

template <Dimension DIM> 
class Grid;



//////////////////////////////////////////////////////////////////////
// class FaceSplit
//////////////////////////////////////////////////////////////////////
class FaceSplit
{
	typedef TDefs<DIM_3D>::GVect		GVect;
	typedef TDefs<DIM_3D>::GCell		GCell;

public:
	FaceSplit()		{}
	FaceSplit( const Key<3>& face) : mFace(face)	{}

	MGSize	Size() const							{ return mtabSubFaces.size();}
	MGSize	SizeInNodes() const						{ return mtabINodes.size();}
	Key<3>	SubFaceSorted( const MGSize& id) const	{ Key<3> key=mtabSubFaces[id]; key.Sort(); return key; }

	void	InsertNode( const MGSize& id)			{ mtabINodes.push_back( id); }

	void	UpdateSubFaces( Kernel<DIM_3D>& kernel );
	void	UpdateSubFaces_new( Kernel<DIM_3D>& kernel );

	bool	IsRecoverd();
	//void	FindRecPoints( pair<EdgeSplit*,GVect>& prop, Kernel<DIM_3D>& kernel);
	void	FindRecPoints( EdgeSplit** pparEdge, Key<2>& subEdge, GVect& propvct, Kernel<DIM_3D>& kernel );
	void	FindRecPoints( GVect& propvct, Kernel<DIM_3D>& kernel);

	const Key<3>&	cFace()	const								{ return mFace; }
	void			SetEdge( const MGSize& i, EdgeSplit* ptr)	{ mtabSplitEdge[i] = ptr; }
	EdgeSplit*		GetEdge( const MGSize& i) const				{ return mtabSplitEdge[i]; }

	void	ExportTEC( ostream& file, const Grid<DIM_3D>& grid);

	bool	IsChanged() const;

private:
	pair<MGSize,MGSize>	mControlSum;

	Key<3>			mFace;
	vector<MGSize>	mtabINodes;
	EdgeSplit*		mtabSplitEdge[3];

	vector< Key<3> > mtabSubFaces;

	vector< Key<2> > mtabTmpEdges;
};


inline bool FaceSplit::IsChanged() const
{
	if ( mControlSum.first != mtabSplitEdge[0]->SizeInNodes() + mtabSplitEdge[1]->SizeInNodes() + mtabSplitEdge[2]->SizeInNodes() )
		return true;

	if ( mControlSum.second != mtabINodes.size() )
		return true;

	return false;
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __FACESPLIT_H__
