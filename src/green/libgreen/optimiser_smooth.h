#ifndef __OPTIMISER_SMOOTH_H__
#define __OPTIMISER_SMOOTH_H__


#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libcoreconfig/configbase.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class BndGrid;

template <Dimension DIM> 
class Kernel;





//////////////////////////////////////////////////////////////////////
// class OptimiserSmooth
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class OptimiserSmooth : public ConfigBase
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

	typedef typename TDefs<DIM>::GNode	GNode;
	typedef typename TDefs<DIM>::GCell	GCell;
	typedef typename TDefs<DIM>::GFace	GFace;

public:
	OptimiserSmooth( Grid<DIM>& grd) : mpGrid(&grd)		{}

	void	DoOptimise( const MGSize& maxiter = 1 );

private:
	Grid<DIM>							*mpGrid;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



#endif // __OPTIMISER_SMOOTH_H__
