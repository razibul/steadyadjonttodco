#include "writebndtec.h"
#include "libgreen/bndgrid.h"
#include "libcoreio/store.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//
////template <>
////void WriteBndTEC<DIM_2D>::DoWriteExploded( const MGString& fname)
//template <>
//void WriteBndTEC<DIM_2D>::DoWrite( const MGString& fname)
//{
//	Vect2D	vc(0,0);
//	MGSize n = 0;
//
//	for ( BndGrid<DIM_2D>::ColBNode::const_iterator itrn = mpGrd->cColNode().begin(); itrn != mpGrd->cColNode().end(); ++itrn)
//	{
//		vc += (*itrn).cPos();
//		++n;
//	}
//	vc /= (MGFloat)n;
//
//	mFName = fname;
//	Store::File f( fname, "wt");
//
//	cout << "\nWriting TEC '" << fname.c_str() << "'" << std::flush;
//
//	for ( BndGrid<DIM_2D>::ColBCell::const_iterator itrc = mpGrd->cColCell().begin(); itrc != mpGrd->cColCell().end(); ++itrc)
//	{
//		Vect2D v1 = mpGrd->cNode( (*itrc).cNodeId(0) ).cPos();
//		Vect2D v2 = mpGrd->cNode( (*itrc).cNodeId(1) ).cPos();
//		Vect2D vm = 0.5*(v1+v2);
//		Vect2D vdist = 1.0*(vm - vc);
//		v1 += vdist;
//		v2 += vdist;
//
//		fprintf( f, "VARIABLES = \"X\", \"Y\"\n");
//		fprintf( f, "ZONE T=\"seg_%d\", N=2, E=1, F=FEPOINT, ET=LINESEG, C=BLACK\n", (*itrc).cId() ); 
//		fprintf( f, "%10.7lg %10.7lg\n", v1.cX(), v1.cY() );
//		fprintf( f, "%10.7lg %10.7lg\n", v2.cX(), v2.cY() );
//		fprintf( f, "1 2\n" );
//	}
//
//	cout << " - FINISHED\n";
//}

template <>
void WriteBndTEC<DIM_2D>::DoWrite( const MGString& fname)
{
	mFName = fname;
	ofstream f( fname.c_str() );

	cout << "\nWriting TEC '" << fname.c_str() << "'" << std::flush;

	f << "TITLE = \"boundary grid\"\n";
	f << "VARIABLES = ";
	f << "\"X\", \"Y\"";
	f << "\n";

	f << "ZONE T=\"bnd grid\", N=" << mpGrd->SizeNodeTab() << ", E=" << mpGrd->SizeCellTab() << ", F=FEPOINT, ET=LINESEG\n";

	for ( BndGrid<DIM_2D>::ColBNode::const_iterator itrn = mpGrd->cColNode().begin(); itrn != mpGrd->cColNode().end(); ++itrn)
	{
		for ( MGSize k=0; k<DIM_2D; ++k)
			f << setprecision(8) << (*itrn).cPos().cX( k) << " ";
		f << endl;
	}

	for ( BndGrid<DIM_2D>::ColBCell::const_iterator itrc = mpGrd->cColCell().begin(); itrc != mpGrd->cColCell().end(); ++itrc)
	{
		f << (*itrc).cNodeId(0) << " " << (*itrc).cNodeId(1) << endl;
	}

	cout << " - FINISHED\n";
}



template <>
void WriteBndTEC<DIM_3D>::DoWrite( const MGString& fname)
{
	mFName = fname;
	ofstream f( fname.c_str() );

	cout << "\nWriting TEC '" << fname.c_str() << "'" << std::flush;


	f << "TITLE = \"boundary grid\"\n";
	f << "VARIABLES = ";
	f << "\"X\", \"Y\", \"Z\"";
	f << "\n";

	f << "ZONE T=\"bnd grid\", N=" << mpGrd->SizeNodeTab() << ", E=" << mpGrd->SizeCellTab() << ", F=FEPOINT, ET=TRIANGLE\n";

	for ( BndGrid<DIM_3D>::ColBNode::const_iterator itrn = mpGrd->cColNode().begin(); itrn != mpGrd->cColNode().end(); ++itrn)
	{
		for ( MGSize k=0; k<DIM_3D; ++k)
			f << setprecision(8) << (*itrn).cPos().cX( k) << " ";
		f << endl;
	}

	for ( BndGrid<DIM_3D>::ColBCell::const_iterator itrc = mpGrd->cColCell().begin(); itrc != mpGrd->cColCell().end(); ++itrc)
	{
		f << (*itrc).cNodeId(0) << " " << (*itrc).cNodeId(1) << " " << (*itrc).cNodeId(2) << endl;
	}

	cout << " - FINISHED\n";
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

