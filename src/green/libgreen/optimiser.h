#ifndef __OPTIMISER_H__
#define __OPTIMISER_H__


#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libcoreconfig/configbase.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class BndGrid;

template <Dimension DIM> 
class Kernel;





//////////////////////////////////////////////////////////////////////
// class Optimiser
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Optimiser : public ConfigBase
{
public:
	Optimiser( Kernel<DIM>& kernel) 
		: mpKernel(&kernel)		{}

	void	DoOptimise( const MGSize& passcount = 1 );

	MGSize	OptimiseEdges( const vector< Key<2> >& tabedge );
	MGSize	OptimiseFaces( const vector< Key<3> >& tabface );

	MGSize	OptimiseCells( const vector<MGSize>& tabcell );
	MGSize	OptimiseNodes( const vector<MGSize>& tabnode, const MGSize& passcount = 1 );

private:
	Kernel<DIM>		*mpKernel;
	vector<MGSize>	mtabCell;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __OPTIMISER_H__
