#include "reconstructor.h"
#include "libgreen/grid.h"
#include "libgreen/bndgrid.h"
#include "libgreen/kernel.h"
#include "libgreen/gridgeom.h"
#include "libgreen/controlspace.h"

#include "libgreen/writegrdtec.h"

//#include "edgereconstructor.h"
//#include "facereconstructor.h"

#include "libcorecommon/progressbar.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// EDGES
//////////////////////////////////////////////////////////////////////

void Reconstructor<DIM_3D>::InitEdges()
{
	mpBndGrd->FindEdges( mtabBndEdges);

	mtabSplitEdges.resize( mtabBndEdges.size() );

	for ( MGSize i=0; i<mtabBndEdges.size(); ++i)
		mtabSplitEdges[i] = EdgeSplit( mtabBndEdges[i], *mpCSpace, *mpKernel );

	random_shuffle( mtabSplitEdges.begin(), mtabSplitEdges.end() );

	FindMissingEdges();
}



void Reconstructor<DIM_3D>::FindMissingEdges()
{
	for ( MGSize i=0; i<mtabSplitEdges.size(); ++i)
		mtabSplitEdges[i].UpdateSubElems();

	// find no of edges to recover
	MGSize n = 0;
	for ( MGSize i=0; i<mtabSplitEdges.size(); ++i)
		n += mtabSplitEdges[i].Size();

	// fill tab of edges to reconstruct
	vector< pair< Key<2>,MGSize> >	tabedge;
	tabedge.reserve( n);

	for ( MGSize i=0; i<mtabSplitEdges.size(); ++i)
		for ( MGSize k=0; k<mtabSplitEdges[i].Size(); ++k)
		{
			pair< Key<2>,MGSize> tmp;
			tmp.first = mtabSplitEdges[i].cSubEdge( k );
			tmp.second = i;
			tabedge.push_back( tmp);
		}


	//find missing edges
	vector< Key<2> >	tabgrde;

	mpKernel->rGrid().FindEdges( tabgrde);
	sort( tabgrde.begin(), tabgrde.end() );	// TODO :: tabgrde should be already sorted; check this

	mtabRecEdges.clear();
	
	for ( vector< pair< Key<2>,MGSize> >::iterator itr = tabedge.begin(); itr != tabedge.end(); ++itr)
	{
		bool bFound = binary_search( tabgrde.begin(), tabgrde.end(), itr->first );

		if ( !bFound)
			mtabRecEdges.push_back( itr->second);	// edges to reconstruct
		else
			mPrEntities.AddEdge( itr->first );	// edges to protect
	}

	cout << "found " << mtabRecEdges.size() << " missing edges" << endl;
	TRACE( "found " << mtabRecEdges.size() << " missing edges" );
}



bool Reconstructor<DIM_3D>::IsEdgeRecovered( const Key<2>& edge)
{
	vector<MGSize>	tabball;
	vector<MGSize>	tabnode;

	tabnode.push_back( edge.cFirst() );
	tabnode.push_back( edge.cSecond() );
	
	if ( ! mpKernel->FindBall( tabball, edge.cFirst() ) )	
	//if ( ! mpKernel->FindBall( tabball, tabnode ) )	
	{
		cout << "failed to find ball [" << edge.cFirst() << ", " << edge.cSecond() << "]\n";
		TRACE( "failed to find ball [" << edge.cFirst() << ", " << edge.cSecond() << "]\n"  );

		THROW_INTERNAL( "failed to find ball");
		
		return false;
	}
	
	//// dump
	//WriteGrdTEC<DIM_3D>	write( & mpKernel->rGrid() );
	//write.WriteCells( "_ball_rec.dat", tabball, "ball");


	vector< Key<2> > tabedge;
	tabedge.reserve( GCell::ESIZE * tabball.size() );

	for ( MGSize i=0; i<tabball.size(); ++i)
	{
		const GCell& cell = mpKernel->rGrid().cCell( tabball[i] );
	
		for ( MGSize k=0; k<GCell::ESIZE; ++k)
		{
			Key<2> key = cell.EdgeKey( k);
			key.Sort();
			tabedge.push_back( key);
		}
	}


	//ofstream of( "-dump.txt");
	//of << edge.cFirst() << " " << edge.cSecond() << endl << endl;
	//for ( MGSize i=0; i<tabedge.size(); ++i)
	//	of << i << "   -  " << tabedge[i].cFirst() << " " << tabedge[i].cSecond() << endl;

	sort( tabedge.begin(), tabedge.end() );

	bool bFound = binary_search( tabedge.begin(), tabedge.end(), edge);
	//vector< Key<2> >::iterator itrlast = unique( tabedge.begin(), tabedge.end() );
	//bool bFound = binary_search( tabedge.begin(), itrlast, edge);

	return bFound;
}


MGSize Reconstructor<DIM_3D>::ReconstructSplitEdges()
{
	cout << endl << "ReconstructSplitEdges()" << endl;

	ProgressBar	bar(40);
	MGSize maxpassno = 1000;

	for ( MGSize i=0; i<maxpassno; ++i)
	{
		bool bBreak = true;

		FindMissingEdges();

		if ( mtabRecEdges.size() == 0 )
			return 0;



		bar.Init( mtabRecEdges.size() );
		bar.Start();

		for ( MGSize k=0; k<mtabRecEdges.size(); ++k, ++bar)
		{
			cout << endl << " EDGE - " << k << endl << endl;
			mtabSplitEdges[ mtabRecEdges[k] ].Recover( mPrEntities );
		}

		bar.Finish();

	//	for ( vector< pair< Key<2>,MGSize> >::iterator itr = mtabRecEdges.begin(); itr != mtabRecEdges.end(); ++itr)
	//	{
	//		if ( ! IsEdgeRecovered( itr->first ) )
	//		{
	//			bBreak = false;
	//			GVect vct1 = mpKernel->rGrid().cNode( itr->first.cFirst() ).cPos();
	//			GVect vct2 = mpKernel->rGrid().cNode( itr->first.cSecond() ).cPos();
	//			GVect vct = 0.5 * ( vct1 + vct2);

	//			//ofstream f( "_edge_.dat");
	//			//const Key<2>& edge = itr->first;
	//			//f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	//			//f << "ZONE T=\"edge\"\n";
	//			//f << vct1.cX() << " " << vct1.cY() << " " << vct1.cZ() << " " << edge.cFirst() << endl;
	//			//f << vct2.cX() << " " << vct2.cY() << " " << vct2.cZ() << " " << edge.cSecond() << endl;
	//			//f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	//			//f << "ZONE T=\"cross\"\n";
	//			//f << vct.cX() << " " << vct.cY() << " " << vct.cZ() << " " << 0 << endl;

	//			//MGSize inod = mpKernel->InsertPoint( vct );
	//			MGSize inod = mpKernel->InsertPointProtect( vct, msetProEdges, msetProFaces );

	//			if ( ! inod)
	//			{
	//				THROW_INTERNAL( "CRASH :: kernel failed to insert new point with coordinates [" 
	//					<< vct.cX() << " " << vct.cY() << " " << vct.cZ() << "]" );
	//			}

	//			mpKernel->InsertMetric( mpCSpace->GetSpacing( vct), inod );

	//			mtabSplitEdges[itr->second].InsertNode( inod, mpKernel->rGrid() );

	//			Key<2> kedge;

	//			kedge = Key<2>( itr->first.cFirst(), inod );
	//			kedge.Sort();
	//			if ( IsEdgeRecovered( kedge) )
	//				msetProEdges.insert( kedge);

	//			kedge = Key<2>( inod, itr->first.cSecond() );
	//			kedge.Sort();
	//			if ( IsEdgeRecovered( kedge) )
	//				msetProEdges.insert( kedge);
	//		}
	//		else
	//		{
	//			msetProEdges.insert( itr->first );
	//		}
	//	}

		if ( mtabRecEdges.size() == 0 )
			break;
	}

	FindMissingEdges();

	return mtabRecEdges.size();
}





//////////////////////////////////////////////////////////////////////
// FACES
//////////////////////////////////////////////////////////////////////

void Reconstructor<DIM_3D>::InitFaces()
{
	cout << "FaceReconstructor::Init()" << endl;

	ProgressBar	bar(40);

	mpBndGrd->FindFaces( mtabBndFaces);


	// finding boundary faces
	mtabSplitFaces.resize( mtabBndFaces.size() );

	// preparing tab of split-faces
	for ( MGSize i=0; i<mtabBndFaces.size(); ++i)
		mtabSplitFaces[i] = FaceSplit( mtabBndFaces[i] );


	// setting split-edges link for every split-face
	vector< pair< Key<2>, MGSize > >	tabref( mtabSplitEdges.size() );

	for ( MGSize i=0; i<mtabSplitEdges.size(); ++i)
		tabref[i] = pair< Key<2>, MGSize >( mtabSplitEdges[i].cEdge(), i );

	sort( tabref.begin(), tabref.end() );


	bar.Init( mtabSplitFaces.size() );
	bar.Start();

	for ( MGSize i=0; i<mtabSplitFaces.size(); ++i, ++bar)
	{
		Key<3> key = mtabSplitFaces[i].cFace();

		for ( MGSize k=0; k<3; ++k)
		{
			Key<2> edge( key.cFirst(), key.cSecond() );
			edge.Sort();

			vector< pair< Key<2>, MGSize > >::iterator	itr;
			itr = lower_bound( tabref.begin(), tabref.end(), pair< Key<2>, MGSize >( edge, 0 ) );
			if ( itr == tabref.end() )
				THROW_INTERNAL( "FaceReconstructor::Init -- failed !!! split-edge not found");

			ASSERT( itr->first == edge );
			ASSERT( mtabSplitEdges[itr->second].cEdge() == edge );

			EdgeSplit*	ptr = &( mtabSplitEdges[itr->second] );

			mtabSplitFaces[i].SetEdge( k, ptr );

			key.Rotate(1);
		}
	}

	bar.Finish();


	//bar.Init( mtabSplitFaces.size() );
	//bar.Start();

	//for ( MGSize i=0; i<mtabSplitFaces.size(); ++i, ++bar)
	//	mtabSplitFaces[i].UpdateSubFaces( *mpKernel );

	//bar.Finish();

	FindMissingFaces();
}


void Reconstructor<DIM_3D>::FindMissingFaces()
{
	cout << "FaceReconstructor::FindMissingFaces()" << endl;

	ProgressBar	bar(40);

	bar.Init( mtabSplitFaces.size() );
	bar.Start();

	for ( MGSize i=0; i<mtabSplitFaces.size(); ++i, ++bar)
		mtabSplitFaces[i].UpdateSubFaces( *mpKernel);

	bar.Finish();


	MGSize nmiss = 0;

	bar.Init( mtabSplitFaces.size() );
	bar.Start();

	ofstream f( "_missing_faces.dat" );

	mtabRecFaces.clear();

	for ( MGSize i=0; i<mtabSplitFaces.size(); ++i, ++bar)
		if ( ! mtabSplitFaces[i].IsRecoverd() )
		{
			mtabRecFaces.push_back( i);
		}
		else
		{
			if ( mtabSplitFaces[i].Size() > 0 )
			{
				for ( MGSize k=0; k<mtabSplitFaces[i].Size(); ++k)
					msetProFaces.insert( mtabSplitFaces[i].SubFaceSorted(k) );
			}
			else
			{
				msetProFaces.insert( mtabSplitFaces[i].cFace() );	// face to protect
			}
		}

	bar.Finish();

	cout << "found " << mtabRecFaces.size() << " missing faces" << endl;
	TRACE( "found " << mtabRecFaces.size() << " missing faces" << endl );
}



MGSize Reconstructor<DIM_3D>::ReconstructSplitFaces()
{
	cout << endl << "ReconstructSplitFaces()" << endl;
	FindMissingFaces();

	typedef pair< GVect, pair< EdgeSplit*, FaceSplit* > >	TPropPnt;

	ProgressBar	bar(40);

	bool bBreak;
	MGSize maxpassno = 100;

	for ( MGSize i=0; i<maxpassno; ++i)
	{
		bBreak = true;

		cout << endl << "face reconstruction iter = " << i << endl;
		MGSize nmiss = 0;

		vector< TPropPnt > tabproppnt;

		random_shuffle( mtabRecFaces.begin(), mtabRecFaces.end() );


		bar.Init( mtabRecFaces.size() );
		bar.Start();

		for ( MGSize k=0; k<mtabRecFaces.size(); ++k, ++bar)
		{
			//if ( ! mtabSplitFaces[i].IsChanged() )
			//	continue;

			const MGSize i = mtabRecFaces[k];

			mtabSplitFaces[i].UpdateSubFaces( *mpKernel);

			if ( ! mtabSplitFaces[i].IsRecoverd() )
			{
				cout << k << " - split face " << i << endl;
		
				MGSize iter = 0;
				do
				{
					if ( ++iter > 250 )
					{
						ofstream of( "_facesplit_500.dat");
						mtabSplitFaces[i].ExportTEC( of, mpKernel->rGrid() );
						cout << "FaceSplit -- CRASH !!! iter limit " << iter;

						bBreak = false;
						break;
					}

					//if ( ++iter > 500 )
					//{
					//	//bBreak = false;
					//	THROW_INTERNAL( "FaceSplit -- CRASH !!! iter limit " << iter );
					//}

					EdgeSplit* pedge = NULL;
					Key<2> sedge;
					GVect propvct;

					//mtabSplitFaces[i].FindRecPoints( &pedge, sedge, propvct, *mpKernel );
					mtabSplitFaces[i].FindRecPoints( propvct, *mpKernel );

					if ( pedge != NULL )
					{
						// find sub edge
						GVect v0 = mpKernel->rGrid().cNode( pedge->cEdge().cFirst() ).cPos();
						MGFloat d0 = (propvct - v0).module();

						bool bFound = false;
						for ( MGSize ie=0; ie<pedge->Size(); ++ie)
						{
							Key<2> e = pedge->cSubEdge( ie);
							MGFloat d1 = ( mpKernel->rGrid().cNode( e.cFirst() ).cPos() - v0 ).module();
							MGFloat d2 = ( mpKernel->rGrid().cNode( e.cSecond() ).cPos() - v0 ).module();

							if ( d0 > d1 && d0 < d2)
							{
								e.Sort();
								sedge = e;
								bFound = true;
								break;
							}
						}

						if ( ! bFound)
							THROW_INTERNAL( "subedge not found !!!");

						// remove protection for sedge and adjacent faces
						RemoveProtection( pedge, sedge);
					}

					MGSize inod = mpKernel->InsertPointProtect( propvct, msetProEdges, msetProFaces );

					if ( ! inod)
					{
						THROW_INTERNAL( "CRASH :: kernel failed to insert new point with coordinates [" 
							<< propvct.cX() << " " << propvct.cY() << " " << propvct.cZ() << "]" );
					}

					mpKernel->InsertMetric( mpCSpace->GetSpacing( propvct), inod );


					if ( pedge != NULL )
					{
						//pedge->InsertNode( inod, mpKernel->rGrid() );

						//for ( MGSize ie=0; ie<pedge->Size(); ++ie)
						//{
						//	Key<2> kedge = pedge->SubEdgeSorted( ie);

						//	if ( kedge.cFirst() == inod || kedge.cSecond() == inod)
						//		if ( IsEdgeRecovered( kedge) )
						//		{
						//			//cout << "---found" << endl;
						//			msetProEdges.insert( kedge);
						//		}
						//		else
						//		{
						//			//vector<MGSize> tab1, tab2;
						//			//mpKernel->FindBall( tab1, kedge.cFirst() );
						//			//mpKernel->FindBall( tab2, kedge.cFirst() );
						//			//		
						//			//WriteGrdTEC<DIM_3D>	write( & mpKernel->rGrid() );
						//			//write.WriteCells( "_ball_1.dat", tab1, "ball1");
						//			//write.WriteCells( "_ball_2.dat", tab2, "ball2");


						//			//ofstream f( "_edge_.dat");
						//			//const Key<2>& edge = kedge;
						//			//GVect vct1 = mpKernel->rGrid().cNode( edge.cFirst() ).cPos();
						//			//GVect vct2 = mpKernel->rGrid().cNode( edge.cSecond() ).cPos();

						//			//f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
						//			//f << "ZONE T=\"edge\"\n";
						//			//f << vct1.cX() << " " << vct1.cY() << " " << vct1.cZ() << " " << edge.cFirst() << endl;
						//			//f << vct2.cX() << " " << vct2.cY() << " " << vct2.cZ() << " " << edge.cSecond() << endl;

						//			//THROW_INTERNAL( "kedge not found");
						//			ReconstructSplitEdges();
						//		}
						//}

						////////////////////////////////

						////ReconstructSplitEdges();

						//Key<2> kedge;

						//kedge = Key<2>( sedge.cFirst(), inod );
						//kedge.Sort();
						//if ( IsEdgeRecovered( kedge) )
						//{
						//	cout << "---found" << endl;
						//	msetProEdges.insert( kedge);
						//}
						//else
						//	ReconstructSplitEdges();
						////	THROW_INTERNAL( "kedge not found");

						//kedge = Key<2>( inod, sedge.cSecond() );
						//kedge.Sort();
						//if ( IsEdgeRecovered( kedge) )
						//{
						//	cout << "---found" << endl;
						//	msetProEdges.insert( kedge);
						//}
						//else
						//	ReconstructSplitEdges();
						////	THROW_INTERNAL( "kedge not found");

						bBreak = false;
					}
					else
					{
						mtabSplitFaces[i].InsertNode( inod);
					}

					mtabSplitFaces[i].UpdateSubFaces( *mpKernel);

					//cout << mtabSplitFaces[i].Size() << " " << mtabSplitFaces[i].SizeInNodes() << endl;

					//if ( mtabSplitFaces[i].Size() > 0 )
					//	for ( MGSize k=0; k<mtabSplitFaces[i].Size(); ++k)
					//		msetProFaces.insert( mtabSplitFaces[i].SubFaceSorted(k) );

					//if ( i == 2909 )
					//	THROW_INTERNAL( "STOP");

				}
				while ( ! mtabSplitFaces[i].IsRecoverd() );
				//while ( ! mtabSplitFaces[i].IsRecoverd() && bBreak );

				if ( mtabSplitFaces[i].Size() > 0 )
					for ( MGSize k=0; k<mtabSplitFaces[i].Size(); ++k)
						msetProFaces.insert( mtabSplitFaces[i].SubFaceSorted(k) );

			}
			//else
			//{
			//	if ( mtabSplitFaces[i].Size() > 0 )
			//	{
			//		for ( MGSize k=0; k<mtabSplitFaces[i].Size(); ++k)
			//			msetProFaces.insert( mtabSplitFaces[i].SubFaceSorted(k) );
			//	}
			//	else
			//	{
			//		msetProFaces.insert( mtabSplitFaces[i].cFace() );	// face to protect
			//	}
			//}
		}

		bar.Finish();

		if ( bBreak)
			break;

		FindMissingFaces();
	}


	return 0;
}



void Reconstructor<DIM_3D>::RemoveProtection( EdgeSplit* pedge, const Key<2>& sedge)
{
	if ( msetProEdges.find( sedge) == msetProEdges.end() )
		THROW_INTERNAL( "Reconstructor<DIM_3D>::RemoveProtection - sedge not found !!!" );

	msetProEdges.erase( msetProEdges.find( sedge) );

	for ( MGSize i=0; i<mtabSplitFaces.size(); ++i)
	{
		for ( MGSize j=0; j<3; ++j)
			if ( mtabSplitFaces[i].GetEdge( j) == pedge )
			{
				mtabSplitFaces[i].UpdateSubFaces( *mpKernel);

				for ( MGSize k=0; k<mtabSplitFaces[i].Size(); ++k)
				{
					Key<3> sface = mtabSplitFaces[i].SubFaceSorted( k);

					if (	( sface.cFirst() == sedge.cFirst() && sface.cSecond() == sedge.cSecond() ) ||
							( sface.cSecond() == sedge.cFirst() && sface.cFirst() == sedge.cSecond() ) ||

							( sface.cSecond() == sedge.cFirst() && sface.cThird() == sedge.cSecond() ) ||
							( sface.cThird() == sedge.cFirst() && sface.cSecond() == sedge.cSecond() ) ||

							( sface.cThird() == sedge.cFirst() && sface.cFirst() == sedge.cSecond() ) ||
							( sface.cFirst() == sedge.cFirst() && sface.cThird() == sedge.cSecond() ) 
						)
					{

						//if ( msetProFaces.find( sface) == msetProFaces.end() )
						//	THROW_INTERNAL( "Reconstructor<DIM_3D>::RemoveProtection - sface not found !!!" );

						if ( msetProFaces.find( sface) != msetProFaces.end() )
							msetProFaces.erase( msetProFaces.find( sface) );
					}
				}
			}
	}
}

//////////////////////////////////////////////////////////////////////


void Reconstructor<DIM_3D>::Init()
{
	InitEdges();
	InitFaces();
}

void Reconstructor<DIM_3D>::Reconstruct()
{
	// recover missing edges
	ReconstructSplitEdges();

	// recover missing faces
	ReconstructSplitFaces();

	//THROW_INTERNAL( "STOP");
}


void Reconstructor<DIM_3D>::FillBFaceTab( vector< Key<3> >& tabbface)
{
	tabbface.clear();

	for ( MGSize i=0; i<mtabSplitFaces.size(); ++i)
		for ( MGSize k=0; k<mtabSplitFaces[i].Size(); ++k)
			tabbface.push_back( mtabSplitFaces[i].SubFaceSorted( k) );
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

