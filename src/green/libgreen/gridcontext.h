#ifndef __GRIDCONTEXT_H__
#define __GRIDCONTEXT_H__

#include "libcoresystem/mgdecl.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class GridContext
//////////////////////////////////////////////////////////////////////
//template <class T>
//class GridContext
//{
//public:
//
//	MGSize		Insert( const T& t)				{ mtabData.push_back( t); return mtabData.size(); }
//	const T&	cData( const MGSize& id) const	{ return mtabData[id-1];}
//	MGSize		Size() const					{ return mtabData.size(); }
//
//private:
//	vector<T>	mtabData;
//};


template <class T>
class GridContext
{
public:
	GridContext()								{}
	GridContext( const MGSize& n);

	MGSize		Insert( const T& t);
	MGSize		Insert( const MGSize& id, const T& t);

	const T&	cData( const MGSize& id) const	{ return mtabData[id-1];}
	MGSize		Size() const					{ return mtabData.size(); }

	void		Resize( const MGSize& n)		{ mtabData.resize(n); }
	T&			rData( const MGSize& id)		{ return mtabData[id-1];}

private:
	vector<T>	mtabData;
};



template <class T>
GridContext<T>::GridContext( const MGSize& n)
{
	mtabData.reserve( n+1);
}

template <class T>
MGSize GridContext<T>::Insert( const T& t)
{ 
	mtabData.push_back( t); 
	return mtabData.size(); 
}
	
template <class T>
MGSize GridContext<T>::Insert( const MGSize& id, const T& t)	
{
	if ( id > mtabData.size() )
	{
		mtabData.resize( id);
		mtabData[id-1] = t;
	}
	else
	{
		mtabData[id-1] = t;
	}

	return id; 
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GRIDCONTEXT_H__
