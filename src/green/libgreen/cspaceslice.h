#ifndef __CSPACESLICE_H__
#define __CSPACESLICE_H__


#include "libgreen/controlspace.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class SliceControlSpace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, Dimension UNIDIM>
class SliceControlSpace : public DualControlSpace<DIM,UNIDIM>
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

	typedef typename TDefs<UNIDIM>::GVect	UVect;
	typedef typename TDefs<UNIDIM>::GMetric	UMetric;

public:
	SliceControlSpace( const ControlSpace<UNIDIM>* pcs, const GET::GeomEntity<DIM,UNIDIM>* pgent) : mpCS(pcs), mpGEnt( pgent)	{}

	virtual GMetric	GetSpacing( const GVect& vct) const;
	virtual UMetric	GetGlobSpacing( const GVect& vct) const;

private:
	const ControlSpace<UNIDIM>*			mpCS;
	const GET::GeomEntity<DIM,UNIDIM>*	mpGEnt;
};


template <Dimension DIM, Dimension UNIDIM>
inline typename TDefs<DIM>::GMetric SliceControlSpace<DIM,UNIDIM>::GetSpacing( const GVect& vct) const
{
	THROW_INTERNAL( "SliceControlSpace<DIM,UNIDIM>::GetSpacing -- Not implemented !!!");
	return GMetric();
}

template <>
inline TDefs<DIM_1D>::GMetric SliceControlSpace<DIM_1D,DIM_2D>::GetSpacing( const GVect& vct) const
{
	//TDefs<DIM_2D>::GVect	vct2d;
	//mpGEnt->GetCoord( vct2d, vct);
	//TDefs<DIM_2D>::GMetric	met2d = mpCS->GetSpacing( vct2d);
	TDefs<DIM_2D>::GMetric	met2d = GetGlobSpacing( vct);

	TDefs<DIM_2D>::GVect tab[DIM_1D];

	mpGEnt->GetFstDeriv( tab, vct);

	GMetric	met;
	//met[0] = met2d.Distance( tab[0] );
	met[0] = met2d.Distance( tab[0].versor() );

	return met;
}


template <>
inline TDefs<DIM_1D>::GMetric SliceControlSpace<DIM_1D,DIM_3D>::GetSpacing( const GVect& vct) const
{
	TDefs<DIM_3D>::GMetric	met3d = GetGlobSpacing( vct);

	TDefs<DIM_3D>::GVect tab[DIM_1D];

	mpGEnt->GetFstDeriv( tab, vct);

	GMetric	met;
	met[0] = met3d.Distance( tab[0].versor() );

	return met;
}


template <>
inline TDefs<DIM_2D>::GMetric SliceControlSpace<DIM_2D,DIM_3D>::GetSpacing( const GVect& vct) const
{
	TDefs<DIM_3D>::GMetric	met3d = GetGlobSpacing( vct);

	TDefs<DIM_3D>::GVect vu, vv, tab[DIM_2D];

	mpGEnt->GetFstDeriv( tab, vct);

	if ( tab[0].module() < ZERO)
		tab[0].rX() = tab[0].rY() = tab[0].rZ() = 1.0e-6;

	if ( tab[1].module() < ZERO)
		tab[1].rX() = tab[1].rY() = tab[1].rZ() = 1.0e-6;


	vu = tab[0].versor();
	vv = tab[1].versor();
	

	SMatrix<3> smet, smdir, smdirt, sres;

	smet = met3d;
	smdir.Resize(3,2);
	smdir(0,0) = vu.cX();
	smdir(1,0) = vu.cY();
	smdir(2,0) = vu.cZ();
	smdir(0,1) = vv.cX();
	smdir(1,1) = vv.cY();
	smdir(2,1) = vv.cZ();

	smdirt = smdir;
	smdirt.Transp();

	sres = smdirt * smet * smdir;


	GMetric	met;
	met[0] = sres(0,0);
	met[1] = 0.5*(sres(0,1) + sres(1,0));
	met[2] = sres(1,1);

	//met.InitIso( 0.005);

	return met;
}



template <Dimension DIM, Dimension UNIDIM>
inline typename TDefs<UNIDIM>::GMetric SliceControlSpace<DIM,UNIDIM>::GetGlobSpacing( const GVect& vct) const
{
	UVect	uvct;
	mpGEnt->GetCoord( uvct, vct);
	return mpCS->GetSpacing( uvct);
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CSPACESLICE_H__
