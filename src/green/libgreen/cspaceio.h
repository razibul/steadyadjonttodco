#ifndef __CSPACEIO_H__
#define __CSPACEIO_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libgreen/cspacemaster.h"
#include "libcorecommon/regexp.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



#define CSP_VER_STRING "CSPACE_VER"
#define CSP_VER_NUMBER "0.01"


//////////////////////////////////////////////////////////////////////
//	class CSpaceIO
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class CSpaceIO
{
public:
	CSpaceIO( CSpaceMaster<DIM>& src);

	void	DoRead( const MGString& fname);
	void	DoWrite( const MGString& fname);

protected:
	static bool	CheckVersion_ascii( istream& file);

	static const MGString& VerNum()	{ return mstVersionNumber;}
	static const MGString& VerStr()	{ return mstVersionString;}

//	void	InitPointCreators();
//	void	InitCurveCreators();
//	void	InitSurfaceCreators();

	void	ReadHeader( istream& file);
	void	ReadGlobalSection( istream& file);
//	void	ReadGeometrySection( istream& file);
//	void	ReadTopologySection( istream& file);

	void	ReadDataSection( istream& file, const MGString& key, void (CSpaceIO<DIM>::*addFun)( const MGString&) );

	void	ReadSourceData( istream& file);
//	void	ReadTopologyData( istream& file);

	void	ParseDef( MGSize& id, MGString& str1, MGString& str2, const MGString& def);

	//void	AddVertexSrc( const MGString& def);
	//void	AddEdgeSrc( const MGString& def);
	//void	AddFaceSrc( const MGString& def);

	template <Dimension TDIM>
	void	AddSourceEnt( const MGString& def);

//
//
//	void	WriteHeader( ostream& file);
//	void	WriteGeometrySection( ostream& file);
//	void	WriteTopologySection( ostream& file);
//
//
private:
	static MGString	mstVersionString;
	static MGString	mstVersionNumber;

	CSpaceMaster<DIM>*	mpSrc;

	RegExp		mRex;

//	vector< BaseFactory< GeomEntity<DIM_0D,DIM> >* >	mtabPointCreator;
//	vector< BaseFactory< GeomEntity<DIM_1D,DIM> >* >	mtabCurveCreator;
//	vector< BaseFactory< GeomEntity<DIM_2D,DIM> >* >	mtabSurfaceCreator;

};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CSPACEIO_H__
