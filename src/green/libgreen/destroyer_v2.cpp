#include "destroyer_v2.h"
#include "libgreen/configconst.h"
#include "libgreen/spointdestructor.h"
#include "libgreen/spointpushdestructor.h"

#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/bndgrid.h"
#include "libgreen/gridgeom.h"
#include "libgreen/controlspace.h"
#include "libgreen/operator3d_collapseedge.h"
#include "libgreen/operator3d_remnode.h"
#include "libgreen/optimiser.h"

#include "libgreen/writegrdtec.h"
#include "libcorecommon/progressbar.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



MGSize EdgeDestrV2::RemoveSPnts()
{
	MGSize ncount = 0;

	if ( mtabInNode.size() == 0 )
		return 0;

	//random_shuffle( mtabInNode.begin(), mtabInNode.end() );

	vector<MGSize> tabin;
	tabin.reserve( mtabInNode.size() );

	static MGSize iii=0;
	for ( vector<MGSize>::iterator itr=mtabInNode.begin(); itr!=mtabInNode.end(); ++itr)
	{
		//cout << ++iii << " / " << mtabInNode.size() << endl;

		SPointDestructor spd( *itr);
		spd.mEdge = mEdge;
		spd.mpParent = mpParent;

		spd.Init( mtabSubEdge );
		
		bool bok = false;
		//if ( itr == mtabInNode.begin() )
		bok = spd.Delete();

		if ( bok)
		{
			++ncount;
			spd.UpdateSubEdges( mtabSubEdge);
			//mpKernel->cGrid().CheckConnectivity();
		}
		else
		{
			tabin.push_back( *itr);
		}
	}

	mtabInNode.swap( tabin);


	return ncount;
}


MGFloat EdgeDestrV2::FindDistance()
{
	MGFloat dist = -1.0;

	for ( vector<MGSize>::iterator itr=mtabInNode.begin(); itr!=mtabInNode.end(); ++itr)
	{
		vector<MGSize> tabball;
		MGSize icell = mpParent->mpKernel->cGrid().cNode(*itr).cCellId();
		mpParent->mpKernel->FindBall( tabball, *itr, icell );

		// find length
		for ( MGSize i=0; i<tabball.size(); ++i)
		{
			const GCell& cell = mpKernel->cGrid().cCell( tabball[i] );
			for ( MGSize k=0; k<GCell::ESIZE; ++k)
			{
				Key<2> ke = cell.EdgeKey( k);
				GVect ve1 = mpKernel->cGrid().cNode(ke.cFirst()).cPos();
				GVect ve2 = mpKernel->cGrid().cNode(ke.cSecond()).cPos();
				MGFloat d = (ve2 - ve1).module();

				if ( d < dist || dist < 0.0)
					dist = d;
			}
		}

	}

	return dist;
}


EdgeDestrV2::GVect EdgeDestrV2::FindNormalVect()
{
	GVect retvn(0,0,0);

	if ( mtabSubEdge.size() == 0 )
		THROW_INTERNAL( "EdgeDestrV2::FindNormalVect() :: CRASH !!!");

	Key<2>	sedge = mtabSubEdge[0];
	vector<MGSize> tabshell;
	MGSize icell1 = mpParent->mpKernel->cGrid().cNode( sedge.cFirst() ).cCellId();
	MGSize icell2 = mpParent->mpKernel->cGrid().cNode( sedge.cSecond() ).cCellId();
	mpParent->mpKernel->FindEdgeShell( tabshell, sedge, icell1, icell2 );

	vector<GVect>	tabvn;
	tabvn.reserve(2);

	//cout << tabshell.size() << endl;
	//sedge.Dump();

	for ( MGSize i=0; i<tabshell.size(); ++i)
	{
		const GCell& cell = mpKernel->cGrid().cCell( tabshell[i] );
		for ( MGSize k=0; k<GCell::ESIZE; ++k)
		{
			if ( cell.cCellId(k).first == 0 )
			{
				Face<DIM_3D> face;
				cell.GetFaceVNIn( face, k);

				SimplexFace<DIM_3D> gface = GridGeom<DIM_3D>::CreateSimplexFace( face, mpParent->mpKernel->cGrid() );

				bool bOk = false;
				for ( MGSize j=0; j<SimplexFace<DIM_3D>::ESIZE; ++j)
				{
					Key<2> edge( face.cNodeId( gface.cEdgeConn( j, 0)), face.cNodeId(gface.cEdgeConn( j, 1)) );
					edge.Sort();
					sedge.Sort();
					//edge.Dump();

					if ( edge == sedge)
					{
						bOk = true;
						break;
					}
				}

				if ( bOk)
					tabvn.push_back( gface.Vn() );
			}
		}
	}

	//cout << tabvn.size() << endl;

	if ( tabvn.size() != 2)
		cout << "tabvn.size = " << tabvn.size() << endl;
	//	THROW_INTERNAL( "EdgeDestrV2::FindNormalVect() :: CRASH !!!");

	//THROW_INTERNAL( "STOP");

	retvn = tabvn[0].versor();
	for ( MGSize i=1; i<tabvn.size(); ++i)
		retvn +=  tabvn[i].versor();

	return retvn.versor();
}


MGSize EdgeDestrV2::RemoveSPntsPP( const MGSize& niter)
{
	mtabPP.clear();

	MGSize ncount = 0;

	if ( mtabInNode.size() == 0 )
		return 0;

	MGFloat dist = FindDistance();
	GVect vn = FindNormalVect();

	//cout << dist << endl;
	//vn.Write();


	vector<MGSize> tabin;
	tabin.reserve( mtabInNode.size() );


	static MGSize iii=0;
	for ( vector<MGSize>::iterator itr=mtabInNode.begin(); itr!=mtabInNode.end(); ++itr)
	{
		SPointPushDestructor spd( *itr);
		spd.mEdge = mEdge;
		spd.mpParent = mpParent;

		spd.Init( mtabSubEdge, vn, dist );
		
		bool bok = spd.Delete( niter);

		if ( bok)
		{
			++ncount;
			spd.UpdateSubEdges( mtabSubEdge);

			Operator3D_RemoveNode rem( *mpKernel );
			rem.Init( spd.mIPNode);
			if ( rem.Error() == 0)
				rem.Execute();
			else
				mtabPP.push_back( spd.mIPNode );

			//mpKernel->cGrid().CheckConnectivity();
		}
		else
		{
			tabin.push_back( *itr);
		}
	}

	mtabInNode.swap( tabin);


	//THROW_INTERNAL( "STOP");

	return ncount;
}


void EdgeDestrV2::GetPPs( vector<MGSize>& tab)
{
	for ( MGSize i=0; i<mtabPP.size(); ++i )
		if ( mpKernel->cGrid().cColNode().is_valid( mtabPP[i] ) )
			tab.push_back( mtabPP[i] );
}

void EdgeDestrV2::GetSPs( vector<MGSize>& tab)
{
	for ( MGSize i=0; i<mtabInNode.size(); ++i )
		tab.push_back( mtabInNode[i] );
}


//////////////////////////////////////////////////////////////////////

void DestroyerV2<DIM_3D>::Init( const vector< KeyData< Key<2>, vector< Key<2> > > >& tabedge)
{
	ProgressBar	bar(40);

	mTotN = 0;

	mtabDEdge.reserve( tabedge.size() );

	bar.Init( tabedge.size() );
	bar.Start();
	for ( vector< KeyData< Key<2>, vector< Key<2> > > >::const_iterator itr=tabedge.begin(); itr!=tabedge.end(); ++itr, ++bar)
	{
		EdgeDestrV2 dedge( mpCSpace, mpKernel );

		dedge.mpParent = this;
		dedge.mEdge = itr->first;
		dedge.mtabSubEdge.resize( itr->second.size() );

		vector<MGSize> tabn;
		for ( MGSize i=0; i<itr->second.size(); ++i)
		{
			if ( itr->second[i].cFirst() != itr->first.cFirst() && itr->second[i].cFirst() != itr->first.cSecond() )
				tabn.push_back( itr->second[i].cFirst() );

			if ( itr->second[i].cSecond() != itr->first.cFirst() && itr->second[i].cSecond() != itr->first.cSecond() )
				tabn.push_back( itr->second[i].cSecond() );

			dedge.mtabSubEdge[i] = itr->second[i];
		}

		sort( tabn.begin(), tabn.end() );
		tabn.erase( unique( tabn.begin(), tabn.end() ), tabn.end() );

		dedge.mtabInNode.resize( tabn.size() );
		copy( tabn.begin(), tabn.end(), dedge.mtabInNode.begin() );

		mTotN += tabn.size();

		mtabDEdge.push_back( dedge);

		////
		mtabINParent.resize( mtabINParent.size() + tabn.size() );
		for ( MGSize i=0; i<tabn.size(); ++i)
			mtabINParent.push_back( KeyData< MGSize, Key<2> >( tabn[i], itr->first ) );

	}
	bar.Finish();

	sort( mtabINParent.begin(), mtabINParent.end() );

	cout << endl;
}


MGSize DestroyerV2<DIM_3D>::CountSPoints() const
{
	MGSize count=0;
	for ( vector<EdgeDestrV2>::const_iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr)
		count += itr->mtabInNode.size();

	return count;
}



void DestroyerV2<DIM_3D>::ExportExtraPnts( const MGString& name)
{
	ofstream of( name.c_str() );

	MGSize n = 0;
	for ( vector<EdgeDestrV2>::const_iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr)
		if ( itr->mtabInNode.size() > 0 )
			++n;

	of << n << endl;
	of << setprecision(17);
	for ( vector<EdgeDestrV2>::const_iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr)
		if ( itr->mtabInNode.size() > 0 )
		{
			GVect en1 = mpKernel->cGrid().cNode( itr->mEdge.cFirst() ).cPos();
			GVect en2 = mpKernel->cGrid().cNode( itr->mEdge.cSecond() ).cPos();

			of << itr->mEdge.cFirst()  << "   " << en1.cX() << " " << en1.cY() << " " << en1.cZ() << endl;
			of << itr->mEdge.cSecond() << "   " << en2.cX() << " " << en2.cY() << " " << en2.cZ() << endl;
			of << itr->mtabInNode.size() << endl;
			for ( MGSize i=0; i<itr->mtabInNode.size(); ++i)
			{
				GVect pnt = mpKernel->cGrid().cNode( itr->mtabInNode[i] ).cPos();
				of << itr->mtabInNode[i]  << "   " << pnt.cX() << " " << pnt.cY() << " " << pnt.cZ() << endl;
			}
			of << endl;
		}

}


void DestroyerV2<DIM_3D>::PurgeEC()
{
	ProgressBar	bar(40);

	mpKernel->rGrid().UpdateNodeCellIds();

	MGSize totcount = 0;
	MGSize count;


	// with no new points 
	do
	{

		random_shuffle( mtabDEdge.begin(), mtabDEdge.end() );

		bar.Init( mtabDEdge.size() );
		bar.Start();

		count = 0;
		for ( vector<EdgeDestrV2>::iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr, ++bar )
		{
			MGSize no = itr->RemoveSPnts();
			count += no;
		}
		bar.Finish();

		totcount += count;
	}
	while ( count > 0);

	//cout << "totcount = " << totcount << endl;
	cout << "removed = " << totcount << " / " << mTotN << " / " << CountSPoints() << endl;
}

MGSize DestroyerV2<DIM_3D>::PurgePP()
{
	// with adding push-points

	ProgressBar	bar(40);

	bar.Init( mtabDEdge.size() );
	bar.Start();

	MGSize count;

	try
	{
		count = 0;
		for ( vector<EdgeDestrV2>::iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr, ++bar )
		{
			MGSize no = itr->RemoveSPntsPP( this->ConfigSect().ValueSize( ConfigStr::Master::Generator::Destroyer::DSTR_PP_NITER) );
			itr->GetPPs( mtabPP);
			count += no;
		}
		bar.Finish();
	}
	catch (...)
	{
		cout << "CATCH :: removed = " << count << endl;
		throw;
	}


	sort( mtabPP.begin(), mtabPP.end() );
	mtabPP.erase( unique( mtabPP.begin(), mtabPP.end() ), mtabPP.end() );

	random_shuffle( mtabPP.begin(), mtabPP.end() );

	bar.Init( mtabPP.size() );
	bar.Start();

	vector<MGSize> tabnpp;
	for ( MGSize i=0; i<mtabPP.size(); ++i, ++bar )
	{
		Operator3D_RemoveNode rem( *mpKernel );
		rem.Init( mtabPP[i]);
		if ( rem.Error() == 0)
			rem.Execute();
		else
			tabnpp.push_back( mtabPP[i] );
	}
	bar.Finish();

	mtabPP.swap( tabnpp);

	cout << "mtabPP.size() = " << mtabPP.size() << endl;

	MGSize nleft = CountSPoints();
	cout << "removed = " << count << endl;
	cout << "left = " << nleft << endl;

	return nleft;
}


void DestroyerV2<DIM_3D>::Purge()
{
	cout << endl;
	cout << "Removing Steiner points (V2)" << endl;


	PurgeEC();
	PurgePP();

	//ProgressBar	bar(40);

	//mpKernel->rGrid().UpdateNodeCellIds();

	//MGSize totcount = 0;
	//MGSize count;


	//// with no new points 
	//do
	//{

	//	random_shuffle( mtabDEdge.begin(), mtabDEdge.end() );

	//	bar.Init( mtabDEdge.size() );
	//	bar.Start();

	//	count = 0;
	//	for ( vector<EdgeDestrV2>::iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr, ++bar )
	//	{
	//		MGSize no = itr->RemoveSPnts();
	//		count += no;
	//	}
	//	bar.Finish();

	//	totcount += count;
	//}
	//while ( count > 0);

	////cout << "totcount = " << totcount << endl;
	//cout << "removed = " << totcount << " / " << mTotN << " / " << CountSPoints() << endl;

	//// with adding push-points
	//bar.Init( mtabDEdge.size() );
	//bar.Start();

	//try
	//{
	//	count = 0;
	//	for ( vector<EdgeDestrV2>::iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr, ++bar )
	//	{
	//		MGSize no = itr->RemoveSPntsPP();
	//		count += no;
	//	}
	//	bar.Finish();
	//}
	//catch (...)
	//{
	//	cout << "CATCH :: removed = " << count << endl;
	//	throw;
	//}

	//cout << "removed = " << count << endl;
	//cout << "left = " << CountSPoints() << endl;

	//ExportExtraPnts( "extrapnts.txt");
}



void DestroyerV2<DIM_3D>::Optimise()
{
	vector<MGSize> tabP;

	for ( vector<EdgeDestrV2>::iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr )
	{
		itr->GetPPs( tabP);
		itr->GetSPs( tabP);
	}

	sort( tabP.begin(), tabP.end() );
	tabP.erase( unique( tabP.begin(), tabP.end() ), tabP.end() );

	random_shuffle( tabP.begin(), tabP.end() );

	Optimiser<DIM_3D> opt( *mpKernel);
	opt.OptimiseNodes( tabP, 1 );
}


void DestroyerV2<DIM_3D>::UpdateBoundaryGrid( BndGrid<DIM_3D>& bgrid)
{
	typedef KeyData< Key<3>, pair< Key<3>, MGSize> > TKeyData;

	cout << "updating boundary grid" << endl;

	map< MGSize, Key<2> > mapnedge;

	for ( vector<EdgeDestrV2>::const_iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr)
	{
		for ( MGSize i=0; i<itr->mtabInNode.size(); ++i )
			mapnedge.insert( map< MGSize, Key<2> >::value_type( itr->mtabInNode[i], itr->mEdge ) );
	}

	// find current bnd faces
	vector< Key<3> > tabcface;
	for ( Grid<DIM_3D>::ColCell::const_iterator citr=mpKernel->cGrid().cColCell().begin(); citr!=mpKernel->cGrid().cColCell().end(); ++citr )
	{
		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
			if ( citr->cCellId(ifc).first == 0 )
			{
				Key<3> key = citr->FaceKey( ifc);
				tabcface.push_back( key);
			}
	}

	// find current bnd nodes
	vector<MGSize> tabcnode;
	for ( vector< Key<3> >::iterator itr=tabcface.begin(); itr!=tabcface.end(); ++itr)
	{
		tabcnode.push_back( itr->cFirst() );
		tabcnode.push_back( itr->cSecond() );
		tabcnode.push_back( itr->cThird() );
	}

	sort( tabcnode.begin(), tabcnode.end() );
	tabcnode.erase( unique( tabcnode.begin(), tabcnode.end() ), tabcnode.end() );




	// store bnd faces
	vector<TKeyData> tabfacesurf;
	for ( BndGrid<DIM_3D>::ColBCell::const_iterator itr=bgrid.cColCell().begin(); itr!=bgrid.cColCell().end(); ++itr)
	{
		Key<3> key, keys;
		for ( MGSize i=0; i<DIM_3D; ++i)
			key.rElem( i) = bgrid.cNode( itr->cNodeId(i) ).cGrdId();
		keys = key;
		keys.Sort();

		tabfacesurf.push_back( TKeyData( keys, pair< Key<3>, MGSize>( key, itr->cTopoId()) ) );
	}

	sort( tabfacesurf.begin(), tabfacesurf.end() );


	// clear bgrid
	bgrid.rColNode().clear();
	bgrid.rColCell().clear();
	
	// nodes
	MGSize	idnode = 0;
	bgrid.rColNode().reserve( tabcnode.size() );
	for ( MGSize i=0; i<tabcnode.size(); ++i)
	{
		BndGrid<DIM_3D>::GBndNode	bnode;

		//bnode.rId() = ++idnode;
		bnode.rId() = i+1;
		bnode.rPos() = mpKernel->cGrid().cNode( tabcnode[i]).cPos();
		bnode.rGrdId() = mpKernel->cGrid().cNode( tabcnode[i]).cId();
		//bnode.rMasterId() = mpKernel->cGrid().cNode( tabnode[i]).cId();

		bgrid.rColNode().push_back( bnode);
	}

	// cells
	MGSize newfacecount = 0;
	MGSize idcell = 0;
	for ( vector< Key<3> >::iterator itr=tabcface.begin(); itr!=tabcface.end(); ++itr)
	{
		Key<3> ckey = *itr;
		ckey.Sort();
		vector<TKeyData>::iterator itrfs = lower_bound( tabfacesurf.begin(), tabfacesurf.end(), TKeyData( ckey ) );

		vector<TKeyData>::iterator itrfsnew;

		if ( itrfs->first != ckey )
		{
			vector<MGSize> tabf;
			for ( MGSize k=0; k<3; ++k)
			{
				map< MGSize, Key<2> >::iterator imap = mapnedge.find( ckey.cElem(k) );
				if ( imap != mapnedge.end() )
				{
					tabf.push_back( imap->second.cFirst() );
					tabf.push_back( imap->second.cSecond() );
				}
				else
				{
					tabf.push_back( ckey.cElem(k) );
				}
			}

			sort( tabf.begin(), tabf.end() );
			tabf.erase( unique( tabf.begin(), tabf.end() ), tabf.end() );

			if ( tabf.size() != 3 )
				THROW_INTERNAL( "DestroyerV2<DIM_3D>::UpdateBoundaryGrid :: tabf.size() != 3 :: " << tabf.size() );

			Key<3> nkey;
			for ( MGSize k=0; k<3; ++k)
				nkey.rElem(k) = tabf[k];

			nkey.Sort();

			itrfsnew = lower_bound( tabfacesurf.begin(), tabfacesurf.end(), TKeyData( nkey ) );
			if ( itrfsnew->first != nkey )
				THROW_INTERNAL( "DestroyerV2<DIM_3D>::UpdateBoundaryGrid :: surf id not found !!!");

			++newfacecount;
			//itr->Dump();
		}
		else
		{
			itrfsnew = itrfs;
			//Key<3> keyo = itrfs->second.first;
			//MGSize idsurf = itrfs->second.second;

			//GVect vn =	( mpKernel->cGrid().cNode(keyo.cSecond()).cPos() - mpKernel->cGrid().cNode(keyo.cFirst()).cPos() ) %
			//			( mpKernel->cGrid().cNode(keyo.cThird()).cPos() - mpKernel->cGrid().cNode(keyo.cFirst()).cPos() );

			//GVect vnsub =	( mpKernel->cGrid().cNode(itr->cSecond()).cPos() - mpKernel->cGrid().cNode(itr->cFirst()).cPos() ) %
			//				( mpKernel->cGrid().cNode(itr->cThird()).cPos() - mpKernel->cGrid().cNode(itr->cFirst()).cPos() );

			//Key<3> sub = *itr;
			//if ( vnsub * vn < 0.0 )
			//{
			//	MGSize t = sub.cSecond();
			//	sub.rSecond() = sub.cThird();
			//	sub.rThird() = t;
			//}



			//BndGrid<DIM_3D>::GBndCell	bcell;

			//bcell.rId() = ++idcell;		// global cell id
			//bcell.rTopoId() = idsurf;

			////for ( MGSize k=0; k<BndGrid<DIM_3D>::GBndCell::SIZE; ++k)
			////	bcell.rNodeId(k) = keyo.cElem(k);

			//for ( MGSize k=0; k<BndGrid<DIM_3D>::GBndCell::SIZE; ++k)
			//{
			//	vector<MGSize>::iterator itrn = lower_bound( tabcnode.begin(), tabcnode.end(), sub.cElem(k) );
			//	if ( *itrn != sub.cElem(k) )
			//		THROW_INTERNAL( "DestroyerV2<DIM_3D>::UpdateBoundaryGrid :: node id not found !!!");

			//	bcell.rNodeId(k) = MGSize( itrn - tabcnode.begin() ) + 1;
			//}

			//bgrid.rColCell().push_back( bcell );
		}

		Key<3> keyo = itrfsnew->second.first;
		MGSize idsurf = itrfsnew->second.second;
		Key<3> sub = *itr;

		GVect vn =	( mpKernel->cGrid().cNode(keyo.cSecond()).cPos() - mpKernel->cGrid().cNode(keyo.cFirst()).cPos() ) %
					( mpKernel->cGrid().cNode(keyo.cThird()).cPos() - mpKernel->cGrid().cNode(keyo.cFirst()).cPos() );

		GVect vnsub =	( mpKernel->cGrid().cNode(sub.cSecond()).cPos() - mpKernel->cGrid().cNode(sub.cFirst()).cPos() ) %
						( mpKernel->cGrid().cNode(sub.cThird()).cPos() - mpKernel->cGrid().cNode(sub.cFirst()).cPos() );

		if ( vnsub * vn < 0.0 )
		{
			MGSize t = sub.cSecond();
			sub.rSecond() = sub.cThird();
			sub.rThird() = t;
		}


		BndGrid<DIM_3D>::GBndCell	bcell;

		bcell.rId() = ++idcell;		// global cell id
		bcell.rTopoId() = idsurf;

		for ( MGSize k=0; k<BndGrid<DIM_3D>::GBndCell::SIZE; ++k)
		{
			vector<MGSize>::iterator itrn = lower_bound( tabcnode.begin(), tabcnode.end(), sub.cElem(k) );
			if ( *itrn != sub.cElem(k) )
				THROW_INTERNAL( "DestroyerV2<DIM_3D>::UpdateBoundaryGrid :: node id not found !!!");

			bcell.rNodeId(k) = MGSize( itrn - tabcnode.begin() ) + 1;
		}

		bgrid.rColCell().push_back( bcell );


	}

	cout << "number of new bfaces = "  << newfacecount << endl;

	cout << "- FINISHED" << endl;
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

