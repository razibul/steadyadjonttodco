#ifndef __DESTROYER_H__
#define __DESTROYER_H__


#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Dimension DIM>
class ControlSpace;

template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class BndGrid;

template <Dimension DIM> 
class Kernel;




//////////////////////////////////////////////////////////////////////
// class EdgeDestr
//////////////////////////////////////////////////////////////////////
class EdgeDestr
{
public:
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

public:
	EdgeDestr( const ControlSpace<DIM_3D>* pcspace, Kernel<DIM_3D>* pkernel ) : mpCSpace(pcspace), mpKernel(pkernel)	{}

	bool	RemoveNode();
	bool	PushNode();
	void	EdgeCollapse( const MGSize& inold, const MGSize& innew, vector<MGSize>& tabc, vector<MGSize>& tabball);
	void	BreakBall( const MGSize& inod, vector<MGSize>& tabball );

	bool	RemoveNode( const MGSize& id);
	void	InsertNode( const GVect& vnod, vector<MGSize>& tabball );

public:
	const ControlSpace<DIM_3D>	*mpCSpace;
	Kernel<DIM_3D>				*mpKernel;

	Key<2>	mEdge;

	vector<MGSize>		mtabInNode;
	vector< Key<2> >	mtabSubEdge;
};



//////////////////////////////////////////////////////////////////////
// class ReconstructorCDT
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Destroyer
{
};


//////////////////////////////////////////////////////////////////////
// class ReconstructorCDT
//////////////////////////////////////////////////////////////////////
template <>
class Destroyer<DIM_2D>
{
public:
	Destroyer( const BndGrid<DIM_2D>& bgrd, const ControlSpace<DIM_2D>& cspace, Kernel<DIM_2D>& kernel) 
		: mpBndGrd(&bgrd), mpCSpace(&cspace), mpKernel(&kernel)		{}
private:
	const BndGrid<DIM_2D>		*mpBndGrd;
	const ControlSpace<DIM_2D>	*mpCSpace;
	Kernel<DIM_2D>				*mpKernel;
};
//////////////////////////////////////////////////////////////////////




//////////////////////////////////////////////////////////////////////
// class ReconstructorCDT
//////////////////////////////////////////////////////////////////////
template <>
class Destroyer<DIM_3D>
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

	friend class FaceCDT;

public:
	Destroyer( const BndGrid<DIM_3D>& bgrd, const ControlSpace<DIM_3D>& cspace, Kernel<DIM_3D>& kernel) 
		: mpBndGrd(&bgrd), mpCSpace(&cspace), mpKernel(&kernel)		{}


	void	Init( const vector< KeyData< Key<2>, vector< Key<2> > > >& tabedge);

	void	StartPurge();

private:
	const BndGrid<DIM_3D>		*mpBndGrd;
	const ControlSpace<DIM_3D>	*mpCSpace;
	Kernel<DIM_3D>				*mpKernel;

	MGSize				mTotN;
	vector<EdgeDestr>	mtabDEdge;
};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __DESTROYER_H__
