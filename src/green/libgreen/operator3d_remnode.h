#ifndef __OPERATOR3D_REMNODE_H__
#define __OPERATOR3D_REMNODE_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class Kernel;


//////////////////////////////////////////////////////////////////////
// class Operator3D_RemoveNode
//////////////////////////////////////////////////////////////////////
class Operator3D_RemoveNode
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

public:
	Operator3D_RemoveNode( Kernel<DIM_3D>& kernel) : mKernel(kernel), mErrorCode(0)	{}


	void	Reset();

	void	Init( const MGSize& idnod);

	void	Execute();

	const MGSize&	Error() const			{ return mErrorCode;}

private:
	Kernel<DIM_3D>	&mKernel;

	MGSize			mIdNode;
	MGSize			mIdNext;

	vector<MGSize>	mtabBall;

	MGSize			mErrorCode;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __OPERATOR3D_REMNODE_H__
