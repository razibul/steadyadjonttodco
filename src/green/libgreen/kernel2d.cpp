#include "kernel.h"
#include "libgreen/localizator.h"
#include "libgreen/gridgeom.h"
#include "libgreen/grid.h"
#include "libgreen/generatordata.h"

#include "libcoreio/store.h"
#include "libgreen/writegrdtec.h"
#include "libgreen/edgepipe.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <class T>
inline bool lesser_pair ( const T& elem1, const T& elem2 )
{
   return  ( elem1.first < elem2.first );
}



bool Kernel<DIM_2D>::FindBall( vector<MGSize>& tabball, const MGSize& inod, const MGSize& icstart)
{
	// TODO :: common with 3D

	vector<GCell*>	tabstack;
	
	GVect	vct = mpGrd->cNode( inod).cPos();
	
	MGSize	icell;
	
	if ( icstart)
		icell = icstart;
	else
		icell = mLoc.Localize( vct);
		
		
	if ( ! icell)
	{
		printf( "failed to localize cell for nod: %d [%lg, %lg]\n", inod, vct.cX(), vct.cY() );
		return false;
	}
	
	GCell& cell = mpGrd->rCell( icell);


	bool bFound = false;	
	for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		if ( cell.cNodeId( ifc ) == inod)
		{
			bFound = true;
			break;
		}
	
	if ( ! bFound)
		THROW_INTERNAL( "FindBall - starting cell does not touch the inod");
	
	
	cell.Paint();
	
	tabstack.push_back( &cell);
	tabball.push_back( icell);
	
	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			if ( cell.cNodeId( ifc ) == inod)
				continue;
				
			MGSize ic = cell.cCellId( ifc).first;
			
			if ( ic )
			{
				GCell &cellnei = mpGrd->rCell( ic);
				
				if ( cellnei.IsPainted() )
					continue;
					
				for ( MGSize in = 0; in <GCell::SIZE; ++in)
					if ( cellnei.cNodeId( in) == inod )
					{
						cellnei.Paint();
						tabstack.push_back( &cellnei);
						tabball.push_back( ic);
						break;
					}
			}
		}
	};
	
	for ( vector<MGSize>::iterator itrp=tabball.begin(); itrp!=tabball.end(); ++itrp)
		mpGrd->rCell( *itrp).WashPaint();

	return true;
}


bool Kernel<DIM_2D>::FindFacePipe( vector<MGSize>& tabpipe, const Key<2>& face)
{
	// TODO :: common with 3D

	//TRACE( "inside FindEdgPipe");

	tabpipe.clear();

	// finding a ball for the first node of the edge
	vector<MGSize>	tabball;
	
	if ( ! FindBall( tabball, face.cFirst() ) )	
	{
		cout << "failed to find an face pipe [" << face.cFirst() << ", " << face.cSecond() << "]\n";
		TRACE( "failed to find an face pipe [" << face.cFirst() << ", " << face.cSecond() << "]\n"  );
		
		return false;
	}

	
	GVect	v1 = mpGrd->cNode( face.cFirst() ).cPos();
	GVect	v2 = mpGrd->cNode( face.cSecond() ).cPos();

	//cout << "tabball.size = "<< tabball.size() << "    " << std::flush;
	//face.Dump();

	// check if the face is existing
	for ( vector<MGSize>::iterator itr = tabball.begin(); itr != tabball.end(); ++itr)
	{
		GCell& cell = mpGrd->rCell( *itr);

		//cout << "id = " << cell.cId() << endl;
		//for ( MGSize i=0; i<=GCell::SIZE; ++i)
		for ( MGSize i=0; i<GCell::SIZE; ++i)
			if ( cell.cNodeId( i) == face.cSecond() )
			{
				//cout << "Kernel<DIM_2D>::FindFacePipe : face is existing" << endl;
				return false;
			}
	}
	
	
	vector<GCell*>	tabstack;
	
	// searching for pipe starting cell/cells
	for ( vector<MGSize>::iterator itr = tabball.begin(); itr != tabball.end(); ++itr)
	{
		GCell& cell = mpGrd->rCell( *itr);
		Simplex<DIM_2D> simp = GridGeom<DIM_2D>::CreateSimplex( cell, *mpGrd);
		
		MGSize in;
		for ( in=0; in<GCell::SIZE; ++in)
			if ( cell.cNodeId( in) == face.cFirst() )
				break;
				
		ASSERT( cell.cNodeId( in) == face.cFirst() );
		ASSERT( cell.cId() == *itr);
		
		SimplexFace<DIM_2D> face = simp.GetFace( in);
		
		if ( face.IsCrossed( v1, v2) )
		{
			cell.Paint();
			tabstack.push_back( &cell);
			tabpipe.push_back( *itr);
		}
	}
	
	if ( tabstack.size() == 0)
	{
		cout << "Kernel<DIM_2D>::FindFacePipe : no starting cell found" << endl;
		return false;
	}
		
	//ASSERT( tabstack.size() >= 1 && tabstack.size() <= 2);
	
//	cout << "tabstack.size = "<< tabstack.size() << "    " << std::flush;


	// searching for remaining cells
	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();
		Simplex<DIM_2D>	simp = GridGeom<DIM_2D>::CreateSimplex( cell, *mpGrd);

		tabstack.pop_back();

		bool bFound = false;
		for ( MGSize i = 0; i<GCell::SIZE; ++i)
			if ( cell.cNodeId( i) == face.cSecond() )
			{
				//cout << "found\n";
				bFound = true;
			}
		
		if ( bFound)
			continue;



		bFound = false;
		MGSize in;
		for ( in=0; in<GCell::SIZE; ++in)
			if ( cell.cNodeId( in) == face.cFirst() )
			{
				bFound = true;
				break;
			}

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			if ( bFound && ifc != in)
				continue;
				
			MGSize ic = cell.cCellId( ifc).first;
			
			if ( ic )
			{
				GCell &cellnei = mpGrd->rCell( ic);
				
				if ( cellnei.IsPainted() )
					continue;

				SimplexFace<DIM_2D> face = simp.GetFace( ifc);
				
				if ( face.IsCrossed( v1, v2) )
				{
					cellnei.Paint();
					tabstack.push_back( &cellnei);
					tabpipe.push_back( ic);
				}
					
			}
		}	
	}
	
	// cleaning cell flags
	for ( vector<MGSize>::iterator itrp=tabpipe.begin(); itrp!=tabpipe.end(); ++itrp)
		mpGrd->rCell( *itrp).WashPaint();

	//TRACE( "FindEdgPipe finished");
	
	return true;


//	cout << "pipe.size = "<< tabpipe.size() << endl;



// 	Store::File	f( "_edge.plt", "wt");
// 	fprintf( f, "VARIABLES = \"X\", \"Y\", \"Z\"\n" );
// 	fprintf( f, " %10.7lg %10.7lg %10.7lg\n", v1.cX(), v1.cY(), v1.cZ() );
// 	fprintf( f, " %10.7lg %10.7lg %10.7lg\n", v2.cX(), v2.cY(), v2.cZ() );
// 	f.Close();
// 
// 	WriteGrdTEC<DIM_3D>	write(mpGrd);
// 	write.WriteCells( "ball.plt", tabball);
// 	write.WriteCells( "pipe.plt", tabpipe);
// 	write.WriteExploded( "pipe_exp.plt", tabpipe, 2.0);


//	THROW_INTERNAL( "END");	
}


//template <> 
MGSize Kernel<DIM_2D>::FindCell( const GVect& vct)
{
	// power search - the worst algo...

	Grid<DIM_2D>::ColCell::const_iterator	itr;

	for ( itr = mpGrd->cColCell().begin(); itr != mpGrd->cColCell().end(); ++itr)
	{
		const GCell& cell = (*itr);
		if ( GridGeom<DIM_2D>::CreateSimplex( cell, *mpGrd).IsInside( vct) )
			return itr.index();
	}

	return 0;
}


//template <>
void Kernel<DIM_2D>::InitBoundingBox( const GVect& vmin, const GVect& vmax)
{
	const MGFloat	expcoeff = 1.4;
	//const MGFloat	expcoeff = 0.2;
	GVect	dv = vmax - vmin;

	mBox.rVMax() = vmax + expcoeff*dv;
	mBox.rVMin() = vmin - expcoeff*dv;

	mBox.Equalize();

	mLoc.InitBox( mBox);

// 	mBox.rVMin().rX() -= 0.001;
// 	mBox.rVMin().rY() -= 0.002;
// 	mBox.rVMin().rZ() -= 0.003;
// 
// 	mBox.rVMax().rX() += 0.011;
// 	mBox.rVMax().rY() += 0.002;
// 	mBox.rVMax().rZ() += 0.033;
}


//template<>
void Kernel<DIM_2D>::InitCube()
{
	mLoc.InitBox( mBox);


	mpGrd->InsertNode( GNode( GVect( mBox.cVMin().cX(), mBox.cVMin().cY() ) ) );
	mpGrd->InsertNode( GNode( GVect( mBox.cVMax().cX(), mBox.cVMin().cY() ) ) );
	mpGrd->InsertNode( GNode( GVect( mBox.cVMax().cX(), mBox.cVMax().cY() ) ) );
	mpGrd->InsertNode( GNode( GVect( mBox.cVMin().cX(), mBox.cVMax().cY() ) ) );

	//mpGrd->rNode(1).SetExternal( true);
	//mpGrd->rNode(2).SetExternal( true);
	//mpGrd->rNode(3).SetExternal( true);
	//mpGrd->rNode(4).SetExternal( true);

	GCell	cell;

	// cell 1
	cell.rId() = 1;
	cell.rNodeId( 0) = 1;
	cell.rNodeId( 1) = 2;
	cell.rNodeId( 2) = 4;

	cell.rCellId( 0) = GNeDef( 2, 0);

	mpGrd->InsertCell( cell);

	// cell 2
	cell.Reset();
	cell.rId() = 2;
	cell.rNodeId( 0) = 3;
	cell.rNodeId( 1) = 4;
	cell.rNodeId( 2) = 2;

	cell.rCellId( 0) = GNeDef( 1, 0);

	mpGrd->InsertCell( cell);


	for ( MGSize ic=1; ic<=2; ++ic)
	{
		GVect	vc = GridGeom<DIM_2D>::CreateSimplex( ic, *mpGrd ).Center();

		mpGrd->rCell(ic).SetTree( true);
		mLoc.TreeInsert( vc, ic);
	}

}


bool Kernel<DIM_2D>::FindCavityMetric( vector<MGSize>& tabcell, const MGSize& icell, const GVect& vct, const GMetric& met)
{
	vector<MGSize>::iterator itrcell;
	static vector<GCell*>	tabstack;

	tabstack.reserve(10);
	tabcell.reserve(10);

	tabstack.resize(0);
	tabcell.resize(0);


	tabcell.push_back( icell );
	tabstack.push_back( &mpGrd->rCell( icell));

	tabstack.back()->Paint();


	GMetric	tabmetric[GCell::SIZE];

	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;

			if ( ic)
			{
				GCell &cellnei = mpGrd->rCell( ic);
				
				const GMetric& metric = met;

				for ( MGSize im=0; im<GCell::SIZE; ++im)
					tabmetric[im] = mrGDat.cMetric( mpGrd->cNode( cellnei.cNodeId(im) ).cId() );

				Simplex<DIM_2D>	tri = GridGeom<DIM_2D>::CreateSimplex( cellnei, *mpGrd);

				// TODO :: add some tolerance not to reconnect the cospherical points
				//if ( (! cellnei.IsPainted()) && ( tet.IsInsideCircle( vct) > 1.e-14 ) )
				if ( (! cellnei.IsPainted()) && 
					 (! cellnei.IsExternal()) && 
					 ( tri.IsInsideSphereMetric( vct, metric, tabmetric) > 0.0 ) )
				{
					tabcell.push_back( cellnei.cId() );
					tabstack.push_back( &cellnei );
					cellnei.Paint();
				}
			}
		}
	}

	if ( ! tabcell.size() )
	{
		TRACE( "Kernel::FindCavity - failed: empty cavity"); 
		return false;
	}

	return true;
}



bool Kernel<DIM_2D>::FindCavity( vector<MGSize>& tabcell, const MGSize& icell, const GVect& vct)
{
	vector<MGSize>::iterator itrcell;
	static vector<GCell*>	tabstack;

	tabstack.reserve(10);
	tabcell.reserve(10);

	tabstack.resize(0);
	tabcell.resize(0);


	tabcell.push_back( icell );
	tabstack.push_back( &mpGrd->rCell( icell));

	tabstack.back()->Paint();


	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;

			if ( ic)
			{
				GCell &cellnei = mpGrd->rCell( ic);
				
				Simplex<DIM_2D>	tri = GridGeom<DIM_2D>::CreateSimplex( cellnei, *mpGrd);

				// TODO :: add some tolerance not to reconnect the cospherical points
				if ( (! cellnei.IsPainted()) && 
					 (! cellnei.IsExternal()) && 
					 ( tri.IsInsideSphere( vct) > 0.0 ) )// || tri.Volume() <= 1.0e-17 ) ) 
				{
					tabcell.push_back( cellnei.cId() );
					tabstack.push_back( &cellnei );
					cellnei.Paint();
				}
			}
		}
	}

	if ( ! tabcell.size() )
	{
		TRACE( "Kernel::FindCavity - failed: empty cavity"); 
		return false;
	}

	return true;
}



void Kernel<DIM_2D>::CavityCorrection( const vector<MGSize>& tabcell, const GVect& vct)
{
	// correction for star-shape of the cavity
	GFace	face;
	bool	bOk;
	bool	bExit;

	// set min cell volume
	vector<MGSize>::const_iterator itc = tabcell.begin();

	MGFloat volmin = GridGeom<DIM_2D>::CreateSimplex( *itc, *mpGrd ).Volume();
	
	for ( ++itc; itc != tabcell.end(); ++itc) 
		volmin = min( volmin, GridGeom<DIM_2D>::CreateSimplex( *itc, *mpGrd ).Volume() );

	volmin *= 1.0e-10;
	volmin = 0.;

	do
	{
		bExit = true;

		for ( MGSize ic=0; ic<tabcell.size(); ++ic)
		{
			GCell	&cell = mpGrd->rCell( tabcell[ic] );

			if ( cell.IsPainted() )
			{

				for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
				{
					const MGSize inei = cell.cCellId( ifc).first;

					bOk = false;

 					if ( ! inei)
 						bOk = true;
 					else if ( ! mpGrd->cCell( inei).IsPainted() )
 						bOk = true;


					if ( bOk )
					{
						Simplex<DIM_2D>		tri( mpGrd->cNode( cell.cFaceNodeId( ifc, 1) ).cPos(), 
												 mpGrd->cNode( cell.cFaceNodeId( ifc, 0) ).cPos(),
												 vct );

						// bad cell has negative volume
						if ( tri.Volume() <= volmin )// -1.0e-14 ) 
						{
							//cout << "star shape cavity correction !\n";
							cell.WashPaint();
							bExit = false;
						}
					}
				}
			}
		}
	}
	while ( ! bExit);

	// TODO :: check the cavity - hanging nodes, disconnected cells
}


bool Kernel<DIM_2D>::CavityBoundary( vector<GFace>& tabface, const vector<MGSize>& tabcell)
{
	bool	bOk;
	GFace	face;
	list<GFace> lstface;

	for ( MGSize ic=0; ic<tabcell.size(); ++ic)
	{
		const GCell	&cell = mpGrd->cCell( tabcell[ic] );

		if ( ! cell.IsPainted() )
			continue;
			

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize inei = cell.cCellId( ifc).first;


			bOk = false;

			if ( ! inei)
				bOk = true;
			else 
			if ( ! mpGrd->cCell( inei).IsPainted() )
				bOk = true;

			if ( bOk )
			{
				cell.GetFaceVNIn( face, ifc);
				lstface.push_back( face);
			}

		}
	}

	if ( lstface.size() == 0 )
	{
		TRACE( "Kernel<DIM_2D>::CavityBoundary : problem with cavity boundary!");
		return false;
	}

	/////////////////////////////////////////////////////////////
	// sorting faces stored in lstface

	list<GFace>::iterator	itrsg, itrsg2;
	itrsg = lstface.end();

	itrsg--;
	do
	{
		for ( itrsg2 = lstface.begin(); itrsg2 != lstface.end(); ++itrsg2)
		{
			if ( (*itrsg).cNodeId(0)/*left*/ == (*itrsg2).cNodeId(1)/*right*/ )
			{
				face = (*itrsg2);
				lstface.erase( itrsg2);
				itrsg = lstface.insert( itrsg, face );
				break;
			}
		}
	}
	while ( itrsg != lstface.begin() );


	for ( itrsg = lstface.begin(); itrsg != lstface.end(); ++itrsg)
	{
		tabface.push_back( *itrsg);
	}

	lstface.clear();

	

	//printf( "tabcell.size = %d\n", tabcell.size() );
	//printf( "tabface.size = %d\n", tabface.size() );


	//Store::File	f( "_cavity.dat", "wt");
	//for ( itrcell=tabcell.begin(); itrcell!=tabcell.end(); ++itrcell)
	//{
	//	const GCell &cell = mpGrd->cCell( *itrcell );
	//	cell.DumpTEC( *mpGrd, f);
	//}
	//f.Close();

	//f.Open( "_cavfaces.dat", "wt");
	//for ( MGSize i=0; i<tabface.size(); ++i)
	//	tabface[i].DumpTEC( *mpGrd, f);
	//f.Close();
	
	
	if ( ! tabcell.size() )
	{
		TRACE( "Kernel::FindCavity - failed: empty cavity"); 
		return false;
	}

	if ( ! tabface.size() )
	{
		THROW_INTERNAL( "no faces found for non-empty cavity...");
	}
	
	return true;
}



////template <> 
//bool Kernel<DIM_2D>::FindCavity( vector<MGSize>& tabcell, vector<GFace>& tabface, const MGSize& icell,
//								 const GVect& vct, const GMetric& met, const bool& bmetric)
//{
//	if ( bmetric)
//		FindCavityMetric( tabcell, icell, vct, met);
//	else
//		FindCavity( tabcell, icell, vct);
//
//	CavityCorrection( tabcell, vct);
//
//	return CavityBoundary( tabface, tabcell);
//}









//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////


//template <> 
MGSize Kernel<DIM_2D>::InsertPoint( vector<MGSize>& tabccell, vector<GFace>& tabcface, const GVect& vct)
{
	//TRACE2( "new point at %10.6lg %10.6lg %10.6lg", vct.cX(), vct.cY() );


	Vect2D	vc;

	for ( MGSize ic=0; ic<tabccell.size(); ++ic)
		if ( mpGrd->cCell(tabccell[ic]).IsTree() )
		{
			vc = GridGeom<DIM_2D>::CreateSimplex( tabccell[ic], *mpGrd).Center();
			mLoc.TreeErase( vc, tabccell[ic]);
		}

	/////////////////////////////////////////////////////////////
	// erasing cavity cells
	mpGrd->rColCell().erase( tabccell );



	// inod is an ID of the new point
	MGSize inod = mpGrd->InsertNode( GNode( vct) );
	
	mpGrd->rNode( inod).rId() = inod;


	/////////////////////////////////////////////////////////////
	// creating new triangles in Delaunay cavity
	static vector<MGSize>	tabnewcell;

	tabnewcell.resize( tabcface.size());

	for ( MGSize icf=0; icf<tabcface.size(); ++icf)
	{
		const GFace &face = tabcface[icf];
		GCell	cell;

		cell.rNodeId(0) = face.cNodeId(0);
		cell.rNodeId(1) = face.cNodeId(1);
		cell.rNodeId(2) = inod;
		cell.rCellId(2) = face.cCellLo();

		//++(mLoc.rCounter());
		mLoc.IncCounter();

		MGSize icell = mpGrd->InsertCell( cell);
		mpGrd->rCell(icell).rId() = icell;

		//tabcface[icf].rCellUp().first = icell;
		//tabcface[icf].rCellUp().second = 0;

		if ( mLoc.TimeToInsert() )
		{
			mpGrd->rCell(icell).SetTree( true);
			vc = GridGeom<DIM_2D>::CreateSimplex( icell, *mpGrd).Center();
			mLoc.TreeInsert( vc, icell);
		}

		MGSize ineib = cell.cCellId(2).first;

		if ( ineib > 0)
		{
			mpGrd->rCell( ineib).rCellId( cell.cCellId(2).second ).first = icell;
			mpGrd->rCell( ineib).rCellId( cell.cCellId(2).second ).second = 2;
		}


		// flag bModified is used for Weatherill triangulation algo
		mpGrd->rCell(icell).SetModified( true);

		tabnewcell[icf] = icell;
	}

	/////////////////////////////////////////////////////////////
	// using existing connectivity info from faces to connect cavity cells

	for ( MGSize icf=1; icf<tabcface.size(); ++icf)
	{
		GCell	&cellLf = mpGrd->rCell( tabnewcell[icf-1] );
		GCell	&cellRt = mpGrd->rCell( tabnewcell[icf] );

		cellLf.rCellId( 0 ).first = cellRt.cId();
		cellLf.rCellId( 0 ).second = 1;
		cellRt.rCellId( 1 ).first = cellLf.cId();
		cellRt.rCellId( 1 ).second = 0;
	}

	GCell	&cellLf = mpGrd->rCell( tabnewcell[tabcface.size()-1] );
	GCell	&cellRt = mpGrd->rCell( tabnewcell[0] );

	cellLf.rCellId( 0 ).first = cellRt.cId();
	cellLf.rCellId( 0 ).second = 1;
	cellRt.rCellId( 1 ).first = cellLf.cId();
	cellRt.rCellId( 1 ).second = 0;


	//Store::File	f( "_cfac.dump", "wt");
	//for ( MGSize icf=0; icf<tabcface.size(); ++icf)
	//{
	//	fprintf( f, "\nFACE ID = %d\n", icf+1);
	//	tabcface[icf].Dump( *mpGrd, f);
	//}
	//f.Close();
	

	//f.Open( "_ccell.dump", "wt");
	//for ( MGSize i=mpGrd->CellBegin(); i<mpGrd->CellEnd(); mpGrd->CellInc( i) )
	//{
	//	mpGrd->cCell(i).Dump( *mpGrd, f);
	//}
	//f.Close();




	// check and/or dump
	//mpGrd->CheckConnectivity();
	//mpGrd->Dump( "_grd.dump");


	//WriteTEC<DIM_3D> wtec(mpGrd);
	//wtec.DoWrite( "_test.dat");


	/////////////////////////////////////////////////////////////
	// tabcell should store ids of newly created cells
	tabccell.swap( tabnewcell);

	return inod;

}





//template <> 
MGSize Kernel<DIM_2D>::InsertPoint( const GVect& vct)
{
	typedef pair< Key<2>, FaceNeighbours >	TInFacPair;

	/////////////////////////////////////////////////////////////
	// find cell containing vct
	MGSize	icell = mLoc.Localize( vct);

	//if ( ! mpGrd->cCell(icell).IsInside( vct, *mpGrd) )
	//	THROW_INTERNAL( "Localization failed");
 
	//MGSize	icell = FindCell( vct);
	//TRACE1( "icell = %d\n", icell);


	/////////////////////////////////////////////////////////////
	// search for the delaunay cavity of vct
	static vector<MGSize>	tabccell;
	static vector<GFace>	tabcface;

	tabcface.reserve(20);
	tabccell.reserve(20);


	tabcface.resize(0);
	tabccell.resize(0);

	/////////////////////////////////////////////////////////////
	// tabccel  - cells creating a cavity for the vct
	// tabcface - faces bounding the cavity

	FindCavity( tabccell, icell, vct);
	CavityCorrection( tabccell, vct);

	if ( ! CavityBoundary( tabcface, tabccell ) )
	{
		// return NULL if cavity is _not_ found 
		return 0;
	}	


	/////////////////////////////////////////////////////////////
	// insert new point into mesh
	return InsertPoint( tabccell, tabcface, vct);
}



MGSize Kernel<DIM_2D>::InsertPointMetric( const GVect& vct, const GMetric& met)
{
	typedef pair< Key<2>, FaceNeighbours >	TInFacPair;

	/////////////////////////////////////////////////////////////
	// find cell containing vct

	MGSize	icell = mLoc.Localize( vct);

	if ( mpGrd->cCell(icell).IsExternal() )
		return 0;

	//if ( ! mpGrd->cCell(icell).IsInside( vct, *mpGrd) )
	//	THROW_INTERNAL( "Localization failed");
 
	//MGSize	icell = FindCell( vct);
	//TRACE1( "icell = %d\n", icell);


	/////////////////////////////////////////////////////////////
	// search for the delaunay cavity of vct

	static vector<MGSize>	tabccell;
	static vector<GFace>	tabcface;

	tabcface.reserve(20);
	tabccell.reserve(20);


	tabcface.resize(0);
	tabccell.resize(0);

	/////////////////////////////////////////////////////////////
	// tabccel  - cells creating a cavity for the vct
	// tabcface - faces bounding the cavity

	FindCavityMetric( tabccell, icell, vct, met);
	CavityCorrection( tabccell, vct);

	if ( ! CavityBoundary( tabcface, tabccell ) )
	{
		// return NULL if cavity is _not_ found 
		return 0;
	}	


	/////////////////////////////////////////////////////////////
	// insert new point into mesh
	return InsertPoint( tabccell, tabcface, vct);
}





//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

