#include "writegrdmsh2.h"
#include "libgreen/gridwbnd.h"
#include "libcoreio/writemsh2.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Dimension DIM>
MGSize WriteGrdMSH2<DIM>::IOSizeNodeTab() const										
{ 
	return mpGrd->SizeNodeTab(); 
}

template <Dimension DIM>
MGSize WriteGrdMSH2<DIM>::IOSizeCellTab( const MGSize& type) const					
{ 
	return (type != DIM+1) ? 0 : mpGrd->SizeCellTab();
}
template <Dimension DIM>
MGSize WriteGrdMSH2<DIM>::IOSizeBFaceTab( const MGSize& type) const					
{ 
	return (type != DIM) ? 0 : mpGrd->cBndGrid().SizeCellTab();
}

template <Dimension DIM>
void WriteGrdMSH2<DIM>::IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mpGrd->cNode( mTabIdNode_G2L[id] ).cPos().cX(i);
}

template <Dimension DIM>
void WriteGrdMSH2<DIM>::IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( tabid.size() == type);

	if ( type == DIM+1)
	{
		for ( MGSize k=0; k<DIM+1; ++k)
			tabid[k] = mTabIdNode_L2G[ mpGrd->cCell( mTabIdCell[id] ).cNodeId(k) ] - 1;

		return;
	}

	THROW_INTERNAL( "from GridWBnd<DIM>::IOGetCellIds - bad cell type" );
}

template <Dimension DIM>
void WriteGrdMSH2<DIM>::IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( tabid.size() == type);

	if ( type == DIM)
	{
		for ( MGSize k=0; k<DIM; ++k)
			tabid[k] = mTabIdNode_L2G[ mpGrd->cBndGrid().cNode( mpGrd->cBndGrid().cCell(id+1).cNodeId(k) ).cGrdId() ] - 1;

		return;
	}

	THROW_INTERNAL( "from GridWBnd<DIM>::IOGetBFaceIds - bad cell type" );
}

template <Dimension DIM>
void WriteGrdMSH2<DIM>::IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id) const
{
	if ( type == DIM)
	{
		idsurf = mpGrd->cBndGrid().cCell(id+1).cTopoId();
		return;
	}

	THROW_INTERNAL( "from GridWBnd<DIM>::IOGetBFaceSurf - bad cell type" );
}

template <Dimension DIM>
void WriteGrdMSH2<DIM>::DoWrite( const MGString& fname)
{
	mTabIdNode_L2G.resize( mpGrd->cColNode().size(), 0 );
	mTabIdNode_G2L.resize( mpGrd->cColNode().size(), 0 );

	MGSize wid = 1;
	for ( typename Grid<DIM>::ColNode::const_iterator itrn = mpGrd->cColNode().begin(); itrn != mpGrd->cColNode().end(); ++itrn)
	{
		mTabIdNode_G2L[ wid-1 ] = (*itrn).cId();
		mTabIdNode_L2G[ (*itrn).cId() ] = wid++;
	}

	mTabIdCell.resize( mpGrd->SizeCellTab() );

	wid = 0;
	for ( typename Grid<DIM>::ColCell::const_iterator itrc = mpGrd->cColCell().begin(); itrc != mpGrd->cColCell().end(); ++itrc)
	{
		mTabIdCell[wid] = itrc->cId();
		wid++;
	}

	IO::WriteMSH2	writer( *this);
	writer.DoWrite( fname, false);

	mTabIdCell.clear();
	mTabIdNode_L2G.clear();
	mTabIdNode_G2L.clear();
}


/*
template <Dimension DIM> 
void GridWBnd<DIM>::IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = cNode(id+1).cPos().cX(i);
}

template <Dimension DIM> 
void GridWBnd<DIM>::IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( tabid.size() == type);

	if ( type == DIM+1)
	{
		for ( MGSize k=0; k<DIM+1; ++k)
			tabid[k] = cCell(id+1).cNodeId(k) - 1;

		return;
	}

	THROW_INTERNAL( "from GridWBnd<DIM>::IOGetCellIds - bad cell type" );
}

template <Dimension DIM> 
void GridWBnd<DIM>::IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( tabid.size() == type);

	if ( type == DIM)
	{
		for ( MGSize k=0; k<DIM; ++k)
			tabid[k] = mBndGrid.cNode( mBndGrid.cCell(id+1).cNodeId(k) ).cGrdId() - 1;

		return;
	}

	THROW_INTERNAL( "from GridWBnd<DIM>::IOGetBFaceIds - bad cell type" );
}

template <> 
inline void GridWBnd<DIM_1D>::IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	THROW_INTERNAL( "should never be called" );
}


template <Dimension DIM> 
void GridWBnd<DIM>::IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id)const
{
	if ( type == DIM)
	{
		idsurf = mBndGrid.cCell(id+1).cTopoId();
		return;
	}

	THROW_INTERNAL( "from GridWBnd<DIM>::IOGetBFaceSurf - bad cell type" );
}

template <> 
inline void GridWBnd<DIM_1D>::IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id)const
{
	THROW_INTERNAL( "should never be called" );
}


template <Dimension DIM> 
void GridWBnd<DIM>::IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id)
{
	THROW_INTERNAL( "not implemented" );
}

template <Dimension DIM> 
void GridWBnd<DIM>::IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id)
{
	THROW_INTERNAL( "not implemented" );
}

template <Dimension DIM> 
void GridWBnd<DIM>::IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id)
{
	THROW_INTERNAL( "not implemented" );
}

template <Dimension DIM> 
void GridWBnd<DIM>::IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id)
{
	THROW_INTERNAL( "not implemented" );
}


template <Dimension DIM> 
void GridWBnd<DIM>::IOGetBNodePos( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mBndGrid.cNode(id+1).cPos().cX(i);
}

template <Dimension DIM> 
void GridWBnd<DIM>::IOGetBNodeVn( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = 0;//mBndGrid.cNode(id).cVn().cX(i);
}

template <Dimension DIM> 
void GridWBnd<DIM>::IOGetBFaceBIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	THROW_INTERNAL( "not implemented" );
}

*/


template class WriteGrdMSH2<DIM_2D>;
template class WriteGrdMSH2<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

