#include "operator3d_restoreedge.h"

#include "libgreen/grid.h"
#include "libgreen/controlspace.h"
#include "libgreen/kernel.h"
#include "libgreen/gridgeom.h"

#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



void Operator3D_RestoreEdge::Init( const Key<2>& edgeL, const Key<2>& edgeR)
{
	// setting edges and nodes
	if ( edgeL.cFirst() == edgeR.cFirst() )
	{
		mIdNode = edgeL.cFirst();
		mIdNodeL = edgeL.cSecond();
		mIdNodeR = edgeR.cSecond();
	}
	else
	if ( edgeL.cFirst() == edgeR.cSecond() )
	{
		mIdNode = edgeL.cFirst();
		mIdNodeL = edgeL.cSecond();
		mIdNodeR = edgeR.cFirst();
	}
	else
	if ( edgeL.cSecond() == edgeR.cFirst() )
	{
		mIdNode = edgeL.cSecond();
		mIdNodeL = edgeL.cFirst();
		mIdNodeR = edgeR.cSecond();
	}
	else
	if ( edgeL.cSecond() == edgeR.cSecond() )
	{
		mIdNode = edgeL.cSecond();
		mIdNodeL = edgeL.cFirst();
		mIdNodeR = edgeR.cFirst();
	}
	else
	{
		mErrorCode = 1;
	}

	mEdgeL = Key<2>( mIdNodeL, mIdNode );
	mEdgeR = Key<2>( mIdNode, mIdNodeR );

	// find ball
	vector<MGSize> tabball;

	mKernel.FindBall( tabball, mIdNode);

	for ( MGSize i=0; i<tabball.size(); ++i)
	{
		const GCell& cell = mKernel.cGrid().cCell( tabball[i] );

		if ( cell.IsExternal() )
			mtabExCell.push_back( tabball[i] );
		else
			mtabInCell.push_back( tabball[i] );
	}

	vector< Key<2> > tabedge;
	tabedge.push_back( mEdgeL);
	tabedge.push_back( mEdgeR);


	MGFloat wgh = 0;
	GVect vc(0,0,0);
	for ( MGSize i=0; i<mtabInCell.size(); ++i)
	{
		//const GCell& cell = mKernel.cGrid().cCell( mtabInCell[i] );
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( mtabInCell[i], mKernel.cGrid() );
		MGFloat vol = tet.Volume();
		vc += tet.Center() * vol;
		wgh += vol;
	}

	vc /= wgh;





	WriteGrdTEC<DIM_3D> writeTEC( &( mKernel.cGrid()) );

	writeTEC.WriteCells( "__incells.dat", mtabInCell, "internal cells");
	writeTEC.WriteCells( "__excells.dat", mtabExCell, "external cells");
	writeTEC.WriteEdges( "__edge.dat", tabedge, "edge" );

	//ofstream f( "__center.dat");
	//f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	//f << "ZONE T=\"center\"\n";
	//f << vc.cX() << " " << vc.cY() << " " << vc.cZ() << " " << 0 << endl;



}



void Operator3D_RestoreEdge::ExecuteInternal()
{
	MGFloat wgh = 0;
	GVect vc(0,0,0);
	for ( MGSize i=0; i<mtabInCell.size(); ++i)
	{
		//const GCell& cell = mKernel.cGrid().cCell( mtabInCell[i] );
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( mtabInCell[i], mKernel.cGrid() );
		MGFloat vol = tet.Volume();
		vc += tet.Center() * vol;
		wgh += vol;
	}

	vc /= wgh;

	/////////////////////////////////////////////////////////////
	// INSERT POINT
	for ( MGSize i=0; i<mtabInCell.size(); ++i)
	{
		GCell& cell = mKernel.rGrid().rCell( mtabInCell[i] );
		cell.Paint();
	}

	vector<GFace>	tabcface;

	//FindCavity( tabccell, icell, vct);
	mKernel.CavityCorrection( mtabInCell, vc);

	MGSize inod;
	if ( ! mKernel.CavityBoundary( tabcface, mtabInCell ) )
		inod = 0;
	else
		inod = mKernel.InsertPoint( mtabInCell, tabcface, vc);


	if ( ! inod)
	{
		cout <<  "Operator3D_RestoreEdge INTERNAL :: kernel failed to insert new point with coordinates [" 
			<< vc.cX() << " " << vc.cY() << " " << vc.cZ() << "]" << endl;

		return;

		//THROW_INTERNAL( "CRASH Operator3D_RestoreEdge :: kernel failed to insert new point with coordinates [" 
		//	<< vc.cX() << " " << vc.cY() << " " << vc.cZ() << "]" );
	}

	mKernel.InsertMetric( mCSpace.GetSpacing( vc), inod );


	WriteGrdTEC<DIM_3D> writeTEC( &( mKernel.cGrid()) );
	vector<MGSize> tabball;
	mKernel.FindBall( tabball, mIdNode);
	writeTEC.WriteCells( "__afterball.dat", tabball, "after ball");
}


void Operator3D_RestoreEdge::ExecuteExternal()
{
	// find edges
	vector< Key<2> > tabEdge;
	vector< Key<2> > tabBndEdge;

	for ( MGSize i=0; i<mtabExCell.size(); ++i)
	{
		const GCell& cell = mKernel.rGrid().cCell( mtabExCell[i] );

		for ( MGSize d=0; d<GCell::SIZE; ++d)
		{
			MGSize icn = cell.cCellId( d).first;
			const GCell& celln = mKernel.rGrid().cCell( cell.cCellId( d).first );

			Key<3> fkey = cell.FaceKey( d);

			Key<2> e1( fkey.cFirst(),  fkey.cSecond() );	e1.Sort();
			Key<2> e2( fkey.cSecond(), fkey.cThird() );		e2.Sort();
			Key<2> e3( fkey.cThird(),  fkey.cFirst() );		e3.Sort();

			if ( e1.cFirst() == mIdNode || e1.cSecond() == mIdNode)
				tabEdge.push_back( e1);

			if ( e2.cFirst() == mIdNode || e2.cSecond() == mIdNode)
				tabEdge.push_back( e2);

			if ( e3.cFirst() == mIdNode || e3.cSecond() == mIdNode)
				tabEdge.push_back( e3);

			if ( ! celln.IsExternal() )
			{
				tabBndEdge.push_back( e1);
				tabBndEdge.push_back( e2);
				tabBndEdge.push_back( e3);
			}
		}
	}

	vector< Key<2> > tabInEdge;

	sort( tabEdge.begin(), tabEdge.end() );
	tabEdge.erase( unique( tabEdge.begin(), tabEdge.end() ), tabEdge.end() );

	sort( tabBndEdge.begin(), tabBndEdge.end() );
	tabBndEdge.erase( unique( tabBndEdge.begin(), tabBndEdge.end() ), tabBndEdge.end() );


	tabInEdge.resize( tabEdge.size() );
	tabInEdge.erase( set_difference( tabEdge.begin(), tabEdge.end(), tabBndEdge.begin(), tabBndEdge.end(), tabInEdge.begin() ), tabInEdge.end() );

	WriteGrdTEC<DIM_3D> writeTEC( &( mKernel.cGrid()) );
	writeTEC.WriteEdges( "__badedges.dat", tabInEdge, "badEdges" );


	// find new node position
	GVect vL = mKernel.rGrid().cNode( mIdNodeL).cPos();
	GVect vR = mKernel.rGrid().cNode( mIdNodeR).cPos();

	GVect vNod = mKernel.rGrid().cNode( mIdNode).cPos();

	MGFloat dist = 0.25 * (vR - vL).module();

	vector<GVect> tabvct;
	tabvct.resize( tabInEdge.size() );

	GVect vC(0.,0.,0.);
	for ( MGSize i=0; i<tabInEdge.size(); ++i)
	{
		MGSize ifar;
		if ( tabInEdge[i].cFirst() == mIdNode )
			ifar = tabInEdge[i].cSecond();
		else
		if ( tabInEdge[i].cSecond() == mIdNode )
			ifar = tabInEdge[i].cFirst();
		else
			THROW_INTERNAL( "corrupted tabInEdge" );

		GVect vFar = mKernel.rGrid().cNode( ifar).cPos();

		GVect ve = vFar - vNod;

		if ( ve.module() < dist )
			dist = 0.5 * ve.module();

		tabvct[i] = (vFar - vNod).versor() * dist + vNod;
		vC += tabvct[i];
	}

	vC /= (MGFloat)tabvct.size();

	GVect vct;
	MGFloat distmin;
	for ( MGSize i=0; i<tabvct.size(); ++i)
	{
		MGFloat d = (tabvct[i] - vC).module();

		if ( i==0 || d < distmin)
		{
			distmin = d;
			vct = tabvct[i];
		}
	}



	//////////////////////////////////////////////
	GVect vc(0,0,0);
	//MGFloat wgh = 0;
	//for ( MGSize i=0; i<mtabExCell.size(); ++i)
	//{
	//	//const GCell& cell = mKernel.cGrid().cCell( mtabExCell[i] );
	//	Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( mtabExCell[i], mKernel.cGrid() );
	//	MGFloat vol = tet.Volume();
	//	vc += tet.Center() * vol;
	//	wgh += vol;

	//	//break;
	//}
	//vc /= wgh;

	vc = vct;


	ofstream f( "__center.dat");
	f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	f << "ZONE T=\"center\"\n";
	f << vc.cX() << " " << vc.cY() << " " << vc.cZ() << " " << 0 << endl;

	/////////////////////////////////////////////////////////////
	// INSERT POINT
	for ( MGSize i=0; i<mtabExCell.size(); ++i)
	{
		GCell& cell = mKernel.rGrid().rCell( mtabExCell[i] );
		cell.Paint();
	}

	vector<GFace>	tabcface;

	//FindCavity( tabccell, icell, vct);
	mKernel.CavityCorrection( mtabExCell, vc);

	MGSize inod;
	if ( ! mKernel.CavityBoundary( tabcface, mtabExCell ) )
		inod = 0;
	else
		inod = mKernel.InsertPoint( mtabExCell, tabcface, vc);


	if ( ! inod)
	{
		cout <<  "Operator3D_RestoreEdge EXTERNAL :: kernel failed to insert new point with coordinates [" 
			<< vc.cX() << " " << vc.cY() << " " << vc.cZ() << "]" << endl;

		return;
		//THROW_INTERNAL( "CRASH Operator3D_RestoreEdge :: kernel failed to insert new point with coordinates [" 
		//	<< vc.cX() << " " << vc.cY() << " " << vc.cZ() << "]" );
	}

	mKernel.InsertMetric( mCSpace.GetSpacing( vc), inod );


	vector<MGSize> tabball;
	mKernel.FindBall( tabball, inod);

	for ( MGSize i=0; i<tabball.size(); ++i)
	{
		GCell& cell = mKernel.rGrid().rCell( tabball[i] );
		cell.SetExternal( true);
	}


	tabball.clear();
	mKernel.FindBall( tabball, mIdNode);
	writeTEC.WriteCells( "__afterball.dat", tabball, "after ball");

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

