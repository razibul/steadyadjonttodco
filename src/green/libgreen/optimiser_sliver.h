#ifndef __OPTIMISER_SLIVER_H__
#define __OPTIMISER_SLIVER_H__


#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libcoreconfig/configbase.h"
#include "libextget/geomentity.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class BndGrid;

template <Dimension DIM> 
class Kernel;





//////////////////////////////////////////////////////////////////////
// class OptimiserSliver
//////////////////////////////////////////////////////////////////////
class OptimiserSliver : public ConfigBase
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

public:
	OptimiserSliver(  Kernel<DIM_3D>& kernel) : mpKernel(&kernel)		{}

	void	DoOptimise();

private:
	Kernel<DIM_3D>	*mpKernel;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



#endif // __OPTIMISER_SLIVER_H__
