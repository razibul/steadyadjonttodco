#include "generator.h"
#include "libgreen/configconst.h"
#include "libcoregeom/tree.h"
#include "libcoreio/store.h"
#include "libgreen/writegrdtec.h"
#include "libgreen/writebndtec.h"
#include "libgreen/grid.h"
#include "libgreen/bndgrid.h"
#include "libgreen/gridgeom.h"
#include "libcorecommon/progressbar.h"
#include "libcorecommon/triple.h"


#include "libgreen/kernel.h"

#include "libgreen/reconstructor_cdt.h"
#include "libgreen/reconstructor.h"

#include "libgreen/optimiser.h"
#include "libgreen/optimiser_sliver.h"

#include "libgreen/controlspace.h"
#include "libgreen/cspaceslice.h"

#include "libextget/getcurve.h"
#include "libgreen/cspacegeom.h"

#include "libgreen/metricinterpol.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 





/*
template<>
void Generator<DIM_3D>::FlagExternalCells( vector< Key<3> >& tabbndfac)
{

	sort( tabbndfac.begin(), tabbndfac.end() );


	// remove duplicate faces from the boundary grid
	vector< Key<3> >::iterator itre = unique( tabbndfac.begin(), tabbndfac.end() );
	tabbndfac.erase( itre, tabbndfac.end() );


//FILE *ff = fopen( "_dump.txt", "wt");
//for ( vector< Key<2> >::iterator itrr=tabbndfac.begin(); itrr!=tabbndfac.end(); ++itrr)
//{
//	Key<2> key = *itrr;
//	key.Sort();
//	Vect2D vb1 = mpGrd->cNode( key.cFirst() ).cPos();
//	Vect2D vb2 = mpGrd->cNode( key.cSecond() ).cPos();
//	fprintf( ff, "key %8d %8d : %8.0lf %8.0lf : %8.0lf %8.0lf\n", key.cFirst(), key.cSecond(),
//		vb1.cX(), vb1.cY(), vb2.cX(), vb2.cY());
//}
//fclose(ff);


	// check if any cell is painted - should not be neccessary
	Grid<DIM_3D>::ColCell::iterator	itr;
	for ( itr=mpGrd->rColCell().begin(); itr!=mpGrd->rColCell().end(); ++itr)
	{
		GCell& cell = *itr;
		if ( cell.IsPainted() )
			cout << "cell with id = " << cell.cId() << " is painted\n";
	}

	// find a starting cell 
	MGSize icell = 0;
	for ( MGSize i=0; i<mpGrd->SizeCellTab(); ++i)
	{
		GCell& cell = mpGrd->rCell( i);
		for ( MGSize in=0; in<GCell::SIZE; ++in)
			if ( cell.cNodeId( in) >= 1 && cell.cNodeId( in) <= 8 ) // bounding box nodes // <- !!!
			{
				cell.SetExternal( true);
				cell.Paint();
				icell = i;
				break;
			}

		if ( icell)
			break;
	}

	// start painting algo
	static vector<GCell*>	tabstack;

	tabstack.push_back( &mpGrd->rCell( icell) );

	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();
		bool bcolor = cell.IsExternal();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;

			if ( ic)
			{
				GCell &cellnei = mpGrd->rCell( ic);

				if ( ! cellnei.IsPainted() )
				{
					tabstack.push_back( &cellnei );
					cellnei.Paint();

					Key<3> key = cell.FaceKey( ifc);
					key.Sort();

					if ( binary_search( tabbndfac.begin(), tabbndfac.end(), key ) )
						cellnei.SetExternal( ! bcolor );
					else
						cellnei.SetExternal( bcolor);
				}
			}
		}
	}

	// wash paint and make statistics
	MGSize	nextern=0, npainted=0;
	for ( itr=mpGrd->rColCell().begin(); itr!=mpGrd->rColCell().end(); ++itr)
	{
		GCell& cell = *itr;

		if ( cell.IsExternal() )
			++nextern;

		if ( cell.IsPainted() )
		{
			++npainted;
			cell.WashPaint();
		}
	}
	
	cout << "found external/painted/total = " << nextern << " / " << npainted << " / " << mpGrd->SizeCellTab() << endl;

	return;
}
*/


template<>
void Generator<DIM_3D>::FlagExternalCells( BndGrid<DIM_3D>& bnd)
{
	// create boundary faces search struct
	vector< Key<3> >	tabbndfac;

	tabbndfac.reserve( bnd.SizeCellTab() );

	BndGrid<DIM_3D>::ColBCell::const_iterator	citr;

	for ( citr=bnd.cColCell().begin(); citr!=bnd.cColCell().end(); ++citr)
	{
		const BndGrid<DIM_3D>::GBndCell	&bcell = *citr;
		Key<3>	key;
		key.rFirst() = bnd.cNode( bcell.cNodeId( 0) ).cGrdId();
		key.rSecond() = bnd.cNode( bcell.cNodeId( 1) ).cGrdId();
		key.rThird() = bnd.cNode( bcell.cNodeId( 2) ).cGrdId();

		// check
		Vect3D vg1 = mpGrd->cNode( bnd.cNode( bcell.cNodeId( 0) ).cGrdId() ).cPos();
		Vect3D vg2 = mpGrd->cNode( bnd.cNode( bcell.cNodeId( 1) ).cGrdId() ).cPos();
		Vect3D vg3 = mpGrd->cNode( bnd.cNode( bcell.cNodeId( 2) ).cGrdId() ).cPos();
		Vect3D vb1 = bnd.cNode( bcell.cNodeId( 0) ).cPos();
		Vect3D vb2 = bnd.cNode( bcell.cNodeId( 1) ).cPos();
		Vect3D vb3 = bnd.cNode( bcell.cNodeId( 2) ).cPos();

		if ( (vg1 - vb1).module() > ZERO || (vg2 - vb2).module() > ZERO || (vg3 - vb3).module() > ZERO )
			cout << "Problem with grid id for boundary nodes\n";

		ASSERT( key.cFirst() > 0 && key.cSecond() > 0  && key.cThird() > 0 );

		key.Sort();

		tabbndfac.push_back( key);
	}

	sort( tabbndfac.begin(), tabbndfac.end() );


	// remove duplicate faces from the boundary grid
	vector< Key<3> >::iterator itre = unique( tabbndfac.begin(), tabbndfac.end() );
	tabbndfac.erase( itre, tabbndfac.end() );


	mKernel.FlagExternalCells( tabbndfac);
}



template<>
void Generator<DIM_3D>::FlagExternalCells()
{
	// flag cells conected to cube nodes
	Grid<DIM_3D>::ColCell::iterator	itr;

	for ( itr=mpGrd->rColCell().begin(); itr!=mpGrd->rColCell().end(); ++itr)
	{
		GCell& cell = *itr;
		for ( MGSize in=0; in<GCell::SIZE; ++in)
			if ( cell.cNodeId( in) >= 1 && cell.cNodeId( in) <= 8 ) // bounding box nodes
			{
				cell.SetExternal( true);
				break;
			}
	}

	// check & flag remainig cells using painting algo
	// TODO :: painting algo
}

template<>
void Generator<DIM_3D>::RemoveExternalCells()
{
	for ( Grid<DIM_3D>::ColCell::iterator itr = mpGrd->rColCell().begin(); itr != mpGrd->rColCell().end(); ++itr)
	{
//		if ( (*itr).IsExternal() )
//			mpGrd->rColCell().erase( itr);

		if ( (*itr).IsExternal() )
			mKernel.EraseCellSafe( itr->cId() );
			//mpGrd->EraseCellSafe( itr->cId() );
	}

	// removing nodes of the external cube
	mpGrd->EraseNode( 1);
	mpGrd->EraseNode( 2);
	mpGrd->EraseNode( 3);
	mpGrd->EraseNode( 4);
	mpGrd->EraseNode( 5);
	mpGrd->EraseNode( 6);
	mpGrd->EraseNode( 7);
	mpGrd->EraseNode( 8);

	mKernel.DisableTree();
 
}



template<>
void Generator<DIM_3D>::Triangulate_CircumCenter()
{
	TRACE( "Triangulation started");

	ProgressBar	bar(40);


	Grid<DIM_3D>&	grid = mKernel.rGrid();
	Grid<DIM_3D>::ColCell::const_iterator	citr;
	
	typedef triple< GVect, MGSize, GMetric> TElem;


	//Metric<MGFloat,DIM_3D>	met;
	//met.InitIso( 1.0);


	
	GVect	vc;
	GMetric dpmet, met;
	MGFloat	tabdist[GCell::SIZE];
	
	MGSize count, iter = 1;
	
	do
	{
		vector<TElem>	tabprop;
		
		bar.Init( grid.cColCell().size_valid() );
		bar.Start();

		for ( citr = grid.cColCell().begin(); citr != grid.cColCell().end(); ++citr, ++bar)
		{
			if ( (*citr).IsGood() || (*citr).IsExternal() )
				continue;
				
				
			//grid.rCell( (*citr).cId() ).SetModified( false);
			

			//////
			//vc = GridGeom<DIM_3D>::CreateSimplex( *citr, grid).Center();
			//GMetric metcell = mpCSpace->GetSpacing( vc);	// TODO :: VERY expensive !!!

			//////

			MetricInterpol< MGFloat, DIM_3D > interpol;

			interpol.Reset();
			for ( MGSize i=0; i<GCell::SIZE; ++i)
				interpol.Add( mData.cMetric( (*citr).cNodeId( i ) ) );

			GMetric::SMtx mtxcell;
			interpol.GetResult( mtxcell);

			GMetric metcell( mtxcell);

			GridGeom<DIM_3D>::CreateSimplex( *citr, grid).SphereCenterMetric( vc, metcell);

			if ( ! mKernel.mBox.IsInside( vc) )
				continue;

			MGSize	icell = mKernel.mLoc.Localize( vc);
			GCell& cell = mpGrd->rCell(icell);

			if ( cell.IsExternal() )
				continue;

			cell.SetModified( false);

			met = mpCSpace->GetSpacing( vc);	// TODO :: VERY expensive !!!


			//MetricInterpol<MGFloat,DIM_3D> metInterpol;
			//GMetric::SMtx smet;

			//const MGFloat wgh = 1.0 / static_cast<MGFloat>( GCell::SIZE );
			//
			//metInterpol.Reset();
			//for ( MGSize i=0; i<GCell::SIZE; ++i)
			//{
			//	const GMetric& metn = mData.cMetric( (*citr).cNodeId( i ) );
			//	metInterpol.Add( metn, wgh);
			//}
			//
			//metInterpol.GetResult( smet);
			//met = smet;


			dpmet.Reset();
			
			for ( MGSize i=0; i<GCell::SIZE; ++i)
			{
				const GVect vtmp = vc - grid.cNode( cell.cNodeId( i ) ).cPos();

				const GMetric& metn = mData.cMetric( cell.cNodeId( i ) );

				MGFloat l1 = ::sqrt( metn.Distance( vtmp) );
				MGFloat l2 = ::sqrt( met.Distance( vtmp) );
				tabdist[i] =  2.0 / 3.0 * (l1*l1 + l1*l2 + l2*l2) / (l1 + l2);
				tabdist[i] = l1;
			}
			

			for ( MGSize i=0; i<GCell::SIZE; ++i)
			{
				if ( 1.0 > 1.2 * tabdist[i] )
				{
					cell.SetGood( true);
					break;
				}
			}
			
			if ( ! cell.IsGood() )
				tabprop.push_back( TElem( vc, cell.cId(), met )  );
			
		}
		
		bar.Finish();

		TRACE( "number of new points = " << tabprop.size() );

		count = 0;
		
		if ( ! tabprop.size() )
			continue;

		random_shuffle( tabprop.begin(), tabprop.end() );
		
		bar.Init( tabprop.size() );
		bar.Start();
		
		
		for ( vector<TElem>::iterator itr = tabprop.begin(); itr != tabprop.end(); ++itr, ++bar)
		{
			
			if ( grid.cCell( (*itr).second ).IsModified() )
				continue;
			
			//const GCell &cell = grid.cCell( (*itr).second );
			//bool bCont = false;
			//for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
			//{
			//	MGSize ic = cell.cCellId( ifc).first;
			//
			//	if ( ic)
			//	{
			//		if ( grid.cCell( ic).IsModified() )
			//			bCont = true;
			//	}
			//}
			//if ( bCont)
			//	continue;

			vc  = itr->first;
			met = itr->third;
			
		
			//met = mpCSpace->GetSpacing( vc);	// TODO :: Expensive !!!

			MGSize inod = mKernel.InsertPointMetric( GNode( vc), met);
			if ( inod)
			{
				mData.InsertMetric( met, inod);
				//mData.InsertMetric( (*itr).third, inod);

				++count;
			}
		}

		bar.Finish();

		cout << "  points / tets  = " << grid.cColNode().size_valid() << " / " << grid.cColCell().size_valid() << endl;

	}
	while ( count > 1 && (iter++) < 50);

}




template<>
void Generator<DIM_3D>::Triangulate_Weatherill()
{
	TRACE( "Triangulation started");

	ProgressBar	bar(40);


	Grid<DIM_3D>&	grid = mKernel.rGrid();
	Grid<DIM_3D>::ColCell::const_iterator	citr;
	
	typedef triple< GVect, MGSize, GMetric> TElem;


	//Metric<MGFloat,DIM_3D>	met;
	//met.InitIso( 1.0);


	
	GVect	vc;
	GMetric dpmet, met;
	MGFloat	tabdist[GCell::SIZE];
	
	MGSize count, iter = 1;
	
	do
	{
		vector<TElem>	tabprop;
		
		bar.Init( grid.cColCell().size_valid() );
		bar.Start();

		for ( citr = grid.cColCell().begin(); citr != grid.cColCell().end(); ++citr, ++bar)
		{
			if ( (*citr).IsGood() || (*citr).IsExternal() )
				continue;
				
				
			grid.rCell( (*citr).cId() ).SetModified( false);
			
			Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( *citr, grid);
			vc = tet.Center();

			//met = mpCSpace->GetSpacing( vc);	// TODO :: VERY expensive !!!


			MetricInterpol<MGFloat,DIM_3D> metInterpol;
			GMetric::SMtx smet;

			//const MGFloat wgh = 1.0 / static_cast<MGFloat>( GCell::SIZE );
			//
			//metInterpol.Reset();
			//for ( MGSize i=0; i<GCell::SIZE; ++i)
			//{
			//	const GMetric& metn = mData.cMetric( (*citr).cNodeId( i ) );
			//	metInterpol.Add( metn, wgh);
			//}
			//
			//metInterpol.GetResult( smet);
			//met = smet;


			dpmet.Reset();
			
			for ( MGSize i=0; i<GCell::SIZE; ++i)
			{
				const GVect vtmp = vc - grid.cNode( (*citr).cNodeId( i ) ).cPos();

				const GMetric& metn = mData.cMetric( (*citr).cNodeId( i ) );

				//metInterpol.Reset();
				//metInterpol.Add( met, 0.5);
				//metInterpol.Add( metn, 0.5);
				//metInterpol.GetResult( smet);
				//GMetric mettmp = smet;
				
				//tabdist[i] = ::sqrt( met.Distance( vtmp) );

				tabdist[i] = ::sqrt( metn.Distance( vtmp) );

				//MGFloat l1 = ::sqrt( metn.Distance( vtmp) );
				//MGFloat l2 = ::sqrt( met.Distance( vtmp) );
				
				//tabdist[i] =  2.0 / 3.0 * (l1*l1 + l1*l2 + l2*l2) / (l1 + l2);
				//tabdist[i] = ( vc - grid.cNode( (*citr).cNodeId( i ) ).cPos() ).module();
			}
			

			for ( MGSize i=0; i<GCell::SIZE; ++i)
				if ( 1.0 > 1.2 * tabdist[i] )
				{
					grid.rCell( (*citr).cId() ).SetGood( true);
					break;
				}
			
			if ( ! (*citr).IsGood() )
			{
				tabprop.push_back( TElem( vc, (*citr).cId(), met )  );
			}
			
		}
		
		bar.Finish();

		TRACE( "number of new points = " << tabprop.size() );

		count = 0;
		
		if ( ! tabprop.size() )
			continue;

		random_shuffle( tabprop.begin(), tabprop.end() );
		
		bar.Init( tabprop.size() );
		bar.Start();
		
		
		for ( vector<TElem>::iterator itr = tabprop.begin(); itr != tabprop.end(); ++itr, ++bar)
		{
			
			if ( grid.cCell( (*itr).second ).IsModified() )
				continue;
			
			//const GCell &cell = grid.cCell( (*itr).second );
			//bool bCont = false;
			//for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
			//{
			//	MGSize ic = cell.cCellId( ifc).first;
			//
			//	if ( ic)
			//	{
			//		if ( grid.cCell( ic).IsModified() )
			//			bCont = true;
			//	}
			//}
			//if ( bCont)
			//	continue;

			vc  = itr->first;
			//met = itr->third;
			
		
			met = mpCSpace->GetSpacing( vc);	// TODO :: Expensive !!!

			MGSize inod = mKernel.InsertPointMetric( GNode( vc), met);
			if ( inod)
			{
				mData.InsertMetric( met, inod);
				//mData.InsertMetric( (*itr).third, inod);

				++count;
			}
		}

		bar.Finish();

		cout << "  points / tets  = " << grid.cColNode().size_valid() << " / " << grid.cColCell().size_valid() << endl;

	}
	while ( count > 1 && (iter++) < 200);

}



template<>
void Generator<DIM_3D>::Triangulate_Weatherill2()
{
	TRACE( "Triangulation started");

	//MGSize			maxProp = 500;
	//const MGSize	constMaxProp = 200000;
	//const MGFloat	constALPHA = 1.15;

	const MGSize	nMaxIter		= this->ConfigSect().ValueSize( ConfigStr::Master::Generator::GEN_MAXITER );
	const MGFloat	constALPHA		= this->ConfigSect().ValueFloat( ConfigStr::Master::Generator::GEN_CONSTALPHA );
	MGSize			maxProp			= this->ConfigSect().ValueSize( ConfigStr::Master::Generator::GEN_MAXPROPBEGIN );
	const MGSize	constMaxProp	= this->ConfigSect().ValueSize( ConfigStr::Master::Generator::GEN_MAXPROPEND );

	const bool		constBDistCheck	= this->ConfigSect().ValueBool( ConfigStr::Master::Generator::GEN_WITH_BDIST_CHECK );
	const MGFloat	constBndALPHA	= this->ConfigSect().ValueFloat( ConfigStr::Master::Generator::GEN_BND_CONSTALPHA );
	

	ProgressBar	bar(40);

	Grid<DIM_3D>&	grid = mKernel.rGrid();
	
	typedef triple< GVect, MGSize, GMetric> TElem;

	
	GVect	vc;
	GMetric dpmet, met;
	MGFloat	tabdist[GCell::SIZE];
	
	MGSize count, iter = 1;
	
	do
	{
		vector<TElem>	tabprop;
		
		MGSize nbar = grid.cColCell().size_valid();

		bar.Init( nbar );
		bar.Start();

		for ( Grid<DIM_3D>::ColCell::const_iterator citr = grid.cColCell().begin(); citr != grid.cColCell().end(); ++citr, ++bar)
		{
			grid.rCell( (*citr).cId() ).SetModified( false);

			if ( (*citr).IsGood() || (*citr).IsExternal() )
				continue;
				
			
			Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( *citr, grid);
			vc = tet.Center();

			//met = mpCSpace->GetSpacing( vc);	// TODO :: VERY expensive !!!


			MetricInterpol<MGFloat,DIM_3D> metInterpol;
			GMetric::SMtx smet;

			//const MGFloat wgh = 1.0 / static_cast<MGFloat>( GCell::SIZE );
			//
			//metInterpol.Reset();
			//for ( MGSize i=0; i<GCell::SIZE; ++i)
			//{
			//	const GMetric& metn = mData.cMetric( (*citr).cNodeId( i ) );
			//	metInterpol.Add( metn, wgh);
			//}
			//
			//metInterpol.GetResult( smet);
			//met = smet;


			dpmet.Reset();
			
			for ( MGSize i=0; i<GCell::SIZE; ++i)
			{
				const GVect vtmp = vc - grid.cNode( (*citr).cNodeId( i ) ).cPos();

				const GMetric& metn = mData.cMetric( (*citr).cNodeId( i ) );
				//const GMetric& metn = mpCSpace->GetSpacing( grid.cNode( (*citr).cNodeId( i ) ).cPos() );

				//metInterpol.Reset();
				//metInterpol.Add( met, 0.5);
				//metInterpol.Add( metn, 0.5);
				//metInterpol.GetResult( smet);
				//GMetric mettmp = smet;
				
				//tabdist[i] = ::sqrt( met.Distance( vtmp) );

				tabdist[i] = ::sqrt( metn.Distance( vtmp) );

				//MGFloat l1 = ::sqrt( metn.Distance( vtmp) );
				//MGFloat l2 = ::sqrt( met.Distance( vtmp) );
				
				//tabdist[i] =  2.0 / 3.0 * (l1*l1 + l1*l2 + l2*l2) / (l1 + l2);
				//tabdist[i] = ( vc - grid.cNode( (*citr).cNodeId( i ) ).cPos() ).module();
			}
			


			for ( MGSize i=0; i<GCell::SIZE; ++i)
				if ( 1.0 > constALPHA * tabdist[i] )
				{
					grid.rCell( (*citr).cId() ).SetGood( true);
					break;
				}

			// check dist from boundary

			//bool bDistOk = true;
			if ( ! (*citr).IsGood() && constBDistCheck )
			{
				for ( MGSize i=0; i<GCell::SIZE; ++i)
				{
					const GCell& celln = grid.cCell( citr->cCellId(i).first );

					if ( celln.IsExternal() )  // check dist
					{
						GFace face;
						citr->GetFaceVNIn( face, i);
						SimplexFace<DIM_3D> sface = GridGeom<DIM_3D>::CreateSimplexFace( face, grid);

						GVect vnface = sface.Vn();
						GVect dv = vc - sface.Center();
						GVect vv = ( (dv * vnface) / (vnface*vnface) ) * vnface;

						MGFloat h = tet.Volume() / sface.Area();

						//vv = h * vnface.versor();

						MGFloat facedist;
						//GMetric met = mpCSpace->GetSpacing( vc);	// TODO :: VERY expensive !!!
						//facedist = ::sqrt( met.Distance( vv) );
						//facedist =  vv * vv;

						facedist = max( max( mData.cMetric( face.cNodeId( 0 ) ).Distance( vv ), mData.cMetric( face.cNodeId( 1 ) ).Distance( vv ) ), mData.cMetric( face.cNodeId( 2 ) ).Distance( vv ) );
						facedist = ::sqrt( facedist );


						//if ( 1.0 > facedist )
						if ( 1.0 > constBndALPHA * facedist )
						{
							grid.rCell( (*citr).cId() ).SetGood( true);
							//bDistOk = false;
							//cout << "good\n";
							break;
						}
					}
				}	
			}


			//// check cell thickness
			//if ( ! (*citr).IsGood() )
			//{

			//	for ( MGSize i=0; i<GCell::SIZE; ++i)
			//	{
			//		GFace face;
			//		citr->GetFaceVNIn( face, i);
			//		SimplexFace<DIM_3D> sface = GridGeom<DIM_3D>::CreateSimplexFace( face, grid);

			//		GVect vnface = sface.Vn();

			//		MGFloat h = tet.Volume() / sface.Area();

			//		GVect vv = h * vnface.versor();

			//		const GMetric& metn = mData.cMetric( (*citr).cNodeId( i ) );
			//		//GMetric met = mpCSpace->GetSpacing( vc);	// TODO :: VERY expensive !!!

			//		MGFloat facedist = ::sqrt( metn.Distance( vv) );

			//		facedist = max( max( mData.cMetric( face.cNodeId( 0 ) ).Distance( vv ), mData.cMetric( face.cNodeId( 1 ) ).Distance( vv ) ), mData.cMetric( face.cNodeId( 2 ) ).Distance( vv ) );
			//		facedist = ::sqrt( facedist );

			//		if ( 0.5 > facedist )
			//		{
			//			grid.rCell( (*citr).cId() ).SetGood( true);
			//			//cout << "good\n";
			//			break;
			//		}
			//	}	

			//}


			if ( ! (*citr).IsGood()  )
			//if ( ! (*citr).IsGood() && bDistOk )
			{
				tabprop.push_back( TElem( vc, (*citr).cId(), met )  );
			}
			
		}
		
		bar.Finish();

		TRACE( "number of new points = " << tabprop.size() );

		count = 0;
		
		if ( ! tabprop.size() )
			continue;


		/////////////////////////////////////////////////////////////
		// randomize the order the nodes will be inserted
		random_shuffle( tabprop.begin(), tabprop.end() );


		static vector<MGSize>	tabccell;
		static vector<GFace>	tabcface;

		static vector<GCell*>	tabcavcell;

		tabcface.reserve(20);
		tabccell.reserve(20);
		tabcavcell.reserve(20);


		//nbar = min( constMaxProp, (MGSize)tabprop.size() );
		nbar = min( maxProp, (MGSize)tabprop.size() );
		bar.Init( nbar );
		//bar.Init( tabprop.size() );
		bar.Start();
		
		MGSize ipropcount = 0;
		for ( vector<TElem>::iterator itr = tabprop.begin(); itr != tabprop.end() && ipropcount<maxProp; ++itr, ++bar, ++ipropcount)
		{
			if ( ! grid.cColCell().is_valid( itr->second ) )
				continue;

			if ( grid.cCell( itr->second ).IsModified() )
				continue;
			
			MGSize inod;
			GVect vct  = itr->first;
			
			/////////////////////////////////////////////////////////////
			// find cell containing vct
			//MGSize	icell = mKernel.mLoc.Localize( vct);
			MGSize	icell = itr->second;

			//if ( grid.cCell( icell ).IsModified() )
			//	continue;

			/////////////////////////////////////////////////////////////
			// search for the delaunay cavity of vct
			tabcface.resize(0);
			tabccell.resize(0);
			tabcavcell.resize(0);

			GMetric	met = mpCSpace->GetSpacing( vct);


			// tabccel  - cells creating a cavity for the vct
			// tabcface - faces bounding the cavity

			//if ( ! mKernel.FindCavity( tabccell, icell, vct) )
			if ( ! mKernel.FindCavityMetric( tabccell, icell, vct, met) )
				continue;


			/////////////////////////////////////////////////////////////
			// check dist from boundary
			if ( constBDistCheck )
			{
				bool bOk = true;

				for ( MGSize ic=0; ic<tabccell.size(); ++ic)
				{
					const GCell& cell = mpGrd->cCell( tabccell[ic] );
	
					Simplex<DIM_3D> tri = GridGeom<DIM_3D>::CreateSimplex( icell, *mpGrd);

					for ( MGSize i=0; i<GCell::SIZE; ++i)
					{
						const GCell& celln = mpGrd->cCell( cell.cCellId(i).first );

						if ( celln.IsExternal() )  // check dist
						{
							GFace face;
							cell.GetFaceVNIn( face, i);
							SimplexFace<DIM_3D> sface = GridGeom<DIM_3D>::CreateSimplexFace( face, *mpGrd);

							GVect vnface = sface.Vn();
							GVect dv = vct - sface.Center();
							GVect vv = ( (dv * vnface) / (vnface*vnface) ) * vnface;

							MGFloat h = tri.Volume() / sface.Area();

							//vv = h * vnface.versor();

							MGFloat facedist_cell;
							facedist_cell = met.Distance( vv);
							facedist_cell = ::sqrt( facedist_cell );
						
							MGFloat facedist_wall;
							facedist_wall = max( max( mData.cMetric( face.cNodeId( 0 ) ).Distance( vv ), mData.cMetric( face.cNodeId( 1 ) ).Distance( vv ) ), mData.cMetric( face.cNodeId( 2 ) ).Distance( vv ) );
							facedist_wall = ::sqrt( facedist_wall );

							//if ( 1.0 > facedist )
							//if ( 1.0 > constBndALPHA * facedist )
							if ( 1.0 > constBndALPHA * facedist_cell && 1.0 > constBndALPHA * facedist_wall)
							{
								mpGrd->rCell( icell ).SetGood( true);
								bOk = false;
								//cout << "good\n";
								break;
							}
						}
					}	
				}


				/////////////////////////////////////////////////////////////
				// wash paint and start again
				if ( ! bOk)
				{
					for ( MGSize k=0; k<tabccell.size(); ++k)
						mpGrd->rCell( tabccell[k]).WashPaint();

					continue;
				}
			}

			///////////////////////////////////////////////////////////////
			mKernel.CavityCorrection( tabccell, vct);


			/////////////////////////////////////////////////////////////
			// insert new point into mesh
			mKernel.CavityBoundary( tabcface, tabccell);

			inod = mKernel.InsertPoint( tabccell, tabcface, vct);

			if ( inod)
			{
				mData.InsertMetric( met, inod);
				++count;
			}

		}

		bar.Finish();
		//cout << "new nodes / nodes / tets  = ";
		cout << "  " << count << " / " << grid.cColNode().size_valid() << " / " << grid.cColCell().size_valid() << endl;

		//mDestr.StartPurge();

		maxProp = min( 2*maxProp, constMaxProp );

	}
	while ( count > 1 && (iter++) < nMaxIter);

	cout << endl;

}



template<>
void Generator<DIM_3D>::Triangulate_AlongEdges( vector< Key<2> >* ptabedge )
{
	const MGFloat	constALPHA = 1.3;
	//const MGFloat	constBETA = 1.3;
	const MGFloat	constBETA = 2.2;


	MGSize count, iter = 1;
	ProgressBar	bar(40);


	do
	{

		//for ( Grid<DIM_3D>::ColCell::const_iterator citr = mpGrd->cColCell().begin(); citr != mpGrd->cColCell().end(); ++citr)
		//	mpGrd->rCell( (*citr).cId() ).SetModified( false);

			
		/////////////////////////////////////////////////////////////
		// find all internal edges
		vector< Key<2> > tabedg;
		mpGrd->FindInternalEdges( tabedg );


		bar.Init( tabedg.size() );
		bar.Start();

		vector<GVect>	tabprop;

		for ( MGSize ie=0; ie<tabedg.size(); ++ie, ++bar)
		{
			if ( binary_search( ptabedge->begin(), ptabedge->end(), tabedg[ie] ) )
				continue;

			GVect v0 = mpGrd->cNode( tabedg[ie].cFirst() ).cPos();
			GVect v1 = mpGrd->cNode( tabedg[ie].cSecond() ).cPos();

			// TODO :: metric interpolation !
			GMetric met = 0.5 * ( mData.cMetric( tabedg[ie].cFirst() ) + mData.cMetric( tabedg[ie].cSecond() ) );
			MGFloat dist = ::sqrt( met.Distance( v1 - v0 ) );

			/////////////////////////////////////////////////////////////
			// jump to another edge - current one is short enough
			if ( dist < constALPHA )
				continue;


			/////////////////////////////////////////////////////////////
			// run 1D generator for line created for the edge

			GET::Line<DIM_3D> gline( v0, (v1-v0).versor() );

			SliceControlSpace<DIM_1D,DIM_3D>	scspace( mpCSpace, &gline);

			GridWBnd<DIM_1D>	grid;
			BndGrid<DIM_1D>		&bgrd = grid.rBndGrid();

			Generator<DIM_1D>	gen( grid, &scspace);

			TDefs<DIM_1D>::GBndNode	bnod;

			bnod.rPos() = gline.FindParam( v0);
			bnod.rGrdId() = 0;
			bgrd.rNode1() = bnod;

			bnod.rPos() = gline.FindParam( v1);
			bnod.rGrdId() = 0;
			bgrd.rNode2() = bnod;

			gen.Process();


			/////////////////////////////////////////////////////////////
			// get all internal 1D grid nods and put them in the proposition node array

			//for ( MGSize k=2; k<grid.SizeNodeTab(); ++k)
			for ( MGSize k=3; k<=grid.SizeNodeTab(); ++k)
			{
				GVect vp;
				gline.GetCoord( vp, grid.cNode(k).cPos() );
				tabprop.push_back( vp);
			}
		}

		bar.Finish();


		count = 0;
		
		if ( ! tabprop.size() )
			continue;


		/////////////////////////////////////////////////////////////
		// randomize the order the nodes will be inserted
		random_shuffle( tabprop.begin(), tabprop.end() );


		static vector<MGSize>	tabccell;
		static vector<GFace>	tabcface;

		static vector<GCell*>	tabcavcell;

		tabcface.reserve(20);
		tabccell.reserve(20);
		tabcavcell.reserve(20);


		bar.Init( tabprop.size() );
		bar.Start();
		
		
		for ( vector<GVect>::iterator itr = tabprop.begin(); itr != tabprop.end(); ++itr, ++bar)
		{
			
			MGSize inod;
			GVect vct = *itr;
			
			/////////////////////////////////////////////////////////////
			// find cell containing vct
			MGSize	icell = mKernel.mLoc.Localize( vct);

			//if ( mpGrd->cCell( icell ).IsModified() )
			//	continue;


			/////////////////////////////////////////////////////////////
			// search for the delaunay cavity of vct
			tabcface.resize(0);
			tabccell.resize(0);
			tabcavcell.resize(0);

			GMetric	met = mpCSpace->GetSpacing( vct);


			// tabccel  - cells creating a cavity for the vct
			// tabcface - faces bounding the cavity

			//if ( ! mKernel.FindCavity( tabccell, icell, vct) )
			if ( ! mKernel.FindCavityMetric( tabccell, icell, vct, met) )
				continue;


			/////////////////////////////////////////////////////////////
			// find all nodes of the cavity
			vector<MGSize>	tabnod;
			//for ( MGSize ic=0; ic<tabcavcell.size(); ++ic)
			//	for ( MGSize in=0; in<GCell::SIZE; ++in)
			//		tabnod.push_back( tabcavcell[ic]->cNodeId( in) );

			for ( MGSize ic=0; ic<tabccell.size(); ++ic)
				for ( MGSize in=0; in<GCell::SIZE; ++in)
					tabnod.push_back( mpGrd->cCell( tabccell[ic] ).cNodeId( in) );

			sort( tabnod.begin(), tabnod.end() );
			vector<MGSize>::iterator itrend = unique( tabnod.begin(), tabnod.end() );
			tabnod.erase( itrend, tabnod.end() );

			/////////////////////////////////////////////////////////////
			// check the distance from new point to cavity nodes
			// if one of them is to close then reject the new point
			bool bOk = true;

			for ( MGSize in=0; in<tabnod.size(); ++in)
			{
				const GVect& vn = mpGrd->cNode( tabnod[in]).cPos();
				const GMetric& metn = mData.cMetric( tabnod[in] );
				//const GMetric& metn = mpCSpace->GetSpacing( vn);

				const GVect vtmp = vct - vn;

				MGFloat l1 = ::sqrt( metn.Distance( vtmp) );
				MGFloat l2 = ::sqrt( met.Distance( vtmp) );

				MGFloat dist =  2.0 / 3.0 * (l1*l1 + l1*l2 + l2*l2) / (l1 + l2);

				//MGFloat dist = (vn - vct).module();
				//if ( dist*constBETA < hspac)

				if ( dist*constBETA < 1)
				{
					bOk = false;
					break;
				}
			}


			/////////////////////////////////////////////////////////////
			// wash paint and start again
			if ( ! bOk)
			{
				//for ( MGSize ic=0; ic<tabcavcell.size(); ++ic)
				//	tabcavcell[ic]->WashPaint();

				for ( MGSize k=0; k<tabccell.size(); ++k)
					mpGrd->rCell( tabccell[k]).WashPaint();

				continue;
			}



			/////////////////////////////////////////////////////////////
			// insert new point into mesh
			mKernel.CavityCorrection( tabccell, vct);
			mKernel.CavityBoundary( tabcface, tabccell);

			//if ( !tabccell.size() || !tabcface.size() )
			//{
			//	//cout << " !!!!!\n";
			//	continue;
			//}

			inod = mKernel.InsertPoint( tabccell, tabcface, vct);

			if ( inod)
			{
				mData.InsertMetric( met, inod);
				
				++count;
			}
		}

		bar.Finish();
		cout << "Number of inserted nodes = " << count << endl;

		//WriteGrdTEC<DIM_3D> wtec(mpGrd);
		//wtec.DoWrite( "iter.plt");


	}
	while ( count > 1 && (iter++) < 10);
	//while ( count && (iter++) < 6);

}





template<>
void Generator<DIM_3D>::Triangulate( vector< Key<2> >* ptabedge )
{
	if ( ! this->ConfigSectIsValid() )
		THROW_INTERNAL( "Generator<DIM_3D>::Triangulate -- config is invalid");

	MGString stype = this->ConfigSect().ValueString( ConfigStr::Master::Generator::TriangType::KEY );


	if ( stype == MGString( ConfigStr::Master::Generator::TriangType::Value::GENTT_EDGES ) )
		Triangulate_AlongEdges( ptabedge );
	else
	if ( stype == MGString( ConfigStr::Master::Generator::TriangType::Value::GENTT_CELLC ) )
		Triangulate_Weatherill2();
	else
	if ( stype == MGString( ConfigStr::Master::Generator::TriangType::Value::GENTT_CIRCC ) )
		Triangulate_CircumCenter();
	else
		THROW_INTERNAL( "Generator<DIM_3D>::Triangulate -- unknown option");

	//Triangulate_CircumCenter();
	//Triangulate_Weatherill2();

	//Triangulate_Weatherill();
	//Triangulate_AlongEdges( ptabedge);
}





template<>
void Generator<DIM_3D>::GenerateTestCube( const MGSize& np)
{
	printf( "\ngenerating %d random points in a cube\n", np);

	ProgressBar	bar(40);

	bar.Init( np);
	bar.Start();

	// create starting box
	GVect	vmin(0,0,0), vmax(1,1,1);
	GVect	vct;

	mKernel.InitBoundingBox( vmin, vmax);

	mKernel.InitCube();

	// insert points

	Metric<DIM_3D>	met;
	met.InitIso( 1.0);

	for ( MGSize i=0; i<np; ++i, ++bar)
	{
		vct.rX() = ((MGFloat)::rand()) / ((MGFloat)RAND_MAX);
		vct.rY() = ((MGFloat)::rand()) / ((MGFloat)RAND_MAX);
		vct.rZ() = ((MGFloat)::rand()) / ((MGFloat)RAND_MAX);

		//InsertPointMetric( vct, met );
		mKernel.InsertPoint( vct );
	}

	FlagExternalCells();
	RemoveExternalCells();

	bar.Finish();
}



template<>
void Generator<DIM_3D>::GenerateTestSphere( const MGSize& np)
{
	printf( "\ngenerating %d random points in a sphere\n", np);

	ProgressBar	bar(40);

	bar.Init( np);
	bar.Start();


	// create starting box
	GVect	vmin(0,0,0), vmax(1,1,1);
	GVect	vct;

	Metric<DIM_3D>	met;
	met.InitIso( 1.0);

	mKernel.InitBoundingBox( vmin, vmax);

	mKernel.InitCube();


	// insert points

	for ( MGSize i=0; i<np; ++i, ++bar)
	{
		const MGFloat fi  = (M_PI * (MGFloat)::rand()) / ((MGFloat)RAND_MAX) - M_PI_2;
		const MGFloat lam = (2*M_PI * (MGFloat)::rand()) / ((MGFloat)RAND_MAX) - M_PI;

		vct.rX() = ::cos(fi) * cos(lam);
		vct.rY() = ::cos(fi) * sin(lam);
		vct.rZ() = ::sin( fi);

		mKernel.InsertPointMetric( vct, met );
	}


	FlagExternalCells();
	RemoveExternalCells();

	bar.Finish();

	// export
	//mpGrd->CheckConnectivity();
	//WriteTEC<DIM_3D> wtec(mpGrd);
	//wtec.DoWrite( "_initial.dat");
}


template <>
void Generator<DIM_3D>::Process()
{
	BndGrid<DIM_3D> &bnd = *mpBndGrd;

	ProgressBar	bar(40);


	bar.Init( bnd.SizeNodeTab() );
	bar.Start();


	GVect	vmin, vmax;
	GVect	vct;


	bnd.FindMinMax( vmin, vmax);
	
	mKernel.InitBoundingBox( vmin, vmax);

	mKernel.InitCube();


	/////////////////////////////////////////////////////////////
	// setting metric for box corners to dummy values

	GMetric metdef;
	metdef.InitIso( 1.0);
	for ( MGSize i=1; i<=mpGrd->SizeNodeTab(); ++i)
		mData.InsertMetric( metdef, i);


	/////////////////////////////////////////////////////////////
	// check the grid (box)

	mpGrd->CheckConnectivity();

	WriteGrdTEC<DIM_3D> wtec(mpGrd);

	wtec.DoWrite( "box.plt");




	/////////////////////////////////////////////////////////////
	// insert bundary points

	vector<MGSize>	tabid(bnd.SizeNodeTab());
	for ( MGSize i=1; i<bnd.SizeNodeTab()+1; ++i)
		tabid[i-1] = i;

	random_shuffle( tabid.begin(), tabid.end() );


	for ( MGSize ii=0; ii<tabid.size(); ++ii, ++bar)
	{
		MGSize i = tabid[ii];

		vct = bnd.cNode(i).cPos();

		MGSize inod = mKernel.InsertPoint( vct );

		if ( ! inod)
		{
			char sbuf[512];
			sprintf( sbuf, "CRASH :: kernel failed to insert new point with coordinates [%lg %lg %lg]", 
							vct.cX(), vct.cY(), vct.cZ() );

			THROW_INTERNAL( sbuf);
		}

		//if ( mpCSpace)
			//mData.InsertMetric( mpCSpace->GetSpacing( vct), inod );
			mKernel.InsertMetric( mpCSpace->GetSpacing( vct), inod );

		mpGrd->rNode(inod).rMasterId() = bnd.cNode(i).cMasterId();
		bnd.rNode(i).rGrdId() = inod;

	}


	bar.Finish();



//// HACK
//	cout << endl << "BREAKING THE CUBE" << endl;
//	MGSize np = 10000;
//	//ProgressBar	bar(40);
//
//	bar.Init( np);
//	bar.Start();
//
//	// create starting box
//	GVect	dvm = vmax - vmin;
//	//GVect	vct;
//
//	// insert points
//	for ( MGSize i=0; i<np; ++i, ++bar)
//	{
//		vct.rX() = ((MGFloat)::rand()) / ((MGFloat)RAND_MAX) * dvm.cX() + vmin.cX();
//		vct.rY() = ((MGFloat)::rand()) / ((MGFloat)RAND_MAX) * dvm.cY() + vmin.cY();
//		vct.rZ() = ((MGFloat)::rand()) / ((MGFloat)RAND_MAX) * dvm.cZ() + vmin.cZ();
//
//
//		//InsertPointMetric( vct, met );
//		MGSize inod = mKernel.InsertPoint( vct );
//		mKernel.InsertMetric( mpCSpace->GetSpacing( vct), inod );
//	}
//
//	bar.Finish();




	/////////////////////////////////////////////////////////////
	// recover boundary

	//// recover missing edges
	//EdgeReconstructor	erec( bnd, *mpCSpace, mKernel);

	//erec.Init();
	//erec.Reconstruct();

	//// recover missing faces
	//FaceReconstructor	frec( bnd, *mpCSpace, mKernel, erec);

	//frec.Init();
	//frec.Reconstruct();

	//cout << "Reconstructor - no of new edge points = " << erec.NoPoints() << endl;
	//cout << "Reconstructor - no of new face points = " << frec.NoPoints() << endl;


	/////////////////////////////////////////////////////////////
	// create tab of bnd faces needed for flagging external cells
	vector< Key<3> >	tabbface;

	//frec.FillBFaceTab( tabbface);


	/////////////////////////////////////////////////////////////
	ReconstructorCDT<DIM_3D>	recon( bnd, *mpCSpace, mKernel );

	recon.Init();
	recon.Reconstruct();
	//recon.UpdateBoundaryGrid( bnd);

	WriteBndTEC<DIM_3D> wtecbnd( &bnd);
	wtecbnd.DoWrite( "bndgrid_after.dat");

	///////////////////////////////////////////////////////////////
	//Reconstructor<DIM_3D>	recon( bnd, *mpCSpace, mKernel );

	//recon.Init();
	//recon.Reconstruct();
	//recon.FillBFaceTab( tabbface);

	////cout << "Reconstructor - no of new edge points = " << erec.NoPoints() << endl;
	////cout << "Reconstructor - no of new face points = " << frec.NoPoints() << endl;

	/////////////////////////////////////////////////////////////
	// flag external cells

	//FlagExternalCells( tabbface);
	mKernel.FlagExternalCells( recon.rTabBnSubFaces() );

	wtec.DoWrite( "_initial.dat");

	mDestr.Init( recon.cTabCompEdge() );

	/////////////////////////////////////////////////////////////
	// start triangulation

	Triangulate( & recon.rTabBnSubEdges() );

	wtec.DoWrite( "_after_triang.dat");

	/////////////////////////////////////////////////////////////
	// optimisation by flips
	//cout << endl << "optimisation sweeps..." << endl;

	MGString secname = MGString( ConfigStr::Master::Generator::Optimiser::NAME );
	const CfgSection* cfgsec = &ConfigSect().GetSection( secname);

	Optimiser<DIM_3D> opt( mKernel);
	opt.Create( cfgsec);

	//opt.DoOptimise();

	//Destroyer<DIM_3D>	destr( bnd, *mpCSpace, mKernel );
	//destr.Init( recon.cTabCompEdge() );


	/////////////////////////////////////////////////////////////
	// remove external cells
	RemoveExternalCells();
	//wtec.DoWrite( "_after_rem.dat");

	/////////////////////////////////////////////////////////////
	// remove extra boundary nodes (used for recovery)

	//mDestr.Optimise();
	mKernel.SetAllCellsModified( true);
	opt.DoOptimise();

	MGSize ncur, nleft, iter=0;
	do
	{
		++iter;
		mDestr.PurgeEC();
		mDestr.Optimise();
		nleft = mDestr.PurgePP();
	}
	while ( iter < 20 && nleft > 0);


	mKernel.SetAllCellsModified( true);

	iter = 0;
	ncur = mDestr.PPTabSize();
	do
	{
		++iter;
		nleft = ncur;

		opt.DoOptimise(1);
		//mDestr.Optimise();
		mDestr.PurgePP();

		ncur = mDestr.PPTabSize();
	}
	while ( nleft - ncur > 0 && iter < 10 );


	//opt.DoOptimise();
	//mDestr.PurgePP();

	//mDestr.Optimise();




	mDestr.UpdateBoundaryGrid( bnd);
	mDestr.ExportExtraPnts( "extrapnts.txt");

	//opt.DoOptimise( 10);

	cout << endl;
	cout << "MESH points / tets  = " << mpGrd->cColNode().size_valid() << " / " << mpGrd->cColCell().size_valid() << endl;

	mpGrd->CheckQuality();
	mpGrd->CheckConnectivity();


	OptimiserSliver optsliv( mKernel);
	optsliv.DoOptimise();

	mpGrd->CheckQuality();
	mpGrd->CheckConnectivity();

	//ofstream oof("celltab.dump");
	//mpGrd->rColCell().dump( oof);

}





//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

