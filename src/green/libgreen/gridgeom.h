#ifndef __GRIDGEOM_H__
#define __GRIDGEOM_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/grid.h"
#include "libgreen/bndgrid.h"
#include "libcoregeom/simplex.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


// TODO :: define threshold values here




//////////////////////////////////////////////////////////////////////
// class GridGeom
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class GridGeom
{
public:

	static Simplex<DIM>		CreateSimplex( const Cell<DIM>& cell, const Grid<DIM>& grd);

	static Simplex<DIM>		CreateSimplex( const MGSize& icell, const Grid<DIM>& grd)
	{
		return CreateSimplex( grd.cCell( icell), grd);
	}


	static SimplexFace<DIM>		CreateSimplexFace( const Key<DIM>& key, const Grid<DIM>& grd);

	static SimplexFace<DIM>		CreateSimplexFace( const Face<DIM>& face, const Grid<DIM>& grd);

	static SimplexFace<DIM>		CreateSimplexFace( const BndCell<DIM>& cell, const BndGrid<DIM>& bndgrd);
};
//////////////////////////////////////////////////////////////////////


template <>
inline Simplex<DIM_1D> GridGeom<DIM_1D>::CreateSimplex( const Cell<DIM_1D>& cell, const Grid<DIM_1D>& grd)
{
	return Simplex<DIM_1D>( grd.cNode( cell.cNodeId(0)).cPos(), grd.cNode( cell.cNodeId(1)).cPos() );
}





template <>
inline Simplex<DIM_2D> GridGeom<DIM_2D>::CreateSimplex( const Cell<DIM_2D>& cell, const Grid<DIM_2D>& grd)
{
	return Simplex<DIM_2D>( grd.cNode( cell.cNodeId(0)).cPos(), grd.cNode( cell.cNodeId(1)).cPos(), 
								grd.cNode( cell.cNodeId(2)).cPos() );
}

template <>
inline SimplexFace<DIM_2D> GridGeom<DIM_2D>::CreateSimplexFace( const Key<DIM_2D>& key, const Grid<DIM_2D>& grd)
{
	return SimplexFace<DIM_2D>( grd.cNode( key.cElem(0)).cPos(), grd.cNode( key.cElem(1)).cPos() );
}

template <>
inline SimplexFace<DIM_2D> GridGeom<DIM_2D>::CreateSimplexFace( const Face<DIM_2D>& face, const Grid<DIM_2D>& grd)
{
	return SimplexFace<DIM_2D>( grd.cNode( face.cNodeId(0)).cPos(), grd.cNode( face.cNodeId(1)).cPos() );
}

template <>
inline SimplexFace<DIM_2D> GridGeom<DIM_2D>::CreateSimplexFace( const BndCell<DIM_2D>& cell, const BndGrid<DIM_2D>& bndgrd)
{
	return SimplexFace<DIM_2D>( bndgrd.cNode( cell.cNodeId(0)).cPos(), bndgrd.cNode( cell.cNodeId(1)).cPos() );
}




template <>
inline Simplex<DIM_3D> GridGeom<DIM_3D>::CreateSimplex( const Cell<DIM_3D>& cell, const Grid<DIM_3D>& grd)
{
	return Simplex<DIM_3D>( grd.cNode( cell.cNodeId(0)).cPos(), grd.cNode( cell.cNodeId(1)).cPos(), 
								grd.cNode( cell.cNodeId(2)).cPos(), grd.cNode( cell.cNodeId(3)).cPos() );
}

template <>
inline SimplexFace<DIM_3D> GridGeom<DIM_3D>::CreateSimplexFace( const Key<DIM_3D>& key, const Grid<DIM_3D>& grd)
{
	return SimplexFace<DIM_3D>( grd.cNode( key.cElem(0)).cPos(), grd.cNode( key.cElem(1)).cPos(), grd.cNode( key.cElem(2)).cPos() );
}

template <>
inline SimplexFace<DIM_3D> GridGeom<DIM_3D>::CreateSimplexFace( const Face<DIM_3D>& face, const Grid<DIM_3D>& grd)
{
	return SimplexFace<DIM_3D>( grd.cNode( face.cNodeId(0)).cPos(), grd.cNode( face.cNodeId(1)).cPos(), 
								grd.cNode( face.cNodeId(2)).cPos() );
}

template <>
inline SimplexFace<DIM_3D> GridGeom<DIM_3D>::CreateSimplexFace( const BndCell<DIM_3D>& cell, const BndGrid<DIM_3D>& bndgrd)
{
	return SimplexFace<DIM_3D>( bndgrd.cNode( cell.cNodeId(0)).cPos(), bndgrd.cNode( cell.cNodeId(1)).cPos(), 
								bndgrd.cNode( cell.cNodeId(2)).cPos() );
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GRIDGEOM_H__

