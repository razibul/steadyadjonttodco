#ifndef __BNDGRID_H__
#define __BNDGRID_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"

#include "libgreen/bndnode.h"
#include "libgreen/bndcell.h"
#include "libgreen/gridcontext.h"

#include "libcorecommon/fvector.h"
#include "libcoregeom/box.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <Dimension DIM>
class Master;

template <Dimension DIM>
class Generator;

template <Dimension DIM>
class ReadBndSMESH;

template <Dimension DIM>
class ReconstructorCDT;

template <Dimension DIM>
class DestroyerV2;

template <Dimension DIM>
class IOReadGreenProxy;


//////////////////////////////////////////////////////////////////////
// class BndGrid
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class BndGrid
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

	typedef typename TDefs<DIM>::GBndNode	GBndNode;
	typedef typename TDefs<DIM>::GBndCell	GBndCell;


	friend class Master<DIM_2D>;
	friend class Master<DIM_3D>;

	friend class Generator<DIM>;
	friend class ReconstructorCDT<DIM>;
	friend class DestroyerV2<DIM>;
	friend class SPointPushDestructor;

	friend class ReadBndSMESH<DIM>;
	friend class IOReadGreenProxy<DIM>;

public:
	typedef fvector<GBndNode>	ColBNode;
	typedef fvector<GBndCell>	ColBCell;

	BndGrid()		{}


	const GBndCell&		cCell( const MGSize& i) const	{ return mtabCell[i];}
	const GBndNode&		cNode( const MGSize& i) const	{ return mtabNode[i];}

	const ColBCell&		cColCell() const				{ return mtabCell;}
	const ColBNode&		cColNode() const				{ return mtabNode;}

	MGSize				SizeNodeTab() const				{ return mtabNode.size_valid();}
	MGSize				SizeCellTab() const				{ return mtabCell.size_valid();}

	const Box<DIM>		cBox() const					{ return mBox;}

	void	CreateCopy( BndGrid<DIM> &grid) const;

	void	InitBox();
	void	FindMinMax( GVect& vmin, GVect& vmax) const;
	void	RemoveDuplicatePoints();										// using MasterId
	void	RemoveDuplicatePointsPosBased( const MGFloat& zero = 1.0e-10);	// using projected pos
	
	void	FindSpacing( GridContext<GMetric>& cxmet);

	void	FindSpacing( vector<MGFloat>& tabspac) const;

	void	FindFrontPnts( vector<GVect>& tabpnt) const;

	void	FindEdges( vector< Key<2> >& tabedg) const;
	void	FindFaces( vector< Key<DIM> >& tabfac) const;

	// HACK ::
	void	FindFaces( vector< Key<DIM> >& tabfac, const MGSize& topoid) const;

protected:
	GBndCell&			rCell( const MGSize& i)			{ return mtabCell[i];}
	GBndNode&			rNode( const MGSize& i)			{ return mtabNode[i];}

	ColBCell&			rColCell()						{ return mtabCell;}
	ColBNode&			rColNode()						{ return mtabNode;}

private:
	Box<DIM>		mBox;

	ColBNode		mtabNode;
	ColBCell		mtabCell;
};



template <Dimension DIM> 
inline void BndGrid<DIM>::FindMinMax( GVect& vmin, GVect& vmax) const
{
	vmin = vmax = mtabNode[1].cPos();

	for ( MGSize i=2; i<mtabNode.size(); ++i)
	{
		for ( MGSize idim=0; idim<DIM; ++idim)
		{
			if ( vmin.cX(idim) > mtabNode[i].cPos().cX(idim) )
				vmin.rX(idim) = mtabNode[i].cPos().cX(idim);

			if ( vmax.cX(idim) < mtabNode[i].cPos().cX(idim) )
				vmax.rX(idim) = mtabNode[i].cPos().cX(idim);
		}
	}
}



//////////////////////////////////////////////////////////////////////
// class BndGrid - specialization for DIM_1D
//////////////////////////////////////////////////////////////////////
template <>
class BndGrid<DIM_1D>
{
	typedef TDefs<DIM_1D>::GVect	GVect;
	typedef TDefs<DIM_1D>::GMetric	GMetric;

	typedef TDefs<DIM_1D>::GBndNode	GBndNode;


	friend class Master<DIM_2D>;
	friend class Master<DIM_3D>;

	friend class Generator<DIM_1D>;
	friend class Generator<DIM_2D>;
	friend class Generator<DIM_3D>;
	friend class ReadBndSMESH<DIM_1D>;

public:
	BndGrid()		{}


	void	CreateCopy( BndGrid<DIM_1D> &grid) const	{ grid.mNode1 = mNode1; grid.mNode2 = mNode2; }


	void	FindMinMax( GVect& vmin, GVect& vmax) const;

	const GBndNode&		cNode1() const	{ return mNode1;}
	const GBndNode&		cNode2() const	{ return mNode2;}

	const Box<DIM_1D>	cBox() const	{ return Box<DIM_1D>();}


	const GBndNode&		cNode( const MGSize& i) const	{ return i==0 ? cNode1() : cNode2();}

	MGSize				SizeNodeTab() const				{ return 2;}
	MGSize				SizeCellTab() const				{ return 0;}


protected:
	GBndNode&			rNode1()		{ return mNode1;}
	GBndNode&			rNode2()		{ return mNode2;}

private:
	GBndNode	mNode1;
	GBndNode	mNode2;
};


inline void BndGrid<DIM_1D>::FindMinMax( GVect& vmin, GVect& vmax) const
{
	if ( mNode1.cPos().cX() < mNode2.cPos().cX() )
	{
		vmin = mNode1.cPos();
		vmax = mNode2.cPos();
	}
	else
	{
		vmin = mNode2.cPos();
		vmax = mNode1.cPos();
	}
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __BNDGRID_H__

