#include "reconstructor_cdt.h"

#include "libgreen/grid.h"
#include "libgreen/bndgrid.h"
#include "libgreen/kernel.h"
#include "libgreen/gridgeom.h"
#include "libgreen/controlspace.h"

#include "libgreen/writegrdtec.h"

#include "libcorecommon/progressbar.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



MGSize ReconstructorCDT<DIM_3D>::InsertPoint( const GVect& vct)
{
	//MGSize inod;
	//inod = mpKernel->InsertPoint( vct);

	//return inod;


	// find cell containing vct
	MGSize	icell = mpKernel->cLocal().Localize( vct);

	/////////////////////////////////////////////////////////////
	// search for the delaunay cavity of vct
	// tabccell   - cells creating a cavity for the vct
	// tabcface   - faces bounding the cavity
	// tabnewcell - new cells filling the cavity
	static vector<MGSize>	tabccell;
	static vector<GFace>	tabcface;
	static vector<MGSize>	tabnewcell;

	tabcface.reserve(60);
	tabccell.reserve(60);

	tabcface.resize(0);
	tabccell.resize(0);

	mpKernel->FindCavity( tabccell, icell, vct);
	mpKernel->CavityCorrection( tabccell, vct, false);

	if ( ! mpKernel->CavityBoundary( tabcface, tabccell ) )
	{
		// return NULL if cavity is _not_ found 
		return 0;
	}	

	// insert new point into mesh
	MGSize inod = mpKernel->InsertPoint( tabnewcell, tabccell, tabcface, vct);

	mpKernel->rGrid().UpdateNodeCellIds( tabnewcell);

	return inod;
}


void ReconstructorCDT<DIM_3D>::FindMissingEdges()
{
	cout << endl;
	cout << "mtabSplitEdges.size() = " << mtabSplitEdges.size() << endl;

	vector< Key<2> > tabedg;

	mpKernel->rGrid().FindEdges( tabedg);
	sort( tabedg.begin(), tabedg.end() );

	MGSize countMis = 0;
	MGSize countRec = 0;

	for ( MGSize i=0; i<mtabSplitEdges.size(); ++i)
	{
		if ( ! binary_search( tabedg.begin(), tabedg.end(), mtabSplitEdges[i].cEdge() ) )
			++countMis;
		else
			++countRec;
	}

	cout << "Missing   = " << countMis << endl;
	cout << "Recovered = " << countRec << endl;
	cout << "      SUM = " << countRec + countMis << endl;
	cout << endl;
}


void ReconstructorCDT<DIM_3D>::FindMissingFaces()
{
	cout << endl;
	cout << "mtabBndSubFaces.size() = " << mtabBndSubFaces.size() << endl;

	vector< Key<3> > tabfac;

	mpKernel->rGrid().FindFaces( tabfac);
	sort( tabfac.begin(), tabfac.end() );

	MGSize countMis = 0;
	MGSize countRec = 0;

	vector< Key<3> > tab;
	vector< Key<3> > tabfound;
	for ( MGSize i=0; i<mtabBndSubFaces.size(); ++i)
	{
		if ( ! binary_search( tabfac.begin(), tabfac.end(), mtabBndSubFaces[i] ) )
		{
			tab.push_back( mtabBndSubFaces[i] );
			++countMis;
		}
		else
		{
			tabfound.push_back( mtabBndSubFaces[i] );
			++countRec;
		}
	}

	if ( tab.size() > 0 )
	{
 		WriteGrdTEC<DIM_3D>	write( & mpKernel->cGrid() );
		write.WriteFaces( "cdt_missing_subfaces.dat", tab);
		write.WriteFaces( "cdt_found_subfaces.dat", tabfound);
		//write.WriteFaces( "cdt_curbnd_subfaces.dat", tabfac);
	}

	cout << "Missing   = " << countMis << endl;
	cout << "Recovered = " << countRec << endl;
	cout << "      SUM = " << countRec + countMis << endl;
	cout << endl;
}




void ReconstructorCDT<DIM_3D>::InitEdges()
{
	mpKernel->rGrid().UpdateNodeCellIds();

	mpBndGrd->FindEdges( mtabBndEdges);

	mtabSplitEdges.resize( mtabBndEdges.size() );
	for ( MGSize i=0; i<mtabBndEdges.size(); ++i)
	{
		mtabSplitEdges[i] = EdgeCDT( mtabBndEdges[i], i );
		mtabSplitEdges[i].rCellId() = mpKernel->cGrid().cNode( mtabSplitEdges[i].cEdge().cFirst() ).cCellId();
	}

	random_shuffle( mtabSplitEdges.begin(), mtabSplitEdges.end() );

	FindMissingEdges();
}


void ReconstructorCDT<DIM_3D>::FlagAcuteNodes( const MGFloat& angle)
{
	MGFloat mincos = cos( angle * M_PI / 180.0);

	vector< vector<MGSize> > tabstencil;
	vector<MGSize> tabsize;

	tabstencil.resize( mpKernel->cGrid().cColNode().size() );
	tabsize.resize( mpKernel->cGrid().cColNode().size(), 0 );

	for ( vector< Key<2> >::const_iterator itr = mtabBndEdges.begin(); itr != mtabBndEdges.end(); ++itr)
	{
		tabsize[ itr->cFirst() ] ++;
		tabsize[ itr->cSecond() ] ++;
	}

	for ( MGSize i=0; i<tabsize.size(); ++i)
		tabstencil[i].reserve( tabsize[i]);

	tabsize.clear();

	for ( vector< Key<2> >::const_iterator itr = mtabBndEdges.begin(); itr != mtabBndEdges.end(); ++itr)
	{
		tabstencil[ itr->cFirst()  ].push_back( itr->cSecond() );
		tabstencil[ itr->cSecond() ].push_back( itr->cFirst() );
	}

	mtabIsAcute.resize( mpKernel->cGrid().cColNode().size(), false);
	for ( MGSize in=0; in<tabstencil.size(); ++in)
	{
		for ( MGSize ie1=0; ie1<tabstencil[in].size(); ++ie1)
		{
			GVect ve1 = mpKernel->cGrid().cNode( tabstencil[in][ie1] ).cPos() - mpKernel->cGrid().cNode( in ).cPos();
			for ( MGSize ie2=ie1+1; ie2<tabstencil[in].size(); ++ie2)
			{
				GVect ve2 = mpKernel->cGrid().cNode( tabstencil[in][ie2] ).cPos() - mpKernel->cGrid().cNode( in).cPos();
				MGFloat acos = ve1 * ve2;
				MGFloat e1 = ve1 * ve1;
				MGFloat e2 = ve2 * ve2;

				acos /= sqrt( e1 * e2);

				if ( acos > mincos )
				{
					mtabIsAcute[in] = true;
					break;
				}
			}

			if ( mtabIsAcute[in] )
				break;
		}
	}

}



bool ReconstructorCDT<DIM_3D>::TestEdgeEncrochement( MGFloat& ccos, const Key<2>& edge, const MGSize& ipnt)
{
	GVect e1  = mpKernel->cGrid().cNode( edge.cFirst() ).cPos();
	GVect e2  = mpKernel->cGrid().cNode( edge.cSecond() ).cPos();
	GVect pnt = mpKernel->cGrid().cNode( ipnt ).cPos();

	GVect e2e1 = e2 - e1;
	GVect pe1  = pnt - e1;
	GVect pe2  = pnt - e2;

	MGFloat de2e1 = e2e1 * e2e1;
	MGFloat dpe1 = pe1 * pe1;
	MGFloat dpe2 = pe2 * pe2;

	ccos = pe1 * pe2 / sqrt( dpe1 * dpe2 );

	//if ( ccos > 1.0e-12 )
	//if ( de2e1 + 1.0e-12 < dpe1 + dpe2 )
	if ( de2e1 < dpe1 + dpe2 )
		return false;

	return true;
}


bool ReconstructorCDT<DIM_3D>::FindEdgePipeExtended( bool& brec, MGSize& inodep, vector<MGSize>& tabnodep, vector<MGSize>& tabcell, EdgeCDT& edgecdt, const MGSize& icstart)
{
	const Key<2>& edge = edgecdt.cEdge();

	MGSize iicstart = mpKernel->cGrid().cNode( edge.cFirst() ).cCellId();

	const GCell& cell = mpKernel->cGrid().cCell( edgecdt.cCellId() );

	MGSize fcount = 0;
	for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
	{
		if ( cell.cNodeId( ifc ) == edge.cSecond() )
			++fcount;

		if ( cell.cNodeId( ifc ) == edge.cFirst() )
			++fcount;
	}

	if ( fcount == 2)
	{
		//cout << "the edge is existing" << endl;
		brec = true;
		return false;
	}


	tabcell.clear();


	// finding a ball for the first node of the edge
	vector<MGSize>	tabball;
	
	if ( ! mpKernel->FindBall( tabball, edge.cFirst(), iicstart ) )	
	{
		cout << "failed to find an edge pipe [" << edge.cFirst() << ", " << edge.cSecond() << "]\n";
		TRACE( "failed to find an edge pipe [" << edge.cFirst() << ", " << edge.cSecond() << "]\n"  );
		
		return false;
	}

	
	GVect	v1 = mpKernel->cGrid().cNode( edge.cFirst() ).cPos();
	GVect	v2 = mpKernel->cGrid().cNode( edge.cSecond() ).cPos();

	// check if the edge is existing
	for ( vector<MGSize>::iterator itr = tabball.begin(); itr != tabball.end(); ++itr)
	{
		const GCell& cell = mpKernel->cGrid().cCell( *itr);
		for ( MGSize i=0; i<GCell::SIZE; ++i)
			if ( cell.cNodeId( i) == edge.cSecond() )
			{
				//cout << "the edge is existing" << endl;
				edgecdt.rCellId() = *itr;
				brec = true;
				return false;
			}
	}

	MGFloat ccosmin = 2.0;

	vector<GCell*>	tabstack;
	
	// searching for pipe starting cell/cells
	for ( vector<MGSize>::iterator itr = tabball.begin(); itr != tabball.end(); ++itr)
	{
		GCell& cell = mpKernel->rGrid().rCell( *itr);

		MGSize in;
		for ( in=0; in<GCell::SIZE; ++in)
			if ( cell.cNodeId( in) == edge.cFirst() )
				break;
				
		ASSERT( cell.cNodeId( in) == edge.cFirst() );
		ASSERT( cell.cId() == *itr);

		Key<3> kent = cell.FaceKey( in);

		// has a node intruding the edge sphere ?
		bool bIntruder = false;
		MGFloat ccos;
		for ( MGSize ifn=0; ifn<3; ++ifn)
			if ( kent.cElem(ifn) != edge.cFirst() && kent.cElem(ifn) != edge.cSecond())
				if ( TestEdgeEncrochement( ccos, edge, kent.cElem(ifn)) )
				{
					tabnodep.push_back( kent.cElem(ifn) );
					bIntruder = true;
					if ( ccos < ccosmin)
					{
						ccosmin = ccos;
						inodep = kent.cElem(ifn);
					}
					break;
				}

		if ( bIntruder )
		{
			cell.Paint();
			tabstack.push_back( &cell);
			tabcell.push_back( *itr);
		}
	}

	if ( tabstack.size() == 0)
	{
		cout << "tabstack.size() == 0" << endl;
		return false;
	}
		
	//ASSERT( tabstack.size() >= 1 && tabstack.size() <= 2);
	
//	cout << "tabstack.size = "<< tabstack.size() << "    " << std::flush;


	// searching for remaining cells
	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();

		tabstack.pop_back();

		bool bFound = false;
		for ( MGSize i=0; i <GCell::SIZE; ++i)
			if ( cell.cNodeId( i) == edge.cSecond() )
			{
				//cout << "found\n";
				bFound = true;
			}
		
		if ( bFound)
			continue;



		//bFound = false;
		//TIndex in;
		//for ( in=0; in<Cell::SIZE; ++in)
		//	if ( cell.cNodeId( in) == edge.cFirst() )
		//	{
		//		bFound = true;
		//		break;
		//	}

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			//if ( bFound && ifc != in)
			//	continue;
				
			MGSize ic = cell.cCellId( ifc).first;
			
			if ( ic )
			{
				GCell &cellnei = mpKernel->rGrid().rCell( ic);
				
				if ( cellnei.IsPainted() )
					continue;

				Key<3> kent = cell.FaceKey( ifc);

				// has a node intruding the edge sphere ?
				bool bIntruder = false;
				MGFloat ccos;
				for ( MGSize ifn=0; ifn<3; ++ifn)
					if ( kent.cElem(ifn) != edge.cFirst() && kent.cElem(ifn) != edge.cSecond())
						if ( TestEdgeEncrochement( ccos, edge, kent.cElem(ifn))  )
						{
							bIntruder = true;
							tabnodep.push_back( kent.cElem(ifn) );
							if ( ccos < ccosmin)
							{
								ccosmin = ccos;
								inodep = kent.cElem(ifn);
							}
							break;
						}

				if ( bIntruder )
				{
					cellnei.Paint();
					tabstack.push_back( &cellnei);
					tabcell.push_back( ic);
				}
		
			}
		}	
	}
	
	// cleaning cell flags
	for ( vector<MGSize>::iterator itrp=tabcell.begin(); itrp!=tabcell.end(); ++itrp)
		mpKernel->rGrid().rCell( *itrp).WashPaint();

	//TRACE( "FindEdgPipe finished");

	sort( tabnodep.begin(), tabnodep.end() );
	tabnodep.erase( unique( tabnodep.begin(), tabnodep.end() ), tabnodep.end() );

	return true;
}



//void ReconstructorCDT<DIM_3D>::FindGuiltyPoint( MGSize& inod, const Key<2>& edge)
//{
//
//	GVect ve1( mpKernel->cGrid().cNode( edge.cFirst() ).cPos() );
//	GVect ve2( mpKernel->cGrid().cNode( edge.cSecond() ).cPos() );
//
//	GVect vmid = 0.5 * (ve1+ve2);
//
//	vector<MGSize> tabcell;
//	vector<MGSize> tabnode;
//
//	MGSize inodep;
//	
//	bool bRec = false;
//	bool bOk;// = FindEdgePipeExtended( bRec, inodep, tabnode, tabcell, edge, 0);
//	if ( bRec)
//		return;
//
//	if ( ! bOk)
//	{
//		cout << "Failed to find edge cavity " << edge.cFirst() << " " << edge.cSecond() << endl;
//	}
//
//	GVect vde  = ve2 - ve1;
//	GVect vsum = ve2 + ve1;
//	MGFloat c2 = vde * vde;
//
//
//	MGSize intest = 0;
//	MGFloat mincos = 0.0;
//
//	MGSize ncount = 0;
//
//	for ( MGSize i=0; i<tabnode.size(); ++i)
//	{
//		MGSize idnode = tabnode[i];
//
//		GVect vp( mpKernel->cGrid().cNode( idnode ).cPos() );
//
//		GVect vpe1 = vp - ve1;
//		GVect vpe2 = vp - ve2;
//
//		MGFloat pe1 = vpe1 * vpe1;
//		MGFloat pe2 = vpe2 * vpe2;
//
//		MGFloat curcos = vpe1 * vpe2 / sqrt( pe1 * pe2);
//
//
//		GVect vdp = 2.0 * vp - vsum;
//
//		MGFloat dp2 = vdp * vdp;;
//
//		MGFloat left  = c2 + 1.0e-12;
//		MGFloat right = pe1 + pe2;
//
//
//		if ( left >= right )
//		{
//			++ncount;
//
//			if ( curcos < mincos )
//			{
//				mincos = curcos;
//				intest = idnode;
//			}
//		}
//	}
//
//	//if ( ncount != tabnode.size() )
//	//	cout << tabnode.size() << " / " << ncount << endl;
//
//	//cout << tabnode2.size() << " / " << ncount << endl;
//
//	inod = intest;
//
//	//if ( inod == 0 )
//	//	THROW_INTERNAL( "ReconstructorCDT<T,DIM_3D>::FindGuilyPoint -- failed !!!");
//}


void ReconstructorCDT<DIM_3D>::FindSplitPoint_T1( GVect& vct, const Key<2>& edge, const MGSize& ipnt)
{
	GVect ve1( mpKernel->cGrid().cNode( edge.cFirst() ).cPos() );
	GVect ve2( mpKernel->cGrid().cNode( edge.cSecond() ).cPos() );
	GVect vp( mpKernel->cGrid().cNode( ipnt ).cPos() );

	GVect ve21 = ve2 - ve1;
	MGFloat e21 = ve21.module();

	GVect vpe1 = ve1 - vp;
	MGFloat pe1 = vpe1.module();

	GVect vpe2 = ve2 - vp;
	MGFloat pe2 = vpe2.module();

	GVect vprop;

	if ( pe1 < 0.5 * e21 )
	{
		vprop = ve1 + ve21.versor() * pe1;
	}
	else
	if ( pe2 < 0.5 * e21 )
	{
		vprop = ve2 - ve21.versor() * pe2;
	}
	else
	{
		vprop = 0.5 * ( ve1 + ve2 );
	}

	vct = GVect( vprop);

}

bool ReconstructorCDT<DIM_3D>::FindSplitPoint_T2( GVect& vct, const Key<2>& edge, const MGSize& ipnt, const MGSize& idnacu)
{
	GVect ve1( mpKernel->cGrid().cNode( edge.cFirst() ).cPos() );
	GVect ve2( mpKernel->cGrid().cNode( edge.cSecond() ).cPos() );
	GVect vek( mpKernel->cGrid().cNode( idnacu ).cPos() );
	GVect vp( mpKernel->cGrid().cNode( ipnt ).cPos() );

	GVect vprop;

	GVect ve1k = vek - ve1;
	GVect ve2k = vek - ve2;

	if ( ve2k.module() < ve1k.module() )
	{
		vprop = ve1;
		ve1 = ve2;
		ve2 = vprop;
	}

	//
	MGFloat lref = (ve2 - ve1).module();
	//MGFloat dr = (MGFloat)rand() / (MGFloat)RAND_MAX * 1.0e-7 * lref;
	MGFloat dr = (MGFloat)rand() / (MGFloat)RAND_MAX * 1.0e-4 * lref;

//	if ( idnacu == edge.cFirst() || idnacu == edge.cSecond() )
//		dr = 0.0;
	//

	// rule 2

	vprop = vek + (ve2 - vek).versor() * ((vp - vek).module() + dr);

	if ( (ve2 - vprop).module() >= (vp - vprop).module() )
	{
		vct = GVect( vprop);
		return true;
	}

	// rule 3

	GVect ve1v = ve1 - vprop;
	MGFloat e1v = ve1v.module();

	MGFloat r;
	if ( (vp - vprop).module() < 0.5 * (ve1 - vprop).module() )
		r = (vek - ve1).module() + (ve1 - vprop).module() - (vp - vprop).module();
	else
		r = (vek - ve1).module() + 0.5*(ve1 - vprop).module();

	vprop = vek + (ve2 - vek).versor() * (r + dr);

	vct = GVect( vprop);

	return true;
}


MGSize ReconstructorCDT<DIM_3D>::SplitEdges()
{
	vector<EdgeCDT>		tabedge;
	tabedge.clear();	// ???

	mtabSplitEdges.swap( tabedge);

	ProgressBar	bar(40);
	bar.Init( tabedge.size() );
	bar.Start();

	MGSize n=0;
	MGSize nsplit=0;
	for ( vector<EdgeCDT>::iterator itr = tabedge.begin(); itr != tabedge.end(); ++itr, ++bar)
	{
		MGSize inod=0;
		Key<2> edge( itr->cEdge() );

		vector<MGSize> tabcell;
		vector<MGSize> tabnode;

		bool bRec = false;
		bool bOk = FindEdgePipeExtended( bRec, inod, tabnode, tabcell, *itr, 0);
		if ( bRec)
		{
			mtabSplitEdges.push_back( *itr );
			continue;
		}

		if ( ! bOk)
		{
			cout << "Failed to find edge cavity " << edge.cFirst() << " " << edge.cSecond() << endl;
		}

		if ( itr->IsNew() ) // new edge
		{
			if ( mtabIsAcute[ itr->cEdge().cFirst() ] && mtabIsAcute[ itr->cEdge().cSecond() ] ) // split in two edges of type-2
			{
				GVect vct = 0.5 * ( mpKernel->cGrid().cNode( itr->cEdge().cFirst() ).cPos() + mpKernel->cGrid().cNode( itr->cEdge().cSecond() ).cPos() );
				inod = InsertPoint( vct);

				GMetric met = mpCSpace->GetSpacing( vct);	// TODO :: Expensive !!!
				mpKernel->rGenData().InsertMetric( met, inod);


				if ( inod == 0 )
					THROW_INTERNAL( "ReconstructorCDT<DIM_3D>::SplitEdges() -- point insertion failed !!!");

				Key<2> e1( inod, itr->cEdge().cFirst() );
				e1.Sort();

				Key<2> e2( inod, itr->cEdge().cSecond() );
				e2.Sort();

				mtabSplitEdges.push_back( EdgeCDT( e1, itr->cBndEdgeId(), 2, itr->cEdge().cFirst() ) );
				mtabSplitEdges[mtabSplitEdges.size()-1].rCellId() = mpKernel->cGrid().cNode( e1.cFirst() ).cCellId();

				mtabSplitEdges.push_back( EdgeCDT( e2, itr->cBndEdgeId(), 2, itr->cEdge().cSecond() ) );
				mtabSplitEdges[mtabSplitEdges.size()-1].rCellId() = mpKernel->cGrid().cNode( e2.cFirst() ).cCellId();

				++nsplit;
			}
			else
			if ( mtabIsAcute[ itr->cEdge().cFirst() ] ) // type-2 edge
			{
				mtabSplitEdges.push_back( EdgeCDT( itr->cEdge(), itr->cBndEdgeId(), 2, itr->cEdge().cFirst() ) );
			}
			else
			if ( mtabIsAcute[ itr->cEdge().cSecond() ] ) // type-2 edge
			{
				mtabSplitEdges.push_back( EdgeCDT( itr->cEdge(), itr->cBndEdgeId(), 2, itr->cEdge().cSecond() ) );
			}
			else // type-1 edge
			{
				mtabSplitEdges.push_back( EdgeCDT( itr->cEdge(), itr->cBndEdgeId(), 1, 0 ) );
			}
		}
		else
		{
			//FindGuiltyPoint( inod, edge);

			if ( inod == 0 )	// should never happen
			{
				++n;
				mtabSplitEdges.push_back( *itr );
				continue;
			}

			GVect vct;

			if ( itr->cType() == 1 )
			{
				FindSplitPoint_T1( vct, edge, inod);
			}
			else
			if ( itr->cType() == 2 )
			{
				FindSplitPoint_T2( vct, edge, inod, itr->cIdAcuNode() );
			}
			else
				THROW_INTERNAL( " ReconstructorCDT<T,DIM_3D>::SplitEdges -- failed to find split point !!!" );

			// insert the split point
			MGSize id = InsertPoint( vct);

			GMetric met = mpCSpace->GetSpacing( vct);	// TODO :: Expensive !!!
			mpKernel->rGenData().InsertMetric( met, id);

			if ( id == 0 )
				THROW_INTERNAL( "ReconstructorCDT<T,DIM_3D>::SplitEdges() -- point insertion failed !!!");


			Key<2> e1( id, itr->cEdge().cFirst() );
			e1.Sort();

			Key<2> e2( id, itr->cEdge().cSecond() );
			e2.Sort();

			mtabSplitEdges.push_back( EdgeCDT( e1, itr->cBndEdgeId(), itr->cType(), itr->cIdAcuNode() ) );
			mtabSplitEdges[mtabSplitEdges.size()-1].rCellId() = mpKernel->cGrid().cNode( e1.cFirst() ).cCellId();

			mtabSplitEdges.push_back( EdgeCDT( e2, itr->cBndEdgeId(), itr->cType(), itr->cIdAcuNode() ) );
			mtabSplitEdges[mtabSplitEdges.size()-1].rCellId() = mpKernel->cGrid().cNode( e2.cFirst() ).cCellId();

			++nsplit;
		}

	}

	bar.Finish();

	//cout << "n = " << n << endl;
	
	random_shuffle( mtabSplitEdges.begin(), mtabSplitEdges.end() );

	return nsplit;
}

void ReconstructorCDT<DIM_3D>::Init()
{
}


void ReconstructorCDT<DIM_3D>::InitCompEdges()
{
	vector<MGSize>	tabsize( mtabBndEdges.size(), 0);

	for ( vector<EdgeCDT>::const_iterator itr = mtabSplitEdges.begin(); itr != mtabSplitEdges.end(); ++itr)
		++( tabsize[ itr->cBndEdgeId() ] );


	mtabCompEdge.resize( mtabBndEdges.size() );

	for ( MGSize id=0; id<mtabBndEdges.size(); ++id)
	{
		mtabCompEdge[id].first = mtabBndEdges[id];
		mtabCompEdge[id].second.reserve( tabsize[id] );
	}

	for ( vector<EdgeCDT>::const_iterator itr = mtabSplitEdges.begin(); itr != mtabSplitEdges.end(); ++itr)
		mtabCompEdge[ itr->cBndEdgeId() ].second.push_back( itr->cEdge() );

	sort( mtabCompEdge.begin(), mtabCompEdge.end() );

	MGSize n = 0;

	for ( MGSize id=0; id<mtabCompEdge.size(); ++id)
	{
		if ( mtabCompEdge[id].second.size() == 0)
			THROW_INTERNAL( "missing boundary edge " << id);

		n += mtabCompEdge[id].second.size();
	}

	cout << "number of subedges in mtabCompEdge = " << n << endl;
}



void ReconstructorCDT<DIM_3D>::InitFaces()
{
	mpBndGrd->FindFaces( mtabBndFaces);
	{
 		WriteGrdTEC<DIM_3D>	write( & mpKernel->cGrid() );
		write.WriteFaces( "cdt_mtabBndFaces.dat", mtabBndFaces);
	}

	vector<MGSize>		tabn;
	vector< Key<2> >	tabe;

	mtabSplitFaces.resize( mtabBndFaces.size() );
	for ( MGSize i=0; i<mtabBndFaces.size(); ++i)
	{
		Key<3> fkey = mtabBndFaces[i];

		//{
		//	Key<3> kk = fkey;
		//	kk.Sort();
		//	if ( kk == Key<3>( 1811, 4380, 8908 ) )
		//		cout << " FOUND - Key<3>( 1811, 4380, 8908 ) :: i = " << i << endl;
		//}

		tabn.clear();
		tabe.clear();

		mtabSplitFaces[i] = FaceCDT( mtabBndFaces[i], this );
		//mtabSplitFaces[i] = FaceCDT();

		for ( MGSize ie=0; ie<3; ++ie)
		{
			Key<2> ekey( fkey.cFirst(), fkey.cSecond() );
			ekey.Sort();

			vector< KeyData< Key<2>, vector< Key<2> > > >::iterator itr = lower_bound( mtabCompEdge.begin(), mtabCompEdge.end(), KeyData< Key<2>, vector< Key<2> > >( ekey) );

			if ( itr->first != ekey )
				THROW_INTERNAL( "ReconstructorCDT<DIM_3D>::InitFaces() -- boundary edge not found !!!");

			if ( (itr+1) != mtabCompEdge.end() )
				if ( (itr+1)->first == ekey )
					THROW_INTERNAL( "ReconstructorCDT<DIM_3D>::InitFaces() -- duplicate boundary edge found !!!");

			for ( MGSize ie=0; ie<itr->second.size(); ++ie)
			{
				tabe.push_back( itr->second[ie] );
				if ( itr->second[ie].cFirst() != ekey.cFirst() && itr->second[ie].cFirst() != ekey.cSecond() )
					tabn.push_back( itr->second[ie].cFirst() );

				if ( itr->second[ie].cSecond() != ekey.cFirst() && itr->second[ie].cSecond() != ekey.cSecond() )
					tabn.push_back( itr->second[ie].cSecond() );
			}

			fkey.Rotate( 1);
		}

		sort( tabn.begin(), tabn.end() );
		tabn.erase( unique( tabn.begin(), tabn.end() ), tabn.end() ); 

		mtabSplitFaces[i].InitTabInNodes( tabn);
		mtabSplitFaces[i].InitTabBEdges( tabe);
	}

	random_shuffle( mtabSplitFaces.begin(), mtabSplitFaces.end() );
	 
	//
	//vector< Key<3> > tabfac;
	//mpKernel->rGrid().FindFaces( tabfac);
	//sort( tabfac.begin(), tabfac.end() );


}


//bool ReconstructorCDT<DIM_3D>::FindFacePipeExtended( FaceCDT& facecdt)
//{
//	vector<SubFaceCDT> tabsubface;
//	vector< Key<2> > tabcrossedge;
//
//	vector<MGSize> tabpipe;
//	vector<MGSize> tabcell;
//
//	bool brec = false;
//	const Key<3>& face = facecdt.cFace();
//
//	// array with all boundary nodes
//	vector<MGSize> tabebnode;
//	tabebnode.reserve( facecdt.cTabInNodes().size() + 3);
//
//	tabebnode.resize( facecdt.cTabInNodes().size() ); // ???
//	copy( facecdt.cTabInNodes().begin(), facecdt.cTabInNodes().end(), tabebnode.begin() );
//	tabebnode.push_back( face.cFirst() );
//	tabebnode.push_back( face.cSecond() );
//	tabebnode.push_back( face.cThird() );
//
//	sort( tabebnode.begin(), tabebnode.end() );
//
//
//
//	GVect vf1 = mpKernel->cGrid().cNode( face.cFirst() ).cPos();
//	GVect vf2 = mpKernel->cGrid().cNode( face.cSecond() ).cPos();
//	GVect vf3 = mpKernel->cGrid().cNode( face.cThird() ).cPos();
//
//	SimplexFace<DIM_3D> tri( vf1, vf2, vf3 );
//
//	MGSize iicstart = mtabNodeCellId[ face.cFirst() ];
//
//	tabcell.clear();
//
//	// finding a ball for the first node of the face
//	vector<MGSize>	tabball;
//	
//	if ( ! mpKernel->FindBall( tabball, face.cFirst(), iicstart ) )	
//	{
//		cout << "failed to find an edge pipe [" << face.cFirst() << ", " << face.cSecond() << ", " << face.cThird() << "]\n";
//		TRACE( "failed to find an edge pipe [" << face.cFirst() << ", " << face.cSecond() << ", " << face.cThird() << "]\n"  );
//		
//		return false;
//	}
//
//
//	// check if the face is existing
//	for ( vector<MGSize>::iterator itr = tabball.begin(); itr != tabball.end(); ++itr)
//	{
//		const GCell& cell = mpKernel->cGrid().cCell( *itr);
//		for ( MGSize i=0; i<GCell::SIZE; ++i)
//		{
//			Key<3> kface = cell.FaceKey(i);
//			kface.Sort();
//			if ( kface == face )
//			{
//				//cout << "the face is existing" << endl;
//				brec = true;
//				return false;
//			}
//		}
//	}
//
//	vector<GCell*>	tabstack;
//	
//	// searching for pipe starting cell/cells
//	for ( vector<MGSize>::iterator itr = tabball.begin(); itr != tabball.end(); ++itr)
//	{
//		GCell& cell = mpKernel->rGrid().rCell( *itr);
//		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, mpKernel->cGrid() );
//		
//		MGSize in;
//		for ( in=0; in<=GCell::SIZE; ++in)
//			if ( cell.cNodeId( in) == face.cFirst() )
//				break;
//				
//		ASSERT( cell.cNodeId( in) == face.cFirst() );
//		ASSERT( cell.cId() == *itr);
//		
//
//		for ( MGSize ie=0; ie<GCell::ESIZE; ++ie)
//		{
//			MGSize	idn1 = cell.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,0) );
//			MGSize	idn2 = cell.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,1) );
//
//			GVect	ve1 = mpKernel->cGrid().cNode( idn1 ).cPos();
//			GVect	ve2 = mpKernel->cGrid().cNode( idn2 ).cPos();
//
//			//if ( idn1 == face.cFirst() || idn2 == face.cFirst() )
//			//	continue;
//
//			if ( (binary_search( tabebnode.begin(), tabebnode.end(), idn1 ) && binary_search( tabebnode.begin(), tabebnode.end(), idn2 ) )
//				 || (tri.CrossedVol( ve1, ve2) > 0.0 ) )
//			{
//				cell.Paint();
//				tabstack.push_back( &cell);
//				tabcell.push_back( *itr);
//				break;
//			}
//		}
//	}
//
//
//	// searching for remaining cells
//	while ( tabstack.size() )
//	{
//		GCell &cell = *tabstack.back();
//		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, mpKernel->cGrid() );
//
//		tabstack.pop_back();
//
//
//		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
//		{
//			MGSize ic = cell.cCellId( ifc).first;
//			
//			if ( ic )
//			{
//				GCell &cellnei = mpKernel->rGrid().rCell( ic);
//				
//				if ( cellnei.IsPainted() )
//					continue;
//
//				for ( MGSize in=0; in<GCell::SIZE; ++in)
//					if ( binary_search( tabebnode.begin(), tabebnode.end(), cellnei.cNodeId(in) ) )
//					{
//						cellnei.Paint();
//						tabstack.push_back( &cellnei);
//						tabcell.push_back( ic);
//						break;
//					}
//
//				if ( cellnei.IsPainted() )
//					continue;
//
//				for ( MGSize ie=0; ie<GCell::ESIZE; ++ie)
//				{
//					GVect	ve1 = mpKernel->cGrid().cNode( cellnei.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,0) ) ).cPos();
//					GVect	ve2 = mpKernel->cGrid().cNode( cellnei.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,1) ) ).cPos();
//
//					if ( tri.CrossedVol( ve1, ve2) > 0.0 )
//					{
//						cellnei.Paint();
//						tabstack.push_back( &cellnei);
//						tabcell.push_back( ic);
//						break;
//					}
//				}
//					
//			}
//		}	
//	}
//
//	// cleaning cell flags
//	for ( vector<MGSize>::iterator itrp=tabcell.begin(); itrp!=tabcell.end(); ++itrp)
//		mpKernel->rGrid().rCell( *itrp).WashPaint();
//
//
//	// filter cells
//	for ( MGSize ic=0; ic<tabcell.size(); ++ic)
//	{
//		const GCell &cell = mpKernel->cGrid().cCell( tabcell[ic] );
//
//		bool bcont = false;
//		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
//		{
//			Key<3> key = cell.FaceKey( ifc);
//			key.Sort();
//
//			if ( binary_search( tabebnode.begin(), tabebnode.end(), key.cElem(0) ) &&
//				 binary_search( tabebnode.begin(), tabebnode.end(), key.cElem(1) ) &&
//				 binary_search( tabebnode.begin(), tabebnode.end(), key.cElem(2) ) )
//			{
//				tabpipe.push_back( tabcell[ic] );
//				tabsubface.push_back( SubFaceCDT( key, 0, tabcell[ic] ) );
//
//				bcont = true;
//				break;
//			}
//		}
//
//		if ( bcont)
//			continue;
//
//		for ( MGSize ie=0; ie<GCell::ESIZE; ++ie)
//		{
//			MGSize in1 = cell.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,0) );
//			MGSize in2 = cell.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,1) );
//
//			if ( binary_search( tabebnode.begin(), tabebnode.end(), in1 ) ||
//				 binary_search( tabebnode.begin(), tabebnode.end(), in2 ) )
//				 continue;
//
//			GVect	ve1 = mpKernel->cGrid().cNode( in1 ).cPos();
//			GVect	ve2 = mpKernel->cGrid().cNode( in2 ).cPos();
//
//			if ( tri.CrossedVol( ve1, ve2) > 0.0 )
//			{
//				tabpipe.push_back( tabcell[ic] );
//				Key<2> ke( in1, in2);
//				ke.Sort();
//				tabcrossedge.push_back( ke);
//				break;
//			}
//		}
//
//	}
//
//	sort( tabsubface.begin(), tabsubface.end() );
//	tabsubface.erase( unique( tabsubface.begin(), tabsubface.end() ), tabsubface.end() );
//
//	sort( tabcrossedge.begin(), tabcrossedge.end() );
//	tabcrossedge.erase( unique( tabcrossedge.begin(), tabcrossedge.end() ), tabcrossedge.end() );
//
//	if ( tabcrossedge.size() > 0 )
//	{
//		cout << "tabcrossedge.size = "<< tabcrossedge.size() << endl;
//
// 		ofstream	f( "_face_.plt");
// 		f << "VARIABLES = \"X\", \"Y\", \"Z\"\n";
//		f << "ZONE T=\"" << "face" << "\", N=" << 3 << ", E=" << 1 << ", F=FEPOINT, ET=TRIANGLE\n";
// 		f << vf1.cX() << " " << vf1.cY() << " " << vf1.cZ()  << endl;
// 		f << vf2.cX() << " " << vf2.cY() << " " << vf2.cZ()  << endl;
// 		f << vf3.cX() << " " << vf3.cY() << " " << vf3.cZ()  << endl;
//		f << "1 2 3" << endl;
// 
// 		WriteGrdTEC<DIM_3D>	write( &mpKernel->cGrid() );
// 		write.WriteCells( "_pipe_.plt", tabpipe);
//	}
//
//	//if ( tabpipe.size() == 0)
//	//	return false;
//
//	return true;
//}



bool ReconstructorCDT<DIM_3D>::FindSubFacePipe( bool& brec, vector<MGSize>& tabcell, Key<3>& facecdt, const MGSize& icell)
{
	brec = false;
	const Key<3>& face = facecdt; //.cFace();

	tabcell.clear();


	/////////////////////////////////////////////////////////////
	// finding a ball for the first node of the face
	vector<MGSize>	tabball;

	SimplexFace<DIM_3D> tri( mpKernel->cGrid().cNode( face.cFirst() ).cPos(), mpKernel->cGrid().cNode( face.cSecond() ).cPos(), mpKernel->cGrid().cNode( face.cThird() ).cPos() );

	
	if ( ! mpKernel->FindBall( tabball, face.cFirst(), icell ) )	
	{
		cout << "failed to find an face pipe [" << face.cFirst() << ", " << face.cSecond() << ", " << face.cThird() << " ]" << endl;
		return false;
	}

	
	//cout << "tabball.size = "<< tabball.size() << "    " << std::flush;


	/////////////////////////////////////////////////////////////
	// check if the face is existing
	for ( vector<MGSize>::iterator itr = tabball.begin(); itr != tabball.end(); ++itr)
	{
		const GCell& cell = mpKernel->cGrid().cCell( *itr);

		MGSize nfound = 0;
		for ( MGSize i=0; i<=GCell::SIZE; ++i)
			if ( cell.cNodeId( i) == face.cSecond() || cell.cNodeId( i) == face.cThird() )
				++nfound;

		if ( nfound == 2)
		{
			brec = true;
			return false;
		}
	}


	//cout << "tabball.size = "<< tabball.size() << endl;

	vector<GCell*>	tabstack;
	
	/////////////////////////////////////////////////////////////
	// searching for pipe starting cell/cells
	for ( vector<MGSize>::iterator itr = tabball.begin(); itr != tabball.end(); ++itr)
	{
		GCell& cell = mpKernel->rGrid().rCell( *itr);
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, mpKernel->cGrid() );
		
		MGSize in;
		for ( in=0; in<=GCell::SIZE; ++in)
			if ( cell.cNodeId( in) == face.cFirst() )
				break;
				
		ASSERT( cell.cNodeId( in) == face.cFirst() );
		ASSERT( cell.cId() == *itr);
		

		for ( MGSize ie=0; ie<GCell::ESIZE; ++ie)
		{
			MGSize	idn1 = cell.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,0) );
			MGSize	idn2 = cell.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,1) );

			if ( idn1 == face.cFirst() || idn2 == face.cFirst() )
				continue;

			GVect	ve1 = mpKernel->cGrid().cNode( idn1 ).cPos();
			GVect	ve2 = mpKernel->cGrid().cNode( idn2 ).cPos();

			//if ( tri.CrossedVol( ve1, ve2) > 0.0 )
			if ( tri.CrossedVol( ve1, ve2) >= 0.0 )
			{
				cell.Paint();
				tabstack.push_back( &cell);
				tabcell.push_back( *itr);
				break;
			}
		}
	}

	//cout << "tabstack.size = "<< tabstack.size() << endl;

	if ( tabstack.size() == 0)
		return false;
		
	//ASSERT( tabstack.size() >= 1 && tabstack.size() <= 2);
	


	/////////////////////////////////////////////////////////////
	// searching for remaining cells
	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, mpKernel->cGrid() );

		tabstack.pop_back();


		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;
			
			if ( ic )
			{
				GCell &cellnei = mpKernel->rGrid().rCell( ic);
				
				if ( cellnei.IsPainted() )
					continue;

				for ( MGSize ie=0; ie<GCell::ESIZE; ++ie)
				{
					GVect	ve1 = mpKernel->cGrid().cNode( cellnei.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,0) ) ).cPos();
					GVect	ve2 = mpKernel->cGrid().cNode( cellnei.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,1) ) ).cPos();

					//if ( tri.CrossedVol( ve1, ve2) > 0.0 )
					if ( tri.CrossedVol( ve1, ve2) >= 0.0 )
					{
						cellnei.Paint();
						tabstack.push_back( &cellnei);
						tabcell.push_back( ic);
						break;
					}
				}
					
			}
		}	

	}

	//cout << "tabcell.size = "<< tabcell.size() << endl;

	
	/////////////////////////////////////////////////////////////
	// cleaning cell flags
	for ( vector<MGSize>::iterator itrp=tabcell.begin(); itrp!=tabcell.end(); ++itrp)
		mpKernel->rGrid().rCell( *itrp).WashPaint();

	return true;
}


//void ReconstructorCDT<DIM_3D>::GenerateFaceGrid( FaceCDT& facecdt)
//{
//	Grid<DIM_3D>			grid;
//	GeneratorData<DIM_3D>	data;
//	Kernel<DIM_3D>			kernel(grid,data);
//
//	const Key<3>& face = facecdt.cFace();
//
//	GVect vf1 = mpKernel->cGrid().cNode( face.cFirst() ).cPos();
//	GVect vf2 = mpKernel->cGrid().cNode( face.cSecond() ).cPos();
//	GVect vf3 = mpKernel->cGrid().cNode( face.cThird() ).cPos();
//
//	GVect vmin, vmax;
//	vmin = vmax = vf1;
//
//	for ( MGSize i=1; i<3; ++i)
//	{
//		GVect vf = mpKernel->cGrid().cNode( face.cElem(i) ).cPos();
//		vmin = Minimum( vf, vmin);
//		vmax = Maximum( vf, vmax);
//	}
//
//
//	kernel.InitBoundingBox( vmin, vmax);
//	kernel.InitCube();
//
//	for ( MGSize i=0; i<3; ++i)
//	{
//		GVect vct = mpKernel->cGrid().cNode( face.cElem(i) ).cPos();
//		MGSize inod = kernel.InsertPoint( vct );
//
//		if ( ! inod)
//			THROW_INTERNAL( "CRASH :: kernel failed to insert new point with coordinates [" << vct.cX() << ", " << vct.cY() << ", " << vct.cZ() << "]" );
//
//		grid.rNode(inod).rMasterId() = face.cElem(i);
//	}
//
//	for ( MGSize i=0; i<facecdt.cTabInNodes().size(); ++i)
//	{
//		GVect vct = mpKernel->cGrid().cNode( facecdt.cTabInNodes()[i] ).cPos();
//		MGSize inod = kernel.InsertPoint( vct );
//
//		if ( ! inod)
//			THROW_INTERNAL( "CRASH :: kernel failed to insert new point with coordinates [" << vct.cX() << ", " << vct.cY() << ", " << vct.cZ() << "]" );
//
//		grid.rNode(inod).rMasterId() = facecdt.cTabInNodes()[i];
//	}
//
//	vector< Key<3> > tabbface;
//
//	for ( Grid<DIM_3D>::ColCell::const_iterator itc=grid.cColCell().begin(); itc!= grid.cColCell().end(); ++itc)
//	{
//		const GCell& cell = *itc;
//
//		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
//		{
//			Key<3> face = cell.FaceKey( ifc);
//			face.Sort();
//
//			bool bok = true;
//			for ( MGSize in=0; in<3; ++in)
//				if ( face.cElem(in) <= 8)
//				{
//					bok = false;
//					break;
//				}
//
//			if ( bok)
//				tabbface.push_back( face);
//
//		}
//	}
//
//	sort( tabbface.begin(), tabbface.end() );
//	tabbface.erase( unique( tabbface.begin(), tabbface.end() ), tabbface.end() );
//
//	//WriteGrdTEC<DIM_3D> wtec( &grid);
//	//wtec.WriteFaces( "subfaces.dat", tabbface);
//
//	for ( MGSize i=0; i<tabbface.size(); ++i)
//	{
//		Key<3> key( grid.cNode( tabbface[i].cFirst() ).cMasterId(), grid.cNode( tabbface[i].cSecond() ).cMasterId(), grid.cNode( tabbface[i].cThird() ).cMasterId() );
//		key.Sort();
//
//		MGSize ic = mtabNodeCellId[ key.cFirst() ];
//
//		bool brec = false;
//		vector<MGSize> tabpipe;
//
//		FindSubFacePipe( brec, tabpipe, key, ic);
//
//		if ( ! brec)
//		{
//			if ( tabpipe.size() == 0 )
//				cout << "tabpipe.size() == 0" << endl;
//
//			mtabSubFacesMissing.push_back( key);
//		}
//
//	}
//
//	//wtec.DoWrite( "face_grid.plt");
//
//}



void ReconstructorCDT<DIM_3D>::SearchForDegeneracies()
{
	MGSize countSphere = 0;
	MGSize countZero = 0;
	MGFloat volmin = 1.0;

	vector<GCell*> tabcell;

	for ( Grid<DIM_3D>::ColCell::iterator itrc = mpKernel->rGrid().rColCell().begin(); itrc != mpKernel->rGrid().rColCell().end(); ++itrc)
	{
		GCell& cell = *itrc;
		if ( cell.IsPainted() )
			continue;

		Simplex<DIM_3D>	tet = GridGeom<DIM_3D>::CreateSimplex( cell, mpKernel->cGrid() );

		MGFloat vol = tet.Volume();
		if ( vol < 1.0e-15 ) 
			++countZero;

		if ( vol < volmin || itrc == mpKernel->rGrid().rColCell().begin() )
			volmin = vol;

		vector<GCell*>	tabstack;

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			MGSize ic = cell.cCellId( k).first;

			if ( ic)
			{
				GCell& cellnext = mpKernel->rGrid().rCell( ic );;

				GVect vct = mpKernel->cGrid().cNode( cellnext.cNodeId( cell.cCellId(k).second ) ).cPos();

				if ( tet.IsInsideSphere( vct) >= 0 ) 
				{
					tabstack.push_back( &cellnext);
					tabstack.push_back( &cell);

					cellnext.Paint();
					tabcell.push_back( &cellnext);
				
					cell.Paint();
					tabcell.push_back( &cell);

					++countSphere;
					break;
				}
			}
		}

		while ( tabstack.size() )
		{
			GCell &cell = *tabstack.back();

			tabstack.pop_back();

			Simplex<DIM_3D>	tet = GridGeom<DIM_3D>::CreateSimplex( cell, mpKernel->cGrid() );

			for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
			{
				MGSize ic = cell.cCellId( ifc).first;

				if ( ic)
				{
					GCell& cellnext = mpKernel->rGrid().rCell( ic );

					if ( cellnext.IsPainted() )
						continue;

					GVect vct = mpKernel->cGrid().cNode( cellnext.cNodeId( cell.cCellId(ifc).second ) ).cPos();

					if ( tet.IsInsideSphere( vct) >= 0 ) 
					{
						tabstack.push_back( &cellnext);

						cellnext.Paint();
						tabcell.push_back( &cellnext);
					}
				}
			}
		}

	}

	for ( vector<GCell*>::iterator itrp=tabcell.begin(); itrp!=tabcell.end(); ++itrp)
		(*itrp)->WashPaint();


	cout << "no of degeneracies (in sphere)= " << countSphere << endl;
	cout << "no of degeneracies (zero vol) = " << countZero << endl;
	cout << "volmin = " << volmin << endl;
	cout << endl;
}


void ReconstructorCDT<DIM_3D>::Reconstruct()
{
	//////////////////////////////////////////////////////////////////////
	// edges
	InitEdges();
	FlagAcuteNodes( 60.0);

	ProgressBar	bar(40);

	bar.Init( 10, false );
	bar.Start();

	MGSize count = 0;
	MGSize nsplit;
	do
	{
		mpKernel->rGrid().UpdateNodeCellIds();

		nsplit = SplitEdges();
		++count;
	}
	while( count < 200 &&  nsplit != 0 );

	cout << "no of passes = " << count << endl << endl;

	bar.Finish();

	InitCompEdges();

	FindMissingEdges();

	//////////////////////////////////////////////////////////////////////

	mtabBndSubEdges.reserve( mtabSplitEdges.size() );
	for ( MGSize i=0; i<mtabSplitEdges.size(); ++i)
		mtabBndSubEdges.push_back( mtabSplitEdges[i].cEdge() );

	sort( mtabBndSubEdges.begin(), mtabBndSubEdges.end() );


	//////////////////////////////////////////////////////////////////////
	//faces
	SearchForDegeneracies();

	InitFaces();

	bar.Init( mtabSplitFaces.size() );
	bar.Start();

	MGSize countmissing = 0;
	mtabSubFacesMissing.clear();
	for ( vector<FaceCDT>::iterator itr=mtabSplitFaces.begin(); itr!=mtabSplitFaces.end(); ++itr, ++bar)
	{
		itr->FindFacePipeExtended();
		if ( ! itr->IsRecoverd() )
		{
			itr->Reconstruct();
			itr->UpdateBndSubFaces();
			++countmissing;

			//{
			//	Key<3> kk = itr->cFace();
			//	kk.Sort();
			//	if ( kk == Key<3>( 1811, 4380, 8908 ) )
			//		cout << " FOUND - Key<3>( 1811, 4380, 8908 ) :: ! itr->IsRecoverd() "  << endl;
			//}
		}
		else
		{
			itr->UpdateBndSubFaces();
			//{
			//	Key<3> kk = itr->cFace();
			//	kk.Sort();
			//	if ( kk == Key<3>( 1811, 4380, 8908 ) )
			//		cout << " FOUND - Key<3>( 1811, 4380, 8908 ) :: recovered " << endl;
			//}

		}
	}
	bar.Finish();

	cout << "mtabSubFacesMissing.size() = " << mtabSubFacesMissing.size() << endl;
	cout << "countmissing = " << countmissing << endl;

	FindMissingFaces();

	SearchForDegeneracies();

}


void ReconstructorCDT<DIM_3D>::UpdateBoundaryGrid( BndGrid<DIM_3D>& bgrid)
{
	typedef KeyData< Key<3>, pair< Key<3>, MGSize> > TKeyData;

	// find boundary nodes
	vector<MGSize> tabnode;
	for ( vector<FaceCDT>::iterator itr=mtabSplitFaces.begin(); itr!=mtabSplitFaces.end(); ++itr)
	{
		tabnode.push_back( itr->cFace().cFirst() );
		tabnode.push_back( itr->cFace().cSecond() );
		tabnode.push_back( itr->cFace().cThird() );
		for ( MGSize i=0; i<itr->cTabInNodes().size(); ++i)
			tabnode.push_back( itr->cTabInNodes()[i] );
	}

	sort( tabnode.begin(), tabnode.end() );
	tabnode.erase( unique( tabnode.begin(), tabnode.end() ), tabnode.end() );

	// get surf ids
	vector<TKeyData> tabfacesurf;
	for ( BndGrid<DIM_3D>::ColBCell::const_iterator itr=bgrid.cColCell().begin(); itr!=bgrid.cColCell().end(); ++itr)
	{
		Key<3> key, keys;
		for ( MGSize i=0; i<DIM_3D; ++i)
			key.rElem( i) = bgrid.cNode( itr->cNodeId(i) ).cGrdId();
		keys = key;
		keys.Sort();

		tabfacesurf.push_back( TKeyData( keys, pair< Key<3>, MGSize>( key, itr->cTopoId()) ) );
	}

	sort( tabfacesurf.begin(), tabfacesurf.end() );

	// clear bgrid
	bgrid.rColNode().clear();
	bgrid.rColCell().clear();
	
	// nodes
	MGSize	idnode = 0;
	bgrid.rColNode().reserve( tabnode.size() );
	for ( MGSize i=0; i<tabnode.size(); ++i)
	{
		BndGrid<DIM_3D>::GBndNode	bnode;

		//bnode.rId() = ++idnode;
		bnode.rId() = i+1;
		bnode.rPos() = mpKernel->cGrid().cNode( tabnode[i]).cPos();
		bnode.rGrdId() = mpKernel->cGrid().cNode( tabnode[i]).cId();
		//bnode.rMasterId() = mpKernel->cGrid().cNode( tabnode[i]).cId();

		bgrid.rColNode().push_back( bnode);
	}

	// cells
	MGSize idcell = 0;
	for ( vector<FaceCDT>::iterator itr=mtabSplitFaces.begin(); itr!=mtabSplitFaces.end(); ++itr)
	{
		vector<TKeyData>::iterator itrfs = lower_bound( tabfacesurf.begin(), tabfacesurf.end(), TKeyData( itr->cFace() ) );

		if ( itrfs->first != itr->cFace() )
			THROW_INTERNAL( "ReconstructorCDT<DIM_3D>::UpdateBoundaryGrid :: surf id not found !!!");

		Key<3> keyo = itrfs->second.first;
		MGSize idsurf = itrfs->second.second;

		GVect vn =	( mpKernel->cGrid().cNode(keyo.cSecond()).cPos() - mpKernel->cGrid().cNode(keyo.cFirst()).cPos() ) %
					( mpKernel->cGrid().cNode(keyo.cThird()).cPos() - mpKernel->cGrid().cNode(keyo.cFirst()).cPos() );

		for ( MGSize j=0; j<itr->cTabSubFaces().size(); ++j)
		{
			Key<3> sub = itr->cTabSubFaces()[j].cFace();

			GVect vnsub =	( mpKernel->cGrid().cNode(sub.cSecond()).cPos() - mpKernel->cGrid().cNode(sub.cFirst()).cPos() ) %
							( mpKernel->cGrid().cNode(sub.cThird()).cPos() - mpKernel->cGrid().cNode(sub.cFirst()).cPos() );

			if ( vnsub * vn < 0.0 )
			{
				MGSize t = sub.cSecond();
				sub.rSecond() = sub.cThird();
				sub.rThird() = t;
			}

			BndGrid<DIM_3D>::GBndCell	bcell;

			bcell.rId() = ++idcell;		// global cell id
			bcell.rTopoId() = idsurf;

			for ( MGSize k=0; k<BndGrid<DIM_3D>::GBndCell::SIZE; ++k)
			{
				vector<MGSize>::iterator itrn = lower_bound( tabnode.begin(), tabnode.end(), sub.cElem(k) );
				if ( *itrn != sub.cElem(k) )
					THROW_INTERNAL( "ReconstructorCDT<DIM_3D>::UpdateBoundaryGrid :: node id not found !!!");

				bcell.rNodeId(k) = MGSize( itrn - tabnode.begin() ) + 1;
			}

			bgrid.rColCell().push_back( bcell );

		}
	}

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

