#ifndef __OPERATOR3D_POINTFACE_H__
#define __OPERATOR3D_POINTFACE_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"

#include "libgreen/grdcell.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class BndGrid;

template <Dimension DIM> 
class Kernel;



//////////////////////////////////////////////////////////////////////
// class Operator3D_PointFace
//////////////////////////////////////////////////////////////////////
class Operator3D_PointFace
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

public:
	Operator3D_PointFace( Kernel<DIM_3D>& kernel) : mKernel(kernel), mErrorCode(0)		{}

	const MGSize&	Error() const		{ return mErrorCode;}

	void	Init( const vector<MGSize>& tabcell);

	void	Execute( vector<MGSize>& tabcell);

private:
	Kernel<DIM_3D>		&mKernel;

	MGSize			mErrorCode;

	MGSize			mtabCellUp[GCell::SIZE];
	MGSize			mtabCellLow[GCell::SIZE];

	GVect			mPointPos;

	vector<MGSize>	mtabOCell;
	vector<GCell>	mtabNCell;
};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __OPERATOR3D_POINTFACE_H__
