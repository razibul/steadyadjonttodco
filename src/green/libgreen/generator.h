#ifndef __GENERATOR_H__
#define __GENERATOR_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libgreen/kernel.h"
#include "libgreen/generatoroptions.h"
#include "libgreen/generatordata.h"
#include "libcoreconfig/configbase.h"

#include "libgreen/gridwbnd.h"

//#include "destroyer.h"
#include "libgreen/destroyer_v2.h"


//template <class DATA, Dimension DIM> 
//class Tree;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

//template <Dimension DIM> 
//class Grid;
//
//template <Dimension DIM> 
//class GridWBnd;
//
//template <Dimension DIM> 
//class BndGrid;

template <Dimension DIM>
class ControlSpace;


//////////////////////////////////////////////////////////////////////
// class Generator
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Generator : public ConfigBase
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

	typedef typename TDefs<DIM>::GNode		GNode;
	typedef typename TDefs<DIM>::GCell		GCell;
	typedef typename TDefs<DIM>::GFace		GFace;

	friend class Kernel<DIM>;

public:
	Generator( GridWBnd<DIM>& grd, const ControlSpace<DIM> *pcs=NULL);

	virtual void Create( const CfgSection* pcfgsec);

	void	Process();

	void	GenerateExp( const MGSize& np);
	void	GenerateTestCube( const MGSize& np);
	void	GenerateTestSphere( const MGSize& np);

protected:
	void	FlagExternalCells();
	void	FlagExternalCells( BndGrid<DIM>& bnd);
	//void	FlagExternalCells( vector< Key<3> >& tabbndfac);
	void	RemoveExternalCells();
	void	Triangulate( vector< Key<2> >* ptabedge=NULL);
	void	Smooth();

	void	DelaunayCorrection( const MGSize& maxiter);
	void	SmoothVariation();

	void	Triangulate_Weatherill();
	void	Triangulate_Weatherill2() {}
	void	Triangulate_CircumCenter() {}
	void	Triangulate_AlongEdges( vector< Key<2> >* ptabedge );

	void	TriangulateFront();

private:

	const ControlSpace<DIM>	*mpCSpace;

	GridWBnd<DIM>		*mpGrd;
	BndGrid<DIM>		*mpBndGrd;

	Kernel<DIM>			mKernel;
	
	DestroyerV2<DIM>	mDestr;

	GeneratorData<DIM>	mData;
};


template <Dimension DIM>
inline Generator<DIM>::Generator( GridWBnd<DIM>& grd, const ControlSpace<DIM> *pcs) 
	: mData(), mpGrd(&grd), mpBndGrd(&grd.rBndGrid()), mpCSpace(pcs), mKernel(grd,mData), mDestr(*mpBndGrd,*mpCSpace, mKernel)
{
}

template <Dimension DIM>
inline void Generator<DIM>::Create( const CfgSection* pcfgsec)
{
	ConfigBase::Create( pcfgsec);
}

template <>
inline void Generator<DIM_3D>::Create( const CfgSection* pcfgsec)
{
	ConfigBase::Create( pcfgsec);

	MGString secname = MGString( ConfigStr::Master::Generator::Destroyer::NAME );
	const CfgSection* cfgsec = &ConfigSect().GetSection( secname);

	mDestr.Create( cfgsec);
}


//////////////////////////////////////////////////////////////////////
// class Generator
//////////////////////////////////////////////////////////////////////
template <>
class Generator<DIM_1D> : public ConfigBase
{
	typedef TDefs<DIM_1D>::GVect	GVect;
	typedef TDefs<DIM_1D>::GMetric	GMetric;

	typedef TDefs<DIM_1D>::GNode	GNode;
	typedef TDefs<DIM_1D>::GCell	GCell;

	friend class Kernel<DIM_1D>;

public:
	Generator( GridWBnd<DIM_1D>& grd, const ControlSpace<DIM_1D> *pcs=NULL);

	void	Process();

	void	GenerateTestCube( const MGSize& np);
	void	GenerateTestSphere( const MGSize& np);

protected:
	void	Triangulate();
	void	Smooth();

private:
	const ControlSpace<DIM_1D>	*mpCSpace;

	BndGrid<DIM_1D>			*mpBndGrd;
	GridWBnd<DIM_1D>		*mpGrd;

	Kernel<DIM_1D>			mKernel;
	
	GeneratorData<DIM_1D>	mData;
};


inline Generator<DIM_1D>::Generator( GridWBnd<DIM_1D>& grd, const ControlSpace<DIM_1D> *pcs) 
	: mData(), mpGrd(&grd), mpBndGrd(&grd.rBndGrid()), mpCSpace(pcs), mKernel(grd,mData)	
{
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GENERATOR_H__

