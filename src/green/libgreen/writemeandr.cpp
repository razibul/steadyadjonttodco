#include "writemeandr.h"
#include "libgreen/grid.h"
#include "libcoreio/store.h"
#include "libgreen/kernel.h"
#include "libgreen/generatordata.h"
#include "libcorecommon/progressbar.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Dimension DIM>
void WriteMEANDR<DIM>::DoWrite( const MGString& fname)
{
	ofstream file;


	try
	{
		MGString fnameNode = fname + MGString(".node");

		cout << "Writing MEANDR '" << fnameNode << "'" << endl;//std::flush;

		if ( mMetricCx.Size() != mGrid.SizeNodeTab() )
			THROW_INTERNAL( "mMetricCx.Size() != mGrid.SizeNodeTab()");

		ProgressBar	bar(40);

		MGSize next = 2;
		for ( MGSize idm=1; idm<DIM; ++idm)
			next *= 2;
		cout << "next = " << next << endl;

		/////////////////////////////////////////////////////////////
		// write node file

		file.open( fnameNode.c_str(), ios::out );

		if ( ! file)
			THROW_FILE( "can not open the file", fnameNode);

		file << mGrid.SizeNodeTab() - next << endl;


		MGSize ind=1;
		typename Grid<DIM>::ColNode::const_iterator citr;
		for ( citr=mGrid.cColNode().begin(); citr!=mGrid.cColNode().end(); ++citr)
		{
			if ( citr->cId() > next)
			{
				file << ind;
				for ( MGSize idim=0; idim<DIM; ++idim)
					file << " " << setprecision(17) << citr->cPos().cX(idim);
				file << endl;
				++ind;
			}
		}

		file.close();

		/////////////////////////////////////////////////////////////
		// write metric file
		MGString fnameMetric = fname + MGString(".metric");

		file.open( fnameMetric.c_str(), ios::out );

		if ( ! file)
			THROW_FILE( "can not open the file", fnameNode);

		file << mMetricCx.Size() - next << endl;

		for ( MGSize i=next+1; i<=mMetricCx.Size(); ++i)
		{
			file  << setprecision(19) << i-next;

			if ( DIM == DIM_2D)
			{
				file << " " << mMetricCx.cData(i)[0];
				file << " " << mMetricCx.cData(i)[1];
				file << " " << mMetricCx.cData(i)[2];
			}
			else
			if ( DIM == DIM_3D)
			{
				// 0 1 2	0 1 3
				// 1 3 4	1 2 4
				// 2 4 5	3 4 5

				file << " " << mMetricCx.cData(i)[0];
				file << " " << mMetricCx.cData(i)[1];
				file << " " << mMetricCx.cData(i)[3];
				file << " " << mMetricCx.cData(i)[2];
				file << " " << mMetricCx.cData(i)[4];
				file << " " << mMetricCx.cData(i)[5];
			}
			else
				THROW_INTERNAL( "unknown DIM");

			file << endl;

		}

		file.close();
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: ReadMEANDR<DIM>::DoRead - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}

}



template class WriteMEANDR<DIM_2D>;
template class WriteMEANDR<DIM_3D>;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

