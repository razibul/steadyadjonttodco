#include "edgepipe.h"
#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/gridgeom.h"

#include "libgreen/writegrdtec.h"
#include "libcoreio/store.h"

#include "libgreen/operator3d_pointedge.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template<>
void EdgePipe<DIM_3D>::Export( const MGString& name)
{
	WriteGrdTEC<DIM_3D>	write( &mpKernel->rGrid());

	write.WriteCells( name, mtabCell);

	ofstream f( name.c_str(), std::ios::out | std::ios::app);

	GVect	v1 = mpKernel->rGrid().cNode( mEdge.cFirst() ).cPos();
	GVect	v2 = mpKernel->rGrid().cNode( mEdge.cSecond() ).cPos();

	f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	f << "ZONE T=\"edge\"\n";
	f << v1.cX() << " " << v1.cY() << " " << v1.cZ() << " " << mEdge.cFirst() << endl;
	f << v2.cX() << " " << v2.cY() << " " << v2.cZ() << " " << mEdge.cSecond() << endl;
}


template<>
bool EdgePipe<DIM_3D>::CreatePipe( const Key<2>& edge)
{
	Reset();
	
	bool	bRes;
	mEdge = edge;
	
	bRes = mpKernel->FindEdgePipe( mtabCell, mEdge);
	
	if ( ! bRes)
		return bRes;


	for ( vector<MGSize>::iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
		for ( MGSize i = 0; i < GCell::SIZE; ++i)
		{
			if ( mpKernel->rGrid().cCell( *itr).cNodeId( i) == edge.cFirst() )
			{
				mtabA.push_back( pair<MGSize,MGSize>( *itr, i) );
			}

			if ( mpKernel->rGrid().cCell( *itr).cNodeId( i) == edge.cSecond() )
			{
				mtabB.push_back( pair<MGSize,MGSize>( *itr, i) );
			}
		}
		
	sort( mtabCell.begin(), mtabCell.end() );
		
	TRACE( "Pipe size = " << mtabCell.size() );
	TRACE( "EPipe mtabA.size = " << mtabA.size() );
	TRACE( "EPipe mtabB.size = " << mtabB.size() );
	
	if ( mtabA.size() == 0 || mtabB.size() == 0)
	{
		TRACE( "edge pipe corrupted !!!");
		return false;
	}

	return true;
}


template<>
void EdgePipe<DIM_3D>::FindCrossEdges( vector< Key<2> >& tabedg)
{
	set< Key<2> >	setedge;
	
	for ( vector<MGSize>::iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		for ( MGSize ie = 0; ie<6; ++ie)
		{
			Key<2> key = mpKernel->rGrid().cCell( *itr).EdgeKey( ie);
			key.Sort();
			setedge.insert( key );
		}
	}
	
	TRACE( "number of all edges in the pipe = " << setedge.size() ); 
	
	const GVect& va = mpKernel->rGrid().cNode( mEdge.cFirst() ).cPos();
	const GVect& vb = mpKernel->rGrid().cNode( mEdge.cSecond() ).cPos();

	set< Key<2> >::iterator	ite;
	for ( ite = setedge.begin(); ite != setedge.end(); ++ite)
	{
		if ( mEdge.cFirst() != (*ite).cFirst() && mEdge.cFirst() != (*ite).cSecond() &&
			 mEdge.cSecond() != (*ite).cFirst() && mEdge.cSecond() != (*ite).cSecond() )
		{
			const GVect& v1 = mpKernel->rGrid().cNode( (*ite).cFirst() ).cPos();
			const GVect& v2 = mpKernel->rGrid().cNode( (*ite).cSecond() ).cPos();
		
			const MGFloat vol = Simplex<DIM_3D>( va, vb, v1, v2 ).Volume();
		
			//if ( ::fabs( vol) < ZERO )
			if ( ::fabs( vol) == 0.0 )
			{
				TRACE( "volume = " << vol);
				const GVect dva = va - v1;
				const GVect dvb = vb - v1;
				const GVect dv = v2 - v1;
				const GVect vpa = dva % dv;
				const GVect vpb = dvb % dv;

				MGFloat res = vpa * vpb;

				if ( res < 0.0 )
					tabedg.push_back( *ite);
			}
		}
	}
}


template<>
MGSize EdgePipe<DIM_3D>::NextCell( const MGSize& iccur, const MGSize& icprev)
{
	const GCell& cellc = mpKernel->rGrid().cCell( iccur);
	
	MGSize icnext = 0;
	
	for ( MGSize i=0; i<GCell::SIZE; ++i)
	{
		if ( cellc.cCellId(i).first != icprev)
			if ( binary_search( mtabCell.begin(), mtabCell.end(), cellc.cCellId(i).first ) )
			{
				icnext = cellc.cCellId(i).first;
				return icnext;
			}
	}
	
	return 0;
}



template<>
MGSize EdgePipe<DIM_3D>::NextCell( const MGSize& iccur, const MGSize& icprev, const MGSize& inode)
{
	const GCell& cellc = mpKernel->rGrid().cCell( iccur);
	const GNode& node = mpKernel->rGrid().cNode( inode);

	GVect vc = GridGeom<DIM_3D>::CreateSimplex( cellc, mpKernel->rGrid() ).Center();
	GVect vec = vc - node.cPos();

	vec = mpKernel->rGrid().cNode( mEdge.cSecond() ).cPos() - mpKernel->rGrid().cNode( mEdge.cFirst() ).cPos();

	MGSize icnext = 0;
	
	for ( MGSize i=0; i<GCell::SIZE; ++i)
	{
		if ( cellc.cCellId(i).first != icprev)
		{
			GFace face;
			cellc.GetFaceVNOut( face, i );
			GVect vn = GridGeom<DIM_3D>::CreateSimplexFace( face, mpKernel->rGrid() ).Vn();

			MGFloat prod = vec * vn;

			if ( (-prod > 0)  && ( binary_search( mtabCell.begin(), mtabCell.end(), cellc.cCellId(i).first ) ) )
			{
				icnext = cellc.cCellId(i).first;
				return icnext;
			}
		}
	}
	
	return 0;
}




template<>
bool EdgePipe<DIM_3D>::RecoverSimple()
{
	TRACE( "EdgePipe::RecoverSimple");
 	WriteGrdTEC<DIM_3D>	write( &mpKernel->rGrid());
	
	ASSERT( mtabA.size() == 1);
	ASSERT( mtabB.size() == 1);
	
	MGSize ic1, ic2, ictmp;
	vector<MGSize>	tab;

	//write.WriteCells( "_spipe.plt", mtabCell);

	MGSize	iter = 0;
	MGSize	niter = 2*Size();

	do
	{	
		TRACE( "---- mtabCell.size = " << mtabCell.size() );
		ic1 = mtabA.front().first;
		ic2 = NextCell( ic1, 0);
		
		tab.clear();
		tab.push_back( ic1);
		tab.push_back( ic2);

		//write.WriteCells( "_spipe_1.plt", tab);


//		if ( ! mpKernel->Flip23( tab) )
//		{
//			TRACE( "simple pipe recovery failed");
//			return false;
//		}

		TRACE( "<<<<<<<< ic1 = " << ic1 << "  ic2 = " << ic2);

		MGSize icount = 0;
		
		while ( ! mpKernel->Flip23( tab) && (++icount) < Size() )
		{
			ictmp = ic2;
			ic2 = NextCell( ic2, ic1);
			ic1 = ictmp;
			
			TRACE( ">>>>>>>> ic1 = " << ic1 << "  ic2 = " << ic2);
			
			if ( ! ic2)
			{
				TRACE( "simple pipe recovery failed");
				return false;
			}
			
			tab.clear();
			tab.push_back( ic1);
			tab.push_back( ic2);
		}

		if ( icount >= Size() )
		{
			TRACE( "simple pipe recovery failed");
			return false;
		}

		//write.WriteCells( "_spipe_2.plt", tab);

		CreatePipe( mEdge);

		//write.WriteCells( "_spipe_3.plt", mtabCell);
	}
	while( mtabCell.size() > 0 && (++iter) < niter);


	if ( iter >= niter )
	{
		TRACE( "simple pipe recovery failed");
		return false;
	}
	
	//if ( mtabCell.size() == 2 )
		//mpKernel->Flip23( mtabCell);
	
	TRACE( "+++ RecoverSimple - success");
	
	return true;
}


template<>
bool EdgePipe<DIM_3D>::ReduceSPipe( vector<MGSize>& tab, const vector< Key<2> >& tabe)
{
	vector<MGSize>	tabtmp;

	set<MGSize>	setnode, setcellnode;
	setnode.insert( mEdge.cFirst() );
	setnode.insert( mEdge.cSecond() );

	for ( vector< Key<2> >::const_iterator itre = tabe.begin(); itre != tabe.end(); ++itre)
	{
		setnode.insert( itre->cFirst() );
		setnode.insert( itre->cSecond() );
	}


	MGSize iter = 0;
	MGSize maxiter = 2*tab.size();
	while ( tab.size() > tabe.size()+1 && ++iter <= maxiter)
	{
		cout << iter << endl;

		// find elements to flip (start from begening)
		MGSize istartcell = 0;
		MGSize freenodecount;
		do
		{
			tabtmp.clear();
			tabtmp.push_back( tab[istartcell] );
			tabtmp.push_back( tab[istartcell+1] );

			setcellnode.clear();
			for ( MGSize in=0; in<4; ++in)
			{
				setcellnode.insert(  mpKernel->rGrid().cCell( tab[istartcell]   ).cNodeId( in ) );
				setcellnode.insert(  mpKernel->rGrid().cCell( tab[istartcell+1] ).cNodeId( in ) );
			}

			freenodecount = 0;
			set<MGSize>::iterator	ite;
			for ( ite = setcellnode.begin(); ite != setcellnode.end(); ++ite)
			{
				if ( setnode.find( *ite) == setnode.end() )
					++freenodecount;
			}

			++istartcell;
		}
		while( freenodecount == 1 && istartcell+1 < tab.size() );

		ASSERT( freenodecount > 1);


		// looking for common edge
		set< Key<2> >	setedge;
		
		for ( vector<MGSize>::iterator itr = tabtmp.begin(); itr != tabtmp.end(); ++itr)
			for ( MGSize ie = 0; ie<6; ++ie)
			{
				Key<2> key = mpKernel->rGrid().cCell( *itr).EdgeKey( ie);
				key.Sort();
				setedge.insert( key );
			}

			
		bool bFound = false;
		Key<2> edge;

		for ( vector< Key<2> >::const_iterator itre = tabe.begin(); itre != tabe.end(); ++itre)
		{
			Key<2> tmpe = *itre;
			tmpe.Sort();

			if ( setedge.find( tmpe) != setedge.end() )
			{
				edge = *itre;
				bFound = true;
				break;
			}
		}

		ASSERT( bFound);


		if ( mpKernel->Flip23( tabtmp) )
		{
			//cout << "ok" << endl;

			// update pipes
			ASSERT( tabtmp.size() == 3);



			Key<3> kfac;
			kfac.rFirst() = edge.cFirst();
			kfac.rSecond() = edge.cSecond();
			kfac.rThird() = mEdge.cFirst();
			kfac.Sort();

			MGSize idcell = 0;
			MGSize nofound = 0;

			// find a cell which belongs to the pipe
			for ( MGSize i=0; i<tabtmp.size(); ++i)
			{
				const GCell& cellc = mpKernel->rGrid().cCell( tabtmp[i]);

				for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
				{
					Key<3> kcur = cellc.FaceKey(ifc);
					kcur.Sort();

					if ( kcur == kfac)
					{
						++nofound;
						idcell = tabtmp[i];
					}

				}
			}

			ASSERT( idcell != 0);

			MGSize idnode;
			bool bf = false;
			for ( MGSize i = 0; i < GCell::SIZE; ++i)
			{
				const GCell& cellc = mpKernel->rGrid().cCell( idcell);

				if ( cellc.cNodeId( i) == mEdge.cFirst() )
				{
					idnode = i;
					bf = true;
				}
			}
		
			Export( "_red.plt");

			ASSERT( bf);
			ASSERT( nofound == 1);		// TODO :: check this !!!

			//cout << nofound << endl;

			// update mtabCell
			vector<MGSize>::iterator iend;
			iend = remove( mtabCell.begin(), mtabCell.end(), tab[0]);
			iend = remove( mtabCell.begin(), iend, tab[1]);
			mtabCell.erase( iend, mtabCell.end());

			mtabCell.push_back( idcell );

			sort( mtabCell.begin(), mtabCell.end());

			// update mtabA
			TCollCellNei::iterator icne;
			for ( icne=mtabA.begin(); icne->first != tab[0] && icne != mtabA.end(); ++icne)
				;

			ASSERT( icne != mtabA.end() );

			pair<MGSize,MGSize> elem( idcell, idnode);
			swap( *icne, elem);

			// update mtabB - not neccessary...

			// update tab

			tab.erase( tab.begin() );
			tab.erase( tab.begin() );

			tab.insert( tab.begin(), idcell );
		}
		else	// try flipping the last two elements
		{
			//tabtmp.clear();
			//tabtmp.push_back( *(tab.end()-1) );
			//tabtmp.push_back( *(tab.end()-2) );


			// find elements to flip (start from end)
			MGSize istartcell = tab.size()-1;
			MGSize freenodecount;
			do
			{
				tabtmp.clear();
				tabtmp.push_back( tab[istartcell] );
				tabtmp.push_back( tab[istartcell-1] );

				setcellnode.clear();
				for ( MGSize in=0; in<4; ++in)
				{
					setcellnode.insert(  mpKernel->rGrid().cCell( tab[istartcell]   ).cNodeId( in ) );
					setcellnode.insert(  mpKernel->rGrid().cCell( tab[istartcell-1] ).cNodeId( in ) );
				}

				freenodecount = 0;
				set<MGSize>::iterator	ite;
				for ( ite = setcellnode.begin(); ite != setcellnode.end(); ++ite)
				{
					if ( setnode.find( *ite) == setnode.end() )
						++freenodecount;
				}

				--istartcell;
			}
			while( freenodecount == 1 && istartcell >= 0 );

			ASSERT( freenodecount > 1);

			// looking for common edge
			set< Key<2> >	setedge;
			
			for ( vector<MGSize>::iterator itr = tabtmp.begin(); itr != tabtmp.end(); ++itr)
				for ( MGSize ie = 0; ie<6; ++ie)
				{
					Key<2> key = mpKernel->rGrid().cCell( *itr).EdgeKey( ie);
					key.Sort();
					setedge.insert( key );
				}

				
			bool bFound = false;
			Key<2> edge;

			for ( vector< Key<2> >::const_iterator itre = tabe.begin(); itre != tabe.end(); ++itre)
			{
				Key<2> tmpe = *itre;
				tmpe.Sort();

				if ( setedge.find( tmpe) != setedge.end() )
				{
					edge = *itre;
					bFound = true;
					break;
				}
			}

			ASSERT( bFound);


			if ( mpKernel->Flip23( tabtmp) )
			{
				//cout << "ok" << endl;

				// update pipes
				ASSERT( tabtmp.size() == 3);

				Key<3> kfac;
				kfac.rFirst() = edge.cFirst();
				kfac.rSecond() = edge.cSecond();
				kfac.rThird() = mEdge.cSecond();		// <-- change
				kfac.Sort();

				MGSize idcell = 0;
				MGSize nofound = 0;

				// find a cell which belongs to the pipe
				for ( MGSize i=0; i<tabtmp.size(); ++i)
				{
					const GCell& cellc = mpKernel->rGrid().cCell( tabtmp[i]);

					for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
					{
						Key<3> kcur = cellc.FaceKey(ifc);
						kcur.Sort();

						if ( kcur == kfac)
						{
							++nofound;
							idcell = tabtmp[i];
						}

					}
				}

				ASSERT( idcell != 0);

				MGSize idnode;
				bool bf = false;
				for ( MGSize i = 0; i < GCell::SIZE; ++i)
				{
					const GCell& cellc = mpKernel->rGrid().cCell( idcell);

					if ( cellc.cNodeId( i) == mEdge.cSecond() )		// <-- change
					{
						idnode = i;
						bf = true;
					}
				}
				ASSERT( bf);
				ASSERT( nofound == 1);

				//cout << nofound << endl;

				// update mtabCell
				vector<MGSize>::iterator iend;
				iend = remove( mtabCell.begin(), mtabCell.end(), *(tab.end()-1));
				iend = remove( mtabCell.begin(), iend, *(tab.end()-2));
				mtabCell.erase( iend, mtabCell.end());

				mtabCell.push_back( idcell );

				sort( mtabCell.begin(), mtabCell.end());

				// update mtabB					// <-- change
				TCollCellNei::iterator icne;
				for ( icne=mtabB.begin(); icne->first != tab.back() && icne != mtabB.end(); ++icne)
					;

				ASSERT( icne != mtabB.end() );

				pair<MGSize,MGSize> elem( idcell, idnode);
				swap( *icne, elem);

				// update mtabA - not neccessary...

				// update tab

				tab.erase( tab.end()-2, tab.end() );
				//tab.erase( tab.end()-1 );

				tab.insert( tab.begin(), idcell );
			}
			else
			{
				return false;
			}
		}
	}

	return true;
}


template<>
bool EdgePipe<DIM_3D>::RecoverGeneral( vector< Key<2> >& tabe)
{
	TRACE( "EdgePipe::RecoverGeneral");
 	WriteGrdTEC<DIM_3D>	write( &mpKernel->rGrid());

	// works when tabe.size() == 1
	// TODO :: apply all neccessary checks

	cout << "mtabCell.size() = " << mtabCell.size() << endl;
	cout << "tabe.size() = " << tabe.size() << endl;
	//cout << "mtabA.size() = " << mtabA.size() << endl;
	//cout << "mtabB.size() = " << mtabB.size() << endl;

	ASSERT( mtabA.size() == 2);
	ASSERT( mtabB.size() == 2);

	vector<MGSize>	tabb;

	for ( TCollCellNei::iterator itr = mtabB.begin(); itr != mtabB.end(); ++itr)
		tabb.push_back( itr->first );

	//cout << tabb.size() << endl;
	sort( tabb.begin(), tabb.end() );

	vector<MGSize>	tabup, tablo;
	
	// find upper pipe
	MGSize	ica1 = mtabA.front().first;
	MGSize	icb1 = (mpKernel->rGrid()).cCell( ica1 ).cCellId( mtabA.front().second ).first;

	tabup.push_back( ica1);
	tabup.push_back( icb1);

	//cout << tabb.size() << endl;
	while ( ! binary_search( tabb.begin(), tabb.end(), icb1) )
	{
		MGSize  icc1 = NextCell( icb1, ica1, mEdge.cFirst() );
		tabup.push_back( icc1);
		ica1 = icb1;
		icb1 = icc1;
	}

	// find lower pipe
	MGSize	ica2 = mtabA.back().first;
	MGSize	icb2 = (mpKernel->rGrid()).cCell( ica2 ).cCellId( mtabA.back().second ).first;
	
	tablo.push_back( ica2);
	tablo.push_back( icb2);

	while ( ! binary_search( tabb.begin(), tabb.end(), icb2) )
	{
		MGSize  icc2 = NextCell( icb2, ica2, mEdge.cFirst() );
		tablo.push_back( icc2);
		ica2 = icb2;
		icb2 = icc2;
	}

	// reduction of pipes to size of 2

	bool bUp = ReduceSPipe( tabup, tabe);
	bool bLo = ReduceSPipe( tablo, tabe);

	if ( !bUp || !bLo)
		return false;


	Export( "_gpipe_after.plt");

	if ( mtabCell.size() > 4)
	{
		do
		{
			vector<MGSize> tabflip;
			tabflip.push_back( mtabA.front().first );
			tabflip.push_back( (mpKernel->rGrid()).cCell( mtabA.front().first ).cCellId( mtabA.front().second ).first );
			tabflip.push_back( mtabA.back().first );
			tabflip.push_back( (mpKernel->rGrid()).cCell( mtabA.back().first ).cCellId( mtabA.back().second ).first );

			const GCell &c1 = mpKernel->rGrid().cCell( tabflip[0] );
			const GCell &c2 = mpKernel->rGrid().cCell( tabflip[1] );

			MGSize in1 = c1.cNodeId( mtabA.front().second );
			MGSize in2 = c2.cNodeId( c1.cCellId( mtabA.front().second ).second );
			
			Key<2>	tmpedge(in1, in2);
			tmpedge.Sort();

			if ( mpKernel->Flip44( tabflip, tmpedge ) )
				CreatePipe( mEdge);
			else
				return false;
		}
		while ( mtabCell.size() > 4);

	}

	if ( mpKernel->Flip44( mtabCell, mEdge ) )
	{
		cout << "ok" << endl;
		return true;
	}

	return false;
}





template<>
bool EdgePipe<DIM_3D>::Recover()
{
	TRACE( "\n##################### EdgePipe::Recover #####################");


	typedef vector< Key<2> >	TKeyTab;
	TKeyTab		tabe;


	// check if pipe is a simple one
	FindCrossEdges( tabe);


	//vector<MGSize> tabc;
	//mpKernel->FindEdgeShell( tabc, tabe[0]);
	//
	//Operator3D_PointEdge	oper( *mpKernel);
	//
	//oper.Init( tabc);
	//oper.Execute( tabc);
	//mpKernel->rGrid().CheckConnectivity();
	//THROW_INTERNAL( "END");

	
	//if ( tabe.size() > 0)
	if ( tabe.size() == 1)
	{
		Export( "_gpipe.plt");
		TRACE( " edge pipe is not simple - tabe.size() = " << tabe.size() );

		return RecoverGeneral( tabe);
	}
	//else 
	//if ( tabe.size() > 1)
	//{
	//	Export( "_gpipe.plt");
	//	TRACE( " edge pipe is not simple - tabe.size() = " << tabe.size() );

	//	return RecoverGeneral( tabe);
	//}
	else 
	if ( tabe.size() == 0)
	{
		return RecoverSimple();
	}

	//Export( "_pipe.plt");
	


	//// some old stuff
	//if ( ! RecoverSimple() )
	//{
	//	return false;
	//	cout << "!!!!!!!!!!\n";
	//	GVect	v1 = mpKernel->rGrid().cNode( mEdge.cFirst() ).cPos();
	//	GVect	v2 = mpKernel->rGrid().cNode( mEdge.cSecond() ).cPos();
	//	GVect	vct = 0.5*(v1+v2);
	//	Metric<MGFloat,DIM_3D>	met;
	//	met.InitIso( 1.0);
	//	
	//	MGSize imid = mpKernel->InsertPointMetric( vct, met );
	//	Key<2>	e1( mEdge.cFirst(), imid );
	//	Key<2>	e2( imid, mEdge.cSecond() );
	//	
	//	vector<MGSize>	tabsh1, tabsh2;
	//	
	//	mpKernel->FindEdgShell( tabsh1, e1);
	//	mpKernel->FindEdgShell( tabsh2, e2);
	//	
	// 	WriteGrdTEC<DIM_3D>	write( &mpKernel->rGrid());

	//	write.WriteCells( "_shell_1.plt", tabsh1);
	//	write.WriteCells( "_shell_2.plt", tabsh2);
	//	
	//	
	//	mpKernel->CollapseEdge( e1, imid);
	//	mpKernel->rGrid().CheckConnectivity();

	//	CreatePipe( mEdge);
	//	TRACE1( "pipe size = %d", Size() );
	//	Export( "_pipe_after.plt");
	//	ASSERT(0);
	//}
	
	
	return false;
}


template<>
MGSize EdgePipe<DIM_3D>::SplitEdge()
{
	//vector< Key<2> > tabe;

	//FindCrossEdges( tabe);

	//if ( tabe.size() )
	//{
	//	GVect vcross;

	//	GVect v1b ;

	//	bool bOK = FindEdgeEdgeCross( vcross, v1b, v1e, v2b, v2e);
	//	if ( ! bOK)
	//		THROW_INTERNAL( "EdgePipe<DIM_3D>::SplitEdge :: crossing point not found");
	//	//vcross = FindCrossing( mEdge, tabe[0]);

	//	vector<MGSize> tabc;

	//	mpKernel->FindEdgeShell( tabc, tabe[0]);
	//	
	//	Operator3D_PointEdge	oper( *mpKernel);
	//	
	//	oper.Init( tabc, vcross);
	//	oper.Execute( tabc);
	//	mpKernel->rGrid().CheckConnectivity();

	//	return oper.cIdNode();
	//}
	//else
	//{
	//}


	return 0;
}


template<>
bool EdgePipe<DIM_3D>::RecoverSplit( vector< Key<2> >& tabsedge)
{
	typedef vector< Key<2> >	TKeyTab;
	TKeyTab		tabe;


	//tabsedge.clear();

	//FindCrossEdges( tabe);
	//while ( tabe.size() > 0 )
	//{
	//	GVect vcross = FindCrossing( mEdge, tabe[0]);

	//	vector<MGSize> tabc;

	//	mpKernel->FindEdgeShell( tabc, tabe[0]);
	//	
	//	Operator3D_PointEdge	oper( *mpKernel);
	//	
	//	oper.Init( tabc, vcross);
	//	oper.Execute( tabc);
	//	mpKernel->rGrid().CheckConnectivity();


	//	FindCrossEdges( tabe);
	//}

	return false;
}


template<>
bool EdgePipe<DIM_3D>::RecoverSteiner()
{
	return false;
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

