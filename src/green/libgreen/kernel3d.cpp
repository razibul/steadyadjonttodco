#include "kernel.h"
#include "libgreen/localizator.h"
#include "libgreen/gridgeom.h"
#include "libgreen/grid.h"

#include "libcoreio/store.h"
#include "libgreen/writegrdtec.h"
#include "libgreen/edgepipe.h"

#include "libgreen/metricinterpol.h"
#include "libgreen/generatordata.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <class T>
inline bool lesser_pair ( const T& elem1, const T& elem2 )
{
   return  ( elem1.first < elem2.first );
}


void Kernel<DIM_3D>::EraseNode( const MGSize& id)		
{ 
	mpGrd->EraseNode( id); 
}



MGSize Kernel<DIM_3D>::InsertCell( const GCell& cell)
{
	const MGSize id = rGrid().InsertCell( cell);
	InsertCellIntoTree( id);

	return id;
}

void Kernel<DIM_3D>::EraseCell( const MGSize& id)
{
	RemoveCellFromTree( id);
	rGrid().EraseCell( id);
}

void Kernel<DIM_3D>::EraseCellSafe( const MGSize& id)
{
	RemoveCellFromTree( id);
	rGrid().EraseCellSafe( id);
}

void Kernel<DIM_3D>::RemoveCellFromTree( const MGSize& id)
{
	if ( mbUseTree )
		if ( rGrid().cCell( id).IsTree() )
		{
			const GVect vc = GridGeom<DIM_3D>::CreateSimplex( id, rGrid() ).Center();
			mLoc.TreeErase( vc, id);
			rGrid().rCell( id).SetTree( false);
		}
}

void Kernel<DIM_3D>::InsertCellIntoTree( const MGSize& id)
{
	if ( mbUseTree )
	{
		const GVect vc = GridGeom<DIM_3D>::CreateSimplex( id, rGrid() ).Center();
		rGrid().rCell(id).SetTree( true);
		mLoc.TreeInsertSafe( vc, id);
	}
}


//template <> 
MGSize Kernel<DIM_3D>::FindCell( const GVect& vct)
{
	// power search - the worst algo...

	Grid<DIM_3D>::ColCell::const_iterator	itr;

	for ( itr = mpGrd->cColCell().begin(); itr != mpGrd->cColCell().end(); ++itr)
	{
		const GCell& cell = (*itr);
		if ( GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd).IsInside( vct) )
			return itr.index();
	}

	return 0;
}


//template <>
void Kernel<DIM_3D>::InitBoundingBox( const GVect& vmin, const GVect& vmax)
{
	const MGFloat	expcoeff = 1.4;
	//const MGFloat	expcoeff = 0.05;
	GVect	dv = vmax - vmin;

	mBox.rVMax() = vmax + expcoeff*dv;
	mBox.rVMin() = vmin - expcoeff*dv;

	mBox.Equalize();

	mLoc.InitBox( mBox);

 	//mBox.rVMin().rX() -= 0.001;
 	//mBox.rVMin().rY() -= 0.002;
 	//mBox.rVMin().rZ() -= 0.003;
 
 	//mBox.rVMax().rX() += 0.011;
 	//mBox.rVMax().rY() += 0.002;
 	//mBox.rVMax().rZ() += 0.033;
}


//template<>
void Kernel<DIM_3D>::InitCube()
{
	MGFloat dist = (mBox.cVMax() - mBox.cVMin()).module() * 10e-7;
	MGFloat	dv =  dist / (MGFloat)RAND_MAX;
	dv = 0.0;

	Box<DIM_3D> box = mBox;
	GVect vplus(1.0, 1.0, 1.0);
	box.rVMax() += vplus*dist;
	box.rVMin() -= vplus*dist;

	//mLoc.InitBox( mBox);
	mLoc.InitBox( box);

	//mpGrd->InsertNode( GNode( Vect3D( mBox.cVMin().cX(), mBox.cVMin().cY(), mBox.cVMin().cZ() ) ) );
	//mpGrd->InsertNode( GNode( Vect3D( mBox.cVMax().cX(), mBox.cVMin().cY(), mBox.cVMin().cZ() ) ) );
	//mpGrd->InsertNode( GNode( Vect3D( mBox.cVMax().cX(), mBox.cVMax().cY(), mBox.cVMin().cZ() ) ) );
	//mpGrd->InsertNode( GNode( Vect3D( mBox.cVMin().cX(), mBox.cVMax().cY(), mBox.cVMin().cZ() ) ) );

	//mpGrd->InsertNode( GNode( Vect3D( mBox.cVMin().cX(), mBox.cVMin().cY(), mBox.cVMax().cZ() ) ) );
	//mpGrd->InsertNode( GNode( Vect3D( mBox.cVMax().cX(), mBox.cVMin().cY(), mBox.cVMax().cZ() ) ) );
	//mpGrd->InsertNode( GNode( Vect3D( mBox.cVMax().cX(), mBox.cVMax().cY(), mBox.cVMax().cZ() ) ) );
	//mpGrd->InsertNode( GNode( Vect3D( mBox.cVMin().cX(), mBox.cVMax().cY(), mBox.cVMax().cZ() ) ) );

	

	mpGrd->InsertNode( GNode( Vect3D( mBox.cVMin().cX()-(MGFloat)rand()*dv, mBox.cVMin().cY()-(MGFloat)rand()*dv, mBox.cVMin().cZ()-(MGFloat)rand()*dv ) ) );
	mpGrd->InsertNode( GNode( Vect3D( mBox.cVMax().cX()+(MGFloat)rand()*dv, mBox.cVMin().cY()-(MGFloat)rand()*dv, mBox.cVMin().cZ()-(MGFloat)rand()*dv ) ) );
	mpGrd->InsertNode( GNode( Vect3D( mBox.cVMax().cX()+(MGFloat)rand()*dv, mBox.cVMax().cY()+(MGFloat)rand()*dv, mBox.cVMin().cZ()-(MGFloat)rand()*dv ) ) );
	mpGrd->InsertNode( GNode( Vect3D( mBox.cVMin().cX()-(MGFloat)rand()*dv, mBox.cVMax().cY()+(MGFloat)rand()*dv, mBox.cVMin().cZ()-(MGFloat)rand()*dv ) ) );

	mpGrd->InsertNode( GNode( Vect3D( mBox.cVMin().cX()-(MGFloat)rand()*dv, mBox.cVMin().cY()-(MGFloat)rand()*dv, mBox.cVMax().cZ()+(MGFloat)rand()*dv ) ) );
	mpGrd->InsertNode( GNode( Vect3D( mBox.cVMax().cX()+(MGFloat)rand()*dv, mBox.cVMin().cY()-(MGFloat)rand()*dv, mBox.cVMax().cZ()+(MGFloat)rand()*dv ) ) );
	mpGrd->InsertNode( GNode( Vect3D( mBox.cVMax().cX()+(MGFloat)rand()*dv, mBox.cVMax().cY()+(MGFloat)rand()*dv, mBox.cVMax().cZ()+(MGFloat)rand()*dv ) ) );
	mpGrd->InsertNode( GNode( Vect3D( mBox.cVMin().cX()-(MGFloat)rand()*dv, mBox.cVMax().cY()+(MGFloat)rand()*dv, mBox.cVMax().cZ()+(MGFloat)rand()*dv ) ) );


	//mpGrd->rNode(1).SetExternal( true);
	//mpGrd->rNode(2).SetExternal( true);
	//mpGrd->rNode(3).SetExternal( true);
	//mpGrd->rNode(4).SetExternal( true);
	//mpGrd->rNode(5).SetExternal( true);
	//mpGrd->rNode(6).SetExternal( true);
	//mpGrd->rNode(7).SetExternal( true);
	//mpGrd->rNode(8).SetExternal( true);


	GCell	cell;

	// cell 1
	cell.rId() = 1;
	cell.rNodeId( 0) = 1;
	cell.rNodeId( 1) = 2;
	cell.rNodeId( 2) = 4;
	cell.rNodeId( 3) = 5;

	cell.rCellId( 0) = GNeDef( 5, 1);

	mpGrd->InsertCell( cell);

	// cell 2
	cell.Reset();
	cell.rId() = 2;
	cell.rNodeId( 0) = 3;
	cell.rNodeId( 1) = 4;
	cell.rNodeId( 2) = 2;
	cell.rNodeId( 3) = 7;

	cell.rCellId( 0) = GNeDef( 5, 3);

	mpGrd->InsertCell( cell);

	// cell 3
	cell.Reset();
	cell.rId() = 3;
	cell.rNodeId( 0) = 6;
	cell.rNodeId( 1) = 5;
	cell.rNodeId( 2) = 7;
	cell.rNodeId( 3) = 2;

	cell.rCellId( 0) = GNeDef( 5, 2);

	mpGrd->InsertCell( cell);

	// cell 4
	cell.Reset();
	cell.rId() = 4;
	cell.rNodeId( 0) = 8;
	cell.rNodeId( 1) = 7;
	cell.rNodeId( 2) = 5;
	cell.rNodeId( 3) = 4;

	cell.rCellId( 0) = GNeDef( 5, 0);

	mpGrd->InsertCell( cell);

	// cell 5
	cell.Reset();
	cell.rId() = 5;
	cell.rNodeId( 0) = 2;
	cell.rNodeId( 1) = 7;
	cell.rNodeId( 2) = 4;
	cell.rNodeId( 3) = 5;

	cell.rCellId( 0) = GNeDef( 4, 0);
	cell.rCellId( 1) = GNeDef( 1, 0);
	cell.rCellId( 2) = GNeDef( 3, 0);
	cell.rCellId( 3) = GNeDef( 2, 0);

	mpGrd->InsertCell( cell);

	for ( MGSize ic=1; ic<=5; ++ic)
	{
		Vect3D	vc = GridGeom<DIM_3D>::CreateSimplex( ic, *mpGrd ).Center();

		mpGrd->rCell(ic).SetTree( true);
		mLoc.TreeInsert( vc, ic);
	}
}




/*
//template <> 
bool Kernel<DIM_3D>::FindCavityMetric( vector<MGSize>& tabcell, vector<GFace>& tabface, const MGSize& icell,
								 const GVect& vct, const GMetric& met)
{

	//if ( ! mpGrd->cCell( icell).IsInsideSphere( vct, *mpGrd) )
	//{
	//	printf( "\nnew point at %10.6lg %10.6lg %10.6lg \n", vct.cX(), vct.cY(), vct.cZ() );
	//	mpGrd->cCell( icell).Dump( *mpGrd);
	//	THROW_INTERNAL( "IsInsideSphere failed for the point Base !!!");
	//}


	vector<MGSize>::iterator itrcell;
	static vector<GCell*>	tabstack;
	static vector<GCell*>	tabcavcell;

	tabstack.reserve(50);
	tabcavcell.reserve(50);

	tabstack.resize(0);
	tabcavcell.resize(0);


	tabcavcell.push_back( &mpGrd->rCell( icell));
	tabstack.push_back( &mpGrd->rCell( icell));

	tabstack.back()->Paint();



	/////////////////////////////////////////////////////////////
	// searching for the elements in the cavity
	GMetric	tabmetric[GCell::SIZE];

	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;

			if ( ic)
			{
				GCell &cellnei = mpGrd->rCell( ic);
				const GMetric& metric = met;

				for ( MGSize im=0; im<GCell::SIZE; ++im)
					tabmetric[im] = met;//[ mpGrd->cNode(im).cId() ]

				//metric.Write();
				//THROW_INTERNAL("BREAK");
				Simplex<DIM_3D>	tet = GridGeom<DIM_3D>::CreateSimplex( cellnei, *mpGrd);

				// TODO :: add some tolerance not to reconnect the cospherical points
				//if ( (! cellnei.IsPainted()) && ( tet.IsInsideSphere( vct) > 1.e-14 ) )
				if ( (! cellnei.IsPainted()) && 
					 (! cellnei.IsExternal()) && 
					 ( tet.IsInsideSphereMetric( vct, metric, tabmetric) > 0 || tet.Volume() <= 1.0e-17 ) ) 
				{
					tabcavcell.push_back( &cellnei );
					tabstack.push_back( &cellnei );
					cellnei.Paint();
// 					tabcavcell.push_back( &mpGrd->rCell( ic) );
// 					tabstack.push_back( &mpGrd->rCell( ic) );
// 					mpGrd->rCell( ic).Paint();
				}
			}
		}
	}


	/////////////////////////////////////////////////////////////
	// correction for star-shape of the cavity

	// set min cell volume
	vector<GCell*>::iterator itc = tabcavcell.begin();
	MGFloat volmin = GridGeom<DIM_3D>::CreateSimplex( *(*itc), *mpGrd ).Volume();
	
	for ( ++itc; itc != tabcavcell.end(); ++itc) 
		volmin = min( volmin, GridGeom<DIM_3D>::CreateSimplex( *(*itc), *mpGrd ).Volume() );

	volmin *= 1.0e-10;


	///////////////////////////////////////
	//// TODO :: check it !!!!!!
	//
	//bool isCon;
	//bool bcont = true;
	//MGSize icon = tabcavcell.size();
	//
	//while ( bcont && icon > 1)
	//{
	//	bcont = false;
	//	icon = 0;
	//	
	//	for ( itc = tabcavcell.begin(); itc != tabcavcell.end(); ++itc) 
	//	{
	//		GCell &cell = *(*itc);

	//		if ( ! cell.IsPainted() ) 
	//			continue;

	//		isCon = false;
	//		
	//		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
	//		{
	//			const MGSize inei = cell.cCellId( ifc).first;
	//			
	//			if ( inei )
	//				if ( mpGrd->cCell( inei).IsPainted() )
	//					isCon = true;
	//		}
	//		
	//		if ( !isCon) 
	//		{
	//			TRACE( "cavity not connected");
	//			cell.WashPaint();
	//			bcont = true;
	//		}
	//		else
	//			icon++;
	//	}
	//}
	//
	//////////////////////////

	

	GFace	face;
	bool	bOk;
	bool	bExit;

	do
	{
		bExit = true;

		for ( MGSize ic=0; ic<tabcavcell.size(); ++ic)
		{
			GCell	&cell = *tabcavcell[ic];

			if ( cell.IsPainted() )
			{

				for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
				{
					const MGSize inei = cell.cCellId( ifc).first;

					bOk = false;

 					if ( ! inei)
 						bOk = true;
 					else if ( ! mpGrd->cCell( inei).IsPainted() )
 						bOk = true;


					if ( bOk )
					{
						Simplex<DIM_3D>		tet( mpGrd->cNode( cell.cFaceNodeId( ifc, 0) ).cPos(), 
												 mpGrd->cNode( cell.cFaceNodeId( ifc, 2) ).cPos(),
												 mpGrd->cNode( cell.cFaceNodeId( ifc, 1) ).cPos(),
												 vct );

						// bad cell has negative volume
						if ( tet.Volume() <= volmin )// -1.0e-14 ) 
						{
							cell.WashPaint();
							bExit = false;
						}
					}
				}
			}
		}
	}
	while ( ! bExit);

	// TODO :: check the cavity - hanging nodes, disconnected cells


	static MGSize	tabfacconn[4][4] = { {0,0,2,1}, {0,0,1,2}, {0,2,0,1}, {0,1,2,0} };
	MGSize	tabfnei[GCell::SIZE];

	/////////////////////////////////////////////////////////////
	// creating cavity boundary faces 
	for ( MGSize ic=0; ic<tabcavcell.size(); ++ic)
	{
		GCell	&cell = *tabcavcell[ic];

		if ( ! cell.IsPainted() )
			continue;
			

		::memset( tabfnei, 0, GCell::SIZE*sizeof(MGSize) );

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize inei = cell.cCellId( ifc).first;


			bOk = false;

			if ( ! inei)
				bOk = true;
			else if ( ! mpGrd->cCell( inei).IsPainted() )
				bOk = true;

			if ( bOk )
			{
				cell.GetFaceVNIn( face, ifc);
				tabface.push_back( face);
				tabfnei[ifc] = tabface.size();		// <--- !!!!!!  ind + 1
			}

		}

		/////////////////////////////////////////////////////////////
		// set connectivity for the cell faces
		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			if ( tabfnei[ifc] )
			{
				GFace &face = tabface[tabfnei[ifc]-1];

				for ( MGSize ifnei=0; ifnei<GCell::SIZE; ++ifnei)
				{
					if ( ifnei != ifc && tabfnei[ifnei] != 0)
					{
						face.rCellId( tabfacconn[ifc][ifnei]).first = tabfnei[ifnei];
						face.rCellId( tabfacconn[ifc][ifnei]).second = tabfacconn[ifnei][ifc];
					}
				}
			}
		}
	}



	for ( MGSize ic=0; ic<tabcavcell.size(); ++ic)
	{
		if ( tabcavcell[ic]->IsPainted() )
			tabcell.push_back( tabcavcell[ic]->cId() );
	}

		

	//printf( "tabcell.size = %d\n", tabcell.size() );
	//printf( "tabface.size = %d\n", tabface.size() );


	//Store::File	f( "_cavity.dat", "wt");
	//for ( itrcell=tabcell.begin(); itrcell!=tabcell.end(); ++itrcell)
	//{
	//	const GCell &cell = mpGrd->cCell( *itrcell );
	//	cell.DumpTEC( *mpGrd, f);
	//}
	//f.Close();

	//f.Open( "_cavfaces.dat", "wt");
	//for ( MGSize i=0; i<tabface.size(); ++i)
	//	tabface[i].DumpTEC( *mpGrd, f);
	//f.Close();
	
	
	if ( ! tabcell.size() || ! tabface.size() )
	{
		TRACE( "Kernel::FindCavity - failed: empty cavity"); 
		return false;
	}
	
	return true;

}
*/


bool Kernel<DIM_3D>::FindBase( vector<MGSize>& tabbase, const GVect& vct, const MGSize& icstart)
{
	MGSize	icell;
	
	if ( icstart)
		icell = icstart;
	else
		icell = mLoc.Localize( vct);
		
	if ( ! icell)
	{
		cout << "failed to localize cell for vct: [ " <<  vct.cX() << " " << vct.cY() << " " << vct.cZ() << " ]" << endl;
		return false;
	}

	tabbase.clear();
	tabbase.push_back( icell);
	return true;
	
	GCell& cell = mpGrd->rCell( icell);

	if ( ! GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd).IsInside( vct) )
	{
		THROW_INTERNAL( "Localizer failed!!! - from Kernel<DIM_3D>::FindBall");
	}

	// start search
	vector<GCell*>	tabstack;
	vector<MGSize>	tabcell;

	tabstack.reserve(50);
	tabcell.reserve(50);

	tabstack.resize(0);
	tabcell.resize(0);

	tabcell.push_back( icell);
	tabstack.push_back( &mpGrd->rCell( icell));

	tabstack.back()->Paint();


	/////////////////////////////////////////////////////////////
	// searching for the elements in the cavity

	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;

			if ( ic)
			{
				GCell &cellnei = mpGrd->rCell( ic);

				Simplex<DIM_3D>	tet = GridGeom<DIM_3D>::CreateSimplex( cellnei, *mpGrd);

				if ( (! cellnei.IsPainted()) && 
					 ( tet.IsInside( vct) || tet.Volume() <= 0.0 ) ) 
				{
					tabcell.push_back( cellnei.cId() );
					tabstack.push_back( &cellnei );
					cellnei.Paint();
				}
			}
		}
	}

	if ( ! tabcell.size() )
	{
		TRACE( "Kernel::FindBase - failed: empty cavity"); 
		return false;
	}

	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		GCell &cell = mpGrd->rCell( tabcell[i] );
		cell.WashPaint();

		Simplex<DIM_3D>	tet = GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd);
		if ( tet.IsInside( vct) )
			tabbase.push_back( tabcell[i] );
	}

	return true;
}


//template <> 
bool Kernel<DIM_3D>::FindBall( vector<MGSize>& tabball, const MGSize& inod, const MGSize& icstart)
{
	vector<GCell*>	tabstack;
	
	tabball.clear();

	GVect	vct = mpGrd->cNode( inod).cPos();
	
	MGSize	icell;
	
	if ( icstart)
		icell = icstart;
	else
		icell = mLoc.Localize( vct);
		
		
	if ( ! icell)
	{
		printf( "failed to localize cell for nod: %d [%lg, %lg, %lg]\n", inod, vct.cX(), vct.cY(), vct.cZ() );
		return false;
	}
	
	GCell& cell = mpGrd->rCell( icell);

	//if ( ! GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd).IsInside( vct) )
	//{
	//	THROW_INTERNAL( "Localizer failed!!! - from Kernel<DIM_3D>::FindBall");
	//}


	bool bFound = false;	
	for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		if ( cell.cNodeId( ifc ) == inod)
		{
			bFound = true;
			break;
		}
	

	if ( ! bFound)
	{
		vector<MGSize> tabbase;

		FindBase( tabbase, vct);

		//cout << "tabbase.size() = " << tabbase.size() << endl;

		for ( MGSize i=0; i<tabbase.size(); ++i )
		{
			GCell& cell = mpGrd->rCell( tabbase[i] );

			for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
				if ( cell.cNodeId( ifc ) == inod)
				{
					icell = tabbase[i];
					bFound = true;
					break;
				}

			if ( bFound)
				break;
		}

		if ( ! bFound)
		{
			cout << "FindBall -- brute force is used !!!" << endl;
			// brute force
			for ( Grid<DIM_3D>::ColCell::iterator itr=mpGrd->rColCell().begin(); itr!=mpGrd->rColCell().end(); ++itr)
			{
				const GCell& cell = *itr;

				for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
					if ( cell.cNodeId( ifc ) == inod)
					{
						icell = cell.cId();
						bFound = true;
						break;
					}
			}
		}

		if ( ! bFound)
		{
			cout << "CELL :: " << icell << endl;
			for ( MGSize i=0; i<GCell::SIZE; ++i)
			{
				GVect v = mpGrd->cNode( cell.cNodeId( i) ).cPos();
				cout << i << " " << v.cX() << " " << v.cY() << " " << v.cZ() << " " << cell.cNodeId( i) << endl;
			}
			cout << endl;
			THROW_INTERNAL( "FindBall - starting cell does not touch the inod = " << inod);
		}
	}
	

	// start search
	GCell& cellfound = mpGrd->rCell( icell);
	
	cellfound.Paint();
	
	tabstack.push_back( &cellfound);
	tabball.push_back( icell);
	
	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			if ( cell.cNodeId( ifc ) == inod)
				continue;
				
			MGSize ic = cell.cCellId( ifc).first;
			
			if ( ic )
			{
				GCell &cellnei = mpGrd->rCell( ic);
				
				if ( cellnei.IsPainted() )
					continue;
					
				for ( MGSize in = 0; in <GCell::SIZE; ++in)
					if ( cellnei.cNodeId( in) == inod )
					{
						cellnei.Paint();
						tabstack.push_back( &cellnei);
						tabball.push_back( ic);
						break;
					}
			}
		}
	};
	
	for ( vector<MGSize>::iterator itrp=tabball.begin(); itrp!=tabball.end(); ++itrp)
		mpGrd->rCell( *itrp).WashPaint();

	return true;
}



bool Kernel<DIM_3D>::FindBall( vector<MGSize>& tabball, const vector<MGSize>& tabnod, const MGSize& icstart)
{
	vector<GCell*>	tabstack;

	MGSize inod = tabnod.front();

	GVect	vct = mpGrd->cNode( inod).cPos();
	
	MGSize	icell;
	
	if ( icstart)
		icell = icstart;
	else
		icell = mLoc.Localize( vct);
		
		
	if ( ! icell)
	{
		printf( "failed to localize cell for nod: %d [%lg, %lg, %lg]\n", inod, vct.cX(), vct.cY(), vct.cZ() );
		return false;
	}
	
	GCell& cell = mpGrd->rCell( icell);

	if ( ! GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd).IsInside( vct) )
	{
		THROW_INTERNAL( "Localizer failed!!! - from Kernel<DIM_3D>::FindBall");
	}


	bool bFound = false;	
	for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		if ( cell.cNodeId( ifc ) == inod)
		{
			bFound = true;
			break;
		}
	
	if ( ! bFound)
	{
		cout << "CELL :: " << icell << endl;
		for ( MGSize i=0; i<GCell::SIZE; ++i)
		{
			GVect v = mpGrd->cNode( cell.cNodeId( i) ).cPos();
			cout << i << " " << v.cX() << " " << v.cY() << " " << v.cZ() << " " << cell.cNodeId( i) << endl;
		}
		cout << endl;
		THROW_INTERNAL( "FindBall - starting cell does not touch the inod = " << inod);
	}
	
	
	cell.Paint();
	
	tabstack.push_back( &cell);
	tabball.push_back( icell);
	
	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			//if ( cell.cNodeId( ifc ) == inod)
			//	continue;
				
			MGSize ic = cell.cCellId( ifc).first;
			
			if ( ic )
			{
				GCell &cellnei = mpGrd->rCell( ic);
				
				if ( cellnei.IsPainted() )
					continue;
					
				for ( MGSize in = 0; in <GCell::SIZE; ++in)
					//if ( cellnei.cNodeId( in) == inod )
					if ( binary_search( tabnod.begin(), tabnod.end(), cellnei.cNodeId( in) ) )
					{
						cellnei.Paint();
						tabstack.push_back( &cellnei);
						tabball.push_back( ic);
						break;
					}
			}
		}
	};
	
	for ( vector<MGSize>::iterator itrp=tabball.begin(); itrp!=tabball.end(); ++itrp)
		mpGrd->rCell( *itrp).WashPaint();

	return true;
}



//template <> 
bool Kernel<DIM_3D>::FindEdgeShell( vector<MGSize>& tabcell, const Key<2>& edge, const MGSize& icstart1, const MGSize& icstart2)
{
	tabcell.clear();

	vector<MGSize>	tabball_fst;
	vector<MGSize>	tabball_snd;

	if ( ! FindBall( tabball_fst, edge.cFirst(), icstart1 ) )
	{
		cout << "failed to find an edge shell [" << edge.cFirst() << ", " << edge.cSecond() << "]\n";
		TRACE( "failed to find an edge shell [" << edge.cFirst() << ", " << edge.cSecond() << "]\n"  );
		
		return false;
	}

	if ( ! FindBall( tabball_snd, edge.cSecond(), icstart2 ) )
	{
		cout << "failed to find an edge shell [" << edge.cFirst() << ", " << edge.cSecond() << "]\n";
		TRACE( "failed to find an edge shell [" << edge.cFirst() << ", " << edge.cSecond() << "]\n"  );
		
		return false;
	}
	
	sort( tabball_fst.begin(), tabball_fst.end() );
	sort( tabball_snd.begin(), tabball_snd.end() );
	
//	tabcell.resize( tabball_fst.size(), tabball_snd.size() );		// <-- ??????????????????????

	tabcell.resize( max( tabball_fst.size(), tabball_snd.size() ) );		// <-- ??????????????????????

	vector<MGSize>::iterator iend = set_intersection( tabball_fst.begin(), tabball_fst.end(), 
													  tabball_snd.begin(), tabball_snd.end(),
													  tabcell.begin() );
	tabcell.erase( iend, tabcell.end() );
	
	return true;
}

	
	
	

//template <> 
bool Kernel<DIM_3D>::FindEdgePipe( vector<MGSize>& tabpipe, const Key<2>& edge)
{
	//TRACE( "inside FindEdgPipe");

	tabpipe.clear();

	// finding a ball for the first node of the edge
	vector<MGSize>	tabball;
	
	if ( ! FindBall( tabball, edge.cFirst() ) )	
	{
		cout << "failed to find an edge pipe [" << edge.cFirst() << ", " << edge.cSecond() << "]\n";
		TRACE( "failed to find an edge pipe [" << edge.cFirst() << ", " << edge.cSecond() << "]\n"  );
		
		return false;
	}

	
	GVect	v1 = mpGrd->cNode( edge.cFirst() ).cPos();
	GVect	v2 = mpGrd->cNode( edge.cSecond() ).cPos();

	//cout << "tabball.size = "<< tabball.size() << "    " << std::flush;

	// check if the edge is existing
	for ( vector<MGSize>::iterator itr = tabball.begin(); itr != tabball.end(); ++itr)
	{
		GCell& cell = mpGrd->rCell( *itr);
		for ( MGSize i=0; i<GCell::SIZE; ++i)
			if ( cell.cNodeId( i) == edge.cSecond() )
				return false;
	}
	
	
	vector<GCell*>	tabstack;
	
	// searching for pipe starting cell/cells
	for ( vector<MGSize>::iterator itr = tabball.begin(); itr != tabball.end(); ++itr)
	{
		GCell& cell = mpGrd->rCell( *itr);
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd);

		if ( tet.Volume() == 0.0)	// fix for flat cell inside the ball
			continue;
		
		MGSize in;
		for ( in=0; in<=GCell::SIZE; ++in)
			if ( cell.cNodeId( in) == edge.cFirst() )
				break;
				
		ASSERT( cell.cNodeId( in) == edge.cFirst() );
		ASSERT( cell.cId() == *itr);
		
		SimplexFace<DIM_3D> tri = tet.GetFace( in);
		
		if ( tri.IsCrossed( v1, v2) )
		{
			cell.Paint();
			tabstack.push_back( &cell);
			tabpipe.push_back( *itr);
		}
	}
	
	if ( tabstack.size() == 0)
		return false;
		
	//ASSERT( tabstack.size() >= 1 && tabstack.size() <= 2);
	
//	cout << "tabstack.size = "<< tabstack.size() << "    " << std::flush;


	// searching for remaining cells
	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd);

		tabstack.pop_back();

		bool bFound = false;
		for ( MGSize i = 0; i <GCell::SIZE; ++i)
			if ( cell.cNodeId( i) == edge.cSecond() )
			{
				//cout << "found\n";
				bFound = true;
			}
		
		if ( bFound)
			continue;



		bFound = false;
		MGSize in;
		for ( in=0; in<GCell::SIZE; ++in)
			if ( cell.cNodeId( in) == edge.cFirst() )
			{
				bFound = true;
				break;
			}

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			if ( bFound && ifc != in)
				continue;
				
			MGSize ic = cell.cCellId( ifc).first;
			
			if ( ic )
			{
				GCell &cellnei = mpGrd->rCell( ic);
				
				if ( cellnei.IsPainted() )
					continue;

				SimplexFace<DIM_3D> tri = tet.GetFace( ifc);
				
				if ( tri.IsCrossed( v1, v2) )
				{
					cellnei.Paint();
					tabstack.push_back( &cellnei);
					tabpipe.push_back( ic);
				}
					
			}
		}	
	}
	
	// cleaning cell flags
	for ( vector<MGSize>::iterator itrp=tabpipe.begin(); itrp!=tabpipe.end(); ++itrp)
		mpGrd->rCell( *itrp).WashPaint();

	//TRACE( "FindEdgPipe finished");
	
	return true;


//	cout << "pipe.size = "<< tabpipe.size() << endl;



// 	Store::File	f( "_edge.plt", "wt");
// 	fprintf( f, "VARIABLES = \"X\", \"Y\", \"Z\"\n" );
// 	fprintf( f, " %10.7lg %10.7lg %10.7lg\n", v1.cX(), v1.cY(), v1.cZ() );
// 	fprintf( f, " %10.7lg %10.7lg %10.7lg\n", v2.cX(), v2.cY(), v2.cZ() );
// 	f.Close();
// 
// 	WriteGrdTEC<DIM_3D>	write(mpGrd);
// 	write.WriteCells( "ball.plt", tabball);
// 	write.WriteCells( "pipe.plt", tabpipe);
// 	write.WriteExploded( "pipe_exp.plt", tabpipe, 2.0);


//	THROW_INTERNAL( "END");	
	
}



//template <> 
bool Kernel<DIM_3D>::FindFaceShell( vector<MGSize>& tabcell, const Key<3>& face, const MGSize& icstart1)
{
	tabcell.clear();
	tabcell.reserve( 2);

	Key<3> facekey = face;
	facekey.Sort();

	vector<MGSize>	tabball;

	if ( ! FindBall( tabball, face.cFirst(), icstart1 ) )
	{
		cout << "failed to find an edge shell [" << face.cFirst() << ", " << face.cSecond() << ", " << face.cThird() << "]\n";
		TRACE( "failed to find an edge shell [" << face.cFirst() << ", " << face.cSecond() << ", " << face.cThird() << "]\n"  );
		
		return false;
	}

	for ( MGSize i=0; i<tabball.size(); ++i)
	{
		const GCell& cell = mpGrd->cCell( tabball[i] );

		for ( MGSize ie=0; ie<GCell::SIZE; ++ie)
		{
			Key<3> key = cell.FaceKey( ie);
			key.Sort();

			if ( key == facekey)
				tabcell.push_back( tabball[i] );
		}

	}

	//if ( tabcell.size() != 2 )
	//{
	//	cout << "tabball.size() = " << tabball.size() << endl;
	//	THROW_INTERNAL( "Kernel<DIM_3D>::FindFaceShell -- tabcell.size() != 2 : " << tabcell.size() );
	//}

	if ( tabcell.size() != 2 )
	{
		TRACE( "Kernel<DIM_3D>::FindFaceShell -- tabcell.size() != 2 : " << tabcell.size() );
		return false;
	}

	
	return true;
}



//template <> 
bool Kernel<DIM_3D>::FindFacePipe( vector<MGSize>& tabcell, vector< Key<2> >& tabedge, const Key<3>& face, const MGSize& icell)
{
	tabcell.clear();


	/////////////////////////////////////////////////////////////
	// finding a ball for the first node of the face
	vector<MGSize>	tabball;

	SimplexFace<DIM_3D> tri( mpGrd->cNode( face.cFirst() ).cPos(), mpGrd->cNode( face.cSecond() ).cPos(), mpGrd->cNode( face.cThird() ).cPos() );

	
	if ( ! FindBall( tabball, face.cFirst(), icell ) )	
	{
		cout << "failed to find an face pipe [" << face.cFirst() << ", " << face.cSecond() << ", " << face.cThird() << " ]" << endl;
		return false;
	}

	
	//cout << "tabball.size = "<< tabball.size() << "    " << std::flush;


	//// check if the edge is existing
	//for ( vector<MGSize>::iterator itr = tabball.begin(); itr != tabball.end(); ++itr)
	//{
	//	GCell& cell = mpGrd->rCell( *itr);
	//	for ( MGSize i=0; i<=GCell::SIZE; ++i)
	//		if ( cell.cNodeId( i) == edge.cSecond() )
	//			return false;
	//}


	cout << "tabball.size = "<< tabball.size() << endl;

	vector<GCell*>	tabstack;
	
	/////////////////////////////////////////////////////////////
	// searching for pipe starting cell/cells
	for ( vector<MGSize>::iterator itr = tabball.begin(); itr != tabball.end(); ++itr)
	{
		GCell& cell = mpGrd->rCell( *itr);
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd);
		
		MGSize in;
		for ( in=0; in<=GCell::SIZE; ++in)
			if ( cell.cNodeId( in) == face.cFirst() )
				break;
				
		ASSERT( cell.cNodeId( in) == face.cFirst() );
		ASSERT( cell.cId() == *itr);
		

		for ( MGSize ie=0; ie<GCell::ESIZE; ++ie)
		{
			MGSize	idn1 = cell.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,0) );
			MGSize	idn2 = cell.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,1) );

			if ( idn1 == face.cFirst() || idn2 == face.cFirst() )
				continue;

			GVect	ve1 = mpGrd->cNode( idn1 ).cPos();
			GVect	ve2 = mpGrd->cNode( idn2 ).cPos();

			if ( tri.CrossedVol( ve1, ve2) > 0.0 )
			{
				cell.Paint();
				tabstack.push_back( &cell);
				tabcell.push_back( *itr);
				break;
			}
		}
	}

	cout << "tabstack.size = "<< tabstack.size() << endl;

	if ( tabstack.size() == 0)
		return false;
		
	//ASSERT( tabstack.size() >= 1 && tabstack.size() <= 2);
	


	/////////////////////////////////////////////////////////////
	// searching for remaining cells
	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd);

		tabstack.pop_back();


		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;
			
			if ( ic )
			{
				GCell &cellnei = mpGrd->rCell( ic);
				
				if ( cellnei.IsPainted() )
					continue;

				for ( MGSize ie=0; ie<GCell::ESIZE; ++ie)
				{
					GVect	ve1 = mpGrd->cNode( cellnei.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,0) ) ).cPos();
					GVect	ve2 = mpGrd->cNode( cellnei.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,1) ) ).cPos();

					if ( tri.CrossedVol( ve1, ve2) > 0.0 )
					{
						cellnei.Paint();
						tabstack.push_back( &cellnei);
						tabcell.push_back( ic);
						break;
					}
				}
					
			}
		}	

	}

	cout << "tabcell.size = "<< tabcell.size() << endl;

	
	/////////////////////////////////////////////////////////////
	// cleaning cell flags
	for ( vector<MGSize>::iterator itrp=tabcell.begin(); itrp!=tabcell.end(); ++itrp)
		mpGrd->rCell( *itrp).WashPaint();

	
	return true;
}




bool Kernel<DIM_3D>::SetAllCellsModified( const bool& b)
{
	for ( Grid<DIM_3D>::ColCell::iterator	itr = mpGrd->rColCell().begin(); itr != mpGrd->rColCell().end(); ++itr)
		itr->SetModified( b);

	return true;
}

bool Kernel<DIM_3D>::FindModifiedCells( vector<MGSize>& tabcell)
{
	tabcell.clear();

	for ( Grid<DIM_3D>::ColCell::const_iterator	itr = mpGrd->cColCell().begin(); itr != mpGrd->cColCell().end(); ++itr)
	{
		if ( itr->IsModified() )
			tabcell.push_back( itr->cId() );
	}

	return true;
}


bool Kernel<DIM_3D>::FindBadCells( vector<MGSize>& tabcell, const MGFloat& thresh)
{
	tabcell.clear();

	for ( Grid<DIM_3D>::ColCell::const_iterator	itr = mpGrd->cColCell().begin(); itr != mpGrd->cColCell().end(); ++itr)
	{
		const GCell &cell = *itr;
		Simplex<DIM_3D> simp = GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd);

		MGFloat b = simp.QMeasureBETAmin();

		if ( b > thresh)
			tabcell.push_back( itr->cId() );

	}

	return true;
}


bool Kernel<DIM_3D>::FindBadCellEdges( vector< Key<2> >& tabedge, const MGFloat& thresh)
{
	vector<MGSize> tabBad;

	FindBadCells( tabBad, thresh);

	tabedge.clear();

	for ( MGSize ic=0; ic<tabBad.size(); ++ic)
	{
		const GCell& cell = mpGrd->cCell( tabBad[ic] );

		for ( MGSize k=0; k<GCell::ESIZE; ++k)
		{
			Key<2> key = cell.EdgeKey( k);
			key.Sort();
			tabedge.push_back( key);
		}
	}

	sort( tabedge.begin(), tabedge.end() );
	vector< Key<2> >::iterator ie = unique( tabedge.begin(), tabedge.end() );
	tabedge.erase( ie, tabedge.end() );

	return true;
}

bool Kernel<DIM_3D>::FindBadEdges( vector< Key<2> >& tabedge, const MGFloat& coefMin, const MGFloat& coefMax)
{
	MetricInterpol<MGFloat,DIM_3D> metInterpol;
	vector< Key<2> > tabtmp;

	GMetric met;
	GMetric::SMtx smet;

	tabedge.clear();

	mpGrd->FindEdges( tabtmp);

	for ( MGSize ie=0; ie<tabtmp.size(); ++ie)
	{
		const GMetric meta = mrGDat.cMetric( tabtmp[ie].cFirst() );
		const GMetric metb = mrGDat.cMetric( tabtmp[ie].cSecond() );

		metInterpol.Reset();
		metInterpol.Add( meta, 0.5);
		metInterpol.Add( metb, 0.5);
		metInterpol.GetResult( smet);
		GMetric mettmp = smet;

		const GVect vtmp =mpGrd->cNode( tabtmp[ie].cSecond() ).cPos() - mpGrd->cNode( tabtmp[ie].cFirst() ).cPos();

		const MGFloat dist = ::sqrt( mettmp.Distance( vtmp) );

		if ( dist < coefMin)	// TODO :: ???
			tabedge.push_back( tabtmp[ie] );
	}

	return true;
}




//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////


//template <> 
MGSize Kernel<DIM_3D>::InsertPoint( vector<MGSize>& tabccell, vector<GFace>& tabcface, const GVect& vct)
{
	typedef pair< Key<2>, FaceNeighbours >	TInFacPair;


	//TRACE3( "new point at %10.6lg %10.6lg %10.6lg", vct.cX(), vct.cY(), vct.cZ() );


	Vect3D	vc;

	if ( mbUseTree )
		for ( MGSize ic=0; ic<tabccell.size(); ++ic)
			if ( mpGrd->cCell(tabccell[ic]).IsTree() )
			{
				vc = GridGeom<DIM_3D>::CreateSimplex( tabccell[ic], *mpGrd).Center();
				mLoc.TreeErase( vc, tabccell[ic]);
			}

	/////////////////////////////////////////////////////////////
	// erasing cavity cells
	mpGrd->rColCell().erase( tabccell );



	// inod is an ID of the new point
	MGSize inod = mpGrd->InsertNode( GNode( vct) );
	
	mpGrd->rNode( inod).rId() = inod;


	/////////////////////////////////////////////////////////////
	// creating new tetrahedra in Delaunay cavity
	static vector<MGSize>	tabnewcell;

	tabnewcell.resize( tabcface.size());

	for ( MGSize icf=0; icf<tabcface.size(); ++icf)
	{
		const GFace &face = tabcface[icf];
		GCell	cell;

		cell.rNodeId(0) = face.cNodeId(0);
		cell.rNodeId(1) = face.cNodeId(1);
		cell.rNodeId(2) = face.cNodeId(2);
		cell.rNodeId(3) = inod;
		cell.rCellId(3) = face.cCellLo();

		//++(mLoc.rCounter());
		mLoc.IncCounter();

		MGSize icell = mpGrd->InsertCell( cell);
		mpGrd->rCell(icell).rId() = icell;

		tabcface[icf].rCellUp().first = icell;
		tabcface[icf].rCellUp().second = 0;

		if ( mLoc.TimeToInsert() )
		{
			mpGrd->rCell(icell).SetTree( true);
			vc = GridGeom<DIM_3D>::CreateSimplex( icell, *mpGrd).Center();
			mLoc.TreeInsert( vc, icell);
		}

		MGSize ineib = cell.cCellId(3).first;

		if ( ineib > 0)
		{
			mpGrd->rCell( ineib).rCellId( cell.cCellId(3).second ).first = icell;
			mpGrd->rCell( ineib).rCellId( cell.cCellId(3).second ).second = 3;
		}


		// flag bModified is used for Weatherill triangulation algo
		mpGrd->rCell(icell).SetModified( true);

		tabnewcell[icf] = icell;
	}





	//Store::File	f( "_cfac.dump", "wt");
	//for ( MGSize icf=0; icf<tabcface.size(); ++icf)
	//{
	//	fprintf( f, "\nFACE ID = %d\n", icf+1);
	//	tabcface[icf].Dump( *mpGrd, f);
	//}
	//f.Close();
	
	/////////////////////////////////////////////////////////////
	// using existing connectivity info from faces to partially connect cavity cells

	for ( MGSize icf=0; icf<tabcface.size(); ++icf)
	{
		const GFace &face = tabcface[icf];
		GCell &cell = mpGrd->rCell( face.cCellUp().first ) ;

		for ( MGSize k=0; k<GFace::SIZE; ++k)
		{
			if ( face.cCellId(k).first != 0)
			{
				const GFace &facenei = tabcface[face.cCellId(k).first-1];

				cell.rCellId( k).first  = facenei.cCellUp().first;
				cell.rCellId( k).second = face.cCellId(k).second;
				
			}
		}
	}

	//f.Open( "_ccell.dump", "wt");
	//for ( MGSize i=mpGrd->CellBegin(); i<mpGrd->CellEnd(); mpGrd->CellInc( i) )
	//{
	//	mpGrd->cCell(i).Dump( *mpGrd, f);
	//}
	//f.Close();



	/////////////////////////////////////////////////////////////
	// reconnecting remaining tetrahedra internal faces

	FaceNeighbours	facenei;

	static vector<TInFacPair>		tabinfac;

	tabinfac.reserve( (MGSize)( 2.4*(MGFloat)tabnewcell.size() ) );
	tabinfac.resize(0);

	for ( MGSize ic=0; ic<tabnewcell.size(); ++ic)
	{
		const GCell &cell = mpGrd->cCell( tabnewcell[ic]);

		for ( MGSize k=0; k<3; ++k)
		{
			if ( cell.cCellId( k).first == 0)
			{
				cell.GetFaceNeisVNOut( facenei, k);
				Key<2> key = cell.FaceKeyL( k);

				tabinfac.push_back( TInFacPair( key, facenei) );
			}
		}
	}

	sort( tabinfac.begin(), tabinfac.end(), lesser_pair<TInFacPair> );
	//sort( tabinfac.begin(), tabinfac.end(), lesser_infacpair );

	//static MGFloat r1=0;
	//static MGFloat r2=0;
	//static MGSize r3=0;
	//if ( r3 < tabinfac.size()/2 )
	//	r3 = tabinfac.size()/2;
	//r1 += tabinfac.size() / 2;
	//r2 += 1;
	//printf( " avr %lf  %d  %lf\n", r1 / r2 , r3, (MGFloat)tabinfac.size() /(MGFloat)tabnewcell.size() );


	MGSize	ip=0;
	//MGSize	ie;

	while ( ip<tabinfac.size())
	{
		//key = tabinfac[ip].first;
		//for ( ie=ip; ie < tabinfac.size(); ++ie)
		//	if ( ! (tabinfac[ie].first == key) )
		//		break;	

		//if ( (ie - ip) != 2)
		//	THROW_INTERNAL( "(ie - ip) is different than 2");

		MGSize ie = ip+1;

		const MGSize& ic1 = tabinfac[ip].second.cCellLo().first;
		const MGSize& if1 = tabinfac[ip].second.cCellLo().second;
		const MGSize& ic2 = tabinfac[ie].second.cCellLo().first;
		const MGSize& if2 = tabinfac[ie].second.cCellLo().second;

		mpGrd->rCell( ic1).rCellId( if1) = tabinfac[ie].second.cCellLo();
		mpGrd->rCell( ic2).rCellId( if2) = tabinfac[ip].second.cCellLo();

		ip = ie+1;
	}


	// check and/or dump
	//mpGrd->CheckConnectivity();
	//mpGrd->Dump( "_grd.dump");


	//WriteTEC<DIM_3D> wtec(mpGrd);
	//wtec.DoWrite( "_test.dat");


	/////////////////////////////////////////////////////////////
	// tabcell should store ids of newly created cells
	tabccell.swap( tabnewcell);

	return inod;
}




MGSize Kernel<DIM_3D>::InsertPoint( vector<MGSize>& tabnewcell, vector<MGSize>& tabccell, vector<GFace>& tabcface, const GVect& vct)
{
	typedef pair< Key<2>, FaceNeighbours >	TInFacPair;


	//TRACE3( "new point at %10.6lg %10.6lg %10.6lg", vct.cX(), vct.cY(), vct.cZ() );


	Vect3D	vc;

	for ( MGSize ic=0; ic<tabccell.size(); ++ic)
		if ( mpGrd->cCell(tabccell[ic]).IsTree() )
		{
			vc = GridGeom<DIM_3D>::CreateSimplex( tabccell[ic], *mpGrd).Center();
			mLoc.TreeErase( vc, tabccell[ic]);
		}

	/////////////////////////////////////////////////////////////
	// erasing cavity cells
	mpGrd->rColCell().erase( tabccell );



	// inod is an ID of the new point
	MGSize inod = mpGrd->InsertNode( GNode( vct) );
	
	mpGrd->rNode( inod).rId() = inod;


	/////////////////////////////////////////////////////////////
	// creating new tetrahedra in Delaunay cavity
	//static vector<MGSize>	tabnewcell;

	tabnewcell.resize( tabcface.size());

	for ( MGSize icf=0; icf<tabcface.size(); ++icf)
	{
		const GFace &face = tabcface[icf];
		GCell	cell;

		cell.rNodeId(0) = face.cNodeId(0);
		cell.rNodeId(1) = face.cNodeId(1);
		cell.rNodeId(2) = face.cNodeId(2);
		cell.rNodeId(3) = inod;
		cell.rCellId(3) = face.cCellLo();

		//++(mLoc.rCounter());
		mLoc.IncCounter();

		MGSize icell = mpGrd->InsertCell( cell);
		mpGrd->rCell(icell).rId() = icell;

		tabcface[icf].rCellUp().first = icell;
		tabcface[icf].rCellUp().second = 0;

		if ( mLoc.TimeToInsert() )
		{
			mpGrd->rCell(icell).SetTree( true);
			vc = GridGeom<DIM_3D>::CreateSimplex( icell, *mpGrd).Center();
			mLoc.TreeInsert( vc, icell);
		}

		MGSize ineib = cell.cCellId(3).first;

		if ( ineib > 0)
		{
			mpGrd->rCell( ineib).rCellId( cell.cCellId(3).second ).first = icell;
			mpGrd->rCell( ineib).rCellId( cell.cCellId(3).second ).second = 3;
		}


		// flag bModified is used for Weatherill triangulation algo
		mpGrd->rCell(icell).SetModified( true);

		tabnewcell[icf] = icell;
	}





	//Store::File	f( "_cfac.dump", "wt");
	//for ( MGSize icf=0; icf<tabcface.size(); ++icf)
	//{
	//	fprintf( f, "\nFACE ID = %d\n", icf+1);
	//	tabcface[icf].Dump( *mpGrd, f);
	//}
	//f.Close();
	
	/////////////////////////////////////////////////////////////
	// using existing connectivity info from faces to partially connect cavity cells

	for ( MGSize icf=0; icf<tabcface.size(); ++icf)
	{
		const GFace &face = tabcface[icf];
		GCell &cell = mpGrd->rCell( face.cCellUp().first ) ;

		for ( MGSize k=0; k<GFace::SIZE; ++k)
		{
			if ( face.cCellId(k).first != 0)
			{
				const GFace &facenei = tabcface[face.cCellId(k).first-1];

				cell.rCellId( k).first  = facenei.cCellUp().first;
				cell.rCellId( k).second = face.cCellId(k).second;
				
			}
		}
	}

	//f.Open( "_ccell.dump", "wt");
	//for ( MGSize i=mpGrd->CellBegin(); i<mpGrd->CellEnd(); mpGrd->CellInc( i) )
	//{
	//	mpGrd->cCell(i).Dump( *mpGrd, f);
	//}
	//f.Close();



	/////////////////////////////////////////////////////////////
	// reconnecting remaining tetrahedra internal faces

	FaceNeighbours	facenei;

	static vector<TInFacPair>		tabinfac;

	tabinfac.reserve( (MGSize)( 2.4*(MGFloat)tabnewcell.size() ) );
	tabinfac.resize(0);

	for ( MGSize ic=0; ic<tabnewcell.size(); ++ic)
	{
		const GCell &cell = mpGrd->cCell( tabnewcell[ic]);

		for ( MGSize k=0; k<3; ++k)
		{
			if ( cell.cCellId( k).first == 0)
			{
				cell.GetFaceNeisVNOut( facenei, k);
				Key<2> key = cell.FaceKeyL( k);

				tabinfac.push_back( TInFacPair( key, facenei) );
			}
		}
	}

	sort( tabinfac.begin(), tabinfac.end(), lesser_pair<TInFacPair> );
	//sort( tabinfac.begin(), tabinfac.end(), lesser_infacpair );

	//static MGFloat r1=0;
	//static MGFloat r2=0;
	//static MGSize r3=0;
	//if ( r3 < tabinfac.size()/2 )
	//	r3 = tabinfac.size()/2;
	//r1 += tabinfac.size() / 2;
	//r2 += 1;
	//printf( " avr %lf  %d  %lf\n", r1 / r2 , r3, (MGFloat)tabinfac.size() /(MGFloat)tabnewcell.size() );


	MGSize	ip=0;
	//MGSize	ie;

	while ( ip<tabinfac.size())
	{
		//key = tabinfac[ip].first;
		//for ( ie=ip; ie < tabinfac.size(); ++ie)
		//	if ( ! (tabinfac[ie].first == key) )
		//		break;	

		//if ( (ie - ip) != 2)
		//	THROW_INTERNAL( "(ie - ip) is different than 2");

		MGSize ie = ip+1;

		const MGSize& ic1 = tabinfac[ip].second.cCellLo().first;
		const MGSize& if1 = tabinfac[ip].second.cCellLo().second;
		const MGSize& ic2 = tabinfac[ie].second.cCellLo().first;
		const MGSize& if2 = tabinfac[ie].second.cCellLo().second;

		mpGrd->rCell( ic1).rCellId( if1) = tabinfac[ie].second.cCellLo();
		mpGrd->rCell( ic2).rCellId( if2) = tabinfac[ip].second.cCellLo();

		ip = ie+1;
	}


	// check and/or dump
	//mpGrd->CheckConnectivity();
	//mpGrd->Dump( "_grd.dump");


	//WriteTEC<DIM_3D> wtec(mpGrd);
	//wtec.DoWrite( "_test.dat");


	/////////////////////////////////////////////////////////////
	// tabcell should store ids of newly created cells
	tabccell.swap( tabnewcell);

	return inod;
}



//template <> 
MGSize Kernel<DIM_3D>::InsertPoint( const GVect& vct)
{
	//typedef pair< Key<2>, FaceNeighbours >	TInFacPair;

	/////////////////////////////////////////////////////////////
	// find cell containing vct
	MGSize	icell = mLoc.Localize( vct);

	//if ( ! mpGrd->cCell(icell).IsInside( vct, *mpGrd) )
	//	THROW_INTERNAL( "Localization failed");
 
	//MGSize	icell = FindCell( vct);
	//TRACE1( "icell = %d\n", icell);


	/////////////////////////////////////////////////////////////
	// search for the delaunay cavity of vct
	static vector<MGSize>	tabccell;
	static vector<GFace>	tabcface;

	tabcface.reserve(60);
	tabccell.reserve(60);


	tabcface.resize(0);
	tabccell.resize(0);

	/////////////////////////////////////////////////////////////
	// tabccel  - cells creating a cavity for the vct
	// tabcface - faces bounding the cavity


	//if ( ! FindCavity( tabccell, tabcface, icell, vct, met) )
	//{
	//	// return NULL if cavity is _not_ found 
	//	return 0;
	//}	

	///
	FindCavity( tabccell, icell, vct);
	CavityCorrection( tabccell, vct, false);

	if ( ! CavityBoundary( tabcface, tabccell ) )
	{
		// return NULL if cavity is _not_ found 
		return 0;
	}	
	///
	//if ( ! FindCavityMetric( tabccell, tabcface, icell, vct, met) )
	//{
	//	// return NULL if cavity is _not_ found 
	//	return 0;
	//}	
	///

	/////////////////////////////////////////////////////////////
	// insert new point into mesh
	return InsertPoint( tabccell, tabcface, vct);
}


//template <> 
MGSize Kernel<DIM_3D>::InsertPointMetric( const GVect& vct, const GMetric& met)
{
	typedef pair< Key<2>, FaceNeighbours >	TInFacPair;

	//cout << "a";

	//TRACE3( "new point at %10.6lg %10.6lg %10.6lg", vct.cX(), vct.cY(), vct.cZ() );


	// find cell containing vct
	MGSize	icell = mLoc.Localize( vct);

	if ( mpGrd->cCell(icell).IsExternal() )
		return 0;

	//if ( ! mpGrd->cCell(icell).IsInside( vct, *mpGrd) )
	//	THROW_INTERNAL( "Localization failed");
 
	//MGSize	icell = FindCell( vct);
	//TRACE1( "icell = %d\n", icell);


	/////////////////////////////////////////////////////////////
	// search for the delaunay cavity of vct
	static vector<MGSize>	tabccell;
	static vector<GFace>	tabcface;

	tabcface.reserve(60);
	tabccell.reserve(60);


	tabcface.resize(0);
	tabccell.resize(0);

	//// tabccel  - cells creating a cavity for the vct
	//// tabcface - faces bounding the cavity
	//if ( ! FindCavityMetric( tabccell, tabcface, icell, vct, met) )
	//{
	//	// return NULL if cavity is _not_ found 
	//	return 0;
	//}	

	///
	FindCavityMetric( tabccell, icell, vct, met);
	CavityCorrection( tabccell, vct);

	if ( ! CavityBoundary( tabcface, tabccell ) )
	{
		// return NULL if cavity is _not_ found 
		return 0;
	}	
	///
	//if ( ! FindCavityMetric( tabccell, tabcface, icell, vct, met) )
	//{
	//	// return NULL if cavity is _not_ found 
	//	return 0;
	//}	
	///


	return InsertPoint( tabccell, tabcface, vct);

/*
	Vect3D	vc;

	for ( MGSize ic=0; ic<tabccell.size(); ++ic)
		if ( mpGrd->cCell(tabccell[ic]).cbTree() )
		{
			vc = GridGeom::Tet( tabccell[ic], *mpGrd).Center();
			mLoc.TreeErase( vc, tabccell[ic]);
		}

	/////////////////////////////////////////////////////////////
	// erasing cavity cells
	mpGrd->rColCell().erase( tabccell );


	// inod is an ID of the new point
	MGSize inod = mpGrd->InsertNode( GNode( vct) );
	
	mpGrd->rNode( inod).rId() = inod;


	/////////////////////////////////////////////////////////////
	// creating new tetrahedra in Delaunay cavity
	static vector<MGSize>	tabnewcell;

	tabnewcell.resize( tabcface.size());

	for ( MGSize icf=0; icf<tabcface.size(); ++icf)
	{
		const GFace &face = tabcface[icf];
		GCell	cell;

		cell.rNodeId(0) = face.cNodeId(0);
		cell.rNodeId(1) = face.cNodeId(1);
		cell.rNodeId(2) = face.cNodeId(2);
		cell.rNodeId(3) = inod;
		cell.rCellId(3) = face.cCellLo();

		++(mLoc.rCounter());

		MGSize icell = mpGrd->InsertCell( cell);
		mpGrd->rCell(icell).rId() = icell;

		tabcface[icf].rCellUp().first = icell;
		tabcface[icf].rCellUp().second = 0;

		if ( mLoc.TimeToInsert() )
		{
			mpGrd->rCell(icell).rbTree() = true;
			vc = GridGeom::Tet( icell, *mpGrd).Center();
			mLoc.TreeInsert( vc, icell);
		}

		MGSize ineib = cell.cCellId(3).first;

		if ( ineib > 0)
		{
			mpGrd->rCell( ineib).rCellId( cell.cCellId(3).second ).first = icell;
			mpGrd->rCell( ineib).rCellId( cell.cCellId(3).second ).second = 3;
		}


		// flag bModified is used for Weatherill triangulation algo
		mpGrd->rCell(icell).rbModified() = true;

		tabnewcell[icf] = icell;
	}





	//Store::File	f( "_cfac.dump", "wt");
	//for ( MGSize icf=0; icf<tabcface.size(); ++icf)
	//{
	//	fprintf( f, "\nFACE ID = %d\n", icf+1);
	//	tabcface[icf].Dump( *mpGrd, f);
	//}
	//f.Close();
	
	/////////////////////////////////////////////////////////////
	// using existing connectivity info from faces to partially connect cavity cells

	for ( MGSize icf=0; icf<tabcface.size(); ++icf)
	{
		const GFace &face = tabcface[icf];
		GCell &cell = mpGrd->rCell( face.cCellUp().first ) ;

		for ( MGSize k=0; k<GFace::SIZE; ++k)
		{
			if ( face.cCellId(k).first != 0)
			{
				const GFace &facenei = tabcface[face.cCellId(k).first-1];

				cell.rCellId( k).first  = facenei.cCellUp().first;
				cell.rCellId( k).second = face.cCellId(k).second;
				
			}
		}
	}

	//f.Open( "_ccell.dump", "wt");
	//for ( MGSize i=mpGrd->CellBegin(); i<mpGrd->CellEnd(); mpGrd->CellInc( i) )
	//{
	//	mpGrd->cCell(i).Dump( *mpGrd, f);
	//}
	//f.Close();



	/////////////////////////////////////////////////////////////
	// reconnecting remaining tetrahedra internal faces

	FaceNeighbours	facenei;

	static vector<TInFacPair>		tabinfac;

	tabinfac.reserve( (MGSize)( 2.4*(MGFloat)tabnewcell.size() ) );
	tabinfac.resize(0);

	for ( MGSize ic=0; ic<tabnewcell.size(); ++ic)
	{
		const GCell &cell = mpGrd->cCell( tabnewcell[ic]);

		for ( MGSize k=0; k<3; ++k)
		{
			if ( cell.cCellId( k).first == 0)
			{
				cell.GetFaceNeisVNOut( facenei, k);
				Key<2> key = cell.FaceKeyL( k);

				tabinfac.push_back( TInFacPair( key, facenei) );
			}
		}
	}

	sort( tabinfac.begin(), tabinfac.end(), lesser_pair<TInFacPair> );
	//sort( tabinfac.begin(), tabinfac.end(), lesser_infacpair );

	//static MGFloat r1=0;
	//static MGFloat r2=0;
	//static MGSize r3=0;
	//if ( r3 < tabinfac.size()/2 )
	//	r3 = tabinfac.size()/2;
	//r1 += tabinfac.size() / 2;
	//r2 += 1;
	//printf( " avr %lf  %d  %lf\n", r1 / r2 , r3, (MGFloat)tabinfac.size() /(MGFloat)tabnewcell.size() );


	MGSize	ip=0;
	//MGSize	ie;

	while ( ip<tabinfac.size())
	{
		//key = tabinfac[ip].first;
		//for ( ie=ip; ie < tabinfac.size(); ++ie)
		//	if ( ! (tabinfac[ie].first == key) )
		//		break;	

		//if ( (ie - ip) != 2)
		//	THROW_INTERNAL( "(ie - ip) is different than 2");

		MGSize ie = ip+1;

		const MGSize& ic1 = tabinfac[ip].second.cCellLo().first;
		const MGSize& if1 = tabinfac[ip].second.cCellLo().second;
		const MGSize& ic2 = tabinfac[ie].second.cCellLo().first;
		const MGSize& if2 = tabinfac[ie].second.cCellLo().second;

		mpGrd->rCell( ic1).rCellId( if1) = tabinfac[ie].second.cCellLo();
		mpGrd->rCell( ic2).rCellId( if2) = tabinfac[ip].second.cCellLo();

		ip = ie+1;
	}

	//DArray< Cell<DIM_3D> >&	array = mpGrd->rCollCell();
	//array.Dump();
	//printf( "no of cell = %d\n\n", mpGrd->SizeCellTab() );


	// check and/or dump
	//mpGrd->CheckConnectivity();
	//mpGrd->Dump( "_grd.dump");


	//WriteTEC<DIM_3D> wtec(mpGrd);
	//wtec.DoWrite( "_test.dat");

	return inod;
*/
}



MGSize Kernel<DIM_3D>::InsertPointMetricProtect( const GVect& vct, const GMetric& met, const set< Key<3> >& setf)
{
	typedef pair< Key<2>, FaceNeighbours >	TInFacPair;

	// find cell containing vct
	MGSize	icell = mLoc.Localize( vct);

	/////////////////////////////////////////////////////////////
	// search for the delaunay cavity of vct
	static vector<MGSize>	tabccell;
	static vector<GFace>	tabcface;

	tabcface.reserve(60);
	tabccell.reserve(60);


	tabcface.resize(0);
	tabccell.resize(0);

	FindCavityMetricProtect( tabccell, icell, vct, met, setf);
	CavityCorrection( tabccell, vct);

	if ( ! CavityBoundary( tabcface, tabccell ) )
	{
		// return NULL if cavity is _not_ found 
		return 0;
	}	

	return InsertPoint( tabccell, tabcface, vct);
}


void Kernel<DIM_3D>::CavityBoundaryProtectEdges( vector<GFace>& tabcface, vector<MGSize>& tabccell, const GVect& vct, const set< Key<2> >& sete)
{
	bool bLoop;

	do 
	{
		bLoop = false;
		//// dump
		//WriteGrdTEC<DIM_3D>	write( mpGrd );
		//write.WriteCells( "_cavity_cells_.dat", tabccell);

		set<MGSize>		setbase;
		set< Key<2> >	setinedge, setbndedge;
		set< Key<3> >	setinface, setbndface;

		// find edges and faces on the cavity boundary
		for ( MGSize i=0; i<tabcface.size(); ++i)
		{
			const GFace& face = tabcface[i];
			Key<3> kface( face.cNodeId(0), face.cNodeId(1), face.cNodeId(2) );
			kface.Sort();

			setbndface.insert( kface);

			for ( MGSize ifc=0; ifc<3; ++ifc)
			{
				Key<2> kedge( face.cNodeId( Simplex<DIM_2D>::cFaceConn( ifc, 0) ), face.cNodeId( Simplex<DIM_2D>::cFaceConn( ifc, 1) ) );
				kedge.Sort();

				setbndedge.insert( kedge);
			}
		}

		// find base
		// find cavity internal edges and faces
		for ( MGSize i=0; i<tabccell.size(); ++i)
		{
			const GCell &cell = mpGrd->cCell( tabccell[i]);

			if ( ! cell.IsPainted() )
				continue;

			Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd);
			//MGFloat zero = ::pow( tet.Volume(), 1./3. ) * 1.0e-10;

			if ( tet.IsInside( vct) )
				setbase.insert( tabccell[i] );

			for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
			{
				Key<3> kface = cell.FaceKey( ifc);
				kface.Sort();
				if ( setbndface.find( kface) == setbndface.end() )
					setinface.insert( kface);
			}

			for ( MGSize iec=0; iec<GCell::ESIZE; ++iec)
			{
				Key<2> kedge = cell.EdgeKey( iec);
				kedge.Sort();
				if ( setbndedge.find( kedge) == setbndedge.end() )
					setinedge.insert( kedge);
			}
		}

		// find edges which needs protection
		Key<2> edgepro;
		//vector< Key<2> > tabepro;
		for ( set< Key<2> >::const_iterator citre=setinedge.begin(); citre!=setinedge.end(); ++citre)
		{
			const Key<2>& edge = *citre;

			if ( sete.find( edge ) != sete.end() )
			{
				edgepro = edge;
				bLoop = true;
				//tabepro.push_back( edge);
			}
		}


		// find cell to remove from cavity
		if ( bLoop )
		{
			MGFloat dist = 0.0;
			GCell* pcell = NULL;

			for ( vector<MGSize>::iterator itr = tabccell.begin(); itr != tabccell.end(); ++itr)
			{
				GCell &cell = mpGrd->rCell( *itr);

				if ( ! cell.IsPainted() )
					continue;

				if ( setbase.find( *itr) != setbase.end() )
					continue;

				Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd);
				MGFloat curdist = ( tet.Center() - vct).module();

				bool found = false;
				for ( MGSize iec=0; iec<GCell::ESIZE; ++iec)
				{
					Key<2> kedge = cell.EdgeKey( iec);
					kedge.Sort();

					if ( kedge == edgepro )
					{
						found = true;
						break;
					}
				}

				if ( found && curdist > dist)
				{
					pcell = &cell;
					//cell.WashPaint();
					//tabccell.erase( itr);
				}
			}

			if ( pcell )
				pcell->WashPaint();
			else
				bLoop = false;
				//THROW_INTERNAL( "cell not found (edge protect algo)");
		}


		CavityCorrectionQ( tabccell, vct, false);

		//{
		//	vector<MGSize> tmptabccell;
		//	for ( vector<MGSize>::iterator itr = tabccell.begin(); itr != tabccell.end(); ++itr)
		//	{
		//		GCell &cell = mpGrd->rCell( *itr);

		//		if ( cell.IsPainted() )
		//			tmptabccell.push_back( *itr);
		//	}

		//	cout << "[3] tmptabccell.size() = " << tmptabccell.size() << endl;
		//}



		tabcface.clear();

		if ( ! CavityBoundary( tabcface, tabccell ) )
		{
			// return NULL if cavity is _not_ found 

			//WriteGrdTEC<DIM_3D>	write( mpGrd );
			//write.WriteCells( "_cavity_cells_.dat", tmptabccell, "cavity");
			//write.WriteCells( "_base_cells_.dat", tmptabbase, "base");

			cout << "tabcface.size() = " << tabcface.size() << endl;
			cout << "tabccell.size() = " << tabccell.size() << " " << endl;
			cout << "[1] cavity is _not_ found" << endl;
			return;
		}
	}
	while ( bLoop );

}


void Kernel<DIM_3D>::CavityBoundaryProtectFaces( vector<GFace>& tabcface, vector<MGSize>& tabccell, const GVect& vct, const set< Key<3> >& setf)
{
	bool bLoop;

	do 
	{
		bLoop = false;
		//// dump
		//WriteGrdTEC<DIM_3D>	write( mpGrd );
		//write.WriteCells( "_cavity_cells_.dat", tabccell);

		set<MGSize>		setbase;
		set< Key<3> >	setinface, setbndface;

		// find edges and faces on the cavity boundary
		for ( MGSize i=0; i<tabcface.size(); ++i)
		{
			const GFace& face = tabcface[i];
			Key<3> kface( face.cNodeId(0), face.cNodeId(1), face.cNodeId(2) );
			kface.Sort();

			setbndface.insert( kface);
		}

		// find base
		// find cavity internal edges and faces
		for ( MGSize i=0; i<tabccell.size(); ++i)
		{
			const GCell &cell = mpGrd->cCell( tabccell[i]);

			if ( ! cell.IsPainted() )
				continue;

			Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd);
			//MGFloat zero = ::pow( tet.Volume(), 1./3. ) * 1.0e-10;

			if ( tet.IsInside( vct) )
				setbase.insert( tabccell[i] );

			for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
			{
				Key<3> kface = cell.FaceKey( ifc);
				kface.Sort();
				if ( setbndface.find( kface) == setbndface.end() )
					setinface.insert( kface);
			}
		}

		// find faces which needs protection
		Key<3> facepro;

		for ( set< Key<3> >::const_iterator citre=setinface.begin(); citre!=setinface.end(); ++citre)
		{
			const Key<3>& face = *citre;

			if ( setf.find( face ) != setf.end() )
			{
				facepro = face;
				bLoop = true;
				break;
				//tabepro.push_back( edge);
			}
		}


		// find cell to remove from cavity
		if ( bLoop )
		{
			MGFloat dist = 0.0;
			GCell* pcell = NULL;

			for ( vector<MGSize>::iterator itr = tabccell.begin(); itr != tabccell.end(); ++itr)
			{
				GCell &cell = mpGrd->rCell( *itr);

				if ( ! cell.IsPainted() )
					continue;

				if ( setbase.find( *itr) != setbase.end() )
					continue;

				Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd);
				MGFloat curdist = ( tet.Center() - vct).module();

				bool found = false;
				for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
				{
					Key<3> kface = cell.FaceKey( ifc);
					kface.Sort();

					if ( kface == facepro )
					{
						found = true;
						break;
					}
				}

				if ( found && curdist > dist)
				{
					pcell = &cell;
					//cell.WashPaint();
					//tabccell.erase( itr);
				}
			}

			if ( pcell )
				pcell->WashPaint();
			else
				bLoop = false;
				//THROW_INTERNAL( "cell not found (face protect algo)");
		}


		CavityCorrectionQ( tabccell, vct, false);

		//{
		//	vector<MGSize> tmptabccell;
		//	for ( vector<MGSize>::iterator itr = tabccell.begin(); itr != tabccell.end(); ++itr)
		//	{
		//		GCell &cell = mpGrd->rCell( *itr);

		//		if ( cell.IsPainted() )
		//			tmptabccell.push_back( *itr);
		//	}

		//	cout << "[3] tmptabccell.size() = " << tmptabccell.size() << endl;
		//}



		tabcface.clear();

		if ( ! CavityBoundary( tabcface, tabccell ) )
		{
			// return NULL if cavity is _not_ found 

			//WriteGrdTEC<DIM_3D>	write( mpGrd );
			//write.WriteCells( "_cavity_cells_.dat", tmptabccell, "cavity");
			//write.WriteCells( "_base_cells_.dat", tmptabbase, "base");

			cout << "tabcface.size() = " << tabcface.size() << endl;
			cout << "tabccell.size() = " << tabccell.size() << " " << endl;
			cout << "[1] cavity is _not_ found" << endl;
			return;
		}
	}
	while ( bLoop );
}



MGSize Kernel<DIM_3D>::InsertPointProtect( const GVect& vct, const set< Key<2> >& sete, const set< Key<3> >& setf )
{
	/////////////////////////////////////////////////////////////
	// find cell containing vct
	MGSize	icell = mLoc.Localize( vct);

	/////////////////////////////////////////////////////////////
	// search for the delaunay cavity of vct
	static vector<MGSize>	tabccell;
	static vector<GFace>	tabcface;

	tabcface.reserve(60);
	tabccell.reserve(60);

	tabcface.resize(0);
	tabccell.resize(0);

	/////////////////////////////////////////////////////////////
	// tabccel  - cells creating a cavity for the vct
	// tabcface - faces bounding the cavity

	FindCavityProtect( tabccell, icell, vct, setf);

	/////////////////////////////////////////////////////////////
	// cavity corrections
	//CavityCorrectionQ( tabccell, vct, false);
	CavityCorrection( tabccell, vct, false);

	if ( ! CavityBoundary( tabcface, tabccell ) )
	{
		// return NULL if cavity is _not_ found 
		cout << "[0] cavity is _not_ found" << endl;
		return 0;
	}	

	//CavityBoundaryProtectEdges( tabcface, tabccell, vct, sete);
	//CavityBoundaryProtectFaces( tabcface, tabccell, vct, setf);

	//WriteGrdTEC<DIM_3D>	write( mpGrd );
	//write.WriteCells( "_cavity_cells_.dat", tabccell, "cavity");


	/////////////////////////////////////////////////////////////
	// insert new point into mesh
	return InsertPoint( tabccell, tabcface, vct);
}





//////////////////////////////////

bool Kernel<DIM_3D>::FindCavityBase( vector<MGSize>& tabcell, const vector<MGSize>& tabbase, const MGSize& icell, const GVect& vct)
{
	static vector<GCell*>	tabstack;

	tabstack.reserve(50);
	tabcell.reserve(50);

	tabstack.resize(0);
	tabcell.resize(0);

	for ( MGSize i=0; i<tabbase.size(); ++i)
	{
		GCell& cell = mpGrd->rCell( tabbase[i] );
		cell.Paint();
		tabstack.push_back( &cell);
		tabcell.push_back( tabbase[i]);
	}

	//tabcell.push_back( icell);
	//tabstack.push_back( &mpGrd->rCell( icell));

	//tabstack.back()->Paint();



	/////////////////////////////////////////////////////////////
	// searching for the elements in the cavity

	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;

			if ( ic)
			{
				GCell &cellnei = mpGrd->rCell( ic);

				//metric.Write();
				//THROW_INTERNAL("BREAK");
				Simplex<DIM_3D>	tet = GridGeom<DIM_3D>::CreateSimplex( cellnei, *mpGrd);

				// TODO :: add some tolerance not to reconnect the cospherical points
				//if ( (! cellnei.IsPainted()) && ( tet.IsInsideSphere( vct) > 1.e-14 ) )
				if ( (! cellnei.IsPainted()) && 
					 (! cellnei.IsExternal()) && 
					 //( tet.IsInsideSphere( vct) > 0.0 || tet.Volume() <= 0.0 ) ) 
					 ( tet.IsInsideSphere( vct) > 0 /*|| tet.Volume() <= 0.0*/ ) ) 
				{
					tabcell.push_back( cellnei.cId() );
					tabstack.push_back( &cellnei );
					cellnei.Paint();
				}
			}
		}
	}

	if ( ! tabcell.size() )
	{
		TRACE( "Kernel::FindCavity - failed: empty cavity"); 
		return false;
	}

	return true;
}



bool Kernel<DIM_3D>::FindCavity( vector<MGSize>& tabcell, const MGSize& icell, const GVect& vct)
{
	static vector<GCell*>	tabstack;

	tabstack.reserve(50);
	tabcell.reserve(50);

	tabstack.resize(0);
	tabcell.resize(0);


	FindBase( tabcell, vct, icell);
	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		GCell& cell = mpGrd->rCell( tabcell[i] );
		cell.Paint();
		tabstack.push_back( &cell);
	}

	//tabcell.push_back( icell);
	//tabstack.push_back( &mpGrd->rCell( icell));

	//tabstack.back()->Paint();



	/////////////////////////////////////////////////////////////
	// searching for the elements in the cavity

	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;

			if ( ic)
			{
				GCell &cellnei = mpGrd->rCell( ic);

				//metric.Write();
				//THROW_INTERNAL("BREAK");
				Simplex<DIM_3D>	tet = GridGeom<DIM_3D>::CreateSimplex( cellnei, *mpGrd);

				// TODO :: add some tolerance not to reconnect the cospherical points
				//if ( (! cellnei.IsPainted()) && ( tet.IsInsideSphere( vct) > 1.e-14 ) )
				if ( (! cellnei.IsPainted()) && 
					 (! cellnei.IsExternal()) && 
					 //( tet.IsInsideSphere( vct) > 0.0 || tet.Volume() <= 0.0 ) ) 
					 ( tet.IsInsideSphere( vct) > 0 /*|| tet.Volume() <= 0.0*/ ) ) 
				{
					tabcell.push_back( cellnei.cId() );
					tabstack.push_back( &cellnei );
					cellnei.Paint();
				}
			}
		}
	}

	if ( ! tabcell.size() )
	{
		TRACE( "Kernel::FindCavity - failed: empty cavity"); 
		return false;
	}

	return true;
}


bool Kernel<DIM_3D>::FindCavityProtect( vector<MGSize>& tabcell, const MGSize& icell, const GVect& vct, const set< Key<3> >& setf)
{
	static vector<GCell*>	tabstack;

	tabstack.reserve(50);
	tabcell.reserve(50);

	tabstack.resize(0);
	tabcell.resize(0);

	FindBase( tabcell, vct, icell);
	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		GCell& cell = mpGrd->rCell( tabcell[i] );
		cell.Paint();
		tabstack.push_back( &cell);
	}

	//tabcell.push_back( icell);
	//tabstack.push_back( &mpGrd->rCell( icell));

	//tabstack.back()->Paint();



	/////////////////////////////////////////////////////////////
	// searching for the elements in the cavity

	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;

			Key<3> face = cell.FaceKey( ifc);
			face.Sort();

			if ( ic && setf.find( face) == setf.end() )
			{
				GCell &cellnei = mpGrd->rCell( ic);

				//metric.Write();
				//THROW_INTERNAL("BREAK");
				Simplex<DIM_3D>	tet = GridGeom<DIM_3D>::CreateSimplex( cellnei, *mpGrd);

				// TODO :: add some tolerance not to reconnect the cospherical points
				if ( (! cellnei.IsPainted()) && 
					 (! cellnei.IsExternal()) && 
					 ( tet.IsInsideSphere( vct) >= 0 /*|| tet.Volume() <= 0.0*/ ) ) 
				{
					tabcell.push_back( cellnei.cId() );
					tabstack.push_back( &cellnei );
					cellnei.Paint();
				}

			}
		}
	}

	if ( ! tabcell.size() )
	{
		TRACE( "Kernel::FindCavity - failed: empty cavity"); 
		return false;
	}

	return true;
}


bool Kernel<DIM_3D>::FindCavityMetricBase( vector<MGSize>& tabcell, const vector<MGSize>& tabbase, const MGSize& icell, const GVect& vct, const GMetric& met)
{

	//if ( ! mpGrd->cCell( icell).IsInsideSphere( vct, *mpGrd) )
	//{
	//	printf( "\nnew point at %10.6lg %10.6lg %10.6lg \n", vct.cX(), vct.cY(), vct.cZ() );
	//	mpGrd->cCell( icell).Dump( *mpGrd);
	//	THROW_INTERNAL( "IsInsideSphere failed for the point Base !!!");
	//}


	static vector<GCell*>	tabstack;

	tabstack.reserve(50);
	tabcell.reserve(50);

	tabstack.resize(0);
	tabcell.resize(0);

	for ( MGSize i=0; i<tabbase.size(); ++i)
	{
		GCell& cell = mpGrd->rCell( tabbase[i] );
		cell.Paint();
		tabstack.push_back( &cell);
		tabcell.push_back( tabbase[i]);
	}


	//tabcell.push_back( icell);
	//tabstack.push_back( &mpGrd->rCell( icell));

	//tabstack.back()->Paint();



	/////////////////////////////////////////////////////////////
	// searching for the elements in the cavity
	static GMetric	tabmetric[GCell::SIZE];

	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;

			if ( ic)
			{
				GCell &cellnei = mpGrd->rCell( ic);

				const GMetric& metric = met;

				for ( MGSize im=0; im<GCell::SIZE; ++im)
					tabmetric[im] = met;//[ mpGrd->cNode(im).cId() ]

				//metric.Write();
				//THROW_INTERNAL("BREAK");
				Simplex<DIM_3D>	tet = GridGeom<DIM_3D>::CreateSimplex( cellnei, *mpGrd);
				//const MGFloat beta = tet.QMeasureBETAmin();


				// TODO :: add some tolerance not to reconnect the cospherical points
				//if ( (! cellnei.IsPainted()) && ( tet.IsInsideSphere( vct) > 1.e-14 ) )
				if ( (! cellnei.IsPainted()) && 
					 (! cellnei.IsExternal()) && 
					 ( tet.IsInsideSphereMetric( vct, metric, tabmetric) > 0 ) ) 
					 //( tet.IsInsideSphereMetric( vct, metric, tabmetric) > 0 || tet.Volume() <= 0.0 ) ) 
					 //( tet.IsInsideSphereMetric( vct, metric, tabmetric) > 0 || beta > 1.0e4 ) ) 
					 //( tet.IsInsideSphereMetric( vct, metric, tabmetric) > 0 || tet.Volume() <= 1.0e-17 ) ) 
				{
					tabcell.push_back( cellnei.cId() );
					tabstack.push_back( &cellnei );
					cellnei.Paint();
				}
			}
		}
	}

	if ( ! tabcell.size() )
	{
		TRACE( "Kernel::FindCavity - failed: empty cavity"); 
		return false;
	}

	return true;

}


bool Kernel<DIM_3D>::FindCavityMetric( vector<MGSize>& tabcell, const MGSize& icell, const GVect& vct, const GMetric& met)
{

	//if ( ! mpGrd->cCell( icell).IsInsideSphere( vct, *mpGrd) )
	//{
	//	printf( "\nnew point at %10.6lg %10.6lg %10.6lg \n", vct.cX(), vct.cY(), vct.cZ() );
	//	mpGrd->cCell( icell).Dump( *mpGrd);
	//	THROW_INTERNAL( "IsInsideSphere failed for the point Base !!!");
	//}


	static vector<GCell*>	tabstack;

	tabstack.reserve(50);
	tabcell.reserve(50);

	tabstack.resize(0);
	tabcell.resize(0);


	tabcell.push_back( icell);
	tabstack.push_back( &mpGrd->rCell( icell));

	tabstack.back()->Paint();



	/////////////////////////////////////////////////////////////
	// searching for the elements in the cavity
	static GMetric	tabmetric[GCell::SIZE];

	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;

			if ( ic)
			{
				GCell &cellnei = mpGrd->rCell( ic);

				const GMetric& metric = met;

				for ( MGSize im=0; im<GCell::SIZE; ++im)
					tabmetric[im] = met;//[ mpGrd->cNode(im).cId() ]

				//metric.Write();
				//THROW_INTERNAL("BREAK");
				Simplex<DIM_3D>	tet = GridGeom<DIM_3D>::CreateSimplex( cellnei, *mpGrd);
				//const MGFloat beta = tet.QMeasureBETAmin();


				// TODO :: add some tolerance not to reconnect the cospherical points
				//if ( (! cellnei.IsPainted()) && ( tet.IsInsideSphere( vct) > 1.e-14 ) )
				if ( (! cellnei.IsPainted()) && 
					 (! cellnei.IsExternal()) && 
					 ( tet.IsInsideSphereMetric( vct, metric, tabmetric) > 0 ) ) 
					 //( tet.IsInsideSphereMetric( vct, metric, tabmetric) > 0 || tet.Volume() <= 0.0 ) ) 
					 //( tet.IsInsideSphereMetric( vct, metric, tabmetric) > 0 || beta > 1.0e4 ) ) 
					 //( tet.IsInsideSphereMetric( vct, metric, tabmetric) > 0 || tet.Volume() <= 1.0e-17 ) ) 
				{
					tabcell.push_back( cellnei.cId() );
					tabstack.push_back( &cellnei );
					cellnei.Paint();
				}
			}
		}
	}

	if ( ! tabcell.size() )
	{
		TRACE( "Kernel::FindCavity - failed: empty cavity"); 
		return false;
	}

	return true;

}


bool Kernel<DIM_3D>::FindCavityMetricProtect( vector<MGSize>& tabcell, const MGSize& icell, const GVect& vct, const GMetric& met, const set< Key<3> >& setf)
{
	static vector<GCell*>	tabstack;

	tabstack.reserve(50);
	tabcell.reserve(50);

	tabstack.resize(0);
	tabcell.resize(0);


	tabcell.push_back( icell);
	tabstack.push_back( &mpGrd->rCell( icell));

	tabstack.back()->Paint();



	/////////////////////////////////////////////////////////////
	// searching for the elements in the cavity
	static GMetric	tabmetric[GCell::SIZE];

	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;

			Key<3> face = cell.FaceKey( ifc);
			face.Sort();

			//if ( ic)
			if ( ic && setf.find( face) == setf.end() )
			{
				GCell &cellnei = mpGrd->rCell( ic);

				const GMetric& metric = met;

				for ( MGSize im=0; im<GCell::SIZE; ++im)
					tabmetric[im] = met;//[ mpGrd->cNode(im).cId() ]

				//metric.Write();
				//THROW_INTERNAL("BREAK");
				Simplex<DIM_3D>	tet = GridGeom<DIM_3D>::CreateSimplex( cellnei, *mpGrd);
				//const MGFloat beta = tet.QMeasureBETAmin();


				// TODO :: add some tolerance not to reconnect the cospherical points
				//if ( (! cellnei.IsPainted()) && ( tet.IsInsideSphere( vct) > 1.e-14 ) )
				if ( (! cellnei.IsPainted()) && 
					 (! cellnei.IsExternal()) && 
					 ( tet.IsInsideSphereMetric( vct, metric, tabmetric) > 0 ) ) 
					 //( tet.IsInsideSphereMetric( vct, metric, tabmetric) > 0 || tet.Volume() <= 0.0 ) ) 
					 //( tet.IsInsideSphereMetric( vct, metric, tabmetric) > 0 || beta > 1.0e4 ) ) 
					 //( tet.IsInsideSphereMetric( vct, metric, tabmetric) > 0 || tet.Volume() <= 1.0e-17 ) ) 
				{
					tabcell.push_back( cellnei.cId() );
					tabstack.push_back( &cellnei );
					cellnei.Paint();
				}
			}
		}
	}

	if ( ! tabcell.size() )
	{
		TRACE( "Kernel::FindCavity - failed: empty cavity"); 
		return false;
	}

	return true;
}




void Kernel<DIM_3D>::CavityCorrectionQ( const vector<MGSize>& tabcell, const GVect& vct, const bool& bmin)
{
	THROW_INTERNAL( "DON'T USE THIS !!!");

	vector<MGSize> tabbase;

	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		const GCell	&cell = mpGrd->cCell( tabcell[i] );
		Simplex<DIM_3D> tetcell = GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd);

		if ( tetcell.IsInside( vct) )
			tabbase.push_back( tabcell[i] );
	}

	sort( tabbase.begin(), tabbase.end() );

	MGFloat qbase = 0.0;

	for ( MGSize i=0; i<tabbase.size(); ++i)
	{
		GCell	&cell = mpGrd->rCell( tabbase[i] );
		Simplex<DIM_3D> tetcell = GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd);

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			const MGSize inei = cell.cCellId( ifc).first;

			if ( ! binary_search( tabbase.begin(), tabbase.end(), inei ) )
			{
				Simplex<DIM_3D>		tet( mpGrd->cNode( cell.cFaceNodeId( ifc, 0) ).cPos(), 
										 mpGrd->cNode( cell.cFaceNodeId( ifc, 2) ).cPos(),
										 mpGrd->cNode( cell.cFaceNodeId( ifc, 1) ).cPos(),
										 vct );

				if ( tet.QMeasureBETAmin() > qbase )
				{
					qbase = tet.QMeasureBETAmin();
				}
			}
		}
	}

	//cout << endl;
	//cout << "gbase = " << qbase << endl;
	//cout << "tabbase.size() = " << tabbase.size() << endl;



	/////////////////////////////////////////////////////////////
	// correction for star-shape of the cavity

	bool	bOk;
	bool	bExit;

	do
	{
		bExit = true;

		for ( MGSize ic=0; ic<tabcell.size(); ++ic)
		{
			GCell	&cell = mpGrd->rCell( tabcell[ic] );
			Simplex<DIM_3D> tetcell = GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd);

			if ( tetcell.IsInside( vct) )
				continue;


			if ( cell.IsPainted() )
			{

				for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
				{
					const MGSize inei = cell.cCellId( ifc).first;

					bOk = false;

 					if ( ! inei)
 						bOk = true;
 					else if ( ! mpGrd->cCell( inei).IsPainted() )
 						bOk = true;


					if ( bOk )
					{
						Simplex<DIM_3D>		tet( mpGrd->cNode( cell.cFaceNodeId( ifc, 0) ).cPos(), 
												 mpGrd->cNode( cell.cFaceNodeId( ifc, 2) ).cPos(),
												 mpGrd->cNode( cell.cFaceNodeId( ifc, 1) ).cPos(),
												 vct );

						// bad cell has negative volume
						if ( tet.QMeasureBETAmin() >= 0.5*qbase || tet.Volume() <= 0.0 )
						{
							cell.WashPaint();
							bExit = false;
						}
					}
				}
			}
		}
	}
	while ( ! bExit);

	// TODO :: check the cavity - hanging nodes, disconnected cells

}



void Kernel<DIM_3D>::CavityCorrection( const vector<MGSize>& tabcell, const GVect& vct, const bool& bmin)
{

	/////////////////////////////////////////////////////////////
	// set min cell volume
	vector<MGSize>::const_iterator itc = tabcell.begin();
	MGFloat volmin = 0.0;
	//volmin = GridGeom<DIM_3D>::CreateSimplex( *itc, *mpGrd ).Volume();
	
	//for ( ++itc; itc != tabcell.end(); ++itc)
	//{
	//	volmin = min( volmin, GridGeom<DIM_3D>::CreateSimplex( *itc, *mpGrd ).Volume() );
	//}

	//volmin *= 1.0e-16;


	/////////////////////////////////////
	// TODO :: check this !!!!!!

	/////////////////////////////////////////////////////////////
	// checking if all cavity cells are connected
	/*
	bool isCon;
	bool bcont = true;
	MGSize icon = tabcavcell.size();
	
	while ( bcont && icon > 1)
	{
		bcont = false;
		icon = 0;
		
		for ( itc = tabcavcell.begin(); itc != tabcavcell.end(); ++itc) 
		{
			GCell &cell = *(*itc);

			if ( ! cell.IsPainted() ) 
				continue;

			isCon = false;
			
			for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
			{
				const MGSize inei = cell.cCellId( ifc).first;
				
				if ( inei )
					if ( mpGrd->cCell( inei).IsPainted() )
						isCon = true;
			}
			
			if ( !isCon) 
			{
				TRACE( "cavity not connected");
				cell.WashPaint();
				bcont = true;
			}
			else
				icon++;
		}
	}
	*/
	////////////////////////

	

	/////////////////////////////////////////////////////////////
	// correction for star-shape of the cavity

	bool	bOk;
	bool	bExit;

	do
	{
		bExit = true;

		for ( MGSize ic=0; ic<tabcell.size(); ++ic)
		{
			GCell	&cell = mpGrd->rCell( tabcell[ic] );
			Simplex<DIM_3D> tetcell = GridGeom<DIM_3D>::CreateSimplex( cell, *mpGrd);

			if ( tetcell.IsInside( vct) )
				continue;


			if ( cell.IsPainted() )
			{

				for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
				{
					const MGSize inei = cell.cCellId( ifc).first;

					bOk = false;

 					if ( ! inei)
 						bOk = true;
 					else if ( ! mpGrd->cCell( inei).IsPainted() )
 						bOk = true;


					if ( bOk )
					{
						Simplex<DIM_3D>		tet( mpGrd->cNode( cell.cFaceNodeId( ifc, 0) ).cPos(), 
												 mpGrd->cNode( cell.cFaceNodeId( ifc, 2) ).cPos(),
												 mpGrd->cNode( cell.cFaceNodeId( ifc, 1) ).cPos(),
												 vct );

						// bad cell has negative volume
						if ( tet.Volume() <= volmin )
						{
							cell.WashPaint();
							bExit = false;
						}
					}
				}
			}
		}
	}
	while ( ! bExit);

	// TODO :: check the cavity - hanging nodes, disconnected cells

}



bool Kernel<DIM_3D>::CavityBoundary( vector<GFace>& tabface, vector<MGSize>& tabcell)
{
	bool	bOk;
	GFace	face;

	static MGSize	tabfacconn[4][4] = { {0,0,2,1}, {0,0,1,2}, {0,2,0,1}, {0,1,2,0} };
	static MGSize	tabfnei[GCell::SIZE];


	static vector<MGSize> tabcavcell;

	tabcavcell.swap( tabcell);

	/////////////////////////////////////////////////////////////
	// creating cavity boundary faces 
	for ( MGSize ic=0; ic<tabcavcell.size(); ++ic)
	{
		const GCell	&cell = mpGrd->cCell( tabcavcell[ic] );
		//GCell	&cell = *tabcavcell[ic];

		if ( ! cell.IsPainted() )
			continue;
			

		::memset( tabfnei, 0, GCell::SIZE*sizeof(MGSize) );

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize inei = cell.cCellId( ifc).first;


			bOk = false;

			if ( ! inei)
				bOk = true;
			else if ( ! mpGrd->cCell( inei).IsPainted() )
				bOk = true;

			if ( bOk )
			{
				cell.GetFaceVNIn( face, ifc);
				tabface.push_back( face);
				tabfnei[ifc] = tabface.size();		// <--- !!!!!!  ind + 1
			}

		}

		/////////////////////////////////////////////////////////////
		// set connectivity for the cell faces
		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			if ( tabfnei[ifc] )
			{
				GFace &face = tabface[tabfnei[ifc]-1];

				for ( MGSize ifnei=0; ifnei<GCell::SIZE; ++ifnei)
				{
					if ( ifnei != ifc && tabfnei[ifnei] != 0)
					{
						face.rCellId( tabfacconn[ifc][ifnei]).first = tabfnei[ifnei];
						face.rCellId( tabfacconn[ifc][ifnei]).second = tabfacconn[ifnei][ifc];
					}
				}
			}
		}
	}



	//tabcell.erase( tabcell.begin(), tabcell.end() );

	tabcell.reserve( tabcavcell.size() );
	tabcell.resize(0);

	for ( MGSize ic=0; ic<tabcavcell.size(); ++ic)
	{
		if ( mpGrd->cCell( tabcavcell[ic] ).IsPainted() )
			tabcell.push_back( mpGrd->cCell( tabcavcell[ic] ).cId() );
	}

		

	//printf( "tabcell.size = %d\n", tabcell.size() );
	//printf( "tabface.size = %d\n", tabface.size() );


	//Store::File	f( "_cavity.dat", "wt");
	//for ( itrcell=tabcell.begin(); itrcell!=tabcell.end(); ++itrcell)
	//{
	//	const GCell &cell = mpGrd->cCell( *itrcell );
	//	cell.DumpTEC( *mpGrd, f);
	//}
	//f.Close();

	//f.Open( "_cavfaces.dat", "wt");
	//for ( MGSize i=0; i<tabface.size(); ++i)
	//	tabface[i].DumpTEC( *mpGrd, f);
	//f.Close();
	
	
	if ( ! tabcell.size() || ! tabface.size() )
	{
		TRACE( "Kernel::FindCavity - failed: empty cavity"); 
		return false;
	}
	
	return true;

}



void Kernel<DIM_3D>::FlagExternalCells( vector< Key<3> >& tabbndfac)
{

	sort( tabbndfac.begin(), tabbndfac.end() );


	//// remove duplicate faces from the boundary grid
	//vector< Key<3> >::iterator itre = unique( tabbndfac.begin(), tabbndfac.end() );
	//tabbndfac.erase( itre, tabbndfac.end() );


//FILE *ff = fopen( "_dump.txt", "wt");
//for ( vector< Key<2> >::iterator itrr=tabbndfac.begin(); itrr!=tabbndfac.end(); ++itrr)
//{
//	Key<2> key = *itrr;
//	key.Sort();
//	Vect2D vb1 = mpGrd->cNode( key.cFirst() ).cPos();
//	Vect2D vb2 = mpGrd->cNode( key.cSecond() ).cPos();
//	fprintf( ff, "key %8d %8d : %8.0lf %8.0lf : %8.0lf %8.0lf\n", key.cFirst(), key.cSecond(),
//		vb1.cX(), vb1.cY(), vb2.cX(), vb2.cY());
//}
//fclose(ff);


	// check if any cell is painted - should not be neccessary
	Grid<DIM_3D>::ColCell::iterator	itr;
	for ( itr=mpGrd->rColCell().begin(); itr!=mpGrd->rColCell().end(); ++itr)
	{
		GCell& cell = *itr;
		if ( cell.IsPainted() )
			cout << "cell with id = " << cell.cId() << " is painted\n";
	}

	// find a starting cell 
	MGSize icell = 0;
	for ( itr=mpGrd->rColCell().begin(); itr!=mpGrd->rColCell().end(); ++itr)
	{
		GCell& cell = *itr;
		for ( MGSize in=0; in<GCell::SIZE; ++in)
			if ( cell.cNodeId( in) >= 1 && cell.cNodeId( in) <= 8 /* <- !!!*/ ) // bounding box nodes
			{
				cell.SetExternal( true);
				cell.Paint();
				icell = itr->cId();
				break;
			}

		if ( icell)
			break;
	}


	// start painting algo
	static vector<GCell*>	tabstack;

	tabstack.push_back( &mpGrd->rCell( icell) );

	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();
		bool bcolor = cell.IsExternal();

		tabstack.pop_back();

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;

			if ( ic)
			{
				GCell &cellnei = mpGrd->rCell( ic);

				if ( ! cellnei.IsPainted() )
				{
					tabstack.push_back( &cellnei );
					cellnei.Paint();

					Key<3> key = cell.FaceKey( ifc);
					key.Sort();

					if ( binary_search( tabbndfac.begin(), tabbndfac.end(), key ) )
						cellnei.SetExternal( ! bcolor );
					else
						cellnei.SetExternal( bcolor);
				}
			}
		}
	}

	// wash paint and make statistics
	MGSize	nextern=0, npainted=0;
	for ( itr=mpGrd->rColCell().begin(); itr!=mpGrd->rColCell().end(); ++itr)
	{
		GCell& cell = *itr;

		if ( cell.IsExternal() )
			++nextern;

		if ( cell.IsPainted() )
		{
			++npainted;
			cell.WashPaint();
		}
	}
	
	//cout << "found external/painted/total = " << nextern << " / " << npainted << " / " << mpGrd->SizeCellTab() << endl;

	return;
}


bool Kernel<DIM_3D>::CheckCellVolume( vector<GCell>& tabcell )
{
	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabcell[i], rGrid() );
		MGFloat vol = tet.Volume();
		if ( vol <= 0.0 )
			return false;
	}

	return true;
}


MGFloat Kernel<DIM_3D>::MinCellVolume( vector<GCell>& tabcell )
{
	MGFloat volret;
	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabcell[i], rGrid() );
		MGFloat vol = tet.Volume();
		if ( i==0 || vol < volret )
			volret = vol;
	}

	return volret;
}



MGFloat Kernel<DIM_3D>::CheckCellQuality( vector<GCell>& tabcell )
{
	bool bFirst = true;
	MGFloat qbeta = 0.0;

	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabcell[i], rGrid() );

		MGFloat imr = tet.QMeasureShape();
		MGFloat ss = tet.QMeasureInterp();

		MGFloat qb = 1.0 / (ss*ss);// + 1.0 / (imr*imr);
		//MGFloat qb = tet.QMeasureBETAmin();
		//MGFloat qb = tet.QMeasureBETAmax();
		if ( bFirst || qb > qbeta)
		{
			qbeta = qb;
			bFirst = false;
		}
	}

	return qbeta;
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

