#include "operator3d_flip44.h"

#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/gridgeom.h"

#include "libcorecommon/progressbar.h"
#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




void Operator3D_Flip44::Init( const vector<MGSize>& tabcell)
{
	if ( tabcell.size() != 4)
	{
		mErrorCode = 1;
		return;
	}

	mtabOCell = tabcell;

	Grid<DIM_3D>&	grid = mKernel.rGrid();


	/////////////////////////////////////////////////////////////
	// trying to find the common edge
	vector<MGSize> tab;

	const GCell &fstcell = grid.cCell( mtabOCell[0] );

	for ( MGSize i=0; i<GCell::SIZE; ++i)
	{
		MGSize in = fstcell.cNodeId( i);
		MGSize count = 1;

		for ( MGSize ic=1; ic<mtabOCell.size(); ++ic)
		{
			const GCell &celln = grid.cCell( mtabOCell[ic] );
		
			for ( MGSize k=0; k<GCell::SIZE; ++k)
				if ( celln.cNodeId(k) == in)
					++count;
		}

		if ( count == 4)
			tab.push_back( in);
	}

	if ( tab.size() != 2)
	{
		mErrorCode = 2;
		return;
	}

	mEdge.rFirst()  = tab[0];
	mEdge.rSecond() = tab[1];

	/////////////////////////////////////////////////////////////
	// trying to find the equator
	vector< Key<2> > tabedge;

	for ( MGSize ic=0; ic<mtabOCell.size(); ++ic)
	{
		const GCell &cell = grid.cCell( mtabOCell[ic] );

		for ( MGSize ie=0; ie<GCell::ESIZE; ++ie)
		{
			Key<2> edge = cell.EdgeKey( ie);
			if ( edge.cFirst() != mEdge.cFirst() && edge.cFirst() != mEdge.cSecond() &&
				 edge.cSecond() != mEdge.cFirst() && edge.cSecond() != mEdge.cSecond() )
				 tabedge.push_back( edge);
		}
	}

	if ( tabedge.size() != 4)
	{
		mErrorCode = 3;
		return;
	}

	/////////////////////////////////////////////////////////////
	// check orientation of the equator
	for ( MGSize ie=0; ie<tabedge.size(); ++ie)
	{
		Key<2>	&edge = tabedge[ie];

		Simplex<DIM_3D> tet( grid.cNode( mEdge.cFirst() ).cPos(),
							 grid.cNode( edge.cFirst() ).cPos(),
							 grid.cNode( edge.cSecond() ).cPos(),
							 grid.cNode( mEdge.cSecond() ).cPos() );

		if ( tet.Volume() < 0.0)
		{
			const MGSize itmp = edge.cFirst();
			edge.rFirst() = edge.cSecond();
			edge.rSecond() = itmp;
		}
	}
	/////////////////////////////////////////////////////////////
	// sort the equator

	for ( MGSize ie1 = 1; ie1 < tabedge.size(); ++ie1)
		if ( tabedge[ie1].cFirst() != tabedge[ie1-1].cSecond() )
		{
			for ( MGSize ie2 = ie1+1; ie2 < tabedge.size(); ++ie2)
				if ( tabedge[ie2].cFirst() == tabedge[ie1-1].cSecond() )
				{
					Key<2>	edge = tabedge[ie1];
					tabedge[ie1] = tabedge[ie2];
					tabedge[ie2] = edge;
					break;
				}
		}


	/////////////////////////////////////////////////////////////
	// check the equator is closed
	if ( tabedge[0].cSecond() != tabedge[1].cFirst() ||
		 tabedge[1].cSecond() != tabedge[2].cFirst() ||
		 tabedge[2].cSecond() != tabedge[3].cFirst() ||
		 tabedge[3].cSecond() != tabedge[0].cFirst() )
	{
		reverse( tabedge.begin(), tabedge.end() );

		if ( tabedge[0].cSecond() != tabedge[1].cFirst() ||
			 tabedge[1].cSecond() != tabedge[2].cFirst() ||
			 tabedge[2].cSecond() != tabedge[3].cFirst() ||
			 tabedge[3].cSecond() != tabedge[0].cFirst() )
		{
			mErrorCode = 4;
			return;
		}
	}

	for ( MGSize ie=0; ie<tabedge.size(); ++ie)
		mtabENode.push_back( tabedge[ie].cSecond() );


}



void Operator3D_Flip44::SelectForceEdge( const Key<2>& edge)
{
	if ( mErrorCode != 0)
		return;

	/////////////////////////////////////////////////////////////
	// check if edge exists in equator 
	MGSize ien0=100, ien1=100;

	for ( MGSize i=0; i<mtabENode.size(); ++i)
	{
		if ( mtabENode[i] == edge.cFirst() )
			ien0 = i;

		if ( mtabENode[i] == edge.cSecond() )
			ien1 = i;
	}

	if ( ien0 == 100 || ien1 == 100)
	{
		mErrorCode = 11;
		return;
	}

	if ( ::abs( (MGInt)ien1 - (MGInt)ien0 ) != 2 )
	{
		mErrorCode = 12;
		return;
	}

	/////////////////////////////////////////////////////////////
	// reset order in equator tab - should start from edge.cFirst()
	rotate( mtabENode.begin(), mtabENode.begin()+ien0, mtabENode.end() );



	/////////////////////////////////////////////////////////////
	// init new cells tab
	BuildCells( mtabNCell);

	if ( ! mKernel.CheckCellVolume( mtabNCell) )
		mErrorCode = 2000;
}


void Operator3D_Flip44::SelectQBased()
{
	if ( mErrorCode != 0)
		return;

	vector<GCell> tabco;
	vector<GCell> tabc1;
	vector<GCell> tabc2;

	vector<MGSize> tabENode_backup = mtabENode;

	for ( MGSize i=0; i<mtabOCell.size(); ++i)
		tabco.push_back( mKernel.rGrid().cCell( mtabOCell[i]) );

	BuildCells( tabc1);

	rotate( mtabENode.begin(), mtabENode.begin()+1, mtabENode.end() );
	BuildCells( tabc2);



	bool boko = mKernel.CheckCellVolume( tabco);
	MGFloat qbo = mKernel.CheckCellQuality( tabco);

	bool bok1 = mKernel.CheckCellVolume( tabc1);
	MGFloat qb1 = mKernel.CheckCellQuality( tabc1);

	bool bok2 = mKernel.CheckCellVolume( tabc2);
	MGFloat qb2 = mKernel.CheckCellQuality( tabc2);


	ASSERT( boko);

	MGSize ver = 0;

	MGFloat qmin = qbo;

	if ( bok1 && qb1 < qmin)
	{
		ver = 1;
		qmin = qb1;
	}

	if ( bok2 && qb2 < qmin)
	{
		ver = 2;
		qmin = qb2;
	}

	if ( ver == 0)
	{
		mErrorCode = 1000;
		return;
	}

	if ( ver == 1)
	{
		mtabENode = tabENode_backup;
		mtabNCell = tabc1;
		return;
	}

	if ( ver == 2)
	{
		mtabNCell = tabc2;
		return;
	}
}


void Operator3D_Flip44::SelectVolBased()
{
	if ( mErrorCode != 0)
		return;

	vector<GCell> tabco;
	vector<GCell> tabc1;
	vector<GCell> tabc2;

	vector<MGSize> tabENode_backup = mtabENode;

	for ( MGSize i=0; i<mtabOCell.size(); ++i)
		tabco.push_back( mKernel.rGrid().cCell( mtabOCell[i]) );

	BuildCells( tabc1);

	rotate( mtabENode.begin(), mtabENode.begin()+1, mtabENode.end() );
	BuildCells( tabc2);


	bool boko = mKernel.CheckCellVolume( tabco);
	MGFloat qbo = mKernel.MinCellVolume( tabco);

	bool bok1 = mKernel.CheckCellVolume( tabc1);
	MGFloat qb1 = mKernel.MinCellVolume( tabc1);

	bool bok2 = mKernel.CheckCellVolume( tabc2);
	MGFloat qb2 = mKernel.MinCellVolume( tabc2);


	ASSERT( boko);

	MGSize ver = 0;

	MGFloat qmin = qbo;

	if ( bok1 && qb1 > qmin)
	{
		ver = 1;
		qmin = qb1;
	}

	if ( bok2 && qb2 > qmin)
	{
		ver = 2;
		qmin = qb2;
	}

	if ( ver == 0)
	{
		mErrorCode = 1000;
		return;
	}

	if ( ver == 1)
	{
		mtabENode = tabENode_backup;
		mtabNCell = tabc1;
		return;
	}

	if ( ver == 2)
	{
		mtabNCell = tabc2;
		return;
	}
}


void Operator3D_Flip44::BuildCells( vector<GCell>& tabcell)
{
	GCell cell;

	tabcell.clear();

	cell.rNodeId(0) = mtabENode[0];
	cell.rNodeId(1) = mtabENode[2];
	cell.rNodeId(2) = mEdge.cFirst();
	cell.rNodeId(3) = mtabENode[3];
	tabcell.push_back( cell);

	cell.rNodeId(0) = mtabENode[0];
	cell.rNodeId(1) = mtabENode[2];
	cell.rNodeId(2) = mtabENode[3];
	cell.rNodeId(3) = mEdge.cSecond();
	tabcell.push_back( cell);

	cell.rNodeId(0) = mtabENode[0];
	cell.rNodeId(1) = mtabENode[2];
	cell.rNodeId(2) = mEdge.cSecond();
	cell.rNodeId(3) = mtabENode[1];
	tabcell.push_back( cell);

	cell.rNodeId(0) = mtabENode[0];
	cell.rNodeId(1) = mtabENode[2];
	cell.rNodeId(2) = mtabENode[1];
	cell.rNodeId(3) = mEdge.cFirst();
	tabcell.push_back( cell);

}


void Operator3D_Flip44::DoFlip( vector<MGSize>& tabcell)
{
	if ( mErrorCode != 0)
		return;


	ASSERT( mtabNCell.size() == 4);

	tabcell.clear();

	/////////////////////////////////////////////////////////////
	// remove old cell from the tree
	for ( MGSize ic=0; ic<mtabOCell.size(); ++ic)
		mKernel.RemoveCellFromTree( mtabOCell[ic] );

	/////////////////////////////////////////////////////////////
	// insert new cells 
	for ( MGSize i=0; i<mtabNCell.size(); ++i)
	{
		//MGSize id = mKernel.rGrid().InsertCell( mtabNCell[i] );
		MGSize id = mKernel.InsertCell( mtabNCell[i] );
		tabcell.push_back( id);

		mKernel.rGrid().rCell( id).SetModified( true);
	}

	/////////////////////////////////////////////////////////////
	// update internal connectivity

	// cell 0
	mKernel.rGrid().rCell( tabcell[0] ).rCellId( 3 ).first = tabcell[3];
	mKernel.rGrid().rCell( tabcell[0] ).rCellId( 3 ).second = 2;

	mKernel.rGrid().rCell( tabcell[0] ).rCellId( 2 ).first = tabcell[1];
	mKernel.rGrid().rCell( tabcell[0] ).rCellId( 2 ).second = 3;

	// cell 1
	mKernel.rGrid().rCell( tabcell[1] ).rCellId( 3 ).first = tabcell[0];
	mKernel.rGrid().rCell( tabcell[1] ).rCellId( 3 ).second = 2;

	mKernel.rGrid().rCell( tabcell[1] ).rCellId( 2 ).first = tabcell[2];
	mKernel.rGrid().rCell( tabcell[1] ).rCellId( 2 ).second = 3;

	// cell 2
	mKernel.rGrid().rCell( tabcell[2] ).rCellId( 3 ).first = tabcell[1];
	mKernel.rGrid().rCell( tabcell[2] ).rCellId( 3 ).second = 2;

	mKernel.rGrid().rCell( tabcell[2] ).rCellId( 2 ).first = tabcell[3];
	mKernel.rGrid().rCell( tabcell[2] ).rCellId( 2 ).second = 3;

	// cell 3
	mKernel.rGrid().rCell( tabcell[3] ).rCellId( 3 ).first = tabcell[2];
	mKernel.rGrid().rCell( tabcell[3] ).rCellId( 3 ).second = 2;

	mKernel.rGrid().rCell( tabcell[3] ).rCellId( 2 ).first = tabcell[0];
	mKernel.rGrid().rCell( tabcell[3] ).rCellId( 2 ).second = 3;


	/////////////////////////////////////////////////////////////
	// update external connectivity

	typedef map< Key<3>, GNeDef > FaceMap;	
	FaceMap mapface;
	FaceMap::iterator itrmf;

	for ( MGSize i=0; i<mtabOCell.size(); ++i)
	{
		const GCell &cell = mKernel.rGrid().cCell( mtabOCell[i]);

		for ( MGSize ifac=0; ifac<GCell::SIZE; ++ifac)
		{
			Key<3> key = cell.FaceKey( ifac);
			key.Sort();
			mapface.insert( FaceMap::value_type( key, cell.cCellId( ifac) ) );
		}

	}

	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		GCell &cell = mKernel.rGrid().rCell( tabcell[i]);

		// upper external face
		Key<3> keyup = cell.FaceKey( 0);
		keyup.Sort();
		if ( (itrmf = mapface.find( keyup)) == mapface.end() )
		{
			THROW_INTERNAL( "Operator3D_Flip44 - corrupted data !!!");
		}

		cell.rCellId(0) = itrmf->second;

		mKernel.rGrid().rCell( cell.cCellId(0).first ).rCellId( cell.cCellId(0).second ).first = cell.cId();
		mKernel.rGrid().rCell( cell.cCellId(0).first ).rCellId( cell.cCellId(0).second ).second = 0;


		// lower external face
		Key<3> keylo = cell.FaceKey( 1);
		keylo.Sort();
		if ( (itrmf = mapface.find( keylo)) == mapface.end() )
		{
			THROW_INTERNAL( "Operator3D_Flip44 - corrupted data !!!");
		}

		cell.rCellId(1) = itrmf->second;

		mKernel.rGrid().rCell( cell.cCellId(1).first ).rCellId( cell.cCellId(1).second ).first = cell.cId();
		mKernel.rGrid().rCell( cell.cCellId(1).first ).rCellId( cell.cCellId(1).second ).second = 1;
	}


	/////////////////////////////////////////////////////////////
	// remove old cells
	// remove old cell from the tree

	for ( MGSize ic=0; ic<mtabOCell.size(); ++ic)
	{
		//if ( mKernel.rGrid().cCell( mtabOCell[ic]).IsTree() )
		//{
		//	const GVect vc = GridGeom<DIM_3D>::CreateSimplex( mtabOCell[ic], mKernel.rGrid() ).Center();
		//	mKernel.mLoc.TreeErase( vc, mtabOCell[ic]);
		//}

		//mKernel.rGrid().EraseCell( mtabOCell[ic] );
		mKernel.EraseCell( mtabOCell[ic] );

	}

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

