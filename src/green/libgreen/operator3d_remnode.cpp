#include "operator3d_remnode.h"

#include "libgreen/operator3d_collapseedge.h"

#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/gridgeom.h"

#include "libcorecommon/progressbar.h"
#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


void Operator3D_RemoveNode::Reset()
{
}

void Operator3D_RemoveNode::Init( const MGSize& idnod)
{
	mIdNode = idnod;

	// find ball

	MGSize icella = mKernel.cGrid().cNode(mIdNode).cCellId();
	mKernel.FindBall( mtabBall, mIdNode, icella);

	// find all edges touching mIdNode
	vector< Key<2> > tabedg;

	for ( MGSize i=0; i<mtabBall.size(); ++i)
	{
		const GCell& cell = mKernel.cGrid().cCell( mtabBall[i] );
		for ( MGSize k=0; k<GCell::ESIZE; ++k)
		{
			Key<2> key = cell.EdgeKey( k);
			key.Sort();
			if ( key.cFirst() == mIdNode || key.cSecond() == mIdNode )
				tabedg.push_back( key);
		}
	}

	sort( tabedg.begin(), tabedg.end() );
	tabedg.erase( unique( tabedg.begin(), tabedg.end() ), tabedg.end() );

	// select edge for collapse
	mIdNext = 0;
	MGFloat volmin = 0.;

	for ( MGSize i=0; i<tabedg.size(); ++i)
	{
		MGSize idnext = 0;

		if ( tabedg[i].cFirst() == mIdNode )
			idnext = tabedg[i].cSecond();

		if ( tabedg[i].cSecond() == mIdNode )
			idnext = tabedg[i].cFirst();

		if ( idnext == 0 )
			THROW_INTERNAL( "Operator3D_RemoveNode::Init -- CRASH");

		Operator3D_CollapseEdge opedge( mKernel);

		opedge.Init( mIdNode, idnext, mtabBall);

		if ( opedge.Error() != 0 )
			continue;

		if ( opedge.VolMinAfter() > volmin )
		{
			volmin = opedge.VolMinAfter();
			mIdNext = idnext;
		}
		//opedge.QAfter();
		//opedge.QBefore();
	}

	if ( mIdNext == 0 )
		mErrorCode = 100;

}

void Operator3D_RemoveNode::Execute()
{
	if ( Error() != 0 )
		return;

	Operator3D_CollapseEdge opedge( mKernel);
	opedge.Init( mIdNode, mIdNext, mtabBall);
	opedge.Execute();
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

