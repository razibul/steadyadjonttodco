#ifndef __BASECELL_H__
#define __BASECELL_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libcorecommon/key.h"

using namespace Geom;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


typedef pair<MGSize,MGChar>	GNeDef;


//////////////////////////////////////////////////////////////////////
// class BaseCell
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class BaseCell
{
public:
	enum { SIZE = DIM+1};
	enum { ESIZE = DIM*(DIM+1)/2};

	BaseCell()	{ Reset();}

	const MGSize&	cNodeId( const MGSize& i) const	{ return mtabNodeIds[i];}
	MGSize&			rNodeId( const MGSize& i)		{ return mtabNodeIds[i];}

	const GNeDef&	cCellId( const MGSize& i) const	{ return mtabCellIds[i];}
	GNeDef&			rCellId( const MGSize& i)		{ return mtabCellIds[i];}

	Key<SIZE>		CellKey() const					{ return Key<SIZE>( mtabNodeIds ); }


	void	Reset();

private:
	MGSize	mtabNodeIds[SIZE];
	GNeDef	mtabCellIds[SIZE];
};
//////////////////////////////////////////////////////////////////////


template <Dimension DIM> 
inline void BaseCell<DIM>::Reset()
{
	for ( MGSize i=0; i<SIZE; ++i)
	{
		mtabNodeIds[i] = 0;
		mtabCellIds[i] = GNeDef( 0, 0);
	}
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __BASECELL_H__

