#ifndef __WRITEBNDTEC_H__
#define __WRITEBNDTEC_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


using namespace Geom;

template <Dimension DIM>
class BndGrid;



//////////////////////////////////////////////////////////////////////
// class WriteBndTEC
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class WriteBndTEC
{
public:
	WriteBndTEC( BndGrid<DIM>* pgrd) : mpGrd(pgrd)	{}

	void	DoWrite( const MGString& fname);

protected:

private:
	BndGrid<DIM>	*mpGrd;

	MGString			mFName;

	MGString			mTitle;
	vector<MGString>	mtabVars;

	MGString			mZoneT;
	MGString			mElemT;

	MGSize				mnNode;
	MGSize				mnElem;
};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __WRITEBNDTEC_H__

