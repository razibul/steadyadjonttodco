#ifndef __WRITEGRDMSH2_H__
#define __WRITEGRDMSH2_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libcoreio/gridfacade.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class GridWBnd;

//////////////////////////////////////////////////////////////////////
// class WriteGrdTEC
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class WriteGrdMSH2 : public IO::GridWriteFacade
{
public:
	WriteGrdMSH2( const GridWBnd<DIM>* pgrd) : mpGrd(pgrd)	{}

	void	DoWrite( const MGString& fname);

protected:
	// interface for writing mesh in MSH2 format
	// all IDs are based on 0,...,N-1 standard
	virtual Dimension	Dim() const												{ return DIM;}
	virtual void	IOGetName( MGString& name) const							{ name = mFName;}

	virtual MGSize	IOSizeNodeTab() const;
	virtual MGSize	IOSizeCellTab( const MGSize& type) const;
	virtual MGSize	IOSizeBFaceTab( const MGSize& type) const;

	virtual void	IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	virtual void	IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	virtual void	IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id)const;

	virtual MGSize	IONumCellTypes() const										{ return 1;}
	virtual MGSize	IONumBFaceTypes() const										{ return 1;}
	virtual MGSize	IOCellType( const MGSize& i) const							{ return (i==0) ? DIM+1 : 0; }
	virtual MGSize	IOBFaceType( const MGSize& i) const							{ return (i==0) ? DIM : 0;}


private:
	const GridWBnd<DIM>	*mpGrd;

	MGString		mFName;
	
	vector<MGSize>	mTabIdNode_L2G;
	vector<MGSize>	mTabIdNode_G2L;
	vector<MGSize>	mTabIdCell;

};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __WRITEGRDMSH2_H__
