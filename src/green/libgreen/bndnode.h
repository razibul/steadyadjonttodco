#ifndef __BNDNODE_H__
#define __BNDNODE_H__

#include "libgreen/tdefs.h"
#include "libcorecommon/entity.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class BndNode
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class BndNode : public Entity
{
	typedef typename TDefs<DIM>::GVect		GVect;

public:
	BndNode() : mGrdId(0), mMasterId(0)	{}

	const GVect&	cPos() const		{ return mPos;}
	GVect&			rPos()				{ return mPos;}


	MGSize&			rGrdId()			{ return mGrdId;}
	const MGSize&	cGrdId() const		{ return mGrdId;}

	MGSize&			rMasterId()			{ return mMasterId;}
	const MGSize&	cMasterId() const	{ return mMasterId;}

	operator	GVect()					{ return mPos;}

private:
	MGSize	mGrdId;
	MGSize	mMasterId;

	GVect	mPos;
};
//////////////////////////////////////////////////////////////////////



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __BNDNODE_H__

