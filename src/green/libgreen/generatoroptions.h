#ifndef __GENERATOROPTIONS_H__
#define __GENERATOROPTIONS_H__

#include "libcoreconfig/config.h"
#include "libgreen/configconst.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <Dimension DIM>
class GeneratorOptions
{
public:

	enum ETriangType
	{
		TT_NONE = 0,
		TT_EDGES,
		TT_CELLC,
		TT_CIRCC
	};

public:
	GeneratorOptions();

	void	SetUp( const CfgSection& cfgsec);

public:
	ETriangType		mTriangType;
	MGSize			mTriangMaxIter;
	MGFloat			mTriangConstALPHA;
	MGFloat			mTriangConstBETA;
	MGSize			mTriangMaxPropBegin;
	MGSize			mTriangMaxPropEnd;
};



template <Dimension DIM>
inline GeneratorOptions<DIM>::GeneratorOptions()
{
	// setting triangulation type 
	mTriangType			= TT_CELLC;
	mTriangMaxIter		= 200;
	mTriangConstALPHA	= 1.15;
	mTriangConstBETA	= 2.0;
	mTriangMaxPropBegin	= 500;
	mTriangMaxPropEnd	= 200000;

}

template <>
inline GeneratorOptions<DIM_2D>::GeneratorOptions()
{
	// setting triangulation type 
	mTriangType			= TT_EDGES;
	mTriangMaxIter		= 200;
	mTriangConstALPHA	= 1.6;
	mTriangConstBETA	= 1.3;
	mTriangMaxPropBegin	= 500;
	mTriangMaxPropEnd	= 200000;
}




template <Dimension DIM>
inline void GeneratorOptions<DIM>::SetUp( const CfgSection& cfgsec)
{
	//if ( ! cfgsec.ConfigSectIsValid() )

	// setting triangulation type 
	MGString stype = cfgsec.ValueString( ConfigStr::Master::Generator::TriangType::KEY );
	if ( stype == MGString( ConfigStr::Master::Generator::TriangType::Value::GENTT_EDGES ) )
		mTriangType = TT_EDGES;
	else
	if ( stype == MGString( ConfigStr::Master::Generator::TriangType::Value::GENTT_CELLC ) )
		mTriangType = TT_CELLC;
	else
	if ( stype == MGString( ConfigStr::Master::Generator::TriangType::Value::GENTT_CIRCC ) )
		mTriangType = TT_CIRCC;
	else
		mTriangType = TT_NONE;

	mTriangMaxIter		= cfgsec.ValueSize( ConfigStr::Master::Generator::GEN_MAXITER );
	mTriangConstALPHA	= cfgsec.ValueFloat( ConfigStr::Master::Generator::GEN_CONSTALPHA );
	mTriangConstBETA	= cfgsec.ValueFloat( ConfigStr::Master::Generator::GEN_CONSTBETA );
	mTriangMaxPropBegin	= cfgsec.ValueSize( ConfigStr::Master::Generator::GEN_MAXPROPBEGIN );
	mTriangMaxPropEnd	= cfgsec.ValueSize( ConfigStr::Master::Generator::GEN_MAXPROPEND );

}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __GENERATOROPTIONS_H__
