#include "optimiser.h"

#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/operator3d_flip44.h"
#include "libgreen/operator3d_flip32.h"
#include "libgreen/operator3d_flip23.h"

#include "libcorecommon/progressbar.h"
#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <>
MGSize Optimiser<DIM_2D>::OptimiseEdges( const vector< Key<2> >& tabedge )
{
	return 0;
}


template <>
MGSize Optimiser<DIM_2D>::OptimiseFaces( const vector< Key<3> >& tabface )
{
	return 0;
}


template <>
MGSize Optimiser<DIM_3D>::OptimiseEdges( const vector< Key<2> >& tabedge )
{
	Grid<DIM_3D>&	grid = mpKernel->rGrid();
	ProgressBar	bar(40);

	MGSize flip_count = 0;

	bar.Init( tabedge.size() );
	bar.Start();

	for ( MGSize ie=0; ie<tabedge.size(); ++ie, ++bar)
	{
		if ( tabedge[ie].cFirst() >= 1 && tabedge[ie].cFirst() <= 8)
			continue;

		if ( tabedge[ie].cSecond() >= 1 && tabedge[ie].cSecond() <= 8)
			continue;

		vector<MGSize> tabcell;
		try
		{
			mpKernel->FindEdgeShell( tabcell, tabedge[ie], grid.cNode(tabedge[ie].cFirst()).cCellId(), grid.cNode(tabedge[ie].cSecond()).cCellId());
		}
		catch ( EHandler::Except&)
		{
			cout << "mpKernel->FindEdgeShell( tabcell, tabedge[ie]);;" <<endl;
			throw;
		}

		bool bCont = false;
		for ( MGSize k=0; k<tabcell.size(); ++k)
			if ( grid.cCell( tabcell[k]).IsExternal() )
			{
				bCont = true;
				break;
			}

		if ( bCont)
			continue;

		try
		{
			if ( tabcell.size() == 3)
			{
				Operator3D_Flip32 flip( *mpKernel);

				flip.Init( tabcell);
				//flip.SelectQBased();
				flip.SelectVolBased();
				if ( ! flip.Error() )
				{
					flip.DoFlip( tabcell);
					++flip_count;
				}
			}
		}
		catch ( EHandler::Except&)
		{
			cout << "Operator3D_Flip32" <<endl;
			throw;
		}


		try
		{
			if ( tabcell.size() == 4)
			{
				Operator3D_Flip44 flip( *mpKernel);

				flip.Init( tabcell);
				flip.SelectQBased();
				//flip.SelectVolBased();
				if ( ! flip.Error() )
				{
					flip.DoFlip( tabcell);
					++flip_count;
				}
			}
		}
		catch ( EHandler::Except&)
		{
			cout << "Operator3D_Flip44" <<endl;
			throw;
		}


		grid.UpdateNodeCellIds( tabcell);
	}
	
	bar.Finish();

	cout << "no of flips = " << flip_count << endl;

	return flip_count;
}

template <>
MGSize Optimiser<DIM_3D>::OptimiseFaces( const vector< Key<3> >& tabface )
{
	Grid<DIM_3D>&	grid = mpKernel->rGrid();
	ProgressBar	bar(40);

	MGSize flip_count = 0;

	bar.Init( tabface.size() );
	bar.Start();

	for ( MGSize ie=0; ie<tabface.size(); ++ie, ++bar)
	{
		if ( tabface[ie].cFirst() >= 1 && tabface[ie].cFirst() <= 8)
			continue;

		if ( tabface[ie].cSecond() >= 1 && tabface[ie].cSecond() <= 8)
			continue;

		if ( tabface[ie].cThird() >= 1 && tabface[ie].cThird() <= 8)
			continue;

		vector<MGSize> tabcell;
		try
		{
			mpKernel->FindFaceShell( tabcell, tabface[ie], grid.cNode(tabface[ie].cFirst()).cCellId() );
		}
		catch ( EHandler::Except&)
		{
			cout << "mpKernel->FindFaceShell( tabcell, tabface[ie]);" <<endl;
			throw;
		}

		bool bCont = false;
		for ( MGSize k=0; k<tabcell.size(); ++k)
			if ( grid.cCell( tabcell[k]).IsExternal() )
			{
				bCont = true;
				break;
			}

		if ( bCont)
			continue;

		try
		{
			if ( tabcell.size() == 2)
			{
				Operator3D_Flip23 flip( *mpKernel);

				flip.Init( tabcell);
				flip.SelectQBased();
				//flip.SelectVolBased();
				if ( ! flip.Error() )
				{
					flip.DoFlip( tabcell);
					++flip_count;
				}
			}
		}
		catch ( EHandler::Except&)
		{
			cout << "Operator3D_Flip23" <<endl;
			throw;
		}

		grid.UpdateNodeCellIds( tabcell);
	}
	
	bar.Finish();

	cout << "no of flips = " << flip_count << endl;

	return flip_count;
}




template <>
MGSize Optimiser<DIM_2D>::OptimiseCells( const vector<MGSize>& tabcell )
{
	return 0;
}

template <>
MGSize Optimiser<DIM_3D>::OptimiseCells( const vector<MGSize>& tabcell )
{
	//cout << endl;
	//cout << "optimisation started" << endl;

	Grid<DIM_3D>&	grid = mpKernel->rGrid();
	ProgressBar	bar(40);

	grid.UpdateNodeCellIds();

	// edges
	vector< Key<2> > tabedge;
	vector< Key<3> > tabface;

	for ( MGSize ic=0; ic<tabcell.size(); ++ic)
	{
		const Grid<DIM_3D>::GCell& cell = grid.cCell( tabcell[ic] );

		//// skip unchanged cells
		//if ( ! cell.IsModified() )
		//	continue;

		for ( MGSize k=0; k<Grid<DIM_3D>::GCell::ESIZE; ++k)
		{
			Key<2> key = cell.EdgeKey( k);
			key.Sort();
			tabedge.push_back( key);
		}

		for ( MGSize k=0; k<Grid<DIM_3D>::GCell::SIZE; ++k)
		{
			if ( cell.cCellId(k).first == 0 )
				continue;

			Key<3> key = cell.FaceKey( k);
			key.Sort();
			tabface.push_back( key);
		}
	}

	sort( tabedge.begin(), tabedge.end() );
	tabedge.erase( unique( tabedge.begin(), tabedge.end() ), tabedge.end() );

	sort( tabface.begin(), tabface.end() );
	tabface.erase( unique( tabface.begin(), tabface.end() ), tabface.end() );

	MGSize flip_face = OptimiseFaces( tabface);
	MGSize flip_edge = OptimiseEdges( tabedge);


	return flip_edge + flip_face;
}




template <>
MGSize Optimiser<DIM_2D>::OptimiseNodes( const vector<MGSize>& tabnode, const MGSize& passcount )
{
	return 0;
}

template <>
MGSize Optimiser<DIM_3D>::OptimiseNodes( const vector<MGSize>& tabnode, const MGSize& passcount )
{

	mpKernel->rGrid().UpdateNodeCellIds();


	MGSize iter = 0;
	MGSize flip_count, totflip_count=0;

	do
	{
		++iter;

		//mpKernel->rGrid().UpdateNodeCellIds();

		vector<MGSize> tabcell;
		for ( MGSize in=0; in<tabnode.size(); ++in)
		{
			vector<MGSize> tabball;
			mpKernel->FindBall( tabball, tabnode[in], mpKernel->cGrid().cNode( tabnode[in]).cCellId() );

			tabcell.reserve( tabcell.size() + tabball.size() );
			for ( MGSize k=0; k<tabball.size(); ++k)
				tabcell.push_back( tabball[k] );
		}

		sort( tabcell.begin(), tabcell.end() );
		tabcell.erase( unique( tabcell.begin(), tabcell.end() ), tabcell.end() );


		flip_count = OptimiseCells( tabcell );
		totflip_count += flip_count;
	}
	while ( iter<passcount && flip_count > 2 );

	return totflip_count;
}


template <>
void Optimiser<DIM_2D>::DoOptimise( const MGSize& passcount )
{
}

template <>
void Optimiser<DIM_3D>::DoOptimise( const MGSize& passcount )
{
	cout << endl;
	cout << "optimisation started" << endl;

	Grid<DIM_3D>&	grid = mpKernel->rGrid();
	ProgressBar	bar(40);

	grid.UpdateNodeCellIds();

	//mpKernel->SetAllCellsModified( true);

	MGSize iter = 0;
	MGSize flip_count;

	do
	{
		vector< Key<2> > tabedg;
		vector<MGSize> tabcell;

		++iter;

		//try
		//{
		//	mpKernel->FindBadCells( tabcell, 0.0);
		//	random_shuffle( tabcell.begin(), tabcell.end() );
		//	//mpKernel->FindBadCellEdges( tabedg, 0.0);
		//	//random_shuffle( tabedg.begin(), tabedg.end() );
		//}
		//catch ( EHandler::Except&)
		//{
		//	cout << "mpKernel->FindBadCellEdges( tabedg, 6.0);" <<endl;
		//	throw;
		//}

		mpKernel->FindModifiedCells( tabcell);
		random_shuffle( tabcell.begin(), tabcell.end() );

		mpKernel->SetAllCellsModified( false);

		flip_count = OptimiseCells( tabcell);
		//flip_count = OptimiseEdges( tabedg);
	}
	while ( iter<passcount && flip_count > 5 );
}


//class GridOperatorFlip44
//
//operator3dflip44

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

