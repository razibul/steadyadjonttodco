#include "localizator.h"
#include "libgreen/grid.h"
#include "libgreen/gridgeom.h"

#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Dimension DIM> 
MGSize Localizator<DIM>::Localize( const GVect& vct, const bool& bcrash) const
{
	MGSize	istart;
	MGSize	icell=1;
	GVect	vn, vc, vd;


	typename Tree<MGSize,DIM>::TPair	res;
	if ( mCellTree.GetClosestItem( res, vct) )
		istart = res.second;
	else
		istart = 0;	// tree failed - should never happend


	/////////////////////////////////////////////////////////////
	// find best starting point
	if ( ! istart)
	{
		GVect	vrnd;

		vrnd = GridGeom<DIM>::CreateSimplex( icell, *mpGrid ).Center();

		MGFloat d, dmin = (vct - vrnd).module();

		MGSize	irnd;

		MGFloat	coeff = 0.9;
		MGSize	nr = (MGSize)::floor( coeff * ::pow( (MGFloat)mpGrid->SizeCellTab(), 0.3333) );

		nr = 30;//MAX( nr, 40);
		
		for ( MGSize ir=0; ir<nr; ++ir)
		{
			irnd = ( (mpGrid->SizeCellTab()-1) * (MGSize)::rand()) / (RAND_MAX) + 1;


			if ( irnd == 0 && ! mpGrid->cColCell().is_valid( irnd) )
				continue;
		
			vrnd = GridGeom<DIM>::CreateSimplex( irnd, *mpGrid ).Center();

			d = (vct - vrnd).module();
			if ( d < dmin)
			{
				dmin = d;
				icell = irnd;
			}
		}
	}
	else
	{
		icell = istart;
	}


	static vector<GCell*>	track;
	static vector<MGSize>	stacknei;

	track.reserve( 20);
	stacknei.reserve(GCell::SIZE);

	track.resize(0);
	stacknei.resize(0);


	GFace	face;
	MGSize val;


	/////////////////////////////////////////////////////////////
	// start walking
	while ( true)
	{
		GCell	&cell = mpGrid->rCell( icell);

		cell.Paint();
		track.push_back( &cell);


		stacknei.resize(0);

		for ( MGSize i=0; i<GCell::SIZE; ++i)
		{
			cell.GetFaceVNOut( face, i);

			if ( GridGeom<DIM>::CreateSimplexFace( face, *mpGrid).IsAbove( vct) && face.cCellUp().first )
				stacknei.push_back( face.cCellUp().first );
		}

		if ( stacknei.size() == 0 )
			break;	// icell is valid index



		val = stacknei.back();

		bool bErr = false;

		while ( mpGrid->cCell( val).IsPainted() )
		{
			stacknei.pop_back();

			if ( stacknei.size() > 0)
			{
				val = stacknei.back();
			}
			else
			{ 
				bErr = true;
				break;
			}
		}


		icell = val;

		if ( bErr )
		{
			TRACE( "\n");
			TRACE( "is inside box = " << mCellTree.cBox().IsInside( vct) );
			TRACE( "localization problem for point [" << vct.cX() << ", " << vct.cY() << ", " << vct.cZ() << "]" );
			TRACE( "istart = " << istart);
			TRACE( "last cell id = " << icell );
			
			// power search !!!!
			cout << "starting power search" << endl;
			TRACE( "starting power search");
			MGSize idret = 0;
			
			typename Grid<DIM>::ColCell::const_iterator	itr;
			for ( itr = mpGrid->cColCell().begin(); itr != mpGrid->cColCell().end(); ++itr)
			{
				const GCell& cell = (*itr);
				if ( GridGeom<DIM>::CreateSimplex( cell, *mpGrid).IsInside( vct) )
				{
					idret = itr.index();
					break;
				}
			}
			
			TRACE( "checking grid after localization problem occured");

			//cout << "checking grid after localization problem occured" << endl;
			//
			//mpGrid->CheckConnectivity();
			//
			//vector<MGSize>	tab;
			//tab.push_back( idret);
			//
			//WriteGrdTEC<DIM_3D> wtec( mpGrid);
			//wtec.WriteCells( "_cell.dat", tab);


			// clean up
			for ( typename vector<GCell*>::iterator it=track.begin(); it!=track.end(); ++it)
				(*it)->WashPaint();

			if ( idret == 0 && bcrash )
				THROW_INTERNAL( "Localizator<DIM_3D>::Localize(...) - failed !!!");

			return idret;
		}



		//if ( track.size() > mpGrd->SizeCellTab() )
		//	THROW_INTERNAL( "local walk corrupted");

		//printf( "	new icell = %d\n", icell);
	}

	/////////////////////////////////////////////////////////////
	// clean up
	for ( typename vector<GCell*>::iterator it=track.begin(); it!=track.end(); ++it)
		(*it)->WashPaint();



	//if ( ! GridGeom<DIM>::CreateSimplex( icell, *mpGrid).IsInside( vct) )
	//{
	//		// power search !!!!
	//		cout << "starting power search" << endl;
	//		TRACE( "starting power search");
	//		
	//		typename Grid<DIM>::ColCell::const_iterator	itr;
	//		for ( itr = mpGrid->cColCell().begin(); itr != mpGrid->cColCell().end(); ++itr)
	//		{
	//			const GCell& cell = (*itr);
	//			if ( GridGeom<DIM>::CreateSimplex( cell, *mpGrid).IsInside( vct) )
	//			{
	//				icell = itr.index();
	//				break;
	//			}
	//		}
	//}

	return icell;
}


template <>
MGSize Localizator<DIM_1D>::Localize( const GVect& vct, const bool& bcrash) const
{
	MGSize icell = 1;

	do
	{
		Simplex<DIM_1D> simp = GridGeom<DIM_1D>::CreateSimplex( mpGrid->cCell( icell), *mpGrid);
		if ( simp.IsInside( vct) )
			break;
		
		++icell;
	}
	while ( icell < mpGrid->SizeNodeTab() );	// TODO :: check this !!!

	return icell;
}

//template <>
//MGSize Localizator<DIM_2D>::Localize( const GVect& vct, const bool& bcrash) const
//{
//	MGSize	istart=0;
//	MGSize	icell=1;
//	GVect	vn, vc, vd;
//
//
//	Tree<MGSize,DIM_2D>::TPair	res;
//	if ( mCellTree.GetClosestItem( res, vct) )
//		istart = res.second;
//	else
//		istart = 0;	// tree failed - should never happend
//
//
//	/////////////////////////////////////////////////////////////
//	// find best starting point
//	if ( ! istart)
//	{
//		GVect	vrnd;
//
//		vrnd = GridGeom<DIM_2D>::CreateSimplex( icell, *mpGrid ).Center();
//
//		MGFloat d, dmin = (vct - vrnd).module();
//
//		MGSize	irnd;
//
//		MGFloat	coeff = 0.9;
//		MGSize	nr = (MGSize)::floor( coeff * ::pow( (MGFloat)mpGrid->SizeCellTab(), 0.3333) );
//
//		nr = 30;//MAX( nr, 40);
//		
//		for ( MGSize ir=0; ir<nr; ++ir)
//		{
//			irnd = ( (mpGrid->SizeCellTab()-1) * (MGSize)::rand()) / (RAND_MAX) + 1;
//
//
//			if ( irnd == 0 && ! mpGrid->cColCell().is_valid( irnd) )
//				continue;
//		
//			vrnd = GridGeom<DIM_2D>::CreateSimplex( irnd, *mpGrid ).Center();
//
//			d = (vct - vrnd).module();
//			if ( d < dmin)
//			{
//				dmin = d;
//				icell = irnd;
//			}
//		}
//	}
//	else
//	{
//		icell = istart;
//	}
//
//
//	static vector<GCell*>	track;
//	static vector<MGSize>	stacknei;
//
//	track.reserve( 20);
//	stacknei.reserve(GCell::SIZE);
//
//	track.resize(0);
//	stacknei.resize(0);
//
//
//	GFace	face;
//	MGSize val;
//
//
//	/////////////////////////////////////////////////////////////
//	// start walking
//	while ( true)
//	{
//		GCell	&cell = mpGrid->rCell( icell);
//
//		cell.Paint();
//		track.push_back( &cell);
//
//
//		stacknei.resize(0);
//
//		for ( MGSize i=0; i<GCell::SIZE; ++i)
//		{
//			cell.GetFaceVNOut( face, i);
//
//			MGFloat ores = orient2d( mpGrid->cNode( face.cNodeId(0)).cPos().cTab(), 
//				mpGrid->cNode( face.cNodeId(1)).cPos().cTab(), vct.cTab() );
//
//			if ( ores > 0 && face.cCellUp().first )
//				stacknei.push_back( face.cCellUp().first );
//		}
//
//		if ( stacknei.size() == 0 )
//			break;	// icell is valid index
//
//
//
//		val = stacknei.back();
//
//		bool bErr = false;
//
//		while ( mpGrid->cCell( val).IsPainted() )
//		{
//			stacknei.pop_back();
//
//			if ( stacknei.size() > 0)
//			{
//				val = stacknei.back();
//			}
//			else
//			{ 
//				bErr = true;
//				break;
//			}
//		}
//
//
//		icell = val;
//
//		if ( bErr )
//		{
//			TRACE( "\n");
//			TRACE( "is inside box = " << mCellTree.cBox().IsInside( vct) );
//			TRACE( "localization problem for point [" << vct.cX() << ", " << vct.cY() << ", " << vct.cZ() << "]" );
//			TRACE( "istart = " << istart);
//			TRACE( "last cell id = " << icell );
//			
//			// power search !!!!
//			TRACE( "starting power search");
//			MGSize idret = 0;
//			
//			Grid<DIM_2D>::ColCell::const_iterator	itr;
//			for ( itr = mpGrid->cColCell().begin(); itr != mpGrid->cColCell().end(); ++itr)
//			{
//				const GCell& cell = (*itr);
//				if ( GridGeom<DIM_2D>::CreateSimplex( cell, *mpGrid).IsInside( vct) )
//				{
//					idret = itr.index();
//					break;
//				}
//			}
//			
//			TRACE( "checking grid after localization problem occured");
//			
//			mpGrid->CheckConnectivity();
//
//
//			vector<MGSize>	tab;
//			tab.push_back( idret);
//
//			WriteGrdTEC<DIM_2D> wtec( mpGrid);
//			wtec.WriteCells( "_cell.dat", tab);
//
//
//
//			for ( vector<GCell*>::iterator it=track.begin(); it!=track.end(); ++it)
//				(*it)->WashPaint();
//
//
//			if ( idret == 0)
//				THROW_INTERNAL( "Localizator<DIM_2D>::Localize(...) - failed !!!");
//
//			return idret;
//		}
//
//
//
//		//if ( track.size() > mpGrd->SizeCellTab() )
//		//	THROW_INTERNAL( "local walk corrupted");
//
//		//printf( "	new icell = %d\n", icell);
//	}
//
//	/////////////////////////////////////////////////////////////
//	// clean up
//	for ( vector<GCell*>::iterator it=track.begin(); it!=track.end(); ++it)
//		(*it)->WashPaint();
//
//
//	if ( ! GridGeom<DIM_2D>::CreateSimplex( icell, *mpGrid).IsInside( vct) )
//	{
//			icell = 0;
//			
//			Grid<DIM_2D>::ColCell::const_iterator	itr;
//			for ( itr = mpGrid->cColCell().begin(); itr != mpGrid->cColCell().end(); ++itr)
//			{
//				const GCell& cell = (*itr);
//				if ( GridGeom<DIM_2D>::CreateSimplex( cell, *mpGrid).IsInside( vct) )
//				{
//					icell = itr.index();
//					break;
//				}
//			}
//	}
//
//
//	if ( ! GridGeom<DIM_2D>::CreateSimplex( icell, *mpGrid).IsInside( vct) )
//				THROW_INTERNAL( "Localizator<DIM_2D>::Localize(...) - failed !!!");
//
//
//	return icell;
//}



template class Localizator<DIM_1D>;
template class Localizator<DIM_2D>;
template class Localizator<DIM_3D>;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

