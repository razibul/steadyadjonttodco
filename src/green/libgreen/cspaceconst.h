#ifndef __CSPACECONST_H__
#define __CSPACECONST_H__

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

extern const char	CSP_END[];
extern const char	CSP_HEADER[];
extern const char	CSP_GLOBAL[];

extern const char	CSP_GLOB_FILE[];
extern const char	CSP_GLOB_FILESCALE[];
extern const char	CSP_GLOB_TECFILESCALE[];
extern const char	CSP_GLOB_UNIFORM[];
extern const char	CSP_GLOB_USERDEF[];

extern const char	CSP_SEC_VERTEXSRC[];
extern const char	CSP_SEC_EDGESRC[];
extern const char	CSP_SEC_FACESRC[];
extern const char	CSP_SEC_BREPSRC[];

extern const char	CSP_VERTEXSRC[];
extern const char	CSP_EDGESRC[];
extern const char	CSP_FACESRC[];
extern const char	CSP_BREPSRC[];

extern const char	CSP_VRTX[];
extern const char	CSP_EDGE[];
extern const char	CSP_FACE[];


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CSPACECONST_H__
