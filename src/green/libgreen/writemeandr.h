#ifndef __WRITEMEANDR_H__
#define __WRITEMEANDR_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libgreen/gridcontext.h"
#include "libcoregeom/metric.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


using namespace Geom;

template <Dimension DIM>
class Grid;


//////////////////////////////////////////////////////////////////////
// class WriteMEANDR
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class WriteMEANDR
{
public:
	WriteMEANDR( const Grid<DIM>& grd, const GridContext<Metric<DIM> >& cxmet) : mGrid(grd), mMetricCx(cxmet)	{}

	void	DoWrite( const MGString& fname);

private:
	const Grid<DIM>&		mGrid;
	const GridContext<Metric<DIM> >&	mMetricCx;
};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __WRITEMEANDR_H__
