#include "cssource.h"
#include "libcorecommon/regexp.h"
#include "libcoreio/store.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Dimension DIM, Dimension UNIDIM>
void Source<DIM,UNIDIM>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName() )
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),METRIC\\(([^|(\\)]*)\\),(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;
		MGString	buf;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mTopoId;

		vector<MGFloat>	tab;
		IO::ReadTabFloat( tab, rex.GetSubString( 2) );
		if ( UMetric::SIZE != tab.size() )
		{
			cout << sparams << endl;
			THROW_INTERNAL( "bad metric definition");
		}

		for ( MGSize i=0; i<tab.size(); ++i)
			mMetric[i] = tab[i];

		cout << mTopoId << endl;
		mMetric.Write();

		cout << rex.GetSubString( 3) << endl;

		if ( DIM == DIM_0D )	// init vertex source
		{
			RegExp	rexx;
			rexx.InitPattern( "^DFUNC\\(([^|(\\)]*)\\)(.*)$" );
			rexx.SetString( rex.GetSubString( 3) );

			if ( rex.IsOk() )
			{
				MGFloat growth;

				is.clear();
				is.str( rexx.GetSubString( 1) );
				is >> growth;
			}
			else
				THROW_INTERNAL("dfunc not recognized for vertex source");
		}
		else	// init edge/face source
		{
			RegExp	rexx;
			rexx.InitPattern( "^USE([A-Z]*)\\(#([0-9]*),([FT])\\)(.*)$");
			rexx.SetString( rex.GetSubString( 3));

			while ( rexx.IsOk() )
			{
				buf = rexx.GetSubString(4);

				is.clear();
				is.str( rexx.GetSubString( 2) );

				cout << rexx.GetSubString(1) << "  " << rexx.GetSubString(2) << "  " << rexx.GetSubString(3) << endl;

				MGSize srcid;
				is.clear();
				is.str( rexx.GetSubString( 2) );
				is >> srcid;


				bool bAniso;
				if ( rexx.GetSubString(3) == "T")
					bAniso = true;
				else
				if ( rexx.GetSubString(3) == "F")
					bAniso = false;
				else
					THROW_INTERNAL( "unrecognized boolean flag");


				if ( rexx.GetSubString(1) == CSP_VRTX)
				{
					mtabSrcId.push_back( DimBoolId( DIM_1D, srcid, bAniso) );
				}
				else
				if ( rexx.GetSubString(1) == CSP_EDGE)
				{
					if ( DIM <= DIM_1D) 
						THROW_INTERNAL( "can not use edge source in 1D");

					mtabSrcId.push_back( DimBoolId( DIM_2D, srcid, bAniso) );
				}
				else
				if ( rexx.GetSubString(1) == CSP_FACE)
				{
					if ( DIM <= DIM_2D) 
						THROW_INTERNAL( "can not use face source in 2D");

					mtabSrcId.push_back( DimBoolId( DIM_3D, srcid, bAniso) );
				}
				else
				{
					THROW_INTERNAL( "unrecognized source type in USE");
				}

				//if ( buf.size() == 0 )
				//	return tabres.size();

				rexx.InitPattern( "^,USE([A-Z]*)\\(#([0-9]*),([FT])\\)(.*)$");
				rexx.SetString( buf);
			}
		}
		
	}
}


//template <>
//void Source<DIM_0D,DIM_2D>::Init( const MGString& sname, MGString& sparams)
//{
//	if ( sname != ClassName() )
//	{
//		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
//		THROW_FILE( buf.c_str(), "" );
//	}
//}



template <Dimension DIM, Dimension UNIDIM>
void Source<DIM,UNIDIM>::WriteDef( ostream& of) const
{
	of << ClassName() << "( ";
	of << " )";
}



template class Source<DIM_0D,DIM_2D>;
template class Source<DIM_1D,DIM_2D>;
template class Source<DIM_2D,DIM_2D>;

template class Source<DIM_0D,DIM_3D>;
template class Source<DIM_1D,DIM_3D>;
template class Source<DIM_2D,DIM_3D>;
template class Source<DIM_3D,DIM_3D>;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

