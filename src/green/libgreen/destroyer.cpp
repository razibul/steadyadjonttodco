#include "destroyer.h"

#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/bndgrid.h"
#include "libgreen/gridgeom.h"
#include "libgreen/controlspace.h"
#include "libgreen/operator3d_collapseedge.h"
#include "libgreen/optimiser.h"

#include "libgreen/writegrdtec.h"
#include "libcorecommon/progressbar.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



void EdgeDestr::InsertNode( const GVect& vnod, vector<MGSize>& tabball )
{
	//cout << "EdgeDestr::InsertNode" << endl;


	MGSize icell = 0;
	for ( MGSize i=0; i<tabball.size(); ++i)
	{
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabball[i], mpKernel->cGrid());
		if ( tet.IsInside( vnod) )
		{
			icell = tabball[i];
			break;
		}
	}

	if ( icell == 0)
	{
		//cout << "is outside !!!" << endl;
		return;
	}

	GMetric met = mpCSpace->GetSpacing( vnod);	// TODO :: Expensive !!!


	/////////////////////////////////////////////////////////////
	// search for the delaunay cavity of vct
	static vector<MGSize>	tabccell;
	static vector<GFace>	tabcface;

	tabcface.reserve(60);
	tabccell.reserve(60);


	tabcface.resize(0);
	tabccell.resize(0);

	///
	//mpKernel->FindCavityMetric( tabccell, icell, vnod, met);
	//mpKernel->FindCavity( tabccell, icell, vnod);
	mpKernel->FindCavityBase( tabccell, tabball, icell, vnod);

	//tabccell.resize( tabball.size() );
	//copy( tabball.begin(), tabball.end(), tabccell.begin() );

	//for ( MGSize i=0; i<tabccell.size(); ++i)
	//	mpKernel->rGrid().rCell( tabccell[i] ).Paint();


	//cout << "tabccell.size() = " << tabccell.size() << endl;

	mpKernel->CavityCorrection( tabccell, vnod);

	if ( ! mpKernel->CavityBoundary( tabcface, tabccell ) )
	{
		// return NULL if cavity is _not_ found 
		return ;
	}	

	//cout << "tabccell.size() = " << tabccell.size() << endl;

	MGSize in =  mpKernel->InsertPoint( tabccell, tabcface, vnod);

	mpKernel->rGrid().UpdateNodeCellIds( tabccell);

	mpKernel->rGenData().InsertMetric( met, in);

	//cout << endl;
}


void EdgeDestr::BreakBall( const MGSize& inod, vector<MGSize>& tabball )
{
	GVect vnod = mpKernel->cGrid().cNode(inod).cPos();

	GVect vbcent(0,0,0);
	MGFloat w = 0.0;

	for ( MGSize i=0; i<tabball.size(); ++i)
	{
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabball[i], mpKernel->cGrid());
		GVect vc = tet.Center();
		MGFloat vol = tet.Volume();
		//if ( vol <= 0.0)
		//	vnod = vc;

		vbcent += vc * vol;
		w += vol;
	}

	vbcent /= w;
	vnod = 0.5 * ( vnod + vbcent );


	MGSize icell = 0;
	for ( MGSize i=0; i<tabball.size(); ++i)
	{
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabball[i], mpKernel->cGrid());
		if ( tet.IsInside( vnod) )
		{
			icell = tabball[i];
			break;
		}
	}

	if ( icell == 0)
		return;

	GMetric met = mpCSpace->GetSpacing( vnod);	// TODO :: Expensive !!!
	mpKernel->rGenData().InsertMetric( met, inod);


	/////////////////////////////////////////////////////////////
	// search for the delaunay cavity of vct
	static vector<MGSize>	tabccell;
	static vector<GFace>	tabcface;

	tabcface.reserve(60);
	tabccell.reserve(60);


	tabcface.resize(0);
	tabccell.resize(0);

	///
	mpKernel->FindCavityMetric( tabccell, icell, vnod, met);
	mpKernel->CavityCorrection( tabccell, vnod);

	if ( ! mpKernel->CavityBoundary( tabcface, tabccell ) )
	{
		// return NULL if cavity is _not_ found 
		return ;
	}	

	MGSize in =  mpKernel->InsertPoint( tabccell, tabcface, vnod);

	mpKernel->rGrid().UpdateNodeCellIds( tabccell);

	//mpKernel->rGrid().CheckConnectivity();

}


void EdgeDestr::EdgeCollapse( const MGSize& inold, const MGSize& innew, vector<MGSize>& tabc, vector<MGSize>& tabball)
{
	vector<MGSize> taberase;

	sort( tabc.begin(), tabc.end() );

	for ( MGSize i=0; i<tabball.size(); ++i)
		if ( ! binary_search( tabc.begin(), tabc.end(), tabball[i] ) )
			taberase.push_back( tabball[i] );


	//if ( taberase.size() + tabc.size() != tabball.size() )
	//	THROW_INTERNAL( "taberase.size() + tabc.size() != tabball.size() FAILED");

	//{
	//	WriteGrdTEC<DIM_3D>	write( & mpKernel->cGrid() );
	//	write.WriteCells( "_coll_ball.dat", tabball);
	//	write.WriteCells( "_coll_comp.dat", tabc);
	//	write.WriteCells( "_coll_erase.dat", taberase);
	//}

	for ( MGSize i=0; i<tabc.size(); ++i)
	{
		mpKernel->RemoveCellFromTree( tabc[i] );

		GCell& cell = mpKernel->rGrid().rCell( tabc[i] );
		for ( MGSize k=0; k<GCell::SIZE; ++k)
			if ( cell.cNodeId(k) == inold )
				cell.rNodeId(k) = innew;
	}

	for ( MGSize i=0; i<taberase.size(); ++i)
	{
		MGSize kold=GCell::SIZE, knew=GCell::SIZE;

		const GCell& cell = mpKernel->cGrid().cCell( taberase[i] );
		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			if ( cell.cNodeId(k) == inold )
				kold = k;
			if ( cell.cNodeId(k) == innew )
				knew = k;
		}

		if ( knew == GCell::SIZE || kold == GCell::SIZE )
			THROW_INTERNAL( "EdgeDestr::EdgeCollapse :: problem");

		GCell& cellold = mpKernel->rGrid().rCell( cell.cCellId(kold).first );
		GCell& cellnew = mpKernel->rGrid().rCell( cell.cCellId(knew).first );

		cellold.rCellId( cell.cCellId(kold).second ).first  = cell.cCellId(knew).first;
		cellold.rCellId( cell.cCellId(kold).second ).second = cell.cCellId(knew).second;

		cellnew.rCellId( cell.cCellId(knew).second ).first  = cell.cCellId(kold).first;
		cellnew.rCellId( cell.cCellId(knew).second ).second = cell.cCellId(kold).second;
	}

	for ( MGSize ic=0; ic<tabc.size(); ++ic)
	{
		mpKernel->InsertCellIntoTree( tabc[ic]);

		const GCell& cell = mpKernel->cGrid().cCell( tabc[ic] );

		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, mpKernel->cGrid());
		MGFloat vol = tet.Volume();

		if ( vol <= 0.0 )
			THROW_INTERNAL( "EdgeDestr::EdgeCollapse :: problem - volume = " << vol );

		for ( MGSize k=0; k<GCell::SIZE; ++k)
			mpKernel->rGrid().rNode( cell.cNodeId(k) ).rCellId() = cell.cId();
	}


	for ( MGSize i=0; i<taberase.size(); ++i)
		//mpKernel->rGrid().EraseCell( taberase[i] );
		mpKernel->EraseCell( taberase[i] );

	//mpKernel->rGrid().UpdateNodeCellIds( tabccell);

	mpKernel->EraseNode( inold);
}


bool EdgeDestr::RemoveNode()
{
	//ofstream of( "_dump_removenode.txt" );

	if ( mtabInNode.size() == 0 )
		return false;

	//random_shuffle( mtabInNode.begin(), mtabInNode.end() );

	MGSize inod = mtabInNode.back();

	bool bdbg = false;
	if ( inod == 16711 )
	{
		cout << endl << "FOUND" << endl;
		bdbg = true;
	}

	MGSize inodL, inodR;
	vector<MGSize> tabedge;

	Key<2> edgeL, edgeR;
	vector<MGSize> tabL, tabR;

	// find 2 edges
	tabedge.reserve(2);
	for ( MGSize ie=0; ie<mtabSubEdge.size(); ++ie)
	{
		if ( mtabSubEdge[ie].cFirst() == inod )
			tabedge.push_back( ie);

		if ( mtabSubEdge[ie].cSecond() == inod )
			tabedge.push_back( ie);
	}

	if ( tabedge.size() != 2)
		THROW_INTERNAL( "EdgeDestr::RemoveNode() :: tabedge.size() != 2" );

	edgeL = mtabSubEdge[ tabedge[0] ];
	edgeR = mtabSubEdge[ tabedge[1] ];

	if ( edgeL.cFirst() == inod )
		inodL = edgeL.cSecond();
	else
		inodL = edgeL.cFirst();

	if ( edgeR.cFirst() == inod )
		inodR = edgeR.cSecond();
	else
		inodR = edgeR.cFirst();

	//cout << "inod  = " << inod  << endl;
	//cout << "inodL = " << inodL << endl;
	//cout << "inodR = " << inodR << endl;

	// find ball for inod
	MGSize icell = mpKernel->cGrid().cNode(inod).cCellId();
	vector<MGSize> tabball;

	if ( icell == 0)
		THROW_INTERNAL( "EdgeDestr::RemoveNode() :: icell==0 for inod = " << inod );

	mpKernel->FindBall( tabball, inod, icell );

	//cout << tabball.size() << endl;

	Operator3D_CollapseEdge opedgeL( *mpKernel);
	Operator3D_CollapseEdge opedgeR( *mpKernel);

	opedgeL.Init( inod, inodL, tabball);
	opedgeR.Init( inod, inodR, tabball);


	// make the choice
	bool bokL = true;
	if ( opedgeL.Error() != 0 )
		bokL = false;

	bool bokR = true;
	if ( opedgeR.Error() != 0 )
		bokR = false;

	if ( ! bokL && ! bokR)	// both are negative
	{
		//WriteGrdTEC<DIM_3D>	write( & mpKernel->cGrid() );
		//write.WriteCells( "_dest_ball.dat", tabball);

		//THROW_INTERNAL( "STOP");

		return false;
	}

	MGSize ch=0; // 1 left; 2 right

	//if ( volL > volR)
	if ( opedgeR.VolMinAfter() < opedgeL.VolMinAfter() )
		ch = 1;
	else
		ch = 2;

	//if ( (! bokL) || (opedgeR.QAfter() < opedgeL.QAfter() && bokR) )
	if ( (! bokL) || (opedgeR.VolMinAfter() > opedgeL.VolMinAfter() && bokR) )
		ch = 2;

	//if ( (! bokR) || (opedgeL.QAfter() < opedgeR.QAfter() && bokL) )
	if ( (! bokR) || (opedgeL.VolMinAfter() > opedgeR.VolMinAfter() && bokL) )
		ch = 1;

	if ( bdbg)
	{
		cout << "left  = " << opedgeL.VolMinAfter() << "  :   " << opedgeL.QAfter() << endl;
		cout << "right = " << opedgeR.VolMinAfter() << "  :   " << opedgeR.QAfter() << endl;
	}

	if ( ch != 1 && ch != 2 )
	{
		//cout << "left  = " << volL << "  :   " << qL << endl;
		//cout << "right = " << volR << "  :   " << qR << endl;
		THROW_INTERNAL( "EdgeDestr::RemoveNode() :: bad choice" );
	}

	//cout << endl << "ch = " << ch << endl;
	//cout << "left  = " << volL << " " << qL << endl;
	//cout << "right = " << volR << " " << qR << endl;

		
	// execute
	try
	{
	if ( ch == 1)
		opedgeL.Execute();
	else
	if ( ch == 2)
		opedgeR.Execute();
	else
		return false;
	}
	catch ( EHandler::Except& )
	{
		WriteGrdTEC<DIM_3D>	write( & mpKernel->cGrid() );
		write.WriteCells( "_dest_ball.dat", tabball);
		throw;
	}


	//THROW_INTERNAL( "STOP");


	Key<2> newsedge(inodL, inodR);
	newsedge.Sort();

	edgeL.Sort();
	edgeR.Sort();

	vector< Key<2> >	tabNSEdge;
	for ( MGSize i=0; i<mtabSubEdge.size(); ++i)
	{
		if ( mtabSubEdge[i] != edgeL && mtabSubEdge[i] != edgeR)
			tabNSEdge.push_back( mtabSubEdge[i] );
	}

	tabNSEdge.push_back( newsedge );

	mtabSubEdge.swap( tabNSEdge );


	mtabInNode.pop_back();

	return true;
}




/*
bool EdgeDestr::RemoveNode()
{
	//ofstream of( "_dump_removenode.txt" );

	if ( mtabInNode.size() == 0 )
		return false;

	random_shuffle( mtabInNode.begin(), mtabInNode.end() );

	MGSize inod = mtabInNode.back();

	MGSize inodL, inodR;
	vector<MGSize> tabedge;

	Key<2> edgeL, edgeR;
	vector<MGSize> tabL, tabR;

	// find 2 edges
	tabedge.reserve(2);
	for ( MGSize ie=0; ie<mtabSubEdge.size(); ++ie)
	{
		if ( mtabSubEdge[ie].cFirst() == inod )
			tabedge.push_back( ie);

		if ( mtabSubEdge[ie].cSecond() == inod )
			tabedge.push_back( ie);
	}

	if ( tabedge.size() != 2)
		THROW_INTERNAL( "EdgeDestr::RemoveNode() :: tabedge.size() != 2" );

	edgeL = mtabSubEdge[ tabedge[0] ];
	edgeR = mtabSubEdge[ tabedge[1] ];

	if ( edgeL.cFirst() == inod )
		inodL = edgeL.cSecond();
	else
		inodL = edgeL.cFirst();

	if ( edgeR.cFirst() == inod )
		inodR = edgeR.cSecond();
	else
		inodR = edgeR.cFirst();


	// find ball for inod
	MGSize icell = mpKernel->cGrid().cNode(inod).cCellId();
	vector<MGSize> tabball;

	if ( icell == 0)
		THROW_INTERNAL( "EdgeDestr::RemoveNode() :: icell==0 for inod = " << inod );

	mpKernel->FindBall( tabball, inod, icell );

	//of << "inod  = " << inod  << endl;
	//of << "inodL = " << inodL << endl;
	//of << "inodR = " << inodR << endl;

	// find left and right part of the ball
	for ( MGSize ic=0; ic<tabball.size(); ++ic)
	{
		const GCell& cell = mpKernel->cGrid().cCell( tabball[ic] );
		
		bool bisR = true;
		bool bisL = true;
		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			if ( cell.cNodeId(k) == inodL )
				bisL = false;

			if ( cell.cNodeId(k) == inodR )
				bisR = false;
		}

		if ( bisL )
			tabL.push_back( tabball[ic] );

		if ( bisR )
			tabR.push_back( tabball[ic] );
	}

	//of << "tabball.size() = " << tabball.size() << endl;
	//of << "tabL.size()    = " << tabL.size() << endl;
	//of << "tabR.size()    = " << tabR.size() << endl;


	// create proposition cells for left and right collapse
	vector<GCell> tabcellL(tabL.size()), tabcellR(tabR.size());

	for ( MGSize i=0; i<tabL.size(); ++i)
	{
		tabcellL[i] = mpKernel->cGrid().cCell( tabL[i] );
		for ( MGSize k=0; k<GCell::SIZE; ++k)
			if ( tabcellL[i].cNodeId(k) == inod )
				tabcellL[i].rNodeId(k) = inodL;
	}

	for ( MGSize i=0; i<tabR.size(); ++i)
	{
		tabcellR[i] = mpKernel->cGrid().cCell( tabR[i] );
		for ( MGSize k=0; k<GCell::SIZE; ++k)
			if ( tabcellR[i].cNodeId(k) == inod )
				tabcellR[i].rNodeId(k) = inodR;
	}

	// check quality
	MGFloat volL, qL;
	for ( MGSize i=0; i<tabcellL.size(); ++i)
	{
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabcellL[i], mpKernel->cGrid());

		MGFloat vol = tet.Volume();
		if ( i==0 || vol < volL )
			volL = vol;

		MGFloat q = tet.QMeasureBETAmax();
		if ( i==0 || q < qL )
			qL = q;
	
		//cout << "-- left  = " << volL << " : " << vol << endl;
	}

	MGFloat volR, qR;
	for ( MGSize i=0; i<tabcellR.size(); ++i)
	{
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabcellR[i], mpKernel->cGrid());

		MGFloat vol = tet.Volume();
		if ( i==0 || vol < volR )
			volR = vol;

		MGFloat q = tet.QMeasureBETAmax();
		if ( i==0 || q < qR )
			qR = q;

		//cout << "-- right = " << volR << " : " << vol << endl;
	}

	//cout << "left  = " << volL << "  :   " << qL << endl;
	//cout << "right = " << volR << "  :   " << qR << endl << endl;


	// make the choice
	bool bokL = true;
	if ( volL <= 0.0 )
		bokL = false;

	bool bokR = true;
	if ( volR <= 0.0 )
		bokR = false;

	if ( ! bokL && ! bokR)	// both are negative
	{
		//WriteGrdTEC<DIM_3D>	write( & mpKernel->cGrid() );
		//write.WriteCells( "_dest_ball.dat", tabball);
		//write.WriteCells( "_dest_ball_L.dat", tabL);
		//write.WriteCells( "_dest_ball_R.dat", tabR);

		
		//BreakBall( inod, tabball );

		//THROW_INTERNAL( "STOP");

		return false;
	}

	MGSize ch=0; // 1 left; 2 right

	if ( (! bokL) || (qR < qL && bokR) )
		ch = 2;

	if ( (! bokR) || (qL < qR && bokL) )
		ch = 1;

	if ( volL > volR)
		ch = 1;
	else
		ch = 2;

	if ( ch != 1 && ch != 2 )
	{
		cout << "left  = " << volL << "  :   " << qL << endl;
		cout << "right = " << volR << "  :   " << qR << endl;
		THROW_INTERNAL( "EdgeDestr::RemoveNode() :: bad choice" );
	}

	//cout << endl << "ch = " << ch << endl;
	//cout << "left  = " << volL << " " << qL << endl;
	//cout << "right = " << volR << " " << qR << endl;

		
	// execute
	try
	{
	if ( ch == 1)
		EdgeCollapse( inod, inodL, tabL, tabball);
	else
	if ( ch == 2)
		EdgeCollapse( inod, inodR, tabR, tabball);
	else
		return false;
	}
	catch ( EHandler::Except& )
	{
		cout << "ch = " << ch << endl;
		cout << "left  = " << volL << " " << qL << endl;
		cout << "right = " << volR << " " << qR << endl;

		cout << endl << "RIGHT:" << endl;
		for ( MGSize i=0; i<tabR.size(); ++i)
		{
			Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabR[i], mpKernel->cGrid());
			cout << tet.Volume() << endl;
		}
		cout << endl;

		cout << endl << "LEFT:" << endl;
		for ( MGSize i=0; i<tabL.size(); ++i)
		{
			Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabL[i], mpKernel->cGrid());
			cout << tet.Volume() << endl;
		}
		cout << endl;


		WriteGrdTEC<DIM_3D>	write( & mpKernel->cGrid() );
		write.WriteCells( "_dest_ball.dat", tabball);
		write.WriteCells( "_dest_ball_L.dat", tabL);
		write.WriteCells( "_dest_ball_R.dat", tabR);

		throw;
	}

 //	WriteGrdTEC<DIM_3D>	write( & mpKernel->cGrid() );
	//write.WriteCells( "_dest_ball.dat", tabball);
	//write.WriteCells( "_dest_ball_L.dat", tabL);
	//write.WriteCells( "_dest_ball_R.dat", tabR);


	//THROW_INTERNAL( "STOP");


	Key<2> newsedge(inodL, inodR);
	newsedge.Sort();

	vector< Key<2> >	tabNSEdge;
	for ( MGSize i=0; i<mtabSubEdge.size(); ++i)
	{
		if ( mtabSubEdge[i] != edgeL && mtabSubEdge[i] != edgeR)
			tabNSEdge.push_back( mtabSubEdge[i] );
	}

	tabNSEdge.push_back( newsedge );

	mtabSubEdge.swap( tabNSEdge );


	mtabInNode.pop_back();

	return true;
}
*/

bool EdgeDestr::RemoveNode( const MGSize& id)
{
	if ( mtabInNode.size() == 0 )
		return false;

	//random_shuffle( mtabInNode.begin(), mtabInNode.end() );

	MGSize inod = mtabInNode.back();

	MGSize inodL, inodR;
	vector<MGSize> tabedge;

	Key<2> edgeL, edgeR;
	vector<MGSize> tabL, tabR;

	// find 2 edges
	tabedge.reserve(2);
	for ( MGSize ie=0; ie<mtabSubEdge.size(); ++ie)
	{
		if ( mtabSubEdge[ie].cFirst() == inod )
			tabedge.push_back( ie);

		if ( mtabSubEdge[ie].cSecond() == inod )
			tabedge.push_back( ie);
	}

	if ( tabedge.size() != 2)
		THROW_INTERNAL( "EdgeDestr::RemoveNode() :: tabedge.size() != 2" );

	edgeL = mtabSubEdge[ tabedge[0] ];
	edgeR = mtabSubEdge[ tabedge[1] ];

	if ( edgeL.cFirst() == inod )
		inodL = edgeL.cSecond();
	else
		inodL = edgeL.cFirst();

	if ( edgeR.cFirst() == inod )
		inodR = edgeR.cSecond();
	else
		inodR = edgeR.cFirst();


	// find ball for inod
	MGSize icell = mpKernel->cGrid().cNode(inod).cCellId();
	vector<MGSize> tabball;

	if ( icell == 0)
		THROW_INTERNAL( "EdgeDestr::RemoveNode() :: icell==0 for inod = " << inod );

	mpKernel->FindBall( tabball, inod, icell );


	// find left and right part of the ball
	for ( MGSize ic=0; ic<tabball.size(); ++ic)
	{
		const GCell& cell = mpKernel->cGrid().cCell( tabball[ic] );
		
		bool bisR = true;
		bool bisL = true;
		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			if ( cell.cNodeId(k) == inodL )
				bisL = false;

			if ( cell.cNodeId(k) == inodR )
				bisR = false;
		}

		if ( bisL )
			tabL.push_back( tabball[ic] );

		if ( bisR )
			tabR.push_back( tabball[ic] );
	}

	// create proposition cells for left and right collapse
	vector<GCell> tabcellL(tabL.size()), tabcellR(tabR.size());

	for ( MGSize i=0; i<tabL.size(); ++i)
	{
		tabcellL[i] = mpKernel->cGrid().cCell( tabL[i] );
		for ( MGSize k=0; k<GCell::SIZE; ++k)
			if ( tabcellL[i].cNodeId(k) == inod )
				tabcellL[i].rNodeId(k) = inodL;
	}

	for ( MGSize i=0; i<tabR.size(); ++i)
	{
		tabcellR[i] = mpKernel->cGrid().cCell( tabR[i] );
		for ( MGSize k=0; k<GCell::SIZE; ++k)
			if ( tabcellR[i].cNodeId(k) == inod )
				tabcellR[i].rNodeId(k) = inodR;
	}

	// check quality
	MGFloat volL, qL;
	for ( MGSize i=0; i<tabcellL.size(); ++i)
	{
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabcellL[i], mpKernel->cGrid());

		MGFloat vol = tet.Volume();
		if ( i==0 || vol < volL )
			volL = vol;

		MGFloat q = tet.QMeasureBETAmax();
		if ( i==0 || q < qL )
			qL = q;
	
		//cout << "-- left  = " << volL << " : " << vol << endl;
	}

	MGFloat volR, qR;
	for ( MGSize i=0; i<tabcellR.size(); ++i)
	{
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabcellR[i], mpKernel->cGrid());

		MGFloat vol = tet.Volume();
		if ( i==0 || vol < volR )
			volR = vol;

		MGFloat q = tet.QMeasureBETAmax();
		if ( i==0 || q < qR )
			qR = q;

		//cout << "-- right = " << volR << " : " << vol << endl;
	}

	//cout << "left  = " << volL << "  :   " << qL << endl;
	//cout << "right = " << volR << "  :   " << qR << endl << endl;


	// make the choice
	bool bokL = true;
	if ( volL <= 0.0 )
		bokL = false;

	bool bokR = true;
	if ( volR <= 0.0 )
		bokR = false;

	if ( ! bokL && ! bokR)	// both are negative
	{
		//WriteGrdTEC<DIM_3D>	write( & mpKernel->cGrid() );
		//write.WriteCells( "_dest_ball.dat", tabball);
		//write.WriteCells( "_dest_ball_L.dat", tabL);
		//write.WriteCells( "_dest_ball_R.dat", tabR);

		
		//BreakBall( inod, tabball );

		//THROW_INTERNAL( "STOP");

		return false;
	}

	MGSize ch=0; // 1 left; 2 right

	if ( (! bokL) || (qR < qL && bokR) )
		ch = 2;

	if ( (! bokR) || (qL < qR && bokL) )
		ch = 1;

	if ( volL > volR)
		ch = 1;
	else
		ch = 2;

	if ( ch != 1 && ch != 2 )
	{
		cout << "left  = " << volL << "  :   " << qL << endl;
		cout << "right = " << volR << "  :   " << qR << endl;
		THROW_INTERNAL( "EdgeDestr::RemoveNode() :: bad choice" );
	}

	//cout << endl << "ch = " << ch << endl;
	//cout << "left  = " << volL << " " << qL << endl;
	//cout << "right = " << volR << " " << qR << endl;

		
	// execute
	try
	{
	if ( ch == 1)
		EdgeCollapse( inod, inodL, tabL, tabball);

	if ( ch == 2)
		EdgeCollapse( inod, inodR, tabR, tabball);
	}
	catch ( EHandler::Except& )
	{
		cout << "ch = " << ch << endl;
		cout << "left  = " << volL << " " << qL << endl;
		cout << "right = " << volR << " " << qR << endl;

		cout << endl << "RIGHT:" << endl;
		for ( MGSize i=0; i<tabR.size(); ++i)
		{
			Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabR[i], mpKernel->cGrid());
			cout << tet.Volume() << endl;
		}
		cout << endl;

		cout << endl << "LEFT:" << endl;
		for ( MGSize i=0; i<tabL.size(); ++i)
		{
			Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabL[i], mpKernel->cGrid());
			cout << tet.Volume() << endl;
		}
		cout << endl;


		WriteGrdTEC<DIM_3D>	write( & mpKernel->cGrid() );
		write.WriteCells( "_dest_ball.dat", tabball);
		write.WriteCells( "_dest_ball_L.dat", tabL);
		write.WriteCells( "_dest_ball_R.dat", tabR);

		throw;
	}

 //	WriteGrdTEC<DIM_3D>	write( & mpKernel->cGrid() );
	//write.WriteCells( "_dest_ball.dat", tabball);
	//write.WriteCells( "_dest_ball_L.dat", tabL);
	//write.WriteCells( "_dest_ball_R.dat", tabR);


	//THROW_INTERNAL( "STOP");

	Key<2> newsedge(inodL, inodR);
	newsedge.Sort();

	vector< Key<2> >	tabNSEdge;
	for ( MGSize i=0; i<mtabSubEdge.size(); ++i)
	{
		if ( mtabSubEdge[i] != edgeL && mtabSubEdge[i] != edgeR)
			tabNSEdge.push_back( mtabSubEdge[i] );
	}

	tabNSEdge.push_back( newsedge );

	mtabSubEdge.swap( tabNSEdge );


	mtabInNode.pop_back();

	return true;
}


bool EdgeDestr::PushNode()
{
	if ( mtabInNode.size() == 0 )
		return false;

	MGSize inod = mtabInNode.back();
	GVect vnod = mpKernel->cGrid().cNode(inod).cPos();

	// find ball for inod
	MGSize icell = mpKernel->cGrid().cNode(inod).cCellId();
	vector<MGSize> tabball;

	if ( icell == 0)
		THROW_INTERNAL( "EdgeDestr::RemoveNode() :: icell==0 for inod = " << inod );

	mpKernel->FindBall( tabball, inod, icell );

	//WriteGrdTEC<DIM_3D>	write( & mpKernel->cGrid() );
	//write.WriteCells( "_push_ball.dat", tabball);

	// find ball bnd faces
	vector< Key<3> >	tabtmp;
	vector< Key<3> >	tabfac, taboface;

	for ( MGSize i=0; i<tabball.size(); ++i)
	{
		const GCell& cell = mpKernel->cGrid().cCell( tabball[i] );
		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			Key<3> key = cell.FaceKey( k);
			key.Sort();
			tabtmp.push_back( key);
		}
	}

	sort( tabtmp.begin(), tabtmp.end() );

	for ( vector< Key<3> >::const_iterator itr = tabtmp.begin(); itr != tabtmp.end(); ++itr)
	{
		vector< Key<3> >::const_iterator itrnext = itr+1;
		if ( itrnext == tabtmp.end() )
		{
			taboface.push_back( *itr);
		}
		else
		{
			if ( *itr == *itrnext )
				++itr;
			else
				taboface.push_back( *itr);
		}
	}

	for ( MGSize i=0; i<taboface.size(); ++i)
		for ( MGSize k=0; k<3; ++k)
			if ( taboface[i].cElem(k) == inod )
				tabfac.push_back( taboface[i] );

	sort( tabfac.begin(), tabfac.end() );

	//write.WriteFaces("_push_bfaces.dat", tabfac);


	// find normal vector
	GVect vn(0,0,0);

	for ( MGSize i=0; i<tabball.size(); ++i)
	{
		const GCell& cell = mpKernel->cGrid().cCell( tabball[i] );
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, mpKernel->cGrid());
		GVect vc = tet.Center();

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			Key<3> key = cell.FaceKey( k);
			key.Sort();

			if ( binary_search( tabfac.begin(), tabfac.end(), key ) )
			{
				GFace face;
				cell.GetFaceVNIn( face, k);
				SimplexFace<DIM_3D> tri = GridGeom<DIM_3D>::CreateSimplexFace( face, mpKernel->cGrid() );
				vn += tri.Vn();
			}
		}
	}

	// find length
	MGFloat dist = -1.0;
	MGFloat distcoeff = 0.05;

	MGSize ne = 0;
	for ( MGSize i=0; i<tabball.size(); ++i)
	{
		const GCell& cell = mpKernel->cGrid().cCell( tabball[i] );
		for ( MGSize k=0; k<GCell::ESIZE; ++k)
		{
			Key<2> ke = cell.EdgeKey( k);
			GVect ve1 = mpKernel->cGrid().cNode(ke.cFirst()).cPos();
			GVect ve2 = mpKernel->cGrid().cNode(ke.cSecond()).cPos();
			MGFloat d = (ve2 - ve1).module();

			if ( d < dist || dist < 0.0)
				dist = d;
		}
	}

	//cout << "dist = " << dist << endl;

	GVect vpnt = vnod - (distcoeff * dist) * vn.versor();

	//ofstream of( "_push_sedge.dat");

	//of << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"" << endl;
	//of << "ZONE T=\"SEDGE\", N=2, E=1, F=FEPOINT, ET=LINESEG" << endl;
	//of << vnod.cX() << " "  << vnod.cY() << " "  << vnod.cZ() << " " << inod <<  endl;
	//of << vpnt.cX() << " "  << vpnt.cY() << " "  << vpnt.cZ() << " " << 0 <<  endl;
	//of << "1 2" << endl;


	// insert new point
	InsertNode( vpnt, tabball );
	//mpKernel->cGrid().CheckConnectivity();


	// find new ball for inod
	MGSize incell = mpKernel->cGrid().cNode(inod).cCellId();

	if ( incell == 0)
		THROW_INTERNAL( "EdgeDestr::RemoveNode() :: icell==0 for inod = " << inod );

	//vector<MGSize> tabnewball;
	//mpKernel->FindBall( tabnewball, inod, incell );
	//WriteGrdTEC<DIM_3D>	write( & mpKernel->cGrid() );
	//write.WriteCells( "_push_newball.dat", tabnewball);


	//THROW_INTERNAL( "STOP");

	return true;
}


void Destroyer<DIM_3D>::Init( const vector< KeyData< Key<2>, vector< Key<2> > > >& tabedge)
{
	mTotN = 0;

	mtabDEdge.reserve( tabedge.size() );
	for ( vector< KeyData< Key<2>, vector< Key<2> > > >::const_iterator itr=tabedge.begin(); itr!=tabedge.end(); ++itr)
	{
		EdgeDestr dedge( mpCSpace, mpKernel );

		dedge.mEdge = itr->first;
		dedge.mtabSubEdge.resize( itr->second.size() );

		vector<MGSize> tabn;
		for ( MGSize i=0; i<itr->second.size(); ++i)
		{
			if ( itr->second[i].cFirst() != itr->first.cFirst() && itr->second[i].cFirst() != itr->first.cSecond() )
				tabn.push_back( itr->second[i].cFirst() );

			if ( itr->second[i].cSecond() != itr->first.cFirst() && itr->second[i].cSecond() != itr->first.cSecond() )
				tabn.push_back( itr->second[i].cSecond() );

			dedge.mtabSubEdge[i] = itr->second[i];
		}

		sort( tabn.begin(), tabn.end() );
		tabn.erase( unique( tabn.begin(), tabn.end() ), tabn.end() );

		dedge.mtabInNode.resize( tabn.size() );
		copy( tabn.begin(), tabn.end(), dedge.mtabInNode.begin() );

		mTotN += tabn.size();

		mtabDEdge.push_back( dedge);
	}
}


void Destroyer<DIM_3D>::StartPurge()
{
	cout << endl;
	cout << "Removing Steiner points" << endl;

	ProgressBar	bar(40);

	mpKernel->rGrid().UpdateNodeCellIds();

	// remove by edge collapsing

	MGSize totcount = 0;
	//MGSize count;
	//do
	//{

	//	random_shuffle( mtabDEdge.begin(), mtabDEdge.end() );

	//	bar.Init( mtabDEdge.size() );
	//	bar.Start();

	//	count = 0;
	//	for ( vector<EdgeDestr>::iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr, ++bar )
	//	//for ( vector<EdgeDestr>::iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr )
	//	{
	//		random_shuffle( itr->mtabInNode.begin(), itr->mtabInNode.end() );
	//		//bool brm = itr->RemoveNode(0);
	//		bool brm = itr->RemoveNode();
	//		if ( brm )
	//			++count;
	//	}
	//	bar.Finish();

	//	totcount += count;
	//}
	//while ( count > 0);

	cout << endl;

	MGSize pushcount = 0;

	//do
	//{
	//	random_shuffle( mtabDEdge.begin(), mtabDEdge.end() );

	//	bar.Init( mtabDEdge.size() );
	//	bar.Start();

	//	count = 0;
	//	for ( vector<EdgeDestr>::iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr, ++bar )
	//	{
	//		bool brm = itr->RemoveNode();
	//		if ( brm )
	//		{
	//			++count;
	//		}
	//		else
	//		{
	//			if ( itr->PushNode() )
	//				++pushcount;

	//			bool brm = itr->RemoveNode( 0 );
	//			if ( brm )
	//				++count;
	//		}
	//	}
	//	bar.Finish();

	//	totcount += count;
	//}
	//while ( count > 0);


	MGSize	countleft = 0;
	for ( vector<EdgeDestr>::iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr )
	{
		//if ( itr->mtabInNode.size() > 0 )
		//	cout << itr->mtabInNode.size() << endl;

		countleft += itr->mtabInNode.size();
	}

	cout << "removed = " << totcount << " / " << mTotN << " / " << pushcount << " / " << countleft << endl;


	//// remove by point pushing
	//bar.Init( mtabDEdge.size() );
	//bar.Start();
	//for ( vector<EdgeDestr>::iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr, ++bar )
	//{
	//	itr->PushNode();
	//	//bool brm = itr->RemoveNode( 0 );
	//	bool brm = itr->RemoveNode();
	//}
	//bar.Finish();

	//do
	//{
	//	random_shuffle( mtabDEdge.begin(), mtabDEdge.end() );

	//	count = 0;
	//	for ( vector<EdgeDestr>::iterator itr=mtabDEdge.begin(); itr!=mtabDEdge.end(); ++itr )
	//	{
	//		itr->PushNode();
	//		bool brm = itr->RemoveNode();
	//		if ( brm )
	//			++count;
	//	}

	//	totcount += count;
	//}
	//while ( count > 0);

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

