#ifndef __GEOMCSPACECURV_H__
#define __GEOMCSPACECURV_H__

#include "libgreen/tdefs.h"
#include "libextget/geomentity.h"
#include "libgreen/controlspace.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

//////////////////////////////////////////////////////////////////////
//	class GeomCurvControlSpace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, Dimension UNIDIM>
class GeomCurvControlSpace : public DualControlSpace<DIM,UNIDIM>
{
	typedef typename TDefs<UNIDIM>::GVect	UVect;
	typedef typename TDefs<UNIDIM>::GMetric	UMetric;

	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

public:
	GeomCurvControlSpace( const GET::GeomEntity<DIM,UNIDIM>& gent, const MGFloat& ccoeff=1.0 ) : mCurvCoeff(ccoeff), mpGEnt( &gent)	{}
	virtual ~GeomCurvControlSpace()		{}

	virtual GMetric	GetSpacing( const GVect& vct) const;
	virtual UMetric	GetGlobSpacing( const GVect& vct) const;

protected:
	GeomCurvControlSpace() : mpGEnt(NULL)	{}

private:
	MGFloat mCurvCoeff;
	const GET::GeomEntity<DIM,UNIDIM>	*mpGEnt;
};
//////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
////	class NullGeomCurvControlSpace
////////////////////////////////////////////////////////////////////////
//template <Dimension DIM, Dimension UNIDIM>
//class NullGeomCurvControlSpace : public GeomCurvControlSpace<DIM,UNIDIM>
//{
//	typedef typename TDefs<DIM>::GVect		GVect;
//	typedef typename TDefs<DIM>::GMetric	GMetric;
//
//public:
//	NullGeomCurvControlSpace()		{}
//	virtual ~NullGeomCurvControlSpace()		{}
//
//	virtual GMetric	GetSpacing( const GVect& vct) const
//	{
//		GMetric met;
//		met.InitIso(0.04);
//		return met;
//	}
//};
////////////////////////////////////////////////////////////////////////



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GEOMCSPACECURV_H__
