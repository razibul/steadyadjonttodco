#ifndef __OPTIMISER_SURF_H__
#define __OPTIMISER_SURF_H__


#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libcoreconfig/configbase.h"
#include "libextget/geomentity.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class BndGrid;

template <Dimension DIM> 
class Kernel;





//////////////////////////////////////////////////////////////////////
// class OptimiserSurf
//////////////////////////////////////////////////////////////////////
class OptimiserSurf : public ConfigBase
{
	typedef TDefs<DIM_2D>::GVect		GVect;
	typedef TDefs<DIM_2D>::GMetric	GMetric;

	typedef TDefs<DIM_2D>::GNode		GNode;
	typedef TDefs<DIM_2D>::GCell		GCell;
	typedef TDefs<DIM_2D>::GFace		GFace;

public:
	OptimiserSurf( const GET::GeomEntity<DIM_2D,DIM_3D>& gent, Grid<DIM_2D>& grd) : mpSurf(&gent), mpGrid(&grd)		{}

	void	DoOptimise( const MGSize& maxiter = 1 );

private:
	const GET::GeomEntity<DIM_2D,DIM_3D>	*mpSurf;
	Grid<DIM_2D>							*mpGrid;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



#endif // __OPTIMISER_SURF_H__
