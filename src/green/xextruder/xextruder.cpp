#include "libcoresystem/mgdecl.h"
#include "libcorecommon/key.h"
#include "libcoregeom/vect.h"

#include "libcoreio/readmsh2.h"
#include "libcoreio/writemsh2.h"
#include "libcoreio/writetec.h"
//#include "gridfacade.h"

INIT_VERSION("0.1","'xextruder - wip'");


//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Grid : public IO::GridReadFacade, public IO::GridWriteFacade
{
public:
	friend class Extruder;

	typedef pair<Key<DIM>,MGSize>	BFPair;
	typedef Vect<DIM>				GVect;


	virtual Dimension	Dim() const		{ return DIM;}

	virtual void	IOSetName( const MGString& name)							{ mGrdName = name;}
	virtual void	IOGetName( MGString& name) const							{ name = mGrdName;}

	virtual void	IOTabNodeResize( const MGSize& n)							{ mtabNode.resize(n); }
	virtual void	IOTabCellResize( const MGSize& type, const MGSize& n)		{ if (type!=DIM+1) THROW_INTERNAL( "bad cell type "<< type ); mtabCell.resize(n); }
	virtual void	IOTabBFaceResize( const MGSize& type, const MGSize& n)		{ if (type!=DIM) THROW_INTERNAL( "bad bface type "<< type ); mtabBFace.resize(n); }

	virtual void	IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id );
	virtual void	IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id );
	virtual void	IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id );
	virtual void	IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id );


	virtual MGSize	IOSizeNodeTab() const										{ return mtabNode.size(); }
	virtual MGSize	IOSizeCellTab( const MGSize& type) const					{ if (type!=DIM+1) THROW_INTERNAL( "bad cell type "<< type ); return mtabCell.size(); }
	virtual MGSize	IOSizeBFaceTab( const MGSize& type) const					{ if (type!=DIM) THROW_INTERNAL( "bad bface type "<< type ); return mtabBFace.size(); }

	virtual void	IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	virtual void	IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	virtual void	IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id) const;

	virtual MGSize	IONumCellTypes() const										{ return 1;}
	virtual MGSize	IONumBFaceTypes() const										{ return 1;}
	virtual MGSize	IOCellType( const MGSize& i) const							{ ASSERT(i==0); return DIM+1; }
	virtual MGSize	IOBFaceType( const MGSize& i) const							{ ASSERT(i==0); return DIM;}


private:
	string					mGrdName;

	vector<GVect>			mtabNode;
	vector< Key<DIM+1> >	mtabCell;
	vector< BFPair >		mtabBFace;
};

template <Dimension DIM>
void Grid<DIM>::IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id )
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		mtabNode[id].rX(i) = tabx[i];
}

template <Dimension DIM>
void Grid<DIM>::IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
{
	ASSERT( type == DIM+1);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<=DIM; ++k)
		mtabCell[id].rElem(k) = tabid[k];
}

template <Dimension DIM>
void Grid<DIM>::IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
{
	ASSERT( type == DIM);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<DIM; ++k)
		mtabBFace[id].first.rElem(k) = tabid[k];
}

template <Dimension DIM>
void Grid<DIM>::IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id )
{
	ASSERT( type == DIM);
	mtabBFace[id].second = idsurf;
}

template <Dimension DIM>
inline void Grid<DIM>::IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mtabNode[id].cX(i);
}


template <Dimension DIM>
inline void Grid<DIM>::IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( type == DIM+1);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<=DIM; ++k)
		tabid[k] = mtabCell[id].cElem(k);
}


template <Dimension DIM>
inline void Grid<DIM>::IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( type == DIM);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<DIM; ++k)
		tabid[k] = mtabBFace[id].first.cElem(k);
}

template <Dimension DIM>
inline void Grid<DIM>::IOGetBFaceSurf(MGSize& idsurf, const MGSize& type, const MGSize& id) const
{
	ASSERT( type == DIM);
	idsurf = mtabBFace[id].second;
}


//////////////////////////////////////////////////////////////////////
class Extruder
{
public:
	Extruder( const MGFloat& h, const MGSize& n, const Grid<DIM_2D>& g2, Grid<DIM_3D>& g3) : mH(h), mN(n), mG2(g2), mG3(g3)	{}

	void	DoExtrude();

private:
	const Grid<DIM_2D>&	mG2;
	Grid<DIM_3D>&		mG3;

	MGFloat		mH;
	MGSize		mN;
};

void Extruder::DoExtrude()
{
	MGSize offset = mG2.mtabNode.size();

	// nodes
	mG3.mtabNode.resize( mG2.mtabNode.size() * (mN+1) );

	for ( MGSize i=0; i<mG2.mtabNode.size(); ++i)
	{
		const Vect2D& pos=mG2.mtabNode[i];

		for ( MGSize lev=0; lev<=mN; ++lev)
		{
			MGFloat z = lev * mH;
			Vect3D vct( pos.cX(), pos.cY(), z);
			mG3.mtabNode[ i + lev*offset ]  = vct;
		}
	}

	// cells
	mG3.mtabCell.reserve( mG2.mtabCell.size() * (mN+1) * 3 );

	for ( MGSize i=0; i<mG2.mtabCell.size(); ++i)
	{
		const Key<3>& ocell = mG2.mtabCell[i];

		for ( MGSize lev=1; lev<=mN; ++lev)
		{
			Key<3> cell;
			for ( MGSize j=0; j<3; ++j)
				cell.rElem(j) = ocell.cElem(j) + offset*(lev-1);

			vector<MGSize> tabi;
			vector<MGSize> tabiloc;

			tabi.push_back( cell.cElem(0) );
			tabi.push_back( cell.cElem(1) );
			tabi.push_back( cell.cElem(2) );
			sort( tabi.begin(), tabi.end() );

			Key<4> tet1( cell.cElem(0), cell.cElem(1), cell.cElem(2), tabi[0]+offset );
			mG3.mtabCell.push_back( tet1);


			Key<4> tet2;
			for ( MGSize j=0; j<3; ++j)
			{
				if ( cell.cElem(j) == tabi[0] )
					tet2.rElem(j) = tabi[0]+offset;
				else
					tet2.rElem(j) = cell.cElem(j);
			}
			tet2.rElem(3) = tabi[1]+offset;

			mG3.mtabCell.push_back( tet2);


			Key<4> tet3;
			for ( MGSize j=0; j<3; ++j)
			{
				if ( cell.cElem(j) == tabi[0] )
					tet2.rElem(j) = tabi[0]+offset;
				else
				if ( cell.cElem(j) == tabi[1] )
					tet2.rElem(j) = tabi[1]+offset;
				else
					tet2.rElem(j) = cell.cElem(j);
			}
			tet2.rElem(3) = tabi[2]+offset;

			mG3.mtabCell.push_back( tet2);
		}
	}

	// bfaces
	MGSize idsmax = 0;

	mG3.mtabBFace.reserve( mG2.mtabBFace.size() * (mN+1) * 2 );

	for ( MGSize i=0; i<mG2.mtabBFace.size(); ++i)
	{
		const Key<2>& ocell = mG2.mtabBFace[i].first;
		MGSize idsurf = mG2.mtabBFace[i].second;

		if ( idsurf > idsmax)
			idsmax = idsurf;

		for ( MGSize lev=1; lev<=mN; ++lev)
		{
			Key<2> cell;
			for ( MGSize j=0; j<2; ++j)
				cell.rElem(j) = ocell.cElem(j) + offset*(lev-1);

			vector<MGSize> tabi;
			vector<MGSize> tabiloc;

			tabi.push_back( cell.cElem(0) );
			tabi.push_back( cell.cElem(1) );
			sort( tabi.begin(), tabi.end() );

			Key<3> tet1( cell.cElem(0), cell.cElem(1), tabi[0]+offset );
			swap( tet1.rElem(0), tet1.rElem(1) );

			mG3.mtabBFace.push_back( Grid<DIM_3D>::BFPair( tet1, idsurf) );


			Key<3> tet2;
			for ( MGSize j=0; j<2; ++j)
			{
				if ( cell.cElem(j) == tabi[0] )
					tet2.rElem(j) = tabi[0]+offset;
				else
					tet2.rElem(j) = cell.cElem(j);
			}
			tet2.rElem(2) = tabi[1]+offset;
			swap( tet2.rElem(0), tet2.rElem(1) );

			mG3.mtabBFace.push_back( Grid<DIM_3D>::BFPair( tet2, idsurf) );
		}
	}

	for ( MGSize i=0; i<mG2.mtabCell.size(); ++i)
	{
		const Key<3>& ocell = mG2.mtabCell[i];

		mG3.mtabBFace.push_back( Grid<DIM_3D>::BFPair( ocell, idsmax+1) );

		Key<3> cell;
		for ( MGSize j=0; j<3; ++j)
			cell.rElem(j) = ocell.cElem(j) + offset*mN;
		
		swap( cell.rElem(0), cell.rElem(1) );



		mG3.mtabBFace.push_back( Grid<DIM_3D>::BFPair( cell, idsmax+2) );
	}
}


//////////////////////////////////////////////////////////////////////
int main( int argc, char* argv[])
{
	try
	{
		cout << "ver.: " << VERSION_NUMBER << "  build: " << VERSION_DATE << " " << VERSION_TIME << endl << endl;
		//---------------------------------------------------------------------
	
		if ( argc != 4)
		{
			printf( "call:\txextruder [h] [n] [msh2 name]\n");
			return 1;
		}

		MGFloat h;
		sscanf( argv[1], "%lg", &h);

		MGSize n;
		sscanf( argv[2], "%d", &n);

		//MGString fname = "bump_3.msh2";
		//MGString fname = "flatplate_lam_1.msh2";
		MGString fname = argv[3];
		bool bBIN = false;

		Grid<DIM_2D>	grid2d;
		Grid<DIM_3D>	grid3d;

		IO::ReadMSH2	reader( grid2d);
		reader.DoRead( fname, bBIN);


		IO::WriteTEC	wtec( grid2d);
		wtec.DoWrite( "exout.dat");

		Extruder ext( h, n, grid2d, grid3d);
		ext.DoExtrude();

		IO::WriteMSH2	writer( grid3d);
		writer.DoWrite( "exout3.msh2", false);

		IO::WriteTEC	wtec3( grid3d);
		wtec3.DoWrite( "exout3.dat");

		//---------------------------------------------------------------------

		return 0;


	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		cout << endl << "System exception: " << e.what() << endl;
	}
	catch( ...) 
	{
		TRACE( "Unknown exception thrown" );
		cout << endl << "Unknown exception thrown" << endl;
	}

	return 0;
} 

