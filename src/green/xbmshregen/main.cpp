#include "libcoresystem/mgdecl.h"
#include "libgreen/grid.h"
#include "libgreen/bndgrid.h"
#include "libgreen/generator.h"
#include "libgreen/writebndtec.h"
#include "libgreen/writegrdtec.h"
#include "libgreen/readbndsmesh.h"

#include "libgreen/gridgeom.h"
#include "libgreen/cspacegrid.h"

#include "libcorecommon/progressbar.h"

#include "libextnurbs/nurbscurve.h"

#include "libcorecommon/fgvector.h"

#include "libgreen/readtec.h"
#include "libgreen/readmeandr.h"




INIT_VERSION("0.1","''");

using namespace GREEN;



int main( int argc, char* argv[])
{
	try
	{
		cout << "ver.: " << VERSION_NUMBER << "  build: " << VERSION_DATE << " " << VERSION_TIME << endl << endl;
		///////////////////////////////////////////////////////////////////////
	
		exactinit();	// initialization of geom predicates

		///////////////////////////////////////////////////////////////////////

		GridWBnd<DIM_3D>	grid;
		BndGrid<DIM_3D>		&bnd = grid.rBndGrid();

		ReadBndSMESH<DIM_3D>	bndreader( &bnd);
		bndreader.DoRead( argv[2]);
		
		ProgressBar	bar(40);
		bar.Init( 1, false );
		bar.Start();

		bnd.RemoveDuplicatePointsPosBased();

		WriteBndTEC<DIM_3D>		bndwriter( &bnd);
		bndwriter.DoWrite( "boundary.plt");


		GridContext<TDefs<DIM_3D>::GMetric> cxmet;
		bnd.FindSpacing( cxmet);


		GridControlSpace<DIM_3D,DIM_3D>	cspace;
		cspace.Init( cxmet, bnd);


		Generator<DIM_3D>	gen( grid, &cspace);

		gen.Process();

		bar.Finish();
		
		WriteGrdTEC<DIM_3D> wtec( &grid);
		wtec.DoWrite( "_final_.dat");



		return 0;


	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		cout << endl << "System exception: " << e.what() << endl;
	}
	catch( ...) 
	{
		TRACE( "Unknown exception thrown" );
		cout << endl << "Unknown exception thrown" << endl;
	}

	return 0;
} 

