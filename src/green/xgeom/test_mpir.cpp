#include <iostream>
#include <iomanip>

#include <math.h>
#include <time.h> 

#include <mpirxx.h>
//#include <mpir.h>

/*
wiki pi = 
3.14159265358979323846264338327950288419716939937510582097494459230781640628620899
8628034825342117067982148086513282306647093844609550582231725359408128481117450284
1027019385211055596446229489549303819644288109756659334461284756482337867831652712
0190914564856692346034861045432664821339360726024914127372458700660631558817488152
0920962829254091715364367892590360011330530548820466521384146951941511609433057270
3657595919530921861173819326117931051185480744623799627495673518857527248912279381
8301194912983367336244065664308602139494639522473719070217986094370277053921717629
3176752384674818467669405132000568127145263560827785771342757789609173637178721468
4409012249534301465495853710507922796892589235420199561121290219608640344181598136
2977477130996051870721134999999837297804995105973173281609631859502445945534690830
2642522308253344685035261931188171010003137838752886587533208381420617177669147303
5982534904287554687311595628638823537875937519577818577805321712268066130019278766
1119590921642019893809525720106548586327886593615338182796823030195203530185296899
5773622599413891249721775283479131515574857242454150695950829533116861727855889075
0983817546374649393192550604009277016711390098488240128583616035637076601047101819
429555961989467678374494482553797747268471040475346462080466842590694912

mpf_class 4096 pi =
3.14159265358979323846264338327950288419716939937510582097494459230781640628620899
8628034825342117067982148086513282306647093844609550582231725359408128481117450284
1027019385211055596446229489549303819644288109756659334461284756482337867831652712
0190914564856692346034861045432664821339360726024914127372458700660631558817488152
0920962829254091715364367892590360011330530548820466521384146951941511609433057270
3657595919530921861173819326117931051185480744623799627495673518857527248912279381
8301194912983367336244065664308602139494639522473719070217986094370277053921717629
3176752384674818467669405132000568127145263560827785771342757789609173637178721468
4409012249534301465495853710507922796892589235420199561121290219608640344181598136
2977477130996051870721134999999837297804995105973173281609631859502445945534690830
2642522308253344685035261931188171010003137838752886587533208381420617177669147303
5982534904287554687311595628638823537875937519577818577805321712268066130019278766
1119590921642019893809525720106548586327886593615338182796823030195203530185296899
5773622599413891249721775283479131515574857242454150695950829533116861727855889075
0983817546374649393192550604009277016711390098488240128583616035637076601047101819
429549
1234567890123456789012345678901234567890123456789012345678901234567890123456789012  - 82
time = 33.422
*/


template <class T>
T factorial( const T& n)
{
	T m = 1;
	T mold = m;
	T diff;

	for ( T i=2; i<=n; i+=1)
	{
		m *= i;
		diff = m - mold;
		mold = m;
	}

	return diff;
}


template <class T>
T noprime( const T& m)
{
	T ret = 0;

	for ( T k=1; k<=m; ++k)
	{
		T count = 0;

		for ( T i=2; i<k; ++i)
		{
			if ( k % i == 0 )
				++count;
		}

		if ( count == 0 )
			++ret;
	}

	return ret;
}


template <class T>
T findpi( const int& n)
{
	T q = 4.0;
	T fac = 1.0;
	T sig = 1.0;
	T pi = 0.0;

	for ( int i=0; i<n; ++i)
	{
		pi += sig * q / fac;

		sig = -sig;
		fac += 2.0;
	}

	return pi;
}


template <class T>
T findpieul( const int& n)
{
	T q;
	T cfac = 3.0;
	T ccfac = 3.0;
	T fac = 3.0;
	T sig = -1.0;
	T pi = 1.0;

	for ( int i=0; i<n; ++i)
	{
		q = fac * ccfac;;
		pi += sig / q;

		sig = -sig;
		ccfac *= cfac;
		fac += 2.0;
	}

	pi = sqrt( T(12.0) ) * pi;

	return pi;
}


template <class T>
T findpieuler( const int& n)
{
	T q;
	T fac = 1.0;
	T sig = 1.0;
	T pi = 0.0;

	for ( int i=0; i<n; ++i)
	{
		q = sig / fac;
		pi += q * q;

		fac += 1.0;
	}

	pi = sqrt( T(6.0) * pi);

	return pi;
}


//int noprime( const int& m)
//{
//	int ret = 0;
//
//	for ( int k=1; k<=m; ++k)
//	{
//		int count = 0;
//
//		for ( int i=2; i<k; ++i)
//		{
//			if ( k % i == 0 )
//				++count;
//		}
//
//		if ( count == 0 )
//			++ret;
//	}
//
//	return ret;
//}




using namespace std;

int main()
{
	int nmax = 1000000;
	clock_t		start, finish;
	double		totaltime = 0, duration; 
	double		div = CLOCKS_PER_SEC;



	mpf_set_default_prec( 4096);

/*
	int fmax = 5000;


	//start = clock(); // start timing 
	//int df;
	//for ( int i=1; i<=fmax; ++i)
	//{
	//	df = factorial( (int)i);
	//	cout << df << endl;
	//}
	//finish = clock();	// end timing
	//cout << start << " " << finish << endl;

	//duration = (double)(finish - start) / div; 

	//cout << "int " << setprecision(50) << df << endl;
	//cout << "time = " << setprecision(8) << duration << endl;

	//cout << endl;
	//////////////////////////////////////////////



	//start = clock(); // start timing 
	//long long lf;
	//for ( int i=1; i<=fmax; ++i)
	//{
	//	lf = factorial( (long long)i);
	//	cout << lf << endl;
	//}
	//finish = clock();	// end timing
	//cout << start << " " << finish << endl;

	//duration = (double)(finish - start) / div; 

	//cout << "long long " << setprecision(50) << lf << endl;
	//cout << "time = " << setprecision(8) << duration << endl;

	//cout << endl;
	//////////////////////////////////////////////



	start = clock(); // start timing 
	mpz_class zf;
	for ( int i=1; i<=fmax; ++i)
	{
		zf = factorial( (mpz_class)i);
		//cout << zf << endl;
	}
	finish = clock();	// end timing
	cout << start << " " << finish << endl;

	duration = (double)(finish - start) / div; 

	cout << "mpz_class " << setprecision(150) << zf << endl;
	cout << "time = " << setprecision(8) << duration << endl;

	cout << endl;
	////////////////////////////////////////////

	start = clock(); // start timing 
	mpf_class rf;
	for ( int i=1; i<=fmax; ++i)
	{
		rf = factorial( (mpf_class)i);
		//cout << rf << endl;
	}
	finish = clock();	// end timing
	cout << start << " " << finish << endl;

	duration = (double)(finish - start) / div; 

	cout << "mpf_class " << setprecision(500) << rf << endl;
	cout << "time = " << setprecision(8) << duration << endl;

	cout << endl;
	////////////////////////////////////////////


	start = clock(); // start timing 
	double ddf;
	for ( int i=1; i<=fmax; ++i)
	{
		ddf = factorial( (double)i);
		//cout << setprecision(30) << ddf << endl;
	}
	finish = clock();	// end timing
	cout << start << " " << finish << endl;

	duration = (double)(finish - start) / div; 

	cout << "double " << setprecision(60) << ddf << endl;
	cout << "time = " << setprecision(8) << duration << endl;

	cout << endl;


	return 0;

*/

	////////////////////////////////////////////
	////////////////////////////////////////////
	////////////////////////////////////////////
	////////////////////////////////////////////



	int npi = 500000;
	////////////////////////////////////////////

	mpf_class xxx;

	start = clock(); // start timing 

	float fpi = findpieul<float>( npi);

	finish = clock();	// end timing
	cout << start << " " << finish << endl;

	duration = (double)(finish - start) / div; 

	xxx=fpi;
	cout << "float " << setprecision(150) << fpi << endl;
	cout << "time = " << setprecision(8) << duration << endl;

	cout << endl;

	////////////////////////////////////////////

	start = clock(); // start timing 

	double dpi = findpieul<double>( npi);

	finish = clock();	// end timing
	cout << start << " " << finish << endl;

	duration = (double)(finish - start) / div; 

	xxx=dpi;
	cout << "double " << setprecision(150) << dpi << endl;
	cout << "time = " << setprecision(8) << duration << endl;

	cout << endl;
	////////////////////////////////////////////

	start = clock(); // start timing 

	mpf_class rpi = findpieul<mpf_class>( npi);

	finish = clock();	// end timing
	cout << start << " " << finish << endl;

	duration = (double)(finish - start) / div; 

	cout << "double " << setprecision(5000) << rpi << endl;
	cout << "time = " << setprecision(8) << duration << endl;

	cout << endl;

	return 0;


	////////////////////////////////////////////
	////////////////////////////////////////////
	////////////////////////////////////////////
	////////////////////////////////////////////

	start = clock(); // start timing 

	int ifound = noprime( (int)10000);

	finish = clock();	// end timing
	cout << start << " " << finish << endl;

	duration = (double)(finish - start) / div; 

	cout << "int " << ifound << endl;
	cout << "time = " << setprecision(8) << duration << endl;


	cout << endl;
	////////////////////////////////////////////

	start = clock(); // start timing 

	long long lfound = noprime( (long long)10000);

	finish = clock();	// end timing
	cout << start << " " << finish << endl;

	duration = (double)(finish - start) / div; 

	cout << "long long " << lfound << endl;
	cout << "time = " << setprecision(8) << duration << endl;

	cout << endl;
	////////////////////////////////////////////

	start = clock(); // start timing 

	mpz_class rfound = noprime( (mpz_class)10000);

	finish = clock();	// end timing
	cout << start << " " << finish << endl;

	duration = (double)(finish - start) / div; 

	cout << "mpz_class " << rfound << endl;
	cout << "time = " << setprecision(8) << duration << endl;



	return 0;


	////////////////////////////////////////////
	mpf_t ap_a, ap_res;
	mpf_t ap_tmp, ap_coeff;

	mpf_init( ap_a);
	mpf_init( ap_res);
	mpf_init( ap_tmp);
	mpf_init( ap_coeff);

	//mpf_init_set_d( ap_a, 1.0);
	//mpf_init_set_d( ap_coeff, 3.0);

	//mpf_div( ap_res, ap_a, ap_coeff);

	//gmp_printf ("1/3 = %.200Ff\n", ap_res);

	//return 0;


	mpf_init_set_d( ap_a, 2.0);
	mpf_init_set_d( ap_coeff, 0.0001);

	start = clock(); // start timing 
	for ( int i=0; i<nmax; ++i)
	{
		mpf_mul( ap_a, ap_coeff, ap_a);
		mpf_sqrt( ap_res, ap_a);
		mpf_add( ap_a, ap_a, ap_res);
	}

	finish = clock();	// end timing
	duration = (double)(finish - start) / CLOCKS_PER_SEC; 

	cout << "time = " << setprecision(8) << duration << endl;

	gmp_printf ("%.150Ff\n", ap_a);


	////////////////////////////////////////////
	double d = 2.0;
	double dres;

	start = clock(); // start timing 
	for ( int i=0; i<nmax; ++i)
	{
		d *= 0.0001;
		//dres = ::sqrt( d);
		dres = d + d*5.0;
		d += dres;
	}

	finish = clock();	// end timing
	duration = (double)(finish - start) / CLOCKS_PER_SEC; 

	cout << "time = " << setprecision(8) << duration << endl;

	cout << setprecision(150) << d << endl;

	////////////////////////////////////////////
	mpf_class xd = 2.0;
	mpf_class xdres;

	start = clock(); // start timing 
	for ( int i=0; i<nmax; ++i)
	{
		xd *= 0.0001;
		//xdres = sqrt( xd);
		xdres = xd + xd*5.0;
		xd += xdres;
	}

	finish = clock();	// end timing
	duration = (double)(finish - start) / CLOCKS_PER_SEC; 

	cout << "time = " << setprecision(8) << duration << endl;

	cout << setprecision(150) << xd << endl;


	//////////////////////////////////////////////
	//long double ld = 2.0;
	//long double ldres = ::sqrt( ld);

	//start = clock(); // start timing 
	//for ( int i=0; i<nmax; ++i)
	//{
	//	ld *= 0.0001;
	//	ldres = ::sqrt( ld);
	//	ld += ldres;
	//}

	//finish = clock();	// end timing
	//duration = (double)(finish - start) / CLOCKS_PER_SEC; 

	//cout << "time = " << setprecision(8) << duration << endl;

	//cout << setprecision(100) << ldres << endl;


///////////////////////////////////////////////////

	//mpf_class a(1.0);
	//mpf_class b(2.0, 512);
	//mpf_class c(0.0,128);

	//c = sqrt( b);

	//double d;
	//d = ::sqrt( (double)2.0);

	//cout << setprecision(100) << d << endl;
	//cout << endl;

	//cout << setprecision(100) << c << endl;

	////mpz_class a, b, c;
	////a = 1234;
	////b = "-5678";
	////c = a+b;
	////cout << "sum is " << c << "\n";
	////cout << "absolute value is " << abs(c) << "\n";

	return 0;
}