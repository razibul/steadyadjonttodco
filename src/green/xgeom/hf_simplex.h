#ifndef __HF_SIMPLEX_H__
#define __HF_SIMPLEX_H__

#include "mgdecl.h"
#include "hf_gvector.h"
//#include "predicates.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {



// TODO :: define threshold values here

//////////////////////////////////////////////////////////////////////
// class ConnTriangle - connectivity for triangle
//////////////////////////////////////////////////////////////////////
class ConnTriangle
{
public:
	static const MGSize&	cFaceConn( const MGSize& ifc, const MGSize& in)	{ return mtabFaceConn[ifc][in];}	
	static const MGSize&	cEdgeConn( const MGSize& iec, const MGSize& in)	{ return mtabFaceConn[iec][in];}	

private:
	static MGSize	mtabFaceConn[3][2];
};

//////////////////////////////////////////////////////////////////////
// class ConnTetrahedron - connectivity for tetrahedron
//////////////////////////////////////////////////////////////////////
class ConnTetrahedron
{
public:
	static const MGSize&	cFaceConn( const MGSize& ifc, const MGSize& in)	{ return mtabFaceConn[ifc][in];}	
	static const MGSize&	cEdgeConn( const MGSize& iec, const MGSize& in)	{ return mtabEdgeConn[iec][in];}	

private:
	static MGSize	mtabFaceConn[4][3];
	static MGSize	mtabEdgeConn[6][2];
};






//////////////////////////////////////////////////////////////////////
// class Simplex
//////////////////////////////////////////////////////////////////////
template <MGSize DIM, MGSize UNIDIM, class T> 
class Simplex
{
public:
	typedef GVector<UNIDIM,T>	GVect;

	enum { SIZE = DIM+1};
	enum { ESIZE = DIM*(DIM+1)/2};

	class Facet : public Simplex<DIM-1,UNIDIM,T>
	{
	};


	Simplex( const GVect& v1, const GVect& v2);
	Simplex( const GVect& v1, const GVect& v2, const GVect& v3);
	Simplex( const GVect& v1, const GVect& v2, const GVect& v3, const GVect& v4);
	Simplex( const vector<GVect>& tabv);

	// returns volume with sign
	T		Volume() const;
	GVect	Center() const;
	
	// returns volume of a tetrahedron built with point vct and i-th face
	T		FacetPntVolume( const MGSize& i, const GVect& vct) const;

	T		InscribedSphereRadius() const;
	T		CircumSphereRadius() const;


	bool	IsInside( const GVect& vct, const T& zero=0) const;


	// if positive the vct is inside
	// if negative the vct is outside
	// if 0 the vct is cospherical
	// the calculation are not 100% exact - 0 may never happen
	T		IsInsideSphere( const GVect& vct) const;
	
	Facet		GetFacet( const MGSize& i) const;

	static MGSize			NumEdge()		{ return ESIZE;}

	static const MGSize&	cFaceConn( const MGSize& ifc, const MGSize& in);	
	static const MGSize&	cEdgeConn( const MGSize& iec, const MGSize& in);

private:

	const GVect*	mtab[SIZE];
};


//////////////////////////////////////////////////////////////////////
// class Simplex - specialization for 0D - point
//////////////////////////////////////////////////////////////////////
template <MGSize UNIDIM, class T> 
class Simplex<0,UNIDIM,T>
{
public:
	typedef GVector<UNIDIM,T>	GVect;

	enum { SIZE = 1};
	enum { ESIZE = 0};

	Simplex( const GVect& v1);

private:
	const GVect*	mPnt;
};



//////////////////////////////////////////////////////////////////////
// misc functions

//bool FindEdgeEdgeCross( Vect3D& vcross, const Vect3D& ve1b, const Vect3D& ve1e, const Vect3D& ve2b, const Vect3D& ve2e);
//
//bool FindEdgeFaceCross( Vect3D& vcross, const Vect3D& ve1, const Vect3D& ve2, const Vect3D& vf1, const Vect3D& vf2, const Vect3D& vf3);
//
//MGFloat EdgeDistance( const Vect3D& ve1, const Vect3D& ve2, const Vect3D& vct);
//


} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;


#endif // __HF_SIMPLEX_H__

