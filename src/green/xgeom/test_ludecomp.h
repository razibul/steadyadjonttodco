#ifndef __HF_TEST_LUDECOMP_H__
#define __HF_TEST_LUDECOMP_H__


#include "mgdecl.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


//////////////////////////////////////////////////////////////////////
// class Test_LUDecomp
//////////////////////////////////////////////////////////////////////
class Test_LUDecomp
{
public:
	bool	Run();

//private:

	template <class T, MGSize NMAX>
	bool CheckSolve( const MGSize& n);

	template <class T, MGSize NMAX>
	bool CheckDeterminant( const MGSize& n);
};
//////////////////////////////////////////////////////////////////////


template <class T, MGSize NMAX>
bool Test_LUDecomp::CheckSolve( const MGSize& n)
{
	typedef HFGeom::GMatrix<NMAX,T> MTX;
	if ( n > NMAX)
		THROW_INTERNAL( "Test_LUDecomp : n (" << n << ") is greater then " << NMAX );

	MTX	ludmtx(n,n);
	MTX	ludmtxB(n,1);

	for ( MGSize i=0; i<n; ++i)
		for ( MGSize j=0; j<n; ++j)
			ludmtx(i,j) = T(1) / T(i+j+1.);

	for ( MGSize i=0; i<n; ++i)
	{
		T t = T(0); 
		for ( MGSize j=0; j<n; ++j)
			t += (j+1)*ludmtx(i,j);
		ludmtxB(i,0) = t;
	}

	//ludmtx.Write(  );
	//ludmtxB.Write( );

	HFGeom::LUDecomp< T, MTX >	lud( ludmtx);

	lud.Execute();
	lud.Solve( ludmtxB);

	ludmtxB.Write( 32 );


	T err = T(0);
	for ( MGSize i=0; i<ludmtxB.NRows(); ++i)
		err += abs( ludmtxB(i,0) - T(i+1) );

	cout << "error X = " << setprecision(50) << err << endl;

	return true;
}


template <class T, MGSize NMAX>
bool Test_LUDecomp::CheckDeterminant( const MGSize& n)
{
	typedef HFGeom::GMatrix<NMAX,T> MTX;
	if ( n > NMAX)
		THROW_INTERNAL( "Test_LUDecomp : n (" << n << ") is greater then " << NMAX );

	MTX	ludmtx(n,n);

	for ( MGSize i=0; i<n; ++i)
		for ( MGSize j=0; j<n; ++j)
			ludmtx(i,j) = T(1) / T(i+j+1.);

	//ludmtx.Write(  );
	//ludmtxB.Write( );

	HFGeom::LUDecomp< T, MTX >	lud( ludmtx);

	lud.Execute();

	cout << "det = " << setprecision(50) << lud.GetDeterminant() << endl;

	return true;
}



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_TEST_LUDECOMP_H__  
