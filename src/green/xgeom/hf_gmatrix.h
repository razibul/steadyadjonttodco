#ifndef __HF_GMATRIX_H__
#define __HF_GMATRIX_H__


#include "mgdecl.h"
#include "hf_gvector.h"


#ifdef _MATRIX_SIZE_CHECKING

	#define CHECK(f) \
	{ \
		if (! (f) ) \
			THROW_INTERNAL( "CHECK error: '" #f "'"); \
	}

#else // _MATRIX_SIZE_CHECKING

	#define CHECK(f) \
	{ \
	}

#endif // _MATRIX_SIZE_CHECKING




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


//////////////////////////////////////////////////////////////////////
// class GMatrix
//////////////////////////////////////////////////////////////////////
template< MGSize MAX_SIZE, class ELEM_TYPE>
class GMatrix
{
public:
	enum { SIZE = MAX_SIZE * MAX_SIZE };
	typedef ELEM_TYPE	TYPE;


	GMatrix();
	GMatrix( const MGSize& nrows, const MGSize& ncols);
	GMatrix( const MGSize& nrows, const MGSize& ncols, const ELEM_TYPE& d);
	GMatrix( const GMatrix< MAX_SIZE, ELEM_TYPE>& a);
	GMatrix( const GVector< MAX_SIZE, ELEM_TYPE>& v);

    ELEM_TYPE&			operator()(const MGSize& i, const MGSize& j)		{ return mtab[i*MAX_SIZE + j]; }
    const ELEM_TYPE&	operator()(const MGSize& i, const MGSize& j) const	{ return mtab[i*MAX_SIZE + j]; }

    ELEM_TYPE&			operator[](const MGSize& i)							{ return mtab[i]; }
    const ELEM_TYPE&	operator[](const MGSize& i) const					{ return mtab[i]; }

    void	Init( const ELEM_TYPE& d);
	void	InitDelta( const MGSize& n);
	void	InitDelta( const GVector< MAX_SIZE, ELEM_TYPE>& v);
    void	Resize( const MGSize& nrows, const MGSize& ncols);
	void	Resize( const MGSize& nrows, const MGSize& ncols, const ELEM_TYPE& d)	{ Resize(nrows, ncols); Init(d);}

	GMatrix< MAX_SIZE, ELEM_TYPE>&	operator = ( const GMatrix< MAX_SIZE, ELEM_TYPE>& a);
	GMatrix< MAX_SIZE, ELEM_TYPE>&	operator = ( const GVector< MAX_SIZE, ELEM_TYPE>& v);

	GMatrix< MAX_SIZE, ELEM_TYPE>&	operator+=( const GMatrix< MAX_SIZE, ELEM_TYPE>& a);
	GMatrix< MAX_SIZE, ELEM_TYPE>&	operator-=( const GMatrix< MAX_SIZE, ELEM_TYPE>& a);

	GMatrix< MAX_SIZE, ELEM_TYPE>&	operator+=( const ELEM_TYPE& d);
	GMatrix< MAX_SIZE, ELEM_TYPE>&	operator-=( const ELEM_TYPE& d);
	GMatrix< MAX_SIZE, ELEM_TYPE>&	operator*=( const ELEM_TYPE& d);

	static MGSize	MaxSize()		{ return MAX_SIZE;}

	const MGSize&	NRows() const	{ return mNRows;}
	const MGSize&	NCols() const	{ return mNCols;}

	MGSize&			rNRows() 	{ return mNRows;}
	MGSize&			rNCols() 	{ return mNCols;}

	void	InsertSubMtx( const GMatrix<MAX_SIZE,ELEM_TYPE>& mtx, const MGSize& irow, const MGSize& icol, const MGSize& nrow, const MGSize& ncol);
	void	ExtractSubMtx( GMatrix<MAX_SIZE,ELEM_TYPE>& mtx, const MGSize& irow, const MGSize& icol, const MGSize& nrow, const MGSize& ncol) const;

	void	Transp();

	ELEM_TYPE	Determinant() const;

	void	Write( const MGSize& iprec=5, ostream& f=cout) const;

protected:
	ELEM_TYPE	Determinant2x2() const;
	ELEM_TYPE	Determinant3x3() const;
	ELEM_TYPE	Determinant4x4() const;
	ELEM_TYPE	Determinant5x5() const;
	ELEM_TYPE	Determinant6x6() const;


	void	RotateMatrix( GMatrix< MAX_SIZE, ELEM_TYPE>& a, 
						  ELEM_TYPE& g, ELEM_TYPE& h, ELEM_TYPE& s, ELEM_TYPE& c, ELEM_TYPE& tau, 
						  const MGInt& i, const MGInt& j, const MGInt& k, const MGInt& l);

	void	RotateMatrixNew( GMatrix< MAX_SIZE, ELEM_TYPE>& a, 
						  ELEM_TYPE& g, ELEM_TYPE& h, ELEM_TYPE& s, ELEM_TYPE& c, ELEM_TYPE& tau, 
						  const MGSize& i, const MGSize& j, const MGSize& k, const MGSize& l);

	ELEM_TYPE* GetCoreVec() { return mtab; };

private:
	MGSize		mNRows;
	MGSize		mNCols;
    ELEM_TYPE	mtab[SIZE];
};
//////////////////////////////////////////////////////////////////////





template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix< MAX_SIZE, ELEM_TYPE>::GMatrix() : mNRows(MAX_SIZE), mNCols(MAX_SIZE)
{
	for ( MGSize i=0; i<SIZE; mtab[i++]=ELEM_TYPE(0) );
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix< MAX_SIZE, ELEM_TYPE>::GMatrix( const MGSize& nrows, const MGSize& ncols) : mNRows(nrows), mNCols(ncols)
{
	CHECK( nrows <= MAX_SIZE);
	CHECK( ncols <= MAX_SIZE);

	for ( MGSize i=0; i<SIZE; mtab[i++]=ELEM_TYPE(0) );
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix< MAX_SIZE, ELEM_TYPE>::GMatrix( const MGSize& nrows, const MGSize& ncols, const ELEM_TYPE& d) : mNRows(nrows), mNCols(ncols)
{
	CHECK( nrows <= MAX_SIZE);
	CHECK( ncols <= MAX_SIZE);

	for ( MGSize i=0; i<SIZE; mtab[i++]=d );

	//for ( MGSize i=0; i<n; i++)
	//	mtab[i] = d;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix< MAX_SIZE, ELEM_TYPE>::GMatrix( const GMatrix< MAX_SIZE, ELEM_TYPE>& a) : mNRows(a.NRows()), mNCols(a.NCols())
{
	CHECK( a.NRows() <= MAX_SIZE);
	CHECK( a.NCols() <= MAX_SIZE);

    for (MGSize i=0; i < mNRows; ++i)
      for (MGSize j=0; j < mNCols; ++j)
        (*this)(i,j) = a(i,j);
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix< MAX_SIZE, ELEM_TYPE>::GMatrix( const GVector< MAX_SIZE, ELEM_TYPE>& v) : mNRows(v.NRows()), mNCols(1)
{
	CHECK( v.NRows() <= MAX_SIZE);

	for ( MGSize i=0; i<mNRows; ++i)
		(*this)(i,0) = v(i);
}




template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void GMatrix< MAX_SIZE, ELEM_TYPE>::Init( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; mtab[i++]=d );
	//const MGSize n = MAX_SIZE*MAX_SIZE;
	//for ( MGSize i=0; i<n; ++i)
	//	mtab[i] = d;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void GMatrix< MAX_SIZE, ELEM_TYPE>::InitDelta( const MGSize& n)
{
	CHECK( n <= MAX_SIZE);
	mNRows = mNCols = n;

	for ( MGSize i=0; i<SIZE; mtab[i++]=ELEM_TYPE(0) );

	for (MGSize i=0; i < mNRows; ++i)
		(*this)(i,i) = ELEM_TYPE(1);
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void GMatrix< MAX_SIZE, ELEM_TYPE>::InitDelta( const GVector< MAX_SIZE, ELEM_TYPE>& v)
{
	CHECK( v.Size() <= MAX_SIZE);
	mNRows = mNCols = v.Size();

	for ( MGSize i=0; i<SIZE; mtab[i++]=ELEM_TYPE(0) );

    for (MGSize i=0; i < mNRows; ++i)
		(*this)(i,i) = v(i);
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void GMatrix< MAX_SIZE, ELEM_TYPE>::Resize( const MGSize& nrows, const MGSize& ncols)
{
	CHECK( nrows <= MAX_SIZE);
	CHECK( ncols <= MAX_SIZE);
	mNRows = nrows;
	mNCols = ncols;
}

//////////////////////////////////////////////////////////////////////


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void GMatrix< MAX_SIZE, ELEM_TYPE>::Transp()
{
	GMatrix< MAX_SIZE, ELEM_TYPE>	atmp(mNCols, mNRows);
	MGSize i,j;

	for( i=0; i<mNRows; ++i)
		for( j=0; j<mNCols; ++j)
			atmp(j,i) = (*this)(i,j);

	(*this) = atmp;
}



//template< MGSize MAX_SIZE, class ELEM_TYPE>
//template < MGSize N, class T >
//inline void GMatrix< MAX_SIZE, ELEM_TYPE>::InsertSubMtx( const MGSize& irow, const MGSize& icol, const GMatrix<N,T>& mtx)
//{
//	CHECK( mtx.NRows()+irow <= mNRows && mtx.NCols()+icol <= mNCols );
//
//	if ( !( mtx.NRows()+irow <= mNRows && mtx.NCols()+icol <= mNCols ) )
//		THROW_INTERNAL( "CRASH !");
//
//	for ( MGSize i=0; i<mtx.NRows(); ++i)
//		for ( MGSize j=0; j<mtx.NCols(); ++j)
//			(*this)(i+irow,j+icol) = mtx(i,j);
//}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void GMatrix< MAX_SIZE, ELEM_TYPE>::ExtractSubMtx( GMatrix<MAX_SIZE,ELEM_TYPE>& mtx, const MGSize& irow, const MGSize& icol, const MGSize& nrow, const MGSize& ncol) const
{
	CHECK( irow+nrow < MAX_SIZE);
	CHECK( icol+ncol < MAX_SIZE);

	mtx.Resize( nrow, ncol);

	for ( MGSize i=0; i<nrow; ++i)
		for ( MGSize j=0; j<ncol; ++j)
			mtx(i,j) = (*this)(i+irow,j+icol);
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void GMatrix< MAX_SIZE, ELEM_TYPE>::InsertSubMtx( const GMatrix<MAX_SIZE,ELEM_TYPE>& mtx, const MGSize& irow, const MGSize& icol, const MGSize& nrow, const MGSize& ncol)
{
	CHECK( irow+nrow < MAX_SIZE);
	CHECK( icol+ncol < MAX_SIZE);

	mtx.Resize( nrow, ncol);

	for ( MGSize i=0; i<nrow; ++i)
		for ( MGSize j=0; j<ncol; ++j)
			(*this)(i+irow,j+icol) = mtx(i,j);
}



template< MGSize MAX_SIZE, class ELEM_TYPE>
inline ELEM_TYPE GMatrix< MAX_SIZE, ELEM_TYPE>::Determinant2x2() const
{
	CHECK( mNRows == mNCols == 2);

	return	(*this)(0,0)*(*this)(1,1) - (*this)(0,1)*(*this)(1,0) ;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline ELEM_TYPE GMatrix< MAX_SIZE, ELEM_TYPE>::Determinant3x3() const
{
	CHECK( mNRows == mNCols == 3);

	return	(*this)(0,0)*( (*this)(1,1)*(*this)(2,2) - (*this)(1,2)*(*this)(2,1) ) -
			(*this)(0,1)*( (*this)(1,0)*(*this)(2,2) - (*this)(1,2)*(*this)(2,0) ) +
			(*this)(0,2)*( (*this)(1,0)*(*this)(2,1) - (*this)(1,1)*(*this)(2,0) );

}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline ELEM_TYPE GMatrix< MAX_SIZE, ELEM_TYPE>::Determinant4x4() const
{
	CHECK( mNRows == mNCols == 4);

	GMatrix< MAX_SIZE, ELEM_TYPE> mtemp = (*this);

	ELEM_TYPE det2_23_01 = mtemp(2,0)*mtemp(3,1) - mtemp(2,1)*mtemp(3,0);
	ELEM_TYPE det2_23_02 = mtemp(2,0)*mtemp(3,2) - mtemp(2,2)*mtemp(3,0);
	ELEM_TYPE det2_23_03 = mtemp(2,0)*mtemp(3,3) - mtemp(2,3)*mtemp(3,0);
	ELEM_TYPE det2_23_12 = mtemp(2,1)*mtemp(3,2) - mtemp(2,2)*mtemp(3,1);
	ELEM_TYPE det2_23_13 = mtemp(2,1)*mtemp(3,3) - mtemp(2,3)*mtemp(3,1);
	ELEM_TYPE det2_23_23 = mtemp(2,2)*mtemp(3,3) - mtemp(2,3)*mtemp(3,2);

	ELEM_TYPE det3_123_012 = mtemp(1,0)*det2_23_12 - mtemp(1,1)*det2_23_02 + mtemp(1,2)*det2_23_01;
	ELEM_TYPE det3_123_013 = mtemp(1,0)*det2_23_13 - mtemp(1,1)*det2_23_03 + mtemp(1,3)*det2_23_01;
	ELEM_TYPE det3_123_023 = mtemp(1,0)*det2_23_23 - mtemp(1,2)*det2_23_03 + mtemp(1,3)*det2_23_02;
	ELEM_TYPE det3_123_123 = mtemp(1,1)*det2_23_23 - mtemp(1,2)*det2_23_13 + mtemp(1,3)*det2_23_12;

	return mtemp(0,0)*det3_123_123 - mtemp(0,1)*det3_123_023 + mtemp(0,2)*det3_123_013 - mtemp(0,3)*det3_123_012;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline ELEM_TYPE GMatrix< MAX_SIZE, ELEM_TYPE>::Determinant5x5() const
{
	CHECK( mNRows == mNCols == 5);

	const GMatrix< MAX_SIZE, ELEM_TYPE>& mtemp = (*this);

	ELEM_TYPE det2_34_01 = mtemp(3,0)*mtemp(4,1) - mtemp(3,1)*mtemp(4,0);
	ELEM_TYPE det2_34_02 = mtemp(3,0)*mtemp(4,2) - mtemp(3,2)*mtemp(4,0);
	ELEM_TYPE det2_34_03 = mtemp(3,0)*mtemp(4,3) - mtemp(3,3)*mtemp(4,0);
	ELEM_TYPE det2_34_04 = mtemp(3,0)*mtemp(4,4) - mtemp(3,4)*mtemp(4,0);
	ELEM_TYPE det2_34_12 = mtemp(3,1)*mtemp(4,2) - mtemp(3,2)*mtemp(4,1);
	ELEM_TYPE det2_34_13 = mtemp(3,1)*mtemp(4,3) - mtemp(3,3)*mtemp(4,1);
	ELEM_TYPE det2_34_14 = mtemp(3,1)*mtemp(4,4) - mtemp(3,4)*mtemp(4,1);
	ELEM_TYPE det2_34_23 = mtemp(3,2)*mtemp(4,3) - mtemp(3,3)*mtemp(4,2);
	ELEM_TYPE det2_34_24 = mtemp(3,2)*mtemp(4,4) - mtemp(3,4)*mtemp(4,2);
	ELEM_TYPE det2_34_34 = mtemp(3,3)*mtemp(4,4) - mtemp(3,4)*mtemp(4,3);

	ELEM_TYPE det3_234_012 = mtemp(2,0)*det2_34_12 - mtemp(2,1)*det2_34_02  + mtemp(2,2)*det2_34_01;
	ELEM_TYPE det3_234_013 = mtemp(2,0)*det2_34_13 - mtemp(2,1)*det2_34_03  + mtemp(2,3)*det2_34_01;
	ELEM_TYPE det3_234_014 = mtemp(2,0)*det2_34_14 - mtemp(2,1)*det2_34_04  + mtemp(2,4)*det2_34_01;
	ELEM_TYPE det3_234_023 = mtemp(2,0)*det2_34_23 - mtemp(2,2)*det2_34_03  + mtemp(2,3)*det2_34_02;
	ELEM_TYPE det3_234_024 = mtemp(2,0)*det2_34_24 - mtemp(2,2)*det2_34_04  + mtemp(2,4)*det2_34_02;
	ELEM_TYPE det3_234_034 = mtemp(2,0)*det2_34_34 - mtemp(2,3)*det2_34_04  + mtemp(2,4)*det2_34_03;
	ELEM_TYPE det3_234_123 = mtemp(2,1)*det2_34_23 - mtemp(2,2)*det2_34_13  + mtemp(2,3)*det2_34_12;
	ELEM_TYPE det3_234_124 = mtemp(2,1)*det2_34_24 - mtemp(2,2)*det2_34_14  + mtemp(2,4)*det2_34_12;
	ELEM_TYPE det3_234_134 = mtemp(2,1)*det2_34_34 - mtemp(2,3)*det2_34_14  + mtemp(2,4)*det2_34_13;
	ELEM_TYPE det3_234_234 = mtemp(2,2)*det2_34_34 - mtemp(2,3)*det2_34_24  + mtemp(2,4)*det2_34_23;

	ELEM_TYPE det4_1234_0123 = mtemp(1,0)*det3_234_123 - mtemp(1,1)*det3_234_023  + mtemp(1,2)*det3_234_013 - mtemp(1,3)*det3_234_012;
	ELEM_TYPE det4_1234_0124 = mtemp(1,0)*det3_234_124 - mtemp(1,1)*det3_234_024  + mtemp(1,2)*det3_234_014 - mtemp(1,4)*det3_234_012;
	ELEM_TYPE det4_1234_0134 = mtemp(1,0)*det3_234_134 - mtemp(1,1)*det3_234_034  + mtemp(1,3)*det3_234_014 - mtemp(1,4)*det3_234_013;
	ELEM_TYPE det4_1234_0234 = mtemp(1,0)*det3_234_234 - mtemp(1,2)*det3_234_034  + mtemp(1,3)*det3_234_024 - mtemp(1,4)*det3_234_023;
	ELEM_TYPE det4_1234_1234 = mtemp(1,1)*det3_234_234 - mtemp(1,2)*det3_234_134  + mtemp(1,3)*det3_234_124 - mtemp(1,4)*det3_234_123;

	return mtemp(0,0)*det4_1234_1234 - mtemp(0,1)*det4_1234_0234 + mtemp(0,2)*det4_1234_0134 - mtemp(0,3)*det4_1234_0124 + mtemp(0,4)*det4_1234_0123;
}



template< MGSize MAX_SIZE, class ELEM_TYPE>
inline ELEM_TYPE GMatrix< MAX_SIZE, ELEM_TYPE>::Determinant6x6() const
{
	CHECK( mNRows == mNCols == 6);

	const GMatrix< MAX_SIZE, ELEM_TYPE>& mtemp = (*this);

	ELEM_TYPE det2_45_34 = mtemp(4,3)*mtemp(5,4) - mtemp(4,4)*mtemp(5,3);
	ELEM_TYPE det2_45_24 = mtemp(4,2)*mtemp(5,4) - mtemp(4,4)*mtemp(5,2);
	ELEM_TYPE det2_45_23 = mtemp(4,2)*mtemp(5,3) - mtemp(4,3)*mtemp(5,2);
	ELEM_TYPE det2_45_14 = mtemp(4,1)*mtemp(5,4) - mtemp(4,4)*mtemp(5,1);
	ELEM_TYPE det2_45_13 = mtemp(4,1)*mtemp(5,3) - mtemp(4,3)*mtemp(5,1);
	ELEM_TYPE det2_45_12 = mtemp(4,1)*mtemp(5,2) - mtemp(4,2)*mtemp(5,1);
	ELEM_TYPE det2_45_04 = mtemp(4,0)*mtemp(5,4) - mtemp(4,4)*mtemp(5,0);
	ELEM_TYPE det2_45_03 = mtemp(4,0)*mtemp(5,3) - mtemp(4,3)*mtemp(5,0);
	ELEM_TYPE det2_45_02 = mtemp(4,0)*mtemp(5,2) - mtemp(4,2)*mtemp(5,0);
	ELEM_TYPE det2_45_01 = mtemp(4,0)*mtemp(5,1) - mtemp(4,1)*mtemp(5,0);
	ELEM_TYPE det2_45_35 = mtemp(4,3)*mtemp(5,5) - mtemp(4,5)*mtemp(5,3);
	ELEM_TYPE det2_45_25 = mtemp(4,2)*mtemp(5,5) - mtemp(4,5)*mtemp(5,2);
	ELEM_TYPE det2_45_15 = mtemp(4,1)*mtemp(5,5) - mtemp(4,5)*mtemp(5,1);
	ELEM_TYPE det2_45_05 = mtemp(4,0)*mtemp(5,5) - mtemp(4,5)*mtemp(5,0);
	ELEM_TYPE det2_45_45 = mtemp(4,4)*mtemp(5,5) - mtemp(4,5)*mtemp(5,4);
	
	ELEM_TYPE det3_345_234 = mtemp(3,2)*det2_45_34 - mtemp(3,3)*det2_45_24  + mtemp(3,4)*det2_45_23;
	ELEM_TYPE det3_345_134 = mtemp(3,1)*det2_45_34 - mtemp(3,3)*det2_45_14  + mtemp(3,4)*det2_45_13;
	ELEM_TYPE det3_345_124 = mtemp(3,1)*det2_45_24 - mtemp(3,2)*det2_45_14  + mtemp(3,4)*det2_45_12;
	ELEM_TYPE det3_345_123 = mtemp(3,1)*det2_45_23 - mtemp(3,2)*det2_45_13  + mtemp(3,3)*det2_45_12;
	ELEM_TYPE det3_345_034 = mtemp(3,0)*det2_45_34 - mtemp(3,3)*det2_45_04  + mtemp(3,4)*det2_45_03;
	ELEM_TYPE det3_345_024 = mtemp(3,0)*det2_45_24 - mtemp(3,2)*det2_45_04  + mtemp(3,4)*det2_45_02;
	ELEM_TYPE det3_345_023 = mtemp(3,0)*det2_45_23 - mtemp(3,2)*det2_45_03  + mtemp(3,3)*det2_45_02;
	ELEM_TYPE det3_345_014 = mtemp(3,0)*det2_45_14 - mtemp(3,1)*det2_45_04  + mtemp(3,4)*det2_45_01;
	ELEM_TYPE det3_345_013 = mtemp(3,0)*det2_45_13 - mtemp(3,1)*det2_45_03  + mtemp(3,3)*det2_45_01;
	ELEM_TYPE det3_345_012 = mtemp(3,0)*det2_45_12 - mtemp(3,1)*det2_45_02  + mtemp(3,2)*det2_45_01;
	ELEM_TYPE det3_345_235 = mtemp(3,2)*det2_45_35 - mtemp(3,3)*det2_45_25  + mtemp(3,5)*det2_45_23;
	ELEM_TYPE det3_345_135 = mtemp(3,1)*det2_45_35 - mtemp(3,3)*det2_45_15  + mtemp(3,5)*det2_45_13;
	ELEM_TYPE det3_345_125 = mtemp(3,1)*det2_45_25 - mtemp(3,2)*det2_45_15  + mtemp(3,5)*det2_45_12;
	ELEM_TYPE det3_345_035 = mtemp(3,0)*det2_45_35 - mtemp(3,3)*det2_45_05  + mtemp(3,5)*det2_45_03;
	ELEM_TYPE det3_345_025 = mtemp(3,0)*det2_45_25 - mtemp(3,2)*det2_45_05  + mtemp(3,5)*det2_45_02;
	ELEM_TYPE det3_345_015 = mtemp(3,0)*det2_45_15 - mtemp(3,1)*det2_45_05  + mtemp(3,5)*det2_45_01;
	ELEM_TYPE det3_345_245 = mtemp(3,2)*det2_45_45 - mtemp(3,4)*det2_45_25  + mtemp(3,5)*det2_45_24;
	ELEM_TYPE det3_345_145 = mtemp(3,1)*det2_45_45 - mtemp(3,4)*det2_45_15  + mtemp(3,5)*det2_45_14;
	ELEM_TYPE det3_345_045 = mtemp(3,0)*det2_45_45 - mtemp(3,4)*det2_45_05  + mtemp(3,5)*det2_45_04;
	ELEM_TYPE det3_345_345 = mtemp(3,3)*det2_45_45 - mtemp(3,4)*det2_45_35  + mtemp(3,5)*det2_45_34;

	ELEM_TYPE det4_2345_1234 = mtemp(2,1)*det3_345_234 - mtemp(2,2)*det3_345_134  + mtemp(2,3)*det3_345_124 - mtemp(2,4)*det3_345_123;
	ELEM_TYPE det4_2345_0234 = mtemp(2,0)*det3_345_234 - mtemp(2,2)*det3_345_034  + mtemp(2,3)*det3_345_024 - mtemp(2,4)*det3_345_023;
	ELEM_TYPE det4_2345_0134 = mtemp(2,0)*det3_345_134 - mtemp(2,1)*det3_345_034  + mtemp(2,3)*det3_345_014 - mtemp(2,4)*det3_345_013;
	ELEM_TYPE det4_2345_0124 = mtemp(2,0)*det3_345_124 - mtemp(2,1)*det3_345_024  + mtemp(2,2)*det3_345_014 - mtemp(2,4)*det3_345_012;
	ELEM_TYPE det4_2345_0123 = mtemp(2,0)*det3_345_123 - mtemp(2,1)*det3_345_023  + mtemp(2,2)*det3_345_013 - mtemp(2,3)*det3_345_012;
	ELEM_TYPE det4_2345_1235 = mtemp(2,1)*det3_345_235 - mtemp(2,2)*det3_345_135  + mtemp(2,3)*det3_345_125 - mtemp(2,5)*det3_345_123;
	ELEM_TYPE det4_2345_0235 = mtemp(2,0)*det3_345_235 - mtemp(2,2)*det3_345_035  + mtemp(2,3)*det3_345_025 - mtemp(2,5)*det3_345_023;
	ELEM_TYPE det4_2345_0135 = mtemp(2,0)*det3_345_135 - mtemp(2,1)*det3_345_035  + mtemp(2,3)*det3_345_015 - mtemp(2,5)*det3_345_013;
	ELEM_TYPE det4_2345_0125 = mtemp(2,0)*det3_345_125 - mtemp(2,1)*det3_345_025  + mtemp(2,2)*det3_345_015 - mtemp(2,5)*det3_345_012;
	ELEM_TYPE det4_2345_1245 = mtemp(2,1)*det3_345_245 - mtemp(2,2)*det3_345_145  + mtemp(2,4)*det3_345_125 - mtemp(2,5)*det3_345_124;
	ELEM_TYPE det4_2345_0245 = mtemp(2,0)*det3_345_245 - mtemp(2,2)*det3_345_045  + mtemp(2,4)*det3_345_025 - mtemp(2,5)*det3_345_024;
	ELEM_TYPE det4_2345_0145 = mtemp(2,0)*det3_345_145 - mtemp(2,1)*det3_345_045  + mtemp(2,4)*det3_345_015 - mtemp(2,5)*det3_345_014;
	ELEM_TYPE det4_2345_1345 = mtemp(2,1)*det3_345_345 - mtemp(2,3)*det3_345_145  + mtemp(2,4)*det3_345_135 - mtemp(2,5)*det3_345_134;
	ELEM_TYPE det4_2345_0345 = mtemp(2,0)*det3_345_345 - mtemp(2,3)*det3_345_045  + mtemp(2,4)*det3_345_035 - mtemp(2,5)*det3_345_034;
	ELEM_TYPE det4_2345_2345 = mtemp(2,2)*det3_345_345 - mtemp(2,3)*det3_345_245  + mtemp(2,4)*det3_345_235 - mtemp(2,5)*det3_345_234;

	ELEM_TYPE det5_12345_01234 = mtemp(1,0)*det4_2345_1234 - mtemp(1,1)*det4_2345_0234  + mtemp(1,2)*det4_2345_0134 - mtemp(1,3)*det4_2345_0124 + mtemp(1,4)*det4_2345_0123;
	ELEM_TYPE det5_12345_01235 = mtemp(1,0)*det4_2345_1235 - mtemp(1,1)*det4_2345_0235  + mtemp(1,2)*det4_2345_0135 - mtemp(1,3)*det4_2345_0125 + mtemp(1,5)*det4_2345_0123;
	ELEM_TYPE det5_12345_01245 = mtemp(1,0)*det4_2345_1245 - mtemp(1,1)*det4_2345_0245  + mtemp(1,2)*det4_2345_0145 - mtemp(1,4)*det4_2345_0125 + mtemp(1,5)*det4_2345_0124;
	ELEM_TYPE det5_12345_01345 = mtemp(1,0)*det4_2345_1345 - mtemp(1,1)*det4_2345_0345  + mtemp(1,3)*det4_2345_0145 - mtemp(1,4)*det4_2345_0135 + mtemp(1,5)*det4_2345_0134;
	ELEM_TYPE det5_12345_02345 = mtemp(1,0)*det4_2345_2345 - mtemp(1,2)*det4_2345_0345  + mtemp(1,3)*det4_2345_0245 - mtemp(1,4)*det4_2345_0235 + mtemp(1,5)*det4_2345_0234;
	ELEM_TYPE det5_12345_12345 = mtemp(1,1)*det4_2345_2345 - mtemp(1,2)*det4_2345_1345  + mtemp(1,3)*det4_2345_1245 - mtemp(1,4)*det4_2345_1235 + mtemp(1,5)*det4_2345_1234;

	return		mtemp(0,0)*det5_12345_12345 
			-	mtemp(0,1)*det5_12345_02345 
			+	mtemp(0,2)*det5_12345_01345 
			-	mtemp(0,3)*det5_12345_01245 
			+	mtemp(0,4)*det5_12345_01235
			-	mtemp(0,5)*det5_12345_01234;
}



template< MGSize MAX_SIZE, class ELEM_TYPE>
inline ELEM_TYPE GMatrix< MAX_SIZE, ELEM_TYPE>::Determinant() const
{
	CHECK( mNRows == mNCols);

	switch ( mNRows)
	{
	case 2:
		return Determinant2x2();

	case 3:
		return Determinant3x3();

	case 4:
		return Determinant4x4();

	case 5:
		return Determinant5x5();

	case 6:
		return Determinant6x6();
	}

	THROW_INTERNAL( "not implemented");

	return 0;
}






//////////////////////////////////////////////////////////////////////

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix< MAX_SIZE, ELEM_TYPE>& GMatrix< MAX_SIZE, ELEM_TYPE>::operator = ( const GMatrix< MAX_SIZE, ELEM_TYPE>& a)
{
	CHECK( a.NRows() <= MAX_SIZE);
	CHECK( a.NCols() <= MAX_SIZE);
	mNRows = a.mNRows;
	mNCols = a.mNCols;

    for (MGSize i=0; i < mNRows; ++i)
      for (MGSize j=0; j < mNCols; ++j)
        (*this)(i,j) = a(i,j);

	return *this;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix<MAX_SIZE, ELEM_TYPE>& GMatrix<MAX_SIZE, ELEM_TYPE>::operator += ( const GMatrix<MAX_SIZE, ELEM_TYPE>& a)
{
	CHECK( (NRows() == a.NRows()) && (NCols() == a.NCols()) );

	for( MGSize i=0; i<a.NRows(); ++i)
		for( MGSize j=0; j<a.NCols(); ++j)
			(*this)(i,j) += a(i,j);

	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix<MAX_SIZE, ELEM_TYPE>& GMatrix<MAX_SIZE, ELEM_TYPE>::operator -= ( const GMatrix<MAX_SIZE, ELEM_TYPE>& a)
{
	CHECK( (NRows() == a.NRows()) && (NCols() == a.NCols()) );

	for( MGSize i=0; i<a.NRows(); ++i)
		for( MGSize j=0; j<a.NCols(); ++j)
			(*this)(i,j) -= a(i,j);

	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix<MAX_SIZE, ELEM_TYPE>& GMatrix<MAX_SIZE, ELEM_TYPE>::operator += ( const ELEM_TYPE& d)
{
	for( MGSize i=0; i<NRows(); ++i)
		for( MGSize j=0; j<NCols(); ++j)
			(*this)(i,j) += d;

	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix<MAX_SIZE, ELEM_TYPE>& GMatrix<MAX_SIZE, ELEM_TYPE>::operator -= ( const ELEM_TYPE& d)
{
	for( MGSize i=0; i<NRows(); ++i)
		for( MGSize j=0; j<NCols(); ++j)
			(*this)(i,j) -= d;

	return *this;
}

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix<MAX_SIZE, ELEM_TYPE>& GMatrix<MAX_SIZE, ELEM_TYPE>::operator *= ( const ELEM_TYPE& d)
{
	for( MGSize i=0; i<NRows(); ++i)
		for( MGSize j=0; j<NCols(); ++j)
			(*this)(i,j) *= d;

	return *this;
}


//////////////////////////////////////////////////////////////////////

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix<MAX_SIZE, ELEM_TYPE> operator * ( const GMatrix<MAX_SIZE, ELEM_TYPE>& a1, const GMatrix<MAX_SIZE, ELEM_TYPE>& a2)
{
	CHECK( a1.NCols() == a2.NRows() );

	GMatrix<MAX_SIZE, ELEM_TYPE>	atmp(a1.NRows(), a2.NCols());
	for( MGSize i=0; i<a1.NRows(); ++i)
		for( MGSize j=0; j<a2.NCols(); ++j)
		{
			atmp(i,j) = 0.0;
			for( MGSize k=0; k<a1.NCols(); ++k)
				atmp(i,j) += a1(i,k) * a2(k,j);
		}

	return atmp;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix<MAX_SIZE, ELEM_TYPE> operator * ( const ELEM_TYPE& d, const GMatrix<MAX_SIZE, ELEM_TYPE>& a)
{
	GMatrix<MAX_SIZE, ELEM_TYPE>	atmp(a.NRows(), a.NCols());
	for( MGSize i=0; i<a.NRows(); ++i)
		for( MGSize j=0; j<a.NCols(); ++j)
			atmp(i,j) = d * a(i,j);

	return atmp;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix<MAX_SIZE, ELEM_TYPE> operator * ( const GMatrix<MAX_SIZE, ELEM_TYPE>& a, const ELEM_TYPE& d)
{
	GMatrix<MAX_SIZE, ELEM_TYPE>	atmp(a.NRows(), a.NCols());
	for( MGSize i=0; i<a.NRows(); ++i)
		for( MGSize j=0; j<a.NCols(); ++j)
			atmp(i,j) = d * a(i,j);

	return atmp;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix<MAX_SIZE, ELEM_TYPE> operator / ( const GMatrix<MAX_SIZE, ELEM_TYPE>& a, const ELEM_TYPE& d)
{
	GMatrix<MAX_SIZE, ELEM_TYPE>	atmp(a.NRows(), a.NCols());
	for( MGSize i=0; i<a.NRows(); ++i)
		for( MGSize j=0; j<a.NCols(); ++j)
			atmp(i,j) = a(i,j) / d;

	return atmp;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix<MAX_SIZE, ELEM_TYPE> operator + ( const GMatrix<MAX_SIZE, ELEM_TYPE>& a1, const GMatrix<MAX_SIZE, ELEM_TYPE>& a2)
{
	CHECK( (a1.NRows() == a2.NRows()) && (a1.NCols() == a2.NCols()) );

	GMatrix<MAX_SIZE, ELEM_TYPE>	atmp(a1.NRows(), a1.NCols());
	for( MGSize i=0; i<a1.NRows(); ++i)
		for( MGSize j=0; j<a1.NCols(); ++j)
			atmp(i,j) = a1(i,j) + a2(i,j);

	return atmp;
}


template< MGSize MAX_SIZE, class ELEM_TYPE>
inline GMatrix<MAX_SIZE, ELEM_TYPE> operator - ( const GMatrix<MAX_SIZE, ELEM_TYPE>& a1, const GMatrix<MAX_SIZE, ELEM_TYPE>& a2)
{
	CHECK( (a1.NRows() == a2.NRows()) && (a1.NCols() == a2.NCols()) );

	GMatrix<MAX_SIZE, ELEM_TYPE>	atmp(a1.NRows(), a1.NCols());
	for( MGSize i=0; i<a1.NRows(); ++i)
		for( MGSize j=0; j<a1.NCols(); ++j)
			atmp(i,j) = a1(i,j) - a2(i,j);

	return atmp;
}


//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////

template< MGSize MAX_SIZE, class ELEM_TYPE>
inline void GMatrix< MAX_SIZE, ELEM_TYPE>::Write( const MGSize& iprec, ostream& f) const
{
	f << "+-";
	for( MGSize j=0; j<mNCols; ++j)
		f << "-------------";
	f << "-+" << endl;

	for( MGSize i=0; i<mNRows; ++i)
	{
		f << "| ";
		for( MGSize j=0; j<mNCols; ++j)
			f << setprecision(iprec) << setw(12) << (*this)(i,j) << " ";
		f << " |" << endl;
	}

	f << "+-";
	for( MGSize j=0; j<mNCols; ++j)
		f << "-------------";
	f << "-+" << endl;
}



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;

#endif // __HF_GMATRIX_H__