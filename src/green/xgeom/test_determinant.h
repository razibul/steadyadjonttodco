#ifndef __HF_TEST_DETERMINANT_H__
#define __HF_TEST_DETERMINANT_H__


#include "mgdecl.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


//////////////////////////////////////////////////////////////////////
// class Test_Determinant
//////////////////////////////////////////////////////////////////////
class Test_Determinant
{
public:
	bool	Run();

//private:

	template <class T, MGSize NMAX>
	bool CheckDeterminant_Random( const MGSize& n, const T& rmin, const T& rmax);

	template <class T, MGSize NMAX>
	bool CheckDeterminant_Hilbert( const MGSize& n);
};
//////////////////////////////////////////////////////////////////////


template <class T, MGSize NMAX>
bool Test_Determinant::CheckDeterminant_Random( const MGSize& n, const T& rmin, const T& rmax)
{
	typedef HFGeom::GMatrix<NMAX,T> MTX;

	if ( n > NMAX)
		THROW_INTERNAL( "Test_Determinant : n (" << n << ") is greater then " << NMAX );

	MTX	mtx(n,n);
	HFGeom::InitMatrix< MTX::TYPE, MTX >::Random( mtx, n, rmin, rmax);

	HFGeom::Determinant< T, MTX >	det( mtx);

	det.Execute();

	cout << "det = " << setprecision(50) << det.GetDeterminant() << endl;

	return true;
}


template <class T, MGSize NMAX>
bool Test_Determinant::CheckDeterminant_Hilbert( const MGSize& n)
{
	typedef HFGeom::GMatrix<NMAX,T> MTX;

	if ( n > NMAX)
		THROW_INTERNAL( "Test_Determinant : n (" << n << ") is greater then " << NMAX );

	MTX	mtx(n,n);
	HFGeom::InitMatrix< MTX::TYPE, MTX >::Hilbert( mtx, n);

	HFGeom::Determinant< T, MTX >	det( mtx);

	det.Execute();

	cout << "det = " << setprecision(50) << det.GetDeterminant() << endl;

	return true;
}



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_TEST_DETERMINANT_H__