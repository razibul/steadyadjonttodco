#include "backgrid.h"


template< class ELEM_TYPE, MGSize MAX_SIZE>
inline void DecomposeROOT( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxout, const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx)
{
	// TODO :: check if matrix is symetric
	SVector< MAX_SIZE, ELEM_TYPE> vecD;
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxtmp = mtx;
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxN;

	mtxtmp.Decompose( vecD, mtxN, true);


	for ( MGSize k=0; k<MAX_SIZE; ++k)	// TODO :: check if it should not be Size() ???
		vecD(k) = 1.0 / ::sqrt( vecD(k));
		//mtxD(k,k) = 1.0 / ::pow( mtxD(k,k),0.4);

	SMatrix< MAX_SIZE, ELEM_TYPE> mtxNT = mtxN;
	mtxNT.Transp();

	mtxout.Assemble( mtxN, vecD, mtxNT );
	return;
}


template <Dimension DIM, class TYPE>
TYPE BackGrid<DIM,TYPE>::GetValue( const GVect& vct) const
{
	//cout << "-------------\n";
	vector< pair<MGSize,MGFloat> > tabcoeff;

	//++mCount;

	bool bOk = mInterpol.FindCoeff( tabcoeff, vct, false);

	if ( ! bOk)
	{
		//++mCountLost;
		
		//for ( MGSize k=0; k<DIM; ++k)
		//	mFile << setprecision(8) << vct.cX( k) << " ";
		//mFile << endl;

		if ( mBndGrid.SizeNodeTab() == 0 )
			THROW_INTERNAL( "GridGlobControlSpace<"<<DIM<<">::GetSpacing -- mBndGrid is not initialized");

		return GetBndValue( vct);
	}


	//++mCountHit;


	TYPE retmet = TYPE(0.0);
	TYPE smtxret = TYPE(0.0);

	for ( MGSize i=0; i<tabcoeff.size(); ++i)
	{
		MGSize idmet = mGrid.cNode( tabcoeff[i].first ).cMasterId();
		idmet = tabcoeff[i].first;
		TYPE smtx = mData.cData( idmet);

		//smtx.Write();

		//DecomposeROOT( smtx, smtx);

		//smtx.Write();
		//DecomposeSQR( smtx, smtx);
		//smtx.Write();

		smtxret += tabcoeff[i].second * smtx;
	}

	return smtxret;
}


template <Dimension DIM, class TYPE>
TYPE BackGrid<DIM,TYPE>::GetBndValue( const GVect& vct) const
{
	//cout << "-------------\n";
	vector< pair<MGSize,MGFloat> > tabcoeff;

	mBndInterpol.FindCoeff( tabcoeff, vct);

	TYPE retmet = TYPE(0.0);
	TYPE smtxret = TYPE(0.0);

	for ( MGSize i=0; i<tabcoeff.size(); ++i)
	{
		MGSize idmet = mBndGrid.cNode( tabcoeff[i].first ).cMasterId();
		TYPE smtx = mBndData.cData( idmet);

		//smtx.Write();

		//DecomposeROOT( smtx, smtx);

		//smtx.Write();
		//DecomposeSQR( smtx, smtx);
		//smtx.Write();

		smtxret += tabcoeff[i].second * smtx;
	}

	return smtxret;
}




//template <Dimension DIM, class TYPE>
//void BackGrid<DIM,TYPE>::Init( const GridContext<GMetric>& cxmet, const BndGrid<DIM>& bnd)
//{
//	//cout << "DIM = " << DIM << " UNIDIM = " << UNIDIM << endl;
//	//THROW_INTERNAL( "Not implemented");
//
//	GeneratorData<DIM> kerdata;
//	Kernel<DIM> kernel( mGrid, kerdata);
//
//
//	ProgressBar	bar(40);
//
//	bar.Init( bnd.SizeNodeTab() );
//	bar.Start();
//
//
//	GVect	vmin, vmax;
//
//	bnd.FindMinMax( vmin, vmax);
//	
//	kernel.InitBoundingBox( vmin, vmax);
//
//	kernel.InitCube();
//
//	// setting metric for corner nodes of the bounding box
//	for ( typename Grid<DIM>::ColNode::iterator itr = mGrid.rColNode().begin(); itr != mGrid.rColNode().end(); ++itr)
//	{
//		GMetric	met;
//		met.InitIso(1.0);
//
//		SMtx smtx( met);
//		DecomposeROOT( smtx, smtx);
//
//		MGSize idm = mMetricCx.Insert( smtx );
//		//mMetricCx.Insert( idm, smtx );
//		itr->rMasterId() = idm;
//	}
//
//
//
//	// triangulation of the points
//	vector<MGSize>	tabid(bnd.SizeNodeTab());
//	for ( MGSize i=1; i<bnd.SizeNodeTab()+1; ++i)
//		tabid[i-1] = i;
//
//	random_shuffle( tabid.begin(), tabid.end() );
//
//
//	for ( MGSize ii=0; ii<tabid.size(); ++ii, ++bar)
//	{
//		MGSize i = tabid[ii];
//
//		GVect vct = bnd.cNode(i).cPos();
//
//
//		MGSize inod = kernel.InsertPoint( vct );
//
//		if ( ! inod)
//		{
//			//char sbuf[512];
//			//sprintf( sbuf, "CRASH :: kernel failed to insert new point with coordinates [%lg %lg %lg]", 
//			//				vct.cX(), vct.cY(), vct.cZ() );
//
//			THROW_INTERNAL( "CRASH :: kernel failed to insert new point with coordinates [" << 
//				vct.cX() << " " << vct.cY() << " " << vct.cZ() << "]" );
//		}
//
//		SMtx smtx( cxmet.cData( bnd.cNode(i).cMasterId() ) );
//		DecomposeROOT( smtx, smtx);
//
//		MGSize idm = mMetricCx.Insert( smtx );
//		//mMetricCx.Insert( idm, smtx );
//		mGrid.rNode(inod).rMasterId() = idm;
//
//		//mpGrd->rNode(inod).rMasterId() = bnd.cNode(i).cMasterId();
//
//
//		//wtec.DoWrite( "_tmp_grd.plt");
//
//
//		//fprintf( f, "node id = %d\n", i);
//		//smtx = SMatrix<MGFloat,3>( bnd.cNode(i).cMetric());
//		//smtx.Decompose( smtxD, smtxL);
//		//smtxD.Write( f);
//		//smtxL.Write( f);
//	}
//	
//	bar.Finish();
//
//	WriteGrdTEC<DIM> wtec( &mGrid);
//	wtec.DoWrite( "cspacegrid_dim.plt");
//
//}


template <Dimension DIM, class TYPE>
void BackGrid<DIM,TYPE>::InitFileTEC( const MGString& fname)
{
	MGString fnameTEC = fname + MGString(".dat");

	//IOGreenCxSMtxProxy<DIM> cxproxy( &mMetricCx);
	//GREEN::IOReadGreenProxy<DIM>	grdproxy( &mGrid, NULL);

	//IO::ReadTEC	reader( grdproxy, &cxproxy );
	//reader.DoRead( fnameTEC);

}

template <Dimension DIM, class TYPE>
void BackGrid<DIM,TYPE>::PostInitTEC()
{
	vector< Key<DIM> > tabbnd;
	mGrid.RebuildConnectivity( &tabbnd );

	GVect	vmin, vmax;

	map<MGSize,MGSize>	mapbnode;
	for ( MGSize i=0; i<tabbnd.size(); ++i)
	{
		for ( MGSize k=0; k<DIM; ++k)
			if ( mapbnode.find( tabbnd[i].cElem(k) ) == mapbnode.end() )
				mapbnode.insert( map<MGSize,MGSize>::value_type( tabbnd[i].cElem(k), 0 ) );
	}

	vmin = vmax = mGrid.cNode( mapbnode.begin()->first ).cPos();
	MGSize id = 0;
	for ( map<MGSize,MGSize>::iterator itrn = mapbnode.begin(); itrn != mapbnode.end(); ++itrn)
	{
		itrn->second = ++id;
		const GVect& vpos = mGrid.cNode( itrn->first ).cPos();
		vmin = Minimum( vmin, vpos);
		vmax = Maximum( vmax, vpos);
	}


	// setup tree
	mInterpol.rLocalizator().InitBox( Box<DIM>( vmin, vmax) );  

	typename GREEN::Grid<DIM>::ColCell::const_iterator	itr;
	for ( itr = mGrid.cColCell().begin(); itr != mGrid.cColCell().end(); ++itr)
	{
		//mInterpol.rLocalizator().IncCounter();
		//if ( mInterpol.rLocalizator().TimeToInsert() )
		{
			GVect vc = GREEN::GridGeom<DIM>::CreateSimplex( *itr, mGrid).Center();
			mInterpol.rLocalizator().TreeInsertSafe( vc, itr->cId() );
		}
	}

	///////////////////////////////////////////////////////////
	ofstream f( "_bnd_.dat" );

	cout << "\nWriting TEC from PostInitTEC" << std::flush;

	f << "TITLE = \"boundary grid\"\n";
	f << "VARIABLES = ";
	f << "\"X\", \"Y\"";
	f << "\n";

	f << "ZONE T=\"bnd grid\", N=" << mapbnode.size() << ", E=" << tabbnd.size() << ", F=FEPOINT, ET=LINESEG\n";

	for ( map<MGSize,MGSize>::const_iterator itrn = mapbnode.begin(); itrn != mapbnode.end(); ++itrn)
	{
		const GVect& vpos = mGrid.cNode( itrn->first ).cPos();
		for ( MGSize k=0; k<DIM; ++k)
			f << setprecision(8) << vpos.cX( k) << " ";
		f << endl;
	}

	for ( MGSize i=0; i<tabbnd.size(); ++i)
	{
		for ( MGSize k=0; k<DIM; ++k)
			f << mapbnode[ tabbnd[i].cElem(k) ] << " ";
		f << endl;
	}

	cout << " - FINISHED\n";
	///////////////////////////////////////////////////////////


	GREEN::GeneratorData<DIM> kerdata;
	GREEN::Kernel<DIM> kernel( mBndGrid, kerdata);

	kernel.InitBoundingBox( vmin, vmax);
	kernel.InitCube();

	// setting metric for corner nodes of the bounding box
	for ( typename GREEN::Grid<DIM>::ColNode::iterator itr = mBndGrid.rColNode().begin(); itr != mBndGrid.rColNode().end(); ++itr)
	{
		typename GREEN::TDefs<DIM>::GMetric	met;
		met.InitIso(1.0);

		typename GREEN::TDefs<DIM>::GMetric::SMtx smtx( met);
		DecomposeROOT( smtx, smtx);

		MGSize idm = mBndData.Insert( 0.0 );
		//mMetricCx.Insert( idm, smtx );
		itr->rMasterId() = idm;
	}



	// triangulation of the boundary points
	for ( map<MGSize,MGSize>::iterator itrn = mapbnode.begin(); itrn != mapbnode.end(); ++itrn)
	{
		const GVect& vct = mGrid.cNode( itrn->first ).cPos();


		MGSize inod = kernel.InsertPoint( vct );

		if ( ! inod)
		{
			//char sbuf[512];
			//sprintf( sbuf, "CRASH :: kernel failed to insert new point with coordinates [%lg %lg %lg]", 
			//				vct.cX(), vct.cY(), vct.cZ() );

			THROW_INTERNAL( "CRASH :: kernel failed to insert new point with coordinates [" << 
				vct.cX() << " " << vct.cY() << " " << vct.cZ() << "]" );
		}

		TYPE smtx( mData.cData( itrn->first ) );
		//DecomposeROOT( smtx, smtx);

		MGSize idm = mBndData.Insert( smtx );
		//mMetricCx.Insert( idm, smtx );
		mBndGrid.rNode(inod).rMasterId() = idm;

		//mpGrd->rNode(inod).rMasterId() = bnd.cNode(i).cMasterId();


		//wtec.DoWrite( "_tmp_grd.plt");


		//fprintf( f, "node id = %d\n", i);
		//smtx = SMatrix<MGFloat,3>( bnd.cNode(i).cMetric());
		//smtx.Decompose( smtxD, smtxL);
		//smtxD.Write( f);
		//smtxL.Write( f);
	}
	

	//WriteGrdTEC<DIM> wtec( &mBndGrid);
	//wtec.DoWrite( "_bnd_cspacegrid_dim.plt");


}




template class BackGrid<DIM_2D,MGFloat>;
//template class GridGlobControlSpace<DIM_3D>;
