#ifndef __BACKGRID_H__
#define __BACKGRID_H__

#include "libcoresystem/mgdecl.h"

#include "libgreen/grid.h"
#include "libgreen/gridcontext.h"

#include "libgreen/kernel.h"
#include "libgreen/interpolator.h"



template <Dimension DIM, class TYPE>
class BackGrid
{
	typedef typename GREEN::TDefs<DIM>::GVect		GVect;

public:
	BackGrid() : mInterpol(&mGrid), mBndInterpol(&mBndGrid)		
	{ 
	}

	virtual ~BackGrid()		
	{ 
	}

	TYPE	GetValue( const GVect& vct) const;
	TYPE	GetBndValue( const GVect& vct) const;

	//void	Init( const GridContext<GMetric>& cxmet, const BndGrid<DIM>& bgrid);

	void	InitFileTEC( const MGString& fname);
	void	PostInitTEC();

private:
	GREEN::Grid<DIM>			mGrid;
	GREEN::GridContext<TYPE>		mData;
	GREEN::Interpolator<DIM>	mInterpol;

	GREEN::Grid<DIM>			mBndGrid;
	GREEN::GridContext<TYPE>		mBndData;
	GREEN::Interpolator<DIM>	mBndInterpol;


	/////////////////////////
};


#endif // __BACKGRID_H__
