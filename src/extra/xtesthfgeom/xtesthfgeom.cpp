#include "libcoresystem/mgdecl.h"
#include "libcorecommon/key.h"

#include "libhfgeom/hf_gmatrix.h"
#include "libhfgeom/test_determinant.h"

#include "libextsparse/blockmatrix.h"

#include "libcorecommon/stopwatch.h"


typedef HFGeom::TSize TSize;

template <TSize VAL>
class Signum
{
public:
	enum { SIGN = (VAL%2) ? -1 : 1 };
};



template <TSize ID_BLANK, class IDTAB>
class IndexBlankTab
{
public:
	static TSize GetId( const TSize& i)
	{
		return (i < ID_BLANK) ? IDTAB::GetId(i) : IDTAB::GetId(i+1);
	}

	template <TSize ID>
	class Get
	{
	public:
		enum { RET_ID = (ID < ID_BLANK) ? static_cast<TSize>(IDTAB::template Get<ID>::RET_ID) : static_cast<TSize>(IDTAB::template Get<ID+1>::RET_ID) };
	};
};


class IndexTab
{
public:
	static TSize GetId( const TSize& i)
	{
		return i;
	}

	template <TSize ID>
	class Get
	{
	public:
		enum { RET_ID = ID };
	};
};




template < class T, class MATRIX >
class Deter
{
public:
	Deter( const MATRIX& mtx) : mA(mtx)	{};

	T	CalcDet()
	{
		return Signum<MATRIX::MTX_SIZE>::SIGN * Sum< MATRIX::MTX_SIZE, MATRIX::MTX_SIZE >::Exec( mA);
	}

	template < TSize ICOL, TSize N, class TINDEX_ROW=IndexTab, class TINDEX_COL=IndexTab>
	class Sum
	{
	public:
		static T Exec( const MATRIX& mtxA)
		{
			//for ( TSize ir=0; ir<N; ++ir)
			//{
			//	cout << "| ";
			//	for ( TSize ic=0; ic<N; ++ic)
			//		cout << setw(8) << setprecision(4) << mtxA(TINDEX_ROW::GetId(ir), TINDEX_COL::GetId(ic)) << " " ;
			//	cout << " | " << endl;;
			//}
			//cout << endl;;

			T v = T( Signum< ICOL >::SIGN ) 
				* mtxA( TINDEX_ROW::template Get<0>::RET_ID,TINDEX_COL::template Get<ICOL-1>::RET_ID)
				* Sum< N-1, N-1,IndexBlankTab<0,TINDEX_ROW>, IndexBlankTab<ICOL-1,TINDEX_COL> >::Exec( mtxA);

			v += Sum<ICOL-1,N,TINDEX_ROW,TINDEX_COL>::Exec( mtxA); 

			return v;
		}
	};

	template <TSize N, class TINDEX_ROW, class TINDEX_COL>
	class Sum<0,N,TINDEX_ROW,TINDEX_COL>
	{
	public:
		static T Exec( const MATRIX& mtxA)
		{
			return 0;
		}
	};

	template <class TINDEX_ROW, class TINDEX_COL>
	class Sum<0,0,TINDEX_ROW,TINDEX_COL>
	{
	public:
		static T Exec( const MATRIX& mtxA)
		{
			return 1;
		}
	};


private:
	const MATRIX&	mA;
};



//////////////////////////////////////////////////////////////////////
int main( int argc, char* argv[])
{
	StopWatch	swatch;

	try
	{
		const TSize MAX = 6;
		SMatrix<MAX> mtxn, mtxq, mtxp;

		HFGeom::InitMatrix< MGFloat, SMatrix<MAX> >::Hilbert( mtxp, MAX);
		HFGeom::InitMatrix< MGFloat, SMatrix<MAX> >::Hilbert( mtxq, MAX);
		mtxn = mtxp;
		mtxn.Write();

	MGSize niter = 1000000;
	swatch.Reset(); swatch.Start();
		for ( MGSize i=0; i< niter; ++i)
		{
			mtxq.Invert();
		}
	swatch.Mark();
	cout << " :: time " << setprecision(6) << swatch.cDuration() << endl;

		mtxq.Write();
		(mtxn*mtxq).Write();


	swatch.Reset(); swatch.Start();
		for ( MGSize i=0; i< niter; ++i)
		{
			//mtxp.Invert3x3();
			//mtxp.Invert4x4();
			//mtxp.Invert5x5();
			mtxp.Invert6x6();
			//mtxp.Invert7x7();
			//mtxp.Invert8x8();
		}
	swatch.Mark();
	cout << " :: time " << setprecision(6) << swatch.cDuration() << endl;
	
		mtxp.Write();
		(mtxn*mtxp).Write();

		return 0;
		//////////////////////////////////////////////////////////////////////

/*
		const TSize N = 4;
		typedef Sparse::BlockMatrix<N,MGFloat> BMatrix;

		BMatrix mtx;

		HFGeom::InitMatrix< MGFloat, BMatrix >::Hilbert( mtx, N);
		//HFGeom::InitMatrix< MGFloat, BMatrix >::Random( mtx, N, 1, 8);

		//for ( TSize i=0; i<N; ++i)
		//	for ( TSize j=0; j<N; ++j)
		//		mtx(i,j) =  MGFloat(i+j+1.);

		mtx.Write();
		cout << endl;

	MGSize niter = 100000;
	swatch.Reset(); swatch.Start();
		MGFloat d1;
		for ( MGSize i=0; i< niter; ++i)
		{
			HFGeom::InitMatrix< MGFloat, BMatrix >::Random( mtx, N, 1, 8);
			Deter< MGFloat, BMatrix> det( mtx);
			d1 = det.CalcDet();
		}

	swatch.Mark();
	cout << " :: time " << setprecision(6) << swatch.cDuration() << endl;
	cout << " det = " << setprecision(50) << d1 << endl;


	swatch.Reset(); swatch.Start();
		MGFloat d2;
		for ( MGSize i=0; i< niter; ++i)
		{
			HFGeom::InitMatrix< MGFloat, BMatrix >::Random( mtx, N, 1, 8);
			HFGeom::Determinant< MGFloat, BMatrix >	detref( mtx);

			detref.Execute();
			d2 = detref.GetDeterminant();
		}

	swatch.Mark();
	cout << " :: time " << setprecision(6) << swatch.cDuration() << endl;
	cout << " det = " << setprecision(50) << d2 << endl;


		cout << endl;
		//return 0;

		typedef IndexBlankTab< 0, IndexBlankTab<1,IndexTab> >	INDEX;
		//typedef IndexBlankTab<1,IndexTab> INDEX;
		cout << INDEX::Get<0>::RET_ID << endl;
		cout << INDEX::Get<1>::RET_ID << endl;
		cout << INDEX::Get<2>::RET_ID << endl;
		cout << INDEX::Get<3>::RET_ID << endl;
		cout << INDEX::Get<4>::RET_ID << endl;
		cout << INDEX::Get<5>::RET_ID << endl;

		HFGeom::Test_Determinant	testdet;

		MGSize n = 6;
		testdet.CheckDeterminant_Hilbert<MGFloat,10>( n);
		testdet.CheckDeterminant_Hilbert<float,10>( n);

		testdet.CheckDeterminant_Random<MGFloat,10>( n, 1, 1000);
		testdet.CheckDeterminant_Random<float,10>(  n, 1, 1000);
*/
		return 0;


	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		cout << endl << "System exception: " << e.what() << endl;
	}
	catch( ...) 
	{
		TRACE( "Unknown exception thrown" );
		cout << endl << "Unknown exception thrown" << endl;
	}

	return 0;
} 

