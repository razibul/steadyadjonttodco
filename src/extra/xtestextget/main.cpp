#include "libcoresystem/mgdecl.h"
#include "libcorecommon/smatrix.h"


#include "libextget/geotopoio.h"
#include "libextget/geotopomaster.h"


const MGSize MAX_N = 20;




int main( int argc, char* argv[])
{
	try
	{
		//MGSize	n=20;
		//MGFloat	h = 1.0/(n-1.);
		//SMatrix<MAX_N>	mtxA(n,n,0.0), mtxAI;
		//SVector<MAX_N>	vecX(n,0.0), vecB(n, h*h*2.0), vecTmp(n,0.0);

		//for ( MGSize i=1; i<n-1; i++)
		//{
		//	mtxA(i,i-1) =  1.0;
		//	mtxA(i,i)	= -2.0;
		//	mtxA(i,i+1) =  1.0;
		//}

		//mtxA(0,0)	= -1.0;
		//mtxA(0,1)	= 1.0;

		//mtxA(n-1,n-1)	= -2.0;
		//mtxA(n-1,n-2)	= 1.0;

		//vecB(0) = -1.0*h;
		////vecB(n-1) = 0.5;


		////mtxA(3,3)	= 1;
		////mtxA(3,4) = mtxA(3,2)	= 0;
		////vecB(3) = 1.0;


		//mtxA.Write();
		//vecB.Write();

		//MGFloat det = mtxA.Determinant();

		//cout << det << endl;

		//mtxAI = mtxA;
		//mtxAI.Invert();

		//vecX = mtxAI * vecB;

		//vecX.Write();

		//ofstream of("data.plt");

		//for ( MGSize i=0; i<n; ++i)
		//{
		//	of << i*h;
		//	of << "  " << vecX(i) << endl;
		//}
		//
		//of << n*h;
		//of << "  " << 0.0 << endl;

		//return 1;

		MGString	fname("onera_m6.get");

		GET::GeoTopoMaster<Geom::DIM_3D>	getMaster;
		GET::GeoTopoIO<Geom::DIM_3D>	getIO( getMaster);

		getIO.DoRead( fname );

		getMaster.SyncNormals();

		//MGString	fname("channel_2d.get");

		//GET::GeoTopoMaster<Geom::DIM_2D>	getMaster;
		//GET::GeoTopoIO<Geom::DIM_2D>	getIO( getMaster);

		//getIO.DoRead( fname );

		//getMaster.SyncNormals();

	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		fprintf( stderr, "%s\n", e.what() );
	}

	return 0;
}
