#include "stepconst.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace StepSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

// constant variables used in STEP files

const char	STPHD_TOPOLOGY_SCHEMA[]		= "TOPOLOGY_SCHEMA";
const char	STPHD_GEOMETRY_SCHEMA[]		= "GEOMETRY_SCHEMA";
const char	STPHD_GEO_MODEL_SCHEMA[]	= "GEOMETRIC_MODEL_SCHEMA";


const char	STP_HEADER[]			= "HEADER";
const char	STP_FILE_DESCR[]		= "FILE_DESCRIPTION";
const char	STP_FILE_NAME[]			= "FILE_NAME";
const char	STP_FILE_SCHEMA[]		= "FILE_SCHEMA";
const char	STP_ENDSEC[]			= "ENDSEC";
const char	STP_DATA[]				= "DATA";
const char	STP_ISO[]				= "ISO-10303-21";
const char	STP_END_ISO[]			= "END-ISO-10303-21";


const char STP_FLAG_TRUE[]						= ".T.";
const char STP_FLAG_FALSE[]						= ".F.";
const char STP_FLAG_U[]							= ".U.";
const char STP_FLAG_UNSPECIFIED[]				= ".UNSPECIFIED.";
const char STP_FLAG_UNIFORM_KNOTS[]				= ".UNIFORM_KNOTS.";
const char STP_FLAG_QUASI_UNIFORM_KNOTS[]		= ".QUASI_UNIFORM_KNOTS.";
const char STP_FLAG_PIECEWISE_BEZIER_KNOTS[]	= ".PIECEWISE_BEZIER_KNOTS.";
const char STP_FLAG_POLYLINE_FORM[]				= ".POLYLINE_FORM.";
const char STP_FLAG_CIRCULAR_ARC[]				= ".CIRCULAR_ARC.";
const char STP_FLAG_ELLIPTIC_ARC[]				= ".ELLIPTIC_ARC.";
const char STP_FLAG_PARABOLIC_ARC[]				= ".PARABOLIC_ARC.";
const char STP_FLAG_HYPERBOLIC_ARC[]			= ".HYPERBOLIC_ARC.";

const char STP_FLAG_PLANE_SURF[]				= ".PLANE_SURF.";
const char STP_FLAG_CYLINDRICAL_SURF[]			= ".CYLINDRICAL_SURF.";
const char STP_FLAG_CONICAL_SURF[]				= ".CONICAL_SURF.";
const char STP_FLAG_SPHERICAL_SURF[]			= ".SPHERICAL_SURF.";
const char STP_FLAG_TOROIDAL_SURF[]				= ".TOROIDAL_SURF.";
const char STP_FLAG_SURF_OF_REVOLUTION[]		= ".SURF_OF_REVOLUTION.";
const char STP_FLAG_RULED_SURF[]				= ".RULED_SURF.";
const char STP_FLAG_GENERALISED_CONE[]			= ".GENERALISED_CONE.";
const char STP_FLAG_QUADRIC_SURF[]				= ".QUADRIC_SURF.";
const char STP_FLAG_SURF_OF_LINEAR_EXTRUSION[]	= ".SURF_OF_LINEAR_EXTRUSION.";


// STEP entities

const char STP_REPRESENTATION_ITEM[]			= "REPRESENTATION_ITEM";


// geometry
const char STP_GEOMETRIC_REPRESENTATION_ITEM[]	= "GEOMETRIC_REPRESENTATION_ITEM";

const char STP_DIRECTION[]						= "DIRECTION";
const char STP_VECTOR[]							= "VECTOR";
const char STP_AXIS1_PLACEMENT[]				= "AXIS1_PLACEMENT";
const char STP_AXIS2_PLACEMENT_3D[]				= "AXIS2_PLACEMENT_3D";

const char STP_POINT[]							= "POINT";
const char STP_CARTESIAN_POINT[]				= "CARTESIAN_POINT";

const char STP_CURVE[]							= "CURVE";
const char STP_PCURVE[]							= "PCURVE";
const char STP_BOUNDED_CURVE[]					= "BOUNDED_CURVE";
const char STP_B_SPLINE_CURVE[]					= "B_SPLINE_CURVE";
const char STP_B_SPLINE_CURVE_WITH_KNOTS[]		= "B_SPLINE_CURVE_WITH_KNOTS";
const char STP_QUASI_UNIFORM_CURVE[]			= "QUASI_UNIFORM_CURVE";
const char STP_RATIONAL_B_SPLINE_CURVE[]		= "RATIONAL_B_SPLINE_CURVE";
const char STP_TRIMMED_CURVE[]					= "TRIMMED_CURVE";
const char STP_LINE[]							= "LINE";
const char STP_CIRCLE[]							= "CIRCLE";
const char STP_ELLIPSE[]						= "ELLIPSE";
	
const char STP_SURFACE[]						= "SURFACE";
const char STP_BOUNDED_SURFACE[]				= "BOUNDED_SURFACE";
const char STP_B_SPLINE_SURFACE[]				= "B_SPLINE_SURFACE";
const char STP_B_SPLINE_SURFACE_WITH_KNOTS[]	= "B_SPLINE_SURFACE_WITH_KNOTS";
const char STP_QUASI_UNIFORM_SURFACE[]			= "QUASI_UNIFORM_SURFACE";
const char STP_RATIONAL_B_SPLINE_SURFACE[]		= "RATIONAL_B_SPLINE_SURFACE";
const char STP_PLANE[]							= "PLANE";
const char STP_CYLINDRICAL_SURFACE[]			= "CYLINDRICAL_SURFACE";
const char STP_CONICAL_SURFACE[]				= "CONICAL_SURFACE";
const char STP_SPHERICAL_SURFACE[]				= "SPHERICAL_SURFACE";
const char STP_TOROIDAL_SURFACE[]				= "TOROIDAL_SURFACE";
const char STP_SURFACE_OF_REVOLUTION[]			= "SURFACE_OF_REVOLUTION";
const char STP_SURFACE_OF_LINEAR_EXTRUSION[]	= "SURFACE_OF_LINEAR_EXTRUSION";


// topology

const char STP_TOPOLOGIC_REPRESENTATION_ITEM[]	= "TOPOLOGIC_REPRESENTATION_ITEM";

const char STP_VERTEX_POINT[]					= "VERTEX_POINT";
const char STP_VERTEX_LOOP[]					= "VERTEX_LOOP";

const char STP_ORIENTED_EDGE[]					= "ORIENTED_EDGE";
const char STP_EDGE_CURVE[]						= "EDGE_CURVE";
const char STP_EDGE_LOOP[]						= "EDGE_LOOP";

const char STP_ADVANCED_FACE[]					= "ADVANCED_FACE";
const char STP_FACE_BOUND[]						= "FACE_BOUND";
const char STP_FACE_OUTER_BOUND[]				= "FACE_OUTER_BOUND";
const char STP_OPEN_SHELL[]						= "OPEN_SHELL";
const char STP_CLOSED_SHELL[]					= "CLOSED_SHELL";
const char STP_ORIENTED_CLOSED_SHELL[]			= "ORIENTED_CLOSED_SHELL";

const char STP_BREP_WITH_VOIDS[]						= "BREP_WITH_VOIDS";
const char STP_MANIFOLD_SOLID_BREP[]					= "MANIFOLD_SOLID_BREP";
//const char STP_MANIFOLD_SURFACE_SHAPE_REPRESENTATION[]	= "MANIFOLD_SURFACE_SHAPE_REPRESENTATION";
const char STP_GEOMETRIC_CURVE_SET[]					= "GEOMETRIC_CURVE_SET";
const char STP_SHELL_BASED_SURFACE_MODEL[]				= "SHELL_BASED_SURFACE_MODEL";

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StepSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//




