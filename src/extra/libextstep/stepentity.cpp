#include "stepentity.h"
#include "libextstep/stepconst.h"
#include "libcorecommon/regexp.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace StepSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class StepEntity
//////////////////////////////////////////////////////////////////////
MGSize StepEntity::ReadMtxId( vector< vector<MGSize> >& mtxres, const MGString& sparams)
{
	vector<MGSize>	tab;

	MGString	buf;
	RegExp		rex;


	rex.InitPattern( "^\\(([^\\(\\)]*)\\)(.*)$");
	rex.SetString( sparams);
	while ( rex.IsOk() )
	{
		buf = rex.GetSubString(2);

		tab.clear();
		ReadTabId( tab, rex.GetSubString( 1) );
		mtxres.push_back( tab);


		if ( buf.size() == 0 )
			return mtxres.size();

		rex.InitPattern( "^,\\(([^\\(\\)]*)\\)(.*)$");
		rex.SetString( buf);
	}
	
	return 0;
}


MGSize StepEntity::ReadMtxFloat( vector< vector<MGFloat> >& mtxres, const MGString& sparams)
{
	vector<MGFloat>	tab;

	MGString	buf;
	RegExp		rex;


	rex.InitPattern( "^\\(([^\\(\\)]*)\\)(.*)$");
	rex.SetString( sparams);
	while ( rex.IsOk() )
	{
		buf = rex.GetSubString(2);

		tab.clear();
		ReadTabFloat( tab, rex.GetSubString( 1) );
		mtxres.push_back( tab);


		if ( buf.size() == 0 )
			return mtxres.size();

		rex.InitPattern( "^,\\(([^\\(\\)]*)\\)(.*)$");
		rex.SetString( buf);
	}
	
	return 0;
}


MGSize StepEntity::ReadTabFloat( vector<MGFloat>& tabres, const MGString& sparams)
{
	MGFloat		x;
	RegExp		rex;
	MGString	buf;

	istringstream is;

	
	rex.InitPattern( "^([0-9\\+\\.eE-]*)(.*)$");
	rex.SetString( sparams);
	while ( rex.IsOk() )
	{
		buf = rex.GetSubString(2);

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> x;

		tabres.push_back( x);	


		if ( buf.size() == 0 )
			return tabres.size();

		rex.InitPattern( "^,([0-9\\+\\.eE-]*)(.*)$");
		rex.SetString( buf);
	}

	return 0;
}


MGSize StepEntity::ReadTabUInt( vector<MGSize>& tabres, const MGString& sparams)
{
	MGSize		id;
	RegExp		rex;
	MGString	buf;

	istringstream is;

	
	rex.InitPattern( "^([0-9]*)(.*)$");
	rex.SetString( sparams);
	while ( rex.IsOk() )
	{
		buf = rex.GetSubString(2);

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> id;

		tabres.push_back( id);	


		if ( buf.size() == 0 )
			return tabres.size();

		rex.InitPattern( "^,([0-9]*)(.*)$");
		rex.SetString( buf);
	}

	return 0;
}


MGSize StepEntity::ReadTabId( vector<MGSize>& tabres, const MGString& sparams)
{
	MGSize		id;
	RegExp		rex;
	MGString	buf;

	istringstream is;

	
	rex.InitPattern( "^#([0-9]*)(.*)$");
	rex.SetString( sparams);
	while ( rex.IsOk() )
	{
		buf = rex.GetSubString(2);

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> id;

		tabres.push_back( id);	


		if ( buf.size() == 0 )
			return tabres.size();

		rex.InitPattern( "^,#([0-9]*)(.*)$");
		rex.SetString( buf);
	}

	return 0;
}


StepFlag StepEntity::ReadFlag( const MGString& sparams)
{
	if ( sparams == STP_FLAG_TRUE)
		return FLAG_TRUE;
	else if ( sparams == STP_FLAG_FALSE )
		return FLAG_FALSE;
	else if ( sparams == STP_FLAG_UNSPECIFIED )
		return FLAG_UNSPECIFIED;
	else if ( sparams == STP_FLAG_U )	// TODO :: check !!!
		return FLAG_UNSPECIFIED;
	else 
	{
		ostringstream os;
		os << "unrecognized flag " << "  params = '" << sparams << "'";
		THROW_INTERNAL( os.str() );
	}

	return FLAG_UNSPECIFIED;
}


StepFlag_BSplineCurveForm StepEntity::ReadFlagBSplineCurveForm( const MGString& sparams)
{
	if ( sparams == STP_FLAG_POLYLINE_FORM)
		return FLAG_BSCF_POLYLINE_FORM;
	else if ( sparams == STP_FLAG_CIRCULAR_ARC )
		return FLAG_BSCF_CIRCULAR_ARC;
	else if ( sparams == STP_FLAG_ELLIPTIC_ARC )
		return FLAG_BSCF_ELLIPTIC_ARC;
	else if ( sparams == STP_FLAG_PARABOLIC_ARC )
		return FLAG_BSCF_PARABOLIC_ARC;
	else if ( sparams == STP_FLAG_HYPERBOLIC_ARC )
		return FLAG_BSCF_HYPERBOLIC_ARC;
	else if ( sparams == STP_FLAG_UNSPECIFIED )
		return FLAG_BSCF_UNSPECIFIED;
	else 
	{
		ostringstream os;
		os << "unrecognized flag BSplineCurveForm " << "  params = '" << sparams << "'";
		THROW_INTERNAL( os.str() );
	}

	return FLAG_BSCF_UNSPECIFIED;
}


StepFlag_BSplineSurfaceForm StepEntity::ReadFlagBSplineSurfaceForm( const MGString& sparams)
{
	if ( sparams == STP_FLAG_SURF_OF_REVOLUTION )
		return FLAG_BSSF_SURF_OF_REVOLUTION;
	else if ( sparams == STP_FLAG_UNSPECIFIED )
		return FLAG_BSSF_UNSPECIFIED;
	else 
	{
		ostringstream os;
		os << "unrecognized flag BSplineCurveForm " << "  params = '" << sparams << "'";
		THROW_INTERNAL( os.str() );
	}

	return FLAG_BSSF_UNSPECIFIED;
}



StepFlag_KnotType StepEntity::ReadFlagKnotType( const MGString& sparams)
{
	if ( sparams == STP_FLAG_UNIFORM_KNOTS)
		return FLAG_KNTP_UNIFORM_KNOTS;
	else if ( sparams == STP_FLAG_QUASI_UNIFORM_KNOTS )
		return FLAG_KNTP_QUASI_UNIFORM_KNOTS;
	else if ( sparams == STP_FLAG_PIECEWISE_BEZIER_KNOTS )
		return FLAG_KNTP_PIECEWISE_BEZIER_KNOTS;
	else if ( sparams == STP_FLAG_UNSPECIFIED )
		return FLAG_KNTP_UNSPECIFIED;
	else 
	{
		ostringstream os;
		os << "unrecognized flag KnotType" << "  params = '" << sparams << "'";
		THROW_INTERNAL( os.str() );
	}

	return FLAG_KNTP_UNSPECIFIED;
}


//////////////////////////////////////////////////////////////////////
// class StepRepresentationItem
//////////////////////////////////////////////////////////////////////
void StepRepresentationItem::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	RegExp	rex;
	rex.InitPattern( "^'(.*)'(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		mStepId = id;

		mText = rex.GetSubString( 1);

		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StepSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
