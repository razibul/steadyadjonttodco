#ifndef __STEPMASTER_H__
#define __STEPMASTER_H__

#include "libcoresystem/mgdecl.h"
#include "libextstep/stepconst.h"
#include "libextstep/stepdef.h"

#include "libcorecommon/regexp.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace StepSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


class StepFactory;


//////////////////////////////////////////////////////////////////////
// class StepMaster
//////////////////////////////////////////////////////////////////////
class StepMaster
{
public:
	typedef map<MGSize,StepDef>		StepMap;

	StepMaster();
	~StepMaster();

	void	Read( const MGString& name);

	void	RemoveUnreferencedObj( const MGSize& id=0);
	void	CreateStepObjects();
	void	WriteDotGraph( const MGString& name, const MGString& stepname="", const MGString& type=STP_TOPOLOGIC_REPRESENTATION_ITEM);

	const StepMap&		cStepMap() const		{ return mMap;}
	const MGString&		cName() const			{ return mName;}
	const MGString&		cDescription() const	{ return mDescription;}

protected:
	void	ReadHeader( istream& file, const MGString& name="");
	void	ReadData( istream& file, const MGString& name="");
	void	AddToMap( const MGString& sbuf);

	bool	ReadLine( MGString& buf, istream& file);	// TODO :: should be replaced by Store::ReadLineC(istream&, MGString&)

private:
	RegExp		mRex;

	MGString	mName;
	MGString	mDescription;

	StepMap		mMap;

	vector<StepFactory*>	mtabFactory;
};
//////////////////////////////////////////////////////////////////////

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StepSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Step = StepSpace;

#endif // __STEPMASTER_H__
