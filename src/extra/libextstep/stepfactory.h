#ifndef __STEPFACTORY_H__
#define __STEPFACTORY_H__

#include "libcoresystem/mgdecl.h"
#include "libextstep/stepconst.h"
#include "libextstep/stepentity.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace StepSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class StepFactory
//////////////////////////////////////////////////////////////////////
class StepFactory
{
public:
	StepFactory()				{}
	virtual		~StepFactory()	{}

	virtual MGString		ObjectName() = 0;
	virtual StepEntity*		Create( const MGSize& id, const MGString& sname, const MGString& sparams)	= 0;

};


//////////////////////////////////////////////////////////////////////
// class Factory
//////////////////////////////////////////////////////////////////////
template <class T>
class Factory : public StepFactory
{
public:
	virtual MGString		ObjectName()
	{
		return MGString( T::ClassName() );
	}

	virtual StepEntity*		Create( const MGSize& id, const MGString& sname, const MGString& sparams)
	{
		try
		{
			if ( sname != T::ClassName() )
				return NULL;

			T* ptr = new T;
			MGString sbuf = sparams;
			ptr->Init( id, sname, sbuf);
			if ( sbuf.length() > 0 )
				THROW_INTERNAL( "not every argument has been interpreted" );

			return ptr;
		}
		catch ( EHandler::Except&)
		{
			cout << endl;
			cout << T::ClassName() << " : id = " << id << "  params = \"" << sparams << "\"" << endl;

			throw;
		}

		return NULL;

	}
};


//////////////////////////////////////////////////////////////////////
// class Factory
//////////////////////////////////////////////////////////////////////
template <>
class Factory<StepUnknownObject> : public StepFactory
{
public:
	virtual MGString		ObjectName()
	{
		return MGString( StepUnknownObject::ClassName() );
	}

	virtual StepEntity*		Create( const MGSize& id, const MGString& sname, const MGString& sparams);

private:
	MGInt		GetId( const MGString& sname, const vector< pair<MGString,MGString> >& tab);

	//RATIONAL_B_SPLINE_CURVE
	StepEntity*	CreateRationalBSplineCurve( const MGSize& id, const vector< pair<MGString,MGString> >& tab);

	//RATIONAL_B_SPLINE_CURVE
	StepEntity*	CreateRationalBSplineCurve_QuasiUniform( const MGSize& id, const vector< pair<MGString,MGString> >& tab);


	//RATIONAL_B_SPLINE_SURFACE
	StepEntity*	CreateRationalBSplineSurface( const MGSize& id, const vector< pair<MGString,MGString> >& tab);

	//RATIONAL_B_SPLINE_SURFACE
	StepEntity*	CreateRationalBSplineSurface_QuasiUniform( const MGSize& id, const vector< pair<MGString,MGString> >& tab);


};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StepSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Step = StepSpace;


#endif // __STEPFACTORY_H__
