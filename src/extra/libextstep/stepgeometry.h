#ifndef __STEPGEOMETRY_H__
#define __STEPGEOMETRY_H__

#include "libcoresystem/mgdecl.h"
#include "libextstep/stepconst.h"
#include "libextstep/stepentity.h"
#include "libcoregeom/vect.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace StepSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

using namespace Geom;


//////////////////////////////////////////////////////////////////////
// class StepGeometricRepresentationItem
//////////////////////////////////////////////////////////////////////
class StepGeometricRepresentationItem : public StepRepresentationItem
{
public:
	StepGeometricRepresentationItem()		{}

 	static const char*		ClassName()		{ return STP_GEOMETRIC_REPRESENTATION_ITEM;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);
};


inline void StepGeometricRepresentationItem::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepRepresentationItem::GetDotDepend(tab);
}

inline bool StepGeometricRepresentationItem::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepRepresentationItem::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepDirection
//////////////////////////////////////////////////////////////////////
class StepDirection : public StepGeometricRepresentationItem
{
public:
	StepDirection() : mVect(0.,0.,0.)	{}

	static const char*		ClassName()		{ return STP_DIRECTION;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const Vect3D&	cVect() const	{ return mVect;}

private:
	Vect3D	mVect;
};


inline void StepDirection::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepGeometricRepresentationItem::GetDotDepend(tab);
}

inline bool StepDirection::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepGeometricRepresentationItem::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepVector
//////////////////////////////////////////////////////////////////////
class StepVector : public StepGeometricRepresentationItem
{
public:
	StepVector() : mDirId(0), mLength(0.)	{}

	static const char*		ClassName()		{ return STP_VECTOR;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	
		
	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cDirId() const		{ return mDirId;}
	const MGFloat&	cLength() const		{ return mLength;}

private:
	MGSize	mDirId;		// step id of the direction of the vector
	MGFloat	mLength;	// length of the vector
};

inline void StepVector::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepGeometricRepresentationItem::GetDotDepend(tab);
	tab.push_back( mDirId);
}

inline bool StepVector::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepGeometricRepresentationItem::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepAxis1Placement
//////////////////////////////////////////////////////////////////////
class StepAxis1Placement : public StepGeometricRepresentationItem
{
public:
	StepAxis1Placement()	{mIdPoint=mIdAxis=0;}

	static const char*		ClassName()		{ return STP_AXIS1_PLACEMENT;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdPoint() const		{ return mIdPoint;}
	const MGSize&	cIdAxis() const			{ return mIdAxis;}

private:
	MGSize	mIdPoint;	// location
	MGSize	mIdAxis;	// axis  
};


inline void StepAxis1Placement::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepGeometricRepresentationItem::GetDotDepend(tab);
	tab.push_back( mIdPoint);
	tab.push_back( mIdAxis);
}


inline bool StepAxis1Placement::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepGeometricRepresentationItem::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepAxis2Placement3D
//////////////////////////////////////////////////////////////////////
class StepAxis2Placement3D : public StepGeometricRepresentationItem
{
public:
	StepAxis2Placement3D()	{mIdPoint=mIdAxis=mIdRefDir=0;}

	static const char*		ClassName()		{ return STP_AXIS2_PLACEMENT_3D;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdPoint() const		{ return mIdPoint;}
	const MGSize&	cIdAxis() const			{ return mIdAxis;}
	const MGSize&	cIdRefDir() const		{ return mIdRefDir;}

private:
	MGSize	mIdPoint;	// location  
	MGSize	mIdAxis;	// axis  
	MGSize	mIdRefDir;	// ref_direction
};


inline void StepAxis2Placement3D::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepGeometricRepresentationItem::GetDotDepend(tab);
	tab.push_back( mIdPoint);

	if ( mIdAxis )
		tab.push_back( mIdAxis);

	if ( mIdRefDir )
		tab.push_back( mIdRefDir);
}


inline bool StepAxis2Placement3D::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepGeometricRepresentationItem::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepPoint
//////////////////////////////////////////////////////////////////////
class StepPoint : public StepGeometricRepresentationItem
{
public:
	StepPoint()		{}

	static const char*		ClassName()		{ return STP_POINT;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);
};


inline void StepPoint::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepGeometricRepresentationItem::GetDotDepend(tab);
}

inline bool StepPoint::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepGeometricRepresentationItem::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepCartesianPoint
//////////////////////////////////////////////////////////////////////
class StepCartesianPoint : public StepPoint
{
public:
	StepCartesianPoint() : mVect(0.,0.,0.)	{}

	static const char*		ClassName()		{ return STP_CARTESIAN_POINT;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);


	const Vect3D&	cVect() const	{ return mVect;}

private:
	Vect3D	mVect;
};



inline void StepCartesianPoint::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepPoint::GetDotDepend(tab);
}

inline bool StepCartesianPoint::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepPoint::IsType( type);
}






//////////////////////////////////////////////////////////////////////
// class StepCurve
//////////////////////////////////////////////////////////////////////
class StepCurve : public StepGeometricRepresentationItem
{
public:
	StepCurve()		{}

	static const char*		ClassName()		{ return STP_CURVE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);
};


inline void StepCurve::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepGeometricRepresentationItem::GetDotDepend(tab);
}

inline bool StepCurve::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepGeometricRepresentationItem::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepCurve
//////////////////////////////////////////////////////////////////////
class StepPCurve : public StepCurve
{
public:
	StepPCurve()		{}

	static const char*		ClassName()		{ return STP_PCURVE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);
};


inline void StepPCurve::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepCurve::GetDotDepend(tab);
}

inline bool StepPCurve::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepCurve::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepBoundedCurve
//////////////////////////////////////////////////////////////////////
class StepBoundedCurve : public StepCurve
{
public:
	StepBoundedCurve()		{}

	static const char*		ClassName()		{ return STP_BOUNDED_CURVE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);
};


inline void StepBoundedCurve::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepCurve::GetDotDepend(tab);
}

inline bool StepBoundedCurve::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepCurve::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepLine
//////////////////////////////////////////////////////////////////////
class StepLine : public StepCurve
{
public:
	StepLine() : mIdPoint(0), mIdVect(0)	{}

	static const char*		ClassName()		{ return STP_LINE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdPoint() const		{ return mIdPoint;}
	const MGSize&	cIdVect() const			{ return mIdVect;}

private:
	MGSize	mIdPoint;
	MGSize	mIdVect;
};


inline void StepLine::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepCurve::GetDotDepend(tab);
	tab.push_back( mIdPoint);
	tab.push_back( mIdVect);
}

inline bool StepLine::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepCurve::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepCircle
//////////////////////////////////////////////////////////////////////
class StepCircle : public StepCurve
{
public:
	StepCircle() : mIdAxis(0), mRadius(0.)	{}

	static const char*		ClassName()		{ return STP_CIRCLE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdAxis() const			{ return mIdAxis;}
	const MGFloat&	cRadius() const			{ return mRadius;}

private:
	MGSize	mIdAxis;
	MGFloat	mRadius;
};


inline void StepCircle::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepCurve::GetDotDepend(tab);
	tab.push_back( mIdAxis);
}

inline bool StepCircle::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepCurve::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepEllipse
//////////////////////////////////////////////////////////////////////
class StepEllipse : public StepCurve
{
public:
	StepEllipse() : mIdAxis(0), mSemiAxis1(0.), mSemiAxis2(0.)	{}

	static const char*		ClassName()		{ return STP_ELLIPSE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdAxis() const			{ return mIdAxis;}
	const MGFloat&	cSemiAxis1() const		{ return mSemiAxis1;}
	const MGFloat&	cSemiAxis2() const		{ return mSemiAxis2;}

private:
	MGSize	mIdAxis;
	MGFloat	mSemiAxis1;
	MGFloat	mSemiAxis2;
};


inline void StepEllipse::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepCurve::GetDotDepend(tab);
	tab.push_back( mIdAxis);
}

inline bool StepEllipse::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepCurve::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepBSplineCurve
//////////////////////////////////////////////////////////////////////
class StepBSplineCurve : public StepBoundedCurve
{
public:
	StepBSplineCurve() : mDeg(0)			{}

	static const char*		ClassName()		{ return STP_B_SPLINE_CURVE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&			cDeg() const		{ return mDeg;}
	const StepFlag&			cIsClosed() const	{ return mFlag2; }
	const vector<MGSize>&	cTabPntId() const	{ return mtabIdPoint;}


private:
	MGSize						mDeg;			// degree
	vector<MGSize>				mtabIdPoint;	// control points
	StepFlag_BSplineCurveForm	mFlag1;			// curve form ( POLYLINE_FORM, CIRCULAR_ARC, ELLIPTIC_ARC, PARABOLIC_ARC, HYPERBOLIC_ARC, UNSPECIFIED )
	StepFlag					mFlag2;			// is closed
	StepFlag					mFlag3;			// is selfintersecting
};


inline void StepBSplineCurve::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepBoundedCurve::GetDotDepend(tab);
	for ( vector<MGSize>::const_iterator itr=mtabIdPoint.begin(); itr!=mtabIdPoint.end(); ++itr)
		tab.push_back( *itr);
}

inline bool StepBSplineCurve::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepBoundedCurve::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepBSplineCurveWithKnots
//////////////////////////////////////////////////////////////////////
class StepBSplineCurveWithKnots : public StepBSplineCurve
{
public:
	StepBSplineCurveWithKnots()				{}

	static const char*		ClassName()		{ return STP_B_SPLINE_CURVE_WITH_KNOTS;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const vector<MGSize>&	cTabMult() const	{ return mtabMult;}
	const vector<MGFloat>&	cTabKnot() const	{ return mtabKnot;}

private:
	vector<MGSize>		mtabMult;	// knot multiplicities
	vector<MGFloat>		mtabKnot;	// knots
	StepFlag_KnotType	mFlag;		// knot type ( UNIFORM_KNOTS, UNSPECIFIED, QUASI_UNIFORM_KNOTS, PIECEWISE_BEZIER_KNOTS)
};


inline void StepBSplineCurveWithKnots::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepBSplineCurve::GetDotDepend(tab);
}

inline bool StepBSplineCurveWithKnots::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepBSplineCurve::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepBSplineCurveWithKnots
//////////////////////////////////////////////////////////////////////
class StepQuasiUniformCurve : public StepBSplineCurveWithKnots
{
public:
	StepQuasiUniformCurve()				{}

	static const char*		ClassName()		{ return STP_QUASI_UNIFORM_CURVE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

private:
};


inline void StepQuasiUniformCurve::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepBSplineCurveWithKnots::GetDotDepend(tab);
}

inline bool StepQuasiUniformCurve::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepBSplineCurveWithKnots::IsType( type);
}




//////////////////////////////////////////////////////////////////////
// class StepRationalBSplineCurve
//////////////////////////////////////////////////////////////////////
class StepRationalBSplineCurve : public StepBSplineCurveWithKnots
{
public:
	StepRationalBSplineCurve()				{}

	static const char*		ClassName()		{ return STP_RATIONAL_B_SPLINE_CURVE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const vector<MGFloat>&	cTabWght() const	{ return mtabWght;}

private:
	vector<MGFloat>	mtabWght;
};


inline void StepRationalBSplineCurve::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepBSplineCurveWithKnots::GetDotDepend(tab);
}

inline bool StepRationalBSplineCurve::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepBSplineCurveWithKnots::IsType( type);
}




//////////////////////////////////////////////////////////////////////
// class StepSurface
//////////////////////////////////////////////////////////////////////
class StepSurface : public StepGeometricRepresentationItem
{
public:
	StepSurface()		{}

	static const char*		ClassName()		{ return STP_SURFACE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);
};



inline void StepSurface::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepGeometricRepresentationItem::GetDotDepend(tab);
}

inline bool StepSurface::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepGeometricRepresentationItem::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepBoundedSurface
//////////////////////////////////////////////////////////////////////
class StepBoundedSurface : public StepSurface
{
public:
	StepBoundedSurface()		{}

	static const char*		ClassName()		{ return STP_BOUNDED_SURFACE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);
};


inline void StepBoundedSurface::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepSurface::GetDotDepend(tab);
}

inline bool StepBoundedSurface::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepSurface::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepPlane
//////////////////////////////////////////////////////////////////////
class StepPlane : public StepSurface
{
public:
	StepPlane() : mIdAxis(0)	{}

	static const char*		ClassName()		{ return STP_PLANE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdAxis() const		{ return mIdAxis;}

private:
	MGSize	mIdAxis;
};

inline void StepPlane::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepSurface::GetDotDepend(tab);
	tab.push_back( mIdAxis);
}

inline bool StepPlane::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepSurface::IsType( type);
}




//////////////////////////////////////////////////////////////////////
// class StepCylindricalSurface
//////////////////////////////////////////////////////////////////////
class StepCylindricalSurface : public StepSurface
{
public:
	StepCylindricalSurface() : mIdAxis(0),mRadius(0.)	{}

	static const char*		ClassName()		{ return STP_CYLINDRICAL_SURFACE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdAxis() const			{ return mIdAxis;}
	const MGFloat&	cRadius() const			{ return mRadius;}

private:
	MGSize	mIdAxis;
	MGFloat	mRadius;
};

inline void StepCylindricalSurface::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepSurface::GetDotDepend(tab);
	tab.push_back( mIdAxis);
}

inline bool StepCylindricalSurface::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepSurface::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepConicalSurface
//////////////////////////////////////////////////////////////////////
class StepConicalSurface : public StepSurface
{
public:
	StepConicalSurface() : mIdAxis(0),mRadius(0.),mAngle(0.)	{}

	static const char*		ClassName()		{ return STP_CONICAL_SURFACE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdAxis() const			{ return mIdAxis;}
	const MGFloat&	cRadius() const			{ return mRadius;}
	const MGFloat&	cAngle() const			{ return mAngle;}

private:
	MGSize	mIdAxis;	// ref. plane
	MGFloat	mRadius;	// radius of the circle at the ref. plane
	MGFloat	mAngle;		// cone half angle
};

inline void StepConicalSurface::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepSurface::GetDotDepend(tab);
	tab.push_back( mIdAxis);
}

inline bool StepConicalSurface::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepSurface::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepSphericalSurface
//////////////////////////////////////////////////////////////////////
class StepSphericalSurface : public StepSurface
{
public:
	StepSphericalSurface() : mIdAxis(0),mRadius(0.)	{}

	static const char*		ClassName()		{ return STP_SPHERICAL_SURFACE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdAxis() const			{ return mIdAxis;}
	const MGFloat&	cRadius() const			{ return mRadius;}

private:
	MGSize	mIdAxis;	// ref. plane
	MGFloat	mRadius;	// radius of the sphere
};

inline void StepSphericalSurface::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepSurface::GetDotDepend(tab);
	tab.push_back( mIdAxis);
}

inline bool StepSphericalSurface::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepSurface::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepToroidalSurface
//////////////////////////////////////////////////////////////////////
class StepToroidalSurface : public StepSurface
{
public:
	StepToroidalSurface() : mIdAxis(0),mRadius1(0.),mRadius2(0.)	{}

	static const char*		ClassName()		{ return STP_TOROIDAL_SURFACE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdAxis() const			{ return mIdAxis;}
	const MGFloat&	cRadius1() const		{ return mRadius1;}
	const MGFloat&	cRadius2() const		{ return mRadius2;}

private:
	MGSize	mIdAxis;	// ref. plane
	MGFloat	mRadius1;	// major radius
	MGFloat	mRadius2;	// minor radius
};

inline void StepToroidalSurface::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepSurface::GetDotDepend(tab);
	tab.push_back( mIdAxis);
}

inline bool StepToroidalSurface::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepSurface::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepSurfaceOfRevolution
//////////////////////////////////////////////////////////////////////
class StepSurfaceOfRevolution : public StepSurface
{
public:
	StepSurfaceOfRevolution() : mIdAxis(0), mIdCurve(0)		{}

	static const char*		ClassName()		{ return STP_SURFACE_OF_REVOLUTION;}

	virtual MGString	GetStepName() const					{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdAxis() const		{ return mIdAxis;}
	const MGSize&	cIdCurve() const	{ return mIdCurve;}

private:
	MGSize	mIdAxis;
	MGSize	mIdCurve;
};

inline void StepSurfaceOfRevolution::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepSurface::GetDotDepend(tab);
	tab.push_back( mIdAxis);
	tab.push_back( mIdCurve);
}

inline bool StepSurfaceOfRevolution::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepSurface::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepSurfaceOfLinearExtrusion
//////////////////////////////////////////////////////////////////////
class StepSurfaceOfLinearExtrusion : public StepSurface
{
public:
	StepSurfaceOfLinearExtrusion() : mIdCurve(0),mIdVect(0)		{}

	static const char*		ClassName()		{ return STP_SURFACE_OF_LINEAR_EXTRUSION;}

	virtual MGString	GetStepName() const					{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdVect() const		{ return mIdVect;}
	const MGSize&	cIdCurve() const	{ return mIdCurve;}

private:
	MGSize	mIdCurve;
	MGSize	mIdVect;
};

inline void StepSurfaceOfLinearExtrusion::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepSurface::GetDotDepend(tab);
	tab.push_back( mIdVect);
	tab.push_back( mIdCurve);
}

inline bool StepSurfaceOfLinearExtrusion::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepSurface::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepBSplineSurface
//////////////////////////////////////////////////////////////////////
class StepBSplineSurface : public StepBoundedSurface
{
public:
	StepBSplineSurface() : mDegU(0), mDegV(0)			{}

	static const char*		ClassName()		{ return STP_B_SPLINE_SURFACE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&					cDegU() const		{ return mDegU;}
	const MGSize&					cDegV() const		{ return mDegV;}

	const StepFlag&			cIsClosedU() const			{ return mFlagCU; }
	const StepFlag&			cIsClosedV() const			{ return mFlagCV; }

	const vector< vector<MGSize> >&	cMtxPntId() const	{ return mmtxIdPoint;}

private:
	MGSize						mDegU;			// U degree
	MGSize						mDegV;			// V degree
	StepFlag_BSplineSurfaceForm	mFlag1;			// surface form ( PLANE_SURF, CYLINDRICAL_SURF, CONICAL_SURF, SPHERICAL_SURF, TOROIDAL_SURF, SURF_OF_REVOLUTION,RULED_SURF, GENERALISED_CONE, QUADRIC_SURF, SURF_OF_LINEAR_EXTRUSION, UNSPECIFIED)
	StepFlag					mFlagCU;		// curve is closed U (?)
	StepFlag					mFlagCV;		// curve is closed V (?)
	StepFlag					mFlag3;			// is self intersecting

	vector< vector<MGSize> >	mmtxIdPoint;
};


inline void StepBSplineSurface::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepBoundedSurface::GetDotDepend(tab);
	for ( MGSize i=0; i<mmtxIdPoint.size(); ++i)
		for ( MGSize k=0; k<mmtxIdPoint[i].size(); ++k)
            tab.push_back( mmtxIdPoint[i][k]);
}

inline bool StepBSplineSurface::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepBoundedSurface::IsType( type);
}




//////////////////////////////////////////////////////////////////////
// class StepBSplineSurfaceWithKnots
//////////////////////////////////////////////////////////////////////
class StepBSplineSurfaceWithKnots : public StepBSplineSurface
{
public:
	StepBSplineSurfaceWithKnots()			{}

	static const char*		ClassName()		{ return STP_B_SPLINE_SURFACE_WITH_KNOTS;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);


	const vector<MGSize>&	cTabMultU() const	{ return mtabMultU;}
	const vector<MGFloat>&	cTabKnotU() const	{ return mtabKnotU;}

	const vector<MGSize>&	cTabMultV() const	{ return mtabMultV;}
	const vector<MGFloat>&	cTabKnotV() const	{ return mtabKnotV;}


private:
	vector<MGSize>	mtabMultU;	// U knot multiplicities
	vector<MGSize>	mtabMultV;	// V knot multiplicities

	vector<MGFloat>	mtabKnotU;	// U knots
	vector<MGFloat>	mtabKnotV;	// V knots

	StepFlag_KnotType	mFlag;		// knot type
};


inline void StepBSplineSurfaceWithKnots::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepBSplineSurface::GetDotDepend(tab);
}

inline bool StepBSplineSurfaceWithKnots::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepBSplineSurface::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepQuasiUnformSurface
//////////////////////////////////////////////////////////////////////
class StepQuasiUniformSurface : public StepBSplineSurfaceWithKnots
{
public:
	StepQuasiUniformSurface()				{}

	static const char*		ClassName()		{ return STP_QUASI_UNIFORM_SURFACE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

private:
};


inline void StepQuasiUniformSurface::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepBSplineSurfaceWithKnots::GetDotDepend(tab);
}

inline bool StepQuasiUniformSurface::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepBSplineSurfaceWithKnots::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepRationalBSplineSurface
//////////////////////////////////////////////////////////////////////
class StepRationalBSplineSurface : public StepBSplineSurfaceWithKnots
{
public:
	StepRationalBSplineSurface()				{}

	static const char*		ClassName()			{ return STP_RATIONAL_B_SPLINE_SURFACE;}

	virtual MGString	GetStepName() const		{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const vector< vector<MGFloat> >&	cMtxWght() const	{ return mmtxWght;}


private:
	vector< vector<MGFloat> >	mmtxWght;
};


inline void StepRationalBSplineSurface::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepBSplineSurfaceWithKnots::GetDotDepend(tab);
}

inline bool StepRationalBSplineSurface::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepBSplineSurfaceWithKnots::IsType( type);
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StepSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Step = StepSpace;



#endif // __STEPGEOMETRY_H__
