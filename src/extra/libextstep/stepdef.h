#ifndef __STEPDEF_H__
#define __STEPDEF_H__

#include "libcoresystem/mgdecl.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace StepSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

class StepEntity;

//////////////////////////////////////////////////////////////////////
// class StepDef
//////////////////////////////////////////////////////////////////////
class StepDef
{
	friend class StepMaster;

public:
	StepDef( const MGSize& id, const MGString& name, const MGString& params) 
		: mId(id), mName(name), mParams(params), mbFlag(false), mpObj(NULL)			{}

	~StepDef();


	const MGSize&		cId() const			{ return mId;}
	const MGString&		cName() const		{ return mName;}
	const MGString&		cParams() const		{ return mParams;}
	const StepEntity*	cpObject() const	{ return mpObj;}
	const bool&			cFlag() const		{ return mbFlag;}

	bool&				rFlag()				{ return mbFlag;}

protected:
	void	SetObjectPtr( StepEntity* ptr)		{ mpObj = ptr;}

private:
	MGSize		mId;
	MGString	mName;
	MGString	mParams;
	bool		mbFlag;
	StepEntity*	mpObj;
};
//////////////////////////////////////////////////////////////////////

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StepSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Step = StepSpace;

#endif // __STEPDEF_H__
