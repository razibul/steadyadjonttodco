#include "steptopology.h"
#include "libcorecommon/regexp.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace StepSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//



//////////////////////////////////////////////////////////////////////
// class StepTopologicRepresentationItem
//////////////////////////////////////////////////////////////////////
void StepTopologicRepresentationItem::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepRepresentationItem::Init( id, sname, sparams);
}

//////////////////////////////////////////////////////////////////////
// class StepVertexPoint
//////////////////////////////////////////////////////////////////////
void StepVertexPoint::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepTopologicRepresentationItem::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdPoint;

		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );
}


//////////////////////////////////////////////////////////////////////
// class StepVertexLoop
//////////////////////////////////////////////////////////////////////
void StepVertexLoop::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepTopologicRepresentationItem::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdVertex;

		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}



//////////////////////////////////////////////////////////////////////
// class StepEdgeCurve
//////////////////////////////////////////////////////////////////////
void StepEdgeCurve::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepTopologicRepresentationItem::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),#([0-9]*),#([0-9]*),([\\.A-Z]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdVertex1;

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mIdVertex2;

		is.clear();
		is.str( rex.GetSubString( 3) );
		is >> mIdCurve;

		mflOrient = ReadFlag( rex.GetSubString( 4) );

		sparams = rex.GetSubString( 5);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepOrientedEdge
//////////////////////////////////////////////////////////////////////
void StepOrientedEdge::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepTopologicRepresentationItem::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^\\*,\\*,#([0-9]*),([\\.A-Z]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdEdge;

		mflOrient = ReadFlag( rex.GetSubString( 2) );

		sparams = rex.GetSubString( 3);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

//////////////////////////////////////////////////////////////////////
// class StepEdgeLoop
//////////////////////////////////////////////////////////////////////
void StepEdgeLoop::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepTopologicRepresentationItem::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		ReadTabId( mtabIdEdge, rex.GetSubString( 1) );
		if ( mtabIdEdge.size() == 0)
			THROW_INTERNAL( "empty list of edges found" );

		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

//////////////////////////////////////////////////////////////////////
// class StepFaceBound
//////////////////////////////////////////////////////////////////////
void StepFaceBound::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepTopologicRepresentationItem::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),([\\.A-Z]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdLoop;

		mflOrient = ReadFlag( rex.GetSubString( 2) );

		sparams = rex.GetSubString( 3);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepFaceOuterBound
//////////////////////////////////////////////////////////////////////
void StepFaceOuterBound::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepFaceBound::Init( id, sname, sparams);
}


//////////////////////////////////////////////////////////////////////
// class StepAdvancedFace
//////////////////////////////////////////////////////////////////////
void StepAdvancedFace::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepTopologicRepresentationItem::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\),#([0-9]*),([\\.A-Z]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		ReadTabId( mtabIdBound, rex.GetSubString( 1) );
		if ( mtabIdBound.size() == 0)
			THROW_INTERNAL( "empty list of facebounds found" );

		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mIdSurface;

		mflOrient = ReadFlag( rex.GetSubString( 3) );

		sparams = rex.GetSubString( 4);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepOpenShell
//////////////////////////////////////////////////////////////////////
void StepOpenShell::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepTopologicRepresentationItem::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		ReadTabId( mtabIdFace, rex.GetSubString( 1) );
		if ( mtabIdFace.size() == 0)
			THROW_INTERNAL( "empty list of edges found" );

		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}



//////////////////////////////////////////////////////////////////////
// class StepClosedShell
//////////////////////////////////////////////////////////////////////
void StepClosedShell::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepOpenShell::Init( id, sname, sparams);
}



//////////////////////////////////////////////////////////////////////
// class StepOrientedClosedShell
//////////////////////////////////////////////////////////////////////
void StepOrientedClosedShell::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepTopologicRepresentationItem::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\)(.*)$" );
	rex.InitPattern( "^\\*,#([0-9]*),([\\.A-Z]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdCShell;

		mflOrient = ReadFlag( rex.GetSubString( 2) );


		sparams = rex.GetSubString( 3);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepManifoldSolidBRep
//////////////////////////////////////////////////////////////////////
void StepManifoldSolidBRep::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepTopologicRepresentationItem::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdCShell;

		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepBRepWithVoids
//////////////////////////////////////////////////////////////////////
void StepBRepWithVoids::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepManifoldSolidBRep::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		ReadTabId( mtabIdShell, rex.GetSubString( 1) );
		if ( mtabIdShell.size() == 0)
			THROW_INTERNAL( "empty list of shells found" );

		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepShellBasedSurfaceModel
//////////////////////////////////////////////////////////////////////
void StepShellBasedSurfaceModel::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepTopologicRepresentationItem::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		ReadTabId( mtabIdShell, rex.GetSubString( 1) );
		if ( mtabIdShell.size() == 0)
			THROW_INTERNAL( "empty list of shells found" );

		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StepSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

