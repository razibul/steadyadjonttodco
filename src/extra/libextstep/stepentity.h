#ifndef __STEPENTITY_H__
#define __STEPENTITY_H__

#include "libcoresystem/mgdecl.h"
#include "libextstep/stepconst.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace StepSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

enum StepFlag
{
	FLAG_FALSE = 0,
	FLAG_TRUE = 1,
	FLAG_UNSPECIFIED = 999
};

enum StepFlag_KnotType
{
	FLAG_KNTP_UNIFORM_KNOTS,
	FLAG_KNTP_QUASI_UNIFORM_KNOTS,
	FLAG_KNTP_PIECEWISE_BEZIER_KNOTS,
	FLAG_KNTP_UNSPECIFIED = 999
};


enum StepFlag_BSplineCurveForm
{
	FLAG_BSCF_POLYLINE_FORM,
	FLAG_BSCF_CIRCULAR_ARC,
	FLAG_BSCF_ELLIPTIC_ARC,
	FLAG_BSCF_PARABOLIC_ARC,
	FLAG_BSCF_HYPERBOLIC_ARC,
	FLAG_BSCF_UNSPECIFIED = 999
};

enum StepFlag_BSplineSurfaceForm
{
	FLAG_BSSF_PLANE_SURF,
	FLAG_BSSF_CYLINDRICAL_SURF,
	FLAG_BSSF_CONICAL_SURF,
	FLAG_BSSF_SPHERICAL_SURF,
	FLAG_BSSF_TOROIDAL_SURF,
	FLAG_BSSF_SURF_OF_REVOLUTION,
	FLAG_BSSF_RULED_SURF,
	FLAG_BSSF_GENERALISED_CONE,
	FLAG_BSSF_QUADRIC_SURF,
	FLAG_BSSF_SURF_OF_LINEAR_EXTRUSION,
	FLAG_BSSF_UNSPECIFIED = 999
};


//////////////////////////////////////////////////////////////////////
// class StepEntity
//////////////////////////////////////////////////////////////////////
class StepEntity
{
public:
	StepEntity()		{}

	// sparams string is modified during initialization (used def is removed)
	//virtual void	Init( const MGSize& id, const MGString& sname, MGString& sparams)	= 0;

	virtual MGString	GetStepName() const							= 0;
	virtual void		GetDotDepend( vector<MGSize>& tab) const	= 0;
	virtual bool		IsType( const MGString& type) const			= 0;

protected:
	MGSize		ReadMtxId( vector< vector<MGSize> >& mtxres, const MGString& sparams);
	MGSize		ReadMtxFloat( vector< vector<MGFloat> >& mtxres, const MGString& sparams);

	MGSize		ReadTabId( vector<MGSize>& tabres, const MGString& sparams);
	MGSize		ReadTabUInt( vector<MGSize>& tabres, const MGString& sparams);
	MGSize		ReadTabFloat( vector<MGFloat>& tabres, const MGString& sparams);

	StepFlag					ReadFlag( const MGString& sparams);
	StepFlag_BSplineCurveForm	ReadFlagBSplineCurveForm( const MGString& sparams);
	StepFlag_BSplineSurfaceForm ReadFlagBSplineSurfaceForm( const MGString& sparams);
	StepFlag_KnotType			ReadFlagKnotType( const MGString& sparams);

};



//////////////////////////////////////////////////////////////////////
// class StepRepresentationItem
//////////////////////////////////////////////////////////////////////
class StepRepresentationItem : public StepEntity
{
public:
	StepRepresentationItem() : mText("")	{}

	static const char*		ClassName()		{ return STP_REPRESENTATION_ITEM;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const	{}
	virtual bool		IsType( const MGString& type) const		{ return (type == ClassName());}

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&		cStepId() const		{ return mStepId;}
	const MGString&		cText() const		{ return mText;}

private:
	MGSize		mStepId;
	MGString	mText;
};


//////////////////////////////////////////////////////////////////////
// class StepUnknownObject
//////////////////////////////////////////////////////////////////////
class StepUnknownObject : public StepRepresentationItem
{
public:
	StepUnknownObject()		{}

	static const char*		ClassName()		{ return "UNKNOWN";}
};





//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StepSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Step = StepSpace;


#endif // __STEPENTITY_H__
