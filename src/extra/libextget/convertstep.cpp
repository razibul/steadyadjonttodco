#include "convertstep.h"

#include "libextstep/stepmaster.h"
#include "libextstep/stepconst.h"
#include "libextstep/stepgeometry.h"
#include "libextstep/steptopology.h"

#include "libextget/geotopomaster.h"
#include "libextget/getpoint.h"
#include "libextget/getcurve.h"
#include "libextget/getsurface.h"
#include "libextget/topoentity.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


bool GetStepFlag( const Step::StepFlag& sflg)
{
	if ( sflg == Step::FLAG_TRUE)
		return true;

	if ( sflg == Step::FLAG_FALSE)
		return false;

	THROW_INTERNAL( "ConvertStep::GetStepFlag : unrecognized flag" );
}


const Step::StepEntity* ConvertStep::GetStepEnt( const MGSize& id)
{
	const Step::StepMaster::StepMap		&smap = mpStep->cStepMap();
	Step::StepMaster::StepMap::const_iterator	citr = smap.find( id);

	if ( citr == smap.end() )
		return NULL;

	return (*citr).second.cpObject();
}

template <class T>
const T* ConvertStep::GetStepEntity( const MGSize& id)
{
	const Step::StepEntity* pstp = GetStepEnt( id );
	if ( ! pstp)
	{
		ostringstream os;
		os << "ConvertStep::GetStepEntity : valid entity '" << T::ClassName() << "' not found; id = " << id;
		THROW_INTERNAL( os.str() );
	}
	
	if ( ! pstp->IsType( T::ClassName() ) )
	{
		ostringstream os;
		os << "ConvertStep::GetStepEntity : not a '" << T::ClassName() << "'; id = " << id << ", type = '" << pstp->GetStepName() << "'";
		THROW_INTERNAL( os.str() );
	}
	
	return (const T*)pstp;
}


MGInt ConvertStep::GetId( const MGSize& stepid) const
{
	ConvertMap::const_iterator	citr;

	if ( (citr = mmapId.find( stepid)) == mmapId.end() )
		THROW_INTERNAL( "ConvertStep::GetId : STEP id not found inside a map");

	return (*citr).second;
}


void ConvertStep::UpdateId( const MGSize& stepid, const MGSize& getid)
{
	ConvertMap::iterator	itr;

	if ( (itr = mmapId.find( stepid)) == mmapId.end() )
		THROW_INTERNAL( "ConvertStep::GetId : STEP id not found inside a map");

	(*itr).second = getid;
}




void ConvertStep::InitMapId()
{
	const Step::StepMaster::StepMap		&smap = mpStep->cStepMap();

	Step::StepMaster::StepMap::const_iterator	citr;
	for ( citr=smap.begin(); citr!=smap.end(); ++citr )
	{
		mmapId.insert( ConvertMap::value_type( (*citr).first, -1 ) );
	}

}

//////////////////////////////////////////////////////////////////////

void ConvertStep::BuildVertexTab()
{
	const Step::StepMaster::StepMap		&smap = mpStep->cStepMap();

	Step::StepMaster::StepMap::const_iterator	citr;
	for ( citr=smap.begin(); citr!=smap.end(); ++citr )
	{
		if ( (*citr).second.cName() == Step::STP_VERTEX_POINT )
		{
			CreateVertexPnt( (*citr).first );
		}
	}
}

void ConvertStep::BuildEdgeTab()
{
	const Step::StepMaster::StepMap		&smap = mpStep->cStepMap();

	Step::StepMaster::StepMap::const_iterator	citr;
	for ( citr=smap.begin(); citr!=smap.end(); ++citr )
	{
		if ( (*citr).second.cName() == Step::STP_EDGE_CURVE )
		{
			CreateEdgeCrv( (*citr).first );
		}
	}

	//Step::StepMaster::StepMap::const_iterator	citr;
	//for ( citr=smap.begin(); citr!=smap.end(); ++citr )
	//{
	//	if ( (*citr).second.cName() == Step::STP_CIRCLE )
	//	{
	//		CreateCurve( (*citr).first );
	//	}
	//}
}

void ConvertStep::BuildFaceTab()
{
	const Step::StepMaster::StepMap		&smap = mpStep->cStepMap();

	Step::StepMaster::StepMap::const_iterator	citr;
	for ( citr=smap.begin(); citr!=smap.end(); ++citr )
	{
		if ( (*citr).second.cName() == Step::STP_ADVANCED_FACE )
		{
			CreateFaceSrf( (*citr).first );
		}
	}

}


void ConvertStep::BuildBRepTab()
{
	const Step::StepMaster::StepMap		&smap = mpStep->cStepMap();

	Step::StepMaster::StepMap::const_iterator	citr;
	for ( citr=smap.begin(); citr!=smap.end(); ++citr )
	{
		if ( (*citr).second.cName() == Step::STP_MANIFOLD_SOLID_BREP )
		{
			CreateBRep( (*citr).first );
		}
		else
		if ( (*citr).second.cName() == Step::STP_BREP_WITH_VOIDS )
		{
			CreateBRepVoids( (*citr).first );
		}
	}
}

//////////////////////////////////////////////////////////////////////


MGSize ConvertStep::CreateVertexPnt( const MGSize& stepid )
{
	const Step::StepVertexPoint* pstpVrtx = GetStepEntity<Step::StepVertexPoint>( stepid);

	MGInt	getid = GetId( pstpVrtx->cIdPoint() );

	if ( getid < 0 )
		getid = CreatePoint( pstpVrtx->cIdPoint() );

	if ( getid <= 0 )
		THROW_INTERNAL( "failed to initialize VertexPnt");



	MGString sname, sparams;
	
	sname = GET::TopoEntity<DIM_0D>::ClassName();

	ostringstream os;
	os << "#" << getid;
	sparams = os.str();


	GET::TopoEntity<DIM_0D>	tent;

	tent.Init( sname, sparams);

	MGSize id = mpGeT->mTOwner.PushBack<DIM_0D>( tent );

	UpdateId( stepid, id);

	return id;
}

MGSize ConvertStep::CreateEdgeCrv( const MGSize& stepid )
{
	const Step::StepEdgeCurve* pstpEdge = GetStepEntity<Step::StepEdgeCurve>( stepid);

	MGInt	getcrvid = GetId( pstpEdge->cIdCurve() );
	if ( getcrvid < 0 )
		getcrvid = CreateCurve( pstpEdge->cIdCurve() );

	MGInt	getvrt1id = GetId( pstpEdge->cIdVertex1() );
	if ( getvrt1id < 0 )
		getvrt1id = CreateVertexPnt( pstpEdge->cIdVertex1() );

	MGInt	getvrt2id = GetId( pstpEdge->cIdVertex2() );
	if ( getvrt2id < 0 )
		getvrt2id = CreateVertexPnt( pstpEdge->cIdVertex2() );


	if ( getcrvid <= 0 || getvrt1id <= 0 || getvrt2id <= 0 )
		THROW_INTERNAL( "failed to initialize EdgeCrv");

	bool borient = GET::GetStepFlag( pstpEdge->cflOrient() );
	// TODO :: do something with the orientation !!!


	MGString sname, sparams;
	
	sname = GET::TopoEntity<DIM_1D>::ClassName();

	ostringstream os;
	os << "#" << getcrvid << ",(#" << getvrt1id << ",#" << getvrt2id << ")";
	os << ",";
	if ( borient)
		os << "T";
	else
		os << "F";
	sparams = os.str();


	GET::TopoEntity<DIM_1D>	tent;

	tent.Init( sname, sparams);

	MGSize id = mpGeT->mTOwner.PushBack<DIM_1D>( tent );

	UpdateId( stepid, id);

	return id;

}


MGSize ConvertStep::CreateFaceSrf( const MGSize& stepid)
{
	const Step::StepAdvancedFace* pstpFace = GetStepEntity<Step::StepAdvancedFace>( stepid);

	MGInt getsrfid = GetId( pstpFace->cIdSurface() );

	if ( getsrfid < 0 )
		getsrfid = CreateSurface( pstpFace->cIdSurface() );

	//bool bsrforient = GeT::GetStepFlag( pstpFace->cflOrient() );

	//MGSize id = mpGet->mtabFace.size();
	//mpGet->mtabFace.push_back( FaceSrf( id, getsrfid, bsrforient) );
	//FaceSrf& face = mpGet->mtabFace[id];

	MGString sname, sparams;
	
	sname = GET::TopoEntity<DIM_2D>::ClassName();

	ostringstream os;

	os << "#" << getsrfid << ",";

	for ( vector<MGSize>::const_iterator i=pstpFace->ctabIdBound().begin(); i!=pstpFace->ctabIdBound().end(); ++i)
	{
		const Step::StepFaceBound* pstpFBound = GetStepEntity<Step::StepFaceBound>( *i );
		const Step::StepEdgeLoop* pstpELoop = GetStepEntity<Step::StepEdgeLoop>( pstpFBound->cIdLoop() );

		bool boFB = GET::GetStepFlag( pstpFBound->cflOrient() );

		if ( i != pstpFace->ctabIdBound().begin() )
			os << ",";

		os << "LOOP((";
				
		for ( vector<MGSize>::const_iterator ie=pstpELoop->ctabIdEdge().begin(); ie!=pstpELoop->ctabIdEdge().end(); ++ie)
		{
			if ( ie != pstpELoop->ctabIdEdge().begin() )
				os << ",";

			const Step::StepOrientedEdge* pstpOEdg = GetStepEntity<Step::StepOrientedEdge>( *ie );
			bool boOE = GET::GetStepFlag( pstpOEdg->cflOrient() );

			if ( (boOE && boFB) || (!boOE && !boFB) )
				boOE = true;
			else
				boOE = false;

			MGInt	getedgid = GetId( pstpOEdg->cIdEdge() );
			if ( getedgid < 0 )
				getedgid = CreateEdgeCrv( pstpOEdg->cIdEdge() );


			os << "(#" << getedgid << ",";
			if ( boOE )
				os << "T";
			else
				os << "F";

			os << ")";
		}

		os << "),T)";

	}

	//os << "#" << getcrvid << ",(#" << getvrt1id << ",#" << getvrt2id << ")";
	sparams = os.str();


	GET::TopoEntity<DIM_2D>	tent;

	tent.Init( sname, sparams);

	MGSize id = mpGeT->mTOwner.PushBack<DIM_2D>( tent );

	UpdateId( stepid, id);

	return id;
}


MGSize ConvertStep::CreateBRep( const MGSize& stepid)
{
	const Step::StepManifoldSolidBRep* pstpBRep = GetStepEntity<Step::StepManifoldSolidBRep>( stepid);

	MGSize	idshell = pstpBRep->cIdCShell();
	const Step::StepClosedShell* pstpCShell = GetStepEntity<Step::StepClosedShell>( idshell);


	//bool bsrforient = GeT::GetStepFlag( pstpFace->cflOrient() );

	//MGSize id = mpGet->mtabFace.size();
	//mpGet->mtabFace.push_back( FaceSrf( id, getsrfid, bsrforient) );
	//FaceSrf& face = mpGet->mtabFace[id];

	MGString sname, sparams;
	
	sname = GET::TopoEntity<DIM_3D>::ClassName();

	ostringstream os;

	os << "#" << 0 << ",";

	os << "LOOP((";

	for ( vector<MGSize>::const_iterator i=pstpCShell->ctabIdFace().begin(); i!=pstpCShell->ctabIdFace().end(); ++i)
	{
		if ( i != pstpCShell->ctabIdFace().begin() )
			os << ",";

		//const Step::StepAdvancedFace* pstpFace = GetStepEntity<Step::StepAdvancedFace>( *i );

		MGSize	getfaceid = GetId( *i );
		os << "(#" << getfaceid << ",T)";
	}

	os << "),T)";

	//os << "#" << getcrvid << ",(#" << getvrt1id << ",#" << getvrt2id << ")";
	sparams = os.str();


	GET::TopoEntity<DIM_3D>	tent;

	tent.Init( sname, sparams);

	MGSize id = mpGeT->mTOwner.PushBack<DIM_3D>( tent );

	UpdateId( stepid, id);

	return id;
}


MGSize ConvertStep::CreateBRepVoids( const MGSize& stepid)
{
	const Step::StepBRepWithVoids* pstpBRep = GetStepEntity<Step::StepBRepWithVoids>( stepid);

	MGSize	idshell = pstpBRep->cIdCShell();
	const Step::StepClosedShell* pstpCShell = GetStepEntity<Step::StepClosedShell>( idshell);

	//bool bsrforient = GeT::GetStepFlag( pstpFace->cflOrient() );

	MGString sname, sparams;
	
	sname = GET::TopoEntity<DIM_3D>::ClassName();

	ostringstream os;

	os << "#" << 0 << ",";	// there is no geometry for 3D domains
	os << "LOOP((";

	for ( vector<MGSize>::const_iterator i=pstpCShell->ctabIdFace().begin(); i!=pstpCShell->ctabIdFace().end(); ++i)
	{
		if ( i != pstpCShell->ctabIdFace().begin() )
			os << ",";

		//const Step::StepAdvancedFace* pstpFace = GetStepEntity<Step::StepAdvancedFace>( *i );

		MGSize	getfaceid = GetId( *i );
		os << "(#" << getfaceid << ",T)";
	}

	os << "),T)";


	// TODO :: dump all shells defining voids



	//os << "#" << getcrvid << ",(#" << getvrt1id << ",#" << getvrt2id << ")";
	sparams = os.str();


	GET::TopoEntity<DIM_3D>	tent;

	tent.Init( sname, sparams);

	MGSize id = mpGeT->mTOwner.PushBack<DIM_3D>( tent );

	UpdateId( stepid, id);

	return id;
}


//////////////////////////////////////////////////////////////////////

template <class T>
T* ConvertStep::Create( const MGString& sname, const MGString& sparams)
{
	try
	{
		if ( sname != T::ClassName() )
			return NULL;

		T* ptr = new T;
		MGString sbuf = sparams;
		ptr->Init( sname, sbuf);
		if ( sbuf.length() > 0 )
			THROW_INTERNAL( "not every argument has been interpreted" );

		return ptr;
	}
	catch ( EHandler::Except&)
	{
		cout << endl;
		cout << "ConvertStep :: " << T::ClassName() << "  params = \"" << sparams << "\"" << endl;

		throw;
	}

	return NULL;

}



//Vect3D ConvertStep::GetStepDirection( const MGSize& stepid )
//{
//	const Step::StepEntity* pSEnt = GetStepEntity<Step::StepPoint>( stepid);
//
//	if ( pSEnt->GetStepName() == Step::STP_DIRECTION )
//	{
//		const Step::StepDirection	*pstpDir = (const Step::StepDirection*)pSEnt;
//
//		return pstpDir->cVect();
//	}
//	else
//	{
//		THROW_INTERNAL( "ConvertStep::GetStepDirection : unknown Direction type");
//	}
//
//	return Vect3D();
//}
//
//
//Vect3D ConvertStep::GetStepPointCoordinates( const MGSize& stepid )
//{
//	const Step::StepEntity* pSEnt = GetStepEntity<Step::StepPoint>( stepid);
//
//	if ( pSEnt->GetStepName() == Step::STP_CARTESIAN_POINT )
//	{
//		const Step::StepCartesianPoint	*pstpPnt = (const Step::StepCartesianPoint*)pSEnt;
//
//		return pstpPnt->cVect();
//	}
//	else
//	{
//		THROW_INTERNAL( "ConvertStep::GetStepPointCoordinates : unknown Point type");
//	}
//
//	return Vect3D();
//}
//
//Vect3D ConvertStep::GetStepVector( const MGSize& stepid )
//{
//	const Step::StepEntity* pSEnt = GetStepEntity<Step::StepVector>( stepid);
//
//	if ( pSEnt->GetStepName() == Step::STP_VECTOR )
//	{
//		const Step::StepVector	*pstpVct = (const Step::StepVector*)pSEnt;
//
//		MGFloat length = pstpVct->cLength();
//		Vect3D vct = GetStepDirection( pstpVct->cDirId() );
//
//
//		return length * vct;
//	}
//	else
//	{
//		THROW_INTERNAL( "ConvertStep::GetStepVector : unknown Vector type");
//	}
//
//	return Vect3D();
//}


MGSize ConvertStep::CreatePoint( const MGSize& stepid )
{
	Vect3D	vct = GetStpPointCoor( stepid );


	MGString sname, sparams;
	
	sname = GET::Point<DIM_3D>::ClassName();

	ostringstream os;
	os << setprecision(16); 
	os << vct.cX() << "," << vct.cY() << "," << vct.cZ();
	sparams = os.str();

	GET::Point<DIM_3D>	*ptr = Create< GET::Point<DIM_3D> >( sname, sparams);

	MGSize id = mpGeT->mGOwner.PushBack<DIM_0D>( ptr );

	UpdateId( stepid, id);

	return id;
}



MGSize ConvertStep::CreateCurve( const MGSize& stepid )
{
	const Step::StepEntity* pSEnt = GetStepEntity<Step::StepCurve>( stepid);

	if ( pSEnt->GetStepName() == Step::STP_LINE )
	{
		return CreateLine( stepid);
	}
	else
	if ( pSEnt->GetStepName() == Step::STP_CIRCLE )
	{
		return CreateCircle( stepid);
	}
	else
	if ( pSEnt->GetStepName() == Step::STP_ELLIPSE )
	{
		return CreateEllipse( stepid);
	}
	else
	if ( pSEnt->GetStepName() == Step::STP_B_SPLINE_CURVE_WITH_KNOTS )
	{
		return CreateCrvBSplineWKnots( stepid);
	}
	else
	if ( pSEnt->GetStepName() == Step::STP_QUASI_UNIFORM_CURVE )
	{
		return CreateCrvBSplineWKnots( stepid);
	}
	else
	if ( pSEnt->GetStepName() == Step::STP_RATIONAL_B_SPLINE_CURVE )
	{
		return CreateCrvRationalBSpline( stepid);
		//MGString buf = MGString( "conversion of the STEP entity '") + MGString( Step::STP_RATIONAL_B_SPLINE_CURVE) + MGString( "' not implemented ");
		//THROW_INTERNAL( buf.c_str() );
	}
	else
	{
		cout << pSEnt->GetStepName() << endl;
		THROW_INTERNAL( "ConvertStep::CreateCurve : unknown Curve type");
	}
	
	return 0;
}


MGSize ConvertStep::CreateSurface( const MGSize& stepid)
{
	const Step::StepEntity* pSEnt = GetStepEntity<Step::StepSurface>( stepid);

	if ( pSEnt->GetStepName() == Step::STP_PLANE )
	{
		return CreatePlane( stepid);
	}
	else
	if ( pSEnt->GetStepName() == Step::STP_B_SPLINE_SURFACE_WITH_KNOTS )
	{
		return CreateSrfBSplineWKnots( stepid);
	}
	else
	if ( pSEnt->GetStepName() == Step::STP_QUASI_UNIFORM_SURFACE )
	{
		return CreateSrfBSplineWKnots( stepid);
	}
	else
	if ( pSEnt->GetStepName() == Step::STP_RATIONAL_B_SPLINE_SURFACE )
	{
		return CreateSrfRationalBSpline( stepid);
	}
	else
	if ( pSEnt->GetStepName() == Step::STP_CYLINDRICAL_SURFACE )
	{
		return CreateCylindricalSrf( stepid);
		MGString buf = MGString( "conversion of the STEP entity '") + MGString( Step::STP_CYLINDRICAL_SURFACE) + MGString( "' not implemented ");
		//THROW_INTERNAL( buf.c_str() );
	}
	else
	if ( pSEnt->GetStepName() == Step::STP_CONICAL_SURFACE )
	{
		return CreateConicalSrf( stepid);
		MGString buf = MGString( "conversion of the STEP entity '") + MGString( Step::STP_CONICAL_SURFACE) + MGString( "' not implemented ");
		//THROW_INTERNAL( buf.c_str() );
	}
	else
	if ( pSEnt->GetStepName() == Step::STP_SPHERICAL_SURFACE )
	{
		return CreateSphericalSrf( stepid);
		MGString buf = MGString( "conversion of the STEP entity '") + MGString( Step::STP_SPHERICAL_SURFACE) + MGString( "' not implemented ");
		//THROW_INTERNAL( buf.c_str() );
	}
	else
	if ( pSEnt->GetStepName() == Step::STP_TOROIDAL_SURFACE )
	{
		return CreateToroidalSrf( stepid);
		MGString buf = MGString( "conversion of the STEP entity '") + MGString( Step::STP_TOROIDAL_SURFACE) + MGString( "' not implemented ");
		//THROW_INTERNAL( buf.c_str() );
	}
	else
	{
		cout << pSEnt->GetStepName() << endl;
		THROW_INTERNAL( "ConvertStep::CreateSurface : unknown Surface type");
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////


MGSize ConvertStep::CreateLine( const MGSize& stepid )
{
	const Step::StepLine* pstpLine = GetStepEntity<Step::StepLine>( stepid);

	Vect3D pos = GetStpPointCoor( pstpLine->cIdPoint() );
	Vect3D dir = GetStpVector( pstpLine->cIdVect() );
	

	MGString sname, sparams;
	
	sname = GET::Line<DIM_3D>::ClassName();

	ostringstream os;
	os << setprecision(16); 
	os <<  "(" << pos.cX() << "," << pos.cY() << "," << pos.cZ() << ")";
	os << ",(" << dir.cX() << "," << dir.cY() << "," << dir.cZ() << ")";
	sparams = os.str();

	GET::Line<DIM_3D>	*ptr = Create< GET::Line<DIM_3D> >( sname, sparams);

	MGSize id = mpGeT->mGOwner.PushBack<DIM_1D>( ptr );

	UpdateId( stepid, id);
	return id;
}


MGSize ConvertStep::CreateCircle( const MGSize& stepid )
{
	const Step::StepCircle* pstpCircle = GetStepEntity<Step::StepCircle>( stepid);
	const Step::StepAxis2Placement3D * pstpAxis = GetStepEntity<Step::StepAxis2Placement3D >( pstpCircle->cIdAxis() );

	Vect3D pos = GetStpPointCoor( pstpAxis->cIdPoint() );

	Vect3D dirn, dirx, diry;
	BuildAxes( pstpAxis->cIdAxis(), pstpAxis->cIdRefDir(), dirx, diry, dirn);

	MGFloat rad = pstpCircle->cRadius();

	MGString sname, sparams;
	
	sname = GET::Circle<DIM_3D>::ClassName();

	ostringstream os;
	os << setprecision(16); 
	os <<  "(" << pos.cX() << "," << pos.cY() << "," << pos.cZ() << ")";
	os << ",(" << dirx.cX() << "," << dirx.cY() << "," << dirx.cZ() << ")";
	os << ",(" << diry.cX() << "," << diry.cY() << "," << diry.cZ() << ")";
	os << "," << rad;
	sparams = os.str();


	GET::Circle<DIM_3D>	*ptr = Create< GET::Circle<DIM_3D> >( sname, sparams);

	MGSize id = mpGeT->mGOwner.PushBack<DIM_1D>( ptr );

	UpdateId( stepid, id);
	return id;
}

MGSize ConvertStep::CreateEllipse( const MGSize& stepid )
{
	const Step::StepEllipse* pstpEllipse = GetStepEntity<Step::StepEllipse>( stepid);
	const Step::StepAxis2Placement3D * pstpAxis = GetStepEntity<Step::StepAxis2Placement3D >( pstpEllipse->cIdAxis() );

	Vect3D pos = GetStpPointCoor( pstpAxis->cIdPoint() );

	Vect3D dirn, dirx, diry;
	BuildAxes( pstpAxis->cIdAxis(), pstpAxis->cIdRefDir(), dirx, diry, dirn);

	MGFloat saOX = pstpEllipse->cSemiAxis1();
	MGFloat saOY = pstpEllipse->cSemiAxis2();

	MGString sname, sparams;
	
	sname = GET::Ellipse<DIM_3D>::ClassName();

	ostringstream os;
	os << setprecision(16); 
	os <<  "(" << pos.cX() << "," << pos.cY() << "," << pos.cZ() << ")";
	os << ",(" << dirx.cX() << "," << dirx.cY() << "," << dirx.cZ() << ")";
	os << ",(" << diry.cX() << "," << diry.cY() << "," << diry.cZ() << ")";
	os << "," << saOX;
	os << "," << saOY;
	sparams = os.str();


	GET::Ellipse<DIM_3D>	*ptr = Create< GET::Ellipse<DIM_3D> >( sname, sparams);

	MGSize id = mpGeT->mGOwner.PushBack<DIM_1D>( ptr );

	UpdateId( stepid, id);
	return id;
}


MGSize ConvertStep::CreateCrvBSplineWKnots( const MGSize& stepid )
{
	const Step::StepBSplineCurveWithKnots* pstpBSpline = GetStepEntity<Step::StepBSplineCurveWithKnots>( stepid);
	
	MGSize deg = pstpBSpline->cDeg();
	const vector<MGSize>& tabpnt = pstpBSpline->cTabPntId();
	const vector<MGFloat>& tabknot = pstpBSpline->cTabKnot();
	const vector<MGSize>& tabmult = pstpBSpline->cTabMult();

	MGString sname, sparams;
	
	sname = GET::NurbsCrv<DIM_3D>::ClassName();

	ostringstream os;
	os << setprecision(16); 

	if ( pstpBSpline->cIsClosed() == Step::FLAG_TRUE )
		os << "T,";
	else
		os << "F,";

	os << deg << ",(";

	for ( MGSize iknot=0; iknot<tabknot.size(); ++iknot)
	{
		os << tabknot[iknot];
		for ( MGSize imult=1; imult<tabmult[iknot]; ++imult)
			os << "," << tabknot[iknot];

		if ( iknot < tabknot.size()-1 )
			os << ",";
	}

	os << "),(";

	for ( MGSize ipnt=0; ipnt<tabpnt.size(); ++ipnt)
	{
		Vect3D vct = GetStpPointCoor( tabpnt[ipnt] );
		os << "(" << vct.cX() << "," << vct.cY() << "," << vct.cZ() << ",1.0)";
		
		if ( ipnt < tabpnt.size()-1 )
			os << ",";
	}

	os << ")";
	sparams = os.str();


	GET::NurbsCrv<DIM_3D>	*ptr = Create< GET::NurbsCrv<DIM_3D> >( sname, sparams);

	MGSize id = mpGeT->mGOwner.PushBack<DIM_1D>( ptr );

	UpdateId( stepid, id);
	return id;
}


MGSize ConvertStep::CreateCrvRationalBSpline( const MGSize& stepid )
{
	const Step::StepRationalBSplineCurve* pstpBSpline = GetStepEntity<Step::StepRationalBSplineCurve>( stepid);
	

	MGSize deg = pstpBSpline->cDeg();
	const vector<MGSize>& tabpnt = pstpBSpline->cTabPntId();
	const vector<MGFloat>& tabknot = pstpBSpline->cTabKnot();
	const vector<MGFloat>& tabwght = pstpBSpline->cTabWght();
	const vector<MGSize>& tabmult = pstpBSpline->cTabMult();

	MGString sname, sparams;
	
	sname = GET::NurbsCrv<DIM_3D>::ClassName();

	ostringstream os;
	os << setprecision(16); 
	
	if ( pstpBSpline->cIsClosed() == Step::FLAG_TRUE )
		os << "T,";
	else
		os << "F,";

	
	os << deg << ",(";

	for ( MGSize iknot=0; iknot<tabknot.size(); ++iknot)
	{
		os << tabknot[iknot];
		for ( MGSize imult=1; imult<tabmult[iknot]; ++imult)
			os << "," << tabknot[iknot];

		if ( iknot < tabknot.size()-1 )
			os << ",";
	}

	os << "),(";

	for ( MGSize ipnt=0; ipnt<tabpnt.size(); ++ipnt)
	{
		Vect3D vct = GetStpPointCoor( tabpnt[ipnt] );
		MGFloat wght = tabwght[ipnt];

		os << "(" << wght*vct.cX() << "," << wght*vct.cY() << "," << wght*vct.cZ() << "," << wght << ")";
		
		if ( ipnt < tabpnt.size()-1 )
			os << ",";
	}

	os << ")";
	sparams = os.str();


	GET::NurbsCrv<DIM_3D>	*ptr = Create< GET::NurbsCrv<DIM_3D> >( sname, sparams);

	MGSize id = mpGeT->mGOwner.PushBack<DIM_1D>( ptr );

	UpdateId( stepid, id);
	return id;
}




MGSize ConvertStep::CreatePlane( const MGSize& stepid )
{
	const Step::StepPlane* pstpPlane = GetStepEntity<Step::StepPlane>( stepid);

	const Step::StepAxis2Placement3D* pstpAxis = GetStepEntity<Step::StepAxis2Placement3D>( pstpPlane->cIdAxis() );
	
	
	Vect3D pos = GetStpPointCoor( pstpAxis->cIdPoint() );

	Vect3D dirn, dirx, diry;
	BuildAxes( pstpAxis->cIdAxis(), pstpAxis->cIdRefDir(), dirx, diry, dirn);
	
	MGString sname, sparams;
	
	sname = GET::Plane<DIM_3D>::ClassName();

	ostringstream os;
	os << setprecision(16); 
	os <<  "(" << pos.cX() << "," << pos.cY() << "," << pos.cZ() << ")";
	os << ",(" << dirx.cX() << "," << dirx.cY() << "," << dirx.cZ() << ")";
	os << ",(" << diry.cX() << "," << diry.cY() << "," << diry.cZ() << ")";
	sparams = os.str();

	GET::Plane<DIM_3D>	*ptr = Create< GET::Plane<DIM_3D> >( sname, sparams);

	MGSize id = mpGeT->mGOwner.PushBack<DIM_2D>( ptr );

	UpdateId( stepid, id);
	return id;
}


MGSize ConvertStep::CreateSrfBSplineWKnots( const MGSize& stepid)
{
	const Step::StepBSplineSurfaceWithKnots* pstpBSpline = GetStepEntity<Step::StepBSplineSurfaceWithKnots>( stepid);

	MGSize degu = pstpBSpline->cDegU();
	MGSize degv = pstpBSpline->cDegV();

	const vector< vector<MGSize> >& mtxpnt = pstpBSpline->cMtxPntId();

	const vector<MGFloat>& tabknotu = pstpBSpline->cTabKnotU();
	const vector<MGSize>& tabmultu = pstpBSpline->cTabMultU();

	const vector<MGFloat>& tabknotv = pstpBSpline->cTabKnotV();
	const vector<MGSize>& tabmultv = pstpBSpline->cTabMultV();

	MGString sname, sparams;
	
	sname = GET::NurbsSrf<DIM_3D>::ClassName();

	ostringstream os;
	os << setprecision(16); 

	if ( pstpBSpline->cIsClosedU() == Step::FLAG_TRUE )
		os << "T,";
	else
		os << "F,";

	if ( pstpBSpline->cIsClosedV() == Step::FLAG_TRUE )
		os << "T,";
	else
		os << "F,";


	os << degu << "," << degv << ",(";

	for ( MGSize iknot=0; iknot<tabknotu.size(); ++iknot)
	{
		os << tabknotu[iknot];
		for ( MGSize imult=1; imult<tabmultu[iknot]; ++imult)
			os << "," << tabknotu[iknot];

		if ( iknot < tabknotu.size()-1 )
			os << ",";
	}

	os << "),(";

	for ( MGSize iknot=0; iknot<tabknotv.size(); ++iknot)
	{
		os << tabknotv[iknot];
		for ( MGSize imult=1; imult<tabmultv[iknot]; ++imult)
			os << "," << tabknotv[iknot];

		if ( iknot < tabknotv.size()-1 )
			os << ",";
	}

	os << "),(";

	for ( MGSize ipntu=0; ipntu<mtxpnt.size(); ++ipntu)
	{
		os << "(";
		for ( MGSize ipntv=0; ipntv<mtxpnt[ipntu].size(); ++ipntv)
		{
			Vect3D vct = GetStpPointCoor( mtxpnt[ipntu][ipntv] );
			os << "(" << vct.cX() << "," << vct.cY() << "," << vct.cZ() << ",1.0)";
			
			if ( ipntv < mtxpnt[ipntu].size()-1 )
				os << ",";
		}
		os << ")";
		if ( ipntu < mtxpnt.size()-1 )
			os << ",";
	}

	os << ")";
	sparams = os.str();


	GET::NurbsSrf<DIM_3D>	*ptr = Create< GET::NurbsSrf<DIM_3D> >( sname, sparams);

	MGSize id = mpGeT->mGOwner.PushBack<DIM_2D>( ptr );

	UpdateId( stepid, id);
	return id;
}

MGSize ConvertStep::CreateSrfRationalBSpline( const MGSize& stepid)
{
	//THROW_INTERNAL("CreateSrfRationalBSpline : not implemented");

	const Step::StepRationalBSplineSurface* pstpBSpline = GetStepEntity<Step::StepRationalBSplineSurface>( stepid);

	MGSize degu = pstpBSpline->cDegU();
	MGSize degv = pstpBSpline->cDegV();

	const vector< vector<MGSize> >& mtxpnt = pstpBSpline->cMtxPntId();
	const vector< vector<MGFloat> >& mtxwght = pstpBSpline->cMtxWght();

	const vector<MGFloat>& tabknotu = pstpBSpline->cTabKnotU();
	const vector<MGSize>& tabmultu = pstpBSpline->cTabMultU();

	const vector<MGFloat>& tabknotv = pstpBSpline->cTabKnotV();
	const vector<MGSize>& tabmultv = pstpBSpline->cTabMultV();

	MGString sname, sparams;
	
	sname = GET::NurbsSrf<DIM_3D>::ClassName();

	ostringstream os;
	os << setprecision(16); 

	if ( pstpBSpline->cIsClosedU() == Step::FLAG_TRUE )
		os << "T,";
	else
		os << "F,";

	if ( pstpBSpline->cIsClosedV() == Step::FLAG_TRUE )
		os << "T,";
	else
		os << "F,";

	os << degu << "," << degv << ",(";

	for ( MGSize iknot=0; iknot<tabknotu.size(); ++iknot)
	{
		os << tabknotu[iknot];
		for ( MGSize imult=1; imult<tabmultu[iknot]; ++imult)
			os << "," << tabknotu[iknot];

		if ( iknot < tabknotu.size()-1 )
			os << ",";
	}

	os << "),(";

	for ( MGSize iknot=0; iknot<tabknotv.size(); ++iknot)
	{
		os << tabknotv[iknot];
		for ( MGSize imult=1; imult<tabmultv[iknot]; ++imult)
			os << "," << tabknotv[iknot];

		if ( iknot < tabknotv.size()-1 )
			os << ",";
	}

	os << "),(";

	for ( MGSize ipntu=0; ipntu<mtxpnt.size(); ++ipntu)
	{
		os << "(";
		for ( MGSize ipntv=0; ipntv<mtxpnt[ipntu].size(); ++ipntv)
		{
			Vect3D vct = GetStpPointCoor( mtxpnt[ipntu][ipntv] );
			MGFloat wght = mtxwght[ipntu][ipntv];
			os << "(" << wght*vct.cX() << "," << wght*vct.cY() << "," << wght*vct.cZ() << "," << wght << ")";
			
			if ( ipntv < mtxpnt[ipntu].size()-1 )
				os << ",";
		}
		os << ")";
		if ( ipntu < mtxpnt.size()-1 )
			os << ",";
	}

	os << ")";
	sparams = os.str();


	GET::NurbsSrf<DIM_3D>	*ptr = Create< GET::NurbsSrf<DIM_3D> >( sname, sparams);

	MGSize id = mpGeT->mGOwner.PushBack<DIM_2D>( ptr );

	UpdateId( stepid, id);
	return id;
}




MGSize ConvertStep::CreateCylindricalSrf( const MGSize& stepid )
{
	const Step::StepCylindricalSurface* pstpCylinder = GetStepEntity<Step::StepCylindricalSurface>( stepid);

	const Step::StepAxis2Placement3D* pstpAxis = GetStepEntity<Step::StepAxis2Placement3D>( pstpCylinder->cIdAxis() );
	
	
	Vect3D pos = GetStpPointCoor( pstpAxis->cIdPoint() );

	Vect3D dirn, dirx, diry;
	BuildAxes( pstpAxis->cIdAxis(), pstpAxis->cIdRefDir(), dirx, diry, dirn);
	
	MGString sname, sparams;
	
	sname = GET::CylindricalSrf<DIM_3D>::ClassName();

	ostringstream os;
	os << setprecision(16); 
	os <<  "(" << pos.cX() << "," << pos.cY() << "," << pos.cZ() << ")";
	os << ",(" << dirx.cX() << "," << dirx.cY() << "," << dirx.cZ() << ")";
	os << ",(" << diry.cX() << "," << diry.cY() << "," << diry.cZ() << ")";
	os << "," << pstpCylinder->cRadius();
	sparams = os.str();

	GET::CylindricalSrf<DIM_3D>	*ptr = Create< GET::CylindricalSrf<DIM_3D> >( sname, sparams);

	MGSize id = mpGeT->mGOwner.PushBack<DIM_2D>( ptr );

	UpdateId( stepid, id);
	return id;
}


MGSize ConvertStep::CreateConicalSrf( const MGSize& stepid )
{
	const Step::StepConicalSurface* pstpCylinder = GetStepEntity<Step::StepConicalSurface>( stepid);

	const Step::StepAxis2Placement3D* pstpAxis = GetStepEntity<Step::StepAxis2Placement3D>( pstpCylinder->cIdAxis() );
	
	
	Vect3D pos = GetStpPointCoor( pstpAxis->cIdPoint() );

	Vect3D dirn, dirx, diry;
	BuildAxes( pstpAxis->cIdAxis(), pstpAxis->cIdRefDir(), dirx, diry, dirn);
	
	MGString sname, sparams;
	
	sname = GET::ConicalSrf<DIM_3D>::ClassName();

	ostringstream os;
	os << setprecision(16); 
	os <<  "(" << pos.cX() << "," << pos.cY() << "," << pos.cZ() << ")";
	os << ",(" << dirx.cX() << "," << dirx.cY() << "," << dirx.cZ() << ")";
	os << ",(" << diry.cX() << "," << diry.cY() << "," << diry.cZ() << ")";
	os << "," << pstpCylinder->cRadius();
	//os << "," << pstpCylinder->cAngle();
	os << "," << pstpCylinder->cAngle() * M_PI/180.0;
	sparams = os.str();

	GET::ConicalSrf<DIM_3D>	*ptr = Create< GET::ConicalSrf<DIM_3D> >( sname, sparams);

	MGSize id = mpGeT->mGOwner.PushBack<DIM_2D>( ptr );

	UpdateId( stepid, id);
	return id;
}

MGSize ConvertStep::CreateSphericalSrf( const MGSize& stepid )
{
	const Step::StepSphericalSurface* pstpSphere = GetStepEntity<Step::StepSphericalSurface>( stepid);

	const Step::StepAxis2Placement3D* pstpAxis = GetStepEntity<Step::StepAxis2Placement3D>( pstpSphere->cIdAxis() );
	
	
	Vect3D pos = GetStpPointCoor( pstpAxis->cIdPoint() );

	Vect3D dirn, dirx, diry;
	BuildAxes( pstpAxis->cIdAxis(), pstpAxis->cIdRefDir(), dirx, diry, dirn);
	
	MGString sname, sparams;
	
	sname = GET::Sphere<DIM_3D>::ClassName();

	ostringstream os;
	os << setprecision(16); 
	os <<  "(" << pos.cX() << "," << pos.cY() << "," << pos.cZ() << ")";
	os << ",(" << dirx.cX() << "," << dirx.cY() << "," << dirx.cZ() << ")";
	os << ",(" << diry.cX() << "," << diry.cY() << "," << diry.cZ() << ")";
	os << "," << pstpSphere->cRadius();
	sparams = os.str();

	GET::Sphere<DIM_3D>	*ptr = Create< GET::Sphere<DIM_3D> >( sname, sparams);

	MGSize id = mpGeT->mGOwner.PushBack<DIM_2D>( ptr );

	UpdateId( stepid, id);
	return id;
}


MGSize ConvertStep::CreateToroidalSrf( const MGSize& stepid )
{
	const Step::StepToroidalSurface* pstpSphere = GetStepEntity<Step::StepToroidalSurface>( stepid);

	const Step::StepAxis2Placement3D* pstpAxis = GetStepEntity<Step::StepAxis2Placement3D>( pstpSphere->cIdAxis() );
	
	
	Vect3D pos = GetStpPointCoor( pstpAxis->cIdPoint() );

	Vect3D dirn, dirx, diry;
	BuildAxes( pstpAxis->cIdAxis(), pstpAxis->cIdRefDir(), dirx, diry, dirn);
	
	MGString sname, sparams;
	
	sname = GET::Torus<DIM_3D>::ClassName();

	ostringstream os;
	os << setprecision(16); 
	os <<  "(" << pos.cX() << "," << pos.cY() << "," << pos.cZ() << ")";
	os << ",(" << dirx.cX() << "," << dirx.cY() << "," << dirx.cZ() << ")";
	os << ",(" << diry.cX() << "," << diry.cY() << "," << diry.cZ() << ")";
	os << "," << pstpSphere->cRadius1();
	os << "," << pstpSphere->cRadius2();
	sparams = os.str();

	GET::Torus<DIM_3D>	*ptr = Create< GET::Torus<DIM_3D> >( sname, sparams);

	MGSize id = mpGeT->mGOwner.PushBack<DIM_2D>( ptr );

	UpdateId( stepid, id);
	return id;
}


MGSize ConvertStep::CreateExtrusionSrf( const MGSize& stepid )
{
	const Step::StepSurfaceOfLinearExtrusion* pstpSurf = GetStepEntity<Step::StepSurfaceOfLinearExtrusion>( stepid);

	//const Step::StepCurve* pstpCurve = GetStepEntity<Step::StepCurve>( pstpSurf->cIdCurve() );
	//const Step::StepVector* pstpVector =  GetStepEntity<Step::StepVector>( pstpSurf->cIdVect() );
	
	MGSize igetCurve = CreateCurve( pstpSurf->cIdCurve() );
	
	GeomEntity<DIM_1D,DIM_3D>* pget;

	THROW_INTERNAL("STOP");
	//Vect3D pos = GetStpPointCoor( pstpAxis->cIdPoint() );

	//Vect3D dirn, dirx, diry;
	//BuildAxes( pstpAxis->cIdAxis(), pstpAxis->cIdRefDir(), dirx, diry, dirn);
	//
	//MGString sname, sparams;
	//
	//sname = GET::Sphere<DIM_3D>::ClassName();

	//ostringstream os;
	//os << setprecision(16); 
	//os <<  "(" << pos.cX() << "," << pos.cY() << "," << pos.cZ() << ")";
	//os << ",(" << dirx.cX() << "," << dirx.cY() << "," << dirx.cZ() << ")";
	//os << ",(" << diry.cX() << "," << diry.cY() << "," << diry.cZ() << ")";
	//os << "," << pstpSphere->cRadius();
	//sparams = os.str();

	//GET::Sphere<DIM_3D>	*ptr = Create< GET::Sphere<DIM_3D> >( sname, sparams);

	//MGSize id = mpGeT->mGOwner.PushBack<DIM_2D>( ptr );

	//UpdateId( stepid, id);
	//return id;

	return 0;
}

//////////////////////////////////////////////////////////////////////

Vect3D ConvertStep::GetStpDirection( const MGSize& stepid)
{
	const Step::StepDirection* ptr = GetStepEntity<Step::StepDirection>( stepid);

	return ptr->cVect();
}

Vect3D ConvertStep::GetStpVector( const MGSize& stepid)
{
	const Step::StepVector* ptr = GetStepEntity<Step::StepVector>( stepid);

	return GetStpDirection( ptr->cDirId() ) * ptr->cLength();	// should be a versor ???
}

Vect3D ConvertStep::GetStpPointCoor( const MGSize& stepid)
{
	const Step::StepCartesianPoint* ptr = GetStepEntity<Step::StepCartesianPoint>( stepid);

	return ptr->cVect();
}


void ConvertStep::BuildAxes( const MGSize& stepidAxis, const MGSize& stepidRefDir, Vect3D& vOX, Vect3D& vOY, Vect3D& vOZ)
{
	Vect3D dirn, dirx, diry;

	if ( stepidAxis )
		dirn = GetStpDirection( stepidAxis ).versor(); // normalise ???
	else
		dirn = Vect3D( 0, 0, 1);

	if ( stepidRefDir )
	{
		dirx = GetStpDirection( stepidRefDir ).versor(); // normalise ???
	}
	else
	{
		Vect3D vcx = dirn % Vect3D( 1, 0, 0);
		Vect3D vcy = dirn % Vect3D( 0, 1, 0);

		//if ( (dirn % Vect3D( 1, 0, 0)).module() != 0.0)
		if ( vcx.module() >= vcy.module() )
			dirx = Vect3D( 1, 0, 0);
		else
			dirx = Vect3D( 0, 1, 0);
	}

	diry = (dirn % dirx).versor();  // normalise ???

	vOX = dirx;
	vOY = diry;
	vOZ = dirn;
}


//////////////////////////////////////////////////////////////////////

void ConvertStep::DoConvert()
{
	cout << endl << "converting STEP to GeT" << endl << endl;
	InitMapId();

	mpGeT->mName = mpStep->cName();
	mpGeT->mDescription = mpStep->cDescription();

	BuildVertexTab();
	BuildEdgeTab();
	BuildFaceTab();
	BuildBRepTab();

	//cout << "tabPoint size = " << mpGet->mtabPoint.size() << endl;
	//cout << "tabCurve size = " << mpGet->mtabCurve.size() << endl;
	//cout << "tabSurface size = " << mpGet->mtabSurface.size() << endl;

	//cout << "tabVertex size = " << mpGet->mtabVertex.size() << endl;
	//cout << "tabEdge size = " << mpGet->mtabEdge.size() << endl;
	//cout << "tabFace size = " << mpGet->mtabFace.size() << endl;
	//cout << "tabBRep size = " << mpGet->mtabBRep.size() << endl;

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

