#include "getconst.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

const char	GET_END[]				= "END";
const char	GET_HEADER[]			= "HEADER";
const char	GET_GEOMETRY[]			= "GEOMETRY";
const char	GET_TOPOLOGY[]			= "TOPOLOGY";

const char	GET_SEC_POINT[]			= "SEC_POINT";
const char	GET_SEC_CURVE[]			= "SEC_CURVE";
const char	GET_SEC_SURFACE[]		= "SEC_SURFACE";

const char	GET_SEC_VERTEX[]		= "SEC_VERTEX";
const char	GET_SEC_EDGE[]			= "SEC_EDGE";
const char	GET_SEC_FACE[]			= "SEC_FACE";
const char	GET_SEC_BREP[]			= "SEC_BREP";

const char	GET_POINT[]				= "POINT";
const char	GET_LINE[]				= "LINE";
const char	GET_CIRCLE[]			= "CIRCLE";
const char	GET_ELLIPSE[]			= "ELLIPSE";
const char	GET_SPLINE[]			= "SPLINE";
const char	GET_NURBSCRV[]			= "NURBSCRV";

const char	GET_PLANE[]				= "PLANE";
const char	GET_SPHERE[]			= "SPHERE";
const char	GET_TORUS[]				= "TORUS";
const char	GET_NURBSSRF[]			= "NURBSSRF";
const char	GET_CYLINDRICALSRF[]	= "CYLINDRICALSRF";
const char	GET_CONICALSRF[]		= "CONICALSRF";
const char	GET_EXTRUSIONSRF[]		= "EXTRUSIONSRF";
const char	GET_REVOLUTIONSRF[]		= "REVOLUTIONSRF";


const char	GET_VERTEX[]			= "VERTEX";
const char	GET_EDGE[]				= "EDGE";
const char	GET_FACE[]				= "FACE";
const char	GET_BREP[]				= "BREP";

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
