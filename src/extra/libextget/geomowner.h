#ifndef __GEOMOWNER_H__
#define __GEOMOWNER_H__

#include "libcoresystem/mgdecl.h"
#include "libextget/geomentity.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

//////////////////////////////////////////////////////////////////////
//	class GeomOwner
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, Dimension UNIDIM>
class GeomOwner : public GeomOwner< static_cast<Dimension>( DIM-1 ), UNIDIM >
{
public:
	~GeomOwner()
	{
		for ( MGSize i=0; i<mtabpGeom.size(); ++i)
			delete mtabpGeom[i];
	}


	template <Dimension D>
	MGSize PushBack()		
	{ 
		return 2;
	}

	template <Dimension D>
	MGSize PushBack( GeomEntity<D,UNIDIM>* ptr)		
	{ 
		GeomOwner<D,UNIDIM>::mtabpGeom.push_back( ptr);
		ptr->rId() = GeomOwner<D,UNIDIM>::mtabpGeom.size();
		return ptr->cId();
	}

	template <Dimension D>
	const GeomEntity<D,UNIDIM>*	cGEnt( const MGSize& id) const	
	{ 
		if ( id == 0)
			return NULL;
		else
			return GeomOwner<D,UNIDIM>::mtabpGeom[id-1]; 
	}

	template <Dimension D>
	const MGSize				Size() const					{ return GeomOwner<D,UNIDIM>::mtabpGeom.size(); }

	template <Dimension D>
	void RebuidTab( vector<MGSize>& tabid)		
	{ 
		vector< GeomEntity<D,UNIDIM>* >	tabgeom;
		MGSize id=0;
		for ( MGSize i=1; i<tabid.size(); ++i)
		{
			if ( tabid[i] != 0 )
			{
				tabid[i] = ++id;

				tabgeom.push_back( GeomOwner<D,UNIDIM>::mtabpGeom[i-1] );

				MGSize getid = tabgeom.size();

				if ( getid != id)
				{
					THROW_INTERNAL( "Crash inside GeomOwner<DIM_3D>::RebuidTab(...)");
				}
			}
			else
			{
				delete GeomOwner<D,UNIDIM>::mtabpGeom[i-1];
			}

			GeomOwner<D,UNIDIM>::mtabpGeom[i-1] = NULL;
		}

		GeomOwner<D,UNIDIM>::mtabpGeom.swap( tabgeom);
	}


protected:
	vector< GeomEntity<DIM,UNIDIM>* >	mtabpGeom;
};


//////////////////////////////////////////////////////////////////////
//	class GeomOwner + specialization for 0D
//////////////////////////////////////////////////////////////////////
template <Dimension UNIDIM>
class GeomOwner<DIM_0D, UNIDIM>
{
public:
	~GeomOwner()
	{
		for ( MGSize i=0; i<mtabpGeom.size(); ++i)
			delete mtabpGeom[i];
	}

protected:
	vector< GeomEntity<DIM_0D,UNIDIM>* >	mtabpGeom;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace GET = GETSpace;


#endif // __GEOMOWNER_H__
