#include "geotopomaster.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

template <Dimension DIM>
void GeoTopoMaster<DIM>::SyncNormals()
{
	//cout << this->cTopo().template Size<DIM_0D>() << endl;
	//cout << this->cTopo().template Size<DIM_1D>() << endl;
	//cout << this->cTopo().template Size<DIM_2D>() << endl;
	cout << this->cTopo().template Size<DIM>() << endl;


	typedef map< MGSize, bool >	tmapflag;
	typedef multimap< MGSize, MGSize >  tmapcon;

	tmapcon mapface;	// edge id -> face id
	tmapflag mapflag;	// face id -> done
	tmapflag maporient;	// face id -> orient

	for ( MGSize i=1; i <= this->cTopo().template Size<DIM>(); ++i)
	{
		cout << "  Entity id = " << i << endl;
		const TopoEntity< static_cast<Dimension>(DIM) >& tent = this->cTopo().template cTEnt< static_cast<Dimension>(DIM) >( i);

		for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
		{
			const TopoLoop	&loop = tent.cTab()[iloop].first;
			bool loopOr = tent.cTab()[iloop].second;

			for ( MGSize ifac=0; ifac<loop.size(); ++ifac)
			{
				const MGSize &idf = loop[ifac].first;
				bool facOr = loop[ifac].second;

				//facOr = ! ( facOr != loopOr);

				const TopoEntity< static_cast<Dimension>(DIM-1) >& tfac = this->cTopo().template cTEnt< static_cast<Dimension>(DIM-1) >( idf);

				maporient[ tfac.cId() ] = ! ( facOr != loopOr);
				mapflag[ tfac.cId() ] = false;

				for ( MGSize ifloop=0; ifloop<tfac.cTab().size(); ++ifloop)
				{
					const TopoLoop	&facloop = tfac.cTab()[ifloop].first;
					for ( MGSize iedg=0; iedg<facloop.size(); ++iedg)
					{
						const MGSize &ide = facloop[iedg].first;

						mapface.insert( tmapcon::value_type( ide, tfac.cId() ) );
					}
				}	

			}
		}	
	}

	for ( tmapcon::iterator itr = mapface.begin(); itr!=mapface.end(); ++itr)
		cout << itr->first << " --> " << itr->second << ", " << maporient[itr->second] << endl;


	MGSize idfirst = mapface.begin()->second;
	mapflag[idfirst] = true;

	vector<MGSize> tabstack;
	tabstack.push_back( idfirst);

	cout << "START id = " << idfirst << endl;

	while ( tabstack.size() )
	{
		MGSize idcur = tabstack.back();
		cout << "CUR id = " << idcur << endl;

		tabstack.pop_back();

		const TopoEntity< static_cast<Dimension>(DIM-1) >& tent = 
			this->cTopo().template cTEnt< static_cast<Dimension>(DIM-1) >( idcur);

		const GeomEntity< static_cast<Dimension>(DIM-1), DIM >* gent =
			this->cGeom().template cGEnt< static_cast<Dimension>(DIM-1) >( tent.cGeomId() );

		//Vect<DIM> vncur = 

		for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
		{
			const TopoLoop	&loop = tent.cTab()[iloop].first;

			for ( MGSize iedg=0; iedg<loop.size(); ++iedg)
			{
				const MGSize &ide = loop[iedg].first;
				pair<tmapcon::iterator,tmapcon::iterator> range = mapface.equal_range( ide);

				MGSize count = 0;
				for ( tmapcon::iterator itr=range.first; itr!=range.second; ++itr, ++count)
				{
					if ( ! mapflag[itr->second] )
					{
						const TopoEntity< static_cast<Dimension>(DIM-1) >& tcur = this->cTopo().template cTEnt< static_cast<Dimension>(DIM-1) >( itr->second);

						// check tent and tcur vns

						mapflag[itr->second] = true;
						tabstack.push_back( itr->second);
					}

				}
			}
		}

	}


}


template class GeoTopoMaster<DIM_2D>;
template class GeoTopoMaster<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
