#ifndef __GETCONST_H__
#define __GETCONST_H__


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

extern const char	GET_END[];
extern const char	GET_HEADER[];
extern const char	GET_GEOMETRY[];
extern const char	GET_TOPOLOGY[];

extern const char	GET_SEC_POINT[];
extern const char	GET_SEC_CURVE[];
extern const char	GET_SEC_SURFACE[];

extern const char	GET_SEC_VERTEX[];
extern const char	GET_SEC_EDGE[];
extern const char	GET_SEC_FACE[];
extern const char	GET_SEC_BREP[];


extern const char	GET_POINT[];
extern const char	GET_LINE[];
extern const char	GET_CIRCLE[];
extern const char	GET_ELLIPSE[];
extern const char	GET_SPLINE[];
extern const char	GET_NURBSCRV[];

extern const char	GET_PLANE[];
extern const char	GET_SPHERE[];
extern const char	GET_TORUS[];
extern const char	GET_NURBSSRF[];
extern const char	GET_CYLINDRICALSRF[];
extern const char	GET_CONICALSRF[];
extern const char	GET_EXTRUSIONSRF[];
extern const char	GET_REVOLUTIONSRF[];


extern const char	GET_VERTEX[];
extern const char	GET_EDGE[];
extern const char	GET_FACE[];
extern const char	GET_BREP[];


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace GET = GETSpace;

#endif // __GETCONST_H__
