#include "topoanalyzer.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

template <Dimension DIM>
void TopoAnalyzer<DIM>::CheckEdges()
{
}


template <>
void TopoAnalyzer<DIM_3D>::CheckEdges()
{
	cout << "----- TopoAnalyzer -----" << endl;


	vector<MGSize> tabec;
	tabec.resize( mTOwner.Size<DIM_1D>() + 1, 0);


	for ( MGSize i=1; i <= mTOwner.Size<DIM_2D>(); ++i)
	{
		cout << "  FACE Entity id = " << i << endl;

		// get topo face
		const TopoEntity<DIM_2D>& tent = mTOwner.cTEnt<DIM_2D>( i);

		// go through all boundary edegs
		for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
		{
			const GET::TopoLoop	&loop = tent.cTab()[iloop].first;

			for ( MGSize iedg=0; iedg<loop.size(); ++iedg)
			{
				const MGSize &ide = loop[iedg].first;

				tabec[ide] ++;
			}
		}
	}

	for ( MGSize i=1; i<tabec.size(); ++i)
	{
		if ( tabec[i] != 2)
			cout << "edge id = " << i << " : " << tabec[i] << endl;
	}

	cout << "----- TopoAnalyzer -----" << endl;

	//THROW_INTERNAL( "END");
}


template class TopoAnalyzer<DIM_2D>;
template class TopoAnalyzer<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
