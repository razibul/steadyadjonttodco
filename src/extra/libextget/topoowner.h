#ifndef __TOPOOWNER_H__
#define __TOPOOWNER_H__

#include "libcoresystem/mgdecl.h"
#include "topoentity.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
//	class TopoOwner
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class TopoOwner : public TopoOwner< static_cast<Dimension>( DIM-1 ) >
{
public:
	template <Dimension D>
	MGSize PushBack( const TopoEntity<D>& ent)		
	{ 
		TopoOwner<D>::mtabTopo.push_back( ent);
		MGSize ind = TopoOwner<D>::mtabTopo.size() - 1;
		TopoOwner<D>::mtabTopo[ind].rId() = TopoOwner<D>::mtabTopo.size();
		return TopoOwner<D>::mtabTopo[ind].cId();
	}

	template <Dimension D>
	const vector< TopoEntity<D> >&	cTab() const { return TopoOwner<D>::mtabTopo; }

	template <Dimension D>
	const TopoEntity<D>&	cTEnt( const MGSize& id) const { return TopoOwner<D>::mtabTopo[id-1]; }

	template <Dimension D>
	MGSize	Size() const { return TopoOwner<D>::mtabTopo.size(); }

protected:
	vector< TopoEntity<DIM> >	mtabTopo;
};


//////////////////////////////////////////////////////////////////////
//	class GeomOwner + specialization for 0D
//////////////////////////////////////////////////////////////////////
template <>
class TopoOwner<DIM_0D>
{
public:

protected:
	vector< TopoEntity<DIM_0D> >	mtabTopo;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace GET = GETSpace;


#endif // __TOPOOWNER_H__
