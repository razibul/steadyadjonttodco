#include "getcurve.h"

#include "libcoreio/store.h"
#include "libcorecommon/regexp.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//




//////////////////////////////////////////////////////////////////////
// class Line
//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void Line<DIM>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName() )
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\),\\((.*)\\)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		vector<MGFloat> tab;

		IO::ReadTabFloat( tab, rex.GetSubString( 1) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mOrigin.rX(k) = tab[k];

		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 2) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mTnVect.rX(k) = tab[k];

		sparams = rex.GetSubString( 3);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

template <Dimension DIM>
void Line<DIM>::WriteDef( ostream& of) const
{
	of << setprecision(16); 
	of << ClassName() << "( ";

	of << "( "<< mOrigin.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mOrigin.cX(k);

	of << " ), ( " << mTnVect.cX(0);;
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mTnVect.cX(k);
	of << " )";

	of << " )";
}


template <Dimension DIM>
typename Line<DIM>::GVect Line<DIM>::FindParam( const UVect& vct) const
{
	MGFloat s = mTnVect*(vct - mOrigin);
	s /= mTnVect*mTnVect;
	
	return Vect1D( s);
}

template <Dimension DIM>
void Line<DIM>::GetCoord( UVect& vret, const GVect& vct) const
{
	vret =  mOrigin + vct.cX() * mTnVect;
}

template <Dimension DIM>
void Line<DIM>::GetFstDeriv( UVect tab[DIM_1D], const GVect& vct) const
{
	tab[0] =  mTnVect;
}

template <Dimension DIM>
void Line<DIM>::GetSndDeriv( UVect tab[SIZE], const GVect& vct) const
{
	tab[0] =  UVect( 0.0);
}


template <Dimension DIM>
void Line<DIM>::Eval( UVect& vret, UVect tab1[DIM], UVect tab2[SIZE], const GVect& vct) const
{
	vret =  mOrigin + vct.cX() * mTnVect;
	tab1[0] =  mTnVect;
	tab2[0] =  UVect( 0.0);
}


template class Line<DIM_2D>;
template class Line<DIM_3D>;


//////////////////////////////////////////////////////////////////////
// class Circle
//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void Circle<DIM>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName() )
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\),\\((.*)\\),\\((.*)\\),([-0-9\\.eE]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;
		vector<MGFloat> tab;

		// circle center
		IO::ReadTabFloat( tab, rex.GetSubString( 1) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mCenter.rX(k) = tab[k];

		// OX axis
		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 2) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mAxisOX.rX(k) = tab[k];

		// OY axis
		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 3) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mAxisOY.rX(k) = tab[k];

		// radius
		is.clear();
		is.str( rex.GetSubString( 4) );
		is >> mRadius;


		sparams = rex.GetSubString( 5);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

template <Dimension DIM>
void Circle<DIM>::WriteDef( ostream& of) const
{
	of << setprecision(16); 
	of << ClassName() << "( ";

	of << "( " << mCenter.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mCenter.cX(k);

	of << " ), ( " << mAxisOX.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mAxisOX.cX(k);

	of << " ), ( " << mAxisOY.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mAxisOY.cX(k);

	of << " ), " << mRadius;

	of << " )";
}



template <Dimension DIM>
typename Circle<DIM>::GVect Circle<DIM>::FindParam( const UVect& vct) const
{
	UVect v1 = vct - mCenter;
	MGFloat s = atan2( v1*mAxisOY, v1*mAxisOX );	
	return GVect( s);

}

template <Dimension DIM>
void Circle<DIM>::GetCoord( UVect& vret, const GVect& vct) const
{
	vret = mAxisOX * (mRadius * ::cos( vct.cX())) + mAxisOY * (mRadius * ::sin( vct.cX()))  + mCenter;
}

template <Dimension DIM>
void Circle<DIM>::GetFstDeriv( UVect tab[DIM_1D], const GVect& vct) const
{
	tab[0] = mAxisOX * (-mRadius * ::sin( vct.cX())) + mAxisOY * (mRadius * ::cos( vct.cX()));
}

template <Dimension DIM>
void Circle<DIM>::GetSndDeriv( UVect tab[SIZE], const GVect& vct) const
{
	tab[0] =  mAxisOX * (-mRadius * ::cos( vct.cX())) + mAxisOY * (-mRadius * ::sin( vct.cX()));
}

template <Dimension DIM>
void Circle<DIM>::Eval( UVect& vret, UVect tab1[DIM], UVect tab2[SIZE], const GVect& vct) const
{
	vret = mAxisOX * (mRadius * ::cos( vct.cX())) + mAxisOY * (mRadius * ::sin( vct.cX()))  + mCenter;
	tab1[0] = mAxisOX * (-mRadius * ::sin( vct.cX())) + mAxisOY * (mRadius * ::cos( vct.cX()));
	tab2[0] =  mAxisOX * (-mRadius * ::cos( vct.cX())) + mAxisOY * (-mRadius * ::sin( vct.cX()));
}


template class Circle<DIM_2D>;
template class Circle<DIM_3D>;



//////////////////////////////////////////////////////////////////////
// class Ellipse
//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void Ellipse<DIM>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName() )
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\),\\((.*)\\),\\((.*)\\),([-0-9\\.eE]*),([-0-9\\.eE]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;
		vector<MGFloat> tab;

		// circle center
		IO::ReadTabFloat( tab, rex.GetSubString( 1) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mCenter.rX(k) = tab[k];

		// OX axis
		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 2) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mAxisOX.rX(k) = tab[k];

		// OY axis
		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 3) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mAxisOY.rX(k) = tab[k];

		// radius
		is.clear();
		is.str( rex.GetSubString( 4) );
		is >> mSemiAxisOX;

		// radius
		is.clear();
		is.str( rex.GetSubString( 5) );
		is >> mSemiAxisOY;


		sparams = rex.GetSubString( 6);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

template <Dimension DIM>
void Ellipse<DIM>::WriteDef( ostream& of) const
{
	of << setprecision(16); 
	of << ClassName() << "( ";

	of << "( " << mCenter.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mCenter.cX(k);

	of << " ), ( " << mAxisOX.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mAxisOX.cX(k);

	of << " ), ( " << mAxisOY.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mAxisOY.cX(k);

	of << " ), " << mSemiAxisOX;
	of << ", " << mSemiAxisOY;

	of << " )";
}



template <Dimension DIM>
typename Ellipse<DIM>::GVect Ellipse<DIM>::FindParam( const UVect& vct) const
{
	UVect v1 = vct - mCenter;
	MGFloat s = atan2( v1*mAxisOY/mSemiAxisOY , v1*mAxisOX/mSemiAxisOX );	
	return GVect( s);

}

template <Dimension DIM>
void Ellipse<DIM>::GetCoord( UVect& vret, const GVect& vct) const
{
	vret = mAxisOX * (mSemiAxisOX * ::cos( vct.cX())) + mAxisOY * (mSemiAxisOY * ::sin( vct.cX()))  + mCenter;
}

template <Dimension DIM>
void Ellipse<DIM>::GetFstDeriv( UVect tab[DIM_1D], const GVect& vct) const
{
	tab[0] = mAxisOX * (-mSemiAxisOX * ::sin( vct.cX())) + mAxisOY * (mSemiAxisOY * ::cos( vct.cX()));
}

template <Dimension DIM>
void Ellipse<DIM>::GetSndDeriv( UVect tab[SIZE], const GVect& vct) const
{
	tab[0] =  mAxisOX * (-mSemiAxisOX * ::cos( vct.cX())) + mAxisOY * (-mSemiAxisOY * ::sin( vct.cX()));
}

template <Dimension DIM>
void Ellipse<DIM>::Eval( UVect& vret, UVect tab1[DIM], UVect tab2[SIZE], const GVect& vct) const
{
	vret = mAxisOX * (mSemiAxisOX * ::cos( vct.cX())) + mAxisOY * (mSemiAxisOY * ::sin( vct.cX()))  + mCenter;
	tab1[0] = mAxisOX * (-mSemiAxisOX * ::sin( vct.cX())) + mAxisOY * (mSemiAxisOY * ::cos( vct.cX()));
	tab2[0] =  mAxisOX * (-mSemiAxisOX * ::cos( vct.cX())) + mAxisOY * (-mSemiAxisOY * ::sin( vct.cX()));
}


template class Ellipse<DIM_2D>;
template class Ellipse<DIM_3D>;



//////////////////////////////////////////////////////////////////////
// class Spline
//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void Spline<DIM>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName() )
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;
	rex.InitPattern( "^\\(([^\\(\\)]*)\\)(.*)$" );
	rex.SetString( sparams);

	MGSize count = 0;

	while ( rex.IsOk() )
	{
		vector<MGFloat> tab;
		UVect vct;

		//cout << rex.GetSubString( 1) << endl;

		IO::ReadTabFloat( tab, rex.GetSubString( 1) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			vct.rX(k) = tab[k];

		mtabS.push_back( count);
		for ( MGSize k=0; k<DIM; ++k)
			mtabX[k].push_back( vct.cX( k) );

		++count;
		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );

		rex.InitPattern( "^\\(([^\\(\\)]*)\\)(.*)$" );
		rex.SetString( sparams);
	}

	if ( count == 0)
		THROW_INTERNAL( "unrecognized definition" );

	mNum = count;

	// finish initialization
	//MGFloat	w[5] = { 0.236927, 0.478629, 0.568889, 0.478629, 0.236927};
	//MGFloat	px[5] = { -0.906180, -0.538469, 0.0, 0.538469, 0.906180};
	static MGFloat	w[5] = 
	{	(322.0 - 13.0 * ::sqrt( 70.0)) / 900.0, 
		(322.0 + 13.0 * ::sqrt( 70.0)) / 900.0, 
		128.0 / 255.0,
		(322.0 + 13.0 * ::sqrt( 70.0)) / 900.0, 
		(322.0 - 13.0 * ::sqrt( 70.0)) / 900.0
	};

	static MGFloat	px[5] = 
	{ 
		- ::sqrt( 245.0 + 14.0 * ::sqrt( 70.0)) / 21.0, 
		- ::sqrt( 245.0 - 14.0 * ::sqrt( 70.0)) / 21.0, 
		0.0, 
		::sqrt( 245.0 - 14.0 * ::sqrt( 70.0)) / 21.0, 
		::sqrt( 245.0 + 14.0 * ::sqrt( 70.0)) / 21.0, 
	};

	MGFloat	xt[DIM], xt1[DIM], xt2[DIM];
	Vect3D	d1, d2;
	MGFloatArr	tab;

	MGFloat	SL, dSL;
	MGFloat	ss, ds, s0;

	tab.resize( mNum);

	for ( MGSize k=0; k<DIM; ++k)
	{
		mFun[k].UsePoints( mNum, &mtabS, &mtabX[k]);
		mFun[k].CalcCoeff();
	}
	//return;


	// reparametrization of the curve

	for ( MGSize iter=0; iter<1; ++iter)	// TODO :: remove magic number
	{
		tab[0] = 0.0;
		for ( MGSize i=0; i<mNum-1; ++i)
		{
			ds = 0.5 * (mtabS[i+1] - mtabS[i]);
			s0 = 0.5 * (mtabS[i+1] + mtabS[i]);

			SL = 0.0;
			for ( MGSize j=0; j<5; j++)
			{
				ss = s0 + px[j]*ds;
				for ( MGSize k=0; k<DIM; ++k)
					mFun[k].Diff( ss, xt[k], xt1[k], xt2[k]);

				//				dSL = sqrt( xt1*xt1 + yt1*yt1);
				dSL = 0.0;
				for ( MGSize k=0; k<DIM; ++k)
					dSL += xt1[k] * xt1[k];
				dSL = w[j] * ::sqrt( dSL);

				SL += dSL;
			}
			SL = ds*SL;
			tab[i+1] = tab[i] + SL;
		}

		mtabS.swap( tab);

		for ( MGSize k=0; k<DIM; ++k)
		{
			mFun[k].UsePoints( mNum, &mtabS, &mtabX[k]);
			mFun[k].CalcCoeff();
		}
	}

}


template <Dimension DIM>
void Spline<DIM>::WriteDef( ostream& of) const
{
	of << setprecision(16); 
	of << ClassName() << "( ";

	ASSERT( mNum == mtabX[0].size() );

	for ( MGSize i=0; i<mNum; ++i)
	{
		of << "( " << mtabX[0][i];
		for ( MGSize k=1; k<DIM; ++k)
			of << ", " << mtabX[k][i];
		of << " )";

		if ( i != mNum-1)
			of << ", ";
	}
	of << " )";
}



template <Dimension DIM>
typename Spline<DIM>::GVect Spline<DIM>::FindParam( const UVect& vct) const
{
	MGFloat	d;
	UVect	x;

	for ( MGSize i=0; i<mNum; i++)
	{
		for ( MGSize k=0; k<DIM; ++k)
			x.rX(k) = mFun[k].FunTb( i);

		x = x - vct;
		d = x.module();
		
		
		if ( d < ZERO )
			return Vect1D( mtabS[i] );
	}
	
	THROW_INTERNAL( "Spline: Parameter not found");

	return Vect1D( 0);
}


template <Dimension DIM>
void Spline<DIM>::GetCoord( UVect& vret, const GVect& vct) const
{
	for ( MGSize k=0; k<DIM; ++k)
		vret.rX(k) = mFun[k].Fun( vct.cX());
}


template <Dimension DIM>
void Spline<DIM>::GetFstDeriv( UVect tab[DIM_1D], const GVect& vct) const
{
	MGFloat	d, d1, d2;
	
	for ( MGSize k=0; k<DIM; ++k)
	{
		mFun[k].Diff( vct.cX(), d, d1, d2);
		tab[0].rX(k) = d1;
	}
}

template <Dimension DIM>
void Spline<DIM>::GetSndDeriv( UVect tab[SIZE], const GVect& vct) const
{
	MGFloat	d, d1, d2;
	
	for ( MGSize k=0; k<DIM; ++k)
	{
		mFun[k].Diff( vct.cX(), d, d1, d2);
		tab[0].rX(k) = d2;
	}
}

template <Dimension DIM>
void Spline<DIM>::Eval( UVect& vret, UVect tab1[DIM], UVect tab2[SIZE], const GVect& vct) const
{
	MGFloat	d, d1, d2;
	
	for ( MGSize k=0; k<DIM; ++k)
	{
		mFun[k].Diff( vct.cX(), d, d1, d2);
		vret.rX(k) = d;
		tab1[0].rX(k) = d1;
		tab2[0].rX(k) = d2;
	}
}



template class Spline<DIM_2D>;
template class Spline<DIM_3D>;


//////////////////////////////////////////////////////////////////////
// class NurbsCrv
//////////////////////////////////////////////////////////////////////


template <Dimension DIM>
void NurbsCrv<DIM>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName() )
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;

	rex.InitPattern( "^(T|F),(.*)$");
	rex.SetString( sparams);

	bool bor = false;
	if ( rex.IsOk() )	// orientation flag
	{
		if ( rex.GetSubString(1) == "T")
			bor = true;
		else 
		if ( rex.GetSubString(1) == "F")
			bor = false;
		else
			THROW_INTERNAL( "unrecognized orientation");

		sparams = rex.GetSubString(2);
	}

	rex.InitPattern( "^([0-9]*),\\(([^\\(\\)]*)\\),\\((.*)\\)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		// degree
		MGSize deg;
		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> deg;

		// knot vector
		vector<MGFloat> tabU;
		IO::ReadTabFloat( tabU, rex.GetSubString( 2) );

		// control points
		sparams = rex.GetSubString( 3);

		typedef Vect<static_cast<Dimension>(DIM+1)> TypeVect;

		vector<MGFloat> tab;
		vector<TypeVect> tabCP;

		IO::ReadTabVect<static_cast<Dimension>(DIM+1)>( tabCP, sparams );

		sparams = "";

		if ( deg <= 0 || tabU.size() == 0 || tabCP.size() == 0)
			THROW_INTERNAL( "unrecognized definition" );

		// finish initialization
		mNurbsCrv.Init( deg, tabU, tabCP);
		//mNurbsCrv.ExportCrvTEC( "nurbscrv.plt");
		//THROW_INTERNAL( "");
	}
	else
	{
		vector<UVect>	tabX;

		IO::ReadTabVect<DIM>( tabX, sparams );

		sparams = "";

		if ( tabX.size() == 0)
			THROW_INTERNAL( "unrecognized definition" );

		// finish initialization
		mNurbsCrv.GlobalInterpol( tabX, 3);

		if ( tabX[0] == tabX[tabX.size()-1] )
		{
			mbPeriodic = true;
			mPeriod = mNurbsCrv.cKnotVector().UMax() - mNurbsCrv.cKnotVector().UMin();
		}
	}

	if ( bor ) 
	{
		mbPeriodic = true;
		mPeriod = mNurbsCrv.cKnotVector().UMax() - mNurbsCrv.cKnotVector().UMin();
	}


}

template <Dimension DIM>
void NurbsCrv<DIM>::WriteDef( ostream& of) const
{
	of << setprecision(16); 
	of << ClassName() << "( ";

	if ( mbPeriodic == true )
		of << "T,";
	else
		of << "F,";

	of << mNurbsCrv.cUdeg() << ", ( ";
	of << mNurbsCrv.cKnot(0);
	for ( MGSize i=1; i<mNurbsCrv.KnotTabSize(); ++i)
		of << ", " << mNurbsCrv.cKnot(i);

	of << " ), ( ";
	for ( MGSize i=0; i<mNurbsCrv.CPointTabSize(); ++i)
	{
		UVect vct = mNurbsCrv.cCPoint(i).ProjectW();
		of << "( " << vct.cX(0);
		for ( MGSize k=1; k<DIM; ++k)
			of << ", " << vct.cX(k);
		of << ", " << mNurbsCrv.cCPoint(i).cWeight();
		of << " )";

		if ( i != mNurbsCrv.CPointTabSize()-1 )
			of << ", ";
	}

	of << " )";
	of << " )";
}


template <Dimension DIM>
typename NurbsCrv<DIM>::GVect NurbsCrv<DIM>::FindParam( const UVect& vct) const
{
	MGFloat t = mNurbsCrv.PointProject( vct, 10 );
	//MGFloat t = mNurbsCrv.PointProject( vct, 40 );
	return Vect1D( t);
}



template <Dimension DIM>
void NurbsCrv<DIM>::GetCoord( UVect& vret, const GVect& vct) const
{
	GVect vtmp = vct;

	if ( IsPeriodic(0) )
		if ( vtmp.cX() > mNurbsCrv.cKnotVector().UMax() ) 
			vtmp -= mPeriod;
		if ( vtmp.cX() < mNurbsCrv.cKnotVector().UMin() ) 
			vtmp.rX() += mPeriod;

	mNurbsCrv.CalcPoint( vtmp, vret);

	//mNurbsCrv.CalcPoint( vct, vret);
}


template <Dimension DIM>
void NurbsCrv<DIM>::GetFstDeriv( UVect tab[DIM_1D], const GVect& vct) const
{
	GVect vtmp = vct;

	if ( IsPeriodic(0) )
		if ( vtmp.cX() > mNurbsCrv.cKnotVector().UMax() ) 
			vtmp -= mPeriod;
		if ( vtmp.cX() < mNurbsCrv.cKnotVector().UMin() ) 
			vtmp.rX() += mPeriod;

	vector<UVect>	tabD;
	mNurbsCrv.CalcDerivs( vtmp, 1, tabD);
	tab[0] = tabD[1];


	//vector<UVect>	tabD;
	//mNurbsCrv.CalcDerivs( vct, 1, tabD);
	//tab[0] = tabD[1];
}

template <Dimension DIM>
void NurbsCrv<DIM>::GetSndDeriv( UVect tab[SIZE], const GVect& vct) const
{
	GVect vtmp = vct;

	if ( IsPeriodic(0) )
		if ( vtmp.cX() > mNurbsCrv.cKnotVector().UMax() ) 
			vtmp -= mPeriod;
		if ( vtmp.cX() < mNurbsCrv.cKnotVector().UMin() ) 
			vtmp.rX() += mPeriod;

	vector<UVect>	tabD;
	mNurbsCrv.CalcDerivs( vtmp, 2, tabD);
	tab[0] = tabD[2];

	//vector<UVect>	tabD;
	//mNurbsCrv.CalcDerivs( vct, 2, tabD);
	//tab[0] = tabD[2];
}

template <Dimension DIM>
void NurbsCrv<DIM>::Eval( UVect& vret, UVect tab1[DIM], UVect tab2[SIZE], const GVect& vct) const
{
	GVect vtmp = vct;

	if ( IsPeriodic(0) )
		if ( vtmp.cX() > mNurbsCrv.cKnotVector().UMax() ) 
			vtmp -= mPeriod;
		if ( vtmp.cX() < mNurbsCrv.cKnotVector().UMin() ) 
			vtmp.rX() += mPeriod;

	vector<UVect>	tabD;
	mNurbsCrv.CalcDerivs( vtmp, 2, tabD);
	vret    = tabD[0];
	tab1[0] = tabD[1];
	tab2[0] = tabD[2];

	//vector<UVect>	tabD;
	//mNurbsCrv.CalcDerivs( vct, 2, tabD);
	//vret    = tabD[0];
	//tab1[0] = tabD[1];
	//tab2[0] = tabD[2];
}

template <Dimension DIM>
void NurbsCrv<DIM>::ExportTEC( ostream& os) const
{
	mNurbsCrv.ExportCrvTEC( os, 20);
}


template class NurbsCrv<DIM_2D>;
template class NurbsCrv<DIM_3D>;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

