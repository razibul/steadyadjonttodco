#include "getsurface.h"

#include "libcoreio/store.h"
#include "libcorecommon/regexp.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//



//////////////////////////////////////////////////////////////////////
// class Plane
//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void Plane<DIM>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName())
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\),\\((.*)\\),\\((.*)\\)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		vector<MGFloat> tab;

		IO::ReadTabFloat( tab, rex.GetSubString( 1) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mOrigin.rX(k) = tab[k];

		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 2) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mvOX.rX(k) = tab[k];

		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 3) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mvOY.rX(k) = tab[k];

		sparams = rex.GetSubString( 4);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

template <Dimension DIM>
void Plane<DIM>::WriteDef( ostream& of) const
{
	of << setprecision(16); 
	of << ClassName() << "( ";

	of << "( " << mOrigin.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mOrigin.cX(k);

	of << " ), ( " << mvOX.cX(0);;
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mvOX.cX(k);
	of << " )";

	of << " ), ( " << mvOY.cX(0);;
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mvOY.cX(k);
	of << " )";

	of << " )";
}


template <Dimension DIM>
typename Plane<DIM>::GVect Plane<DIM>::FindParam( const UVect& vct) const
{
	return Vect2D( (vct - mOrigin )*mvOX, (vct - mOrigin )*mvOY );
}

template <Dimension DIM>
void Plane<DIM>::GetCoord( UVect& vret, const GVect& vct) const
{
	vret =  mvOX*vct.cX() + mvOY*vct.cY() + mOrigin;
}

template <Dimension DIM>
void Plane<DIM>::GetFstDeriv( UVect tab[DIM_2D], const GVect& vct) const
{
	tab[0] =  mvOX;
	tab[1] =  mvOY;
}

template <Dimension DIM>
void Plane<DIM>::GetSndDeriv( UVect tab[SIZE], const GVect& vct) const
{
	tab[0] =  UVect( 0.0);
	tab[1] =  UVect( 0.0);
	tab[2] =  UVect( 0.0);
}

template <Dimension DIM>
void Plane<DIM>::Eval( UVect& vret, UVect tab1[DIM_2D], UVect tab2[SIZE], const GVect& vct) const
{
	vret =  mvOX*vct.cX() + mvOY*vct.cY() + mOrigin;

	tab1[0] =  mvOX;
	tab1[1] =  mvOY;

	tab2[0] =  UVect( 0.0);
	tab2[1] =  UVect( 0.0);
	tab2[2] =  UVect( 0.0);
}


template class Plane<DIM_3D>;





//////////////////////////////////////////////////////////////////////
// class Sphere
//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void Sphere<DIM>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName())
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\),\\((.*)\\),\\((.*)\\),([-0-9\\.eE]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;
		vector<MGFloat> tab;

		// sphere center
		IO::ReadTabFloat( tab, rex.GetSubString( 1) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mCenter.rX(k) = tab[k];

		// OX axis
		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 2) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mAxisOX.rX(k) = tab[k];

		// OY axis
		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 3) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mAxisOY.rX(k) = tab[k];

		// radius
		is.clear();
		is.str( rex.GetSubString( 4) );
		is >> mRadius;

		
		sparams = rex.GetSubString( 5);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

template <Dimension DIM>
void Sphere<DIM>::WriteDef( ostream& of) const
{
	of << setprecision(16); 
	of << ClassName() << "( ";

	of << "( " << mCenter.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mCenter.cX(k);

	of << " ), ( " << mAxisOX.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mAxisOX.cX(k);

	of << " ), ( " << mAxisOY.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mAxisOY.cX(k);

	of << " ), " << mRadius;

	of << " )";
}


template <Dimension DIM>
typename Sphere<DIM>::GVect Sphere<DIM>::FindParam( const UVect& vct) const
{
    Vect2D	par;	
    UVect	dvct = vct - mCenter;
	UVect	axisOZ = mAxisOX % mAxisOY;
	
   // par.rY()	= asin( dvct * axisOZ / mRadius );
	par.rY() = asin(dvct * axisOZ / dvct.module() );
    par.rX()	= atan2( dvct * mAxisOY , dvct * mAxisOX  );
 
    return par;
}

template <Dimension DIM>
void Sphere<DIM>::GetCoord( UVect& vret, const GVect& vct) const
{
	MGFloat	v = vct.cY();
	UVect	axisOZ = mAxisOX % mAxisOY;
	vret = mCenter + mRadius*( cos(vct.cY())*( cos(vct.cX())*mAxisOX + sin(vct.cX())*mAxisOY ) + sin(vct.cY())*axisOZ );
}

template <Dimension DIM>
void Sphere<DIM>::GetFstDeriv( UVect tab[DIM_2D], const GVect& vct) const
{
	UVect	axisOZ = mAxisOX % mAxisOY;
	tab[0] =  mRadius * cos( vct.cY() )*( -sin( vct.cX() ) * mAxisOX + cos( vct.cX() )*mAxisOY );
	tab[1] =  mRadius * ( -sin( vct.cY() )*( cos( vct.cX() )*mAxisOX + sin( vct.cX() )*mAxisOY ) + cos( vct.cY() )*axisOZ );
}

template <Dimension DIM>
void Sphere<DIM>::GetSndDeriv( UVect tab[SIZE], const GVect& vct) const
{
	// TODO :: do this !!!
	tab[0] =  UVect( 0.0);
	tab[1] =  UVect( 0.0);
	tab[2] =  UVect( 0.0);
}

template <Dimension DIM>
void Sphere<DIM>::Eval( UVect& vret, UVect tab1[DIM_2D], UVect tab2[SIZE], const GVect& vct) const
{
	UVect	axisOZ = mAxisOX % mAxisOY;

	vret = mCenter + mRadius*( cos(vct.cY())*( cos(vct.cX())*mAxisOX + sin(vct.cX())*mAxisOY ) + sin(vct.cY())*axisOZ );

	tab1[0] =  mRadius * cos( vct.cY() )*( -sin( vct.cX() ) * mAxisOX + cos( vct.cX() )*mAxisOY );
	tab1[1] =  mRadius * ( -sin( vct.cY() )*( cos( vct.cX() )*mAxisOX + sin( vct.cX() )*mAxisOY ) + cos( vct.cY() )*axisOZ );

	tab2[0] =  UVect( 0.0);
	tab2[1] =  UVect( 0.0);
	tab2[2] =  UVect( 0.0);
}


template class Sphere<DIM_3D>;




//////////////////////////////////////////////////////////////////////
// class Torus
//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void Torus<DIM>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName())
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\),\\((.*)\\),\\((.*)\\),([-0-9\\.eE]*),([-0-9\\.eE]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;
		vector<MGFloat> tab;

		// sphere center
		IO::ReadTabFloat( tab, rex.GetSubString( 1) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mCenter.rX(k) = tab[k];

		// OX axis
		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 2) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mAxisOX.rX(k) = tab[k];

		// OY axis
		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 3) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mAxisOY.rX(k) = tab[k];

		// radius
		is.clear();
		is.str( rex.GetSubString( 4) );
		is >> mRadius1;

		// radius
		is.clear();
		is.str( rex.GetSubString( 5) );
		is >> mRadius2;

		
		sparams = rex.GetSubString( 6);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

template <Dimension DIM>
void Torus<DIM>::WriteDef( ostream& of) const
{
	of << setprecision(16); 
	of << ClassName() << "( ";

	of << "( " << mCenter.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mCenter.cX(k);

	of << " ), ( " << mAxisOX.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mAxisOX.cX(k);

	of << " ), ( " << mAxisOY.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mAxisOY.cX(k);

	of << " ), " << mRadius1;
	of << " , "  << mRadius2;

	of << " )";
}


template <Dimension DIM>
typename Torus<DIM>::GVect Torus<DIM>::FindParam( const UVect& vct) const
{
    Vect2D	par;	
    UVect	dvct = vct - mCenter;
	UVect	axisOZ = mAxisOX % mAxisOY;
	
	MGFloat u = atan2( dvct*mAxisOY, dvct*mAxisOX );

	UVect veu = cos(u)*mAxisOX + sin(u)*mAxisOY;

	UVect vloc = dvct - mRadius1*veu;
	MGFloat v = atan2( vloc*axisOZ, vloc*veu );

    return Vect2D(u,v);
}

template <Dimension DIM>
void Torus<DIM>::GetCoord( UVect& vret, const GVect& vct) const
{
	//(u,v) = C    + (R + r cos v)((cos u)x + (sin u)y) + r(sin v)z

	MGFloat	v = vct.cY();
	UVect	axisOZ = mAxisOX % mAxisOY;
	vret = mCenter + (mRadius1 + mRadius2*cos(vct.cY()) ) * ( cos(vct.cX())*mAxisOX + sin(vct.cX())*mAxisOY )  + mRadius2*sin(vct.cY())*axisOZ;
}

template <Dimension DIM>
void Torus<DIM>::GetFstDeriv( UVect tab[DIM_2D], const GVect& vct) const
{
	UVect	axisOZ = mAxisOX % mAxisOY;

	tab[0] =  (mRadius1 + mRadius2*cos( vct.cY() ) ) * ( -sin(vct.cX()) * mAxisOX + cos(vct.cX()) * mAxisOY );
	tab[1] =  -mRadius2 * sin( vct.cY() ) * ( cos( vct.cX() ) * mAxisOX + sin( vct.cX() ) * mAxisOY ) + mRadius2*cos( vct.cY() )*axisOZ;
}

template <Dimension DIM>
void Torus<DIM>::GetSndDeriv( UVect tab[SIZE], const GVect& vct) const
{
	// TODO :: do this !!!
	tab[0] =  UVect( 0.0);
	tab[1] =  UVect( 0.0);
	tab[2] =  UVect( 0.0);
}

template <Dimension DIM>
void Torus<DIM>::Eval( UVect& vret, UVect tab1[DIM_2D], UVect tab2[SIZE], const GVect& vct) const
{
	UVect	axisOZ = mAxisOX % mAxisOY;

	vret = mCenter + mRadius1*( cos(vct.cY())*( cos(vct.cX())*mAxisOX + sin(vct.cX())*mAxisOY ) + sin(vct.cY())*axisOZ );

	tab1[0] =  (mRadius1 + mRadius2*cos( vct.cY() ) ) * ( -sin(vct.cX()) * mAxisOX + cos(vct.cX()) * mAxisOY );
	tab1[1] =  -mRadius2 * sin( vct.cY() ) * ( cos( vct.cX() ) * mAxisOX + sin( vct.cX() ) * mAxisOY ) + mRadius2*cos( vct.cY() )*axisOZ;

	tab2[0] =  UVect( 0.0);
	tab2[1] =  UVect( 0.0);
	tab2[2] =  UVect( 0.0);
}


template class Torus<DIM_3D>;


//////////////////////////////////////////////////////////////////////
// class NurbsSrf
//////////////////////////////////////////////////////////////////////




template <Dimension DIM>
void NurbsSrf<DIM>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName())
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}


	RegExp	rex;

	rex.InitPattern( "^(T|F),(T|F),(.*)$");
	rex.SetString( sparams);

	bool borU = false;
	bool borV = false;
	if ( rex.IsOk() )	// orientation flag
	{
		if ( rex.GetSubString(1) == "T")
			borU = true;
		else 
		if ( rex.GetSubString(1) == "F")
			borU = false;
		else
			THROW_INTERNAL( "unrecognized orientation");

		if ( rex.GetSubString(2) == "T")
			borV = true;
		else 
		if ( rex.GetSubString(2) == "F")
			borV = false;
		else
			THROW_INTERNAL( "unrecognized orientation");

		sparams = rex.GetSubString(3);
	}


	rex.InitPattern( "^([0-9]*),([0-9]*),\\(([^\\(\\)]*)\\),\\(([^\\(\\)]*)\\),\\((.*)\\)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		// degree U
		MGSize degU;
		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> degU;

		// degree V
		MGSize degV;
		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> degV;

		// knot vector U
		vector<MGFloat> tabU;
		IO::ReadTabFloat( tabU, rex.GetSubString( 3) );

		// knot vector V
		vector<MGFloat> tabV;
		IO::ReadTabFloat( tabV, rex.GetSubString( 4) );

		// control points
		vector< vector< Vect< static_cast<Dimension>(DIM+1)> > > mtxCP;
		IO::ReadMtxVect<static_cast<Dimension>(DIM+1)>( mtxCP, rex.GetSubString( 5) );

		sparams = "";

		if ( degU <= 0 || degV <= 0 || tabU.size() == 0 || tabV.size() == 0 || mtxCP.size() == 0)
			THROW_INTERNAL( "unrecognized definition" );

		// finish initialization
		mNurbsSrf.Init( degU, degV, tabU, tabV, mtxCP);
		mNurbsSrf.ExportSrfTEC( "_surf.dat", 30, 30);
		//mNurbsSrf.ExportSrfTEC( "_surf.dat", 15, 15);

	}
	else
	{
		vector< vector<UVect> >	mtxX;

		IO::ReadMtxVect<DIM>( mtxX, sparams );

		sparams = "";

		if ( mtxX.size() == 0)
			THROW_INTERNAL( "unrecognized definition" );

		// finish initialization
		//mNurbsSrf.GlobalInterpol( mtxX, 3, 3);
	}


	if ( borU ) 
	{
		mbPeriodic[0] = true;
		mPeriod[0] = mNurbsSrf.cKnotVectorU().UMax() - mNurbsSrf.cKnotVectorU().UMin();
	}
	else
	{
		mbPeriodic[0] = false;
		mPeriod[0] = 0.0;
	}

	if ( borV ) 
	{
		mbPeriodic[1] = true;
		mPeriod[1] = mNurbsSrf.cKnotVectorV().UMax() - mNurbsSrf.cKnotVectorV().UMin();
	}
	else
	{
		mbPeriodic[1] = false;
		mPeriod[1] = 0.0;
	}

}

template <Dimension DIM>
void NurbsSrf<DIM>::WriteDef( ostream& of) const
{
	of << setprecision(16); 
	of << ClassName() << "( ";

	if ( mbPeriodic[0] == true )
		of << "T,";
	else
		of << "F,";

	if ( mbPeriodic[1] == true )
		of << "T,";
	else
		of << "F,";


	of << mNurbsSrf.cUdeg() << ", " << mNurbsSrf.cVdeg() << ", ( ";
	of << mNurbsSrf.cKnotU(0);
	for ( MGSize i=1; i<mNurbsSrf.KnotUTabSize(); ++i)
		of << ", " << mNurbsSrf.cKnotU(i);
	of << " ), ( ";
	of << mNurbsSrf.cKnotV(0);
	for ( MGSize i=1; i<mNurbsSrf.KnotVTabSize(); ++i)
		of << ", " << mNurbsSrf.cKnotV(i);
	of << " ), ( ";

	for ( MGSize i=0; i<mNurbsSrf.CPointMtxUSize(); ++i)
	{
		of << "( ";
		for ( MGSize j=0; j<mNurbsSrf.CPointMtxVSize(); ++j)
		{
			UVect vct = mNurbsSrf.cCPoint(i,j).ProjectW();
			of << "( " << vct.cX(0);
			for ( MGSize k=1; k<DIM; ++k)
				of << ", " << vct.cX(k);
			of << ", " << mNurbsSrf.cCPoint(i,j).cWeight();
			of << " )";

			if ( j != mNurbsSrf.CPointMtxVSize()-1)
				of << ", ";
		}
		of << " )";
		if ( i != mNurbsSrf.CPointMtxUSize()-1)
			of << ", ";
	}
	of << " )";
	of << " )";
}


template <Dimension DIM>
typename NurbsSrf<DIM>::GVect NurbsSrf<DIM>::FindParam( const UVect& vct) const
{
	const MGSize ntries = 5;
	Vect2D vret;

	//cout << "--------------------------\n\t\t\t\t";
	//vct.Write();

	MGSize n = 1;
	for ( MGSize i=0; i<ntries; ++i)
	{
		vret = mNurbsSrf.FindProjectApprox( vct, n, n );
		//vret = Vect2D( 0.5, 0.5);

		if ( mNurbsSrf.PointProject( vret, vct) )
		{
			//vret.Write();
			return vret;
		}


		n *= 2;
	}

	cout << "Problems with point projection....\n";
	vret.Write();

	//THROW_INTERNAL( "Could not find successfyuly the projection");

	return vret;
}

template <Dimension DIM>
void NurbsSrf<DIM>::GetCoord( UVect& vret, const GVect& vct) const
{
	mNurbsSrf.CalcPoint( vret, vct);
}

template <Dimension DIM>
void NurbsSrf<DIM>::GetFstDeriv( UVect tab[DIM_2D], const GVect& vct) const
{
	NURBS::Matrix<UVect>	mtxD;
	mNurbsSrf.CalcDerivs( vct, 1, mtxD);
	tab[0] =  mtxD(1,0);
	tab[1] =  mtxD(0,1);
}

template <Dimension DIM>
void NurbsSrf<DIM>::GetSndDeriv( UVect tab[SIZE], const GVect& vct) const
{
	NURBS::Matrix<UVect>	mtxD;
	mNurbsSrf.CalcDerivs( vct, 2, mtxD);
	tab[0] =  mtxD(2,0);
	tab[1] =  mtxD(0,2);
	tab[2] =  mtxD(1,1);
}

template <Dimension DIM>
void NurbsSrf<DIM>::Eval( UVect& vret, UVect tab1[DIM_2D], UVect tab2[SIZE], const GVect& vct) const
{
	NURBS::Matrix<UVect>	mtxD;
	mNurbsSrf.CalcDerivs( vct, 2, mtxD);
	
	vret =  mtxD(2,0);

	tab1[0] =  mtxD(1,0);
	tab1[1] =  mtxD(0,1);

	tab2[0] =  mtxD(2,0);
	tab2[1] =  mtxD(0,2);
	tab2[1] =  mtxD(1,1);
}


template <Dimension DIM>
void NurbsSrf<DIM>::ExportTEC( ostream& os) const
{
	ostringstream sos;
	sos << "ID-" << this->cId();
	mNurbsSrf.ExportSrfTEC( os, sos.str(), 10, 10);
}



template class NurbsSrf<DIM_3D>;






//////////////////////////////////////////////////////////////////////
// class CylindricalSrf
//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void CylindricalSrf<DIM>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName())
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\),\\((.*)\\),\\((.*)\\),([-0-9\\.eE]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;
		vector<MGFloat> tab;

		// CylindricalSrf origin
		IO::ReadTabFloat( tab, rex.GetSubString( 1) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mOrigin.rX(k) = tab[k];

		// OX axis
		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 2) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mAxisOX.rX(k) = tab[k];

		// OY axis
		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 3) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mAxisOY.rX(k) = tab[k];

		// radius
		is.clear();
		is.str( rex.GetSubString( 4) );
		is >> mRadius;

		
		sparams = rex.GetSubString( 5);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

template <Dimension DIM>
void CylindricalSrf<DIM>::WriteDef( ostream& of) const
{
	of << setprecision(16); 
	of << ClassName() << "( ";

	of << "( " << mOrigin.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mOrigin.cX(k);

	of << " ), ( " << mAxisOX.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mAxisOX.cX(k);

	of << " ), ( " << mAxisOY.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mAxisOY.cX(k);

	of << " ), " << mRadius;

	of << " )";
}


template <Dimension DIM>
typename CylindricalSrf<DIM>::GVect CylindricalSrf<DIM>::FindParam( const UVect& vct) const
{
    Vect2D	par;	
    UVect	dvct = vct - mOrigin;
	UVect	axisOZ = mAxisOX % mAxisOY;
	
    par.rY()	= dvct * axisOZ;
    par.rX()	= atan2( dvct * mAxisOY , dvct * mAxisOX  );
 
    return par;
}

template <Dimension DIM>
void CylindricalSrf<DIM>::GetCoord( UVect& vret, const GVect& vct) const
{
	MGFloat	v = vct.cY();
	UVect	axisOZ = mAxisOX % mAxisOY;
	vret = mOrigin + mRadius*( cos(vct.cX())*mAxisOX + sin(vct.cX())*mAxisOY ) + vct.cY()*axisOZ;
}

template <Dimension DIM>
void CylindricalSrf<DIM>::GetFstDeriv( UVect tab[DIM_2D], const GVect& vct) const
{
	UVect	axisOZ = mAxisOX % mAxisOY;
	tab[0] =  mRadius * ( -sin( vct.cX() ) * mAxisOX + cos( vct.cX() )*mAxisOY );
	tab[1] =  axisOZ;
}

template <Dimension DIM>
void CylindricalSrf<DIM>::GetSndDeriv( UVect tab[SIZE], const GVect& vct) const
{
	// TODO :: do this !!!
	tab[0] =  UVect( 0.0);
	tab[1] =  UVect( 0.0);
	tab[2] =  UVect( 0.0);
}

template <Dimension DIM>
void CylindricalSrf<DIM>::Eval( UVect& vret, UVect tab1[DIM_2D], UVect tab2[SIZE], const GVect& vct) const
{
	UVect	axisOZ = mAxisOX % mAxisOY;

	vret = mOrigin + mRadius*( cos(vct.cX())*mAxisOX + sin(vct.cX())*mAxisOY ) + vct.cY()*axisOZ;

	tab1[0] =  mRadius * ( -sin( vct.cX() ) * mAxisOX + cos( vct.cX() )*mAxisOY );
	tab1[1] =  axisOZ;

	tab2[0] =  UVect( 0.0);
	tab2[1] =  UVect( 0.0);
	tab2[2] =  UVect( 0.0);
}


template class CylindricalSrf<DIM_3D>;



//////////////////////////////////////////////////////////////////////
// class ConicalSrf
//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void ConicalSrf<DIM>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName())
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\),\\((.*)\\),\\((.*)\\),([-0-9\\.eE]*),([-0-9\\.eE]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;
		vector<MGFloat> tab;

		// ConicalSrf origin
		IO::ReadTabFloat( tab, rex.GetSubString( 1) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mOrigin.rX(k) = tab[k];

		// OX axis
		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 2) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mAxisOX.rX(k) = tab[k];

		// OY axis
		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 3) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mAxisOY.rX(k) = tab[k];

		// radius
		is.clear();
		is.str( rex.GetSubString( 4) );
		is >> mRadius;

		// semi angle
		is.clear();
		is.str( rex.GetSubString( 5) );
		is >> mAngle;

		//mAngle *= M_PI / 180.; //fix fo degs

		sparams = rex.GetSubString( 6);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

template <Dimension DIM>
void ConicalSrf<DIM>::WriteDef( ostream& of) const
{
	of << setprecision(16); 
	of << ClassName() << "( ";

	of << "( " << mOrigin.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mOrigin.cX(k);

	of << " ), ( " << mAxisOX.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mAxisOX.cX(k);

	of << " ), ( " << mAxisOY.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mAxisOY.cX(k);

	of << " ), " << mRadius << ", " << mAngle;

	of << " )";
}


template <Dimension DIM>
typename ConicalSrf<DIM>::GVect ConicalSrf<DIM>::FindParam( const UVect& vct) const
{
    Vect2D	par;	
    UVect	dvct = vct - mOrigin;
	UVect	axisOZ = (mAxisOX % mAxisOY).versor();
	
    par.rY()	= dvct * axisOZ;
    par.rX()	= atan2( dvct * mAxisOY , dvct * mAxisOX  );
 
	//UVect vtest;
	//GetCoord( vtest, par);

	//if ( (vtest-vct).module() > 1.0e-6 )
	//	par.rX() += 2.*M_PI;

    return par;
}

template <Dimension DIM>
void ConicalSrf<DIM>::GetCoord( UVect& vret, const GVect& vct) const
{
	MGFloat	v = vct.cY();
	UVect	axisOZ = (mAxisOX % mAxisOY).versor();

	//(u,v) = C + (R + vtan )((cos u)x + (sin u)y) + vz

	vret = mOrigin + (mRadius + ::tan( mAngle)*vct.cY())*( ::cos( vct.cX())*mAxisOX + ::sin( vct.cX())*mAxisOY ) + vct.cY()*axisOZ;
}

template <Dimension DIM>
void ConicalSrf<DIM>::GetFstDeriv( UVect tab[DIM_2D], const GVect& vct) const
{
	UVect	axisOZ = (mAxisOX % mAxisOY).versor();
	tab[0] =  (mRadius + ::tan( mAngle)*vct.cY()) * ( -sin( vct.cX() ) * mAxisOX + cos( vct.cX() )*mAxisOY );
	tab[1] =  axisOZ + ::tan( mAngle)*( ::cos( vct.cX())*mAxisOX + ::sin( vct.cX())*mAxisOY );
}

template <Dimension DIM>
void ConicalSrf<DIM>::GetSndDeriv( UVect tab[SIZE], const GVect& vct) const
{
	// TODO :: do this !!!
	tab[0] =  UVect( 0.0);
	tab[1] =  UVect( 0.0);
	tab[2] =  UVect( 0.0);
}

template <Dimension DIM>
void ConicalSrf<DIM>::Eval( UVect& vret, UVect tab1[DIM_2D], UVect tab2[SIZE], const GVect& vct) const
{
	UVect	axisOZ = (mAxisOX % mAxisOY).versor();

	vret = mOrigin + (mRadius + ::tan( mAngle)*vct.cY())*( ::cos( vct.cX())*mAxisOX + ::sin( vct.cX())*mAxisOY ) + vct.cY()*axisOZ;

	tab1[0] =  (mRadius + ::tan( mAngle)*vct.cY()) * ( -sin( vct.cX() ) * mAxisOX + cos( vct.cX() )*mAxisOY );
	tab1[1] =  axisOZ + ::tan( mAngle)*( ::cos( vct.cX())*mAxisOX + ::sin( vct.cX())*mAxisOY );

	tab2[0] =  UVect( 0.0);
	tab2[1] =  UVect( 0.0);
	tab2[2] =  UVect( 0.0);
}


template class ConicalSrf<DIM_3D>;


//////////////////////////////////////////////////////////////////////
// class ExtrusionSrf
//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void ExtrusionSrf<DIM>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName())
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\),\\((.*)\\),\\((.*)\\),([-0-9\\.eE]*),([-0-9\\.eE]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;
		vector<MGFloat> tab;

		// ExtrusionSrf origin
		IO::ReadTabFloat( tab, rex.GetSubString( 1) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mOrigin.rX(k) = tab[k];

		// OX axis
		tab.clear();
		IO::ReadTabFloat( tab, rex.GetSubString( 2) );
		if ( tab.size() != DIM)
			THROW_INTERNAL( "DIM coordinates expected" );

		for ( MGSize k=0; k<DIM; ++k)
			mAxisOX.rX(k) = tab[k];

		sparams = rex.GetSubString( 6);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

template <Dimension DIM>
void ExtrusionSrf<DIM>::WriteDef( ostream& of) const
{
	of << setprecision(16); 
	of << ClassName() << "( ";

	of << "( " << mOrigin.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mOrigin.cX(k);

	of << " ), ( " << mAxisOX.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mAxisOX.cX(k);

	of << " )";
}


template <Dimension DIM>
typename ExtrusionSrf<DIM>::GVect ExtrusionSrf<DIM>::FindParam( const UVect& vct) const
{
    Vect2D	par;	
 //   UVect	dvct = vct - mOrigin;
	//UVect	axisOZ = mAxisOX % mAxisOY;
	//
 //   par.rY()	= dvct * axisOZ;
 //   par.rX()	= atan2( dvct * mAxisOY , dvct * mAxisOX  );
 
    return par;
}

template <Dimension DIM>
void ExtrusionSrf<DIM>::GetCoord( UVect& vret, const GVect& vct) const
{
	//MGFloat	v = vct.cY();
	//UVect	axisOZ = mAxisOX % mAxisOY;
	//vret = mOrigin + (mRadius + ::tan( mAngle)*vct.cY())*( ::cos( vct.cX())*mAxisOX + ::sin( vct.cX())*mAxisOY ) + vct.cY()*axisOZ;
}

template <Dimension DIM>
void ExtrusionSrf<DIM>::GetFstDeriv( UVect tab[DIM_2D], const GVect& vct) const
{
	//UVect	axisOZ = mAxisOX % mAxisOY;
	//tab[0] =  (mRadius + ::tan( mAngle)*vct.cY()) * ( -sin( vct.cX() ) * mAxisOX + cos( vct.cX() )*mAxisOY );
	//tab[1] =  axisOZ + ::tan( mAngle)*( ::cos( vct.cX())*mAxisOX + ::sin( vct.cX())*mAxisOY );
}

template <Dimension DIM>
void ExtrusionSrf<DIM>::GetSndDeriv( UVect tab[SIZE], const GVect& vct) const
{
	//// TODO :: do this !!!
	//tab[0] =  UVect( 0.0);
	//tab[1] =  UVect( 0.0);
	//tab[2] =  UVect( 0.0);
}

template <Dimension DIM>
void ExtrusionSrf<DIM>::Eval( UVect& vret, UVect tab1[DIM_2D], UVect tab2[SIZE], const GVect& vct) const
{
	//UVect	axisOZ = mAxisOX % mAxisOY;

	//vret = mOrigin + (mRadius + ::tan( mAngle)*vct.cY())*( ::cos( vct.cX())*mAxisOX + ::sin( vct.cX())*mAxisOY ) + vct.cY()*axisOZ;

	//tab1[0] =  (mRadius + ::tan( mAngle)*vct.cY()) * ( -sin( vct.cX() ) * mAxisOX + cos( vct.cX() )*mAxisOY );
	//tab1[1] =  axisOZ + ::tan( mAngle)*( ::cos( vct.cX())*mAxisOX + ::sin( vct.cX())*mAxisOY );

	//tab2[0] =  UVect( 0.0);
	//tab2[1] =  UVect( 0.0);
	//tab2[2] =  UVect( 0.0);
}


template class ExtrusionSrf<DIM_3D>;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
