#ifndef __GETSURFACE_H__
#define __GETSURFACE_H__


#include "libextget/geomentity.h"
#include "libextget/getconst.h"

#include "libextnurbs/nurbssurface.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//




//////////////////////////////////////////////////////////////////////
// class Plane
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Plane : public GeomEntity<DIM_2D,DIM>
{
	typedef typename GeomEntity<DIM_2D,DIM>::UVect	UVect;
	typedef typename GeomEntity<DIM_2D,DIM>::GVect	GVect;

public:
	enum { SIZE = GeomEntity<DIM_2D,DIM>::SIZE};

	Plane( const MGSize& id=0) : GeomEntity<DIM_2D,DIM>()		{}
	virtual ~Plane()											{}
	

	static const char*	ClassName()								{ return GET_PLANE;}

	virtual MGString	GetName() const							{ return ClassName();};
	virtual bool		IsType( const MGString& type) const		{ return type == ClassName() ? true : false; }

	virtual void		Init( const MGString& sname, MGString& sparams);
	virtual void		WriteDef( ostream& of) const;


	virtual bool		IsPeriodic( const MGSize& idir) const	{ return false;}
	virtual MGFloat		GetPeriod( const MGSize& idir) const	{ return 0.0;}

	virtual GVect		FindParam( const UVect& vct) const;

	virtual void		GetCoord( UVect& vret, const GVect& vct) const;
	virtual void		GetFstDeriv( UVect tab[DIM_2D], const GVect& vct) const;
	virtual void		GetSndDeriv( UVect tab[SIZE], const GVect& vct) const;

	virtual void		Eval( UVect& vret, UVect tab1[DIM_2D], UVect tab2[SIZE], const GVect& vct) const;

protected:
	UVect&	rOrigin()		{ return mOrigin;}	// base point
	UVect&	rvOX()			{ return mvOX;}		// OX vector
	UVect&	rvOY()			{ return mvOY;}		// OY vector

private:
	UVect	mOrigin;	// base point
	UVect	mvOX;		// OX vector
	UVect	mvOY;		// OY vector

};




//////////////////////////////////////////////////////////////////////
// class Sphere
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Sphere : public GeomEntity<DIM_2D,DIM>
{
	typedef typename GeomEntity<DIM_2D,DIM>::UVect	UVect;
	typedef typename GeomEntity<DIM_2D,DIM>::GVect	GVect;

public:
	enum { SIZE = GeomEntity<DIM_2D,DIM>::SIZE};

	Sphere( const MGSize& id=0) : GeomEntity<DIM_2D,DIM>()		{}
	virtual ~Sphere()											{}
	

	static const char*	ClassName()								{ return GET_SPHERE;}

	virtual MGString	GetName() const							{ return ClassName();};
	virtual bool		IsType( const MGString& type) const		{ return type == ClassName() ? true : false; }

	virtual void		Init( const MGString& sname, MGString& sparams);
	virtual void		WriteDef( ostream& of) const;


	//virtual bool		IsPeriodic( const MGSize& idir) const	{ return false;}
	//virtual MGFloat	GetPeriod( const MGSize& idir) const	{ return 0.0;}
	virtual bool		IsPeriodic( const MGSize& idir) const	{ return true;}
	virtual MGFloat		GetPeriod( const MGSize& idir) const	{ return 2.*M_PI;}

	virtual GVect		FindParam( const UVect& vct) const;

	virtual void		GetCoord( UVect& vret, const GVect& vct) const;
	virtual void		GetFstDeriv( UVect tab[DIM_2D], const GVect& vct) const;
	virtual void		GetSndDeriv( UVect tab[SIZE], const GVect& vct) const;

	virtual void		Eval( UVect& vret, UVect tab1[DIM_2D], UVect tab2[SIZE], const GVect& vct) const;

protected:
	UVect&		rCenter()	{ return mCenter;}	// center point
	UVect&		rAxisOX()	{ return mAxisOX;}	// OX vector
	UVect&		rAxisOY()	{ return mAxisOY;}	// OY vector
	MGFloat&	rRadius()	{ return mRadius;}	// radius

private:
	UVect	mCenter;	// center point
	UVect	mAxisOX;	// OX vector
	UVect	mAxisOY;	// OY vector
	MGFloat	mRadius;	// radius

};

//////////////////////////////////////////////////////////////////////
// class Torus
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Torus : public GeomEntity<DIM_2D,DIM>
{
	typedef typename GeomEntity<DIM_2D,DIM>::UVect	UVect;
	typedef typename GeomEntity<DIM_2D,DIM>::GVect	GVect;

public:
	enum { SIZE = GeomEntity<DIM_2D,DIM>::SIZE};

	Torus( const MGSize& id=0) : GeomEntity<DIM_2D,DIM>()		{}
	virtual ~Torus()											{}
	

	static const char*	ClassName()								{ return GET_TORUS;}

	virtual MGString	GetName() const							{ return ClassName();};
	virtual bool		IsType( const MGString& type) const		{ return type == ClassName() ? true : false; }

	virtual void		Init( const MGString& sname, MGString& sparams);
	virtual void		WriteDef( ostream& of) const;


	virtual bool		IsPeriodic( const MGSize& idir) const	{ return true;}
	virtual MGFloat		GetPeriod( const MGSize& idir) const	{ return 2.*M_PI;}

	virtual GVect		FindParam( const UVect& vct) const;

	virtual void		GetCoord( UVect& vret, const GVect& vct) const;
	virtual void		GetFstDeriv( UVect tab[DIM_2D], const GVect& vct) const;
	virtual void		GetSndDeriv( UVect tab[SIZE], const GVect& vct) const;

	virtual void		Eval( UVect& vret, UVect tab1[DIM_2D], UVect tab2[SIZE], const GVect& vct) const;

protected:
	UVect&		rCenter()	{ return mCenter;}	// center point
	UVect&		rAxisOX()	{ return mAxisOX;}	// OX vector
	UVect&		rAxisOY()	{ return mAxisOY;}	// OY vector
	MGFloat&	rRadius1()	{ return mRadius1;}	// radius
	MGFloat&	rRadius2()	{ return mRadius2;}	// radius

private:
	UVect	mCenter;	// center point
	UVect	mAxisOX;	// OX vector
	UVect	mAxisOY;	// OY vector
	MGFloat	mRadius1;	// major radius
	MGFloat	mRadius2;	// minor radius
};


//////////////////////////////////////////////////////////////////////
// class NurbsSrf
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class NurbsSrf : public GeomEntity<DIM_2D,DIM>
{
	typedef typename GeomEntity<DIM_2D,DIM>::UVect	UVect;
	typedef typename GeomEntity<DIM_2D,DIM>::GVect	GVect;

public:
	enum { SIZE = GeomEntity<DIM_2D,DIM>::SIZE};

	NurbsSrf( const MGSize& id=0) : GeomEntity<DIM_2D,DIM>()	{}
	virtual ~NurbsSrf()											{}
	

	static const char*	ClassName()								{ return GET_NURBSSRF;}

	virtual MGString	GetName() const							{ return ClassName();};
	virtual bool		IsType( const MGString& type) const		{ return type == ClassName() ? true : false; }

	virtual void		Init( const MGString& sname, MGString& sparams);
	virtual void		WriteDef( ostream& of) const;


	//virtual bool		IsPeriodic( const MGSize& idir) const	{ return mbPeriodic[idir];}
	//virtual MGFloat		GetPeriod( const MGSize& idir) const	{ return mPeriod[idir];}
	virtual bool		IsPeriodic( const MGSize& idir) const	{ return mbPeriodic[idir]; }
	virtual MGFloat		GetPeriod( const MGSize& idir) const	{ return mPeriod[idir]; }

	virtual GVect		FindParam( const UVect& vct) const;

	virtual void		GetCoord( UVect& vret, const GVect& vct) const;
	virtual void		GetFstDeriv( UVect tab[DIM_2D], const GVect& vct) const;
	virtual void		GetSndDeriv( UVect tab[SIZE], const GVect& vct) const;

	virtual void		Eval( UVect& vret, UVect tab1[DIM_2D], UVect tab2[SIZE], const GVect& vct) const;

	virtual void		ExportTEC( ostream& os) const;

protected:
	bool&		rbPeriodic( const MGSize& i)	{ return mbPeriodic[i];}
	MGFloat&	rPeriod( const MGSize& i)		{ return mPeriod[i];}

	NURBS::NurbsSurface<DIM,MGFloat>&	rNurbsSurf()		{ return mNurbsSrf;}

private:
	bool	mbPeriodic[DIM_2D];
	MGFloat	mPeriod[DIM_2D];

	NURBS::NurbsSurface<DIM,MGFloat>	mNurbsSrf;
};




//////////////////////////////////////////////////////////////////////
// class CylindricalSrf
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class CylindricalSrf : public GeomEntity<DIM_2D,DIM>
{
	typedef typename GeomEntity<DIM_2D,DIM>::UVect	UVect;
	typedef typename GeomEntity<DIM_2D,DIM>::GVect	GVect;

public:
	enum { SIZE = GeomEntity<DIM_2D,DIM>::SIZE};

	CylindricalSrf( const MGSize& id=0) : GeomEntity<DIM_2D,DIM>()		{}
	virtual ~CylindricalSrf()											{}
	

	static const char*	ClassName()								{ return GET_CYLINDRICALSRF;}

	virtual MGString	GetName() const							{ return ClassName();};
	virtual bool		IsType( const MGString& type) const		{ return type == ClassName() ? true : false; }

	virtual void		Init( const MGString& sname, MGString& sparams);
	virtual void		WriteDef( ostream& of) const;


	virtual bool		IsPeriodic( const MGSize& idir) const	{ if ( idir == 0 ) return true; return false;}
	virtual MGFloat		GetPeriod( const MGSize& idir) const	{ if ( idir == 0 ) return 2*M_PI; return 0.0;}

	virtual GVect		FindParam( const UVect& vct) const;

	virtual void		GetCoord( UVect& vret, const GVect& vct) const;
	virtual void		GetFstDeriv( UVect tab[DIM_2D], const GVect& vct) const;
	virtual void		GetSndDeriv( UVect tab[SIZE], const GVect& vct) const;

	virtual void		Eval( UVect& vret, UVect tab1[DIM_2D], UVect tab2[SIZE], const GVect& vct) const;

protected:
	UVect&		rOrigin()	{ return mOrigin;}	// base point
	UVect&		rAxisOX()	{ return mAxisOX;}	// OX vector
	UVect&		rAxisOY()	{ return mAxisOY;}	// OY vector
	MGFloat&	rRadius()	{ return mRadius;}	// radius


private:
	UVect	mOrigin;	// base point
	UVect	mAxisOX;	// OX vector
	UVect	mAxisOY;	// OY vector
	MGFloat	mRadius;	// radius

};


//////////////////////////////////////////////////////////////////////
// class ConicalSrf
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class ConicalSrf : public GeomEntity<DIM_2D,DIM>
{
	typedef typename GeomEntity<DIM_2D,DIM>::UVect	UVect;
	typedef typename GeomEntity<DIM_2D,DIM>::GVect	GVect;

public:
	enum { SIZE = GeomEntity<DIM_2D,DIM>::SIZE};

	ConicalSrf( const MGSize& id=0) : GeomEntity<DIM_2D,DIM>()		{}
	virtual ~ConicalSrf()											{}
	

	static const char*	ClassName()								{ return GET_CONICALSRF;}

	virtual MGString	GetName() const							{ return ClassName();};
	virtual bool		IsType( const MGString& type) const		{ return type == ClassName() ? true : false; }

	virtual void		Init( const MGString& sname, MGString& sparams);
	virtual void		WriteDef( ostream& of) const;


	virtual bool		IsPeriodic( const MGSize& idir) const	{ if ( idir == 0 ) return true; return false;}
	virtual MGFloat		GetPeriod( const MGSize& idir) const	{ if ( idir == 0 ) return 2*M_PI; return 0.0;}

	virtual GVect		FindParam( const UVect& vct) const;

	virtual void		GetCoord( UVect& vret, const GVect& vct) const;
	virtual void		GetFstDeriv( UVect tab[DIM_2D], const GVect& vct) const;
	virtual void		GetSndDeriv( UVect tab[SIZE], const GVect& vct) const;

	virtual void		Eval( UVect& vret, UVect tab1[DIM_2D], UVect tab2[SIZE], const GVect& vct) const;

protected:
	UVect&		rOrigin()	{ return mOrigin;}	// base point
	UVect&		rAxisOX()	{ return mAxisOX;}	// OX vector
	UVect&		rAxisOY()	{ return mAxisOY;}	// OY vector
	MGFloat&	rRadius()	{ return mRadius;}	// radius
	MGFloat&	rAngle()	{ return mAngle;}	// semi angle

private:
	UVect	mOrigin;	// base point
	UVect	mAxisOX;	// OX vector
	UVect	mAxisOY;	// OY vector
	MGFloat	mRadius;	// radius
	MGFloat	mAngle;		// semi angle

};



//////////////////////////////////////////////////////////////////////
// class ExtrusionSrf
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class ExtrusionSrf : public GeomEntity<DIM_2D,DIM>
{
	typedef typename GeomEntity<DIM_2D,DIM>::UVect	UVect;
	typedef typename GeomEntity<DIM_2D,DIM>::GVect	GVect;

public:
	enum { SIZE = GeomEntity<DIM_2D,DIM>::SIZE};

	ExtrusionSrf( const MGSize& id=0) : GeomEntity<DIM_2D,DIM>(), mpCurve(NULL)		{}
	virtual ~ExtrusionSrf()										{ if ( mpCurve) delete mpCurve;}
	

	static const char*	ClassName()								{ return GET_EXTRUSIONSRF;}

	virtual MGString	GetName() const							{ return ClassName();};
	virtual bool		IsType( const MGString& type) const		{ return type == ClassName() ? true : false; }

	virtual void		Init( const MGString& sname, MGString& sparams);
	virtual void		WriteDef( ostream& of) const;


	virtual bool		IsPeriodic( const MGSize& idir) const	{ return false;}
	virtual MGFloat		GetPeriod( const MGSize& idir) const	{ return 0.0;}

	virtual GVect		FindParam( const UVect& vct) const;

	virtual void		GetCoord( UVect& vret, const GVect& vct) const;
	virtual void		GetFstDeriv( UVect tab[DIM_2D], const GVect& vct) const;
	virtual void		GetSndDeriv( UVect tab[SIZE], const GVect& vct) const;

	virtual void		Eval( UVect& vret, UVect tab1[DIM_2D], UVect tab2[SIZE], const GVect& vct) const;

protected:
	UVect&		rOrigin()	{ return mOrigin;}	// base point
	UVect&		rAxisOX()	{ return mAxisOX;}	// OX vector (extrusion direction)

private:
	UVect	mOrigin;	// base point
	UVect	mAxisOX;	// OX vector

	GeomEntity<DIM_1D,DIM>* mpCurve;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace GET = GETSpace;


#endif // __GETSURFACE_H__
