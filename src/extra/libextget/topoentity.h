#ifndef __TOPOENTITY_H__
#define __TOPOENTITY_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libcorecommon/entity.h"
#include "libextget/getconst.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

using namespace Geom;


typedef pair<MGSize,bool>	OrientedId;
typedef vector<OrientedId>	TopoLoop;
typedef pair<TopoLoop,bool>	OrientedLoop;


template <Dimension DIM>
class GeoTopoIO;

//////////////////////////////////////////////////////////////////////
//	class TopoEntity
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class TopoEntity : public Entity
{
	friend class GeoTopoIO<DIM_2D>;
	friend class GeoTopoIO<DIM_3D>;

public:
	TopoEntity() : Entity()			{}

	static const char*		ClassName();



	const MGSize&	cGeomId() const				{ return mGeomEntId;}

	const vector<OrientedLoop>&	cTab() const	{ return mtabLoop;}

	void	Init( const MGString& sname, MGString& sparams);
	void	WriteDef( ostream& of) const;

protected:
	MGSize&					rGeomId()	{ return mGeomEntId;}
	vector<OrientedLoop>&	rTab()		{ return mtabLoop;}

private:
	MGSize					mGeomEntId;	// Geometrical Entity ID
	vector<OrientedLoop>	mtabLoop;	// IDs of the boundary Loops
};


template <>
inline const char* TopoEntity<DIM_0D>::ClassName()
{ 
	return GET_VERTEX;
}

template <>
inline const char* TopoEntity<DIM_1D>::ClassName()
{ 
	return GET_EDGE;
}

template <>
inline const char* TopoEntity<DIM_2D>::ClassName()
{ 
	return GET_FACE;
}

template <>
inline const char* TopoEntity<DIM_3D>::ClassName()
{ 
	return GET_BREP;
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace GET = GETSpace;


#endif // __TOPOENTITY_H__
