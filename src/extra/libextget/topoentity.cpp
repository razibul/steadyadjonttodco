#include "topoentity.h"

#include "libcoreio/store.h"
#include "libcorecommon/regexp.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


template <Dimension DIM>
void TopoEntity<DIM>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName() )
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mGeomEntId;

		MGString	buf = rex.GetSubString( 2);
		MGString	loopdef = rex.GetSubString( 2);

		MGSize	loopcount = 0;

		rex.InitPattern( "^LOOP\\(\\(([\\(\\)#TF0-9,]*)\\),(T|F)\\)(.*)$");
		rex.SetString( buf);
		while ( rex.IsOk() )
		{
			++loopcount;

			buf = rex.GetSubString(3);

			mtabLoop.push_back( OrientedLoop() );

			TopoLoop&	tloop = mtabLoop[mtabLoop.size()-1].first;
			bool&		bor = mtabLoop[mtabLoop.size()-1].second;

			if ( rex.GetSubString(2) == "T")
				bor = true;
			else 
			if ( rex.GetSubString(2) == "F")
				bor = false;
			else
				THROW_INTERNAL( "unrecognized flag in the loop definition");

			MGString buftmp = rex.GetSubString(1);

			MGSize	count = 0;
			rex.InitPattern( "^\\(#([0-9]*),(T|F)\\)(.*)$");
			rex.SetString( buftmp);
			while ( rex.IsOk() )
			{
				++count;
				buftmp = rex.GetSubString(3);

				if ( ! buftmp.empty() )
					if ( buftmp[0] == ',' )
						buftmp.erase( buftmp.begin() );
				
				bool bflag;
				MGSize id;

				if ( rex.GetSubString(2) == "T")
					bflag = true;
				else 
				if ( rex.GetSubString(2) == "F")
					bflag = false;
				else
					THROW_INTERNAL( "unrecognized flag in the loop definition");

				is.clear();
				is.str( rex.GetSubString( 1) );
				is >> id;

				tloop.push_back( OrientedId( id, bflag) );

				rex.InitPattern( "^\\(#([0-9]*),(T|F)\\)(.*)$");
				rex.SetString( buftmp);
			};

			if ( count == 0)
			{
				MGString out = MGString( "could not find any oriented ids in the LOOP : '") + loopdef + MGString( "'; ");
				THROW_FILE( out.c_str(), "" );
			}

			if ( ! buf.empty() )
				if ( buf[0] == ',' )
					buf.erase( buf.begin() );

			loopdef = buf;

			rex.InitPattern( "^LOOP\\(\\(([\\(\\)#TF0-9,]*)\\),(T|F)\\)(.*)$");
			rex.SetString( buf);
		}

		if ( loopcount == 0)
		{
			MGString out = MGString( "could not find any LOOP : '") + sparams + MGString( "'; ");
			THROW_FILE( out.c_str(), "" );
		}

	}

}

template <>
void TopoEntity<DIM_0D>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName() )
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mGeomEntId;

		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );
}

template <>
void TopoEntity<DIM_1D>::Init( const MGString& sname, MGString& sparams)
{
	if ( sname != ClassName() )
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName() ) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),\\(#([0-9]*),#([0-9]*)\\),(T|F)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mGeomEntId;

		mtabLoop.clear();
		mtabLoop.push_back( OrientedLoop() );

		ASSERT( mtabLoop.size() == 1);

		TopoLoop&	tloop = mtabLoop[0].first;
		tloop.push_back( OrientedId( 0, true) );
		tloop.push_back( OrientedId( 0, true) );

		ASSERT( tloop.size() == 2);

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> tloop[0].first;

		is.clear();
		is.str( rex.GetSubString( 3) );
		is >> tloop[1].first;

		bool bflag;

		if ( rex.GetSubString(4) == "T")
			bflag = true;
		else 
		if ( rex.GetSubString(4) == "F")
			bflag = false;
		else
			THROW_INTERNAL( "unrecognized flag in the edge definition");

		mtabLoop[0].second = bflag;


		sparams = rex.GetSubString( 5);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


template <Dimension DIM>
void TopoEntity<DIM>::WriteDef( ostream& of) const
{
	of << ClassName() << "( ";
	of << "#" << mGeomEntId;
	of << ", ";

	for ( MGSize i=0; i<mtabLoop.size(); ++i)
	{
		of << "LOOP( (";

		for ( MGSize j=0; j<mtabLoop[i].first.size(); ++j)
		{
			of << "(#" << mtabLoop[i].first[j].first << ",";
			if ( mtabLoop[i].first[j].second )
				of << "T";
			else
				of << "F";
			of << ")";
			if ( j != mtabLoop[i].first.size()-1 )
				of << ",";
		}

		of << "), ";
		
		if ( mtabLoop[i].second )
			of << "T";
		else
			of << "F";

		of << " )";
		if ( i != mtabLoop.size()-1 )
			of << ", ";
	}

	of << " )";
}

template <>
void TopoEntity<DIM_0D>::WriteDef( ostream& of) const
{
	of << ClassName() << "( ";
	of << "#" << mGeomEntId;
	of << " )";
}

template <>
void TopoEntity<DIM_1D>::WriteDef( ostream& of) const
{
	if ( mtabLoop.size() != 1)
		THROW_INTERNAL( "TopoEntity<DIM_1D>::WriteDef : EDGE is not initialized properly");

	if ( mtabLoop[0].first.size() != 2)
		THROW_INTERNAL( "TopoEntity<DIM_1D>::WriteDef : EDGE is not initialized properly");

	of << ClassName() << "( ";
	of << "#" << mGeomEntId;
	of << ", ( ";
	of << "#" << mtabLoop[0].first[0].first;
	of << ", ";
	of << "#" << mtabLoop[0].first[1].first;
	of << " ), ";

	if ( mtabLoop[0].second )
		of << "T";
	else
		of << "F";

	of << " )";
}





template class TopoEntity<DIM_0D>;
template class TopoEntity<DIM_1D>;
template class TopoEntity<DIM_2D>;
template class TopoEntity<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
