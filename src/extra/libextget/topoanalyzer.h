#ifndef __TOPOANALYZER_H__
#define __TOPOANALYZER_H__

#include "topoowner.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
//	class TopoOwner
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class TopoAnalyzer
{
public:
	TopoAnalyzer( const TopoOwner<DIM>& town) : mTOwner(town)	{}

	void	CheckEdges();

private:
	const TopoOwner<DIM>&		mTOwner;
};





//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace GET = GETSpace;


#endif // __TOPOANALYZER_H__
