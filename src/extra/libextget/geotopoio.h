#ifndef __GEOTOPOIO_H__
#define __GEOTOPOIO_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libcorecommon/regexp.h"

#include "libextget/geomentity.h"
#include "libextget/topoentity.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


#define GET_VER_STRING "GET_VER"
#define GET_VER_NUMBER "0.2"


template <Dimension DIM>
class GeoTopoMaster;



//////////////////////////////////////////////////////////////////////
// class BaseFactory
//////////////////////////////////////////////////////////////////////
template <class T>
class BaseFactory
{
public:
	BaseFactory()				{}
	virtual		~BaseFactory()	{}

	virtual MGString	ObjectName() = 0;
	virtual T*			Create( const MGSize& id, const MGString& sname, const MGString& sparams)	= 0;

};


//////////////////////////////////////////////////////////////////////
// class Factory
//////////////////////////////////////////////////////////////////////
template <class T, class B>
class Factory : public BaseFactory<B>
{
public:
	virtual MGString		ObjectName()
	{
		return MGString( T::ClassName() );
	}

	virtual B*		Create( const MGSize& id, const MGString& sname, const MGString& sparams)
	{
		try
		{
			if ( sname != T::ClassName() )
				return NULL;

			T* ptr = new T;
			MGString sbuf = sparams;
			ptr->Init( sname, sbuf);
			if ( sbuf.length() > 0 )
				THROW_INTERNAL( "not every argument has been interpreted" );

			return ptr;
		}
		catch ( EHandler::Except&)
		{
			cout << endl;
			cout << T::ClassName() << " : id = " << id << "  params = \"" << sparams << "\"" << endl;

			throw;
		}

		return NULL;

	}
};



//////////////////////////////////////////////////////////////////////
//	class GeoTopoIO
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class GeoTopoIO
{
public:
	GeoTopoIO( GeoTopoMaster<DIM>& get);

	~GeoTopoIO();

	void	DoRead( const MGString& fname);
	void	DoWrite( const MGString& fname);

	void	ExtractFaces( const vector<MGSize> tabfid );

	void	ExportTEC( const MGString& fname);

protected:
	static bool	CheckVersion_ascii( istream& file);

	static const MGString& VerNum()	{ return mstVersionNumber;}
	static const MGString& VerStr()	{ return mstVersionString;}

	void	InitPointCreators();
	void	InitCurveCreators();
	void	InitSurfaceCreators();

	void	ReadHeader( istream& file);
	void	ReadGeometrySection( istream& file);
	void	ReadTopologySection( istream& file);

	void	ReadDataSection( istream& file, const MGString& key, void (GeoTopoIO<DIM>::*addFun)( const MGString&) );

	void	ReadGeometryData( istream& file);
	void	ReadTopologyData( istream& file);

	void	ParseDef( MGSize& id, MGString& str1, MGString& str2, const MGString& def);
	void	ParseDef( MGSize& id, MGString& str1, MGString& str2,  MGString& sname, const MGString& def);

	void	AddPoint( const MGString& def);
	void	AddCurve( const MGString& def);
	void	AddSurface( const MGString& def);

	template <Dimension TDIM>
	void	AddTopoEnt( const MGString& def);


	void	RemoveUnreferenced();
	void	RemoveUnreferencedGeom();

	void	WriteHeader( ostream& file);
	void	WriteGeometrySection( ostream& file);
	void	WriteTopologySection( ostream& file);


	void	ExportCurvTEC( ostream& file);
	void	ExportSurfTEC( ostream& file);

private:
	static MGString	mstVersionString;
	static MGString	mstVersionNumber;

	GeoTopoMaster<DIM>*	mpGeT;

	RegExp		mRex;

	vector< BaseFactory< GeomEntity<DIM_0D,DIM> >* >	mtabPointCreator;
	vector< BaseFactory< GeomEntity<DIM_1D,DIM> >* >	mtabCurveCreator;
	vector< BaseFactory< GeomEntity<DIM_2D,DIM> >* >	mtabSurfaceCreator;

};

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace GET = GETSpace;



#endif // __GEOTOPOIO_H__
