#ifndef __Point_H__
#define __Point_H__

#include "geomentity.h"
#include "getconst.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class Point
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Point : public GeomEntity<DIM_0D,DIM>
{
	typedef typename GeomEntity<DIM_0D,DIM>::UVect	UVect;

public:
	Point( const MGSize& id=0) : GeomEntity<DIM_0D,DIM>()	{}
	virtual ~Point()											{}
	

	static const char*		ClassName()							{ return GET_POINT;}

	virtual MGString	GetName() const							{ return ClassName();};
	virtual bool		IsType( const MGString& type) const		{ return type == ClassName() ? true : false; }

	virtual void		Init( const MGString& sname, MGString& sparams);
	virtual void		WriteDef( ostream& of) const;


	virtual void		GetCoord( UVect& vret) const 			{ vret = mVect;};

protected:
	UVect&	rVect()		{ return mVect;}

private:
	UVect	mVect;
};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace GET = GETSpace;


#endif // __Point_H__
