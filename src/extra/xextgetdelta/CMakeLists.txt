LIST ( APPEND xextgetdelta_files
	main.cpp
) 


INCLUDE_DIRECTORIES (
  ${FLOW2_SOURCE_DIR}/${NAME_CORE}
  ${FLOW2_SOURCE_DIR}/${NAME_AUX}
)


LINK_DIRECTORIES (
  ${FLOW2_BINARY_DIR}/${NAME_CORE}/libcoresystem 
  ${FLOW2_BINARY_DIR}/${NAME_CORE}/libcorecommon 
  ${FLOW2_BINARY_DIR}/${NAME_CORE}/libcoreio
  ${FLOW2_BINARY_DIR}/${NAME_CORE}/libcoregeom
  ${FLOW2_BINARY_DIR}/${NAME_AUX}/libextsparse
  ${FLOW2_BINARY_DIR}/${NAME_AUX}/libextget
  ${FLOW2_BINARY_DIR}/${NAME_AUX}/libextnurbs
  ${FLOW2_BINARY_DIR}/${NAME_AUX}/libextstep
)

ADD_EXECUTABLE ( xextgetdelta ${xextgetdelta_files} )


##############################################################
# !!!  the order of the libraries in the list is IMPORTANT !!!
TARGET_LINK_LIBRARIES ( xextgetdelta 
	extget
	extstep
	extnurbs
	extsparse 
	coreio 
	coregeom 
	corecommon 
	coresystem 
) 

