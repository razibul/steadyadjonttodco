#include "libcoresystem/mgdecl.h"
#include "libcoregeom/vect.h"

using namespace Geom;


const MGFloat leangle = 15.0;								// leading edge angle;
const MGFloat edgeangle = 15.0;
const MGFloat cord = 1.0;									// length of the wing root 
const MGFloat span = ::tan( leangle * M_PI/180.) * cord;		// half of the span
const MGFloat thick = 0.01;									// wing thickness
const MGFloat d = thick / ::tan( edgeangle * M_PI/180.);

const MGFloat radius = cord * 50;



int main( int argc, char* argv[])
{
	try
	{
		// POINTS
		MGSize npoint=8;
		Vect3D tabPoint[8];
		tabPoint[0] = Vect3D( 0, 0, 0);
		tabPoint[1] = Vect3D( cord, 0, span);
		tabPoint[2] = Vect3D( cord, 0, 0);
		tabPoint[3] = Vect3D( d / ::sin( leangle * M_PI/180.), -thick, 0);
		tabPoint[4] = Vect3D( cord, -thick, span - d / ::cos( leangle * M_PI/180.) );
		tabPoint[5] = Vect3D( cord, -thick, 0);
		tabPoint[6] = Vect3D( -radius+cord, 0, 0);
		tabPoint[7] = Vect3D( radius+cord, 0, 0);

		// LINES
		Vect3D tabLinePnt[9];
		Vect3D tabLine[9];

		tabLinePnt[0] = tabPoint[0];
		tabLinePnt[1] = tabPoint[0];
		tabLinePnt[2] = tabPoint[2];
		tabLinePnt[3] = tabPoint[3];
		tabLinePnt[4] = tabPoint[3];
		tabLinePnt[5] = tabPoint[5];
		tabLinePnt[6] = tabPoint[0];
		tabLinePnt[7] = tabPoint[2];
		tabLinePnt[8] = tabPoint[1];

		tabLine[0] = ( tabPoint[2] - tabPoint[0] ).versor();
		tabLine[1] = ( tabPoint[1] - tabPoint[0] ).versor();
		tabLine[2] = ( tabPoint[1] - tabPoint[2] ).versor();
		tabLine[3] = ( tabPoint[5] - tabPoint[3] ).versor();
		tabLine[4] = ( tabPoint[4] - tabPoint[3] ).versor();
		tabLine[5] = ( tabPoint[4] - tabPoint[5] ).versor();
		tabLine[6] = ( tabPoint[3] - tabPoint[0] ).versor();
		tabLine[7] = ( tabPoint[5] - tabPoint[2] ).versor();
		tabLine[8] = ( tabPoint[4] - tabPoint[1] ).versor();

		// SURFACES
		Vect3D tabSurfPnt[5];
		Vect3D tabSurfTU[5];
		Vect3D tabSurfTV[5];

		tabSurfPnt[0] = tabPoint[0];
		tabSurfPnt[1] = tabPoint[3];
		tabSurfPnt[2] = tabPoint[0];
		tabSurfPnt[3] = tabPoint[4];
		tabSurfPnt[4] = tabPoint[2];

		tabSurfTU[0] = Vect3D( 0,0,1);
		tabSurfTV[0] = Vect3D( 1,0,0);

		tabSurfTU[1] = Vect3D( 0,0,1);
		tabSurfTV[1] = Vect3D( 1,0,0);

		tabSurfTU[2] = Vect3D( 1,0,0);
		tabSurfTV[2] = Vect3D( 0,1,0);

		Vect3D vn = ( tabLine[8] % tabLine[6] ).versor();
		tabSurfTU[3] = tabLine[4];
		tabSurfTV[3] = ( vn % tabSurfTU[3] ).versor();;

		tabSurfTU[4] = Vect3D( 0,0,1);
		tabSurfTV[4] = Vect3D( 0,-1,0);

		// writing GET file

		ofstream f( "deltawing.get");

		f << "GET_VER 0.2" << endl;

		f << "HEADER;" << endl;
		f << "	3D;" << endl;
		f << "	deltawing;" << endl;
		f << "	delta wing with leading edge angle = " << leangle << ";" << endl;
		f << "END_HEADER;" << endl << endl;


		f << "GEOMETRY;" << endl;

		f << "	SEC_POINT;" << endl;
		for ( MGSize i=0; i<npoint; ++i)
			f << "	#" << i+1 << " = POINT(" << tabPoint[i].cX() << ", " << tabPoint[i].cY() << ", " << tabPoint[i].cZ() << " );" << endl;
		f << "	END_SEC_POINT;" << endl << endl;


		f << "	SEC_CURVE;" << endl;
		for ( MGSize i=0; i<9; ++i)
		{
			f	<< "	#" << i+1 << " = LINE( ( " << tabLinePnt[i].cX() << ", " << tabLinePnt[i].cY() << ", " << tabLinePnt[i].cZ()
				<< " ), ( " << tabLine[i].cX() << ", " << tabLine[i].cY() << ", " << tabLine[i].cZ() << " ) );" << endl;
		}
		f << "	#10 = CIRCLE( ( " << cord << ", 0, 0 ), ( 1, 0, 0 ), ( 0, 0, 1 ), " << radius << " );" << endl;
		f << "	#11 = CIRCLE( ( " << cord << ", 0, 0 ), ( 1, 0, 0 ), ( 0, 1, 0 ), " << radius << " );" << endl;
		f << "	END_SEC_CURVE;" << endl << endl;

		f << "	SEC_SURFACE;" << endl;
		for ( MGSize i=0; i<5; ++i)
		{
			f	<< "	#" << i+1 << " = PLANE( ( " << tabSurfPnt[i].cX() << ", " << tabSurfPnt[i].cY() << ", " << tabSurfPnt[i].cZ() << " ), ( "
				<< tabSurfTU[i].cX() << ", " << tabSurfTU[i].cY() << ", " << tabSurfTU[i].cZ() << " ) ), ( " 
				<< tabSurfTV[i].cX() << ", " << tabSurfTV[i].cY() << ", " << tabSurfTV[i].cZ() << " ) );" << endl;
		}
		f << "	#6 = SPHERE( ( " << cord << ", 0, 0 ), ( 0, 1, 0 ), ( 0, 0, 1 ), " << radius << " );" << endl;
		f << "	END_SEC_SURFACE;" << endl << endl;
		f << "END_GEOMETRY;" << endl << endl;

		// TOPOLOGY

		f << "TOPOLOGY;" << endl;

		f << "	SEC_VERTEX;" << endl;
		for ( MGSize i=0; i<npoint; ++i)
		f << "	#" << i+1 << " = VERTEX( #" << i+1 << " );" << endl;
		f << "	END_SEC_VERTEX;" << endl << endl;

		f << "	SEC_EDGE;" << endl;
		f << "	#1 = EDGE( #1, ( #1, #3 ), T );" << endl;
		f << "	#2 = EDGE( #2, ( #1, #2 ), T );" << endl;
		f << "	#3 = EDGE( #3, ( #3, #2 ), T );" << endl;
		f << "	#4 = EDGE( #4, ( #4, #6 ), T );" << endl;
		f << "	#5 = EDGE( #5, ( #4, #5 ), T );" << endl;
		f << "	#6 = EDGE( #6, ( #6, #5 ), T );" << endl;
		f << "	#7 = EDGE( #7, ( #1, #4 ), T );" << endl;
		f << "	#8 = EDGE( #8, ( #3, #6 ), T );" << endl;
		f << "	#9 = EDGE( #9, ( #2, #5 ), T );" << endl;

		f << "	#10 = EDGE( #10, ( #7, #8 ), F );" << endl;
		f << "	#11 = EDGE( #11, ( #7, #8 ), F );" << endl;
		f << "	#12 = EDGE( #11, ( #7, #8 ), T );" << endl;
		f << "	END_SEC_EDGE;" << endl << endl;

		f << "	SEC_FACE;" << endl;
		f << "	#1 = FACE( #1, LOOP( ((#2,T),(#3,F),(#1,F)), T ) );" << endl;
		f << "	#2 = FACE( #2, LOOP( ((#4,T),(#6,T),(#5,F)), T ) );" << endl;
		f << "	#3 = FACE( #4, LOOP( ((#5,T),(#9,F),(#2,F),(#7,T))), T ) );" << endl;
		f << "	#4 = FACE( #5, LOOP( ((#3,T),(#9,T),(#6,F),(#8,F))), T ) );" << endl;

		f << "	#5 = FACE( #6, LOOP( ((#10,T),(#11,F)), T ) );" << endl;
		f << "	#6 = FACE( #6, LOOP( ((#10,F),(#12,T)), T ) );" << endl;
		f << "	#7 = FACE( #3, LOOP( ((#11,F),(#12,T)), T ), LOOP( ((#1,T),(#8,T),(#4,F),(#7,F)), T ) );" << endl;
		f << "	END_SEC_FACE;" << endl << endl;

		f << "	SEC_BREP;" << endl;
		f << "	#1 = BREP( #0, LOOP( ((#1,T),(#2,T),(#3,T),(#4,T),(#5,T),(#6,T),(#7,T)), T ) );" << endl;
		f << "	END_SEC_BREP;" << endl << endl;

		f << "END_TOPOLOGY;" << endl;



	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		fprintf( stderr, "%s\n", e.what() );
	}

	return 0;
}
