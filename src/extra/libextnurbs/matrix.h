#ifndef __MATRIX_H__
#define __MATRIX_H__


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace NURBSSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
//	class Matrix
//////////////////////////////////////////////////////////////////////

template <class T>
class Matrix//	: public vector< vector<T> >
{
public:
	Matrix()	{}
	Matrix( const MGSize& n, const MGSize& m);
	~Matrix()	{}
	

    T&			operator()(const MGSize& i, const MGSize& j)		{ return mtab[i*mNCols + j]; }
    const T&	operator()(const MGSize& i, const MGSize& j) const	{ return mtab[i*mNCols + j]; }

	void	Init( const T& d);
	void	Resize( const MGSize& n, const MGSize& m);
	void	Resize( const MGSize& n, const MGSize& m, const T& d);
	//void	Reserve( const MGSize& n, const MGSize& m);
	//void	Clear();
	
	const MGSize&	NRows() const	{ return mNRows;}
	const MGSize&	NCols() const	{ return mNCols;}
	
private:
	MGSize		mNRows;
	MGSize		mNCols;
	vector<T>	mtab;
};
//////////////////////////////////////////////////////////////////////



template <class T>
inline Matrix<T>::Matrix( const MGSize& n, const MGSize& m)
{
	Resize( n, m);
}

template <class T>
inline void Matrix<T>::Resize( const MGSize& n, const MGSize& m)
{
	mNRows = n;
	mNCols = m;
	mtab.resize( mNRows * mNCols);
}

template <class T>
inline void Matrix<T>::Resize( const MGSize& n, const MGSize& m, const T& d)
{
	mNRows = n;
	mNCols = m;
	mtab.resize( mNRows * mNCols, d);
}

//template <class T>
//inline void Matrix<T>::Reserve( const MGSize& n, const MGSize& m)
//{
//	mNRows = n;
//	mNCols = m;
//	mtab.reserve( mNRows * mNCols);
//}


template <class T>
inline void Matrix<T>::Init( const T& d)
{
	for ( MGSize i=0; i<mtab.size(); i++)
		mtab[i] = d;
}

//template <class T>
//inline void Matrix<T>::Clear()
//{
//	mtab.clear();
//}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace NURBS = NURBSSpace;



#endif // __MATRIX_H__
