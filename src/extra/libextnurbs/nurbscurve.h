#ifndef __NURBSCURVE_H__
#define __NURBSCURVE_H__

#include "libcoresystem/mgdecl.h"
#include "libextnurbs/nurbsconst.h"
#include "libextnurbs/wcpoint.h"
#include "libextnurbs/knotvector.h"
#include "libextnurbs/matrix.h"
#include "libcoregeom/vect.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace NURBSSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

template <Dimension DIM, class T>
class NurbsSurface;


//////////////////////////////////////////////////////////////////////
// class NurbsCurve
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class T>
class NurbsCurve
{
	friend class NurbsSurface<DIM,T>;

	typedef typename WCPoint<DIM,T>::WVect WVect;
	typedef typename WCPoint<DIM,T>::GVect GVect;

public:
	NurbsCurve()			{}
	NurbsCurve( const MGSize& ndeg, const vector<T>& tabu, const vector<WVect>& tabcp)
							{ Init(ndeg,tabu,tabcp);}
							

	void	Init( const MGSize& ndeg, const vector<T>& tabu, const vector<WVect>& tabcp)
							{ 
								mUdeg = ndeg;
								InitKnots( tabu); 
								InitCPoints( tabcp);
							}

	void	InitMirror( const NurbsCurve<DIM,T>& crv, const GVect& pnt, const GVect& vn);

	const MGSize&	cUdeg() const						{ return mUdeg;}

	const T&		cKnot( const MGInt& i) const		{ return mtabU[i];}

	MGSize			KnotTabSize() const					{ return mtabU.size();}

	const KnotVector<T>&	cKnotVector() const			{ return mtabU;}


	const WCPoint<DIM,T>&	cCPoint( const MGInt& i) const		{ return mtabCP[i];}

	MGSize			CPointTabSize() const				{ return mtabCP.size();}



	void	CalcPoint( const Vect<DIM_1D,T>& vloc, GVect& vct) const;

			// tabD - tabD[0] - point position; tabD[1] - first derivative ...
	void	CalcDerivs( const Vect<DIM_1D,T>& vloc, const MGSize& nder, vector<GVect>& tabD) const;
	
			// ntmp - no of points for initial search per single span 
			// e1 - a measure of Euclidean dist.
			// e2 - a zero cosine measure
	T		PointProject( const GVect& vct, const MGSize& ntmp=10, 
						const T& e1=NURBS_ZERO, const T& e2=NURBS_ZERO) const;


	void	GlobalInterpol( const vector<GVect>& tabQ, const MGSize& deg );


	void	ExportCrvTEC( char sname[], const MGSize& nop=100 ) const;
	void	ExportCrvTEC( ostream& os, const MGSize& nop=10 ) const;
	void	ExportCPolyTEC( char sname[] );


protected:
	void	InitCPoints( const vector<WVect>& tabcp);
	void	InitKnots( const vector<T>& tabu)		{ mtabU.Init( tabu); }

	void	CalcWDerivs( const Vect<DIM_1D,T>& vloc, const MGSize& nder, vector< WCPoint<DIM,T> >& tabD) const;

	MGSize&	rUdeg()	{ return mUdeg;}

private:
	MGSize						mUdeg;		// degree of the NURBS curve
	vector< WCPoint<DIM,T> >	mtabCP;		// vector of control points
	KnotVector<T>				mtabU ;		// knot vector


};
//////////////////////////////////////////////////////////////////////



template <Dimension DIM, class T>
inline void NurbsCurve<DIM,T>::InitCPoints( const vector<WVect>& tabcp)
{
	mtabCP.resize( tabcp.size() );
	copy( tabcp.begin(), tabcp.end(), mtabCP.begin() );
	//for ( MGSize i=0; i<tabcp.size(); i++)
	//	mtabCP[i] = tabcp[i];

	cout << "tabCP size = " << mtabCP.size() << endl;
}



template <Dimension DIM, class T>
inline void NurbsCurve<DIM,T>::CalcPoint( const Vect<DIM_1D,T>& vloc, GVect& vct) const
{
	MGSize isp;
	vector<T> tabNf;

	isp = mtabU.FindSpan( vloc.cX(), mUdeg);
	mtabU.CalcBasis( isp, vloc.cX(), mUdeg, tabNf);
	WCPoint<DIM,T> pw = WVect( 0.0);
	for ( MGSize j=0; j<=mUdeg; j++)
		pw += tabNf[j] * mtabCP[isp-mUdeg+j];

	vct = pw.Pos();
}


template <Dimension DIM, class T>
inline void NurbsCurve<DIM,T>::CalcWDerivs( const Vect<DIM_1D,T>& vloc, const MGSize& nder, vector<WCPoint<DIM,T> >& tabD) const
{
	MGInt	isp;
	MGSize	nd;

	Matrix<T>	mtx;

	nd = min( nder, mUdeg);
	tabD.resize( nd+1);

	for ( MGSize k=mUdeg+1; k<=nd; k++)
		tabD[k] = WCPoint<DIM,T>( WVect( 0.0) );
		//tabD[k] = 0.0;

	isp = mtabU.FindSpan( vloc.cX(), mUdeg);
	mtabU.CalcDerBasis( isp, vloc.cX(), mUdeg, nd, mtx);

	for ( MGSize k=0; k<=nd; k++)
	{
		tabD[k] = WCPoint<DIM,T>( WVect( 0.0) );
		for ( MGSize j=0; j<=mUdeg; j++)
			tabD[k] += mtx(k,j)*mtabCP[isp-mUdeg+j];
			//tabD[k] += mtx[k][j]*mtabCP[isp-mUdeg+j];
	}

}



template <Dimension DIM, class T>
inline void NurbsCurve<DIM,T>::CalcDerivs( const Vect<DIM_1D,T>& vloc, const MGSize& nder, vector<GVect>& tabD) const
{
	GVect	vct;
	vector<WCPoint<DIM,T> >	tabWD;

	tabD.resize( nder+1);

	CalcWDerivs( vloc.cX(), nder, tabWD);

	//nd = min( nder, mUdeg);

	for ( MGSize k=0; k<=nder; k++)
	{
		if ( k <= mUdeg)
		{
			for ( MGSize j=0; j<DIM; ++j)
				vct.rX(j) = tabWD[k].cX(j);

			for ( MGSize i=1; i<=k; i++)
				vct -= Binomial(k,i) * tabWD[i].cWeight() * tabD[k-i];
		}
		else
			vct = GVect( 0.0);

		tabD[k] = vct / tabWD[0].cWeight();
	}
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace NURBS = NURBSSpace;


#endif // __NURBSCURVE_H__
