#ifndef __NURBSSURFACE_H__
#define __NURBSSURFACE_H__

#include "libcoresystem/mgdecl.h"
#include "libextnurbs/nurbsconst.h"
#include "libextnurbs/wcpoint.h"
#include "libextnurbs/knotvector.h"
#include "libextnurbs/nurbscurve.h"
#include "libcoregeom/vect.h"
#include "libextnurbs/matrix.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace NURBSSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//



//////////////////////////////////////////////////////////////////////
// class NurbsSurface
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class T>
class NurbsSurface
{
	typedef typename WCPoint<DIM,T>::WVect	WVect;
	typedef typename WCPoint<DIM,T>::GVect	GVect;
	typedef Vect<DIM_2D,T>					LVect;

public:
	NurbsSurface()			{}

	NurbsSurface( const MGInt& ndegu, const MGInt& ndegv, const vector<T>& tabu, const vector<T>& tabv, const vector< vector<WVect> >& mtxcp )
				  : mUdeg(ndegu), mVdeg(ndegv), mRefLength(1.0)
							{ 	
								InitUKnots(tabu); 
								InitVKnots(tabv); 
								InitCPoints(mtxcp); 
								FindRefLength();

								Redistribute( mRefVect.cX(), mRefVect.cY() );
							}

	void	Init( const MGInt& ndegu, const MGInt& ndegv, const vector<T>& tabu, const vector<T>& tabv, const vector< vector<WVect> >& mtxcp )
							{ 	
								mUdeg = ndegu;
								mVdeg = ndegv;
								InitUKnots(tabu); 
								InitVKnots(tabv); 
								InitCPoints(mtxcp); 
								FindRefLength();

								Redistribute( mRefVect.cX(), mRefVect.cY() );
							}

	void	InitRuled( const NurbsCurve<DIM,T>& crv1, const NurbsCurve<DIM,T>& crv2 ); 

	void	InitMirror( const NurbsSurface<DIM,T>& srf, const GVect& pnt, const GVect& vn);

	// redistributes positions of knots to make them closer to the real spacing
	void	Redistribute( const T& lengthU, const T& lengthV)
							{	
								//mtabU.Redistribute( 1 );
								//mtabV.Redistribute( 1 );
								mtabU.Redistribute( lengthU );
								mtabV.Redistribute( lengthV );
							}


	void	FindRefLength();

	// surface of revolution - where
	// - crv - rotated curve
	// - bpoint - point def. begining of the axis of revoultion
	// - dir - vector def. the direction of the axis of rev.
	// - theta - revolution angle in deg
	NurbsSurface( const NurbsCurve<DIM,T>& crv, const GVect& bpoint, const GVect& dir, const T& theta);
							
	virtual ~NurbsSurface()	{}


	void	GlobalInterpol( const Matrix<GVect>& mtxQ, const MGInt& degu, const MGInt& degv );

	const MGSize&	cUdeg() const	{ return mUdeg;}
	const MGSize&	cVdeg() const 	{ return mVdeg;}


	const KnotVector<T>&	cKnotVectorU() const	{ return mtabU;}
	const KnotVector<T>&	cKnotVectorV() const	{ return mtabV;}

	MGSize		KnotUTabSize() const				{ return mtabU.size();}
	MGSize		KnotVTabSize() const				{ return mtabV.size();}

	const T&	cKnotU( const MGInt& i) const		{ return mtabU[i];}
	const T&	cKnotV( const MGInt& i) const		{ return mtabV[i];}

	MGSize		CPointMtxUSize() const				{ return mmtxCP.NRows();}
	MGSize		CPointMtxVSize() const				{ return mmtxCP.NCols();}

	const WCPoint<DIM,T>&	cCPoint( const MGInt& i, const MGInt& j) const		{ return mmtxCP(i,j);}


	void	CalcPoint( GVect& vct, const Vect2D& vu) const;

	// mtxD - mtxD(0,0) - point position; mtxD(1,0) - fst u derivative; mtxD(0,1) - fst v derivative ...
	void	CalcDerivs( const Vect2D& vu, const MGSize& nder, Matrix<GVect>& mtxD) const;
	
	Vect2D	FindProjectApprox( const GVect& vct, const MGSize& ntmpu=3, const MGSize& ntmpv=3) const;

	// vres at input is an approximation of the projection
	bool	PointProject( Vect2D& vres, const GVect& vct, const T& e1=NURBS_ZERO, const T& e2=NURBS_ZERO) const;


	void	ExportSrfTEC( const char sname[], const MGSize& nu, const MGSize& nv );
	void	ExportSrfTEC( ostream& os, const MGString& zonename, const MGSize& nu, const MGSize& nv ) const;
	void	ExportCPolyTEC( char sname[] );

protected:
	void	InitCPoints( const vector< vector<WVect> >& mtxcp);
	void	InitUKnots( const vector<T>& tabu)		{ mtabU.Init( tabu); }
	void	InitVKnots( const vector<T>& tabv)		{ mtabV.Init( tabv); }

	MGSize&	rUdeg()	{ return mUdeg;}
	MGSize&	rVdeg()	{ return mVdeg;}


	void	CalcWDerivs( const T& u, const T& v, const MGSize& nder,
						 Matrix< WCPoint<DIM,T> >& tabD ) const;

	void	CalcKnots( const Matrix<GVect>& mtxQ, const MGSize& degu, const MGSize& degv,
					   vector<T>& tabuk, vector<T>& tabvk );


private:
	MGSize					mUdeg;		// degree of the NURBS curve in U dir
	MGSize					mVdeg;		// degree of the NURBS curve in V dir
	KnotVector<T>			mtabU ;		// U knot vector
	KnotVector<T>			mtabV ;		// V knot vector
	Matrix<WCPoint<DIM,T> > mmtxCP;		// matrix of control points

	T						mRefLength;	// reference length
	LVect					mRefVect;	// reference lengths in u and v dirs
};
//////////////////////////////////////////////////////////////////////


template <Dimension DIM, class T>
inline void NurbsSurface<DIM,T>::InitCPoints( const vector< vector<WVect> >& mtxcp)
{
	mmtxCP.Resize( mtxcp.size(), mtxcp[0].size() );
	for ( MGSize i=0; i<mmtxCP.NRows(); i++)
		for ( MGSize j=0; j<mmtxCP.NCols(); ++j)
			mmtxCP(i,j) = mtxcp[i][j];
}


template <Dimension DIM, class T>
inline void NurbsSurface<DIM,T>::CalcPoint( GVect& vct, const Vect2D& vloc) const
{
	MGInt	iusp, ivsp;
	vector<T>	tabNuf, tabNvf;
	vector< WCPoint<DIM,T> >	tab;

	tab.resize( mVdeg+1);

	iusp = mtabU.FindSpan( vloc.cX(), mUdeg);
	ivsp = mtabV.FindSpan( vloc.cY(), mVdeg);

//	printf( "(%20.16lg %20.16lg) %d %d\n", u, v, iusp, ivsp );


	mtabU.CalcBasis( iusp, vloc.cX(), mUdeg, tabNuf);
	mtabV.CalcBasis( ivsp, vloc.cY(), mVdeg, tabNvf);
	
	for ( MGSize j=0; j<=mVdeg; ++j)
	{
		tab[j] = WVect( 0.);
		for ( MGSize k=0; k<=mUdeg; ++k)
			tab[j] += tabNuf[k] * mmtxCP(iusp-mUdeg+k, ivsp-mVdeg+j);
	}
	
	WCPoint<DIM,T> vret = WVect( 0.0);
	for ( MGSize j=0; j<=mVdeg; ++j)
		vret += tabNvf[j]*tab[j];
		
	vret /= vret.cWeight();	
	
	//vct = GVect( vret.X(), vret.Y(), vret.Z() );
	for ( MGSize i=0; i<DIM; ++i)
		vct.rX(i) = vret.cX(i);
}


template <Dimension DIM, class T>
inline void NurbsSurface<DIM,T>::CalcWDerivs( const T& u, const T& v, const MGSize& nder,
						 Matrix< WCPoint<DIM,T> >& mtxD ) const
{
	MGSize 	mnu, mnv;

	Matrix<T>	mtxNuf, mtxNvf;
	vector< WCPoint<DIM,T> > tabtmp( mVdeg+1);

	mtxD.Resize( nder+1, nder+1, WCPoint<DIM,T>( WVect(0.)) ); // resized and initialized with 0

	mnu = min( nder, mUdeg);
	mnv = min( nder, mVdeg);

	
	for ( MGSize k=mUdeg+1; k<=nder; ++k)
		for ( MGSize j=0; j<=nder-k; ++j)
			mtxD(k,j) = WCPoint<DIM,T>( WVect(0.));

	for ( MGSize k=mVdeg+1; k<=nder; ++k)
		for ( MGSize j=0; j<=nder-k; ++j)
			mtxD(j,k) = WCPoint<DIM,T>( WVect(0.));

	
	MGSize iusp = mtabU.FindSpan( u, mUdeg);
	MGSize ivsp = mtabV.FindSpan( v, mVdeg);

	mtabU.CalcDerBasis( iusp, u, mUdeg, mnu, mtxNuf);
	mtabV.CalcDerBasis( ivsp, v, mVdeg, mnv, mtxNvf);

	for ( MGSize k=0; k<=mnu; ++k)
	{
		for ( MGSize s=0; s<=mVdeg; ++s)
		{
			tabtmp[s] = WVect( 0.);
			for ( MGSize r=0; r<=mUdeg; ++r)
				tabtmp[s] += mtxNuf(k,r) * mmtxCP(iusp-mUdeg+r,ivsp-mVdeg+s);
		}

		MGSize dd = min( nder-k, mnv);
		for ( MGSize j=0; j<=dd; ++j)
		{
			mtxD(k,j) = WVect( 0.);
			for ( MGSize s=0; s<=mVdeg; ++s)
				mtxD(k,j) += mtxNvf(j,s) * tabtmp[s];
		}
	}

}						 


template <Dimension DIM, class T>
inline void NurbsSurface<DIM,T>::CalcDerivs( const Vect2D& vloc, const MGSize& nder, Matrix<GVect>& mtxD) const
{
	GVect	vct, vct2;
	Matrix< WCPoint<DIM,T> >	mtxWD;


	CalcWDerivs( vloc.cX(), vloc.cY(), nder, mtxWD);

	MGSize 	mnu, mnv;
	mnu = min( nder, mUdeg);
	mnv = min( nder, mVdeg);


//	for ( i=0; i<=nder;i++)
//	{
//		for (j=0; j<=nder; j++)
//			printf( " (%6.3lf %6.3lf %6.3lf) ", mtxWD[i,j].X(), mtxWD[i,j].Y(), mtxWD[i,j].Z() );
//		printf( "\n");
//	}

	mtxD.Resize( nder+1, nder+1, GVect( 0.0) );
	
	//Matrix<T>	mtxBin;
	//mtxBin.Resize( nder+1, nder+1 );


	for ( MGSize k=0; k<=nder; ++k)
		for ( MGSize j=0; j<=nder-k; ++j)
	//for ( MGSize k=0; k<=mnu; ++k)
	//	for ( MGSize j=0; j<=mnv-k; ++j)
		{
			for (MGSize i=0; i<DIM; ++i)
				vct.rX(i) = mtxWD(k,j).cX(i);
				//vct = GVect( mtxWD(k,j).cX(), mtxWD(k,j).cY(), mtxWD(k,j).cZ() );

			for ( MGSize i=1; i<=j; ++i)
				vct -= Binomial(j,i) * mtxWD(0,i).cWeight() * mtxD(k,j-i);
				//vct -= mtxBin(j,i) * mtxWD(0,i).cWeight() * mtxD(k,j-i);

			for ( MGSize i=1; i<=k; ++i)
			{
				vct -= Binomial(k,i) * mtxWD(i,0).cWeight() * mtxD(k-i,j);
				//vct -= mtxBin(k,i) * mtxWD(i,0).cWeight() * mtxD(k-i,j);
				vct2 = GVect( 0.0);

				for ( MGSize s=1; s<=j; ++s)
					vct2 += Binomial(j,s) * mtxWD(i,s).cWeight() * mtxD(k-i,j-s);
					//vct2 += mtxBin(j,s) * mtxWD(i,s).cWeight() * mtxD(k-i,j-s);
				vct -= static_cast<T>( Binomial(k,i) ) * vct2;
				//vct -= static_cast<T>( mtxBin(k,i) ) * vct2;
			}
			mtxD(k,j) = vct / mtxWD(0,0).cWeight();
		}

	//for ( MGSize k=0; k<=nder; ++k)
	//	for ( MGSize j=0; j<=nder-k; ++j)
	//	{
	//		for (MGSize i=0; i<DIM; ++i)
	//			vct.rX(i) = mtxWD(k,j).cX(i);
	//			//vct = GVect( mtxWD(k,j).cX(), mtxWD(k,j).cY(), mtxWD(k,j).cZ() );

	//		for ( MGSize i=1; i<=j; ++i)
	//			vct -= Binomial(j,i) * mtxWD(0,i).cWeight() * mtxD(k,j-i);

	//		for ( MGSize i=1; i<=k; ++i)
	//		{
	//			vct -= Binomial(k,i) * mtxWD(i,0).cWeight() * mtxD(k-i,j);
	//			vct2 = GVect( 0.0);

	//			for ( MGSize s=1; s<=j; ++s)
	//				vct2 += Binomial(j,s) * mtxWD(i,s).cWeight() * mtxD(k-i,j-s);
	//			vct -= static_cast<T>( Binomial(k,i) ) * vct2;
	//		}
	//		mtxD(k,j) = vct / mtxWD(0,0).cWeight();
	//	}


}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace NURBS = NURBSSpace;



#endif // __NURBSSURFACE_H__

