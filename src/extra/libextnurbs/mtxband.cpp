#include "mtxband.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace NURBSSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


void MtxBand::Decompose()
{
	MGInt	i,j,k,l; 
	MGInt	mm; 
	MGFloat	dum; 

	AllocTmp();

	mm = mMlow+mMup+1; 
	l = mMlow; 
	for (i=0;i<mMlow;i++) 
	{ // Rearrange the storage a bit. 
		for (j=mMlow-i;j<mm;j++) 
			mAmtx[i][j-l] = mAmtx[i][j]; 
		l--; 
		for (j=mm-l-1;j<mm;j++) 
			mAmtx[i][j]=0.0; 
	}

	mD=1.0; 
	l=mMlow; 
	for (k=0;k<mN;k++) 
	{ // For each row... 
		dum=mAmtx[k][0]; 
		i=k; 
		if (l < mN) l++; 
		for (j=k+1;j<l;j++) 
		{ // Find the pivot element. 
			if (fabs(mAmtx[j][0]) > fabs(dum)) 
			{ 
				dum=mAmtx[j][0]; 
				i=j; 
			} 
		} 
		mItab[k]=i; 
		if (dum == 0.0) 
		mAmtx[k][0]=TINY; // Matrix is algorithmically singular, but proceed anyway with TINY pivot (desirable in some applications). 
		if (i != k) 
		{ // Interchange rows. 
			mD = -mD; 
			for (j=0;j<mm;j++) 
			SWAP(mAmtx[k][j],mAmtx[i][j]) 
		} 
		for (i=k+1;i<l;i++) 
		{ // Do the elimination. 
			dum=mAmtx[i][0]/mAmtx[k][0]; 
			mAlmtx[k][i-k]=dum; 
			for (j=1;j<mm;j++) 
				mAmtx[i][j-1]=mAmtx[i][j]-dum*mAmtx[k][j]; 
			mAmtx[i][mm-1]=0.0; 
		} 
	} 
}




void MtxBand::Solve( vector<MGFloat>& mtxB )
{
	MGInt	i,k,l; 
	MGInt	mm; 
	MGFloat	dum; 

	ASSERT( mAlmtx && mItab);

	mm = mMlow+mMup+1; 
	l = mMlow; 
	for (k=0;k<mN;k++) 
	{ // Forward substitution, unscrambling the permuted rows as we go. 
		i=mItab[k]; 
		if (i != k) SWAP(mtxB[k],mtxB[i]) 
		if (l < mN) l++; 
		for (i=k+1;i<l;i++) 
			mtxB[i] -= mAlmtx[k][i-k]*mtxB[k]; 
	} 

	l=1; 
	for (i=mN-1;i>=0;i--) 
	{ // Backsubstitution. 
		dum=mtxB[i]; 
		for (k=1;k<l;k++) 
			dum -= mAmtx[i][k]*mtxB[k+i]; 
		mtxB[i]=dum/mAmtx[i][0]; 
		if (l < mm-1) l++; 
	} 
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

