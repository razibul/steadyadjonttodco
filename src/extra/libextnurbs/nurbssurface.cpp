#include "nurbssurface.h"
#include "libextnurbs/mtxband.h"
#include "libcorecommon/smatrix.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace NURBSSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


template <Dimension DIM, class T>
void NurbsSurface<DIM,T>::FindRefLength()
{
	T vdist = 0;
	for ( MGSize ispu=0; ispu<mmtxCP.NRows(); ++ispu)
	{
		GVect vct0 = mmtxCP(ispu, 0).ProjectW();

		for ( MGSize ispv=1; ispv<mmtxCP.NCols(); ++ispv)
		{
			GVect vct = mmtxCP(ispu, ispv).ProjectW();
			vdist += (vct -vct0).module();
			vct0 = vct;
		}
	}

	vdist /= mmtxCP.NRows();


	T udist = 0;
	for ( MGSize ispv=0; ispv<mmtxCP.NCols(); ++ispv)
	{
		GVect vct0 = mmtxCP(0, ispv).ProjectW();

		for ( MGSize ispu=1; ispu<mmtxCP.NRows(); ++ispu)
		{
			GVect vct = mmtxCP(ispu, ispv).ProjectW();
			udist += (vct -vct0).module();
			vct0 = vct;
		}
	}

	udist /= mmtxCP.NCols();


	mRefVect = LVect(udist,vdist);
	mRefLength = mRefVect.module();

	cout << "NURBS surf ref lenghts = [" << udist << "," << vdist << "] = " << mRefLength << endl;

////////////
	//GVect vctmin( mmtxCP(0,0).ProjectW() );
	//GVect vctmax( mmtxCP(0,0).ProjectW() );

	//for ( MGSize ispu=0; ispu<mmtxCP.NRows(); ++ispu)
	//{
	//	for ( MGSize ispv=0; ispv<mmtxCP.NCols(); ++ispv)
	//	{
	//		GVect vct = mmtxCP(ispu, ispv).ProjectW();

	//		if ( vct.cX() < vctmin.cX() )
	//			vctmin.rX() = vct.cX();
	//		if ( vct.cY() < vctmin.cY() )
	//			vctmin.rY() = vct.cY();

	//		if ( vct.cX() > vctmax.cX() )
	//			vctmax.rX() = vct.cX();
	//		if ( vct.cY() > vctmax.cY() )
	//			vctmax.rY() = vct.cY();
	//	}
	//}

	//mRefLength = (vctmax - vctmin).module();

	//cout << "NURBS surf ref lenght = " << mRefLength << endl;
}



template <Dimension DIM, class T>
void NurbsSurface<DIM,T>::InitRuled( const NurbsCurve<DIM,T>& crv1, const NurbsCurve<DIM,T>& crv2 )
{
	// TODO :: check that both curves have the same knot vector, degree etc.

	//
	mUdeg = 1;
	mVdeg = crv1.mUdeg;
	mtabV = crv1.mtabU;

	mtabU.resize( 4);
	mtabU[0] = mtabU[1] = 0.0;
	mtabU[2] = mtabU[3] = 1.0;

	mmtxCP.Resize( mtabU.size()-mUdeg-1, mtabV.size()-mVdeg-1 );

	for ( MGSize j=0; j<crv1.mtabCP.size(); ++j)
	{
		mmtxCP(0,j) = crv1.mtabCP[j];
		mmtxCP(1,j) = crv2.mtabCP[j];
	}

}

template <Dimension DIM, class T>
void NurbsSurface<DIM,T>::InitMirror( const NurbsSurface<DIM,T>& srf, const GVect& pnt, const GVect& vn)
{
	mUdeg = srf.mUdeg;
	mVdeg = srf.mVdeg;
	mtabU = srf.mtabU;
	mtabV = srf.mtabV;

	GVect vnorm = vn.versor();

	mmtxCP.Resize( srf.CPointMtxUSize(), srf.CPointMtxVSize() );

	for ( MGSize i=0; i<srf.CPointMtxUSize(); ++i)
		for ( MGSize j=0; j<srf.CPointMtxVSize(); ++j)
		{
			GVect vec = srf.cCPoint(i,j).Pos() - pnt;

			GVect vv = (vec * vnorm) * vnorm;
			vec -= 2.0 * vv;
			vec += pnt;


			for ( MGSize k=0; k<DIM; ++k)
				mmtxCP(i,j).rX(k) = vec.cX(k) * srf.mmtxCP(i,j).cWeight();

			mmtxCP(i,j).rWeight() = srf.mmtxCP(i,j).cWeight();

		}

}


template <Dimension DIM, class T>
NurbsSurface<DIM,T>::NurbsSurface( const NurbsCurve<DIM,T>& crv, const GVect& bpoint, const GVect& dir, const T& theta)
{
	MGInt	i, j, nu, mu;
	MGInt	narcs;
	T	dtheta;
	GVect	vdir;

	vdir = dir;
	vdir = vdir.versor();

	// initialization of knot vectors
	mUdeg = 2;
	mVdeg = crv.mUdeg;
	mtabV = crv.mtabU;

//	for ( i=0; i<mtabV.size(); i++)
//		printf( "%lg   %lg\n", mtabV[i], crv.mtabU[i]);


	if ( theta <= 90.0)
	{
		narcs = 1;
		nu = 3 + 2*(narcs - 1);
		mu = nu + mUdeg + 1;
		mtabU.resize( mu);
	}
	else if ( theta <= 180.0)
	{
		narcs = 2;
		nu = 3 + 2*(narcs - 1);
		mu = nu + mUdeg + 1;
		mtabU.resize( mu);

		mtabU[3] = mtabU[4] = 0.5;
	}
	else if ( theta <= 270.0)
	{
		narcs = 3;
		nu = 3 + 2*(narcs - 1);
		mu = nu + mUdeg + 1;
		mtabU.resize( mu);

		mtabU[3] = mtabU[4] = 1.0/ 3.0;
		mtabU[5] = mtabU[6] = 2.0/ 3.0;
	}
	else
	{
		narcs = 4;
		nu = 3 + 2*(narcs - 1);
		mu = nu + mUdeg + 1;
		mtabU.resize( mu);

		mtabU[3] = mtabU[4] = 0.25;
		mtabU[5] = mtabU[6] = 0.5;
		mtabU[7] = mtabU[8] = 0.75;
	}

	j = nu;
	for ( i=0; i<3; j++, i++)
	{
		mtabU[i] = 0.0;
		mtabU[j] = 1.0;
	}

	mmtxCP.Resize( mtabU.size()-mUdeg-1, mtabV.size()-mVdeg-1 );

//	printf( "%d %d\n", mtabU.size()-mUdeg-1, mtabV.size()-mVdeg-1 );
//	for ( i=0; i<mtabU.size(); i++)
//		printf( "%lg  \n", mtabU[i]);


	// calculation of knot-points and weights
	GVect	vctp0, vctp2, vproj, vx, vy, vtproj;
	GVect	vn0, vn2, vn;
	MGInt	index;
	T	r, angle, rtheta, wm;
	vector<T> cosines(10), sines(10);
	SMatrix<3,T>	mtxA(3,3), mtxB(3,1), mtx;
	
	vn = dir;
	rtheta = theta*M_PI/180.0;
	dtheta = rtheta / narcs;
	wm = cos( dtheta/2.0);
	angle = 0;

	for ( i=1; i<=narcs; i++)
	{
		angle += dtheta;
		cosines[i] = cos( angle);
		sines[i] = sin( angle);
	}

	for ( MGSize j=0; j<crv.mtabCP.size(); j++)
	{
		vctp0 = crv.mtabCP[j].Pos();
//		vctp0 = GVect( crv.mtabCP[j].X(), crv.mtabCP[j].Y(), crv.mtabCP[j].Z() );
		vproj = (vdir * ( vctp0 - bpoint)) * vdir + bpoint;
		vx = vctp0 - vproj;
		r = vx.module();
		if ( fabs(r) < ZERO )
		{
			mmtxCP(0,j) = crv.mtabCP[j];
			index = 0;
			for ( i=1; i<=narcs; i++)
			{
				mmtxCP(index+2,j) = crv.mtabCP[j];
				mmtxCP(index+1,j) = crv.mtabCP[j];
				index += 2;
			}
		}
		else
		{
			vn0 = vx = vx / r;
			vy = vdir % vx;

			mmtxCP(0,j) = crv.mtabCP[j];

			index = 0;
			for ( i=1; i<=narcs; i++)
			{
				vn2 = r*cosines[i]*vx + r*sines[i]*vy;
				vctp2 = vproj + vn2;
				vn2 = vn2.versor();

				mmtxCP(index+2,j)			= WCPoint<DIM,T>( vctp2 * crv.mtabCP[j].cWeight());
				mmtxCP(index+2,j).rWeight()	= crv.mtabCP[j].cWeight();

				mtxA(0,0) = vn.cX();   mtxA(0,1) = vn.cY();   mtxA(0,2) = vn.cZ();
				mtxA(1,0) = vn0.cX();  mtxA(1,1) = vn0.cY();  mtxA(1,2) = vn0.cZ();
				mtxA(2,0) = vn2.cX();  mtxA(2,1) = vn2.cY();  mtxA(2,2) = vn2.cZ();

				mtxB(0,0) = vn  * vproj;
				mtxB(1,0) = vn0 * vctp0;
				mtxB(2,0) = vn2 * vctp2;

				mtxA.Gauss( mtxB); 

				mmtxCP(index+1,j)		= WCPoint<DIM,T>( GVect( mtxB(0,0), mtxB(1,0), mtxB(2,0) ) * wm * crv.mtabCP[j].cWeight() );
				mmtxCP(index+1,j).rWeight()	= wm * crv.mtabCP[j].cWeight();

				index += 2;
				vctp0 = vctp2;
				vn0 = vn2;
			}
		}
	}
}



template <Dimension DIM, class T>
Vect2D NurbsSurface<DIM,T>::FindProjectApprox( const GVect& vct, const MGSize& ntmpu, const MGSize& ntmpv) const
{
	T	u, v, du, dv, umin, vmin, dst, dstmin;
	GVect	vtmp, vr;

	// looking for the best approximation

	dstmin = 0;

	bool	bFst = true;

	for ( MGSize ispu=mUdeg; ispu<mmtxCP.NRows(); ++ispu)
	{
		//du = (mtabU[ispu+1] - mtabU[ispu]) / (ntmpu-1);
		//u = mtabU[ispu];
		du = (mtabU[ispu+1] - mtabU[ispu]) / ntmpu;
		u = mtabU[ispu] + 0.5*du;

		for ( MGSize i=0; i<ntmpu; ++i)
		{
			for ( MGSize ispv=mVdeg; ispv<mmtxCP.NCols(); ++ispv)
			{

				//dv = (mtabV[ispv+1] - mtabV[ispv]) / (ntmpv-1);
				//v = mtabV[ispv];
				dv = (mtabV[ispv+1] - mtabV[ispv]) / ntmpv;
				v = mtabV[ispv] + 0.5*dv;

				for ( MGSize j=0; j<ntmpv; ++j)
				{
					CalcPoint( vtmp,  Vect2D( u, v) );
					dst = (vtmp - vct).module();
					if ( dst < dstmin || bFst)
					{
						umin = u;
						vmin = v;
						dstmin = dst;
						bFst = false;
					}
					v += dv;
				}
			}
			u += du;
		}
	}

	//printf( "(%20.16lg, %20.16lg)\n", umin, vmin );

	return Vect2D( umin, vmin);
}


template <Dimension DIM, class T>
bool NurbsSurface<DIM,T>::PointProject( Vect2D& vres, const GVect& vct, const T& e1, const T& e2) const
{
	T	u, v, du, dv, dst;
	T	cc1, cc2, cc31, cc32, cc4;

	GVect	vtmp, vr;
	Matrix<GVect>	mtxD;

	SMatrix<2,T>	mtxJ;
	T	mtxK[2];

	T un = vres.cX();
	T vn = vres.cY();

	const MGSize itermax = 400;


	////
	CalcPoint( vtmp, vres);
	vtmp -= vct;

	if ( vtmp.module() < 1.0e-6*mRefLength )
		return true;


	// try to solve projection problem

	MGSize iter = 0;
		
	do
	{
		u = un;
		v = vn;

		CalcDerivs( Vect2D(u, v), 2, mtxD);

		vr = mtxD(0,0) - vct;

		//if ( vr.module() < 1.0e-6*mRefLength )
		//	break;

		mtxJ(0,0) = mtxD(1,0) * mtxD(1,0);
		mtxJ(1,1) = mtxD(0,1) * mtxD(0,1);
		mtxJ(1,0) = mtxJ(0,1) = mtxD(1,0) * mtxD(0,1);

		mtxK[0] = - (mtxD(1,0)* ( mtxD(0,0) - vct));
		mtxK[1] = - (mtxD(0,1)* ( mtxD(0,0) - vct));


		//mtxJ(0,0) = mtxD(1,0) * mtxD(1,0) + vr * mtxD(2,0);
		//mtxJ(1,1) = mtxD(0,1) * mtxD(0,1) + vr * mtxD(0,2);
		//mtxJ(1,0) = mtxJ(0,1) = mtxD(1,0) * mtxD(0,1) + vr * mtxD(1,1);
		//mtxK[0] = - (mtxD(1,0)* ( mtxD(0,0) - vct));
		//mtxK[1] = - (mtxD(0,1)* ( mtxD(0,0) - vct));


		dst = mtxJ(1,0) * mtxJ(0,1) - mtxJ(0,0)*mtxJ(1,1);

		// singularity - one of the derivatives is 0
		//if ( fabs(dst ) < 1.0e-20)
		if ( dst == 0.0)
			return false;

		du = mtxK[1] * mtxJ(0,1) - mtxK[0]*mtxJ(1,1);
		dv = mtxJ(1,0) * mtxK[0] - mtxJ(0,0)*mtxK[1];

		du /= dst;
		dv /= dst;

		un = u + du;
		vn = v + dv;


		// correction of un for a curve which is NOT closed 
		if ( un < mtabU.UMin())
		{
			un = mtabU.UMin();
			du = 0.0;
		}
		if ( un > mtabU.UMax())
		{
			un = mtabU.UMax();
			du = 0.0;
		}

		// correction of vn for a curve which is NOT closed 
		if ( vn < mtabV.UMin())
		{
			vn = mtabV.UMin();
			dv = 0.0;
		}
		if ( vn > mtabV.UMax())
		{
			vn = mtabV.UMax();
			dv = 0.0;
		}


			
		cc1 = (du*mtxD(1,0) + dv*mtxD(0,1) ).module();
		cc2 = (mtxD(0,0) - vct).module();

		cc31 = fabs( mtxK[0]);
		cc32 = fabs( mtxK[1]);

		cc4 = Vect2D( du, dv).module();

		++iter;

	}
	while ( (cc1 > e1*mRefLength || cc2 > /*e2*/1.0e-6*mRefLength) && iter < itermax );

	vres = Vect2D( un, vn);


	if ( iter >= itermax)
	{
		//cout << "NurbsSurface<DIM,T>::PointProject : max iter limit reached " << cc1 << " "  << cc2 /*<< " " << cc31 << " " << cc32*/ << endl;
		
		if ( cc2 < 1.0e-4*mRefLength )
			return true;

		cout << "NurbsSurface<DIM,T>::PointProject : max iter limit reached " << cc1 << " "  << cc2 /*<< " " << cc31 << " " << cc32*/ << endl;

		return false;
	}
	
	return true;
}





template <Dimension DIM, class T>
void NurbsSurface<DIM,T>::GlobalInterpol( const Matrix<GVect>& mtxQ, const MGInt& degu, const MGInt& degv )
{
	MGInt	n, m, i, j, k;
	MGInt	isp;
	GVect	dv;

	vector<T>	tabNf;
	vector<T>	tabuk, tabvk;
	Matrix< WCPoint<DIM,T> > mtxCR;

	CalcKnots( mtxQ, degu, degv, tabuk, tabvk);

	n = mtxQ.NRows();
	m = mtxQ.NCols();

	MtxBand		Au(n, mUdeg, mUdeg);
	vector<T>	Bu(n);
	MtxBand		Av(m, mVdeg, mVdeg);
	vector<T>	Bv(m);

	//for (k=0; k<n; k++) printf( "tabuk[%d] = %lg\n", k, tabuk[k]);
	//printf( "\n");
	//for (k=0; k<m; k++) printf( "tabvk[%d] = %lg\n", k, tabvk[k]);


	mtxCR.Resize( n, m);
	for ( k=0; k<m; k++)
	{
		// preparing the A matrix
		Au.Init( 0.0);
		for ( i=0; i<n; i++)
		{
			isp = mtabU.FindSpan( tabuk[i], mUdeg);
			mtabU.CalcBasis( isp, tabuk[i], mUdeg, tabNf);
			for ( MGSize j=0; j<=mUdeg; j++)
				Au(i, isp+j-i) = tabNf[j];
		}

		// solving system of equation with LU decomposition of band matrix
		Au.Decompose();

		for ( j=0; j<3; j++)
		{
			for ( i=0; i<n; i++)
				Bu[i] = mtxQ(i,k).cX(j);
			Au.Solve( Bu);
			for ( i=0; i<n; i++)
				mtxCR(i,k).rX(j) = Bu[i];
		}

		for ( i=0; i<n; i++)
			mtxCR(i,k).rW() = 1.0;

		// cleaning temporaries used for calc. of solution
		Au.Clean();
	}

	mmtxCP.Resize( n, m);
	for ( k=0; k<n; k++)
	{
		// preparing the A matrix
		Av.Init( 0.0);
		for ( i=0; i<m; i++)
		{
			isp = mtabV.FindSpan( tabvk[i], mVdeg);
			mtabV.CalcBasis( isp, tabvk[i], mVdeg, tabNf);
			for ( MGSize j=0; j<=mVdeg; j++)
				Av(i, isp+j-i) = tabNf[j];
		}

		// solving system of equation with LU decomposition of band matrix
		Av.Decompose();

		for ( j=0; j<DIM; j++)
		{
			for ( i=0; i<m; i++)
				Bv[i] = mtxCR(k,i).cX(j);
			Av.Solve( Bv);
			for ( i=0; i<m; i++)
				mmtxCP(k,i).rX(j) = Bv[i];
		}

		for ( i=0; i<m; i++)
			mmtxCP(k,i).rWeight() = 1.0;

		// cleaning temporaries used for calc. of solution
		Av.Clean();
	}


	FILE *f=fopen( "nurbssrv.def", "wt");
	fprintf( f, "%d\n", mUdeg );

	fprintf( f, "\n");
	for ( MGSize i=0; i<mtabU.size(); ++i)
		fprintf( f, "%20.16lg\n", mtabU[i] );

	fprintf( f, "\n");
	for ( MGSize i=0; i<mtabV.size(); ++i)
		fprintf( f, "%20.16lg\n", mtabV[i] );

	fprintf( f, "\n");
	for ( MGSize i=0; i<mmtxCP.NRows(); ++i)
	{
		for ( MGSize j=0; j<mmtxCP.NCols(); ++j)
		{
			for ( MGSize k=0; k<DIM; ++k)
				fprintf( f, "%20.16lg \n", mmtxCP(i,j).cX(k) );
			fprintf( f, "%20.16lg\n", mmtxCP(i,j).cWeight() );
		}
	}

	fclose( f);

}



template <Dimension DIM, class T>
void NurbsSurface<DIM,T>::CalcKnots( const Matrix<GVect>& mtxQ, const MGSize& degu, const MGSize& degv,
							 vector<T>& tabuk, vector<T>& tabvk )
{
	T	tot, d;

	vector<T>	tabdst;
	vector<T>	tabu, tabv;

	MGSize n = mtxQ.NRows();
	MGSize m = mtxQ.NCols();

	// calculating values of  Uk
	tabuk.resize( n);
	tabdst.resize( n);

	tabuk[0] = 0.0;
	tabuk[n-1] = 1.0;

	MGSize num = m;

	for ( MGSize k=1; k<n-1; ++k)
		tabuk[k] = 0.0;

	for ( MGSize j=0; j<m; ++j)
	{
		tot = 0.0;
		for ( MGSize k=1; k<n; ++k)
		{
			tabdst[k] = (mtxQ(k,j) - mtxQ(k-1,j)).module();
			tot += tabdst[k];
		}
		if ( fabs( tot) < ZERO )
			num--;
		else
		{
			d = 0.0;
			for ( MGSize k=1; k<n-1; ++k)
			{
				d += tabdst[k];
				tabuk[k] += d/tot;
			}
		}
	}

	if ( num == 0)
		THROW_INTERNAL( "bad point set");

	for ( MGSize k=1; k<n-1; k++)
		tabuk[k] /= (T)num;

	// calculating knot vector U
	num = n + degu + 1;

	tabu.resize( num);
	for ( MGSize j=0; j<=degu; ++j)
		tabu[j] = 0.0;

	for ( MGSize j=num-degu-1; j<num; ++j)
		tabu[j] = 1.0;

	for ( MGSize j=1; j<n-degu; ++j)
	{
		tabu[j+degu] = 0.0;
		for ( MGSize i=j; i<j+degu; ++i)
			tabu[j+degu] += tabuk[i];

		tabu[j+degu] /= (T)degu;
	}

	mUdeg = degu;
	mtabU.Init( tabu);

	// calculating values of Vk
	tabvk.resize( m);
	tabdst.resize( m);

	tabvk[0] = 0.0;
	tabvk[m-1] = 1.0;

	num = n;
	for ( MGSize k=1; k<m-1; ++k)
		tabvk[k] = 0.0;

	for ( MGSize j=0; j<n; ++j)
	{
		tot = 0.0;
		for ( MGSize k=1; k<m; ++k)
		{
			tabdst[k] = (mtxQ(j,k) - mtxQ(j,k-1)).module();
			tot += tabdst[k];
		}
		if ( fabs( tot) < ZERO )
			num--;
		else
		{
			d = 0.0;
			for ( MGSize k=1; k<m-1; ++k)
			{
				d += tabdst[k];
				tabvk[k] += d/tot;
			}
		}
	}

	if ( num == 0)
		THROW_INTERNAL( "bad point set");

	for ( MGSize k=1; k<m-1; ++k)
	{
		tabvk[k] /= (T)num;
	}

	// calculating knot vector V
	num = m + degv + 1;

	tabv.resize( num);
	for ( MGSize j=0; j<=degv; ++j)
		tabv[j] = 0.0;

	for ( MGSize j=num-degv-1; j<num; ++j)
		tabv[j] = 1.0;

	for ( MGSize j=1; j<m-degv; ++j)
	{
		tabv[j+degv] = 0.0;
		for ( MGSize i=j; i<j+degv; ++i)
			tabv[j+degv] += tabvk[i];

		tabv[j+degv] /= (T)degv;
	}

	mVdeg = degv;
	mtabV.Init( tabv);

}


template <Dimension DIM, class T>
void NurbsSurface<DIM,T>::ExportSrfTEC( const char sname[], const MGSize& nu, const MGSize& nv )
{
	FILE *f;
	T	v, dv, u, du;
	GVect	vct;

	if ( !( f = fopen( sname, "wt") ))
		THROW_FILE( "opening error", sname);

	du = ( mtabU.UMax() - mtabU.UMin() ) / ( nu - 1);
	dv = ( mtabV.UMax() - mtabV.UMin() ) / ( nv - 1);

	fprintf( f, "VARIABLES = " );
	for ( MGSize k=1; k<=DIM; ++k)
		fprintf( f, "\"X%1d\",", k );
	fprintf( f, "\"U\",\"V\"\n" );
	fprintf( f, "ZONE I=%d, J=%d, F=POINT\n", nu, nv );

	v = mtabV.UMin();
	for ( MGSize j=0; j<nv; j++)
	{
		u = mtabU.UMin();
		for ( MGSize i=0; i<nu; i++)
		{
			CalcPoint( vct, Vect2D(u, v) );
			for ( MGSize k=0; k<DIM; ++k)
				fprintf( f, "%20.16lg ", vct.cX(k) );
			fprintf( f, "%20.16lg %20.16lg \n", u, v );
			u += du;
		}
		v += dv;
	}

	fclose( f);
}


template <Dimension DIM, class T>
void NurbsSurface<DIM,T>::ExportSrfTEC( ostream& os, const MGString& zonename, const MGSize& nu, const MGSize& nv ) const
{
	Matrix<GVect>	mtxD;
	T	v, dv, u, du;
	GVect	vct;

	du = ( mtabU.UMax() - mtabU.UMin() ) / ( nu - 1);
	dv = ( mtabV.UMax() - mtabV.UMin() ) / ( nv - 1);

	os << "VARIABLES = ";

	//os << "\"U\",\"V\"";
	os << "\"U1\",\"U2\"";

	for ( MGSize k=1; k<=DIM; ++k)
		os << ",\"X" << k << "\"";

	for ( MGSize k=1; k<=DIM; ++k)
		os << ",\"dU1_X" << k << "\"";

	for ( MGSize k=1; k<=DIM; ++k)
		os << ",\"dU2_X" << k << "\"";

	os << endl;

	os << "ZONE T=\"" << zonename << "\"I=" << nu << ", J=" << nv << ", F=POINT\n";

	v = mtabV.UMin();
	for ( MGSize j=0; j<nv; j++)
	{
		u = mtabU.UMin();
		for ( MGSize i=0; i<nu; i++)
		{
			CalcDerivs( Vect2D(u, v), 1, mtxD);

			os << u << " " << v << " ";

			for ( MGSize k=0; k<DIM; ++k)
				os << mtxD(0,0).cX(k) << " ";

			for ( MGSize k=0; k<DIM; ++k)
				os << mtxD(1,0).cX(k) << " ";

			for ( MGSize k=0; k<DIM; ++k)
				os << mtxD(0,1).cX(k) << " ";

			os <<endl;

			u += du;
		}
		v += dv;
	}

}



template <Dimension DIM, class T>
void NurbsSurface<DIM,T>::ExportCPolyTEC( char sname[] )
{
	FILE *f;

	if ( !( f = fopen( sname, "wt") ))
		THROW_FILE( "opening error", sname);

	fprintf( f, "VARIABLES = \"X\",\"Y\",\"Z\"\n" );
	fprintf( f, "ZONE I=%d, J=%d F=POINT\n", mmtxCP.NRows(), mmtxCP.NCols() );

	for ( MGSize j=0; j<mmtxCP.NCols(); j++)
		for ( MGSize i=0; i<mmtxCP.NRows(); i++)
		{
			GVect vct = mmtxCP(i,j).Pos();
			fprintf( f, "%20.16lg %20.16lg %20.16lg \n", vct.cX(), vct.cY(), vct.cZ() );
		}

	fclose( f);
}

template class NurbsSurface<DIM_3D,MGFloat>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


