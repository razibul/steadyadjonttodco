#include "nurbscurve.h"
#include "libextnurbs/mtxband.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace NURBSSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//



template <Dimension DIM, class T>
void NurbsCurve<DIM,T>::InitMirror( const NurbsCurve<DIM,T>& crv, const GVect& pnt, const GVect& vn)
{
	mUdeg = crv.mUdeg;
	mtabU = crv.mtabU;

	GVect vnorm = vn.versor();

	mtabCP.resize( crv.CPointTabSize() );
	for ( MGSize i=0; i<crv.CPointTabSize(); ++i)
	{
		GVect vec = crv.cCPoint(i).Pos() - pnt;

		GVect vv = (vec * vnorm) * vnorm;
		vec -= 2.0 * vv;
		vec += pnt;


		for ( MGSize k=0; k<DIM; ++k)
			mtabCP[i].rX(k) = vec.cX(k) * crv.mtabCP[i].cWeight();

		mtabCP[i].rWeight() = crv.mtabCP[i].cWeight();

	}

}


template <Dimension DIM, class T>
T NurbsCurve<DIM,T>::PointProject( const GVect& vct, const MGSize& ntmp, const T& e1, const T& e2) const
{
	bool	bFst = true, bRFst = true;
	T	u, du, dst, dstmin, umin, un;
	T	cc1, cc2, cc3;
	GVect	vret, vtmp;
	
	vector<GVect>	tabD;
	
	dstmin = 0;

//	printf( "ntmp = %d\n", ntmp );
	for ( MGSize isp=mUdeg; isp<mtabCP.size(); isp++)
	{
		du = (mtabU[isp+1] - mtabU[isp]) / (ntmp-1);
		u = mtabU[isp];
		for ( MGSize i=0; i<ntmp; i++)
		{
			CalcPoint( Vect<DIM_1D,T>( u), vtmp );
			dst = (vtmp - vct).module();	
			if ( dst < dstmin || bFst)
			{
				umin = u;
				dstmin = dst;
				bFst = false;
			}
			u += du;
		}
	}
		
	un = umin;

	CalcPoint( Vect<DIM_1D,T>( un), vtmp );
	//cout << "starting point = " ;
	//vtmp.Write();

		
	do
	{
		u = un;
		
		CalcDerivs( u, 2, tabD);

		//if ( (tabD[0] - vct).module() < e1 )
		//	break;

		//cout << "u   = " << u << endl;
		//cout << "pos = "; tabD[0].Write();
		//cout << "fst = "; tabD[1].Write();

		dst = tabD[1]*(tabD[0] - vct);
		dstmin = tabD[2]*(tabD[0] - vct) + fabs( tabD[1]*tabD[1]);
		un = u - dst/dstmin;

		// correction of un for a curve which is NOT closed 
		if ( un < mtabU.UMin())
			un = mtabU.UMin();
		if ( un > mtabU.UMax())
			un = mtabU.UMax();
			
		cc1 = ((un - u)*tabD[1]).module();
		cc2 = (tabD[0] - vct).module();
		cc3 = fabs( dst); // / cc2 / tabD[1].module();
		
		//cout << cc1 << " " << cc2 << " " << cc3 << endl;
	}
	while ( cc1 > e1 && cc2 > e1 && cc3 > e2);
		
	return un;
}



template <Dimension DIM, class T>
void NurbsCurve<DIM,T>::GlobalInterpol( const vector<GVect>& tabQ, const MGSize& deg )
{
	GVect	dv;
	T	dlength, dtmp;
	vector<T>	tabu, tabuk, tabNf;
	vector<GVect>	tabP;

	mUdeg = deg;
	MGSize n = tabQ.size();
	MGSize m = n + deg + 1;

	MtxBand		A(n, mUdeg, mUdeg);
	vector<T>	B(n);


	tabuk.resize( n);
	tabu.resize( m);

	dlength = 0;
	for ( MGSize i=1; i<n; i++)
	{
		dv = tabQ[i] - tabQ[i-1];
		dlength += dv.module();
	}

	tabuk[0] = 0.0;
	for ( MGSize i=1; i<n; i++)
	{
		dv = tabQ[i] - tabQ[i-1];
		dtmp = dv.module() / dlength;
		tabuk[i] = tabuk[i-1] + dv.module() / dlength;
	}

	for ( MGSize j=0; j<=deg; j++)
		tabu[j] = 0.0;

	for ( MGSize j=m-deg-1; j<m; j++)
		tabu[j] = 1.0;

	for ( MGSize j=1; j<n-deg; j++)
	{
		tabu[j+deg] = 0.0;
		for ( MGSize i=j; i<j+deg; i++)
			tabu[j+deg] += tabuk[i];

		tabu[j+deg] /= static_cast<T>( deg);
	}

	mtabU.Init( tabu);

//	for ( i=0; i<mtabU.size(); i++)
//		printf( "%lg\n", mtabU[i]);


	// preparing the A matrix
	A.Init( 0.0);
	for ( MGSize i=0; i<n; i++)
	{
		MGSize isp = mtabU.FindSpan( tabuk[i], mUdeg);
		mtabU.CalcBasis( isp, tabuk[i], mUdeg, tabNf);
		for ( MGSize j=0; j<=mUdeg; j++)
			A(i, isp+j-i) = tabNf[j];
	}

	mtabCP.resize( n);

	// solving system of equation with LU decomposition of band matrix
	A.Decompose();

	for ( MGSize j=0; j<DIM; j++)
	{
		for ( MGSize i=0; i<n; i++)
			B[i] = tabQ[i].cX(j);
			//B[i] = tabQ[i].cX(j);

		A.Solve( B);

		for ( MGSize i=0; i<n; i++)
			mtabCP[i].rX(j) = B[i];
			//mtabCP[i].rX(j) = B[i];
	}

	for ( MGSize i=0; i<n; i++)
		mtabCP[i].rWeight() = 1.0;

	// cleaning temporaries used for calc. of solution
	A.Clean();


	FILE *f=fopen( "nurbscrv.def", "wt");
	fprintf( f, "%d\n", mUdeg );

	fprintf( f, "\n");
	for ( MGSize i=0; i<mtabU.size(); i++)
		fprintf( f, "%20.16lg\n", mtabU[i] );

	fprintf( f, "\n");
	for ( MGSize i=0; i<mtabCP.size(); i++)
	{
		for ( MGSize k=0; k<DIM; ++k)
			fprintf( f, "%20.16lg ", mtabCP[i].cX(k) );
		fprintf( f, "%20.16lg\n", mtabCP[i].cWeight() );
		//fprintf( f, "%20.16lg %20.16lg %20.16lg %20.16lg\n", mtabCP[i].cX(), mtabCP[i].cY(), mtabCP[i].cZ(), mtabCP[i].cW() );
	}

	fclose( f);
}



template <Dimension DIM, class T>
void NurbsCurve<DIM,T>::ExportCrvTEC( char sname[], const MGSize& nop ) const
{
	FILE *f;
	MGFloat	u, du;
	GVect	vct;

	if ( !( f = fopen( sname, "wt") ))
		THROW_FILE( "opening error", sname);

	du = ( mtabU.UMax() - mtabU.UMin() ) / ( nop - 1);
	u = mtabU.UMin();

	fprintf( f, "VARIABLES = " );
	for ( MGSize k=0; k<DIM; ++k)
		fprintf( f, "\"X%1d\", ", k+1 );
	fprintf( f, "\"U\"\n" );
	fprintf( f, "ZONE I=%d, F=POINT\n", nop );

	for ( MGSize i=0; i<nop; i++)
	{
		CalcPoint( Vect<DIM_1D,T>( u), vct );
		for ( MGSize k=0; k<DIM; ++k)
			fprintf( f, "%20.16lg ", vct.cX(k) );
		fprintf( f, " %20.16lg \n", u );
		u += du;
	}

	fclose( f);
}


template <Dimension DIM, class T>
void NurbsCurve<DIM,T>::ExportCrvTEC(  ostream& os, const MGSize& nop ) const
{
	MGFloat	u, du;
	GVect	vct;
	vector<GVect> tabD;

	du = ( mtabU.UMax() - mtabU.UMin() ) / ( nop - 1);
	u = mtabU.UMin();

	os << "VARIABLES = ";

	os << "\"U\"";

	for ( MGSize k=0; k<DIM; ++k)
		os << ",\"X" << k+1 << "\"";

	for ( MGSize k=0; k<DIM; ++k)
		os << ",\"dU_X" << k+1 << "\"";

	os << endl;

	os << "ZONE I=" << nop << ", F=POINT\n";


	for ( MGSize i=0; i<nop; i++)
	{
		CalcDerivs( Vect1D(u), 1, tabD);

		os << u << " ";

		for ( MGSize k=0; k<DIM; ++k)
			os << tabD[0].cX(k) << " ";

		for ( MGSize k=0; k<DIM; ++k)
			os << tabD[1].cX(k) << " ";

		os <<endl;

		//CalcPoint( Vect<MGFloat,DIM_1D>( u), vct );
		//for ( MGSize k=0; k<DIM; ++k)
		//	fprintf( f, "%20.16lg ", vct.cX(k) );
		//fprintf( f, " %20.16lg \n", u );

		u += du;
	}

}



template <Dimension DIM, class T>
void NurbsCurve<DIM,T>::ExportCPolyTEC( char sname[] )
{
	ofstream of( sname);

	of << "VARIABLES = \"X\",\"Y\",\"Z\"" << endl;
	of << "ZONE I=" << mtabCP.size() << ", F=POINT" << endl;

	for ( MGSize i=0; i<mtabCP.size(); i++)
	{
		GVect	vct = mtabCP[i].Pos();
		for ( MGSize k=0; k<DIM; ++k)
			of << setw(20) << setprecision(16) << vct.cX(k) << " ";
		of << endl;
	}
}



template class NurbsCurve<DIM_2D,MGFloat>;
template class NurbsCurve<DIM_3D,MGFloat>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

