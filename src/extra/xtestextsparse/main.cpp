
#define VIENNACL_WITH_OPENMP
#define VIENNACL_WITH_OPENCL


#include "libcoresystem/mgdecl.h"
#include "libcorecommon/smatrix.h"


#include "libextget/geotopoio.h"
#include "libextget/geotopomaster.h"

#include "libextsparse/sparsevector.h"
#include "libextsparse/sparsematrix.h"

#include "libextsparse/precond.h"
#include "libextsparse/itersolver.h"

#include "libcorecommon/stopwatch.h"


#include "viennacl/scalar.hpp"
#include "viennacl/vector.hpp"
#include "viennacl/compressed_matrix.hpp"
#include "viennacl/linalg/prod.hpp"
#include "viennacl/linalg/ilu.hpp"
#include "viennacl/linalg/jacobi_precond.hpp"
#include "viennacl/linalg/cg.hpp"
#include "viennacl/linalg/bicgstab.hpp"
#include "viennacl/linalg/gmres.hpp"


const MGSize BSIZE = 5;

typedef double TYPE;


template <std::size_t BSIZE, typename SCALARTYPE>
void copy_flow2( const viennacl::vector_base<SCALARTYPE> & gpu_vec, Sparse::Vector<BSIZE,SCALARTYPE> & cpu_vector)
{
	viennacl::copy( gpu_vec.begin(), gpu_vec.end(), &(cpu_vector[0][0]) );
}


template <std::size_t BSIZE, typename SCALARTYPE>
void copy_flow2( const Sparse::Vector<BSIZE,SCALARTYPE> & cpu_vector, viennacl::vector_base<SCALARTYPE> & gpu_vec)
{
	viennacl::copy( &(cpu_vector[0][0]), (&(cpu_vector[cpu_vector.Size()-1][BSIZE-1]))+1, gpu_vec.begin() );
}


template <std::size_t BSIZE, typename SCALARTYPE, unsigned int ALIGNMENT>
void copy_flow2( const Sparse::Matrix<BSIZE,SCALARTYPE> & cpu_matrix, viennacl::compressed_matrix<SCALARTYPE, ALIGNMENT> & gpu_matrix)
{
	std::size_t nonzeros = 0;
	for ( std::size_t irow=0; irow<cpu_matrix.NRows(); ++irow)
		nonzeros += cpu_matrix.RowSize(irow);

	nonzeros *= BSIZE*BSIZE;

	viennacl::backend::typesafe_host_array<unsigned int> row_buffer(gpu_matrix.handle1(), cpu_matrix.NRows()*BSIZE + 1);
	viennacl::backend::typesafe_host_array<unsigned int> col_buffer(gpu_matrix.handle2(), nonzeros);
	std::vector<SCALARTYPE> elements(nonzeros);

	std::size_t row_index  = 0;
	std::size_t data_index = 0;

	for ( std::size_t irow=0; irow<cpu_matrix.NRows(); ++irow)
	{
		for ( std::size_t ibrow=0; ibrow<BSIZE; ++ibrow)
		{
			row_buffer.set( row_index, data_index);
			++row_index;

			for ( std::size_t k=0; k<cpu_matrix.RowSize(irow); ++k)
			{
				const Sparse::Matrix<BSIZE,SCALARTYPE>::BlockMtx& block = cpu_matrix.GetBlock( irow, k);
				for ( std::size_t ibcol=0; ibcol<BSIZE; ++ibcol)
				{
					std::size_t igcol = cpu_matrix.GetColId(irow,k)*BSIZE + ibcol;

					col_buffer.set( data_index, igcol);
					elements[data_index] = block(ibrow,ibcol);
					++data_index;
				}

			}
		
			data_index = viennacl::tools::roundUpToNextMultiple<std::size_t>(data_index, ALIGNMENT); //take care of alignment
		}
	}

	row_buffer.set(row_index, data_index);

	gpu_matrix.set(row_buffer.get(),
		col_buffer.get(),
		&elements[0], 
		cpu_matrix.NRows()*BSIZE,
		cpu_matrix.NRows()*BSIZE,
		nonzeros);
}



int main( int argc, char* argv[])
{
	StopWatch	swatch;

	try
	{
		const MGSize n = 10000;

		Sparse::Vector<BSIZE,TYPE>	vecX;
		Sparse::Vector<BSIZE,TYPE>	vecRHS;
		Sparse::Matrix<BSIZE,TYPE>	mtxA;

		Sparse::BlockVector<BSIZE,TYPE>	bvect;
		Sparse::BlockMatrix<BSIZE,TYPE>	bmtx;

		for ( MGSize ir=0; ir<BSIZE; ++ir)
		{
			bvect[ir] = 0.0;
			for ( MGSize ic=0; ic<BSIZE; ++ic)
			{
				if ( ir == ic) 
					bmtx(ir,ic) = TYPE(1.2);
				else
					bmtx(ir,ic) = TYPE(0.324);

				bvect[ir] += bmtx(ir,ic);
			}
		}

		//bmtx.Write();
		//bvect.Write();


		mtxA.Resize( n);
		vecRHS.Resize( n);
		vecX.Resize( n);

		for ( int ir=0; ir<n; ++ir)
		{
			vecRHS[ir] = Sparse::BlockVector<BSIZE,TYPE>( TYPE(0.0) );

			if ( ir+2 < n)
			{
				mtxA.InsertBlock( ir, ir+2 );
				mtxA( ir, ir+2 ) = bmtx;
				vecRHS[ir] += bvect;
			}

			if ( ir+4 < n)
			{
				mtxA.InsertBlock( ir, ir+4 );
				mtxA( ir, ir+4 ) = TYPE(3.0)*bmtx;
				vecRHS[ir] += TYPE(3.0)*bvect;
			}

			if ( ir-2 >= 0)
			{
				mtxA.InsertBlock( ir, ir-2 );
				mtxA( ir, ir-2 ) = TYPE(2.0)*bmtx;
				vecRHS[ir] += TYPE(2.0)*bvect;

			}

			if ( ir-4 >= 0)
			{
				mtxA.InsertBlock( ir, ir-4 );
				mtxA( ir, ir-4 ) = bmtx;
				vecRHS[ir] += bvect;

			}

			mtxA.InsertBlock( ir, ir );
			mtxA( ir, ir ) = TYPE(4.0)*bmtx;
			vecRHS[ir] += TYPE(4.0)*bvect;

		}

		//mtxA.DumpMtx( cout);
		//vecRHS.Dump( cout);

		for ( int ir=0; ir<n; ++ir)
			vecX[ir] = Sparse::BlockVector<BSIZE,TYPE>( TYPE(0.0) );

		cout << endl;
	swatch.Reset(); swatch.Start();

		MGSize max_iter 	= 100;
		MGSize m 			= 100;
		TYPE gmres_eps 	= TYPE(1.0e-30);

		Sparse::PreILU<BSIZE,TYPE>	preILU;
		preILU.Init( mtxA);
		GMRES< Sparse::Matrix<BSIZE,TYPE>, Sparse::Vector<BSIZE,TYPE>,Sparse::PreILU<BSIZE,TYPE>,SMatrix<201,TYPE>,SVector<201,TYPE>,TYPE >
			( mtxA, vecX, vecRHS, preILU, m, max_iter, gmres_eps);

		//GMRES< Sparse::Matrix<BSIZE,TYPE>, Sparse::Vector<BSIZE,TYPE>,Sparse::PreDiag<BSIZE,TYPE>,SMatrix<201,TYPE>,SVector<201,TYPE>,TYPE >
		//	( mtxA, vecX, vecRHS, preILU, m, max_iter, gmres_eps);

	swatch.Mark();
	cout << " :: time " << setprecision(6) << swatch.cDuration() << endl;
	cout << max_iter << " " << gmres_eps << endl;

		//vecX.Dump( cout);
		cout << Dot( vecX, vecX) << endl;

		for ( int ir=0; ir<n; ++ir)
			vecX[ir] -= Sparse::BlockVector<BSIZE,TYPE>( TYPE(1.0) );

		cout << vecX.Norm() << "  " << vecX.Norm()/n << endl;

////////////////////////////////////////////////////////////
		// ViennaCL
		cout << endl << "ViennaCL" << endl;

		viennacl::ocl::set_context_device_type(0, viennacl::ocl::gpu_tag());
		
		std::cout << viennacl::ocl::current_device().info() << std::endl;

		//std::vector< std::map< unsigned int, TYPE> > cpu_sparse_matrix(n*BSIZE);
		std::vector<TYPE> cpu_rhs(n*BSIZE,0.0);
		std::vector<TYPE> cpu_results(n*BSIZE,0.0);

		//for ( int ir=0; ir<n; ++ir)
		//{
		//	for ( int ic=0; ic<n; ++ic)
		//		if ( mtxA.Exist(ir,ic) )
		//		{
		//			for ( int ibr=0; ibr<BSIZE; ++ibr)
		//				for ( int ibc=0; ibc<BSIZE; ++ibc)
		//					cpu_sparse_matrix[ir*BSIZE+ibr][ic*BSIZE+ibc] = (TYPE)mtxA(ir,ic)(ibr,ibc);
		//		}

		//	for ( int ibr=0; ibr<BSIZE; ++ibr)
		//		cpu_rhs[ir*BSIZE+ibr] = (TYPE)vecRHS[ir][ibr];
		//}
		
		for ( int ir=0; ir<n; ++ir)
			for ( int ibr=0; ibr<BSIZE; ++ibr)
				cpu_rhs[ir*BSIZE+ibr] = (TYPE)vecRHS[ir][ibr];


		viennacl::compressed_matrix<TYPE> vcl_compressed_matrix(n*BSIZE,n*BSIZE);
		viennacl::vector<TYPE> vcl_rhs(n*BSIZE); 
		viennacl::vector<TYPE> vcl_result(n*BSIZE);

		copy_flow2( mtxA, vcl_compressed_matrix );
		copy_flow2( vecRHS, vcl_rhs);

		//for ( int i=0; i<cpu_sparse_matrix.size(); ++i)
		//{
		//	for ( int j=0; j<cpu_sparse_matrix.size(); ++j)
		//	{
		//		if ( cpu_sparse_matrix[i].find(j) != cpu_sparse_matrix[i].end() )
		//			cout << setw(6) << cpu_sparse_matrix[i][j];
		//		else
		//			cout << setw(6) << 0;
		//	}
		//	cout << endl;
		//}

		//viennacl::copy( &cpu_rhs[0], (&cpu_rhs[cpu_rhs.size()-1])+1, vcl_rhs.begin() );
		//viennacl::copy( cpu_rhs, vcl_rhs);

	cout << "vcl starts" << endl;
	swatch.Reset(); swatch.Start();

		//viennacl::linalg::ilu0_tag ilu0_config;
		//viennacl::linalg::ilu0_precond< viennacl::compressed_matrix<TYPE> > vcl_ilut( vcl_compressed_matrix, ilu0_config);

		viennacl::linalg::gmres_tag custom(gmres_eps, max_iter, m);
		//viennacl::linalg::bicgstab_tag custom(gmres_eps, max_iter, m);

		vcl_result = viennacl::linalg::solve(vcl_compressed_matrix, vcl_rhs, custom);//, vcl_ilut );
		//vcl_result = viennacl::linalg::solve(vcl_compressed_matrix, vcl_rhs, custom, vcl_ilut );

		
	swatch.Mark();
	cout << " :: time " << setprecision(6) << swatch.cDuration() << endl;
	cout << custom.iters() << " " << custom.error() << endl;

  		Sparse::Vector<BSIZE,TYPE>	vecCLX;
		vecCLX.Resize( n);

		copy_flow2( vcl_result, vecCLX);

		cout << Dot( vecCLX, vecCLX) << endl;

		for ( int ir=0; ir<n; ++ir)
			vecCLX[ir] -= Sparse::BlockVector<BSIZE,TYPE>( TYPE(1.0) );

		cout << vecCLX.Norm() << "  " << vecCLX.Norm()/n << endl;

		//for ( int i=0; i<n; ++i)
		//{
		//	cout << i << " " ;
		//	for ( int k=0; k<BSIZE; ++k)
		//		cout << cpu_results[i];
		//	cout << endl;
		//}

		viennacl::copy( vcl_result, cpu_results);

		TYPE dnorm = 0.0;
		TYPE sum = 0.0;
		for ( int i=0; i<n*BSIZE; ++i)
		{
			sum += TYPE( cpu_results[i]);
			dnorm += TYPE( (cpu_results[i] - 1.0)*(cpu_results[i] - 1.0) );
		}

		cout << sum << endl;
		cout << sqrt(dnorm) << " " << sqrt(dnorm)/n << endl;
		//cout << sqrt(dnorm) << endl;
	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		fprintf( stderr, "%s\n", e.what() );
	}

	return 0;
}
