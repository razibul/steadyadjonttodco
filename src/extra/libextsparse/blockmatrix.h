#ifndef __BLOCKMATRIX_H__
#define __BLOCKMATRIX_H__

#include "libcoresystem/mgdecl.h"
#include "libextsparse/blockvector.h"
#include "libcorecommon/smatrix.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceSparse {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


template <MGSize SIZE, class ELEM_TYPE> 
class BlockMatrix;

template <MGSize SIZE, class ELEM_TYPE> 
BlockMatrix<SIZE,ELEM_TYPE>	operator +( const BlockMatrix<SIZE,ELEM_TYPE>&	v1,	const BlockMatrix<SIZE,ELEM_TYPE>& v2);

template <MGSize SIZE, class ELEM_TYPE> 
BlockMatrix<SIZE,ELEM_TYPE> operator -( const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockMatrix<SIZE,ELEM_TYPE>& v2);

template <MGSize SIZE, class ELEM_TYPE> 
BlockMatrix<SIZE,ELEM_TYPE> operator *( const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockMatrix<SIZE,ELEM_TYPE>& v2);

template <MGSize SIZE, class ELEM_TYPE> 
BlockMatrix<SIZE,ELEM_TYPE> operator /( const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockMatrix<SIZE,ELEM_TYPE>& v2);

template <MGSize SIZE, class ELEM_TYPE> 
BlockVector<SIZE,ELEM_TYPE> operator *( const BlockMatrix<SIZE,ELEM_TYPE>& a, const BlockVector<SIZE,ELEM_TYPE>& v);

template <MGSize SIZE, class ELEM_TYPE> 
BlockMatrix<SIZE,ELEM_TYPE> operator +( const BlockMatrix<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);

template <MGSize SIZE, class ELEM_TYPE> 
BlockMatrix<SIZE,ELEM_TYPE> operator +( const ELEM_TYPE& d, const BlockMatrix<SIZE,ELEM_TYPE>& v);

template <MGSize SIZE, class ELEM_TYPE> 
BlockMatrix<SIZE,ELEM_TYPE> operator -( const BlockMatrix<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);

template <MGSize SIZE, class ELEM_TYPE> 
BlockMatrix<SIZE,ELEM_TYPE> operator -( const ELEM_TYPE& d, const BlockMatrix<SIZE,ELEM_TYPE>& v);

template <MGSize SIZE, class ELEM_TYPE> 
BlockMatrix<SIZE,ELEM_TYPE> operator *( const BlockMatrix<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);

template <MGSize SIZE, class ELEM_TYPE> 
BlockMatrix<SIZE,ELEM_TYPE> operator *( const ELEM_TYPE& d, const BlockMatrix<SIZE,ELEM_TYPE>& v);

template <MGSize SIZE, class ELEM_TYPE> 
BlockMatrix<SIZE,ELEM_TYPE> operator /( const BlockMatrix<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);

template <MGSize SIZE, class ELEM_TYPE> 
BlockMatrix<SIZE,ELEM_TYPE> operator /( const ELEM_TYPE& d, const BlockMatrix<SIZE,ELEM_TYPE>& v);



template <MGSize SIZE, class ELEM_TYPE> 
void Multiply( BlockMatrix<SIZE,ELEM_TYPE>& mtx, const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockMatrix<SIZE,ELEM_TYPE>& v2);

template <MGSize SIZE, class ELEM_TYPE> 
void Multiply( BlockVector<SIZE,ELEM_TYPE>& vec, const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2);

template <MGSize SIZE, class ELEM_TYPE> 
void AddMultiply( BlockVector<SIZE,ELEM_TYPE>& vec, const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2);

//////////////////////////////////////////////////////////////////////
// class BlockMatrix
//////////////////////////////////////////////////////////////////////
template <MGSize SIZE, class ELEM_TYPE=MGFloat> 
class BlockMatrix
{
public:
	enum { MTX_SIZE = SIZE };

	BlockMatrix( const BlockMatrix<SIZE,ELEM_TYPE>& v);
	BlockMatrix( const ELEM_TYPE& d=0);

	template<class MATRIX> 
	explicit BlockMatrix( const MATRIX& v);


	static MGSize	MaxSize()		{ return SIZE;}
	static MGSize	Size()			{ return SIZE;}

	MGSize	NRows() const						{ return SIZE;}
	MGSize	NCols() const						{ return SIZE;}

	BlockVector<SIZE,ELEM_TYPE>&		operator()( const MGSize& i)		{ return *(mtab+i); }
	const BlockVector<SIZE,ELEM_TYPE>&	operator()( const MGSize& i) const	{ return *(mtab+i); }

	ELEM_TYPE&			operator()( const MGSize& i, const MGSize& j)		{ return (*(mtab+i))(j); }
	const ELEM_TYPE&	operator()( const MGSize& i, const MGSize& j) const	{ return (*(mtab+i))(j); }

    void	Init( const ELEM_TYPE& d);
	void	Resize( const MGSize& nr, const MGSize& nc);

	void	Invert();
	void	Transpose();

	friend BlockMatrix<SIZE,ELEM_TYPE>	operator +<>( const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockMatrix<SIZE,ELEM_TYPE>& v2);
	friend BlockMatrix<SIZE,ELEM_TYPE>	operator -<>( const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockMatrix<SIZE,ELEM_TYPE>& v2);
	friend BlockMatrix<SIZE,ELEM_TYPE>	operator *<>( const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockMatrix<SIZE,ELEM_TYPE>& v2);
	friend BlockMatrix<SIZE,ELEM_TYPE>	operator /<>( const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockMatrix<SIZE,ELEM_TYPE>& v2);
	friend BlockMatrix<SIZE,ELEM_TYPE>	operator +<>( const BlockMatrix<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);
	friend BlockMatrix<SIZE,ELEM_TYPE>	operator +<>( const ELEM_TYPE& d, const BlockMatrix<SIZE,ELEM_TYPE>& v);
	friend BlockMatrix<SIZE,ELEM_TYPE>	operator -<>( const BlockMatrix<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);
	friend BlockMatrix<SIZE,ELEM_TYPE>	operator -<>( const ELEM_TYPE& d, const BlockMatrix<SIZE,ELEM_TYPE>& v );
	friend BlockMatrix<SIZE,ELEM_TYPE>	operator *<>( const BlockMatrix<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);
	friend BlockMatrix<SIZE,ELEM_TYPE>	operator *<>( const ELEM_TYPE& d, const BlockMatrix<SIZE,ELEM_TYPE>& v);
	friend BlockMatrix<SIZE,ELEM_TYPE>	operator /<>( const BlockMatrix<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);
	friend BlockMatrix<SIZE,ELEM_TYPE>	operator /<>( const ELEM_TYPE& d, const BlockMatrix<SIZE,ELEM_TYPE>& v );
	friend BlockVector<SIZE,ELEM_TYPE>	operator *<>( const BlockMatrix<SIZE,ELEM_TYPE>& a, const BlockVector<SIZE,ELEM_TYPE>& v);


	BlockMatrix<SIZE,ELEM_TYPE>&		operator*=( const BlockMatrix<SIZE,ELEM_TYPE>& v);
	BlockMatrix<SIZE,ELEM_TYPE>&		operator-=( const BlockMatrix<SIZE,ELEM_TYPE>& v);
	BlockMatrix<SIZE,ELEM_TYPE>&		operator+=( const BlockMatrix<SIZE,ELEM_TYPE>& v);
	BlockMatrix<SIZE,ELEM_TYPE>&		operator/=( const BlockMatrix<SIZE,ELEM_TYPE>& v);

	BlockMatrix<SIZE,ELEM_TYPE>&		operator*=( const ELEM_TYPE& d);
	BlockMatrix<SIZE,ELEM_TYPE>&		operator-=( const ELEM_TYPE& d);
	BlockMatrix<SIZE,ELEM_TYPE>&		operator+=( const ELEM_TYPE& d);
	BlockMatrix<SIZE,ELEM_TYPE>&		operator/=( const ELEM_TYPE& d);

	void	Write( ostream& f=cout) const;

protected:
	BlockVector<SIZE,ELEM_TYPE>		mtab[SIZE];
};
//////////////////////////////////////////////////////////////////////




//////////////////////////////////////////////////////////////////////
// ---- Constructors ----

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE>::BlockMatrix( const BlockMatrix<SIZE,ELEM_TYPE>& v)
{
	for ( MGSize i=0; i<SIZE; ++i )
		*(mtab+i) = *(v.mtab+i);
}


template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE>::BlockMatrix( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; *(mtab+(i++)) = BlockVector<SIZE,ELEM_TYPE>(d) );
}

template <MGSize SIZE, class ELEM_TYPE> 
template< class MATRIX>
inline BlockMatrix<SIZE,ELEM_TYPE>::BlockMatrix( const MATRIX& v)
{
	ASSERT( v.NRows() == SIZE && v.NCols() == SIZE);
	for ( MGSize i=0; i<SIZE; ++i)
		for ( MGSize j=0; j<SIZE; ++j)
			(*this)(i,j) = v(i,j);
}


template <MGSize SIZE, class ELEM_TYPE> 
inline void BlockMatrix<SIZE,ELEM_TYPE>::Init( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; ++i )
		(mtab+i)->Init( d);
}

template <MGSize SIZE, class ELEM_TYPE> 
inline void BlockMatrix<SIZE,ELEM_TYPE>::Resize( const MGSize& nr, const MGSize& nc)
{
	if ( nr != SIZE || nc != SIZE)
		THROW_INTERNAL( "Bad usage of Flow::BlockMatrix - Resize()");
}


//////////////////////////////////////////////////////////////////////
// ---- one arg operators ----

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE>& BlockMatrix<SIZE,ELEM_TYPE>::operator*=( const BlockMatrix<SIZE,ELEM_TYPE>& v)
{
	for ( MGSize i=0; i<SIZE; ++i)
		*(mtab+i) *= *(v.mtab+i);
	return *this;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE>& BlockMatrix<SIZE,ELEM_TYPE>::operator-=( const BlockMatrix<SIZE,ELEM_TYPE>& v)
{
	for ( MGSize i=0; i<SIZE; ++i)
		*(mtab+i) -= *(v.mtab+i);
	return *this;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE>& BlockMatrix<SIZE,ELEM_TYPE>::operator+=( const BlockMatrix<SIZE,ELEM_TYPE>& v)
{
	for ( MGSize i=0; i<SIZE; ++i)
		*(mtab+i) += *(v.mtab+i);
	return *this;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE>& BlockMatrix<SIZE,ELEM_TYPE>::operator/=( const BlockMatrix<SIZE,ELEM_TYPE>& v)
{
	for ( MGSize i=0; i<SIZE; ++i)
		*(mtab+i) /= *(v.mtab+i);
	return *this;
}

	
template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE>& BlockMatrix<SIZE,ELEM_TYPE>::operator*=( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; ++i)
		*(mtab+i) *= d;
	return *this;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE>& BlockMatrix<SIZE,ELEM_TYPE>::operator-=( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; ++i)
		*(mtab+i) -= d;
	return *this;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE>& BlockMatrix<SIZE,ELEM_TYPE>::operator+=( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; ++i)
		*(mtab+i) += d;
	return *this;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE>& BlockMatrix<SIZE,ELEM_TYPE>::operator/=( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; ++i)
		*(mtab+i) /= d;
	return *this;
}


//////////////////////////////////////////////////////////////////////
// ---- two arg operators ----

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE> operator +( const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockMatrix<SIZE,ELEM_TYPE>& v2)
{
	BlockMatrix<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v1.mtab+i) + *(v2.mtab+i);
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE> operator -( const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockMatrix<SIZE,ELEM_TYPE>& v2)
{
	BlockMatrix<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v1.mtab+i) - *(v2.mtab+i);
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE> operator *( const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockMatrix<SIZE,ELEM_TYPE>& v2)
{
	BlockMatrix<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v1.mtab+i) * *(v2.mtab+i);
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE> operator /( const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockMatrix<SIZE,ELEM_TYPE>& v2)
{
	BlockMatrix<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v1.mtab+i) / *(v2.mtab+i);
	return vtemp;
}



template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE> operator +( const BlockMatrix<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d)
{
	BlockMatrix<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v.mtab+i) + d;
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE> operator +( const ELEM_TYPE& d, const BlockMatrix<SIZE,ELEM_TYPE>& v)
{
	BlockMatrix<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v.mtab+i) + d;
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE> operator -( const BlockMatrix<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d)
{
	BlockMatrix<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v.mtab+i) - d;
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE> operator -( const ELEM_TYPE& d, const BlockMatrix<SIZE,ELEM_TYPE>& v )
{
	BlockMatrix<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = d - *(v.mtab+i);
	return vtemp;
}



template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE> operator *( const BlockMatrix<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d)
{
	BlockMatrix<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v.mtab+i) * d;
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE> operator *( const ELEM_TYPE& d, const BlockMatrix<SIZE,ELEM_TYPE>& v)
{
	BlockMatrix<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v.mtab+i) * d;
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE> operator /( const BlockMatrix<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d)
{
	BlockMatrix<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v.mtab+i) / d;
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE> 
inline BlockMatrix<SIZE,ELEM_TYPE> operator /( const ELEM_TYPE& d, const BlockMatrix<SIZE,ELEM_TYPE>& v )
{
	BlockMatrix<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = d / *(v.mtab+i);
	return vtemp;
}



template <MGSize SIZE, class ELEM_TYPE> 
inline BlockVector<SIZE,ELEM_TYPE> operator *( const BlockMatrix<SIZE,ELEM_TYPE>& a, const BlockVector<SIZE,ELEM_TYPE>& v)
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE;  ++i)
		vtemp(i) = Dot( (*(a.mtab+(i))), v );
	return vtemp;
}



//////////////////////////////////////////////////////////////////////
// ---- misc functions ----

template <MGSize SIZE, class ELEM_TYPE> 
inline void BlockMatrix<SIZE,ELEM_TYPE>::Write( ostream& f) const
{
	for( MGSize i=0; i<SIZE; i++)
		(mtab+i)->Write( f);

	f << endl;
}


template <MGSize SIZE, class ELEM_TYPE> 
inline void BlockMatrix<SIZE,ELEM_TYPE>::Invert()
{
	SMatrix<SIZE,ELEM_TYPE>	mtx(SIZE,SIZE);
	for ( MGSize i=0; i<SIZE; ++i )
		for ( MGSize j=0; j<SIZE; ++j )
			mtx(i,j) = (*this)(i,j);

	mtx.Invert();

	for ( MGSize i=0; i<SIZE; ++i )
		for ( MGSize j=0; j<SIZE; ++j )
			(*this)(i,j) = mtx(i,j);
}

//template <MGSize SIZE, class ELEM_TYPE> 
//inline void BlockMatrix<SIZE,ELEM_TYPE>::Invert()
//{
//	SMatrix<SIZE,ELEM_TYPE>	mtx(SIZE,SIZE), mtxI, mtxD;
//	for ( MGSize i=0; i<SIZE; ++i )
//		for ( MGSize j=0; j<SIZE; ++j )
//			mtx(i,j) = (*this)(i,j);
//
//	mtxI = mtx;
//	mtx.Invert();
//	mtxD = mtxI*mtx;
//
//	ELEM_TYPE sum = ELEM_TYPE(0);
//
//	for ( MGSize i=0; i<SIZE; ++i )
//		for ( MGSize j=0; j<SIZE; ++j )
//		{
//			(*this)(i,j) = mtx(i,j);
//			sum += abs( mtxD(i,j) );
//		}
//
//	if ( abs( sum - ELEM_TYPE(SIZE) ) > ELEM_TYPE(1.0e-6) )
//		*this = BlockMatrix<SIZE,ELEM_TYPE>( ELEM_TYPE(0) );
//}


template <MGSize SIZE, class ELEM_TYPE>
inline void BlockMatrix<SIZE, ELEM_TYPE>::Transpose()
{
	SMatrix<SIZE, ELEM_TYPE>	mtx(SIZE, SIZE);
	for (MGSize i = 0; i<SIZE; ++i)
		for (MGSize j = 0; j<SIZE; ++j)
			mtx(j, i) = (*this)(i, j);

	for (MGSize i = 0; i<SIZE; ++i)
		for (MGSize j = 0; j<SIZE; ++j)
			(*this)(i, j) = mtx(i, j);
}

template <MGSize SIZE, class ELEM_TYPE> 
inline void Multiply( BlockMatrix<SIZE,ELEM_TYPE>& mtx, const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockMatrix<SIZE,ELEM_TYPE>& v2)
{
	for ( MGSize i=0; i<SIZE; ++i )
		for ( MGSize j=0; j<SIZE; ++j )
		{
			mtx(i,j) = v1(i,0)*v2(0,j);
			for ( MGSize k=1; k<SIZE; ++k )
				mtx(i,j) += v1(i,k)*v2(k,j);
		}
}

template <MGSize SIZE, class ELEM_TYPE> 
inline void Multiply( BlockVector<SIZE,ELEM_TYPE>& vec, const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2)
{
	for ( MGSize i=0; i<SIZE; ++i )
	{
		vec(i) = v1(i,0)*v2(0);
		for ( MGSize k=1; k<SIZE; ++k )
			vec(i) += v1(i,k)*v2(k);
	}
}

template <class ELEM_TYPE> 
inline void Multiply( BlockVector<5,ELEM_TYPE>& vec, const BlockMatrix<5,ELEM_TYPE>& v1, const BlockVector<5,ELEM_TYPE>& v2)
{
		vec(0) = v1(0,0)*v2(0) +  v1(0,1)*v2(1) +  v1(0,2)*v2(2) +  v1(0,3)*v2(3) +  v1(0,4)*v2(4);
		vec(1) = v1(1,0)*v2(0) +  v1(1,1)*v2(1) +  v1(1,2)*v2(2) +  v1(1,3)*v2(3) +  v1(1,4)*v2(4);
		vec(2) = v1(2,0)*v2(0) +  v1(2,1)*v2(1) +  v1(2,2)*v2(2) +  v1(2,3)*v2(3) +  v1(2,4)*v2(4);
		vec(3) = v1(3,0)*v2(0) +  v1(3,1)*v2(1) +  v1(3,2)*v2(2) +  v1(3,3)*v2(3) +  v1(3,4)*v2(4);
		vec(4) = v1(4,0)*v2(0) +  v1(4,1)*v2(1) +  v1(4,2)*v2(2) +  v1(4,3)*v2(3) +  v1(4,4)*v2(4);
}




template <MGSize SIZE, class ELEM_TYPE> 
inline void AddMultiply( BlockVector<SIZE,ELEM_TYPE>& vec, const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2)
{
	for ( MGSize i=0; i<SIZE; ++i )
	{
		for ( MGSize k=0; k<SIZE; ++k )
			vec(i) += v1(i,k)*v2(k);
	}
}

template <class ELEM_TYPE> 
inline void AddMultiply( BlockVector<5,ELEM_TYPE>& vec, const BlockMatrix<5,ELEM_TYPE>& v1, const BlockVector<5,ELEM_TYPE>& v2)
{
	vec(0) += v1(0,0)*v2(0) +  v1(0,1)*v2(1) +  v1(0,2)*v2(2) +  v1(0,3)*v2(3) +  v1(0,4)*v2(4);
	vec(1) += v1(1,0)*v2(0) +  v1(1,1)*v2(1) +  v1(1,2)*v2(2) +  v1(1,3)*v2(3) +  v1(1,4)*v2(4);
	vec(2) += v1(2,0)*v2(0) +  v1(2,1)*v2(1) +  v1(2,2)*v2(2) +  v1(2,3)*v2(3) +  v1(2,4)*v2(4);
	vec(3) += v1(3,0)*v2(0) +  v1(3,1)*v2(1) +  v1(3,2)*v2(2) +  v1(3,3)*v2(3) +  v1(3,4)*v2(4);
	vec(4) += v1(4,0)*v2(0) +  v1(4,1)*v2(1) +  v1(4,2)*v2(2) +  v1(4,3)*v2(3) +  v1(4,4)*v2(4);
}





template <MGSize SIZE, class ELEM_TYPE> 
inline void SubMultiply( BlockMatrix<SIZE,ELEM_TYPE>& mtx, const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockMatrix<SIZE,ELEM_TYPE>& v2)
{
	for ( MGSize i=0; i<SIZE; ++i )
		for ( MGSize j=0; j<SIZE; ++j )
		{
			for ( MGSize k=0; k<SIZE; ++k )
				mtx(i,j) -= v1(i,k)*v2(k,j);
		}
}


template <MGSize SIZE, class ELEM_TYPE> 
inline void SubMultiply( BlockVector<SIZE,ELEM_TYPE>& vec, const BlockMatrix<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2)
{
	for ( MGSize i=0; i<SIZE; ++i )
	{
		for ( MGSize k=0; k<SIZE; ++k )
			vec(i) -= v1(i,k)*v2(k);
	}
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace SpaceSparse
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Sparse = SpaceSparse;


#endif // __BLOCKMATRIX_H__
