#ifndef __BLOCKVECTOR_H__
#define __BLOCKVECTOR_H__

#include "libcoresystem/mgdecl.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceSparse {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

template <MGSize SIZE, class ELEM_TYPE> 
class BlockVector;

template <MGSize SIZE, class ELEM_TYPE> 
BlockVector<SIZE,ELEM_TYPE>	operator +( const BlockVector<SIZE,ELEM_TYPE>&	v1,	const BlockVector<SIZE,ELEM_TYPE>& v2);

template <MGSize SIZE, class ELEM_TYPE> 
BlockVector<SIZE,ELEM_TYPE> operator -( const BlockVector<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2);

template <MGSize SIZE, class ELEM_TYPE> 
BlockVector<SIZE,ELEM_TYPE> operator *( const BlockVector<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2);

template <MGSize SIZE, class ELEM_TYPE> 
BlockVector<SIZE,ELEM_TYPE> operator /( const BlockVector<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2);

template <MGSize SIZE, class ELEM_TYPE> 
BlockVector<SIZE,ELEM_TYPE> operator +( const BlockVector<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);

template <MGSize SIZE, class ELEM_TYPE> 
BlockVector<SIZE,ELEM_TYPE> operator +( const ELEM_TYPE& d, const BlockVector<SIZE,ELEM_TYPE>& v);

template <MGSize SIZE, class ELEM_TYPE> 
BlockVector<SIZE,ELEM_TYPE> operator -( const BlockVector<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);

template <MGSize SIZE, class ELEM_TYPE> 
BlockVector<SIZE,ELEM_TYPE> operator -( const ELEM_TYPE& d, const BlockVector<SIZE,ELEM_TYPE>& v);

template <MGSize SIZE, class ELEM_TYPE> 
BlockVector<SIZE,ELEM_TYPE> operator *( const BlockVector<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);

template <MGSize SIZE, class ELEM_TYPE> 
BlockVector<SIZE,ELEM_TYPE> operator *( const ELEM_TYPE& d, const BlockVector<SIZE,ELEM_TYPE>& v);

template <MGSize SIZE, class ELEM_TYPE> 
BlockVector<SIZE,ELEM_TYPE> operator /( const BlockVector<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);

template <MGSize SIZE, class ELEM_TYPE> 
BlockVector<SIZE,ELEM_TYPE> operator /( const ELEM_TYPE& d, const BlockVector<SIZE,ELEM_TYPE>& v);


template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> sqrt( const BlockVector<SIZE,ELEM_TYPE>& v);

template <MGSize SIZE, class ELEM_TYPE>
inline ELEM_TYPE Dot( const BlockVector<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2);

//////////////////////////////////////////////////////////////////////
// class BlockVector
//////////////////////////////////////////////////////////////////////
template <MGSize SIZE, class ELEM_TYPE=MGFloat> 
class BlockVector
{
public:
	BlockVector( const BlockVector<SIZE,ELEM_TYPE>& v);
	BlockVector( const ELEM_TYPE& d=0);

	template<class VECTOR> 
	explicit BlockVector( const VECTOR& v);

	BlockVector( const ELEM_TYPE& d1, const ELEM_TYPE& d2);
	BlockVector( const ELEM_TYPE& d1, const ELEM_TYPE& d2, const ELEM_TYPE& d3);
	BlockVector( const ELEM_TYPE& d1, const ELEM_TYPE& d2, const ELEM_TYPE& d3, const ELEM_TYPE& d4);
	BlockVector( const ELEM_TYPE& d1, const ELEM_TYPE& d2, const ELEM_TYPE& d3, const ELEM_TYPE& d4, const ELEM_TYPE& d5);

	~BlockVector()	{};


	static MGSize	MaxSize()	{ return SIZE;}
	static MGSize	Size()		{ return SIZE;}


	ELEM_TYPE&			operator()( const MGSize& ind);
	const ELEM_TYPE&	operator()( const MGSize& ind) const;

	ELEM_TYPE&			operator[]( const MGSize& ind);
	const ELEM_TYPE&	operator[]( const MGSize& ind) const;

	void	Init( const ELEM_TYPE& d);
	void	Resize( const MGSize& nr);


	friend BlockVector<SIZE,ELEM_TYPE>	operator +<>( const BlockVector<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2);
	friend BlockVector<SIZE,ELEM_TYPE>	operator -<>( const BlockVector<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2);
	friend BlockVector<SIZE,ELEM_TYPE>	operator *<>( const BlockVector<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2);
	friend BlockVector<SIZE,ELEM_TYPE>	operator /<>( const BlockVector<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2);

	friend BlockVector<SIZE,ELEM_TYPE>	operator +<>( const BlockVector<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);
	friend BlockVector<SIZE,ELEM_TYPE>	operator +<>( const ELEM_TYPE& d, const BlockVector<SIZE,ELEM_TYPE>& v);
	friend BlockVector<SIZE,ELEM_TYPE>	operator -<>( const BlockVector<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);
	friend BlockVector<SIZE,ELEM_TYPE>	operator -<>( const ELEM_TYPE& d, const BlockVector<SIZE,ELEM_TYPE>& v);
	friend BlockVector<SIZE,ELEM_TYPE>	operator *<>( const BlockVector<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);
	friend BlockVector<SIZE,ELEM_TYPE>	operator *<>( const ELEM_TYPE& d, const BlockVector<SIZE,ELEM_TYPE>& v);
	friend BlockVector<SIZE,ELEM_TYPE>	operator /<>( const BlockVector<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d);
	friend BlockVector<SIZE,ELEM_TYPE>	operator /<>( const ELEM_TYPE& d, const BlockVector<SIZE,ELEM_TYPE>& v);

	friend BlockVector<SIZE,ELEM_TYPE> sqrt<>( const BlockVector<SIZE,ELEM_TYPE>& v);
	friend ELEM_TYPE Dot<>( const BlockVector<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2);


	BlockVector<SIZE,ELEM_TYPE>		operator -();

	BlockVector<SIZE,ELEM_TYPE>&		operator*=( const BlockVector<SIZE,ELEM_TYPE>& v);
	BlockVector<SIZE,ELEM_TYPE>&		operator-=( const BlockVector<SIZE,ELEM_TYPE>& v);
	BlockVector<SIZE,ELEM_TYPE>&		operator+=( const BlockVector<SIZE,ELEM_TYPE>& v);
	BlockVector<SIZE,ELEM_TYPE>&		operator/=( const BlockVector<SIZE,ELEM_TYPE>& v);
	
	BlockVector<SIZE,ELEM_TYPE>&		operator*=( const ELEM_TYPE& d);
	BlockVector<SIZE,ELEM_TYPE>&		operator-=( const ELEM_TYPE& d);
	BlockVector<SIZE,ELEM_TYPE>&		operator+=( const ELEM_TYPE& d);
	BlockVector<SIZE,ELEM_TYPE>&		operator/=( const ELEM_TYPE& d);

	void	Write( ostream& f=cout) const;


protected:
	ELEM_TYPE	mtab[SIZE>0?SIZE:1];

};
//////////////////////////////////////////////////////////////////////




//////////////////////////////////////////////////////////////////////
// ---- Constructors ----

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE>::BlockVector( const BlockVector<SIZE,ELEM_TYPE>& v)
{
	for ( MGSize i=0; i<SIZE; ++i )
		*(mtab+i) = *(v.mtab+i);
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE>::BlockVector( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; *(mtab+(i++)) = d );
}


template <MGSize SIZE, class ELEM_TYPE>
template< class VECTOR>
BlockVector<SIZE,ELEM_TYPE>::BlockVector( const VECTOR& v)
{
	CHECK( v.Size() == SIZE);
	for ( MGSize i=0; i<SIZE; ++i)
		*(mtab+i) = v(i);
}


template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE>::BlockVector( const ELEM_TYPE& d1, const ELEM_TYPE& d2)
{
	mtab[0] = d1;
	mtab[1] = d2;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE>::BlockVector( const ELEM_TYPE& d1, const ELEM_TYPE& d2, const ELEM_TYPE& d3)
{
	mtab[0] = d1;
	mtab[1] = d2;
	mtab[2] = d3;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE>::BlockVector( const ELEM_TYPE& d1, const ELEM_TYPE& d2, const ELEM_TYPE& d3, const ELEM_TYPE& d4)
{
	mtab[0] = d1;
	mtab[1] = d2;
	mtab[2] = d3;
	mtab[3] = d4;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE>::BlockVector( const ELEM_TYPE& d1, const ELEM_TYPE& d2, const ELEM_TYPE& d3, const ELEM_TYPE& d4, const ELEM_TYPE& d5)
{
	mtab[0] = d1;
	mtab[1] = d2;
	mtab[2] = d3;
	mtab[3] = d4;
	mtab[4] = d5;
}


template <MGSize SIZE, class ELEM_TYPE> 
inline void BlockVector<SIZE,ELEM_TYPE>::Init( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; *(mtab+(i++)) = d );
}

template <MGSize SIZE, class ELEM_TYPE> 
inline void BlockVector<SIZE,ELEM_TYPE>::Resize( const MGSize& nr)
{
	if ( nr != SIZE)
		THROW_INTERNAL( "Bad usage of Flow::BlockVector - Resize()");
}


template <MGSize SIZE, class ELEM_TYPE>
inline ELEM_TYPE& BlockVector<SIZE,ELEM_TYPE>::operator()( const MGSize& ind)
{
	return *(mtab+ind);
}

template <MGSize SIZE, class ELEM_TYPE>
inline const ELEM_TYPE& BlockVector<SIZE,ELEM_TYPE>::operator()( const MGSize& ind) const
{
	return *(mtab+ind);
}

template <MGSize SIZE, class ELEM_TYPE>
inline ELEM_TYPE& BlockVector<SIZE,ELEM_TYPE>::operator[]( const MGSize& ind)
{
	return *(mtab+ind);
}

template <MGSize SIZE, class ELEM_TYPE>
inline const ELEM_TYPE& BlockVector<SIZE,ELEM_TYPE>::operator[]( const MGSize& ind) const
{
	return *(mtab+ind);
}


//inline void BlockVector::Init( const MGFloat& ro, const MGFloat& u, const MGFloat& w, const MGFloat& p)
//{
//	MGFloat	v;
//	v = u*u + w*w;
//	mtab[0] = ro;
//	mtab[1] = ro*u;
//	mtab[2] = ro*w;
//	mtab[3] = p/(FLO_K-1) + 0.5*ro*v;
//}




//////////////////////////////////////////////////////////////////////
// ---- one arg operators ----

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> BlockVector<SIZE,ELEM_TYPE>::operator-()
{
	BlockVector<SIZE,ELEM_TYPE>	v;
	for ( MGInt i=0; i<SIZE; ++i)
		v.mtab[i] = -mtab[i];
	return v;
}


template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE>& BlockVector<SIZE,ELEM_TYPE>::operator*=( const BlockVector<SIZE,ELEM_TYPE>& v)
{
	for ( MGSize i=0; i<SIZE; ++i )
		*(mtab+i) *= *(v.mtab+i);
	return *this;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE>& BlockVector<SIZE,ELEM_TYPE>::operator-=( const BlockVector<SIZE,ELEM_TYPE>& v)
{
	for ( MGSize i=0; i<SIZE; ++i )
		*(mtab+i) -= *(v.mtab+i);
	return *this;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE>& BlockVector<SIZE,ELEM_TYPE>::operator+=( const BlockVector<SIZE,ELEM_TYPE>& v)
{
	for ( MGSize i=0; i<SIZE; ++i )
		*(mtab+i) += *(v.mtab+i);
	return *this;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE>& BlockVector<SIZE,ELEM_TYPE>::operator/=( const BlockVector<SIZE,ELEM_TYPE>& v)
{
	for ( MGSize i=0; i<SIZE; ++i )
		*(mtab+i) /= *(v.mtab+i);
	return *this;
}

	
template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE>& BlockVector<SIZE,ELEM_TYPE>::operator*=( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; *(mtab+(i++)) *= d );
	return *this;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE>& BlockVector<SIZE,ELEM_TYPE>::operator-=( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; *(mtab+(i++)) -= d );
	return *this;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE>& BlockVector<SIZE,ELEM_TYPE>::operator+=( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; *(mtab+(i++)) += d );
	return *this;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE>& BlockVector<SIZE,ELEM_TYPE>::operator/=( const ELEM_TYPE& d)
{
	for ( MGSize i=0; i<SIZE; *(mtab+(i++)) /= d );
	return *this;
}




//////////////////////////////////////////////////////////////////////
// ---- two arg operators ----

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> operator +( const BlockVector<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2)
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v1.mtab+i) + *(v2.mtab+i);
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> operator -( const BlockVector<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2)
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v1.mtab+i) - *(v2.mtab+i);
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> operator *( const BlockVector<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2)
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v1.mtab+i) * *(v2.mtab+i);
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> operator /( const BlockVector<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2)
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v1.mtab+i) / *(v2.mtab+i);
	return vtemp;
}



template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> operator +( const BlockVector<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d)
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v.mtab+i) + d;
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> operator +( const ELEM_TYPE& d, const BlockVector<SIZE,ELEM_TYPE>& v)
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v.mtab+i) + d;
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> operator -( const BlockVector<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d)
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v.mtab+i) - d;
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> operator -( const ELEM_TYPE& d, const BlockVector<SIZE,ELEM_TYPE>& v )
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = d - *(v.mtab+i);
	return vtemp;
}




template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> operator *( const BlockVector<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d)
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v.mtab+i) * d;
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> operator *( const ELEM_TYPE& d, const BlockVector<SIZE,ELEM_TYPE>& v)
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v.mtab+i) * d;
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> operator /( const BlockVector<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d)
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = *(v.mtab+i) / d;
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> operator /( const ELEM_TYPE& d, const BlockVector<SIZE,ELEM_TYPE>& v )
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = d / *(v.mtab+i);
	return vtemp;
}


//////////////////////////////////////////////////////////////////////
// ---- misc functions ----

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> sqrt( const BlockVector<SIZE,ELEM_TYPE>& v)
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = ::sqrt( *(v.mtab+i) );
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> pow( const BlockVector<SIZE,ELEM_TYPE>& v, const ELEM_TYPE& d)
{
	BlockVector<SIZE,ELEM_TYPE> vtemp;
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = ::pow( *(v.mtab+i), d );
	return vtemp;
}


template <MGSize SIZE, class ELEM_TYPE>
inline BlockVector<SIZE,ELEM_TYPE> fabs( const BlockVector<SIZE,ELEM_TYPE>& v)
{
	static BlockVector<SIZE,ELEM_TYPE>	vtemp;
	
	for ( MGSize i=0; i<SIZE; ++i )
		*(vtemp.mtab+i) = ::fabs( *(v.mtab+i) );
	return vtemp;
}

template <MGSize SIZE, class ELEM_TYPE>
inline ELEM_TYPE Dot( const BlockVector<SIZE,ELEM_TYPE>& v1, const BlockVector<SIZE,ELEM_TYPE>& v2)
{
	ELEM_TYPE	d=*(v1.mtab+0) * *(v2.mtab+0);
	for ( MGSize i=1; i<SIZE; ++i )
		d += *(v1.mtab+i) * *(v2.mtab+i);

	return d;
}


template <MGSize SIZE, class ELEM_TYPE>
inline ELEM_TYPE Norm( const BlockVector<SIZE,ELEM_TYPE>& v)
{
	ELEM_TYPE temp = Dot(v,v);
	return ::sqrt(temp);
}


template <MGSize SIZE, class ELEM_TYPE>
inline ELEM_TYPE MaxNorm( const BlockVector<SIZE,ELEM_TYPE>& v)
{
	ELEM_TYPE temp = ::fabs( v(0) );
	for ( MGSize i=1; i< SIZE; i++)
		if ( ::fabs( v(i) ) > temp )
			temp = ::fabs( v(i) );

	return temp;
}


template <MGSize SIZE, class ELEM_TYPE>
inline void	BlockVector<SIZE,ELEM_TYPE>::Write( ostream& f) const
{
	f << "| ";
	for ( MGSize i=0; i<SIZE; ++i)
		f << setprecision(7) << setw(11) << mtab[i] << " ";
	f << "|" << endl;
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace SpaceSparse
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Sparse = SpaceSparse;

#endif // __BLOCKVECTOR_H__
