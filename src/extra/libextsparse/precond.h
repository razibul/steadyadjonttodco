#ifndef __PRECOND_H__
#define __PRECOND_H__

#include "libcoresystem/mgdecl.h"
#include "libextsparse/sparsevector.h"
#include "libextsparse/sparsematrix.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceSparse {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class PreNone
//////////////////////////////////////////////////////////////////////
template <MGSize BSIZE, class ELEM_TYPE=MGFloat> 
class PreNone
{
public:
	void	Solve( Sparse::Vector<BSIZE,ELEM_TYPE>& vd, const Sparse::Vector<BSIZE,ELEM_TYPE>& v) const;
private:
};

template <MGSize BSIZE, class ELEM_TYPE> 
inline void PreNone<BSIZE,ELEM_TYPE>::Solve( Sparse::Vector<BSIZE,ELEM_TYPE>& vd, const Sparse::Vector<BSIZE,ELEM_TYPE>& v) const
{
	vd.Resize( v.Size() );
	vd.SetActiveRows( v.NRows() );
	for ( MGSize i=0; i<v.NRows(); ++i)
		vd[i] = v[i];
}



//////////////////////////////////////////////////////////////////////
// class PreDiag
//////////////////////////////////////////////////////////////////////
template <MGSize BSIZE, class ELEM_TYPE=MGFloat> 
class PreDiag
{
public:
	PreDiag()	{}
	void	Init( const Sparse::Matrix<BSIZE,ELEM_TYPE>& mtx);

	void	Solve( Sparse::Vector<BSIZE,ELEM_TYPE>& vd, const Sparse::Vector<BSIZE,ELEM_TYPE>& v) const;

private:
	vector< BlockMatrix<BSIZE,ELEM_TYPE> >	mtabD;
};

template <MGSize BSIZE, class ELEM_TYPE> 
inline void PreDiag<BSIZE,ELEM_TYPE>::Init( const Sparse::Matrix<BSIZE,ELEM_TYPE>& mtx)
{
	if ( mtabD.size() != mtx.NRows() )
		mtabD.resize( mtx.NRows() );

	for ( MGSize i=0; i<mtabD.size(); ++i)
	{
		mtabD[i] = mtx(i,i);
		mtabD[i].Invert();
	}
}



template <MGSize BSIZE, class ELEM_TYPE> 
inline void PreDiag<BSIZE,ELEM_TYPE>::Solve( Sparse::Vector<BSIZE,ELEM_TYPE>& vd, const Sparse::Vector<BSIZE,ELEM_TYPE>& v) const
{
	//THROW_INTERNAL("Not implemented");
	vd.Resize( v.Size() );
	vd.SetActiveRows( v.NRows() );
	for ( MGSize i=0; i<v.NRows(); ++i)
		Sparse::Multiply( vd[i], mtabD[i], v[i] );
}



//////////////////////////////////////////////////////////////////////
// class PreILU
//////////////////////////////////////////////////////////////////////
template <MGSize BSIZE, class ELEM_TYPE=MGFloat> 
class PreILU
{
public:
	PreILU()	{};
	void	Init( const Sparse::Matrix<BSIZE,ELEM_TYPE>& mtx);

	void	Solve( Sparse::Vector<BSIZE,ELEM_TYPE>& vd, const Sparse::Vector<BSIZE,ELEM_TYPE>& v) const;

private:
	Sparse::Matrix<BSIZE,ELEM_TYPE>	mMtx;
};


template <MGSize BSIZE, class ELEM_TYPE> 
inline void PreILU<BSIZE,ELEM_TYPE>::Init( const Sparse::Matrix<BSIZE,ELEM_TYPE>& mtx)
{
	mMtx.Copy( mtx);
	mMtx.DecompLU();
}

template <MGSize BSIZE, class ELEM_TYPE> 
inline void PreILU<BSIZE,ELEM_TYPE>::Solve( Sparse::Vector<BSIZE,ELEM_TYPE>& vd, const Sparse::Vector<BSIZE,ELEM_TYPE>& v) const
{
	vd.Resize( v.Size() );
	mMtx.Solve( vd, v);
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StoreTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Sparse = SpaceSparse;


#endif // __PRECOND_H__
