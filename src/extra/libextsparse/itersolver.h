#ifndef __ITERSOLVER_H__
#define __ITERSOLVER_H__


template < class T >
void GeneratePlaneRotation( const T &dx, const T &dy, T &cs, T &sn)
{
	if ( ::fabs( dy) < ZERO )
	{
		cs = 1.0;
		sn = 0.0;
	}
	else if ( ::fabs(dy) > ::fabs(dx))
	{
		T temp = dx / dy;
		sn = T( 1.0 / sqrt( T(1.0) + temp*temp ) );
		cs = temp * sn;
	}
	else
	{
		T temp = dy / dx;
		cs = T( 1.0 / sqrt( 1.0 + temp*temp ) );
		sn = temp * cs;
	}
}


template < class T >
void ApplyPlaneRotation( T &dx, T &dy, const T &cs, const T &sn)
{
	T temp  =  cs * dx + sn * dy;
	dy = -sn * dx + cs * dy;
	dx = temp;
}


template < class T >
T abs( const T& x)
{
	return (x > 0 ? x : -x);
}


//////////////////////////////////////////////////////////////////////
// Conjugate Gradient Method
//////////////////////////////////////////////////////////////////////
template < class OPERATOR, class VECTOR, class T >
int ConjGradient( OPERATOR &A, VECTOR &x, const VECTOR &b, MGSize &max_iter, T &tol)
{
	VECTOR	r, p, w;
	T alpha, beta, norm, tmpdot;

	r.Resize( b.Size() );
	r.SetActiveRows( b.NRows() );

	w.Resize( b.Size() );
	w.SetActiveRows( b.NRows() );

	p.Resize( b.Size() );
	p.SetActiveRows( b.NRows() );

	A.Mult( r, x);										// <-----

	for ( MGSize kk=0; kk<r.NRows(); ++kk)
		p[kk] = r[kk] = b[kk] - r[kk];

	tmpdot =  Dot( r, r);

	for ( MGSize i=0; i<max_iter; ++i)
	{
		A.Mult( w, p);										// <-----

		alpha = tmpdot / Dot( w, p);

		for ( MGSize kk=0; kk<r.NRows(); ++kk)
			x[kk] += alpha * p[kk];

		norm = ::sqrt( tmpdot);

		if ( norm < tol && i > 0 )
		{
			tol = norm;
			max_iter = i;
			return 0;
		}


		for ( MGSize kk=0; kk<r.NRows(); ++kk)
			r[kk] -= alpha * w[kk];

		T t =  Dot( r, r);
		beta = t / tmpdot;
		tmpdot = t;

		for ( MGSize kk=0; kk<r.NRows(); ++kk)
			p[kk] = r[kk] + beta * p[kk];
	}

	tol = norm;
	return 1;
}



//////////////////////////////////////////////////////////////////////
// Preconditioned Conjugate Gradient Method
//////////////////////////////////////////////////////////////////////
template < class OPERATOR, class VECTOR, class PRECOND, class T >
int PrecConjGradient( OPERATOR &A, VECTOR &x, const VECTOR &b, const PRECOND &M, MGSize &max_iter, T &tol)
{
	VECTOR	r, p, w, z;
	T alpha, beta, norm, tmpdot;

	r.Resize( b.Size() );
	r.SetActiveRows( b.NRows() );

	p.Resize( b.Size() );
	p.SetActiveRows( b.NRows() );

	w.Resize( b.Size() );
	w.SetActiveRows( b.NRows() );

	z.Resize( b.Size() );
	x.SetActiveRows( b.NRows() );

	A.Mult( r, x);										// <-----

	for ( MGSize kk=0; kk<r.NRows(); ++kk)
		r[kk] = b[kk] - r[kk];

	M.Solve( z, r);

	for ( MGSize kk=0; kk<r.NRows(); ++kk)
		p[kk] = z[kk];

	tmpdot =  Dot( r, z);

	for ( MGSize i=0; i<max_iter; ++i)
	{
		A.Mult( w, p);										// <-----

		alpha = tmpdot / Dot( w, p);

		for ( MGSize kk=0; kk<r.NRows(); ++kk)
			x[kk] += alpha * p[kk];

		norm = ::sqrt( tmpdot);

		if ( norm < tol && i > 0 )
		{
			tol = norm;
			max_iter = i;
			return 0;
		}


		for ( MGSize kk=0; kk<r.NRows(); ++kk)
			r[kk] -= alpha * w[kk];

		M.Solve( z, r);

		T t =  Dot( r, z);
		beta = t / tmpdot;
		tmpdot = t;

		for ( MGSize kk=0; kk<p.NRows(); ++kk)
			p[kk] = z[kk] + beta * p[kk];
	}

	tol = norm;
	return 1;
}



//////////////////////////////////////////////////////////////////////
// Preconditioned GMRES Method
//////////////////////////////////////////////////////////////////////
template < class OPERATOR, class VECTOR, class PRECOND, 
		   class SMATRIX, class SVECTOR, class T >

int GMRES( OPERATOR &A, VECTOR &x, const VECTOR &b, const PRECOND &M, 
		   MGSize m, MGSize &max_iter, T &tol)
{
	if ( m >= max_iter)
		m = max_iter - 1;

	MGSize		i, j = 1, k;
	T		normb, beta, resid;
	VECTOR	r, z, w;;
	SMATRIX	H(m+1,m);
	SVECTOR	s(m+1), cs(m), sn(m);

	vector<VECTOR> tabv(m+1);

	// find approximation x0
	M.Solve( x, b);
	normb = x.Norm();

	if ( abs( normb) < T(ZERO) )
		normb = 1;



	z.Resize( b.Size() );
	z.SetActiveRows( b.NRows() );

	while ( j < max_iter) 
	{
		//z = A * x;
		A.Mult( z, x);										// <-----

		//z = b - z;										// <-----
		for ( MGSize kk=0; kk<z.NRows(); ++kk)
			z[kk] = b[kk] - z[kk];

		// M*r = b - A*x
		M.Solve(r, z);									
		beta = r.Norm();

		if ((resid = beta / normb) <= tol ) 
		//if ((resid = beta / normb) <= tol && j > 1) 
		{
			tol = resid;
			max_iter = 0;
			return 0;
		}

		//v[0] = r * (1.0 / beta);							// <-----
		tabv[0].Resize( r.NRows() );
		for ( MGSize kk=0; kk<r.NRows(); ++kk)
			tabv[0][kk] = r[kk] / beta;

		
		//s = 0.0;	// !!!!
		for ( MGSize kk=0; kk<s.NRows(); s(kk++)=0.0);

		s(0) = beta;
    
		for ( i = 0; i < m && j <= max_iter; ++j, ++i) 
		{
			//z = A * v[i];
			A.Mult( z, tabv[i]);								// <-----

			// M*w = z
			M.Solve( w, z);									// <-----

			for ( k = 0; k <= i; ++k) 
			{
				H(k, i) = Dot(w, tabv[k]);

				//w -= H(k, i) * v[k];						// <-----
				for ( MGSize kk=0; kk<w.NRows(); ++kk)
					w[kk] -= H(k, i) * tabv[k][kk];
			}

			H(i+1, i) = w.Norm();

			//v[i+1] = w * (1.0 / H(i+1, i));				// <-----
			tabv[i+1].Resize( w.NRows() );
			for ( MGSize kk=0; kk<w.NRows(); ++kk)
				tabv[i+1][kk] = w[kk] * (T(1.0) / H(i+1, i));
				
			for ( k = 0; k < i; ++k)
				ApplyPlaneRotation( H(k,i), H(k+1,i), cs(k), sn(k));

			GeneratePlaneRotation( H(i,i), H(i+1,i), cs(i), sn(i));
			ApplyPlaneRotation( H(i,i), H(i+1,i), cs(i), sn(i));
			ApplyPlaneRotation( s(i), s(i+1), cs(i), sn(i));

			//cout << "iter = " << j << " " << i << "  res = " << resid << endl;

			if ((resid = abs( s(i+1)) / normb) < tol) 
			{
				//printf( "leaving\n");
				Update( x, i, H, s, tabv);
				tol = resid;
				max_iter = j;
				return 0;
			}
		}

		Update( x, m-1, H, s, tabv);

	}
  
	tol = resid;
	return 1;
}


template < class VECTOR, class SMATRIX, class SVECTOR >
void Update( VECTOR &x, const MGSize& k, const SMATRIX &h, const SVECTOR &s, const vector<VECTOR>& v)
{
	SVECTOR y(s);

	// Backsolve:  
	for ( MGInt i = (MGInt)k; i >= 0; --i) 
	{
		y(i) /= h(i,i);
		for ( MGInt j = i - 1; j >= 0; --j)
			y(j) -= h(j,i) * y(i);
	}

	for ( MGSize j = 0; j <= k; ++j)
		for ( MGSize kk=0; kk<x.NRows(); ++kk)
			x[kk] += v[j][kk] * y(j);
}





#endif // __ITERSOLVER_H__
