#ifndef __SPARSEMATRIX_H__
#define __SPARSEMATRIX_H__


#include "libcoresystem/mgdecl.h"
#include "libextsparse/blockmatrix.h"
#include "libextsparse/blockvector.h"
#include "libextsparse/sparsevector.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceSparse {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class Matrix
//////////////////////////////////////////////////////////////////////
template <MGSize BSIZE, class ELEM_TYPE=MGFloat> 
class Matrix
{
public:
	typedef BlockMatrix<BSIZE,ELEM_TYPE>	BlockMtx;
	typedef BlockVector<BSIZE,ELEM_TYPE>	BlockVec;

	Matrix() : mNRows(0), mNCols(0)			{}

	static MGSize	BlockSize()				{ return BSIZE;}


	void	Resize( const MGSize& n)		{ mTab.resize(n); mtabInd.resize(n); mNRows=mNCols=n; }
	MGSize	Size() const					{ return mTab.size(); }
	MGSize	RowSize( const MGSize& i) const	{ return mTab[i].size(); }

	void			SetActiveRows( const MGSize& n)		{ mNRows=min( n, (const MGSize)mTab.size() );}
	void			SetActiveCols( const MGSize& n)		{ mNCols=min( n, (const MGSize)mTab.size() );}
	const MGSize&	NRows() const						{ return mNRows;}
	const MGSize&	NCols() const						{ return mNCols;}

	BlockMtx&		operator()( const MGSize& i, const MGSize& j);
    const BlockMtx&	operator()( const MGSize& i, const MGSize& j) const;

	void	Reset();

	bool	Exist( const MGSize& i, const MGSize& j) const;
	void	InsertBlock( const MGSize& i, const MGSize& j);


	void	Mult( Sparse::Vector<BSIZE,ELEM_TYPE>& vr, const Sparse::Vector<BSIZE,ELEM_TYPE>& v);
	
	void	Transpose();


	BlockMtx&		GetDiagBlock( const MGSize& i)						{ return mTab[i][FindIndex( i, i)];}
    const BlockMtx&	GetDiagBlock( const MGSize& i) const				{ return mTab[i][FindIndex( i, i)];}
	BlockMtx&		GetBlock( const MGSize& i, const MGSize& j)			{ return mTab[i][j];}
    const BlockMtx&	GetBlock( const MGSize& i, const MGSize& j) const	{ return mTab[i][j];}
	MGSize&			GetColId( const MGSize& i, const MGSize& j)			{ return mtabInd[i][j];}
	const MGSize&	GetColId( const MGSize& i, const MGSize& j) const	{ return mtabInd[i][j];}

	void	Copy( const Matrix& mtx);
	void	DecompLU();
	void	Solve( Vector<BSIZE,ELEM_TYPE>& x, const Vector<BSIZE,ELEM_TYPE>& b) const;
	void	GaussSeidel( Vector<BSIZE,ELEM_TYPE> &x, const Vector<BSIZE,ELEM_TYPE> &b, MGSize &max_iter, ELEM_TYPE &tol);

	void	DumpBlockPos( FILE *f);
	void	DumpMtx( FILE *f);
	void	DumpInd( FILE *f);

	void	DumpBlockPos( ostream& f=cout) const;
	void	DumpMtx( ostream& f=cout) const;
	void	DumpInd( ostream& f=cout) const;
	void	ExportMtx( ostream& f=cout) const;


	MGInt	FindIndex( const MGSize& i, const MGSize& j) const;

protected:
	MGInt	FindNext( const MGSize& i, const MGSize& j) const;

private:
	MGSize				mNRows;
	MGSize				mNCols;

	vector< vector< BlockMtx > >	mTab;
	vector< vector< MGSize > >		mtabInd;
};



template <MGSize BSIZE, class ELEM_TYPE> 
inline typename Matrix<BSIZE,ELEM_TYPE>::BlockMtx& Matrix<BSIZE,ELEM_TYPE>::operator()( const MGSize& i, const MGSize& j)
{
	MGInt	jb = FindIndex( i, j);
	ASSERT( jb >= 0);
	return mTab[i][jb];
}

template <MGSize BSIZE, class ELEM_TYPE> 
inline const typename Matrix<BSIZE,ELEM_TYPE>::BlockMtx& Matrix<BSIZE,ELEM_TYPE>::operator()( const MGSize& i, const MGSize& j) const
{
	MGInt	jb = FindIndex( i, j);
	ASSERT( jb >= 0);
	return mTab[i][jb];
}


template <MGSize BSIZE, class ELEM_TYPE> 
inline void Matrix<BSIZE,ELEM_TYPE>::Reset()
{
	for ( MGSize i=0; i<mTab.size(); i++)
		for ( MGSize j=0; j<mTab[i].size(); j++)
			mTab[i][j].Init( 0.0);
}


template <MGSize BSIZE, class ELEM_TYPE> 
inline MGInt Matrix<BSIZE,ELEM_TYPE>::FindIndex( const MGSize& i, const MGSize& j) const
{
	MGInt	ib = -1;
	for ( MGSize k=0; k<mtabInd[i].size(); k++)
		if ( mtabInd[i][k] == j)
		{
			ib = (MGInt)k;
			break;
		}

	return ib;
}

template <MGSize BSIZE, class ELEM_TYPE> 
inline MGInt Matrix<BSIZE,ELEM_TYPE>::FindNext( const MGSize& i, const MGSize& j) const
{
	MGInt	k = 0;

	k = 0;
	while ( mtabInd[i][k] <= j )
	{
		++k;
		if ( k >= static_cast<MGInt>( mtabInd[i].size() ) )
			return -1;
	}

	return k;

}


template <MGSize BSIZE, class ELEM_TYPE> 
inline void Matrix<BSIZE,ELEM_TYPE>::InsertBlock( const MGSize& i, const MGSize& j)
{
	typename vector< BlockMtx >::iterator	itr = mTab[i].begin();
	typename vector< MGSize >::iterator		itri = mtabInd[i].begin();

	while ( itri != mtabInd[i].end() )
	{
		if ( *itri < j )
		{
			itr++;
			itri++;
		}
		else
		{
			if ( *itri == j)
				return;

			break;
		}
	}


	mTab[i].insert( itr, BlockMtx( ELEM_TYPE(0.0) ) );
	mtabInd[i].insert( itri, j );
}



template <MGSize BSIZE, class ELEM_TYPE> 
inline void Matrix<BSIZE,ELEM_TYPE>::Mult( Sparse::Vector<BSIZE,ELEM_TYPE>& vr, const Sparse::Vector<BSIZE,ELEM_TYPE>& v)
{
	//#pragma omp parallel for
	for ( MGSize i=0; i<NRows(); ++i)
	{
		Sparse::Multiply( vr[i], mTab[i][0], v[ mtabInd[i][0]] );

		for ( MGSize j=1; j<mTab[i].size(); ++j)
		{
			MGSize icol = mtabInd[i][j];
			if ( icol < NCols() )
				Sparse::AddMultiply( vr[i], mTab[i][j], v[icol] );
		}
	}
	//for ( MGSize i=0; i<mTab.size(); ++i)
	//{
	//	Sparse::Multiply( vr[i], mTab[i][0], v[ mtabInd[i][0]] );

	//	for ( MGSize j=1; j<mTab[i].size(); ++j)
	//		Sparse::AddMultiply( vr[i], mTab[i][j], v[ mtabInd[i][j]] );
	//}
}


template <MGSize BSIZE, class ELEM_TYPE>
inline void Matrix<BSIZE, ELEM_TYPE>::Transpose()
{
	//assumed square matrix (mNRows == mNCols)
	vector<MGSize> tabCount(mNRows,0);
	vector<MGSize> tmpCount(mNRows, 0);

	for (MGSize i = 0; i < mNRows; i++)
		for (MGSize j = 0; j < mtabInd[i].size(); j++)
			tabCount[ mtabInd[i][j] ]++;

	vector< vector<MGSize> > newtabInd(mNRows);
	vector< vector< BlockMtx > >  newtabBlock(mNRows);
	for (MGSize j = 0; j < mNRows; j++)
	{
		newtabInd[j].resize( tabCount[j] );
		newtabBlock[j].resize( tabCount[j] );
	}
	
	
	for (MGSize i = 0; i < mNRows; i++)
		for (MGSize j = 0; j < mtabInd[i].size(); j++)
		{
			MGSize indx = mtabInd[i][j];
			newtabInd[ indx ][ tmpCount[ indx ] ] = i;
			
			BlockMtx mtx = GetBlock(i, j);
			mtx.Transpose();

			newtabBlock[ indx ][ tmpCount[indx] ] = mtx;

			tmpCount[indx]++;
		}

	
	for (MGSize i = 0; i < mNRows; i++)
	{
		
		mtabInd[i].resize( newtabInd[i].size() );
		for (MGSize j = 0; j < newtabInd[i].size(); j++)
		{
			mtabInd[i][j] = newtabInd[i][j];
			mTab[i][j] = newtabBlock[i][j];

		}
	}


}

template <MGSize BSIZE, class ELEM_TYPE> 
inline void Matrix<BSIZE,ELEM_TYPE>::Copy( const Matrix& mtx)
{
	if ( mTab.size() != mtx.mTab.size() )
	{
		mTab.resize( mtx.mTab.size() );
		mtabInd.resize( mtx.mTab.size() );
	}

	for ( MGSize i=0; i<mTab.size(); ++i)
	{
		if ( mTab[i].size() != mtx.mTab[i].size() )
		{
			mTab[i].resize( mtx.mTab[i].size() );
			mtabInd[i].resize( mtx.mtabInd[i].size() );
		}

		for ( MGSize k=0; k<mTab[i].size();++k)
		{
			mTab[i][k] = mtx.mTab[i][k];
			mtabInd[i][k] = mtx.mtabInd[i][k];
		}
	}

	mNRows = mtx.mNRows;
	mNCols = mtx.mNCols;

}


template <MGSize BSIZE, class ELEM_TYPE> 
inline void Matrix<BSIZE,ELEM_TYPE>::DecompLU()
{
	// using Crout algorithm
	// block matrices on main diagonal are inverted !!! 

	MGSize icol, kcol;
	BlockMtx	mtxtmp;

	mTab[0][0].Invert();

	for ( MGSize i=1; i<mNRows; ++i)
	{

		// alpha
		MGSize j;
		for ( j=0; (icol=mtabInd[i][j])<i; ++j)
		{
			BlockMtx sum = mTab[i][j];

			for ( MGSize k=0; (kcol=mtabInd[i][k])<icol; ++k)
				if ( Exist(kcol,icol) )
					SubMultiply( sum, mTab[i][k], (*this)(kcol,icol) );

			//const BlockMtx&	mtxinv = (*this)(icol,icol);
			//Multiply( mTab[i][j], sum, mtxinv);

			Multiply( mTab[i][j], sum, GetDiagBlock( icol) );

		}

		// beta
		for ( ; j<mtabInd[i].size(); ++j)
		{
			icol=mtabInd[i][j];

			for ( MGSize k=0; (kcol=mtabInd[i][k])<i; ++k)
				if ( Exist(kcol,icol) )
					SubMultiply( mTab[i][j], mTab[i][k], (*this)(kcol,icol) );

			//BlockMtx	xtm = mTab[i][j];
			//try {

			if ( icol == i)
				mTab[i][j].Invert();

			//}catch(...)
			//{
			//	xtm.Write();
			//	cout << "i = " << icol << " j = " << i << endl;
			//	throw;
			//}

		}
	}

}


template <MGSize BSIZE, class ELEM_TYPE> 
inline void Matrix<BSIZE,ELEM_TYPE>::Solve( Vector<BSIZE,ELEM_TYPE>& x, const Vector<BSIZE,ELEM_TYPE>& b) const
{

	BlockVec	sum;
	BlockMtx	minv;

	const MGSize n = Size();
	MGSize	icol, idiag;

	x.Resize( n);

	for ( MGSize i=0; i<n; i++) 
	{
		sum = b[i]; 

		for ( MGInt j=0; (icol=mtabInd[i][j])<i; j++) 
				SubMultiply( sum, mTab[i][j], x[icol] );

		x[i]=sum; 
	} 


	for ( MGInt i=(MGInt)(n-1); i>=0; i--) 
	{
		sum = x[i]; 

		idiag = FindIndex(i,i);
		for ( MGSize j=idiag+1; j<mTab[i].size(); j++) 
			SubMultiply( sum, mTab[i][j], x[mtabInd[i][j]] ); 

		// block matrix on main diagonal should be already inverted 
		minv = mTab[i][idiag];
		Multiply( x[i], minv, sum); 
	}  

}



template <MGSize BSIZE, class ELEM_TYPE> 
inline void Matrix<BSIZE,ELEM_TYPE>::GaussSeidel( Vector<BSIZE,ELEM_TYPE> &x, const Vector<BSIZE,ELEM_TYPE> &b, MGSize &max_iter, ELEM_TYPE &tol)
{
	for ( MGSize iter=0; iter<max_iter; ++iter)
	{
		ELEM_TYPE err = 0.0;

		for ( MGSize i=0; i<NRows(); ++i)
		{
			typename Matrix<BSIZE,ELEM_TYPE>::BlockMtx bmtx;
			typename Matrix<BSIZE,ELEM_TYPE>::BlockVec bvec(0.0);
			typename Matrix<BSIZE,ELEM_TYPE>::BlockVec dvec(0.0);

			for ( MGSize j=0; j<mTab[i].size(); ++j)
			{
				MGSize icol = mtabInd[i][j];
				if ( icol < NCols() )
				{
					if ( icol != i )
						Sparse::AddMultiply( bvec, mTab[i][j], x[icol] );
					else
					{
						bmtx = mTab[i][j];
						bmtx.Invert();
					}
				}
			}

			bvec = b[i] - bvec;
			dvec = x[i];

			Sparse::Multiply( x[i], bmtx, bvec );
			dvec = x[i] - dvec;

			err += Dot( dvec, dvec);
		}

		err = ::sqrt( err);

		cout << "GS : err = " << err << endl;

		if ( err < tol)
		{
			tol = err;
			max_iter = iter;
			return;
		}
	}

}




template <MGSize BSIZE, class ELEM_TYPE> 
inline bool Matrix<BSIZE,ELEM_TYPE>::Exist( const MGSize& i, const MGSize& j) const
{
	if ( FindIndex( i, j) >= 0 )
		return true;
	else
		return false;
}


template <MGSize BSIZE, class ELEM_TYPE> 
inline void Matrix<BSIZE,ELEM_TYPE>::DumpBlockPos( FILE *f)
{
	fprintf( f, "     " );
	for ( MGSize j=0; j<mTab.size(); j++)
		fprintf( f, " %2d", j);
	fprintf( f, "\n");

	for ( MGSize i=0; i<mTab.size(); i++)
	{
		fprintf( f, "[%2d] ", i);
		for ( MGSize j=0; j<mTab.size(); j++)
		{
			if ( Exist(i,j) )
				fprintf( f, "  X");
			else
				fprintf( f, "  .");
		}
		fprintf( f, "\n");
	}
	fprintf( f, "\n");
}


template <MGSize BSIZE, class ELEM_TYPE> 
inline void Matrix<BSIZE,ELEM_TYPE>::DumpMtx( FILE *f)
{
	MGInt	ind;
	for ( MGSize i=0; i<mTab.size(); i++)
	{
		for ( MGSize j=0; j<BlockMtx::Size(); j++)
		{

			for ( MGSize k=0; k<mTab.size(); k++)
			{
				ind = FindIndex( i, k);
				for ( MGSize m=0; m<BlockMtx::Size(); m++)
				{
					if ( ind >= 0 )
						fprintf( f, " %6.3lf", mTab[i][ind](j,m) );
					else
						fprintf( f, " ------" );
				}

				fprintf( f, "  ");
			}
			fprintf( f, "\n");
		}
		fprintf( f, "\n");
	}

}

template <MGSize BSIZE, class ELEM_TYPE> 
inline void Matrix<BSIZE,ELEM_TYPE>::DumpInd( FILE *f)
{
	for ( MGSize i=0; i<mtabInd.size(); i++)
	{
		fprintf( f, "[%d]:", i);
		for ( MGSize j=0; j<mtabInd[i].size(); j++)
			fprintf( f, " %3d", mtabInd[i][j] );
		fprintf( f, "\n");
	}
	fprintf( f, "\n");
}


template <MGSize BSIZE, class ELEM_TYPE> 
inline void Matrix<BSIZE,ELEM_TYPE>::DumpBlockPos( ostream& f) const
{
	f << "     ";
	for ( MGSize j=0; j<mTab.size(); j++)
		f << " " << setw(2) << j;
	f << endl;

	for ( MGSize i=0; i<mTab.size(); i++)
	{
		f << "[" << i << "]: ";
		for ( MGSize j=0; j<mTab.size(); j++)
		{
			if ( Exist(i,j) )
				f << "  X";
			else
				f << "  .";
		}
		f << endl;
	}
	f << endl;
}


template <MGSize BSIZE, class ELEM_TYPE> 
inline void Matrix<BSIZE,ELEM_TYPE>::DumpMtx( ostream& f) const
{
	MGInt	ind;
	for ( MGSize i=0; i<mTab.size(); i++)
	{
		for ( MGSize j=0; j<BlockMtx::Size(); j++)
		{

			for ( MGSize k=0; k<mTab.size(); k++)
			{
				ind = FindIndex( i, k);
				for ( MGSize m=0; m<BlockMtx::Size(); m++)
				{
					if ( ind >= 0 )
						f << " " << setprecision(3) << setw(8) << mTab[i][ind](j,m);
					else
						f << " --------";
				}

				f << "  ";
			}
			f << endl;
		}
		f << endl;
	}

}

template <MGSize BSIZE, class ELEM_TYPE> 
inline void Matrix<BSIZE,ELEM_TYPE>::DumpInd( ostream& f) const
{
	for ( MGSize i=0; i<mtabInd.size(); i++)
	{
		f << "[" << i << "]:";
		for ( MGSize j=0; j<mtabInd[i].size(); j++)
			f << " " << setw(3) << mtabInd[i][j];
		f << endl;
	}
	f << endl;
}



template <MGSize BSIZE, class ELEM_TYPE> 
inline void Matrix<BSIZE,ELEM_TYPE>::ExportMtx( ostream& f) const
{
	MGSize nblock = 0;
	for ( MGSize i=0; i<mtabInd.size(); i++)
		nblock += mtabInd[i].size();

	f << mTab.size() << endl;
	f << nblock << " " << BSIZE <<  endl;
	f << endl;

	for ( MGSize i=0; i<mtabInd.size(); i++)
		for ( MGSize j=0; j<mtabInd[i].size(); j++)
		{
			f << i << " " << mtabInd[i][j] << endl;

			for ( MGSize k=0; k<BSIZE; k++)
			{
				for ( MGSize m=0; m<BSIZE; m++)
					f << setw(22) << setprecision(12) << mTab[i][j](k,m) << " ";
				f << endl;
			}
		}
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StoreTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Sparse = SpaceSparse;

#endif // __SPARSEMATRIX_H__
