#ifndef __SPARSEVECTOR_H__
#define __SPARSEVECTOR_H__


#include "libcoresystem/mgdecl.h"
#include "libextsparse/blockvector.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceSparse {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class Vector
//////////////////////////////////////////////////////////////////////
template <MGSize BSIZE, class ELEM_TYPE=MGFloat> 
class Vector
{
public:
	typedef BlockVector<BSIZE,ELEM_TYPE>	BlockVec;

	Vector() : mNRows(0)								{}

	static MGSize	BlockSize()							{ return BSIZE;}

	void			Resize( const MGSize& n)			{ mTab.resize(n); mNRows=n;}
	MGSize			Size() const						{ return mTab.size();}

	void			SetActiveRows( const MGSize& n)		{ mNRows=min( n, (const MGSize)mTab.size() );}
	const MGSize&	NRows() const						{ return mNRows;}

	const BlockVec&	operator[]( const MGSize& i) const	{ return mTab[i];}
	BlockVec&		operator[]( const MGSize& i)		{ return mTab[i];}

	void	Reset();
	ELEM_TYPE	Norm() const;
	ELEM_TYPE	MaxNorm();

	vector<BlockVec>& Tab()	{ return mTab;}

	void	Dump( ostream& f=cout) const;

private:
	MGSize				mNRows;
	vector<BlockVec>	mTab;
};



template <MGSize BSIZE, class ELEM_TYPE> 
inline void Vector<BSIZE,ELEM_TYPE>::Reset()
{
	for ( MGSize i=0; i<Size(); i++)
			mTab[i].Init( 0.0);
}


template <MGSize BSIZE, class ELEM_TYPE> 
inline ELEM_TYPE Vector<BSIZE,ELEM_TYPE>::MaxNorm()
{
	ELEM_TYPE	d;
	ELEM_TYPE maxd = Sparse::MaxNorm( mTab[0] );

	for ( MGSize i=1; i<NRows(); ++i)
	{
		d = Sparse::MaxNorm( mTab[i] );
		if ( d > maxd) maxd = d;
	}

	return maxd;
}


template <MGSize BSIZE, class ELEM_TYPE> 
inline ELEM_TYPE Vector<BSIZE,ELEM_TYPE>::Norm() const
{
	ELEM_TYPE	d = Dot( *this, *this);

	return ::sqrt( d);
}


template <MGSize BSIZE, class ELEM_TYPE> 
inline ELEM_TYPE Dot( const Vector<BSIZE,ELEM_TYPE>& v1, const Vector<BSIZE,ELEM_TYPE>& v2)
{
	//  Check for compatible dimensions:
	if ( v1.NRows() != v2.NRows())
		THROW_INTERNAL( "Incompatible dimensions in Dot(). " );

	ELEM_TYPE temp=Dot( v1[0], v2[0] );
	for ( MGSize i=1; i<v1.NRows(); ++i)
		temp += Dot( v1[i], v2[i] );

	return temp;
}


template <MGSize BSIZE, class ELEM_TYPE> 
inline typename Vector<BSIZE,ELEM_TYPE>::BlockVec BlockDot( const Vector<BSIZE,ELEM_TYPE>& v1, const Vector<BSIZE,ELEM_TYPE>& v2)
{
	//  Check for compatible dimensions:
	if ( v1.NRows() != v2.NRows())
		THROW_INTERNAL( "Incompatible dimensions in BlockDot(). " );

	typename Vector<BSIZE,ELEM_TYPE>::BlockVec temp = v1[0] * v2[0];
	for ( MGSize i=1; i<v1.NRows(); ++i)
		temp += v1[i] * v2[i];

	return temp;
}





template <MGSize BSIZE, class ELEM_TYPE> 
inline void Vector<BSIZE,ELEM_TYPE>::Dump( ostream& f) const

{
	for ( MGSize i=0; i<Size(); i++)
	{
		f << i;
		for ( MGSize j=0; j<BSIZE; j++)
			f << " " << setw(15) << setprecision(8) << mTab[i](j);
		f << endl;
	}
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace SpaceSparse
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Sparse = SpaceSparse;

#endif // __SPARSEVECTOR_H__
