#ifndef __HF_SUBSIMPLEX_H__
#define __HF_SUBSIMPLEX_H__

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_simplexbase.h"
#include "libhfgeom/hf_toposimplex.h"
#include "libhfgeom/hf_rational.h"
#include "libhfgeom/hf_gmatrix.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {



//////////////////////////////////////////////////////////////////////
// class SimplexFacet
//////////////////////////////////////////////////////////////////////
template < class T, TDimension DIM, TDimension UNIDIM, template< class T, size_t N > class DATA = ArrayData >
class SubSimplex : public SimplexBase<T,DIM,UNIDIM,DATA>
{
public:
	typedef SimplexBase<T,DIM,UNIDIM,DATA>	TBase;
	typedef typename TBase::GVect	GVect;
	typedef typename TBase::ARRAY	ARRAY;
	
	enum { SIZE = TBase::SIZE };

public:
	SubSimplex( const ARRAY& data) : TBase( data)	{}

	SubSimplex<T,DIM-1,UNIDIM,DATA>	GetSubSimplexFacet( const TSize& ifc) const
	{
		GVect	tabv[ TopoSimplexFace<DIM-1,DIM>::SIZE ];
		for ( TSize i=0; i<TopoSimplexFace<DIM-1,DIM>::SIZE; ++i)
			tabv[i] = cPoint( TopoSimplexOrientedFacet<DIM>::cNodeId( ifc, i) );
			//tabv[i] = cPoint( TopoSimplexFace<DIM-1,DIM>::cNodeId( ifc, i) );

		return SubSimplex<T,DIM-1,UNIDIM,DATA>( typename SubSimplex<T,DIM-1,UNIDIM,DATA>::ARRAY(tabv) );
	}


	template <class TC>
	TC Volume2() const
	{
		typedef GVector<TC,UNIDIM> CmpVector;

		CmpVector tabdv[DIM];

		for ( TSize i=0; i<DIM; ++i)
			tabdv[i] = CmpVector( this->mData[i+1] ) - CmpVector( this->mData[0] );

		GMatrix<TC,DIM> mtx(DIM,DIM);

		for ( TSize i=0; i<DIM; ++i)
		{
			mtx(i,i) = tabdv[i] * tabdv[i];
			for ( TSize j=i+1; j<DIM; ++j)
				mtx(j,i) = mtx(i,j) = tabdv[i] * tabdv[j];
		}

		//mtx.Write();

		return DeterminantFunc( mtx);
	}


	template <class TC>
	Rational<TC> ProjPointDist2( const GVect& point) const
	{
		SubSimplex<T,DIM+1,UNIDIM> volsimplex( typename SubSimplex<T,DIM+1,UNIDIM>::ARRAY( point, this->mData) );

		Rational<TC> rat;
		rat.rNumer() = volsimplex.Volume2<TC>();
		rat.rDenom() = this->Volume2<TC>();

		return rat;
	}

};




template < class T, TDimension UNIDIM, template< class T, size_t N > class DATA>
class SubSimplex<T,DIM_0D,UNIDIM,DATA> : public SimplexBase<T,DIM_0D,UNIDIM,DATA>
{
public:
	typedef SimplexBase<T,DIM_0D,UNIDIM,DATA>	TBase;
	typedef typename TBase::GVect	GVect;
	typedef typename TBase::ARRAY	ARRAY;
	
	enum { SIZE = TBase::SIZE };

public:
	SubSimplex( const ARRAY& data) : TBase( data)	{}


	template <class TC>
	TC Volume2() const
	{
		return TC(1);	// ????????????????
	}

};



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;


#endif // __HF_SUBSIMPLEX_H__

