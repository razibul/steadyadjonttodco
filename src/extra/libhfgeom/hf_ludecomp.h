#ifndef __HF_LUDECOMP_H__
#define __HF_LUDECOMP_H__


//#include "sysdecl.h"
#include "libcoresystem/mgdecl.h"
#include "libhfgeom/hf_typedef.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


//////////////////////////////////////////////////////////////////////
// class LUDecomp
//////////////////////////////////////////////////////////////////////
template< class T, class MATRIX>
class LUDecomp
{
public:
	typedef TSize	size_type;

	LUDecomp( const MATRIX& mtx);

	void	Execute();
	void	Solve( MATRIX& mtxB);
	T		GetDeterminant();

private:
	MATRIX				mA;
	size_type			mN;
	T					mSign;
	vector<size_type>	mtabPivot;
};
//////////////////////////////////////////////////////////////////////


template< class T, class MATRIX>
LUDecomp<T,MATRIX>::LUDecomp( const MATRIX& mtx) : mA(mtx), mN(mtx.NRows()), mSign(1), mtabPivot(mtx.NRows())	
{
	if ( mtx.NRows() != mtx.NCols() )
		THROW_INTERNAL( "LUDecomp : LUDecomp : square matrix expected");
}


template< class T, class MATRIX>
void LUDecomp<T,MATRIX>::Execute()
{
	mSign = 1;
	for ( size_type i=0; i<mN; ++i) 
		mtabPivot[i] = i;

	for ( size_type j=0; j<mN; ++j)
	{
		// find pivot
		size_type p = j;
		for ( size_type i=j+1; i<mN; ++i) 
		{
			if ( abs( mA(i,j)) > abs( mA(p,j)) )
				p = i;
		}

		// swap rows if necessary
		if ( p != j)
		{
			for ( size_type k=0; k<mN; ++k)
			{
				T tmp = mA(p,k);
				mA(p,k) = mA(j,k);
				mA(j,k) = tmp;
			}

			mSign = - mSign;
		}

		mtabPivot[j] = p;

		if ( mA(j,j) == 0 )
		{
			THROW_INTERNAL( "LUDecomp : singular matrix" );
		}

		// carry on with elimination
		for ( size_type i=j+1; i<mN; ++i)
		{
			T tmp = mA(i,j) /= mA(j,j);
			for ( size_type k=j+1; k<mN; ++k)
				mA(i,k) -= tmp * mA(j,k);
		}

	}

	//mA.Write();

	//for ( size_type i=0; i<mN; ++i)
	//	cout << mtabPivot[i] << " ";
	//cout << endl;
}



template< class T, class MATRIX>
void LUDecomp<T,MATRIX>::Solve( MATRIX& mtxB)
{
	if ( mtxB.NRows() != mN || mtxB.NCols() < 1 )
		THROW_INTERNAL( "LUDecomp : Solve : mtxB incompatible sizes");


	size_type n = mtxB.NCols();

	// calc P*B
	for ( size_type i=0; i<mN; ++i)
	{
		for ( size_type j=0; j<n; ++j)
		{
			T tmp = mtxB(i,j);
			mtxB(i,j) = mtxB( mtabPivot[i],j);
			mtxB( mtabPivot[i],j) = tmp;
		}
	}


	// solve L*Y = P*B
	for ( size_type k=0; k<mN; ++k) 
		for ( size_type i=k+1; i<mN; ++i) 
			for ( size_type j=0; j<n; ++j) 
				mtxB(i,j) -= mtxB(k,j) * mA(i,k);


	// solve U*X = Y;
	for ( size_type l=mN; l>=1; --l) 
	{
		size_type k = l-1;
		for ( size_type j=0; j<n; ++j) 
			mtxB(k,j) /= mA(k,k);

		for ( size_type i=0; i<k; ++i) 
			for ( size_type j=0; j<n; ++j) 
				mtxB(i,j) -= mtxB(k,j) * mA(i,k);
	}

}


template< class T, class MATRIX>
T LUDecomp<T,MATRIX>::GetDeterminant()
{
	T d = mSign;
	for ( size_type j=0; j<mN; ++j)
		d *= mA(j,j);

	return d;
}



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;

#endif // __HF_LUDECOMP_H__
