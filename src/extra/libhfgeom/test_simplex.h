#ifndef __HF_TEST_SIMPLEX_H__
#define __HF_TEST_SIMPLEX_H__


//#include "sysdecl.h"
#include "libcoresystem/mgdecl.h"
#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_simplex.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {




//////////////////////////////////////////////////////////////////////
// class Test_QRDecomp
//////////////////////////////////////////////////////////////////////
class Test_Simplex
{
public:
	bool	Run();

//private:

	template <class T, class TC, TDimension DIM>
	bool CheckVolume( const T& tmin, const T& tmax, const TSize& n, const bool& bPlus);

	template <class T, class TC, TDimension DIM>
	bool CheckIsInside( const T& tmin, const T& tmax, const TSize& n);

	template <class T, class TC, TDimension DIM>
	bool CheckIsInsideSphere( const T& tmin, const T& tmax, const TSize& n);

private:

	template <class T, class TC, TDimension DIM>
	void CreateNodeTab( GVector<T,DIM> tab[DIM+1], const T& tmin, const T& tmax, const bool& bPlus );

};
//////////////////////////////////////////////////////////////////////



template <class T, class TC, TDimension DIM>
void Test_Simplex::CreateNodeTab( GVector<T,DIM> tab[DIM+1], const T& tmin, const T& tmax, const bool& bPlus )
{
	for ( TSize i=0; i<DIM; ++i)
		tab[0].rX(i) = static_cast<T>( ( TC(tmax - tmin) * TC( rand()) ) / TC(RAND_MAX) + TC(tmin) );

	for ( TSize i=1; i<=DIM; ++i)
	{
		tab[i] = GVector<T,DIM>( T(0) );
		tab[i].rX(i-1) = static_cast<T>( abs( ( TC(tmax - tmin) * TC( rand()) ) / TC(RAND_MAX) + TC(tmin) ) );
	}

	if ( ! bPlus)
		tab[1] = - tab[1];

	for ( TSize i=1; i<=DIM; ++i)
		tab[i] += tab[0];

	//for ( TSize i=0; i<=DIM; ++i)
	//	tab[i].Write();
}


template <class T, class TC, TDimension DIM>
bool Test_Simplex::CheckVolume( const T& tmin, const T& tmax, const TSize& n, const bool& bPlus)
{
	typedef typename Simplex<T,DIM>::ARRAY ARRAY;

	GVector<T,DIM> tab[DIM+1];

	for ( TSize i=0; i<n; ++i)
	{
		CreateNodeTab<T,TC,DIM>( tab, tmin, tmax, bPlus);

		Simplex<T,DIM>	tet( (ARRAY( tab)) );

		cout << "vol = " << tet.template Volume<TC>() << endl;
	}

	cout << endl;

	return true;
}

template <class T, class TC, TDimension DIM>
bool Test_Simplex::CheckIsInside( const T& tmin, const T& tmax, const TSize& n)
{
	typedef typename Simplex<T,DIM>::ARRAY ARRAY;

	GVector<T,DIM> tab[DIM+1];

	for ( TSize i=0; i<n; ++i)
	{
		CreateNodeTab<T,TC,DIM>( tab, tmin, tmax, true);

		Simplex<T,DIM>	tet( (ARRAY( tab)) );

		// center point
		GVector<T,DIM> vc = tet.template Center<TC>();
		cout << "center - ";
		if ( tet.template IsInside<TC>( vc) )
			cout << "T";
		else
			cout << "F";

		// nodes
		for ( TSize in=0; in<tet.SIZE; ++in)
		{
			cout << ", n[" << in << "] - ";
			if ( tet.template IsInside<TC>( tab[in]) )
				cout << "T";
			else
				cout << "F";
		}

		// outside
		GVector<T,DIM> vo = (tab[1] - vc) + tab[1];
		cout << ", out - ";
		if ( tet.template IsInside<TC>( vo) )
			cout << "T";
		else
			cout << "F";

		cout << endl;
	}

	cout << endl;

	return true;
}


template <class T, class TC, TDimension DIM>
bool Test_Simplex::CheckIsInsideSphere( const T& tmin, const T& tmax, const TSize& n)
{
	typedef typename Simplex<T,DIM>::ARRAY ARRAY;

	GVector<T,DIM> tab[DIM+1];
	TC r;

	for ( TSize i=0; i<n; ++i)
	{
		CreateNodeTab<T,TC,DIM>( tab, tmin, tmax, true);

		Simplex<T,DIM>	tet( (ARRAY( tab)) );

		
		//r = tet.template IsInsideSphere<TC>( tab[0]);
		//cout << ", n[" << 0 << "]: " <<  (r==TC(0) ? "0" : (r>TC(0) ? "+" : "-"));
		//cout << endl;
		//return true;
		
		
		
		// center point
		GVector<T,DIM> vc = tet.template Center<TC>();
		r =  tet.template IsInsideSphere<TC>( vc);
		cout << "center: " <<  (r==TC(0) ? "0" : (r>TC(0) ? "+" : "-"));

	
		// nodes
		for ( TSize in=0; in<tet.SIZE; ++in)
		{
			r = tet.template IsInsideSphere<TC>( tab[in]);
			cout << ", n[" << in << "]: " <<  (r==TC(0) ? "0" : (r>TC(0) ? "+" : "-"));
		}

		GVector<T,DIM> extn = tab[0];
		for ( TSize in=0; in<tet.SIZE; ++in)
			extn += tab[in] - tab[0];

		r = tet.template IsInsideSphere<TC>( extn);
		cout << ", en: " <<  (r==TC(0) ? "0" : (r>TC(0) ? "+" : "-"));

		// outside
		GVector<T,DIM> vo = (tab[1] - vc) + tab[1];
		r =  tet.template IsInsideSphere<TC>( vo);
		cout << ", out: " <<  (r==TC(0) ? "0" : (r>TC(0) ? "+" : "-"));

		cout << endl;
	}

	cout << endl;

	return true;
}



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_TEST_SIMPLEX_H__
