#ifndef __HF_SIMPLEX_H__
#define __HF_SIMPLEX_H__

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_simplexbase.h"
#include "libhfgeom/hf_subsimplex.h"

#include "libhfgeom/hf_gmatrix.h"
#include "libhfgeom/hf_determinant.h"
#include "libhfgeom/hf_toposimplex.h"

#include "libcorecommon/key.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {



//////////////////////////////////////////////////////////////////////
// class SimplexCommon
//////////////////////////////////////////////////////////////////////
template < class T, TDimension DIM, template<class T,size_t N> class DATA = ArrayData >
class SimplexCommon : public SimplexBase<T,DIM,DIM,DATA>
{
public:
	typedef SimplexBase<T,DIM,DIM,DATA>	TBase;
	typedef typename TBase::GVect	GVect;
	typedef typename TBase::ARRAY	ARRAY;

	enum { SIZE = TBase::SIZE};


	SimplexCommon( const ARRAY& data) : TBase( data)	{}

	SubSimplex<T,DIM-1,DIM,DATA>	GetSubSimplexFacet( const TSize& ifc) const
	{
		GVect	tabv[ TopoSimplexFace<DIM-1,DIM>::SIZE ];
		for ( TSize i=0; i<TopoSimplexFace<DIM-1,DIM>::SIZE; ++i)
			tabv[i] = cPoint( TopoSimplexOrientedFacet<DIM>::cNodeId( ifc, i) );

		return SubSimplex<T,DIM-1,DIM,DATA>( typename SubSimplex<T,DIM-1,DIM,DATA>::ARRAY(tabv) );
	}


	Key<DIM,TSize>	GetFacetKeyNOut( const TSize& iface) const
	{
		Key<DIM,TSize> key;

		for ( TSize i=0; i<DIM; ++i)
			key.rElem(i) = TopoSimplexOrientedFacet<DIM>::cNodeId( iface, i); 
			
		return key;
	}

	Key<DIM,TSize>	GetFacetKeyNIn( const TSize& iface) const
	{
		Key<DIM,TSize> key;

		key.rFirst()  = TopoSimplexOrientedFacet<DIM>::cNodeId( iface, 1); 
		key.rSecond() = TopoSimplexOrientedFacet<DIM>::cNodeId( iface, 0); 

		for ( TSize i=2; i<DIM; ++i)
			key.rElem(i) = TopoSimplexOrientedFacet<DIM>::cNodeId( iface, i); 
		
		return key;
	}

	template <class TC>
	GVect	Center() const
	{
		GVector<TC,DIM> vc( this->cPoint(0));
		for ( TSize i=1; i<SIZE; ++i)
			vc += GVector<TC,DIM>( this->cPoint(i) );
		vc /= TC( SIZE);

		return GVect( vc);
	}


	template <class TC>
	TC	Volume() const
	{
		GMatrix<TC,DIM> mtx(DIM,DIM);

		for ( TSize i=0; i<DIM; ++i)
			for ( TSize j=0; j<DIM; ++j)
				mtx(i,j) = TC( this->mData[i+1].cX(j)) - TC( this->mData[0].cX(j) );

		//mtx.Write();

		return DeterminantFunc( mtx);

		//Determinant< TC, GMatrix<TC,DIM> >  det(mtx);
		//det.Execute();

		//return det.GetDeterminant();
	}


	template <class TC>
	TC	Thicknes() const
	{
		TC vol = Volume<TC>();

		TC armax = TC( 0);
		for ( TSize i=0; i<SIZE; ++i)
		{
			SubSimplex<T,DIM-1,DIM,DATA> sface = GetSubSimplexFacet( i);
			TC ar = sface.template Volume2<TC>();
			if ( ar > armax )
				armax = ar;
		}

		TC th = vol / sqrt( armax);

		return th;
	}


	template <class TC>
	TC	IsInsideSphere( const GVect& vct) const
	{
		typedef GVector<TC,DIM> CmpVector;

		GMatrix<TC,DIM+1> mtx(DIM+1,DIM+1);

		TC mod = CmpVector( this->mData[0]) * CmpVector( this->mData[0]);

		for ( TSize i=0; i<DIM; ++i)
		{
			for ( TSize j=0; j<DIM; ++j)
				mtx(i,j) = TC( this->mData[i+1].cX(j)) - TC( this->mData[0].cX(j) );

			mtx(i,DIM) = CmpVector( this->mData[i+1]) * CmpVector( this->mData[i+1]) - mod;
		}

		for ( TSize j=0; j<DIM; ++j)
			mtx(DIM,j) = TC( vct.cX(j)) - TC( this->mData[0].cX(j) );

		mtx(DIM,DIM) = CmpVector( vct) * CmpVector( vct) - mod;


		//cout<< endl<< endl << " --- begin" << endl;
		//for ( TSize i=0; i<=DIM; ++i)
		//	this->mData[i].Write();
		//vct.Write();

		//mtx.Write();
		//cout << Volume<TC>() << endl;
		//Volume<TC>();
		
		return ( - DeterminantFunc( mtx) );


		//Determinant< TC, GMatrix<TC,DIM+1> >  det(mtx);
		//det.Execute();
		
		//cout << " det = " << (- det.GetDeterminant()) << endl; 

		//return - det.GetDeterminant();
	}


};






//////////////////////////////////////////////////////////////////////
// class Simplex
//////////////////////////////////////////////////////////////////////
template < class T, TDimension DIM, template< class T, size_t N > class DATA = ArrayData >
class Simplex : public SimplexCommon<T,DIM,DATA>
{
public:
	typedef typename SimplexCommon<T,DIM,DATA>::ARRAY	ARRAY;


	Simplex( const ARRAY& data) : SimplexCommon<T,DIM,DATA>( data)	{}

};


//////////////////////////////////////////////////////////////////////
// class Simplex - specialization for DIM_1D

template <class T, template< class T, size_t N > class DATA> 
class Simplex<T,DIM_1D,DATA> : public SimplexCommon<T,DIM_1D,DATA>
{
public:

	typedef typename SimplexCommon<T,DIM_1D,DATA>::GVect	GVect;
	typedef typename SimplexCommon<T,DIM_1D,DATA>::ARRAY	ARRAY;
	
	enum { SIZE = SimplexCommon<T,DIM_1D,DATA>::SIZE };
	

	Simplex( const ARRAY& data) : SimplexCommon<T,DIM_1D,DATA>( data)	{}


	template <class TC>
	bool	IsInside( const GVect& vct) const
	{
		typedef Simplex<T,DIM_1D,ArrayConstProxy> SimplexProxy;

		SimplexProxy	simp1( typename SimplexProxy::ARRAY( this->mData[0], vct) );
		TC vol1 = simp1.template Volume<TC>();

		SimplexProxy	simp2( typename SimplexProxy::ARRAY( vct, this->mData[1]) );
		TC vol2 = simp2.template Volume<TC>();

		if ( vol1 < TC(0) || vol2 < TC(0)  )
			return false;

		//for ( TSize iface=0; iface<SIZE; ++iface)
		//{
		//	TSize idn  = TopoSimplexOrientedFacet<DIM_1D>::cNodeId( iface, 0); 

		//	//Key<TSize,DIM_1D> key = this->GetFacetKeyNIn( iface);
		//	//SimplexProxy	simp( typename SimplexProxy::ARRAY( this->mData[key.cFirst()], vct) );

		//	SimplexProxy	simp( typename SimplexProxy::ARRAY( this->mData[idn], vct) );

		//	TC vol = simp.template Volume<TC>();

		//	//cout << iface << "  " << vol << endl;

		//	if ( vol < TC(0) )
		//		return false;
		//}

		return true;
	}

};




//////////////////////////////////////////////////////////////////////
// class Simplex - specialization for DIM_2D

template <class T, template< class T, size_t N > class DATA> 
class Simplex<T,DIM_2D,DATA> : public SimplexCommon<T,DIM_2D,DATA>
{
public:

	typedef typename SimplexCommon<T,DIM_2D,DATA>::GVect	GVect;
	typedef typename SimplexCommon<T,DIM_2D,DATA>::ARRAY	ARRAY;
	
	enum { SIZE = SimplexCommon<T,DIM_2D,DATA>::SIZE };
	

	Simplex( const ARRAY& data) : SimplexCommon<T,DIM_2D,DATA>( data)	{}


	template <class TC>
	bool	IsInside( const GVect& vct) const
	{
		typedef Simplex<T,DIM_2D,ArrayConstProxy> SimplexProxy;

		for ( TSize iface=0; iface<SIZE; ++iface)
		{
			Key<DIM_2D,TSize> key = this->GetFacetKeyNIn( iface);

			SimplexProxy	simp( typename SimplexProxy::ARRAY( this->mData[key.cFirst()], this->mData[key.cSecond()], vct) );

			TC vol = simp.template Volume<TC>();

			//cout << iface << "  " << vol << endl;

			if ( vol < TC(0) )
				return false;
		}

		return true;
	}

};



//////////////////////////////////////////////////////////////////////
// class Simplex - specialization for DIM_3D

template <class T, template< class T, size_t N > class DATA> 
class Simplex<T,DIM_3D,DATA> : public SimplexCommon<T,DIM_3D,DATA>
{
public:

	typedef typename SimplexCommon<T,DIM_3D,DATA>::GVect	GVect;
	typedef typename SimplexCommon<T,DIM_3D,DATA>::ARRAY	ARRAY;
	
	enum { SIZE = SimplexCommon<T,DIM_3D,DATA>::SIZE };
	

	Simplex( const ARRAY& data) : SimplexCommon<T,DIM_3D,DATA>( data)	{}


	template <class TC>
	bool	IsInside( const GVect& vct) const
	{
		typedef Simplex<T,DIM_3D,ArrayConstProxy> SimplexProxy;

		for ( TSize iface=0; iface<SIZE; ++iface)
		{
			Key<DIM_3D,TSize> key = this->GetFacetKeyNIn( iface);

			SimplexProxy	simp( typename SimplexProxy::ARRAY( this->mData[key.cFirst()], this->mData[key.cSecond()], this->mData[key.cThird()], vct) );

			TC vol = simp.template Volume<TC>();

			//cout << iface << "  " << vol << endl;

			if ( vol < TC(0) )
				return false;
		}

		return true;
	}

	template <class TC>
	TC	PartVolume( const GVect& vct, const TSize& iface) const
	{
		typedef Simplex<T,DIM_3D,ArrayConstProxy> SimplexProxy;

		Key<DIM_3D,TSize> key = this->GetFacetKeyNIn( iface);
		SimplexProxy	simp( typename SimplexProxy::ARRAY( this->mData[key.cFirst()], this->mData[key.cSecond()], this->mData[key.cThird()], vct) );

		TC vol = simp.template Volume<TC>();

		return vol;
	}

};




//////////////////////////////////////////////////////////////////////
// class Simplex - specialization for DIM_4D

template <class T, template< class T, size_t N > class DATA> 
class Simplex<T,DIM_4D,DATA> : public SimplexCommon<T,DIM_4D,DATA>
{
public:

	typedef typename SimplexCommon<T,DIM_4D,DATA>::GVect	GVect;
	typedef typename SimplexCommon<T,DIM_4D,DATA>::ARRAY	ARRAY;
	
	enum { SIZE = SimplexCommon<T,DIM_4D,DATA>::SIZE };
	

	Simplex( const ARRAY& data) : SimplexCommon<T,DIM_4D,DATA>( data)	{}


	template <class TC>
	bool	IsInside( const GVect& vct) const
	{
		typedef Simplex<T,DIM_4D,ArrayConstProxy> SimplexProxy;

		for ( TSize iface=0; iface<SIZE; ++iface)
		{
			Key<DIM_4D,TSize> key = this->GetFacetKeyNIn( iface);

			SimplexProxy	simp( typename SimplexProxy::ARRAY( this->mData[key.cFirst()], this->mData[key.cSecond()], this->mData[key.cThird()], this->mData[key.cFourth()], vct) );

			TC vol = simp.template Volume<TC>();

			//cout << iface << "  " << vol << endl;

			if ( vol < TC(0) )
				return false;
		}

		return true;
	}

};




} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;


#endif // __HF_SIMPLEX_H__

