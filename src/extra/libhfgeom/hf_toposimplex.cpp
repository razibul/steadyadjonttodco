#include "hf_toposimplex.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


//////////////////////////////////////////////////////////////////////
// class TopoSimplexFace
//////////////////////////////////////////////////////////////////////

template <>	TSize	TopoSimplexFace<1,1>::mtabConn[1][2]	= { {0,1} };
template <>	TSize	TopoSimplexFace<1,2>::mtabConn[3][2]	= { {0,1}, {0,2}, {1,2} };
template <>	TSize	TopoSimplexFace<1,3>::mtabConn[6][2]	= { {0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {2,3} };
template <>	TSize	TopoSimplexFace<1,4>::mtabConn[10][2]	= { {0,1}, {0,2}, {0,3}, {0,4}, {1,2}, {1,3}, {1,4}, {2,3}, {2,4}, {3,4} };
template <>	TSize	TopoSimplexFace<1,5>::mtabConn[15][2]	= { {0,1}, {0,2}, {0,3}, {0,4}, {0,5}, {1,2}, {1,3}, {1,4}, {1,5}, {2,3}, {2,4}, {2,5}, {3,4}, {3,5}, {4,5} };

template <>	TSize	TopoSimplexFace<2,2>::mtabConn[1][3]	= { {0,1,2} };
template <>	TSize	TopoSimplexFace<2,3>::mtabConn[4][3]	= { {0,1,2}, {0,1,3}, {0,2,3}, {1,2,3} };
template <>	TSize	TopoSimplexFace<2,4>::mtabConn[10][3]	= { {0,1,2}, {0,1,3}, {0,1,4}, {0,2,3}, {0,2,4}, {0,3,4}, {1,2,3}, {1,2,4}, {1,3,4}, {2,3,4} };
template <>	TSize	TopoSimplexFace<2,5>::mtabConn[20][3]	= { {0,1,2}, {0,1,3}, {0,1,4}, {0,1,5}, {0,2,3}, {0,2,4}, {0,2,5}, {0,3,4}, {0,3,5}, {0,4,5}, 
												{1,2,3}, {1,2,4}, {1,2,5}, {1,3,4}, {1,3,5}, {1,4,5}, {2,3,4}, {2,3,5}, {2,4,5}, {3,4,5} };

template <>	TSize	TopoSimplexFace<3,3>::mtabConn[1][4]	= { {0,1,2,3} };
template <>	TSize	TopoSimplexFace<3,4>::mtabConn[5][4]	= { {0,1,2,3}, {0,1,2,4}, {0,1,3,4}, {0,2,3,4}, {1,2,3,4} };
template <>	TSize	TopoSimplexFace<3,5>::mtabConn[15][4]	= { {0,1,2,3}, {0,1,2,4}, {0,1,2,5}, {0,1,3,4}, {0,1,3,5}, {0,1,4,5}, {0,2,3,4}, {0,2,3,5}, {0,2,4,5}, {0,3,4,5},
												{1,2,3,4}, {1,2,3,5}, {1,2,4,5}, {1,3,4,5}, {2,3,4,5} };


//////////////////////////////////////////////////////////////////////
// class TopoOrientedFacet - connectivity for Facet ( simplex DIM-1 )
//////////////////////////////////////////////////////////////////////

// OUTWARD normal 
template <>	TSize	TopoSimplexOrientedFacet<1>::mtabConn[2][1] = { {0}, {1} };
template <>	TSize	TopoSimplexOrientedFacet<2>::mtabConn[3][2] = { {2,1}, {0,2}, {1,0} };
template <>	TSize	TopoSimplexOrientedFacet<3>::mtabConn[4][3] = { {1,2,3}, {2,0,3}, {0,1,3}, {1,0,2} };
template <>	TSize	TopoSimplexOrientedFacet<4>::mtabConn[5][4] = { {2,1,3,4}, {0,2,3,4}, {1,0,3,4}, {0,1,2,4}, {1,0,2,3} };
template <>	TSize	TopoSimplexOrientedFacet<5>::mtabConn[6][5] = { {1,2,3,4,5}, {2,0,3,4,5}, {0,1,3,4,5}, {1,0,2,4,5}, {0,1,2,3,5}, {1,0,2,3,4} };



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

