#ifndef __HF_TREELEAF_H__
#define __HF_TREELEAF_H__

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_box.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {



//////////////////////////////////////////////////////////////////////
//  class ChildTab
//////////////////////////////////////////////////////////////////////
template <class DATA, TDimension DIM>
class ChildTab
{
public:
	enum { MASK = 1<<(DIM-1) };
	enum { SIZE = MASK << 1 };

	DATA&						operator() ( const TSize&	 i);
	const DATA&					operator() ( const TSize& i) const;

	ChildTab<DATA,DIM-1>&		operator[] ( const TSize& i)		{ return mtab[i];}
	const ChildTab<DATA,DIM-1>&	operator[] ( const TSize& i) const	{ return mtab[i];}

private:
	ChildTab<DATA,DIM-1>	mtab[2];
};
//////////////////////////////////////////////////////////////////////




template <class DATA, TDimension DIM>
inline DATA& ChildTab<DATA,DIM>::operator() ( const TSize& i)
{
	return mtab[(MASK&i)>>(DIM-1)]((~MASK)&i);
}

template <class DATA, TDimension DIM>
inline const DATA& ChildTab<DATA,DIM>::operator() ( const TSize& i) const
{
	return mtab[(MASK&i)>>(DIM-1)]((~MASK)&i);
}




//////////////////////////////////////////////////////////////////////
//  class ChildTab - specialization for DIM = 1
//////////////////////////////////////////////////////////////////////
template <class DATA>
class ChildTab< DATA, 1 >
{
public:
	enum { SIZE = 2 };

	DATA&		operator() ( const TSize& i)		{ return mtab[1&i];}
	const DATA&	operator() ( const TSize& i) const	{ return mtab[1&i];}
	DATA&		operator[] ( const TSize& i)		{ return mtab[i];}
	const DATA&	operator[] ( const TSize& i) const	{ return mtab[i];}

private:
	DATA				mtab[2];
};








template <class TELEM, class TCOMP, class DATA, TDimension DIM>
class Tree;

//////////////////////////////////////////////////////////////////////
//  class Leaf
//////////////////////////////////////////////////////////////////////
template <class TELEM, class TCOMP, class DATA, TDimension DIM>
class Leaf
{
	friend class Tree<TELEM,TCOMP,DATA,DIM>;
	
protected:
	typedef GVector<TELEM,DIM>			TVect;
	typedef GVector<TCOMP,DIM>			TCmpVect;
	typedef pair< TVect, DATA >			TPair;
	typedef GBox<TELEM,DIM>				TBox;
	typedef Leaf<TELEM,TCOMP,DATA,DIM>	TLeaf;
	typedef ChildTab< TLeaf*, DIM >		TTab;


	Leaf();
	Leaf( const TVect& vc, TLeaf *ptr = NULL);
	~Leaf();


	const DATA&		cData( const TSize& i) const	{ return mtabData[i].second;}
		  DATA&		rData( const TSize& i)			{ return mtabData[i].second;}

	const TVect&	cPos( const TSize& i) const		{ return mtabData[i].first;} 
		  TVect&	rPos( const TSize& i)			{ return mtabData[i].first;}


	TLeaf*			cChild( const TSize& i) const	{ return mtabChild(i); }
	TLeaf*&			rChild( const TSize& i)			{ return mtabChild(i); }

	const TVect&	cCenter() const					{ return mCenter;}
	TVect&			rCenter() 						{ return mCenter;}

	vector< TPair >&	rTab()						{ return mtabData;}

	bool		IsEmpty();
	bool		HaveData()					{ return (mtabData.size() > 0);}

	//TSize		FindChildIndex( const TVect& pos) const;

	TBox		FindBoundBox() const;

	void		ExportTEC( FILE *f);

	TLeaf*		DetachFromParent();
	TLeaf*		Traverse( const TVect& pos);

	void		Insert( const TVect& pos, const DATA& dat, const TSize& maxdsize, TBox box);
	void		RangeSearch( const TBox& rec, vector< TLeaf* >& lst);

	TSize		IndClosestData( const TVect& vct);
	
	void		Erase( const TVect& pos, const DATA& dat);



private:
	vector< TPair >		mtabData;		// used for storage of data connected to point

	TVect				mCenter;		// center point of the leaf; bbox is calc. thanks to parent informations
	TLeaf				*mpParent;		// pointer to parent leaf; for leafs attached to tree should be always valid

	TTab				mtabChild;	// table of children: 2 for 1D, 4 for 2D, 8 for 3D
};
//////////////////////////////////////////////////////////////////////





template <class TELEM, class TCOMP, class DATA, TDimension DIM>
inline Leaf<TELEM,TCOMP,DATA,DIM>::Leaf()	
{
	for ( TSize i=0; i<DIM; ++i)
		mCenter.rX(i)=TELEM(0);

	mpParent = NULL;

	//mtabData.reserve( cSize());

	for ( TSize i=0; i<TTab::SIZE; ++i)
		mtabChild(i) = NULL;
}


template <class TELEM, class TCOMP, class DATA, TDimension DIM>
inline Leaf<TELEM,TCOMP,DATA,DIM>::Leaf( const TVect& vc, TLeaf *ptr)	
{
	mCenter = vc;
	mpParent = ptr;

	//mtabData.reserve( cSize());

	for ( TSize i=0; i<TTab::SIZE; ++i)
		mtabChild(i) = NULL;
}


template <class TELEM, class TCOMP, class DATA, TDimension DIM>
inline Leaf<TELEM,TCOMP,DATA,DIM>::~Leaf()	
{
	for ( TSize i=0; i<TTab::SIZE; ++i)
		if ( mtabChild(i) )
		{
			delete mtabChild(i);
			mtabChild(i) = NULL;
		}
}



//////////////

//template <class TELEM, class TCOMP, class DATA, TDimension DIM>
//inline TSize Leaf<TELEM,TCOMP,DATA,DIM>::FindChildIndex( const TVect& pos) const
//{
//	TSize		ind = 0;
//
//	for ( TSize i=0; i<DIM; ++i)
//	{
//		ind <<= 1;
//
//		if ( pos.cX(i) > mCenter.cX(i) )
//		{
//			box.rVMin().rX(i) = mCenter.cX(i);
//			++ind;
//		}
//		else
//		{
//			box.rVMax().rX(i) = mCenter.cX(i);
//		}
//	}
//
//	return ind;
//}

template <class TELEM, class TCOMP, class DATA, TDimension DIM>
inline GBox<TELEM,DIM> Leaf<TELEM,TCOMP,DATA,DIM>::FindBoundBox() const
{
	TVect	vdst;
	TBox	rec;
	
	if ( mpParent)
	{
		vdst = mpParent->cCenter() - cCenter();

		for ( TSize i=0; i<DIM; ++i)
			vdst.rX(i) = abs( vdst.cX(i) );

		rec.rVMax() = cCenter() + vdst;
		rec.rVMin() = cCenter() - vdst;
	}

	return rec;
}


template <class TELEM, class TCOMP, class DATA, TDimension DIM>
inline void Leaf<TELEM,TCOMP,DATA,DIM>::Erase( const TVect& pos, const DATA& dat)
{
	bool bFound = false;
	typename vector<TPair>::iterator itr;

	for ( itr=mtabData.begin(); itr!=mtabData.end(); ++itr)
	{
		//if ( ((*itr).first - pos).module() < ZERO && (*itr).second == dat )
		//if ( ((*itr).first - pos).module() < 1.0e-8 && (*itr).second == dat )
		if ( (*itr).second == dat )
		{
			bFound = true;
			break;
		}
	}

	if ( bFound)
		mtabData.erase( itr);
	else
	{
		TRACE( "corrupted Leaf<TELEM,TCOMP,DATA,DIM>::Erase(...)");
		THROW_INTERNAL( "corrupted Leaf<TELEM,TCOMP,DATA,DIM>::Erase(...)");
	}
}



template <class TELEM, class TCOMP, class DATA, TDimension DIM>
inline TSize Leaf<TELEM,TCOMP,DATA,DIM>::IndClosestData( const TVect& vct)
{	
	TSize ind = 0;
	TCmpVect cvct = vct - cPos(0);
	TCOMP dst = sqrt( cvct * cvct );;

	for ( TSize k=0; k<mtabData.size(); ++k)
	{
		cvct = vct - cPos(k);
		TCOMP dmod = sqrt( cvct * cvct);;

		if ( dmod < dst )
		{
			ind = k;
			dst = dmod;
		}
	}

	return ind;
}




template <class TELEM, class TCOMP, class DATA, TDimension DIM>
inline Leaf<TELEM,TCOMP,DATA,DIM>* Leaf<TELEM,TCOMP,DATA,DIM>::DetachFromParent()
{
	if ( mpParent != NULL)
	{
		for ( TSize i=0; i<TTab::SIZE; ++i)
			if ( mpParent->cChild( i) == this)
				mpParent->rChild( i) = NULL;
	}

	return mpParent;
}


template <class TELEM, class TCOMP, class DATA, TDimension DIM>
bool Leaf<TELEM,TCOMP,DATA,DIM>::IsEmpty()
{
	for ( TSize i=0; i<TTab::SIZE; ++i)
		if ( this->cChild( i) != NULL)
			return false;

	return true;
}


template <class TELEM, class TCOMP, class DATA, TDimension DIM>
Leaf<TELEM,TCOMP,DATA,DIM>* Leaf<TELEM,TCOMP,DATA,DIM>::Traverse( const TVect& pos)
{
	TSize		k, ind=0;

	Leaf<TELEM,TCOMP,DATA,DIM>* plf;
	
	for ( TSize i=0; i<DIM; ++i)
	{
		ind <<= 1;

		if ( pos.cX(i) > mCenter.cX(i) )
			k = 1;
		else
			k = 0;

		ind += k;
	}

	plf = cChild( ind);
	
	if ( plf)
		return plf->Traverse( pos);

	return this;
}


template <class TELEM, class TCOMP, class DATA, TDimension DIM>
void Leaf<TELEM,TCOMP,DATA,DIM>::Insert( const TVect& pos, const DATA& dat, const TSize& maxdsize, TBox box)
{
	TBox		oldbox;
	TSize		ind;

	Leaf<TELEM,TCOMP,DATA,DIM>	*plf;


	if ( HaveData() )
	{
		for ( TSize i=0; i<mtabData.size(); ++i)
		{
			bool bOk = false;

			for ( TSize k=0; k<DIM; ++k)
				if ( pos.cX(k) != cPos(i).cX(k) )
					bOk = true;

			if ( ! bOk )
			{
				//TRACE( "new point (" << pos.cX() << ", " << pos.cY() << ", " << pos.cZ() 
				//	<< ") old point (" << cPos(i).cX() << ", " << cPos(i).cY() << ", " << cPos(i).cZ() << ")" );

				THROW_INTERNAL( "Leaf<DATA>::Insert - trying to insert two points with the same coordinates");
			}
		}
	}

	// check if it has at leas one child
	bool b = true;
	for ( TSize i=0; i<TTab::SIZE; ++i)
		if ( cChild(i) )
		{
			b = false;
			break;
		}

	// if there is a place in the mtabData and there are NO childs
	if ( (mtabData.size() < maxdsize ) && b)
	{
		mtabData.push_back( TPair( pos, dat) );
		return;
	}
	
	oldbox = box;

	// find index of a child containing the point
	ind = 0;
	for ( TSize i=0; i<DIM; ++i)
	{
		ind <<= 1;

		if ( pos.cX(i) > mCenter.cX(i) )
		{
			box.rVMin().rX(i) = mCenter.cX(i);
			++ind;
		}
		else
		{
			box.rVMax().rX(i) = mCenter.cX(i);
		}
	}

	plf = cChild( ind);


	// if at ind there is no child create one
	if ( plf == NULL)
	{
		plf = new Leaf<TELEM,TCOMP,DATA,DIM>( box.Center(), this );
		rChild( ind) = plf;
	}

	plf->Insert( pos, dat, maxdsize, box );

	// this leaf is terminal no more
	// move all data to its child-leafs
	if ( HaveData() )
	{
		for ( TSize k=0; k<mtabData.size(); ++k)
		{
			box = oldbox;

			ind = 0;
			for ( TSize i=0; i<DIM; ++i)
			{
				ind <<= 1;

				if ( cPos(k).cX(i) > mCenter.cX(i) )
				{
					box.rVMin().rX(i) = mCenter.cX(i);
					++ind;
				}
				else
				{
					box.rVMax().rX(i) = mCenter.cX(i);
				}
			}


			plf = cChild( ind);

			if ( plf == NULL)
			{
				plf = new Leaf<TELEM,TCOMP,DATA,DIM>( box.Center(), this );
				rChild( ind) = plf;
			}

			plf->Insert( cPos(k), cData(k), maxdsize, box );

		}

		mtabData.clear();
	}

}





template <class TELEM, class TCOMP, class DATA, TDimension DIM>
void Leaf<TELEM,TCOMP,DATA,DIM>::RangeSearch( const TBox& rec, vector<Leaf<TELEM,TCOMP,DATA,DIM>*>& lst)
{
	TBox	bbox;
	
	bbox = FindBoundBox();
	
	if ( rec.IsOverlapping( bbox) )
	{
		if ( HaveData() )
			//lst.insert( lst.end(), this);
			lst.push_back( this);
		else
			for ( TSize i=0; i<TTab::SIZE; ++i)
				if ( cChild( i) )
					cChild( i)->RangeSearch( rec, lst);
	}
}






template <class TELEM, class TCOMP, class DATA, TDimension DIM>
void Leaf<TELEM,TCOMP,DATA,DIM>::ExportTEC( FILE *f)
{
	TBox 		box;
	
	box = FindBoundBox();
	box.ExportTEC( f);

	for ( TSize i=0; i<TTab::SIZE; ++i)
		if ( this->Child(i) )
			this->Child(i)->ExportTEC( f);

}





} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_TREELEAF_H__
