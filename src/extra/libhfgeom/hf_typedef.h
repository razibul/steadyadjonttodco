#ifndef __HF_TYPEDEF_H__
#define __HF_TYPEDEF_H__

//#include "sysdecl.h"
//#include "typedef.h"
#include "libcoresystem/mgdecl.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {

//using namespace GenericTypeDef;


typedef size_t	TSize;
typedef TSize	TDimension;

enum
{
	DIM_0D		= 0,
	DIM_1D		= 1,
	DIM_2D		= 2,
	DIM_3D		= 3,
	DIM_4D		= 4
};


TDimension StrToDim( const string& str);
string DimToStr( const TDimension& dim);


//const TDimension DIM_0D		= 0;
//const TDimension DIM_1D		= 1;
//const TDimension DIM_2D		= 2;
//const TDimension DIM_3D		= 3;
//const TDimension DIM_4D		= 4;


} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;


#endif // __HF_TYPEDEF_H__
