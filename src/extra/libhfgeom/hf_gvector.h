#ifndef __HF_GVECTOR_H__
#define __HF_GVECTOR_H__


//#include "sysdecl.h"
#include "libcoresystem/mgdecl.h"
#include "libhfgeom/hf_typedef.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {




template <class TELEM, TDimension MAX_SIZE>
class GVector;

template <class TELEM, TDimension MAX_SIZE>
GVector<TELEM,MAX_SIZE> operator *( const TELEM&,  const GVector<TELEM,MAX_SIZE>&);

template <class TELEM, TDimension MAX_SIZE>
GVector<TELEM,MAX_SIZE> operator *( const GVector<TELEM,MAX_SIZE>&, const TELEM&);

template <class TELEM, TDimension MAX_SIZE>
GVector<TELEM,MAX_SIZE> operator /( const GVector<TELEM,MAX_SIZE>&, const TELEM&);

template <class TELEM, TDimension MAX_SIZE>
TELEM	operator *( const GVector<TELEM,MAX_SIZE>&, const GVector<TELEM,MAX_SIZE>&);   // dot product

template <class TELEM, TDimension MAX_SIZE>
GVector<TELEM,MAX_SIZE> operator +( const GVector<TELEM,MAX_SIZE>&, const GVector<TELEM,MAX_SIZE>&);

template <class TELEM, TDimension MAX_SIZE>
GVector<TELEM,MAX_SIZE> operator -( const GVector<TELEM,MAX_SIZE>&, const GVector<TELEM,MAX_SIZE>&);

template <class TELEM, TDimension MAX_SIZE>
GVector<TELEM,MAX_SIZE> operator -( const GVector<TELEM,MAX_SIZE>&);

template <class TELEM, TDimension MAX_SIZE>
GVector<TELEM,MAX_SIZE> Minimum( const GVector<TELEM,MAX_SIZE>& vec1,  const GVector<TELEM,MAX_SIZE>& vec2 );

template <class TELEM, TDimension MAX_SIZE>
GVector<TELEM,MAX_SIZE> Maximum( const GVector<TELEM,MAX_SIZE>& vec1,  const GVector<TELEM,MAX_SIZE>& vec2 );



//////////////////////////////////////////////////////////////////////
//	class GVector
//////////////////////////////////////////////////////////////////////
template <class TELEM, TDimension MAX_SIZE>
class GVector
{
public:
	typedef TELEM	TYPE;

	//////////////////////////////////////////////////////////////////
	// constructors
	GVector();
	GVector( const TELEM& x);
	GVector( const TELEM& x, const TELEM& y);
	GVector( const TELEM& x, const TELEM& y, const TELEM& z);
	GVector( const TELEM& x, const TELEM& y, const TELEM& z, const TELEM& w);

	template <class T> 
	GVector( const GVector<T,MAX_SIZE> &vec);

	GVector( const GVector<TELEM,MAX_SIZE+1> &vct, const TSize& idim)
	{
		TELEM tab[MAX_SIZE+1];
		for ( TSize i=0; i<=MAX_SIZE; ++i)
			tab[i] = vct.cX(i);

		rotate( tab, tab+idim, tab+MAX_SIZE+1 );

		for ( TSize i=1; i<=MAX_SIZE; ++i)
				rX(i-1) = tab[i];
	}

	GVector<TELEM,MAX_SIZE+1> ElevateDim( const TELEM& c, const TSize& idim)
	{
		GVector<TELEM,MAX_SIZE+1> vct;
		TELEM tab[MAX_SIZE+1];

		for ( TSize i=0; i<MAX_SIZE; ++i)
			tab[i] = this->cX(i);
		tab[MAX_SIZE] = c;

		rotate( tab, tab+MAX_SIZE-idim, tab+MAX_SIZE+1 );

		for ( TSize i=0; i<=MAX_SIZE; ++i)
				vct.rX(i) = tab[i];

		return vct;
	}

	GVector<TELEM,MAX_SIZE+1> ElevateDim( const TELEM& c)
	{
		GVector<TELEM,MAX_SIZE+1> vct;
		for ( TSize i=0; i<MAX_SIZE; ++i)
			vct.rX(i) = this->cX(i);
		vct.rX(MAX_SIZE) = c;

		return vct;
	}


    //////////////////////////////////////////////////////////////////
	// declaration of friend two argument operators
	friend GVector		operator *<>( const TELEM&,  const GVector&);
	friend GVector		operator *<>( const GVector&, const TELEM&);
	friend GVector		operator /<>( const GVector&, const TELEM&);
	friend TELEM		operator *<>( const GVector&, const GVector&);   // dot product
	friend GVector		operator +<>( const GVector&, const GVector&);
	friend GVector		operator -<>( const GVector&, const GVector&);
	friend GVector		operator -<>( const GVector&);

	friend GVector		Minimum<>( const GVector&, const GVector& );
	friend GVector		Maximum<>( const GVector&, const GVector& );


    //////////////////////////////////////////////////////////////////
	// one argument operators
	GVector<TELEM,MAX_SIZE>&	operator  =( const GVector<TELEM,MAX_SIZE> &vec);
	GVector<TELEM,MAX_SIZE>&	operator +=( const GVector<TELEM,MAX_SIZE>&);
	GVector<TELEM,MAX_SIZE>&	operator -=( const GVector<TELEM,MAX_SIZE>&);
	GVector<TELEM,MAX_SIZE>&	operator *=( const TELEM&);
	GVector<TELEM,MAX_SIZE>&	operator /=( const TELEM&);


	const TSize		NRows() const	{ return MAX_SIZE;}
	const TSize		Size() const	{ return MAX_SIZE;}

	const TELEM&	operator []( const TSize& i) const	{ return mtab[i];}
	TELEM&			operator []( const TSize& i)		{ return mtab[i];}

	const TELEM&	operator ()( const TSize& i) const	{ return mtab[i];}
	TELEM&			operator ()( const TSize& i)		{ return mtab[i];}

	const TELEM&	cX( const TSize& i) const			{ return mtab[i];}
	TELEM&			rX( const TSize& i)					{ return mtab[i];}

	const TELEM&	cX() const		{ return mtab[0];}
	const TELEM&	cY() const		{ ASSERT(MAX_SIZE>1); return mtab[1];}
	const TELEM&	cZ() const		{ ASSERT(MAX_SIZE>2); return mtab[2];}
	const TELEM&	cW() const		{ ASSERT(MAX_SIZE>3); return mtab[3];}

	TELEM&			rX()			{ return mtab[0];}
	TELEM&			rY()			{ ASSERT(MAX_SIZE>1); return mtab[1];}
	TELEM&			rZ()			{ ASSERT(MAX_SIZE>2); return mtab[2];}
	TELEM&			rW()			{ ASSERT(MAX_SIZE>3); return mtab[3];}

	const TELEM*	cTab() const	{ return mtab;}

	void	Write( const TSize& iprec=5, ostream& f=cout) const;

protected:
	TELEM mtab[MAX_SIZE];
};
//////////////////////////////////////////////////////////////////////



template <class TELEM, TDimension MAX_SIZE>
template <class T> 
inline GVector<TELEM,MAX_SIZE>::GVector( const GVector<T,MAX_SIZE> &vec)
{
	for ( TSize i=0; i<MAX_SIZE; ++i)
		mtab[i] = TELEM( vec.cX(i) );
}

//template <class TELEM, TDimension MAX_SIZE>
//inline GVector<TELEM,MAX_SIZE>::GVector( const GVector<TELEM,MAX_SIZE> &vec)
//{
//	for ( TSize i=0; i<MAX_SIZE; ++i)
//		mtab[i] = vec.mtab[i];
//}



template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE>::GVector( const TELEM& x)
{
	for ( TSize i=0; i<MAX_SIZE; mtab[i++]=x);
}

template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE>::GVector( const TELEM& x, const TELEM& y)
{
		ASSERT( MAX_SIZE == 2);
		mtab[0] = x;
		mtab[1] = y;
}

template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE>::GVector( const TELEM& x, const TELEM& y, const TELEM& z)
{
		ASSERT( MAX_SIZE == 3);
		mtab[0] = x;
		mtab[1] = y;
		mtab[2] = z;
}

template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE>::GVector( const TELEM& x, const TELEM& y, const TELEM& z, const TELEM& w)
{
		ASSERT( MAX_SIZE == 4);
		mtab[0] = x;
		mtab[1] = y;
		mtab[2] = z;
		mtab[3] = w;
}


template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE>::GVector()
{
	for ( TSize i=0; i<MAX_SIZE; mtab[i++]=TELEM(0) );
}




template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE>& GVector<TELEM,MAX_SIZE>::operator =( const GVector<TELEM,MAX_SIZE> &vec)
{
	for ( TSize i=0; i<MAX_SIZE; ++i)
		mtab[i] = vec.mtab[i];
	return *this;
}

template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE>& GVector<TELEM,MAX_SIZE>::operator+=( const GVector<TELEM,MAX_SIZE>& vec)
{
	for ( TSize i=0; i<MAX_SIZE; ++i)
		mtab[i] += vec.mtab[i];
	return *this;
}


template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE>& GVector<TELEM,MAX_SIZE>::operator-=( const GVector<TELEM,MAX_SIZE>& vec)
{
	for ( TSize i=0; i<MAX_SIZE; ++i)
		mtab[i] -= vec.mtab[i];
	return *this;
}

template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE>& GVector<TELEM,MAX_SIZE>::operator*=( const TELEM& doub)
{
	for ( TSize i=0; i<MAX_SIZE; ++i)
		mtab[i] *= doub;
	return *this;
}

template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE>& GVector<TELEM,MAX_SIZE>::operator/=( const TELEM& doub)
{
	for ( TSize i=0; i<MAX_SIZE; ++i)
		mtab[i] /= doub;
	return *this;
}


template <class TELEM, TDimension MAX_SIZE>
inline void GVector<TELEM,MAX_SIZE>::Write( const TSize& iprec, ostream& f) const
{
	f << "[ ";
	for( TSize i=0; i<MAX_SIZE; i++)
		f << /*setw(12) <<*/ setprecision(iprec) << cX(i) << " ";
	f << "]\n";
}




//////////////////////////////////////////////////////////////////////

template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE> operator-( const GVector<TELEM,MAX_SIZE>& vec)
{
	GVector<TELEM,MAX_SIZE>	v;
	for ( TSize i=0; i<MAX_SIZE; ++i)
		v.mtab[i] = - vec.mtab[i];
	return v;
}


template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE> operator-( const GVector<TELEM,MAX_SIZE>& vec1, const GVector<TELEM,MAX_SIZE>& vec2)
{
	GVector<TELEM,MAX_SIZE>	v;
	for ( TSize i=0; i<MAX_SIZE; ++i)
		v.mtab[i] = vec1.mtab[i] - vec2.mtab[i];
	return v;
}

template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE> operator+( const GVector<TELEM,MAX_SIZE>& vec1, const GVector<TELEM,MAX_SIZE>& vec2)
{
	GVector<TELEM,MAX_SIZE>	v;
	for ( TSize i=0; i<MAX_SIZE; ++i)
		v.mtab[i] = vec1.mtab[i] + vec2.mtab[i];
	return v;
}

template <class TELEM, TDimension MAX_SIZE>
inline TELEM operator*( const GVector<TELEM,MAX_SIZE>& vec1, const GVector<TELEM,MAX_SIZE>& vec2)
{
	TELEM	e(0);
	for ( TSize i=0; i<MAX_SIZE; ++i)
		e += vec1.mtab[i] * vec2.mtab[i];
	return e;
}

template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE> operator*( const TELEM& e, const GVector<TELEM,MAX_SIZE>& vec)
{
	GVector<TELEM,MAX_SIZE>	v;
	for ( TSize i=0; i<MAX_SIZE; ++i)
		v.mtab[i] = e * vec.mtab[i];
	return v;
}

template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE> operator*( const GVector<TELEM,MAX_SIZE>& vec, const TELEM& e)
{
	GVector<TELEM,MAX_SIZE>	v;
	for ( TSize i=0; i<MAX_SIZE; ++i)
		v.mtab[i] = e * vec.mtab[i];
	return v;
}

template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE> operator/( const GVector<TELEM,MAX_SIZE>& vec, const TELEM& e)
{
	GVector<TELEM,MAX_SIZE>	v;
	for ( TSize i=0; i<MAX_SIZE; ++i)
		v.mtab[i] = vec.mtab[i] / e;
	return v;
}


template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE> Minimum(  const GVector<TELEM,MAX_SIZE>& vec1,  const GVector<TELEM,MAX_SIZE>& vec2 )
{
	GVector<TELEM,MAX_SIZE>	v;
	for ( TSize i=0; i<MAX_SIZE; ++i)
		v.mtab[i] = ( vec1.mtab[i] < vec2.mtab[i] )  ?  vec1.mtab[i]  : vec2.mtab[i];
	return v;
}

template <class TELEM, TDimension MAX_SIZE>
inline GVector<TELEM,MAX_SIZE> Maximum(  const GVector<TELEM,MAX_SIZE>& vec1,  const GVector<TELEM,MAX_SIZE>& vec2 )
{
	GVector<TELEM,MAX_SIZE>	v;
	for ( TSize i=0; i<MAX_SIZE; ++i)
		v.mtab[i] = ( vec1.mtab[i] > vec2.mtab[i] )  ?  vec1.mtab[i]  : vec2.mtab[i];
	return v;
}





template <class TELEM, TDimension MAX_SIZE>
inline bool operator == ( const GVector<TELEM,MAX_SIZE>& v1, const GVector<TELEM,MAX_SIZE>& v2 )
{
	for ( TSize i=0; i<MAX_SIZE; ++i)
		if ( v1.cX(i) != v2.cX(i) )
			return false;

	return true;
}


template <class TELEM, TDimension MAX_SIZE>
inline bool operator < ( const GVector<TELEM,MAX_SIZE>& v1, const GVector<TELEM,MAX_SIZE>& v2 )
{
	if ( v1.cX() < v2.cX() )
		return true;

	if ( v1.cX() > v2.cX() )
		return false;

	GVector<TELEM,MAX_SIZE-1> w1, w2;

	for ( TSize i=1; i<MAX_SIZE; ++i)
	{
		w1.rX( i-1 ) = v1.cX(i);
		w2.rX( i-1 ) = v2.cX(i);
	}

	return w1 < w2;
}


template <class TELEM>
inline bool operator < ( const GVector<TELEM,1>& v1, const GVector<TELEM,1>& v2 )
{
	return v1.cX() < v2.cX();
}



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_GVECTOR_H__
