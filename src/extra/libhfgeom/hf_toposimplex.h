#ifndef __HF_TOPOSIMPLEX_H__
#define __HF_TOPOSIMPLEX_H__

#include "libhfgeom/hf_typedef.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {




//////////////////////////////////////////////////////////////////////
// class TopoSimplexFace
//////////////////////////////////////////////////////////////////////
template <TDimension D, TDimension DIM>
class TopoSimplexFace
{
public:
	enum { SIZE = TopoSimplexFace<D-1,DIM>::SIZE * (DIM + 1 - D) / (D + 1)  };

	static const TSize& cNodeId( const TSize& ident, const TSize& idnod)	{ return mtabConn[ident][idnod]; }

private:
	static TSize	mtabConn[SIZE][D+1];
};

template <TDimension DIM>
class TopoSimplexFace<DIM_0D,DIM>
{
public:
	enum { SIZE = DIM+1 };
};



//////////////////////////////////////////////////////////////////////
// class TopoOrientedFacet - connectivity for Facet ( simplex DIM-1 )
//////////////////////////////////////////////////////////////////////
template <TDimension DIM> 
class TopoSimplexOrientedFacet
{
public:
	enum { SIZE = DIM+1};

	static const TSize& cNodeId( const TSize& ident, const TSize& idnod)	{ return mtabConn[ident][idnod]; }

private:
	static TSize	mtabConn[SIZE][DIM];
};




} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;


#endif // __HF_TOPOSIMPLEX_H__

