#ifndef __HF_INITMATRIX_H__
#define __HF_INITMATRIX_H__


//#include "sysdecl.h"
#include "libcoresystem/mgdecl.h"
#include "libhfgeom/hf_typedef.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {

template< class T, class MATRIX>
void InitMatrix_Hilbert( MATRIX& mtx, const TSize& n )
{
	if ( n > mtx.MaxSize() )
		THROW_INTERNAL( "n > mtx::MaxSize()");

	mtx.Resize( n, n);

	for ( TSize i=0; i<n; ++i)
		for ( TSize j=0; j<n; ++j)
			mtx(i,j) = T(1) / T(i+j+1.);
}

//////////////////////////////////////////////////////////////////////
// class InitMatrix
//////////////////////////////////////////////////////////////////////
template< class T, class MATRIX>
class InitMatrix
{
public:
	static void Diagonal( MATRIX& mtx, const TSize& n, const T& r );
	static void Hilbert( MATRIX& mtx, const TSize& n );
	static void Random( MATRIX& mtx, const TSize& n, const T& rmin, const T& rmax );
	static void RandomSymmetric( MATRIX& mtx, const TSize& n, const T& rmin, const T& rmax );

};
//////////////////////////////////////////////////////////////////////



template< class T, class MATRIX>
void InitMatrix<T,MATRIX>::Diagonal( MATRIX& mtx, const TSize& n, const T& r )
{
	if ( n > mtx.MaxSize() )
		THROW_INTERNAL( "n > mtx::MaxSize()");

	mtx.Resize( n, n);

	for ( TSize i=0; i<n; ++i)
	{
		for ( TSize j=0; j<n; ++j)
			mtx(i,j) = T(0);
		mtx(i,i) = r;
	}
}


template< class T, class MATRIX>
void InitMatrix<T,MATRIX>::Hilbert( MATRIX& mtx, const TSize& n )
{
	if ( n > mtx.MaxSize() )
		THROW_INTERNAL( "n > mtx::MaxSize()");

	mtx.Resize( n, n);

	for ( TSize i=0; i<n; ++i)
		for ( TSize j=0; j<n; ++j)
			mtx(i,j) = T(1) / T(i+j+1.);
}

template< class T, class MATRIX>
void InitMatrix<T,MATRIX>::Random( MATRIX& mtx, const TSize& n, const T& rmin, const T& rmax )
{
	if ( n > mtx.MaxSize() )
		THROW_INTERNAL( "n > mtx::MaxSize()");

	mtx.Resize( n, n);

	for ( TSize i=0; i<n; ++i)
		for ( TSize j=0; j<n; ++j)
		{
			T u = T(  ( T(rand()) * (rmax - rmin) ) / ( T(RAND_MAX) + T(1) ) + rmin );
			//T u = T( ( long(rand()) * long( 200) ) / long(RAND_MAX + 1) - long(100) );

			mtx(i,j) = u;
		}
}

template< class T, class MATRIX>
void InitMatrix<T,MATRIX>::RandomSymmetric( MATRIX& mtx, const TSize& n, const T& rmin, const T& rmax )
{
	if ( n > mtx.MaxSize() )
		THROW_INTERNAL( "n > mtx::MaxSize()");

	mtx.Resize( n, n);

	for ( TSize i=0; i<n; ++i)
	{
		mtx(i,i) = T(  ( T(rand()) * (rmax - rmin) ) / ( T(RAND_MAX) + T(1) ) + rmin );

		for ( TSize j=i+1; j<n; ++j)
		{
			T u = T(  ( T(rand()) * (rmax - rmin) ) / ( T(RAND_MAX) + T(1) ) + rmin );
			//T u = T( ( long(rand()) * long( 200) ) / long(RAND_MAX + 1) - long(100) );

			mtx(j,i) = mtx(i,j) = u;
		}
	}
}


} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;

#endif // __HF_INITMATRIX_H__
