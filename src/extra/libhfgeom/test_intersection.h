#ifndef __HF_TEST_INTERSECTION_H__
#define __HF_TEST_INTERSECTION_H__


//#include "sysdecl.h"

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_intersection.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


//////////////////////////////////////////////////////////////////////
// class Test_Rational
//////////////////////////////////////////////////////////////////////
class Test_Intersection
{
public:
	bool	Run();

//private:

// FULL

template <class T, class TC>
bool CaseFull_0vs3in3D()
{
	typedef HFGeom::GVector<T,3> Vector;

	SubSimplex<T,0,3> suba( typename SubSimplex<T,0,3>::ARRAY( Vector(  T(1), T(1), T(1)) ) );	
	SubSimplex<T,3,3> subb( typename SubSimplex<T,3,3>::ARRAY( Vector(  T(-1), T(-1), T(-1)), Vector( T(1), T(0), T(0)), Vector( T(0), T(1), T(0)), Vector( T(0), T(0), T(1)) ) );	

	HFGeom::IntersectionFull<T,TC,0,3> inter( suba, subb);
	inter.Execute();

	cout << endl << "--- Test_Intersection::CaseFull_0vs3in3D() ---" << endl;
	PrintResults( inter);

	return true;
}
	
template <class T, class TC>
bool CaseFull_1vs2in3D()
{
	typedef HFGeom::GVector<T,3> Vector;

	SubSimplex<T,1,3> suba( typename SubSimplex<T,1,3>::ARRAY( Vector( -T(1), T(3), T(1)), Vector( T(8), T(0), T(2)) ) );	
	SubSimplex<T,2,3> subb( typename SubSimplex<T,2,3>::ARRAY( Vector( T(1), T(1), T(1)), Vector( T(3), T(3), T(2)), Vector( T(0), T(4), T(2)) ) );	

	HFGeom::IntersectionFull<T,TC,1,2> inter( suba, subb);
	inter.Execute();

	cout << endl << "--- Test_Intersection::CaseFull_1vs2in3D() ---" << endl;
	PrintResults( inter);

	return true;
}


// PROXY
template <class T, class TC>
bool Case_0vs0in3D()
{
	typedef HFGeom::GVector<T,3> Vector;

	SubSimplex<T,0,3> suba( typename SubSimplex<T,0,3>::ARRAY( Vector(  T(1), T(1), T(1)) ) );	
	SubSimplex<T,0,3> subb( typename SubSimplex<T,0,3>::ARRAY( Vector( -T(1), T(3), T(1)) ) );	

	HFGeom::IntersectionProx<T,TC,0,0,3> inter( suba, subb);
	inter.Execute();

	cout << endl << "--- Test_Intersection::Case_0vs0in3D() ---" << endl;
	PrintResults( inter);

	return true;
}


template <class T, class TC>
bool Case_0vs1in3D()
{
	typedef HFGeom::GVector<T,3> Vector;

	SubSimplex<T,0,3> suba( typename SubSimplex<T,0,3>::ARRAY( Vector(  T(1), T(1), T(1)) ) );	
	//SubSimplex<T,1,3> subb( typename SubSimplex<T,1,3>::ARRAY( Vector( -T(1), T(3), T(1)), Vector( T(8), T(0), T(1))) );	
	SubSimplex<T,1,3> subb( typename SubSimplex<T,1,3>::ARRAY( Vector(  T(2), T(2), T(1)), Vector( T(8), T(0), T(1))) );	

	HFGeom::IntersectionProx<T,TC,0,1,3> inter( suba, subb);
	inter.Execute();

	cout << endl << "--- Test_Intersection::Case_0vs1in3D() ---" << endl;
	PrintResults( inter);

	return true;
}

template <class T, class TC>
bool Case_0vs2in3D()
{
	typedef HFGeom::GVector<T,3> Vector;

	SubSimplex<T,0,3> suba( typename SubSimplex<T,0,3>::ARRAY( Vector(  T(5), T(1), T(2)) ) );	
	//SubSimplex<T,1,3> subb( typename SubSimplex<T,1,3>::ARRAY( Vector( -T(1), T(3), T(1)), Vector( T(8), T(0), T(1))) );	
	SubSimplex<T,2,3> subb( typename SubSimplex<T,2,3>::ARRAY( Vector(  T(2), T(2), T(1)), Vector( T(8), T(0), T(1)), Vector( T(0), T(8), T(1))) );	

	HFGeom::IntersectionProx<T,TC,0,2,3> inter( suba, subb);
	inter.Execute();

	cout << endl << "--- Test_Intersection::Case_0vs2in3D() ---" << endl;
	PrintResults( inter);

	return true;
}



template <class T, class TC>
bool Case_0vs3in3D()
{
	typedef HFGeom::GVector<T,3> Vector;

	SubSimplex<T,0,3> suba( typename SubSimplex<T,0,3>::ARRAY( Vector(  T(1), T(1), T(1)) ) );	
	SubSimplex<T,3,3> subb( typename SubSimplex<T,3,3>::ARRAY( Vector(  T(-1), T(-1), T(-1)), Vector( T(1), T(0), T(0)), Vector( T(0), T(1), T(0)), Vector( T(0), T(0), T(1)) ) );	

	HFGeom::IntersectionProx<T,TC,0,3,3> inter( suba, subb);
	inter.Execute();

	cout << endl << "--- Test_Intersection::Case_0vs3in3D() ---" << endl;
	PrintResults( inter);

	return true;
}


template <class T, class TC>
bool Case_1vs1in3D()
{
	typedef HFGeom::GVector<T,3> Vector;

	SubSimplex<T,1,3> suba( typename SubSimplex<T,1,3>::ARRAY( Vector( T(1), T(1), T(1)), Vector( T(3), T(3), T(2)) ) );	
	SubSimplex<T,1,3> subb( typename SubSimplex<T,1,3>::ARRAY( Vector( -T(1), T(3), T(1)), Vector( T(8), T(0), T(1))) );	

	HFGeom::IntersectionProx<T,TC,1,1,3> inter( suba, subb);
	inter.Execute();

	cout << endl << "--- Test_Intersection::Case_1vs1in3D() ---" << endl;
	PrintResults( inter);

	return true;
}


template <class T, class TC>
bool Case_1vs2in3D()
{
	typedef HFGeom::GVector<T,3> Vector;

	SubSimplex<T,1,3> suba( typename SubSimplex<T,1,3>::ARRAY( Vector( -T(1), T(3), T(1)), Vector( T(8), T(0), T(2)) ) );	
	SubSimplex<T,2,3> subb( typename SubSimplex<T,2,3>::ARRAY( Vector( T(1), T(1), T(1)), Vector( T(3), T(3), T(2)), Vector( T(0), T(4), T(2)) ) );	

	HFGeom::IntersectionProx<T,TC,1,2,3> inter( suba, subb);
	inter.Execute();

	cout << endl << "--- Test_Intersection::Case_1vs2in3D() ---" << endl;
	PrintResults( inter);

	return true;
}


template <class T, class TC>
bool Case_Proxy1vs1in3D()
{
	typedef HFGeom::GVector<T,3> Vector;
	//typedef HFGeom::SubSimplex<T,1,3,ArrayConstProxy> SSmpx;
	typedef HFGeom::SubSimplex<T,1,3,ArrayProxy> SSmpx;

	Vector va1( T(1), T(1), T(1)), va2( T(3), T(3), T(1));
	Vector vb1( -T(1), T(3), T(1)), vb2( T(8), T(0), T(1));
	//Vector vb1( -T(0), T(0), T(1)), vb2( T(8), T(0), T(1));

	SSmpx suba( typename SSmpx::ARRAY( va1, va2 ) );	
	SSmpx subb( typename SSmpx::ARRAY( vb1, vb2 ) );	

	//HFGeom::IntersectionProx<T,TC,1,1,3,ArrayConstProxy> inter( suba, subb);
	HFGeom::IntersectionProx<T,TC,1,1,3,ArrayProxy> inter( suba, subb);
	inter.Execute();

	cout << endl << "--- Test_Intersection::Case_Proxy1vs1in3D() ---" << endl;
	PrintResults( inter);

	va2 = Vector( T(3), T(3), T(2));
	inter.Execute();

	cout << endl << "--- Test_Intersection::Case_Proxy1vs1in3D() ---" << endl;
	PrintResults( inter);

	return true;
}



protected:
	template <class T, class TC, TDimension ADIM, TDimension BDIM, TDimension UNIDIM, template<class T,size_t N> class DATA >
	void PrintResults( IntersectionProx<T,TC,ADIM,BDIM,UNIDIM,DATA>& inter)
	{
		if ( ! inter.IsOk() )
		{
			cout << "intersection determinant is less then or equal ZERO" << endl;
			return;
		}

		TC	det = inter.cDet();
		TC	tabA[ADIM+1], tabB[BDIM+1];
		inter.GetABaryCoords( tabA);
		inter.GetBBaryCoords( tabB);

		cout << "det = " << det << endl;

		cout << "tabA = { " << tabA[0];
		for ( TSize i=1; i<=ADIM; ++i)
			cout << ", " << tabA[i];
		cout << " }" << endl;

		cout << "tabB = { " << tabB[0];
		for ( TSize i=1; i<=BDIM; ++i)
			cout << ", " << tabB[i];
		cout << " }" << endl;

		GVector<TC,UNIDIM> vpA, vpB, vpdist;
		inter.GetProPointA( vpA);
		inter.GetProPointB( vpB);

		cout << "vpA = "; vpA.Write();
		cout << "vpB = "; vpB.Write();

		vpdist = vpB - vpA;
		Rational<TC> derr2(vpdist*vpdist, det*det);
		derr2.Simplify();
		cout << "dist = " << derr2 << endl;

		Rational<TC> err = inter.Error2();
		err.Simplify();

		cout << "error = " << err << endl;
	}


	template <class T, class TC, TDimension ADIM, TDimension BDIM, template<class T,size_t N> class DATA >
	void PrintResults( IntersectionFull<T,TC,ADIM,BDIM,DATA>& inter)
	{
		cout << inter.cDet() << endl; 
		if ( ! inter.IsOk() )
		{
			cout << "intersection determinant is ZERO" << endl;
			return;
		}

		TC	det = inter.cDet();
		TC	tabA[ADIM+1], tabB[BDIM+1];
		inter.GetABaryCoords( tabA);
		inter.GetBBaryCoords( tabB);

		cout << "det = " << det << endl;

		cout << "tabA = { " << tabA[0];
		for ( TSize i=1; i<=ADIM; ++i)
			cout << ", " << tabA[i];
		cout << " }" << endl;

		cout << "tabB = { " << tabB[0];
		for ( TSize i=1; i<=BDIM; ++i)
			cout << ", " << tabB[i];
		cout << " }" << endl;

		GVector<TC,ADIM+BDIM> vpA, vpB, vpdist;
		inter.GetProPointA( vpA);
		inter.GetProPointB( vpB);

		cout << "vpA = "; vpA.Write();
		cout << "vpB = "; vpB.Write();

		vpdist = vpB - vpA;
		Rational<TC> derr2(vpdist*vpdist, det*det);
		derr2.Simplify();
		cout << "dist = " << derr2 << endl;

		//Rational<TC> err = inter.Error2();
		//err.Simplify();

		//cout << "error = " << err << endl;
	}

};
//////////////////////////////////////////////////////////////////////






} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_TEST_INTERSECTION_H__