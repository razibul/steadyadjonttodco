#ifndef __HF_TEST_RATIONAL_H__
#define __HF_TEST_RATIONAL_H__


//#include "sysdecl.h"
#include "libcoresystem/mgdecl.h"
#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_rational.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


//////////////////////////////////////////////////////////////////////
// class Test_Rational
//////////////////////////////////////////////////////////////////////
class Test_Rational
{
public:
	bool	Run();

//private:

	template <class T>
	bool CheckOperators();

};
//////////////////////////////////////////////////////////////////////


template <class T>
bool Test_Rational::CheckOperators()
{
	Rational<T> r1(1,2), r2(4,3), r3(5,7), r4(11,3), r;

	r = (r1+r2*r3)/r4;

	cout << "r = " << setprecision(50) << r.cNumerator() << " / " << r.cDenominator() << " (should be r = 183 / 462)" << endl;

	return true;
}




} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_TEST_RATIONAL_H__