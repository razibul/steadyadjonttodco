#ifndef __HF_RATIONAL_H__
#define __HF_RATIONAL_H__


#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_gvector.h"

#include "libcorecommon/array.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


template< class TELEM>
class Rational;

template< class TELEM>
Rational<TELEM> operator +( const Rational<TELEM>&, const Rational<TELEM>&);

template< class TELEM>
Rational<TELEM> operator -( const Rational<TELEM>&, const Rational<TELEM>&);

template< class TELEM>
Rational<TELEM> operator *( const Rational<TELEM>&, const Rational<TELEM>&);

template< class TELEM>
Rational<TELEM> operator /( const Rational<TELEM>&, const Rational<TELEM>&);


//////////////////////////////////////////////////////////////////////
// class Rational
//////////////////////////////////////////////////////////////////////
template< class TELEM>
class Rational
{
public:

	Rational() : mNumer(TELEM(0)), mDenom(TELEM(1))	{}
	Rational( const TELEM& num, const TELEM& den=TELEM(1) ) : mNumer(num), mDenom(den)	{}

	template <class T> 
	Rational( const Rational<T>& r) : mNumer( TELEM(r.num) ), mDenom( TELEM(r.den) )	{}

	friend Rational		operator +<>( const Rational&, const Rational&);
	friend Rational		operator -<>( const Rational&, const Rational&);
	friend Rational		operator *<>( const Rational&, const Rational&);
	friend Rational		operator /<>( const Rational&, const Rational&);

	Rational&	operator +=( const Rational&);
	Rational&	operator -=( const Rational&);
	Rational&	operator *=( const Rational&);
	Rational&	operator /=( const Rational&);

	const TELEM&	cNumer() const			{ return mNumer;}
	const TELEM&	cDenom() const			{ return mDenom;}

	TELEM&			rNumer()				{ return mNumer;}
	TELEM&			rDenom()				{ return mDenom;}

	void	Simplify();
	TELEM	Round();

protected:
	TELEM	FindGCD() const;

private:
	TELEM	mNumer;
	TELEM	mDenom;
};
//////////////////////////////////////////////////////////////////////



template< class TELEM>
void Rational<TELEM>::Simplify()
{
	TELEM d = FindGCD();

	ASSERT( mNumer%d == TELEM(0) && mDenom%d == TELEM(0) );

	if ( mDenom < TELEM(0) )
	{
		mDenom = - mDenom;
		mNumer = - mNumer;
	}

	mNumer /= d;
	mDenom /= d;
}

template<>
inline void Rational<double>::Simplify()
{
	mNumer /= mDenom;
	mDenom = 1;
}


template< class TELEM>
TELEM Rational<TELEM>::Round()
{
	// TODO :: check this !!!
	if ( mDenom < TELEM(0) )
	{
		mDenom = - mDenom;
		mNumer = - mNumer;
	}

	TELEM a = mNumer / mDenom;
	TELEM b = mNumer % mDenom;

	cout << a << " " << b << endl;

	if ( mDenom / b <= TELEM(2) )
		return a + TELEM(1);

	return a;
}



template< class TELEM>
TELEM Rational<TELEM>::FindGCD() const
{
	TELEM a=mNumer;
	TELEM b=mDenom;

    while ( true )
    {
        if ( a == TELEM(0) )
            return abs(b);
        b %= a;

        if ( b == TELEM(0) )
            return abs(a);
        a %= b;
    }

	return TELEM(1);
}



template< class TELEM>
inline Rational<TELEM>& Rational<TELEM>::operator+=( const Rational<TELEM>& r)
{
	mNumer = mNumer * r.mDenom + r.mNumer * mDenom;
	mDenom *= r.mDenom;
	return *this;
}

template< class TELEM>
inline Rational<TELEM>& Rational<TELEM>::operator-=( const Rational<TELEM>& r)
{
	mNumer = mNumer * r.mDenom - r.mNumer * mDenom;
	mDenom *= r.mDenom;
	return *this;
}

template< class TELEM>
inline Rational<TELEM>& Rational<TELEM>::operator*=( const Rational<TELEM>& r)
{
	mNumer *= r.mNumer;
	mDenom *= r.mDenom;
	return *this;
}

template< class TELEM>
inline Rational<TELEM>& Rational<TELEM>::operator/=( const Rational<TELEM>& r)
{
	mNumer *= r.mDenom;
	mDenom *= r.mNumer;
	return *this;
}


template< class TELEM>
ostream& operator <<( ostream& os, const Rational<TELEM>& r)
{
	os << "{" << r.cNumer() << " / " << r.cDenom() << "}";
	return os;
}


template< class TELEM>
inline Rational<TELEM> operator+( const Rational<TELEM>& r1, const Rational<TELEM>& r2)
{
	Rational<TELEM> r;
	r.mNumer = r1.mNumer * r2.mDenom + r2.mNumer * r1.mDenom;
	r.mDenom = r1.mDenom * r2.mDenom;
	return r;
}

template< class TELEM>
inline Rational<TELEM> operator-( const Rational<TELEM>& r1, const Rational<TELEM>& r2)
{
	Rational<TELEM> r;
	r.mNumer = r1.mNumer * r2.mDenom - r2.mNumer * r1.mDenom;
	r.mDenom = r1.mDenom * r2.mDenom;
	return r;
}

template< class TELEM>
inline Rational<TELEM> operator*( const Rational<TELEM>& r1, const Rational<TELEM>& r2)
{
	Rational<TELEM> r;
	r.mNumer = r1.mNumer * r2.mNumer;
	r.mDenom = r1.mDenom * r2.mDenom;
	return r;
}

template< class TELEM>
inline Rational<TELEM> operator/( const Rational<TELEM>& r1, const Rational<TELEM>& r2)
{
	Rational<TELEM> r;
	r.mNumer = r1.mNumer * r2.mDenom;
	r.mDenom = r1.mDenom * r2.mNumer;
	return r;
}






} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_RATIONAL_H__
