#ifndef __HF_EVALUATOR_H__  
#define __HF_EVALUATOR_H__  


#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_gvector.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {

//////////////////////////////////////////////////////////////////////
// class InitMatrix
//////////////////////////////////////////////////////////////////////
template< class T, TDimension DIM>
class Evaluator
{
public:
	typedef GVector<T,UNIDIM>	GVect;

	Evaluator( const T& h=0) : mH(h)	{}

private:
	T	mH;
};
//////////////////////////////////////////////////////////////////////


} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_EVALUATOR_H__  
