#ifndef __HF_SIMPLEXBASE_H__
#define __HF_SIMPLEXBASE_H__

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_gvector.h"
#include "libhfgeom/hf_box.h"

#include "libcorecommon/array.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {




//////////////////////////////////////////////////////////////////////
// class SimplexBase
//////////////////////////////////////////////////////////////////////
template < class T, TDimension DIM, TDimension UNIDIM, template< class T, size_t N > class DATA = ArrayData >
class SimplexBase
{
public:
	enum { SIZE = DIM+1};

	typedef GVector<T,UNIDIM>	GVect;
	typedef DATA<GVect,SIZE>	ARRAY;

protected:
	SimplexBase( const ARRAY& data) : mData( data)	{}

public:
	const GVect&	cPoint( const TSize& i) const		{ return mData[i]; }

	GBox<T,UNIDIM> GetBox()
	{
		GBox<T,UNIDIM> box;

		for ( TSize i=0; i<UNIDIM; ++i)
		{
			box.rVMin().rX(i) = box.rVMax().rX(i) = cPoint(0).cX(i);
			for ( TSize k=1; k<SIZE; ++k)
			{
				if ( cPoint(k).cX(i) > box.rVMax().cX(i) )
					box.rVMax().rX(i) = cPoint(k).cX(i);

				if ( cPoint(k).cX(i) < box.rVMin().cX(i) )
					box.rVMin().rX(i) = cPoint(k).cX(i);
			}
		}

		return box;
	}

protected:
	ARRAY	mData;
};




} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;


#endif // __HF_SIMPLEXBASE_H__

