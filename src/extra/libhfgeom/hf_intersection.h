#ifndef __HF_INTERSECTION_H__
#define __HF_INTERSECTION_H__

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_subsimplex.h"
#include "libhfgeom/hf_rational.h"
#include "libhfgeom/hf_gmatrix.h"
#include "libhfgeom/hf_determinant.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {



//////////////////////////////////////////////////////////////////////
// class IntersectionCommon
//////////////////////////////////////////////////////////////////////
template < class T, class TC, TDimension ADIM, TDimension BDIM, TDimension UNIDIM, template<class T,size_t N> class DATA = ArrayData >
class IntersectionCommon
{
public:
	typedef SubSimplex<T,ADIM,UNIDIM,DATA>	ASimplex;
	typedef SubSimplex<T,BDIM,UNIDIM,DATA>	BSimplex;
	typedef GVector<T,UNIDIM>	GVect;
	typedef GVector<TC,UNIDIM>	CVector;
	typedef GMatrix<TC,UNIDIM>	CMatrix;
	
	enum { SIZE = ADIM + BDIM};

public:
	IntersectionCommon( const ASimplex& smpxa, const BSimplex& smpxb ) : mSmpxA(smpxa), mSmpxB(smpxb), mDet(TC(0))		{ ASSERT( SIZE <= UNIDIM); }

	const ASimplex& cSimplexA() const	{ return mSmpxA; }
	const BSimplex& cSimplexB() const	{ return mSmpxB; }

	const TC&		cDet() const		{ return mDet; }

	void	GetABaryCoords( TC tab[ADIM+1]) const
	{
		TC sum = TC(mDet);
		for ( TSize i=0; i<ADIM; ++i)
			sum -= tab[i+1] = mtabCoor[i];
		tab[0] = sum;
	}

	void	GetBBaryCoords( TC tab[BDIM+1]) const
	{
		TC sum = TC(mDet);
		for ( TSize i=0; i<BDIM; ++i)
			sum -= tab[i+1] = mtabCoor[i+ADIM];
		tab[0] = sum;
	}

	void	GetProPointA( CVector& vpnt)
	{
		TC tab[ADIM+1];
		GetABaryCoords( tab);

		for ( TSize id=0; id<UNIDIM; ++id)
		{
			TC t(0);
			for ( TSize in=0; in<=ADIM; ++in)
				t += tab[in] * TC( mSmpxA.cPoint(in).cX(id) );
			vpnt.rX(id) = t;
		}
	}

	void	GetProPointB( CVector& vpnt)
	{
		TC tab[BDIM+1];
		GetBBaryCoords( tab);

		for ( TSize id=0; id<UNIDIM; ++id)
		{
			TC t(0);
			for ( TSize in=0; in<=BDIM; ++in)
				t += tab[in] * TC( mSmpxB.cPoint(in).cX(id) );
			vpnt.rX(id) = t;
		}
	}

protected:
	const ASimplex& mSmpxA;
	const BSimplex& mSmpxB;

	CMatrix		mMtxN;
	CVector		mVecOff;
	TC			mDet;
	TC			mtabCoor[SIZE];
};



//////////////////////////////////////////////////////////////////////
// class IntersectionFull
//
// coordinates comp:  R(TC) ~= ( ( ADIM + BDIM ) + 1 ) * R(T)
//////////////////////////////////////////////////////////////////////
template < class T, class TC, TDimension ADIM, TDimension BDIM, template<class T,size_t N> class DATA = ArrayData >
class IntersectionFull : public IntersectionCommon<T,TC,ADIM,BDIM,ADIM+BDIM,DATA>
{
public:
	enum { UNIDIM = ADIM + BDIM };
	enum { SIZE = ADIM + BDIM};

	typedef IntersectionCommon<T,TC,ADIM,BDIM,ADIM+BDIM,DATA> TBase;

	typedef typename TBase::ASimplex	ASimplex;
	typedef typename TBase::BSimplex	BSimplex;
	typedef typename TBase::GVect		GVect;
	typedef typename TBase::CVector	CVector;
	typedef typename TBase::CMatrix	CMatrix;

public:
	IntersectionFull( const ASimplex& smpxa, const BSimplex& smpxb ) : TBase(smpxa,smpxb)		{ ASSERT( SIZE == UNIDIM); }

	void	Execute();

	bool	IsOk() const	{ if ( this->mDet != TC(0)) return true; else return false; }
};



template < class T, class TC, TDimension ADIM, TDimension BDIM, template<class T,size_t N> class DATA >
void IntersectionFull<T,TC,ADIM,BDIM,DATA>::Execute()
{
	//this->mSmpxA.cPoint(0).Write();
	//this->mSmpxA.cPoint(1).Write();
	//cout << endl;
	//this->mSmpxB.cPoint(0).Write();
	//this->mSmpxB.cPoint(1).Write();
	//this->mSmpxB.cPoint(2).Write();

	// combine subspaces
	this->mMtxN.Resize( UNIDIM, SIZE );

	for ( TSize irow=0; irow<UNIDIM; ++irow)
	{
		for ( TSize icol=0; icol<ADIM; ++icol)
			this->mMtxN(irow, icol) = - TC( this->mSmpxA.cPoint(icol+1)[irow] - this->mSmpxA.cPoint(0)[irow] );

		for ( TSize icol=0; icol<BDIM; ++icol)
			this->mMtxN(irow, icol+ADIM) = TC( this->mSmpxB.cPoint(icol+1)[irow] - this->mSmpxB.cPoint(0)[irow] );

		this->mVecOff[irow] = TC( this->mSmpxA.cPoint(0)[irow] - this->mSmpxB.cPoint(0)[irow] );
	}

	// solving
	this->mDet = DeterminantFunc( this->mMtxN);
	//this->mMtxN.Write();

	//cout << "det = " << this->mDet << endl;

	//THROW_INTERNAL("STOP");

	for ( TSize iu=0; iu<SIZE; ++iu)
	{
		GMatrix<TC,SIZE>	mtx( this->mMtxN);
		for ( TSize irow=0; irow<SIZE; ++irow)
			mtx(irow,iu) = this->mVecOff[irow];

		this->mtabCoor[iu] = DeterminantFunc( mtx);
	}


	if ( this->mDet < TC(0) )
	{
		for ( TSize iu=0; iu<SIZE; ++iu)
			this->mtabCoor[iu] = - this->mtabCoor[iu];
		this->mDet = - this->mDet;
	}
}



//////////////////////////////////////////////////////////////////////
// class Intersection
//
// coordinates comp:  R(TC) ~= ( UNIDIM * ( ADIM + BDIM ) + 1 ) * R(T)
// error comp:        R(TC) ~= ( UNIDIM * ( ADIM + BDIM ) + 2 ) * R(T)
//////////////////////////////////////////////////////////////////////
template < class T, class TC, TDimension ADIM, TDimension BDIM, TDimension UNIDIM, template<class T,size_t N> class DATA = ArrayData >
class IntersectionProx : public IntersectionCommon<T,TC,ADIM,BDIM,UNIDIM,DATA>
{
public:
	enum { SIZE = ADIM + BDIM};

	typedef IntersectionCommon<T,TC,ADIM,BDIM,UNIDIM,DATA> TBase;

	typedef typename TBase::ASimplex	ASimplex;
	typedef typename TBase::BSimplex	BSimplex;
	typedef typename TBase::GVect		GVect;
	typedef typename TBase::CVector	CVector;
	typedef typename TBase::CMatrix	CMatrix;
	

public:
	IntersectionProx( const ASimplex& smpxa, const BSimplex& smpxb ) : TBase(smpxa,smpxb)		{ ASSERT( SIZE <= UNIDIM); }

	void	Execute();

	bool	IsOk() const	{ if ( this->mDet <= TC(0)) return false; else return true; }

	Rational<TC>	Error2() const		{ return Rational<TC>( this->mErr2, this->mDet); }
	const TC&		cError2() const		{ return mErr2; }

private:
	TC			mErr2;
};


template < class T, class TC, TDimension ADIM, TDimension BDIM, TDimension UNIDIM, template<class T,size_t N> class DATA >
void IntersectionProx<T,TC,ADIM,BDIM,UNIDIM,DATA>::Execute()
{
	// combine subspaces
	this->mMtxN.Resize( UNIDIM, SIZE );

	for ( TSize irow=0; irow<UNIDIM; ++irow)
	{
		for ( TSize icol=0; icol<ADIM; ++icol)
			this->mMtxN(irow, icol) = - TC( this->mSmpxA.cPoint(icol+1)[irow] - this->mSmpxA.cPoint(0)[irow] );

		for ( TSize icol=0; icol<BDIM; ++icol)
			this->mMtxN(irow, icol+ADIM) = TC( this->mSmpxB.cPoint(icol+1)[irow] - this->mSmpxB.cPoint(0)[irow] );

		this->mVecOff[irow] = TC( this->mSmpxA.cPoint(0)[irow] - this->mSmpxB.cPoint(0)[irow] );
	}

	//setup linear system
	GMatrix<TC,SIZE>	mtxM(SIZE,SIZE);
	GMatrix<TC,SIZE>	mtxR(SIZE,1);

	for ( TSize irow=0; irow<SIZE; ++irow)
		for ( TSize icol=0; icol<SIZE; ++icol)
		{
			TC tmp = TC(0);
			for ( TSize k=0; k<UNIDIM; ++k)
				tmp += this->mMtxN(k,irow) * this->mMtxN(k,icol);
			mtxM(irow,icol) = tmp;
		}

	for ( TSize irow=0; irow<SIZE; ++irow)
		{
			TC tmp = TC(0);
			for ( TSize k=0; k<UNIDIM; ++k)
				tmp += this->mMtxN(k,irow) * this->mVecOff[k];
			mtxR(irow,0) = tmp;
		}

	// solving
	this->mDet = DeterminantFunc( mtxM);

	CMatrix	mtxU(SIZE,1);

	for ( TSize iu=0; iu<SIZE; ++iu)
	{
		GMatrix<TC,SIZE>	mtx( mtxM);

		for ( TSize irow=0; irow<SIZE; ++irow)
			mtx(irow,iu) = mtxR(irow,0);

		this->mtabCoor[iu] = mtxU(iu,0) = DeterminantFunc( mtx);
		//cout << " coor[" << iu << "] = " << mtabCoor[iu] << endl;
	}

	// find error 
	CMatrix mtxOff( this->mVecOff);
	CMatrix mtxErr = this->mDet*mtxOff - this->mMtxN * mtxU;

	mtxOff.Transp();
	mtxErr = mtxOff * mtxErr;

	this->mErr2 = TC( mtxErr(0,0));

	//cout << mErr2 << endl;
}




//////////////////////////////////////////////////////////////////////
// class IntersectionProx
// specialization for 0D, 0D
//////////////////////////////////////////////////////////////////////
template < class T, class TC, TDimension UNIDIM, template<class T,size_t N> class DATA >
class IntersectionProx<T,TC,0,0,UNIDIM,DATA>
{
public:
	typedef SubSimplex<T,0,UNIDIM>	TSimplex;
	typedef GVector<T,UNIDIM>	GVect;
	typedef GVector<TC,UNIDIM>	CVector;

public:

	IntersectionProx( const TSimplex& smpxa, const TSimplex& smpxb ) : mSmpxA(smpxa), mSmpxB(smpxb)		{}

	void	Execute();

	bool	IsOk() const			{ return true; }


	const TC&		cError2() const		{ return mErr2; }
	Rational<TC>	Error2() const		{ return Rational<TC>( mErr2, TC(1)); }

	// for compatibilty
	TC		cDet() const							{ return TC(1); }	

	void	GetABaryCoords( TC tab[1]) const		{ tab[0] = TC(1); }
	void	GetBBaryCoords( TC tab[1]) const		{ tab[0] = TC(1); }

	void	GetProPointA( CVector& vpnt)			{ vpnt = CVector( mSmpxA.cPoint(0) ); }
	void	GetProPointB( CVector& vpnt)			{ vpnt = CVector( mSmpxB.cPoint(0) ); }

private:
	const TSimplex& mSmpxA;
	const TSimplex& mSmpxB;

	CVector		mVecOff;
	TC			mErr2;
};



template < class T, class TC, TDimension UNIDIM, template<class T,size_t N> class DATA >
void IntersectionProx<T,TC,0,0,UNIDIM,DATA>::Execute()
{
	for ( TSize irow=0; irow<UNIDIM; ++irow)
		mVecOff[irow] = TC( mSmpxA.cPoint(0)[irow] - mSmpxB.cPoint(0)[irow] );

	mErr2 = TC( mVecOff * mVecOff );
}


} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;


#endif // __HF_INTERSECTION_H__

