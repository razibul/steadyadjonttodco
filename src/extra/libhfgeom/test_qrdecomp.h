#ifndef __HF_TEST_QRDECOMP_H__
#define __HF_TEST_QRDECOMP_H__


//#include "sysdecl.h"
#include "libcoresystem/mgdecl.h"
#include "libhfgeom/hf_typedef.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {




//////////////////////////////////////////////////////////////////////
// class Test_QRDecomp
//////////////////////////////////////////////////////////////////////
class Test_QRDecomp
{
public:
	bool	Run();

//private:

	template <class T, TSize NMAX>
	bool CheckDecomposition( const TSize& nrow, const TSize& ncol);

	template <class T, TSize NMAX>
	bool CheckSolve( const TSize& n);

	template <class T, TSize NMAX>
	bool CheckDeterminant( const TSize& n);

	template <class T, TSize NMAX>
	void Exper( const TSize& nrow, const TSize& ncol);

	template <class T, TSize NMAX>
	void Exper2( const TSize& n);

};
//////////////////////////////////////////////////////////////////////


template <class T, TSize NMAX>
bool Test_QRDecomp::CheckDecomposition( const TSize& nrow, const TSize& ncol)
{
	typedef HFGeom::GMatrix<NMAX,T> MTX;

	if ( max(nrow, ncol) > NMAX)
		THROW_INTERNAL( "Test_QRDecomp : nrow or ncol is greater then " << NMAX );

	MTX	mtx(nrow,ncol);

	for ( TSize i=0; i<nrow; ++i)
		for ( TSize j=0; j<ncol; ++j)
			mtx(i,j) = T(1) / T(i+j+1.);

	HFGeom::QRDecomp< T, MTX >	qrd( mtx);

	qrd.Execute();

	cout << "error ERROR  = " << setprecision(50) << qrd.Error() << endl;

	MTX	mtxQ, mtxR, mtxQT, mtxI, mtxE;

	qrd.AssembleQ( mtxQ);
	qrd.AssembleR( mtxR);

	mtxE = mtxQ * mtxR - mtx;
	//mtxE.Write();

	T sumE = T(0);
	for ( TSize i=0; i<mtxE.NRows(); ++i)
		for ( TSize j=0; j<mtxE.NCols(); ++j)
			sumE += abs( mtxE(i,j) );
			//sumE += abs( mtxE(i,j) / mtx(i,j) );

	cout << "error Q*R - A  = " << setprecision(50) << sumE << endl;

	mtxQT = mtxQ;
	mtxQT.Transp();

	mtxI = mtxQT * mtxQ;
	//mtxI = mtxQ * mtxQT;
	//mtxI.Write();

	for ( TSize i=0; i<mtxI.NRows(); ++i)
		mtxI(i,i) -= T(1);

	T sumI = T(0);
	for ( TSize i=0; i<mtxI.NRows(); ++i)
		for ( TSize j=0; j<mtxI.NCols(); ++j)
			sumI += abs(mtxI(i,j));


	cout << "error QT*Q - I = " << setprecision(50) << sumI << endl;

	//cout << endl << endl;
	//mtxQ.Write();
	//mtxQT.Write();

	//mtxI.Write();

	//cout << endl << endl;
	//mtx = mtxQ * mtxR;
	//mtx.Write();

	return true;
}


template <class T, TSize NMAX>
bool Test_QRDecomp::CheckSolve( const TSize& n)
{
	typedef HFGeom::GMatrix<NMAX,T> MTX;

	//if ( max(nrow, ncol) > NMAX)
	//	THROW_INTERNAL( "Test_QRDecomp : nrow or ncol is greater then " << NMAX );

	MTX	mtx(n,n);
	MTX	mtxB(n,1);

	for ( TSize i=0; i<n; ++i)
		for ( TSize j=0; j<n; ++j)
			mtx(i,j) = T(1) / T(i+j+1.);

	for ( TSize i=0; i<n; ++i)
	{
		T t = T(0); 
		for ( TSize j=0; j<n; ++j)
			t += (j+1)*mtx(i,j);
		mtxB(i,0) = t;
	}

	HFGeom::QRDecomp< T, MTX >	qrd( mtx);

	qrd.Execute();
	qrd.Solve( mtxB);

	mtxB.Write( 32 );


	T err = T(0);
	for ( TSize i=0; i<mtxB.NRows(); ++i)
		err += abs( mtxB(i,0) - T(i+1) );

	cout << "error X = " << setprecision(50) << err << endl;

	return true;
}


template <class T, TSize NMAX>
bool Test_QRDecomp::CheckDeterminant( const TSize& n)
{
	typedef HFGeom::GMatrix<NMAX,T> MTX;
	if ( n > NMAX)
		THROW_INTERNAL( "Test_QRDecomp : n (" << n << ") is greater then " << NMAX );

	MTX	mtx(n,n);

	for ( TSize i=0; i<n; ++i)
		for ( TSize j=0; j<n; ++j)
			mtx(i,j) = T(1) / T(i+j+1.);

	//ludmtx.Write(  );
	//ludmtxB.Write( );

	HFGeom::QRDecomp< T, MTX >	qrd( mtx);

	qrd.Execute();

	cout << "det = " << setprecision(50) << qrd.GetDeterminant() << endl;

	return true;
}

template <class T, TSize NMAX>
void Test_QRDecomp::Exper( const TSize& nrow, const TSize& ncol)
{
	typedef HFGeom::GMatrix<NMAX,T> MTX;

	if ( max(nrow, ncol) > NMAX)
		THROW_INTERNAL( "Test_QRDecomp : nrow or ncol is greater then " << NMAX );

	MTX	mtx(nrow,nrow);

	for ( TSize i=0; i<ncol-1; ++i)
		mtx(i,i) = T(1);

	mtx(1,0) = T(2.34567891234567);

	mtx(ncol-2,ncol-1) = T(1.234567891234567);
	mtx(ncol-1,ncol-1) = T(1.0e-140);


	cout << "A = " << endl;
	mtx.Write();

	HFGeom::QRDecomp< T, MTX >	qrd( mtx);

	qrd.Execute();

	MTX	mtxQ, mtxR, mtxQT, mtxI, mtxE;

	qrd.AssembleQ( mtxQ);
	qrd.AssembleR( mtxR);

	cout << "Q = " << endl;
	mtxQ.Write();
	cout << "R = " << endl;
	mtxR.Write();
	cout << endl;

	mtxQT = mtxQ;
	mtxQT.Transp();

	cout << "QT * Q = " << endl;
	mtxI = mtxQT * mtxQ;
	mtxI.Write();

	cout << "Q * QT = " << endl;
	mtxI = mtxQ * mtxQT;
	mtxI.Write();

}



template <class T, TSize NMAX>
void Test_QRDecomp::Exper2( const TSize& n)
{
	typedef HFGeom::GMatrix<NMAX,T> MTX;

	if ( n >= NMAX)
		THROW_INTERNAL( "Test_QRDecomp : nrow or ncol is greater then " << NMAX );

	MTX	mtx(n,n);
	MTX	mtxB(n,1);

	for ( TSize i=0; i<n; ++i)
		for ( TSize j=0; j<n; ++j)
			mtx(i,j) = T(1) / T(i+j+1.);

	ofstream of( "out.txt");

	of << "A = " << endl;
	mtx.Write( 20, of);

	HFGeom::QRDecomp< T, MTX >	qrd( mtx);

	qrd.Execute();

	MTX	mtxQ, mtxR, mtxQT, mtxI, mtxE;

	qrd.AssembleQ( mtxQ);
	qrd.AssembleR( mtxR);

	of << "Q = " << endl;
	mtxQ.Write( 20, of);
	of << "R = " << endl;
	mtxR.Write( 20, of);
	of << endl;

	mtxQT = mtxQ;
	mtxQT.Transp();

	of << "QT * Q = " << endl;
	mtxI = mtxQT * mtxQ;
	mtxI.Write( 20, of);

	of << "Q * QT = " << endl;
	mtxI = mtxQ * mtxQT;
	mtxI.Write( 20, of);

}



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_TEST_QRDECOMP_H__  
